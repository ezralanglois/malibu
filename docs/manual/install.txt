Introduction
============

Installing malibu on your system is made easy with the use of `Boost.Jam`_. Boost.Jam is the build 
system that handles the complexes dependencies when compiling and linking the C++ code that comprises 
a majority of the algorithms in malibu.

The objectives of this tutorial are as follows:

* Introduce prerequisites to installing/using malibu
* Test your platform for compatibility
* Install basic malibu algorithms
* Install more algorithms
* Test algorithms on your system
* Troubleshoot your installation

malibu has been tested on both Windows XP and Linux (CentOS):

+--------------------+---------------------------+
| Operating System   | Compiler                  |
+====================+===========================+
| Windows XP         | Visual Studio 2005        |
+--------------------+---------------------------+
| Windows XP         | GCC 3.4.5                 |
+--------------------+---------------------------+
| CentOS             | GCC 4.3.0                 |
+--------------------+---------------------------+

Note that the following examples use Linux syntax; however, they should work in Windows with only 
minor adjustments.

.. _`Boost.Jam`: http://www.boost.org/doc/tools/build/index.html

Prerequisites
=============

Building (and using) malibu requires third-party source, libraries and applications:

* Building malibu

  - **A Compiler** - GCC, MSVC, etc.
  - `Bjam`_ - required to build malibu
  - `C4.5`_ (source code) - any C4.5 based algorithm (not included for license reasons)
  - `Boost`_ (headers and libraries) - Python bindings and posting to a database
  - `MPICH2`_ (headers and libraries) - distributed computing (cluster and multi-core/processor)

* Using malibu

  - `GNUplot`_
  - `Graphviz`_
  - `Doxygen`_

.. admonition:: Note

	To install C4.5 extract the archive to malibu/src/3rdparty/ml/c45

.. _`Bjam`: http://www.boost.org/doc/tools/build/index.html
.. _`C4.5`: http://www.rulequest.com/Personal/c4.5r8.tar.gz
.. _`Boost`: http://www.boost.org/
.. _`MPICH2`: http://www.mcs.anl.gov/research/projects/mpich2/
.. _`GNUplot`: http://www.gnuplot.info/
.. _`Graphviz`: http://www.graphviz.org/
.. _`Doxygen`: http://www.doxygen.org/


While each of the above third-party resource provides excellent documentation in terms of 
installation and use, here are some malibu centric tips:

.. sourcecode:: sh

	# Installing the boost libraries and headers
	
    $ bjam install --layout=system --build-type=complete


Basic Installation
==================

For the following instructions, ensure that your are in the *src* directory of your malibu source 
directory. The following assumes that you are familiar with the command line interface of your 
operating system and that you have the appropriate compiler installed.

The JAM files controlling the malibu build process try to detect the presence of many prerequisites 
and disable features when the corresponding prerequisites are not found. So, in order to ensure 
the desired algorithms are installed, the platform can be tested using the following command:

.. sourcecode:: sh

	[user@linux malibu/src] bjam test
	Testing third-party configuration...
	C4.5 configured.
	MPI configured.
	Boost configured.
	Boost.Asio configured.
	Boost.Python configured.
	Python configured (auto).
	...found 1 target...

After confirming that Bjam finds all the required prerequisites (for the algorithms desired), the
algorithms can be installed using the following command (if you have administrator privileges):

.. sourcecode:: sh

	[user@linux malibu/src] bjam install

This installs malibu algorithms into the following directory structure:

On Linux:

.. sourcecode:: sh

	#-----------------------
	# Root installation directory
	#-----------------------
	/usr/local
	#-----------------------
	# Executable installation directory
	#-----------------------
	/usr/local/bin
	#-----------------------
	# Library installation directory
	#-----------------------
	/usr/local/lib

On Windows:

.. sourcecode:: bat

	REM -----------------------
	REM Root installation directory
	REM -----------------------
	C:\\Malibu
	REM -----------------------
	REM Executable installation directory
	REM -----------------------
	C:\\Malibu\\bin
	REM -----------------------
	REM Library installation directory
	REM -----------------------
	C:\\Malibu\\lib

If you do not have administrator privileges or wish to choose the *root* directory, then use the 
following command:

.. sourcecode:: sh

	[user@linux malibu/src] bjam install --prefix=<desired-directory>

Note that this command only changes the *root* directory, executables and libraries will be installed 
in:

* Libraries: <desired-directory>/lib
* Executables: <desired-directory>/bin

If you wish to install malibu in your home directory (in Linux), then use the following:

.. sourcecode:: sh

	[user@linux malibu/src] bjam install --prefix=$HOME

List of Utilities (Installed by Default)
----------------------------------------

+-----------+---------------------------------------------------------------------------+
| Name      | Description                                                               |
+===========+===========================================================================+
| partition | partitions a dataset using a validation algorithm e.g. cross-validation   |
+-----------+---------------------------------------------------------------------------+
| bench     | calculates the metrics or plots for a set of predictions                  |
+-----------+---------------------------------------------------------------------------+
| deform    | transforms columns of attributes                                          |
+-----------+---------------------------------------------------------------------------+
| postfile  | post the prediction output file to distributed experiment manager         |
+-----------+---------------------------------------------------------------------------+

List of Learning Algorithms (Installed by Default)
---------------------------------------------------

+---------------+---------------------------------------------------------------+
| Name          | Description                                                   |
+===============+===============================================================+
| c45           | the classic decision tree implementation (third-party)        |
+---------------+---------------------------------------------------------------+
| willow        | an internal decision tree implementation                      |
+---------------+---------------------------------------------------------------+
| ind           | a set of decision tree algorithms                             |
+---------------+---------------------------------------------------------------+
| adtree        | the alternating decision tree                                 |
+---------------+---------------------------------------------------------------+
| knn           | Cover tree implementation of the *k*-nearest neighbor         |
+---------------+---------------------------------------------------------------+
| libsvm        | the classic libsvm implementation of support vector machines  |
+---------------+---------------------------------------------------------------+
| wwillowboost  | the weighted boosted tree algorithm                           |
+---------------+---------------------------------------------------------------+


Advanced Installation
======================

By default, malibu only installs a small core subset of algorithms. The JAM files, however, provide 
many options for installing algorithms including installing individual algorithms and groups of 
algorithms. For example, installing the malibu implementation of *libsvm* can be done with the 
following:

.. sourcecode:: sh

	[user@linux malibu/src] bjam install-libsvm

The message passing interface (MPI) version of the learning algorithms can be installed (or built) 
by prefixing the algorithm (or group) with *mpi-*:

.. sourcecode:: sh

	[user@linux malibu/src] bjam install-mpi-libsvm

Similarly, the dynamic python libraries that bind python to malibu can be installed by prefixing an 
algorithm or group with *py-*.

.. sourcecode:: sh

	[user@linux malibu/src] bjam install-py-libsvm

Groups of learning algorithms can also be installed, e.g. all classifiers.

.. sourcecode:: sh

	[user@linux malibu/src] bjam install-classifiers

malibu General Targets: Prefixes and Groups
--------------------------------------------

+-------------+---------------------------------------------------------+
| Name        | Description                                             |
+=============+=========================================================+
| mpi-        | prefix for MPI versions of learning algorithms          |
+-------------+---------------------------------------------------------+
| py-         | prefix for python library binding version of program    |
+-------------+---------------------------------------------------------+
| install-    | prefix for installing a program                         |
+-------------+---------------------------------------------------------+
| test-       | prefix for testing a program                            |
+-------------+---------------------------------------------------------+
| build       | builds utilities and default learning algorithms        |
+-------------+---------------------------------------------------------+
| install     | install utilities and default learning algorithms       |
+-------------+---------------------------------------------------------+
| test        | test configuration                                      |
+-------------+---------------------------------------------------------+
| learners    | default learning algorithms                             |
+-------------+---------------------------------------------------------+
| util        | all utilities                                           |
+-------------+---------------------------------------------------------+
| install-all | install all algorithms                                  |
+-------------+---------------------------------------------------------+
| build-all   | build all algorithms                                    |
+-------------+---------------------------------------------------------+

malibu Learning Targets
-----------------------

These targets correspond to groups of learning algorithms that handle a specific algorithms.

+--------------------+----------------------------------------------------------+
| Name               | Description                                              |
+====================+==========================================================+
| classifiers        | all binary classifiers                                   |
+--------------------+----------------------------------------------------------+
| probclassifiers    | calibration and probing on each classifier               |
+--------------------+----------------------------------------------------------+
| multiclassifiers   | output codes on each classifier                          |
+--------------------+----------------------------------------------------------+
| milclassifiers     | multiple-instance boosters on all non-meta classifiers   |
+--------------------+----------------------------------------------------------+
| regressors         | svm regression and quanting on all classifiers           |
+--------------------+----------------------------------------------------------+
| csclassifiers      | cost-sensitive classifiers                               |
+--------------------+----------------------------------------------------------+
| iwclassifiers      | importance-weighted classifiers                          |
+--------------------+----------------------------------------------------------+


Troubleshooting
===============

The following are common problems when dealing with malibu and their corresponding 
workarounds/solutions.

Problem: *WARNING: No MPI installation configured. (when MPI is installed)*
----------------------------------------------------------------------------

Boost.Jam may fail to autodetect the configuration for a tool; one common example is MPICH2 for the 
MPI configuration. This problem can be worked around using the *user-config.jam* file, which must be 
placed in the user's home directory (see `search paths`_). More information on user-config.jam can 
be found in the `advanced bjam`_. Specifying the location of your MPICH2 installation can be done by 
adding the following line to the user-config.jam:

.. sourcecode:: csharp

	using mpi : : <find-shared-library>mpich <find-shared-library>mpichcxx ;

For more examples on configuring other MPI implementations see the `Boost Tutorial`_



.. _`advanced bjam`: http://www.boost.org/doc/libs/1_36_0/doc/html/bbv2/advanced.html
.. _`Boost Tutorial`: http://www.boost.org/doc/tools/build/index.html
.. _`search paths`: http://www.boost.org/doc/libs/1_36_0/doc/html/bbv2/reference.html#bbv2.reference.init.config



Problem: *Algorithm gives unexpected result or crashes*
--------------------------------------------------------

malibu comes with built in testing suite that serves two purposes:

#. It allows the user to test all algorithms to ensure malibu works on an untested operating system.
#. It allows the user to quickly test if other algorithms fail with the same configuration file or dataset

Remember, when you report a problem, attach a configuration file and dataset (or link to that dataset). This ensures the problem will be fixed quickly. This fix can be significantly increased by using the test-suite with the valgrind option and attaching the corresponding output file.

.. sourcecode:: sh

	#
	# Current directory: malibu/src
	# 

	# Standard command
	bjam test-learn

	# Use the following if the program crashes (requires valgrind)
	bjam test-learn --memcheck

	# Testing more than one algorithm or dataset and have multiple cores/CPUs, e.g. 3
	bjam test-learn -j 3


