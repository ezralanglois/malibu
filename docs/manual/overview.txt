
Introduction
============

malibu is a machine learning workbench written primarily in C++. It can handle a number of supervised
learning problems including: classification, regression and multiple-instance learning. While malibu 
is targeted at remote Linux environments, it will compile on both Windows and Linux with minimal effort 
using several compilers. Each algorithm in malibu compiles into a separate binary executable with a 
command-line interface and support for descriptive configuration files. Since algorithm parameters tend 
to overlap, it can output groups of parameters and supports several input configuration files. The 
learning algorithms in malibu support cluster computing as well as multiple CPUs/cores using the message 
passing interface (MPI).

What is Machine Learning?
-------------------------

Examples of machine learning include the following:

* Data-mining

  - Learn to make decisions based on known examples
  - Extract rules from known examples
  
* Applications too complex to program

  - Speech recognition
  - Autonomous driving
  
* Self-customizing programs

  - Movie site learning user preferences
  - Search engine learning links most relevant to search term

A more detailed reference can be found in `Introduction to Machine Learning`_.

.. _`Introduction to Machine Learning`: http://robotics.stanford.edu/~nilsson/mlbook.html

What is a Machine Learning Workbench?
-------------------------------------

A machine learning workbench is more than a collection of learning algorithms. A machine learning
workbench provides additional features to facilitate learning including:

* Parsing and transforming datasets
* Partitioning datasets to estimate generalization
* Evaluating the performance of a learning algorithm
* User interface/management

A comprehensive collection of open source machine learning software can be found at `mloss.org`_. 

.. _`mloss.org`: http://mloss.org/software/


What makes malibu Different?
---------------------------------

malibu is primarily geared toward efficient learning in remote linux environments. It has a number
of features that make it ideal in this situation:

* Configuration files
* Cluster computing through MPI
* Parses many dataset formats
* Supports command-line evaluation of learning
* Automated parameter selection

Third-party Applications
========================

The malibu workbench requires and builds on third-party applications, libraries and code. The 
advantages of using third-party packages include:

* Less bugs
* Better optimization
* More features

However, there are some disadvantages that accompany third-party packages:

* Conflicting licenses
* Reduced Portability
* Reduced Flexibility

After considering these issues, the following third-party packages were chosen to
maximize usefullness.

Packaged
--------

malibu comes packaged with several third-party learning algorithms:

* `LibSVM`_: Support Vector Machines Library
* `IND Trees`_: Creation and manipulation of decision trees
* `CoverTree kNN`_: Fast Cover Tree Data Structure for K-Nearest Neighbor

It also comes packaged with the following third-party utilities:

* `MersenneTwister`_: Random Number Generator
* `bzip2`_: Data Compression
* `bzip2 iostream`_: Data Compression IOstream
* `Local Optimization`_: Local optimization methods

.. _`LibSVM`: http://www.csie.ntu.edu.tw/~cjlin/libsvm/
.. _`IND Trees`: http://opensource.arc.nasa.gov/project/ind/
.. _`CoverTree kNN`: http://hunch.net/~jl/projects/cover_tree/cover_tree.html
.. _`MersenneTwister`: http://www.math.keio.ac.jp/~matumoto/emt.html
.. _`bzip2`: http://www.bzip.org/
.. _`bzip2 iostream`: http://www.codeproject.com/KB/stl/zipstream.aspx
.. _`Local Optimization`: http://code.google.com/p/localoptimization/


Libraries
---------

malibu also has optional components, which require the following source code and libraries to be 
downloaded separately:

* `C4.5`_: C4.5 Decision Tree (source code)
* `Boost`_: Boost C++ Libraries
* `MPICH2`_: Cluster computing through MPI

.. _`C4.5`: http://www.rulequest.com/Personal/c4.5r8.tar.gz
.. _`Boost`: http://www.boost.org/
.. _`MPICH2`: http://www.mcs.anl.gov/research/projects/mpich2/

Applications
------------

malibu requires a number of programs to provide support:

* `Boost.Jam`_: *Make* system to compile libraries and executables
* `GNUplot`_: Creates a plot image from text file coordinates
* `Graphviz`_: Creates graph image from a DOT text file
* `Doxygen`_: Creates API documentation from source code
* `Python`_: malibu libraries extend Python for a script interface


.. _`Boost.Jam`: http://www.boost.org/doc/tools/build/index.html
.. _`GNUplot`: http://www.gnuplot.info/
.. _`Graphviz`: http://www.graphviz.org/
.. _`Doxygen`: http://www.doxygen.org/
.. _`Python`: http://www.python.org/

Features
========

malibu has a number of features that make it well suited to many machine learning tasks. 


User Interface
--------------

malibu comprises a set of executables and libraries. The executable have a command-line interface yet
promote project management. They accomplish this task through a unique *configuration file* system, which
has the following features:

* Supports implicit configuration files
 
  - A file that matches the learning algorithm name
  - A file that matches a portion of that name
  - A global configuration file

* Supports writing out groups of common parameters

  - E.g. Parameters that support dataset reading
  - E.g. Parameters that support validation

* Intuitive parameter overriding

  - E.g. Command-line parameters override configuration file parameters

The malibu libraries provide modules for Python, which allows malibu algorithms to be invoked from a 
Python script or a python graphical user interface (although none currently exist).


Learning Problems
-----------------

The malibu workbench handles supervised learning problems including:

* Classification

  - Binary classification
  - Cost-sensitive classification
  - Importance-weighted classification
  - Multi-class classification
  
* Regression

  - Quantile Regression
  - Probabilistic Regression

* Multiple-instance learning

Learning Validation
-------------------

One crucial task in machine learning is to estimate the *generalization* of the learned model. This is
done by evaluating a trained model on a held-out set of test data. Indeed, the held-out testing set is 
a set of examples that has not been used for parameter selection or training. One problem in estimating
the generalization of a learning algorithm is that labeled examples are often limited. That is, a larger
proportion of training data will build a strong model but the generalization performance will not be
significant. At the same time, a larger proportion of examples for testing increases the significance
of the performance yet a smaller proportion of training examples will lead to a weaker model yielding
pessimistic results. The following malibu validation algorithms attempt to alleviate these problems:

* Holdout
* Cross-validation
* Sampling
* Progressive-validation
* Testset: an independent file

Every malibu learning algorithm supports the following additional validation features:

* Writing out a file for each learned model
* Maintain class distribution in each partition
* Repeat validation algorithm for multiple runs
* Use previous random partitions to compare algorithms
 
Learning Evaluation
-------------------

Evaluating the results of a learning algorithm is akin to asking the question: What
did this learning learn? For example, precision and recall measure the ability of the
learning algorithm to both accurately predict and cover positive examples in a binary
classification problem. malibu supports a number metrics that can be broken down into
the following groups:

* Binary classification
* Multi-class classification
* Ranking
* Regression

Parameter Selection
-------------------

The process of learning often involves selecting parameters to optimize the generalization of the 
model. malibu currently uses a grid search in conjunction with the learning validation and
evaluation algorithms described previously to select the best parameters.

malibu uses a novel *tune parameter tree* that supports iterating through multiple conditional 
arguments.

Dataset Parsing
---------------

malibu supports parsing multiple dataset formats:

* Multiple labels
* Arbitrary class index
* Optional attribute header
* Optional relational label (MIL)
* Choose positive class
* Convert multi-class to binary
* Ensure dataset is parsed correctly
* Dataset statistics
* Multiple attribute separators

Advanced Features
=================

malibu has a number of advanced features including:

* Algorithm recovery
* Algorithm comparison
* Cluster computing
* Python binding
* Model graphics (for Trees)
* Configuration file

Cluster Computing
-----------------

malibu supports cluster computing distributing validation and parameter selection tasks to multiple
CPUs/cores or computers. It can also distribute independent ensemble learning tasks. One important
feature for cluster computing is algorithm recovery since tasks may be preempted for higher priority
jobs.

Python Binding
--------------

malibu executables are primarily command-line driven and support many of the common tasks required
for learning. However, research requires a more dynamic interface; to this end, malibu algorithms
can be compiled into libraries, which can be imported into Python as modules. Python has a large
community both in and outside the machine learning community making it ideal for fast prototyping
new ideas.

Utilities
=========

Many of the above described features found in a standard malibu learner are also useful as standalone 
utilities: this includes both libraries (Python modules) and executables. 

Benchmark Performance
---------------------

A learning algorithm in malibu outputs predictions for every example in the testset. However, a 
performance summary is often more useful, e.g. a set of metrics or a plot. malibu provides two 
programs: the *bench* executable and the bench Python module. Both of these programs provide
the sets of metrics and/or graphs summarizing the performance of a learning algorithm.

Dataset Partition
-----------------

Estimating the generalization of a learning requires the dataset to be partitioned into training
and testing sets. This is accomplished by the validation algorithms described above. malibu provides
the *partition* executable to divide the dataset based on a chosen validation algorithm.

Attribute Transformation
------------------------

An example attribute can have the many representations including:

* Real-valued: 10.02, 0.23
* Normalized: 0.01, 0.99
* Discreet-valued: 1, 2, 3
* Nominal: Sunny, Rainy, Windy
* Missing

Certain learning algorithm can only reliably learn from a subset of attribute representations. For example,
distance-based learning algorithms cannot handle missing or nominal values. The *deform* executable transforms
attributes in a dataset from one representation to another.


Credits
=======
The malibu workbench was developed by `Robert Ezra Langlois`_ under the guidance of `Prof. Hui Lu`_. The 
current web framework was developed by `Morten Kallberg`_. In addition, `Adam Carlson`_ and `George Genchev`_ 
helped develop the original website. `Matthew B. Carson`_ and `Nitin Bhardwaj`_ provided valuable assistance as 
two of the core users during the development of this project. One of the newest members of this project
is `Xishu Wang`_ who will soon be a codeveloper on this project.

.. _`Robert Ezra Langlois`: http://proteomics.bioengr.uic.edu/people/ezra/
.. _`Prof. Hui Lu`: http://proteomics.bioengr.uic.edu
.. _`Matthew B. Carson`: http://proteomics.bioengr.uic.edu/people/matt.htm
.. _`Morten Kallberg`: http://proteomics.bioengr.uic.edu/people.htm
.. _`Adam Carlson`: http://proteomics.bioengr.uic.edu/people.htm
.. _`George Genchev`: http://proteomics.bioengr.uic.edu/people.htm
.. _`Nitin Bhardwaj`: http://proteomics.bioengr.uic.edu/people/nitin.htm
.. _`Xishu Wang`: http://www2.uic.edu/~xwang59/


