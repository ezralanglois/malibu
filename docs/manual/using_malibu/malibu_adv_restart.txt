Setting up a learning algorithm for restart
============================================

A learning algorithm can only be restarted if the intermediary state is saved. This state is heavily dependent on the type of experiment run; hence a number of examples will be given for a particular experiment type. A malibu algorithm cannot be restart if the following arguments are not set when running the algorithm for the first time.

.. admonition:: Note

	Carefully consider to what extent you wish to save the internal state. An algorithm such as willow, which learns quickly, probably does not need to have every model during parameter selection saved whereas for a slower algorithm such as SVM, every model may save considerable time for a restart.

**Experiment 1: Cross-validation over some learning algorithm**

.. sourcecode:: sh

	# An example configuration file
	
	# Experiment type: Any type except *Testset*
	vtype: Cross-valiation
	
	# The shuffle argument ensures that the dataset
  	# partition is exactly the same
	#
	shuffle: some-file-name
	#
	# The model argument ensures that models during completed
	# learning are used instead of creating new learning models.
	#
	model: another-file-name

**Experiment 2: Learning algorithm Parameter Selection**

If the ``vtype`` is anything other than ``Testset`` , then use the configuration above in addition to the one below. The following example considers only the case when ``sel_vtype`` is ``Testset``.

.. sourcecode:: sh

	# An example configuration file
	sel_tune_restart: other-file-name


**Experiment 3: Cross-validation for Parameter Selection**

The same set of parameters as with Experiment 1 with an ``sel_`` prefix. Use this in conjunction with the parameters from Experiments 1 and 2.

.. sourcecode:: sh

	# An example configuration file
	
	# Experiment type: Any type except *Testset*
	sel_vtype: Cross-valiation
	
	# The shuffle argument ensures that the dataset
  	# partition is exactly the same
	#
	sel_shuffle: some-file-name
	#
	# The model argument ensures that models during completed
	# learning are used instead of creating new learning models.
	#
	sel_model: another-file-name

Restarting a learning algorithm
================================

If the learning algorithm exits prematurely, then the same configuration file can be used (including the above parameters) with on additional parameter.

.. sourcecode:: sh

	# An example configuration file
	
	# Only set on the restart execution, not the original
	restart: YES
