#!/usr/bin/env python2.4
#
import sys, os
from time import strftime

def command(cmd):
    h_stdout = os.popen(cmd, "r")
    lines = h_stdout.readlines()
    h_stdout.close()
    return lines

def create_host_file(hostfile, nodes):
    print hostfile
    fout=open(hostfile, 'w')
    for node in nodes:
        fout.write(node+"\n")
    fout.close()

def available_nodes():
    nodes=dict()
    lines = command("qnodes")[3:]
    valid = "Accepting Jobs"
    for line in lines:
        if line.find(valid) == -1:
            continue
        cols = line.lstrip().split(' ')
        load=int(cols[2])-int(cols[0])
        node=cols[3]
        if load in nodes:
            nodes[load].append(node)
        else:
            nodes[load]=[]
            nodes[load].append(node)
    nodes.keys().sort()
    return nodes

def get_nodes(nproc):
    nodedict = available_nodes()
    loads = nodedict.keys()
    loads.sort()
    loads.reverse()
    if int(loads[0]) == 0:
        print "No open machines"
        sys.exit(1)
    k=0
    nodes=[]
    while len(nodes) < nproc and k < len(loads) and loads[k] > 0:
        nodes.extend(nodedict[loads[k]])
        k=k+1
    
    if len(nodes) > nproc:
        nodes = nodes[0:nproc]
    if len(nodes) < nproc:
        print "Cannot find "+str(nproc)+" nodes: "+len(nodes)
        sys.exit(1)
    
    return nodes

def submit_job(nodes, scriptfile):
    nodelist = "+".join(nodes)
    print nodelist
    command("qsub -l nodes="+nodelist+" "+scriptfile)
    
def setup_mpi_ring(host, nproc, hostfile, mpiroot):
    print host
    command("rsh "+host+" \""+mpiroot+"/mpdboot -r rsh -n "+str(nproc)+" -f "+hostfile+" -v\"")

def create_script(cmds, scriptfile, logfile, host, mpiroot):
    print scriptfile
    fout=open(scriptfile, 'w')
    fout.write("#PBS -m bea\n")
    fout.write("#PBS -N MPI-malibu\n")
    fout.write("#PBS -o "+logfile)
    fout.write("\n")
    for cmd in cmds:
        fout.write(cmd+"\n")
    fout.write("\necho \"Closing MPI Ring...\"\n")
    fout.write("rsh "+host+" "+mpiroot+"/mpdallexit\n")
    fout.close()

def create_malibu(learn, data, validation, rootpth, nproc):
    outpth = os.path.join(rootpth, "out")
    errpth = os.path.join(rootpth, "err")
    
    learners=learn.split(' ')
    datasets=data.split(' ')
    cmds=[]
    pfx="mpiexec -n "+str(nproc)
    sfx="-implicit "+rootpth
    for learner in learners:
        for dataset in datasets:
            base=learner+"_"+dataset+"_"+validation
            out=os.path.join(outpth, base+".out")
            err=os.path.join(errpth, base+".err")
            cmd=pfx+" "+learner+" "+dataset+" "+validation+" "+sfx+" > "+out+" 2> "+err
            cmds.append(cmd)
    
    return cmds

if __name__ == "__main__":
    argv=sys.argv[1:]
    
    mpiroot="/usr/common/mpich2-1.0.1/bin"
    nproc = int(argv[0])
    learners = argv[1]
    datasets = argv[2]
    validation = argv[3]
    currpth = os.getcwd()
    rootpth = os.path.join(currpth, argv[4])
    
    base = strftime("%d%m%Y%H%M%S")
    hostsfile = os.path.join(rootpth, ".mpi", "hosts", base+"_mpd.conf")
    scrptfile = os.path.join(rootpth, ".mpi", "script", base+".sh")
    logfile = os.path.join(rootpth, base+".log")
    
    nodes = get_nodes(nproc)
    host = nodes[0]
    cmds = create_malibu(learners, datasets, validation, rootpth, nproc)
    create_script(cmds, scrptfile, logfile, host, mpiroot)
    create_host_file(hostsfile, nodes)
    setup_mpi_ring(host, nproc, hostsfile, mpiroot)
    submit_job(nodes, scrptfile)
