#!/usr/bin/env python2.4
import os,sys

def read_map(filename, tab):
    f=file(filename, 'ra')
    line = 'dummy'
    while line:
        line = f.readline()
        if not line:
            continue
        line=line.strip()
        elems=line.split(',')
        tab[elems[0]]=elems[1:]
    f.close()
    
def read_seqs(filename, out, tab):
    f=file(filename, 'ra')
    line = 'dummy'
    while line:
        line = f.readline()
        if not line:
            continue
        if not line[0] == '>':
            out.write(line)
            continue
        line=line[1:].strip()
        elems=line.split(' ', 2)
        id=elems[0][0:5]
        elems=tab[id]
        out.write('>'+id)
        for elem in elems:
            out.write(','+elem)
        out.write('\n')
    f.close()

if __name__ == "__main__":
    argv=sys.argv[1:]
    tab=dict()
    if len(argv) < 2:
        print 'Script requires at least two arguments'
        sys.exit(1)
    out=sys.stdout
    if len(argv) == 3:
        out=file(argv[2], 'wa')
    read_map(argv[0], tab)
    read_seqs(argv[1], out, tab)
    if len(argv) == 3:
        out.close()