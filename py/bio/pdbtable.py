#!/usr/bin/env python2.4
from Bio.PDB import *
import os

if __name__ == "__main__":
    parser=PDBParser()
    structure=parser.get_structure('1LMB', '1LMB.pdb')
    print structure.header['head']
    print structure.header['name']
    print structure.header['keywords']