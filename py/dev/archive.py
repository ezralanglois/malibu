#!/usr/bin/env python2.4
#
# Triarii: Deploy Script
# University of Illinois at Chicago
# Author: Robert Langlois
#
#   http://proteomics.bioengr.uic.edu/intra/malibu/home.html
#
#   This script archives a the src, script and py directories.

def targz(file, targets):
    import tarfile
    tar = tarfile.open(file, 'w:gz')
    for target in targets:
        tar.add(target)
    tar.close()

def compress(root, files, path):
    import os
    rmpath=""
    os.chdir(path)
    for i in range(0,len(files)):
        files[i] = os.path.join(root, files[i])
    out = root+".tgz"
    if not os.path.exists(out):
        targz(out, files)

if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        sys.exit("Usage: archive")
    
    files = [ "py", "script", "src"  ]
    root = "triarii"
    path = "../../.."
    compress(root, files, path)

