        if chdir:
            oldcwd = os.getcwd()
            if not os.path.isabs(chdir):
                chdir = os.path.join(self.workpath(chdir))
            if self.verbose:
                sys.stderr.write("chdir(" + chdir + ")\n")
            os.chdir(chdir)
        cmd = []
        if program and program[0]:
            if program[0] != self.program[0] and not os.path.isabs(program[0]):
                program[0] = os.path.join(self._cwd, program[0])
            cmd += program
        #    if interpreter:
        #        cmd = interpreter + " " + cmd
        else:
            cmd += self.program
        #    if self.interpreter:
        #        cmd =  self.interpreter + " " + cmd
        if arguments:
            cmd += arguments.split(" ")
        if self.verbose:
            sys.stderr.write(join(cmd, " ") + "\n")
        try:
            p = popen2.Popen3(cmd, 1)
        except AttributeError:
            (tochild, fromchild, childerr) = os.popen3(join(cmd, " "))
            if stdin:
                if type(stdin) is ListType:
                    for line in stdin:
                        tochild.write(line)
                else:
                    tochild.write(stdin)
            tochild.close()
            self._stdout.append(fromchild.read())
            self._stderr.append(childerr.read())                
            fromchild.close()
            self.status = childerr.close()
            if not self.status:
                self.status = 0
        except:
            raise
        else:
            if stdin:
                if type(stdin) is ListType:
                    for line in stdin:
                        p.tochild.write(line)
                else:
                    p.tochild.write(stdin)
            p.tochild.close()
            self._stdout.append(p.fromchild.read())
            self._stderr.append(p.childerr.read())
            self.status = p.wait()
            
        if self.verbose:
            sys.stdout.write(self._stdout[-1])
            sys.stderr.write(self._stderr[-1])
            
        if chdir:
            os.chdir(oldcwd)
#shutil.rmtree(dir, ignore_errors = 1)