#!/usr/bin/env python2.4
#
# Triarii: Test python dataset bindings
# University of Illinois at Chicago
# Author: Robert Langlois
#
#   http://proteomics.bioengr.uic.edu/intra/malibu/home.html
#
#   This script tests the triarii python dataset library bindings.

import sys

def test_dataset_size(d, elen, blen, alen, clen):
    if len(d) != elen:
        print "Dataset example cout not correct"
        sys.exit(2)
    if d.bagCount() != blen:
        print "Dataset bag count not correct"
        sys.exit(3)
    if d.attributeCount() != alen:
        print "Attribute count not correct"
        sys.exit(4)
    if d.classCount() != clen:
        print "Class count not correct"
        sys.exit(5)
    print "Dataset size test passed..."

def test_class(d):
    excnt = len(d)
    cnt = int(excnt/d.bagCount())
    j=0
    for i in range(0,d.bagCount()):
        d.setupbag(i, j, cnt)
        d.bagAt(i).y = i
        j = j + cnt
        if d.bagAt(i).y != i:
            print "Bag class not correct"
            sys.exit(6)
    for i in range(0,len(d)):
        val = (i % 2)
        d[i].y = val
        if d[i].y != val:
            print "Example class not correct"
            sys.exit(7)
    print "Class test passed..."

def test_attribute(d):
    import random
    import math
    acnt = d.attributeCount()
    attr = []
    for j in range(0,acnt):
        attr.append(random.random())
    for i in range(0,len(d)):
        cl=i%2
        d[i].copy(attr, cl)
        if d[i].y != cl:
            print "Example class not correct"
            sys.exit(8)
        for j in range(0,acnt):
            if d.is_sparse() == 0:
                if math.fabs(d[i][j]-attr[j]) > 1e-7:
                    print "Example attribute not correct"
                    sys.exit(9)
            else:
                if d[i][j].index == -1:
                    break
                if math.fabs(d[i][j].value-attr[d[i][j].index]) > 1e-7:
                    print "Example sparseattribute not correct"
                    sys.exit(10)
    
    for k in range(0,d.bagCount()):
        for i in range(0,len(d.bagAt(k))):
            cl=i%2
            d.bagAt(k)[i].copy(attr, cl)
            for j in range(0,acnt):
                if d.is_sparse() == 0:
                    if math.fabs(d.bagAt(k)[i][j]-attr[j]) > 1e-7:
                        print "Example attribute not correct"
                        sys.exit(9)
                else:
                    if d.bagAt(k)[i][j].index == -1:
                        break
                    if math.fabs(d.bagAt(k)[i][j].value-attr[d.bagAt(k)[i][j].index]) > 1e-7:
                        print "Example sparseattribute not correct"
                        sys.exit(10)
    print "Attribute test passed..."

def test_dataset(learn, elen, alen, clen, blen):
    d=learn.Dataset()
    d.resize_header(alen, clen)
    d.resize_body(elen, blen)
    test_dataset_size(d,elen,blen,alen,blen)
    test_class(d)
    test_attribute(d)

def parse_args(argv):
    i=0
    while i < len(argv):
        if argv[i].startswith("-lib="):
            lib=argv[i].split("=")
            sys.path.append(lib[1])
            del argv[i]
            continue
        i = i + 1
    return argv

if __name__ == "__main__":
    if len(sys.argv) < 1:
        sys.exit("Usage: download_dataset learn library-path")
    argv=parse_args(sys.argv[1:])
    for arg in argv:
        print "Testing "+arg+"..."
        learn=__import__(arg)
        test_dataset(learn, 4, 3, 2, 2)
        print

