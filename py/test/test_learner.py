#!/usr/bin/env python2.4
#
# Triarii: Test python dataset bindings
# University of Illinois at Chicago
# Author: Robert Langlois
#
#   http://proteomics.bioengr.uic.edu/intra/malibu/home.html
#
#   This script tests the triarii python dataset library bindings.
import sys

def test_learner(learner, dataset, predictions, argv):
    learner.read_args(argv)
    learner.read(dataset)
    learner.validate(dataset, predictions)
    if learner.is_root():
        for i in range(0, len(predictions)):
            for j in range(0, 10):
            #for j in range(0, len(predictions[i])):
                print predictions[i][j]
    dataset.clear()

def test_selector(learner, dataset, metric, argv):
    learner.read_args(argv)
    learner.read(dataset)
    learner.validate(dataset, metric)
    if learner.is_root():
        print metric.name+": ",
        print float(metric)
    dataset.clear()

def test_learners(util, argv, implicitpth):
    import glob
    pth = implicitpth
    if pth != "":
        pth = os.path.join(pth, "cfg")
    learner=util.Learner()
    selector=util.Selector()
    dataset=util.Dataset()
    metric=util.Metric()
    predictions=util.PredictionSetArray()
    files = glob.glob(os.path.join(pth,"test*.cfg"))
    argv.append("")
    
    val = learner.args()
    print val
    
    try:
        for file in files:
            if file.find("read_model") == -1:
                argv[len(argv)-1] = file
                print "on "+file
                sys.stdout.flush()
                test_learner(learner, dataset, predictions, argv)
                test_selector(selector, dataset, metric, argv)
        learner.finalize()
    except:
        learner.finalize()
        raise

def parse_arg(argv, flag, rm):
    i=0
    rv = ""
    while i < len(argv):
        if argv[i].startswith("-"+flag+"="):
            lib=argv[i].split("=")
            rv=lib[1]
            if rm == 1:
                del argv[i]
            else:
                del argv[i]
                argv.append(lib[0])
                argv.append(lib[1])
            break
        i = i + 1
    return rv

if __name__ == "__main__":
    import glob
    import os
    if len(sys.argv) < 1:
        sys.exit("Usage: test_learner.py library-path implicit-path")
    argv=sys.argv
    lib = parse_arg(argv, "lib", 1)
    if lib != "":
        sys.path.append(lib)
    lrn = parse_arg(argv, "learner", 1)
    learn=__import__(lrn)
    print "Testing "+lrn+"..."
    sys.stdout.flush()
    imp = parse_arg(argv, "implicit", 0)
    test_learners(learn, argv, imp)


