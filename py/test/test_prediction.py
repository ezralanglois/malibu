#!/usr/bin/env python2.4
#
# Triarii: Test python prediction bindings
# University of Illinois at Chicago
# Author: Robert Langlois
#
#   http://proteomics.bioengr.uic.edu/intra/malibu/home.html
#
#   This script tests the triarii python prediction library bindings.

import sys

def test_predictionset_size(p, n, b, r):
    if len(p) != r:
        print "Prediction set size not correct"
        sys.exit(2)
    for i in range(0, len(p)):
        if len(p[i]) != 0:
            print "Predictions size not correct - "+len(p[i])
            sys.exit(3)
    print "Prediction set test passed..."

def test_prediction_value(pred):
    pred.t = 4
    if pred.t != 4:
        print "Prediction threshold not correct"
        sys.exit(4)
    pred.p = 3
    if pred.p != 3:
        print "Prediction confidence not correct"
        sys.exit(5)
    pred.y = 1
    if int(pred.y) != 1:
        print "Prediction class - 1 not correct"
        sys.exit(6)
    print "Prediction test passed..."

def test_prediction(learn, n, b, r):
    p=learn.PredictionSetArray()
    p.resize(n,b,r)
    pred=learn.Prediction()
    test_predictionset_size(p, n, b, r)
    test_prediction_value(pred)

def parse_pred_args(argv):
    i=0
    while i < len(argv):
        if argv[i].startswith("-lib="):
            lib=argv[i].split("=")
            sys.path.append(lib[1])
            del argv[i]
            continue
        i = i + 1
    return argv

if __name__ == "__main__":
    if len(sys.argv) < 1:
        sys.exit("Usage: download_dataset learn library-path")
    argv=parse_pred_args(sys.argv[1:])
    for arg in argv:
        print "Testing "+arg+"..."
        learn=__import__(arg)
        test_prediction(learn, 4, 2, 2)
        print