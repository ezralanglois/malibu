g#!/usr/bin/env python2.4
#
# Triarii: Download Dataset Script
# University of Illinois at Chicago
# Author: Robert Langlois
#
#   http://proteomics.bioengr.uic.edu/intra/malibu/home.html
#
#   This script archives the triarii release of malibu.

from httplib import HTTP
from urlparse import urlparse
import sys,os,urllib,gzip,tarfile

def checkURL(url):
    p = urlparse(url)
    h = HTTP(p[1])
    h.putrequest('HEAD', p[2])
    h.endheaders()
    if h.getreply()[0] == 200: return 1
    else: return 0

def gunzip (locfile):
    extfile, ext = os.path.splitext(locfile)
    if os.path.exists(locfile) and (not os.path.exists(extfile)) and locfile.endswith('.gz'):
        print locfile
        fin = gzip.GzipFile(locfile, 'rb')
        fout = file(extfile, 'wb')
        fout.write(fin.read())
        fout.close()
        fin.close()
        sys.remove(locfile)

def untar_gzip (locfile):
    extfile, ext = os.path.splitext(locfile)
    if os.path.exists(locfile) and (not os.path.exists(extfile)) and locfile.endswith('.Z'):
        fin = tarfile.open(locfile, 'r:gz')
        fout = file(extfile, 'wb')
        fout.write(fin.read())
        fout.close()
        fin.close()
        sys.remove(locfile)

def download(urlfile, locfile):
    extfile, ext = os.path.splitext(locfile)
    locpath = os.path.dirname(locfile)
    if not os.path.exists(locpath):
        os.makedirs(locpath)
    if (not os.path.exists(locfile)) and (not os.path.exists(extfile)) and checkURL(urlfile):
        urllib.urlretrieve(urlfile, locfile)

def localfile(locpath, urlfile):
    ufile = urlparse(urlfile)
    locfile = urllib.url2pathname(ufile[2])
    locfile = os.path.join(locpath, os.path.basename(locfile))
    return locfile

def download_set(locpath, urlfile):
    locfile = localfile(locpath, urlfile)
    download(urlfile, locfile)
    gunzip(locfile)
    untar_gzip(locfile)



if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Usage: download_dataset <dir> <file1> ...")
    locpath=sys.argv[1]
    argv=sys.argv[2:]
    for arg in argv:
        download_set(locpath, arg)

