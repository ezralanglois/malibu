#!/usr/bin/env python2.4
#
# Triarii: Download UCI Datasets Script
# University of Illinois at Chicago
# Author: Robert Langlois
#
#   http://proteomics.bioengr.uic.edu/intra/malibu/home.html
#
#   This script archives the triarii release of malibu.

from download_dataset import *
import glob

uciurl="http://archive.ics.uci.edu/ml/machine-learning-databases/"
dpath="data"
exts=["data.Z", "data.gz", "data", "test.gz", "test"]

def uci_download_set(urlname, trans):
    args = urlname.split(" ")
    urlname=args[0]
    if urlname.find('/') == -1:
        urlspec=uciurl+""+urlname+"/"+urlname
    else:
        urlspec=uciurl+""+urlname
    
    for ext in exts:
        download_set(dpath, urlspec+"."+ext)
    #get urlname
    files=glob.glob(localfile(dpath,urlname)+"*")
    for file in files:
        if file.endswith(".std") or file.endswith(".dst"):
            return
    if len(args) > 0:
        trans.arguments(args)
    for file in files:
        trans.transform(file)
    trans.clear()
    for file in files:
        trans.distance(file)
    trans.clear()

if __name__ == "__main__":
    try:
        import pytransform
    except ImportError:
        sys.exit("Requires pytransform to be installed and place its location in the PYTHONPATH environment variable")
    if len(sys.argv) < 2:
        sys.exit("Usage: uci_download_dataset <file1> ...")
    
    argv=sys.argv[1:]
    d=pytransform.fi()
    for arg in argv:
        uci_download_set(arg, d)
