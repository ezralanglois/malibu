#!/usr/bin/env python2.4
# Module benchrst
#
import types

def exit(msg):
    import sys
    print msg
    sys.exit()

class MalibuBench:
    "Malibu bench.so interface class"
    def __init__(self, files=None):
        try:
            import bench
        except ImportError:
            exit("Requires the malibu bench.so to be installed  (bjam install-py-bench)")
        self.bn = bench.prediction_record_set_bn()
        self.bn_measure = bench.measure_bn()
        self.mc=bench.prediction_record_set_mc()
        
        if type(files) is types.StringType:
            self.readfile(files)
        elif type(files) is types.ListType:
            self.readfiles(files)
    
    def __len__(self):
        return len(self.bn)
    
    def readfile(self, file):
        self.mc.read_file(file)
        self.mc.filter_binary(self.bn)
        if len(self) == 0:
            exit("No predictions read")
    
    def readfiles(self, files):
        self.mc.read_files(files)
        self.mc.filter_binary(self.bn)
        if len(self) == 0:
            exit("No predictions read")
    
    def avg_metric(self, idx, header):
        return self.bn_measure.avg_metric(self.bn[idx], header)
    
    def fill_values(self, values, idx, headers, prec):
        for i in xrange(0,len(headers)):
            values[i] = self.value(idx, headers[i], prec)
        
    def value(self, idx, header, prec):
        val = self.record_value(idx, header)
        if val != "":
            return val
        val = "%*.*f"%(prec[0], prec[1], self.avg_metric(idx, header))
        if len(val) > prec[0]:
            val = "inf"
        return val
    
    def record_value(self, idx, header):
        if header == "Algorithm":
            return self.bn[idx].algorithm
        elif header == "Dataset":
            return self.bn[idx].dataset
        elif header == "Validation":
            return self.bn[idx].validation
        elif header == "Time":
            return "0" #self.bn[idx].time
        else:
            return ""
    
    def record_header_list(self):
        headers = ["Algorithm", "Validation", "Dataset"]
        return headers
    
    def record_headers(self, unique):
        headers = self.record_header_list()
        if unique:
            for i in xrange (len(headers)-1, -1, -1):
                if len(set(self.column_list(headers[i]))) < 2:
                    del headers[i]
        else:
            for i in xrange (len(headers)-1, -1, -1):
                if len(set(self.column_list(headers[i]))) > 1:
                    del headers[i]
        return headers
    
    def plots(self):
        return self.bn_measure.plots()

    def plot(self, idx, name, n):
        return self.bn_measure.plot(self.bn[idx], name, n)
    
    def groups(self):
        return self.bn_measure.metric_groups()
    
    def column_list(self, header):
        if header == "Algorithm":
            return self.algorithm_list()
        elif header == "Dataset":
            return self.dataset_list()
        elif header == "Validation":
            return self.validation_list()
        return []

    def algorithm_list(self):
        tmp = []
        for i in range(0,len(self.bn)):
            tmp.append(self.bn[i].algorithm)
        return tmp
    
    def dataset_list(self):
        tmp = []
        for i in range(0,len(self.bn)):
            tmp.append(self.bn[i].dataset)
        return tmp
    
    def validation_list(self):
        tmp = []
        for i in range(0,len(self.bn)):
            tmp.append(self.bn[i].validation)
        return tmp

def parse_first_file(files):
    if len(files) == 0:
        exit("bench requires at least one input file")
    import os
    output=files[0]
    if not os.path.exists(output):
        del files[0]
    elif os.path.isdir(output):
        del files[0]
    elif output.endswith('.tar') or output.endswith('.gz'):
        del files[0]
    else:
        output, ext = os.path.splitext(output)
    return output

def stripallext(filename):
    import os
    ext="dummy"
    stripname=filename
    while ext != "":
        stripname, ext = os.path.splitext(stripname)
    return stripname

def remove_files(path):
    import os
    for root, dirs, files in os.walk(path, topdown=False):
        for file in files:
            try:
                os.remove(os.path.join(root, file))
            except:
                pass
        for dir in dirs:
            try:
                # Make attributes normal so dir can be deleted.
                win32api.SetFileAttributes(os.path.join(root, d), win32con.FILE_ATTRIBUTE_NORMAL)
            except:
                pass
            try:
                os.rmdir(os.path.join(root, dir))
            except:
                pass
    os.rmdir(path)

def archive(infile, outfile):
    if infile != outfile and (outfile.endswith('.gz') or outfile.endswith('.tar')):
        import tarfile, os
        flag="w"
        if outfile.endswith('gz'):
            flag+=":gz"
        fout = tarfile.open(outfile, flag)
        fout.add(infile)
        fout.close()
        remove_files(infile)

