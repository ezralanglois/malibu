#!/usr/bin/env python2.4
# Module bench_nfold
#
from bench_impl import *

def average_metric(list, type):
    sum=0.0
    tot=0
    pred=MalibuBench(list)
    for idx in xrange(0, len(pred)):
        sum+=pred.avg_metric(idx, type)
        tot+=1
    sum=sum/tot
    return sum

def file_part(file):
    parts=file.split("_")
    dataset=parts[0]
    type=parts[1]
    algo=parts[2].split('.')[0]
    return dataset, type, algo
    
def file_dict(sets, list):
    for file in argv:
        dataset, type, algo = file_part(file)
        if not sets.has_key(algo):
            sets[algo]=dict()
        if not sets[algo].has_key(dataset):
            sets[algo][dataset]=dict()
        sets[algo][dataset][type]=[]
    
    for file in argv:
        dataset, type, algo = file_part(file)
        sets[algo][dataset][type].append(file)

def write_metric(sets, metric):
    header=1
    for d in sets.keys():
        if header == 1:
            sys.stdout.write('dataset')
            for t in sets[d].keys():
                sys.stdout.write("\t"+t)
            sys.stdout.write("\n")
            header=0
        sys.stdout.write(d)
        for t in sets[d].keys():
            sys.stdout.write("\t"+str(average_metric(sets[d][t], metric)))
        sys.stdout.write("\n")
    

if __name__ == "__main__":
    import os,sys
    metric=sys.argv[1]
    argv=sys.argv[2:]
    if len(argv) < 2:
        exit('Too few input files')
    
    sets=dict()
    file_dict(sets, argv)
    
    for a in sets.keys():
        sys.stdout.write(a+"\n")
        write_metric(sets[a], metric)


    