#!/usr/bin/env python2.4
# Module benchrst
#


from bench_impl import *

def get_title(pred, title):
    headers = pred.record_headers(0)
    if len(headers) == 0:
        return title
    title+=":"
    for head in headers:
        title+=" "
        title+=pred.record_value(0, head)
    return title

def get_label(pred, i):
    headers = pred.record_headers(1)
    str=""
    for head in headers:
        str+=pred.record_value(i, head)
        str+=" "
    return str

def get_styles():
    styles = [['b', '--', 's'],
              ['g',':', '>'],
              ['r', '-.', 'd'],
              ['c', '-', 'v'],
              ['m', '--', '+'],
              ['k', ':', 'H'],
              ['b', '-.', 'p'],
              ['g', '-', '<'],
              ['r', '--', '^'],
              ['c', ':', 'x'],
              ['m', '-.', 'o'],
              ['k', '-', 'D'] ]
    return styles

def plot_coord(pylab, x, y, styles, label):
    import math
    pylab.plot(x, y, ls=styles[1], color=styles[0], label=label)
    if len(x) < 11:
        inc = 1
    else:
        inc=int(math.ceil(len(x)/10))
    
    pylab.plot(x[::inc], y[::inc], ls='', marker=styles[2])

def plot_lines(pylab, y1, y2, styles, label):
    x = [0, 1]
    for i in xrange(0, len(y1)):
        y = [ y1[i], y2[i] ]
        pylab.plot(x, y, ls='-', color='r')

def normalize_coord(coord, name):
    import sys
    maxval = max(coord[0])
    minval = min(coord[0])
    
    mulval = 1.0 / (maxval-minval)
    if minval < 0.0 or maxval > 1.0:
        for i in xrange(0, len(coord[0])):
            coord[0][i] = (coord[0][i]-minval)*mulval
        sys.stderr.write("Warning: normalizing lift for: "+name+"\n")
    
def plot2file(pylab, outpfx, pred, plotname, costlines):
    uselegend=False
    styles=get_styles()
    ymax = 1
    if len(styles) < len(pred):
        exit("bench_plot can only compare "+len(styles)+" files")
        
    if plotname[3] == 'LFC':
        for i in xrange(0, len(pred)):
            coord = pred.plot(i, plotname[0], costlines)
            maxtmp = max(coord[1])
            if maxtmp > ymax:
                ymax=maxtmp
    
    pylab.clf()
    pylab.figure(figsize=(4.5,4.5))
    pylab.hold(True)
    for i in xrange(0, len(pred)):
        coord = pred.plot(i, plotname[0], costlines)
        
        ltext=get_label(pred, i)
        if plotname[3] == 'LFC':
            normalize_coord(coord, ltext)
        
        if plotname[3] == 'CST':
            plot_lines(pylab, coord[0], coord[1], styles[i], ltext)
        elif plotname[3] == 'ENVX':
            plot_lines(pylab, coord[0][0:costlines], coord[1][0:costlines], styles[i], ltext)
            plot_coord(pylab, coord[0][costlines:], coord[1][costlines:], styles[i], ltext)
        else:
            plot_coord(pylab, coord[0], coord[1], styles[i], ltext)
        if not uselegend and ltext != "":
            uselegend=True

    if uselegend:
        if plotname[3] == 'ROC' or plotname[3] == 'REL':
            pylab.legend(loc='lower right')
        elif plotname[3] == 'PRC' or plotname[3] == 'NPR':
            pylab.legend(loc='lower left')
        else: # Cost, Lift, ENV, ENVX
            pylab.legend(loc='upper right') 
    
    pylab.hold(False)
    pylab.title(plotname[0]) #pylab.title(get_title(pred, plotname[0]))
    pylab.xlabel(plotname[1])
    pylab.ylabel(plotname[2])
    pylab.axis([0, 1, 0, ymax])
    plotfile=outpfx+"_"+plotname[3]+".png"
    pylab.savefig(plotfile, dpi=200)
    return plotfile

# GNU plot format

def plots_write(pred, pfx):
    import matplotlib
    matplotlib.use('Cairo')
    import pylab
    costlines=4
    
    plotfiles=[]
    plots=pred.plots()
    for plot in plots:
        if (not plot[3] == 'CST' and not plot[3] == 'ENVX') or len(pred) == 1:
            plotfiles.append(plot2file(pylab, pfx, pred, plot, costlines))
    return plotfiles

if __name__ == "__main__":
    import os,sys
    
    argv=sys.argv[1:]
    output=parse_first_file(argv)
    pfx=stripallext(output)
    if not os.path.exists(pfx):
        os.makedirs(pfx)
    pred=MalibuBench(argv)
    plots_write(pred, os.path.join(pfx, pfx))
    archive(pfx, output)

