#!/usr/bin/env python2.4
# Module benchrst
#

def read_prediction_file(name, metric_name, param_name):
    fin = file(name)
    coord = []
    x=0
    y=0
    ydict=dict()
    cdict=dict()
    for line in fin:
        line = line.rstrip()
        elems=line.split('\t')
        if elems[0] == "#TH":
            try:
                y = elems.index(metric_name)
            except ValueError:
                print "Cannot find: ", metric_name
                raise
            try:
                x = elems.index(param_name)
            except ValueError:
                print "Cannot find: ", param_name, elems
                raise
            
        if elems[0] != "#TV" and elems[0] != "#TS":
            continue
        acc=1.0-float(elems[y])
        iter=float(elems[x])
        if iter in cdict:
            ydict[iter]+=acc
            cdict[iter]+=1
        else:
            ydict[iter]=acc
            cdict[iter]=1
    
    fin.close()
    
    for key in cdict.keys():
        avg = ydict[key]/cdict[key]
        coord.append( (key, avg) )
        
    return coord
    
def write_plot_header(out, lines):
    out.write("plot ")
    n=1
    for key in lines:
        if n>1:
            out.write(',')
        out.write("\"-\" using 1:2 with lines lw 3 lt "+str(n)+" title \""+key+"\"")
        n = n + 1
    out.write("\n")

def write_plot_coord(out, coords, max_iter):
    n=0
    for c in coords:
        if max_iter > 0 and n > max_iter:
            break;
        x, y = c
        out.write(str(x)+"\t"+str(y)+"\n")
        n=n+1
    out.write('e\n\n')

if __name__ == "__main__":
    import os,sys
    
    metric_name='Accuracy'
    param_name='Iteration'
    
    argv=sys.argv[1:]
    
    max_iter=0
    try:
        max_iter = int(argv[0])
        argv=argv[1:]
    except ValueError:
        max_iter=0
    
    plots=dict()
    keyx=0
    keyy=0
    for arg in argv:
        plot=os.path.basename(arg)
        c=read_prediction_file(arg, metric_name, param_name)
        plots[plot]=c
    
    metric_name='Error'
    sys.stdout.write("set key "+str(keyx)+","+str(keyy)+"\n")
    sys.stdout.write("set key right top\n")
    sys.stdout.write("set terminal postscript enhanced color \"Times-Roman\" 24\n")
    sys.stdout.write("set output \"plot.eps\"\n")
    sys.stdout.write("set title \""+param_name+" versus "+metric_name+"\"\n")
    sys.stdout.write("set xlabel \""+param_name+"\"\n")
    sys.stdout.write("set ylabel \""+metric_name+"\"\n")
    
    write_plot_header(sys.stdout, plots.keys())
    for plot in plots:
        write_plot_coord(sys.stdout, plots[plot], max_iter)
    