#!/usr/bin/env python2.4
# Module benchrst
#

from benchrst import docutils_write_files
from bench_impl import remove_files

def latex2pdf(outfile):
    import os,subprocess
    cwd=os.getcwd()
    os.chdir(outfile)
    
    try:
        ret=subprocess.call(["pdflatex", outfile+".tex"], stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT)
        if ret != 0:
            print "WARNING: failed to create PDF"
    except OSError, e:
        print >>sys.stderr, "WARNING: failed to create PDF:", e
    #remove tmp
    os.remove(outfile+".out")
    os.remove(outfile+".log")
    os.remove(outfile+".aux")
    os.chdir(cwd)
    
if __name__ == "__main__":
    import sys,os 
    if len(sys.argv[1:]) > 0:
        if sys.argv[1].endswith('.tar') or sys.argv[1].endswith('.gz'):
            exit('cannot archive pdfs')
    
    outfile=docutils_write_files('latex')
    latex2pdf(outfile)
    os.rename(os.path.join(outfile, outfile+".pdf"), outfile+".pdf")
    remove_files(outfile)
    
    
#popen("pdflatex "+filename)
#-output-directory