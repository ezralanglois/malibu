#!/usr/bin/env python2.4
# Module benchrst
#
from bench_impl import *
from bench_plot import plots_write

def max_length(lst, prec):
    if len(lst) == 0:
        return prec
    n=0
    for val in lst:
        if n < len(val):
            n = len(val)
    return n

def max_column_length(pred, headers, prec):
    lens = [ 0 for i in headers ]
    for i in xrange(0,len(headers)):
        lens[i] = max(len(headers[i]), max_length(pred.column_list(headers[i]), prec[0]))
    return lens

def rst_section(str, ch):
    out="\n"
    out+=str
    out+='\n'
    for i in xrange(0,len(str)):
        out += ch
    out+='\n'
    return out

def rst_prediction_summary(out, pred, sec):
    headers = pred.record_headers(0)
    if len(headers) > 0:
        out.write(rst_section("Experiment", sec))
        for head in headers:
            out.write( "| %s: %s\n"%(head,pred.record_value(0, head) ) )
        out.write("\n")

def rst_metric_table_cell(out, vals, end):
    n=len(vals)
    if n > 0:
        out.write(end)
    for i in xrange(0,n):
        out.write(vals[i])
        out.write(end)
    out.write("\n")

def rst_metric_table_row(out, cells, div, fin):
    if len(fin) > 0:
        rst_metric_table_cell(out, div, '+')
    rst_metric_table_cell(out, cells, '|')
    if len(fin) > 0:
        rst_metric_table_cell(out, fin, '+')
    else:
        rst_metric_table_cell(out, div, '+')

def pad(strs, lens, ch, add):
    out = []
    for i in xrange(0,len(strs)):
        out.append( strs[i].center(lens[i]+add, ch) )
    return out

def rst_metric_table(out, pred, headers, prec):
    lens = max_column_length(pred, headers, prec)
    vals = [ "" for i in headers ]
    div = pad([ "" for i in lens ], lens, '-', 2)
    fin = pad([ "" for i in lens ], lens, '=', 2)
    rst_metric_table_row(out, pad(headers, lens, ' ', 2), div, fin)
    fin=[]
    for idx in xrange(0, len(pred)):
        pred.fill_values(vals, idx, headers, prec)
        rst_metric_table_row(out, pad(vals, lens, ' ', 2), div, fin)
    out.write("\n")

def rst_metric_table_groups(out, pred, prec, sec):
    rheaders = pred.record_headers(1)
    groups = pred.groups()
    for group in groups:
        out.write(rst_section(group[0], sec))
        headers = []
        headers.extend( rheaders )
        headers.extend(group[1])
        rst_metric_table(out, pred, headers, prec)

def rst_write_plots(out, pred, plots):
    import os
    #.. image:: images/biohazard.png
    for plot in plots:
        files = plot.split(os.sep)
        file=os.sep.join(files[1:])
        out.write(".. image:: "+file+"\n")
        out.write("\t:height: 4in\n")
        out.write("\t:width: 4in\n")

def rst_write(out, pred):
    prec = [7, 3]
    rst_prediction_summary(out, pred, '=')
    rst_metric_table_groups(out, pred, prec, '=')

def rst_string(pred):
    import StringIO
    output = StringIO.StringIO()
    rst_write(output, pred)
    return output.getvalue()

def rst_string(pred, plots):
    import StringIO
    output = StringIO.StringIO()
    rst_write(output, pred)
    rst_write_plots(output, pred, plots)
    return output.getvalue()

def docutils_write(out, pred, writer):
    try:
        from docutils import core
    except ImportError:
        exit("Requires the docutils to be installed")
    text = rst_string(pred)
    html = core.publish_string(source=text, writer_name=writer)
    out.write(html)

def docutils_write(out, pred, writer, plots):
    try:
        from docutils import core
    except ImportError:
        exit("Requires the docutils to be installed")
    if writer == 'rst':
         rst_write(out, pred)
         rst_write_plots(out, pred, plots)
    else:
        text = rst_string(pred, plots)
        html = core.publish_string(source=text, writer_name=writer)
        out.write(html)

def docutils_write_files(writer):
    import os,sys
    argv=sys.argv[1:]
    if len(argv) == 0:
        exit('Too few input files')
    output=parse_first_file(argv)
    pfx=stripallext(output)
    plotpfx=os.path.join(pfx, 'figs')
    if not os.path.exists(plotpfx):
        os.makedirs(plotpfx)
    pred=MalibuBench(argv)
    plots=plots_write(pred, os.path.join(plotpfx, pfx))
    ext = writer
    if ext == "latex":
        ext="tex"
    
    rstfile=file(os.path.join(pfx, pfx)+"."+ext, 'wa')
    docutils_write(rstfile, pred, writer, plots)
    rstfile.close()
    archive(pfx, output)
    return pfx
    

if __name__ == "__main__":
    import sys
    sys.path.append("/storage/home/ezra/lib")
    docutils_write_files('rst')
   
    
