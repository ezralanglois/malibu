#!/usr/bin/env python2.4
# Module color_pdb
#
import subprocess
import urllib, os, sys, gzip

class PDBPrediction:
    def __init__(self, chain, index, id, p, y, t):
        self.chain = chain
        self.index = index
        self.id = id
        self.p = p
        self.y = y
        self.c = 0
        self.t = t

class prediction:
    def __init__(self, y, p):
        self.y = y
        self.p = p

def is_not_prediction_line(line):
    elems = line.split('\t')
    return elems[0] != "#SP" or len(elems) < 3

def is_not_pdb_id(line):
    return line.find('_') == -1

def parse_prediction_line(line):
    elems=line.split('\t')
    id=elems[1].upper()
    ps=elems[2].split(' ')
    y = ps[0]
    p = ps[1]
    t = ps[2]
    
    return (id, float(p), int(y), float(t))

def parse_id(line):
    ids=line.split('_')
    pdb = ids[0]
    chain = ids[1]
    index = ids[2]
    resnm = ids[3]
    return (pdb, chain, index, resnm)

def list_pdb_in_predictions(predfile, pos_hit, pos_miss, neg_hit, neg_miss):
    pdbs=dict()
    
    if not os.path.exists(pos_hit):
        os.makedirs(pos_hit)
    if not os.path.exists(pos_miss):
        os.makedirs(pos_miss)
    if not os.path.exists(neg_hit):
        os.makedirs(neg_hit)
    if not os.path.exists(neg_miss):
        os.makedirs(neg_miss)

    fin = file(predfile, 'ra')
    for line in fin:
        if is_not_prediction_line(line):
            continue
        id, p, y, t = parse_prediction_line(line)
        if is_not_pdb_id(id):
            continue
        pdb, chain, index, resnm = parse_id(id)
        if y > 0:
            if float(p) > t:
                pdbs[pdb] = pos_hit
            else:
                if pdb in pdbs:
                    if pdbs[pdb] == neg_miss or pdbs[pdb] == neg_hit:
                        pdbs[pdb] = pos_miss
                else:
                    pdbs[pdb] = pos_miss
        else:
            if p > t:
                if pdb in pdbs:
                    if pdbs[pdb] != pos_miss and pdbs[pdb] != pos_hit:
                        pdbs[pdb] = neg_miss
                else:
                    pdbs[pdb] = neg_miss
            else:
                if pdb not in pdbs:
                    pdbs[pdb] = neg_hit
    return pdbs

def read_predictions_for_pdb_dict(preds, predfile, pdbname, idx):  
    pdbname = pdbname.upper()
    fin = file(predfile, 'ra')
    th = determine_threshold(predfile, 0.99)
    mask=1
    mask<<=idx
    for line in fin:
        if is_not_prediction_line(line):
            continue
        id, p, y, t = parse_prediction_line(line)
        if is_not_pdb_id(id):
            continue
        pdb, chain, index, resnm = parse_id(id)
        if pdb != pdbname:
            continue
        if y > 0 and p > th:
            if id in preds:
                preds[id].c |= mask
            else:
                preds[id]=PDBPrediction(chain, index, resnm, p, y, t)
                preds[id].c = mask

def read_predictions_for_pdb(preds, predfile, pdbname):
    pdbname = pdbname.upper()
    fin = file(predfile, 'ra')
    for line in fin:
        if is_not_prediction_line(line):
            continue
        id, p, y, t = parse_prediction_line(line)
        if is_not_pdb_id(id):
            continue
        pdb, chain, index, resnm = parse_id(id)
        if pdb != pdbname:
            continue
        preds.append(PDBPrediction(chain, index, resnm, p, y, t))
    fin.close()

def prediction_normalize(preds):
    max_pos=0
    max_neg=0
    min_pos=1e200
    min_neg=1e200
    
    for pred in preds:
        if pred.p < 0:
            val = -pred.p
            max_neg = max(max_neg, val)
            min_neg = min(min_neg, val)
        else:
            val = pred.p
            max_pos = max(max_pos, val)
            min_pos = min(min_pos, val)
    
    rng_neg=max(abs(max_neg-min_neg),1)
    rng_pos=max(abs(max_pos-min_pos),1)
    for pred in preds:
        if pred.p < 0:
            pred.c=abs((pred.p+min_neg)/rng_neg) #*0.75+0.25
        else:
            pred.c=((pred.p-min_pos)/rng_pos)*0.75+0.25

def color_id(p, y):
    str=""
    if p < 0:
        str="neg"
    else:
        str="pos"
    return '%s_%d' % (str, abs(round(p*100)) )

def add_color(colormap, p, y, c):
    id = color_id(p,y)
    if(p>0):
        colormap[id] = "[%.2f,0,0]" % c;
    else:
        colormap[id] = "[0,0,%.2f]" % c;

def get_pdb(pdbfile):
    pdbfile=os.path.basename(pdbfile)
    return pdbfile.split('.',1)[0]

def pymol_color_by_predictions(cmd, pdbfile, predfiles):
    preds=dict()
    pdbname=get_pdb(pdbfile)
    idx=0
    for predfile in predfiles:
        read_predictions_for_pdb_dict(preds, predfile, pdbname, idx)
        idx=idx+1
    
    # AdaBoost NOMIL - 1 - black
    # AdaBoost       - 2 - blue
    # BOTH AB        - 3 - purple
    # C2MM           - 4 - brown
    # C2MM  + NO     - 5 - orange
    # C2MM  + AB     - 6 - magenta
    # ALL            - 7 - red
    colors=['none', 'black', 'blue', 'purple', 'brown', 'orange', 'magenta', 'red']
    keys=preds.keys()
    for key in keys:
        #cid = color_id(pred.p, pred.y)
        pred=preds[key]
        cid = colors[pred.c]
        id=str(pred.chain)+str(pred.index)
        cmd.append("select "+id+", chain "+pred.chain+" and resi "+str(pred.index))
        cmd.append("color "+cid+", "+id)
        if pred.p > 0 and pred.y > 0:
            cmd.append("show sticks, "+id)
    
    
def pymol_color_by_prediction(cmd, pdbfile, predfile):
    preds=[]
    pdbname=get_pdb(pdbfile)
    read_predictions_for_pdb(preds, predfile, pdbname)
    prediction_normalize(preds)
    colormap=dict()
    for pred in preds:
        add_color(colormap, pred.p, pred.y, pred.c)
    
    for key in colormap.keys():
        cmd.append("set_color "+key+", "+colormap[key])

    for pred in preds:
        cid = color_id(pred.p, pred.y)
        id=str(pred.chain)+str(pred.index)
        cmd.append("select "+id+", chain "+pred.chain+" and resi "+str(pred.index))
        cmd.append("color "+cid+", "+id)
        if pred.p > 0 and pred.y > 0:
            cmd.append("show sticks, "+id)

def pymol_load_pdb(cmd, file):
    name = os.path.basename(file)
    cmd.extend( [
        "load "+name,
        "hide all",
        "bg_color white",
        "turn y, 90",
        "show cartoon, all"
     ])

def pymol_save_png(cmd, file):
    pngfile=file.split('.',1)[0]+".png"
    cmd.extend( [
        "set stick_radius, 0.5",
        "set antialias = 1",
        "ray",
        "png "+pngfile,
     ])

def pymol_color_cmd(cmd, pdbfile, predfile):
     pymol_load_pdb(cmd, pdbfile)
     pymol_color_by_prediction(cmd, pdbfile, predfile)
     pymol_save_png(cmd, pdbfile)

def pymol_color_pml(pdbfile, predfile):
     cmd = []
     pymol_color_cmd(cmd, pdbfile, predfile)
     fout=file(pdbfile.split('.',1)[0]+".pml", "wa")
     fout.write("\n".join(cmd))
     fout.close()
   
def pymol_color_png(pdbfile, predfile):
     proc = subprocess.Popen("pymol -cqp",
                        shell=True,
                        stdin=subprocess.PIPE,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                        )
     cmd = []
     pymol_color_cmd(cmd, pdbfile, predfile)
     stdout_value, stderr_value = proc.communicate("\n".join(cmd))

def download(urlfile, locfile):
    if (not os.path.exists(locfile)):
        urllib.urlretrieve(urlfile, locfile)

def gunzip (locfile):
    extfile, ext = os.path.splitext(locfile)
    if os.path.exists(locfile) and (not os.path.exists(extfile)) and locfile.endswith('.gz'):
        fin = gzip.GzipFile(locfile, 'rb')
        fout = file(extfile, 'wb')
        fout.write(fin.read())
        fout.close()
        fin.close()
        os.remove(locfile)

def download_pdb_file(pdbcode, path):
    pdbgzip=pdbcode.upper()+".pdb"
    if (not os.path.exists(os.path.join(path, pdbgzip))):
        print "Downloading PDB file ", pdbcode
        pdbgzip=pdbgzip+".gz"
        download("http://pdbbeta.rcsb.org/pdb/files/"+pdbgzip, os.path.join(path, pdbgzip))
        gunzip(os.path.join(path, pdbgzip))

def prediction_compare(x, y):
    if x.p < y.p:
        return 1
    elif x.p == y.p:
        return 0
    else:
        return -1

def determine_threshold(predfile, spe_min):
    preds=[]
    fin = file(predfile, 'ra')
    ptot = ntot = 0.0
    for line in fin:
        if is_not_prediction_line(line):
            continue
        id, p, y, t = parse_prediction_line(line)
        if is_not_pdb_id(id):
            continue
        preds.append(prediction(y, p))
        if y > 0.0:
            ptot = ptot + 1.0
        else:
            ntot = ntot + 1.0
    preds.sort(prediction_compare)
    
    sys.stdout.flush()
    
    t=preds[0].p
    tp = fp = 0.0
    for p in preds:
        sen = tp / ptot
        spe = 1.0 - (fp / ntot)
        if spe < spe_min:
            t=p.p
            break
        if p.y > 0:
            tp = tp + 1.0
        else:
            fp = fp + 1.0
    
    print "Threshold: ", predfile, spe, sen, t
    sys.stdout.flush()
    return t

def get_pos_hits(hits, predfile, idx):
    mask=1
    mask<<=idx
    th = determine_threshold(predfile, 0.99)
    fin = file(predfile, 'ra')
    for line in fin:
        if is_not_prediction_line(line):
            continue
        id, p, y, t = parse_prediction_line(line)
        if is_not_pdb_id(id):
            continue
        pdb, chain, index, resnm = parse_id(id)
        if y > 0:
            if float(p) > th:
                if pdb in hits:
                    hits[pdb] |= mask
                else:
                    hits[pdb] = mask

def single_prediction():
    argv=sys.argv[1:]
    pred=argv[0]
    pdbs=list_pdb_in_predictions(pred, "dna/hit", "dna/miss", "non/hit", "non/miss")
    keys=pdbs.keys()
    for pdb in keys:
        path = pdbs[pdb]
        print pdb, path
        download_pdb_file(pdb, path)
        pdbfile=os.path.join(path, pdb+".pdb")
        pymol_color_pml(pdbfile, pred)

def mult_prediction():
    argv=sys.argv[1:]
    hits=dict()
    idx=0
    flag=0
    for arg in argv:
        mask=1
        mask<<=idx
        get_pos_hits(hits, arg, idx)
        idx = idx + 1
        flag |= mask
    path=""
    keys=hits.keys()
    for pdb in keys:
        if hits[pdb] == flag:
            download_pdb_file(pdb, path)
            pdbfile=os.path.join(path, pdb+".pdb")
            cmd = []
            pymol_load_pdb(cmd, pdbfile)
            pymol_color_by_predictions(cmd, pdbfile, argv)
            pymol_save_png(cmd, pdbfile)
            fout=file(pdbfile.split('.',1)[0]+".pml", "wa")
            fout.write("\n".join(cmd))
            fout.close()
        else:
            print pdb, hits[pdb]

if __name__ == "__main__":
    #single_prediction()
    mult_prediction()


