#!/usr/bin/env python2.4
# Module benchrst
#
from bench_plot import *

def plot_to_img(pred, pfx, pname):
    import matplotlib
    matplotlib.use('Cairo')
    import pylab
    costlines=4
    plots=pred.plots()
    for tmp in plots:
        if tmp[3] == pname:
            plot=tmp
            break;
    if (not pname == 'CST' and not pname == 'ENVX') or len(pred) == 1:
        plot2file(pylab, pfx, pred, plot, costlines)

if __name__ == "__main__":
    import os,sys
    
    argv=sys.argv[1:]
    pfx=argv[0]
    type=argv[1]
    if not os.path.exists(pfx):
        os.makedirs(pfx)
    pred=MalibuBench(argv[2:])
    plot_to_img(pred, pfx, type)