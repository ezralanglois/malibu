#!/usr/bin/env python2.4
# Module color_pdb
#
import subprocess
import urllib, os, sys, gzip

class PDBPrediction:
    def __init__(self, chain, index, id, p, y, t):
        self.chain = chain
        self.index = index
        self.id = id
        self.p = p
        self.y = y
        self.c = 0
        self.t = t

