@echo off
if "%PYTHON_PATH%" == "" SET PYTHON_PATH=C:\Python25
if "%OS%" == "Windows_NT" goto WinNT
%PYTHON_PATH%\python.exe %1 %2 %3 %4 %5 %6 %7 %8 %9
goto endofpyfile
:WinNT
%PYTHON_PATH%\python.exe %*
if NOT %errorlevel% == 0 (
	echo You do not have python in "%PYTHON_PATH%"
	SET PYTHON_PATH=
)
:endofpyfile

