@ECHO OFF

REM ~ Copyright 2006-2007 Robert Langlois.
REM ~ Distributed under the malibu Software License, Version 1.0.
REM ~ (See accompanying file LICENSE.txt)

REM ~ Testing Python Bindings

cd ../../src
bjam py-willow > ../script/win/out.txt
cd ..
C:\Python25\python py/test/test_learner.py willow -lib="bin\programs\learn\python\msvc-8.0\debug\threading-multi" cfg/test1.cfg

pause