@ECHO OFF

REM ~ Copyright 2006-2007 Robert Langlois.
REM ~ Distributed under the malibu Software License, Version 1.0.
REM ~ (See accompanying file LICENSE.txt)

REM ~ Testing Python Bindings

cd ../../src
bjam test-py-willow > ../script/win/out.txt