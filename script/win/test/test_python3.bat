@ECHO OFF

REM ~ Copyright 2006-2007 Robert Langlois.
REM ~ Distributed under the malibu Software License, Version 1.0.
REM ~ (See accompanying file LICENSE.txt)

REM ~ Testing Python Bindings

C:\Python25\python ../../py/test/test_learner.py
pause