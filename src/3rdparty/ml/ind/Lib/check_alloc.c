/***
 *** See the file "IND/disclaimers-and-notices.txt" for 
 *** information on usage and redistribution of this file, 
 *** and for a DISCLAIMER OF ALL WARRANTIES.
 ***/

/*IND-version2.0
 *   IND 2.0 released 9/15/92   
 *   NASA Ames Research Center, MS 269-2, Moffett Field, CA 94035
 */

#include <stdio.h>
#include <Lib.h>

#ifdef DEBUG_dealloc
#include <malloc.h>
#include <stdlib.h>
/*
 *	wont compile on some UNIX systems;
 *	but don't care since only needed for debugging
 */

display_alloc()
{/*
	struct  mallinfo  ma;

	ma = mallinfo();
	printf("total space in arena:        %d\n", ma.arena);	
	printf("number of ordinary blocks:   %d\n", ma.ordblks);	
	printf("number of small blocks:     %d\n", ma.smblks);	
	printf("number of holding blocks:     %d\n", ma.hblks);	
	printf("space in holding block headers:     %d\n", ma.hblkhd);	
	printf("space in small blocks in use:     %d\n", ma.usmblks);	
	printf("space in free small blocks:     %d\n", ma.fsmblks);	
	printf("space in ordinary blocks in use:     %d\n", ma.uordblks);	
	printf("space in free ordinary blocks:     %d\n", ma. fordblks);	
	printf("cost of enabling keep option:     %d\n", ma.keepcost);	
	printf("max size of small blocks:     %d\n", ma.mxfast);	
	printf("number of small blocks in a holding block:     %d\n", ma.nlblks);
	printf("small block rounding factor:     %d\n", ma.grain);	
	printf("space (including overhead) allocated in ord. blks:     %d\n", 
				ma.uordbytes);	
	printf("number of ordinary blocks allocated:     %d\n", ma.allocated);
	printf("bytes used in maintaining the free tree:     %d\n", 
			ma.treeoverhead);	*/
}

#endif
