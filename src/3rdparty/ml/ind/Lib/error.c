/***
 *** See the file "IND/disclaimers-and-notices.txt" for 
 *** information on usage and redistribution of this file, 
 *** and for a DISCLAIMER OF ALL WARRANTIES.
 ***/

/*IND-version2.0
 *   IND 2.0 released 9/15/92  
 *   NASA Ames Research Center, MS 269-2, Moffett Field, CA 94035
 */

/* 
 *   error_LF() -- print error message and die 
 *                 passing 0,0 for line file means these aren't
 *		   printed
 */

#include <stdio.h>
#include <errno.h>

void error_LF(s1, s2,line,file)
char	*s1, *s2;
int    line;
char    *file;
{
	/*extern	int	errno, sys_nerr;*/
	/*extern int sys_nerr;*/
	extern	char	*progname;
	void    myexit();

	fprintf(stderr, "\n");
	if (progname)
		fprintf(stderr, "%s: ", progname);
	if ( line && *file )
		fprintf(stderr, "line %d in file %s: ", line, file);
	fprintf(stderr, s1, s2);
#ifdef	__USE_BSD
	/*if (errno > 0 && errno < sys_nerr)*/
		/*fprintf(stderr, " (%s)", sys_errlist[errno]);*/fprintf(stderr, " (%s)", strerror(errno));
#endif
#ifdef	__USE_GNU
	/*if (errno > 0 && errno < sys_nerr)*/
		/*fprintf(stderr, " (%s)", _sys_errlist[errno]);*/ fprintf(stderr, " (%s)", strerror(errno));
#endif
	fprintf(stderr, "\n");
	myexit(1);
}
