/***
 *** See the file "IND/disclaimers-and-notices.txt" for 
 *** information on usage and redistribution of this file, 
 *** and for a DISCLAIMER OF ALL WARRANTIES.
 ***/

/*
 *      returns x st. Q(x)=p,
 *      i.e. x is a Z-score, Q(x) is error in normal distribution 
 *      26.2.22 in Abramowitz & Stegun
 */
#include <math.h>
float Ztoprob(p)
float p;
{
        float t = sqrt(-2.0 * log((double)p));
        return (float) t - (2.30753+0.27061*t)/(1.0+0.99229*t+0.04481*t*t);
}
