/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AbstractArgument.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ABSTRACTARGUMENT_H
#define _EXEGETE_ABSTRACTARGUMENT_H
#include "debugutil.h"
#include <string>
#include <vector>

/** @file AbstractArgument.h
 * @brief Defines an abstract argument.
 * 
 * This file contains an abstract class providing a general interface
 * to a mixed set of class types. This allows every argument to be stored
 * in a single collection.
 *
 * @ingroup ExegeteArgs
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteArgs Argument System
 *  This group holds all the argument related classes and function files.
 */

namespace exegete
{
	/** @brief Abstract interface to an argument for an argument map.
	 * 
	 * This class provides an interface to individual arguments held in the ArgumentMap class. All arguments have the following 
	 *  features:
	 *		- name of the argument (optional for master arguments)
	 *		- description of argument
	 *		- pointer to some object/value
	 *		- a mapping between external and internal values (optional)
	 * 
	 * There are three types of arguments:
	 * 	-# Flagged arguments
	 * 		- E.g. -param_name param_value
	 * 	-# Master arguments
	 * 		- E.g. param_value
	 * 	-# Header (pseudo) argumens
	 * 		- describe groups of arguments in a config file
	 * 
	 * Arguments can hold several value types:
	 * 	-# General: 
	 * 		- can hold values of any type
	 * 		- supports type checking
	 * 	-# Options:
	 * 		- based on general the argument
	 * 		- internal values can be mapped to external names
	 * 		- supports both restricted and unrestricted options
	 * 	-# Boolean
	 * 		- based on option the argument
	 * 		- provides a number of options translating 1 or 0 to Yes or NO
	 */
	class AbstractArgument : public std::vector< std::pair<std::string,std::string> >
	{
	public:
		/** Defines internal enumerations for special arguments:
		 * 		-# NONE - nothing special
		 * 		-# HEAD - a header (not a real argument)
		 * 		-# MSTR - flags a master argument (not require or have a flag)
		 * 		-# OPTN - flags an option argument maps external to internal values
		 * 		-# BOOL - flags a boolearn argument (a special case of option above)
		 */
		enum option_type
		{
			NONE='0',
			HEAD='H',
			MSTR='M',/** Master ONLY **/
			OPTN='>',
			BOOL='?'//position?
		};
		/** Defines an enumerated option type as an option type.  **/
		typedef enum option_type						option_type;
		/** Defines a pair of strings as an option pair. **/
		typedef std::pair<std::string,std::string>		option_pair;
		/** Defines a vector or option_pairs as an option vector. **/
		typedef std::vector< option_pair >				option_vector;
		/** Defines an option vector iterator as an iterator. **/
		typedef option_vector::iterator					iterator;
		/** Defines an option_vector const_iterator as an constant iterator. **/
		typedef option_vector::const_iterator			const_iterator;
	public:
		/** Constructs a header argument.
		 *
		 * @param ds description of header.
		 * @param l a level grouping arguments into views.
		 */
		AbstractArgument(std::string ds, int l=0) : descStr(ds), typeInt(HEAD), multMaster(false), levelInt(l)
		{
		}
		/** Constructs all other argument types including:
		 *	-# Boolean
		 *	-# Option Strict
		 *	-# Option Unstrict
		 *	-# Other
		 *
		 * @param nm name of argument.
		 * @param ds description for argument.
		 * @param m bool indicating if argument is hybrid master.
		 * @param l a level grouping arguments into views.
		 */
		AbstractArgument(std::string nm, std::string ds, bool m, int l) : nameStr(nm), descStr(ds), typeInt(NONE), multMaster(m), levelInt(l)
		{
			static const char* FLG=";:";
			std::string::size_type n;
			if( (n=ds.find(BOOL)) != std::string::npos )
			{
				typeInt = BOOL;
				ds = "NO:0;YES:1;Y:1;N:0;yes:1;no:0;y:1;n:0;0:0;1:1";
			}
			else if( (n=ds.find(OPTN)) != std::string::npos )
			{
				typeInt = OPTN;
				ds = ds.substr(n+1);
			}
			if( n != std::string::npos )
			{
				descStr = descStr.substr(0,n);
				bool flag = stringToValue(ds, vec(), FLG);
				ASSERT(flag);
				if(!flag) std::cerr << "WARNING: Cannot convert to string" << std::endl;
			}
		}
		/** Destructs an abstract argument.
		 */
		virtual ~AbstractArgument()
		{
		}

	public:
		/** Compares the argument map the specified string.
		 *
		 * @param str string to compare.
		 * @return true if specified string matches argument name.
		 */
		bool operator==(std::string str)
		{
			return nameStr == str;
		}

	public:
		/** Converts the specified string and sets the value of the 
		 *  object pointed to by the argument.
		 * 
		 * @warning This is only an abstract interface, if called directly it returns an error.
		 *
		 * @param str string with new value.
		 * @return an error message or NULL.
		 */
		virtual const char* set(const char* str)
		{ 
			ASSERT(false);
			return ERRORMSG("Not work for header arguments");
		}
		/** Converts the value of the object pointed to by the argument to 
		 *  a string and stores it in the destination string argument.
		 * 
		 * @warning This is only an abstract interface, if called directly it does nothing.
		 *
		 * @param str reference to string recieving value.
		 */
		virtual void get(std::string& str)const
		{
			ASSERT(false);
		}

	public:
		/** Gets the name of the argument.
		 *
		 * @return name of argument.
		 */
		const std::string& name()const
		{
			return nameStr;
		}
		/** Gets the description of the argument.
		 *
		 * @return description of argument.
		 */
		const std::string& desc()const
		{
			return descStr;
		}
		/** Tests if the name has been set.
		 *
		 * @return true if argument is not master or header.
		 */
		bool hasflag()const
		{
			return !nameStr.empty();
		}
		/** Gets the enumerated type of the argument.
		 *
		 * @return an enumerated type (option_type).
		 */
		option_type type()const
		{
			return typeInt;
		}
		/** Sets an option type for the argument.
		 *
		 * @param t an enumerated option_type.
		 */
		void type(option_type t)
		{
			typeInt = t;
		}
		/** Gets the currently selected value.
		 *
		 * @note Options in the description may have the form "Name:Value" where the name is a descriptive label and value is the internal value.
		 *
		 * @param n index of option.
		 * @return empty if n exceeds the number of options, otherwise the string value of the option.
		 */
		virtual const std::string selectedOption(unsigned int n)const
		{
			std::string val;
			get(val);
			return val;
		}
		/** Gets a string label for an option type argument.
		 *
		 * @note Options in the description may have the form "Name:Value" where the name is a descriptive label and value is the internal value.
		 *
		 * @param n index of option.
		 * @return empty of n exceeds number of options, otherwise the string label of the option.
		 */
		virtual const std::string optionAt(unsigned int n)const
		{
			if( option_vector::size() == 0 )
			{
				std::string val;
				get(val);
				return val;
			}
			if( n >= option_vector::size() ) return "";
			return option_vector::operator[](n).first;
		}
		/** Tests if an option at the specified index is selected.
		 *
		 * @param n index of the option.
		 * @return true if option is selected.
		 */
		bool isSelected(unsigned int n)const
		{
			if( option_vector::empty() ) return true;
			std::string val;
			get(val);
			return n == indexof(option_vector::begin(), option_vector::end(), val);
		}
		/** Tests if this argument accepts multiple command line argument 
		 * values.
		 *
		 * @return true if argument accepts multiple values.
		 */
		bool ismultiple()const
		{
			return multMaster;
		}
		/** Get the output level of the argument.
		 * 
		 * @return output level of the argument.
		 */
		int level()const
		{
			return levelInt;
		}

	protected:
		/** Searches a range of options for a particular label.
		 *
		 * @param beg a constant option iterator to the start of a range.
		 * @param end a constant option iterator to the end of a range.
		 * @param val a string label to search.
		 * @return a constant option iterator to position of value found or end of range.
		 */
		static const_iterator find(const_iterator beg, const_iterator end, const std::string& val)
		{
			for(;beg != end;++beg) if( val == beg->first ) break;
			return beg;
		}
		/** Searches a range of options for a particular value.
		 *
		 * @param beg a constant option iterator to start of range.
		 * @param end a constant option iterator to end of range.
		 * @param str a string value to search.
		 * @return a constant option iterator to position of value found or end of range.
		 */
		static const_iterator findvalue(const_iterator beg, const_iterator end, const std::string& str)
		{
			for(;beg != end;++beg) if( str == beg->second ) break;
			return beg;
		}
		/** Searches a range of options for a particular label and returns its index.
		 *
		 * @param beg a constant option iterator to start of range.
		 * @param end a constant option iterator to end of range.
		 * @param val a string label to search.
		 * @return an index of label found or size of the range.
		 */
		static unsigned int indexof(const_iterator beg, const_iterator end, const std::string& val)
		{
			return (unsigned int)std::distance(beg, find(beg,end,val));
		}
		/** Searches a range of options for a particular value and returns its index.
		 *
		 * @param beg a constant option iterator to start of range.
		 * @param end a constant option iterator to end of range.
		 * @param val a string value to search.
		 * @return an index of value found or size of the range.
		 */
		static unsigned int indexofValue(const_iterator beg, const_iterator end, const std::string& val)
		{
			return (unsigned int)std::distance(beg, findvalue(beg,end,val));
		}
		/** Gets a reference to the parent class.
		 *
		 * @return a reference to the parent class.
		 */
		option_vector& vec()
		{
			return *this;
		}

	private:
		std::string nameStr;
		std::string descStr;
		option_type typeInt;
		bool multMaster;
		int levelInt;
	};

};


#endif


