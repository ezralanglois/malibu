/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Argument.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ARGUMENT_H
#define _EXEGETE_ARGUMENT_H
#include "ArgumentType.h"
#include "ArgumentList.h"

/** @file Argument.h
 * @brief Defines an argument object.
 * 
 * This file contains a class to interface special argument values types 
 * such as ArgumentList.
 *
 * @ingroup ExegeteArgs
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Interface to an argument with a specific value.
	 * 
	 * This class template provides an interface for special argument types.
	 */
	template<class T>
	class Argument : public ArgumentType<T>
	{
		typedef ArgumentType<T> parent_type;
	public:
		/** Defines the first template parameter as a value type. **/
		typedef T										value_type;
		/** Defines a pointer to the first template parameter as a pointer. **/
		typedef T*										pointer;
		/** Defines an argument type iterator as an iterator. **/
		typedef typename parent_type::iterator			iterator;
		/** Defines an argument type constant iterator as a constant iterator. **/
		typedef typename parent_type::const_iterator	const_iterator;
		
	public:
		/** Constructs a header argument with a description.
		 *
		 * @param ds a std::string description.
		 */
		Argument(std::string ds) : parent_type(ds)
		{
		}
		/** Constructs a master argument with a description.
		 *
		 * @param val a reference to the value type.
		 * @param ds a std::string description.
		 */
		Argument(T& val, std::string ds) : parent_type(val, ds)
		{
		}
		/** Constructs an argument with a description.
		 *
		 * @param val a reference to the value type.
		 * @param nm a string name.
		 * @param ds a string description.
		 * @param m is argument added as hybrid master.
		 * @param l a level grouping arguments into views.
		 */
		Argument(T& val, std::string nm, std::string ds, bool m, int l) : parent_type(val, nm, ds, m, l)
		{
		}
		/** Destructs an argument. 
		 */
		~Argument()
		{
		}
	};

	
	/** @brief Interface to an argument with a ArgumentList as a value.
	 * 
	 * This class template provides an interface for special argument types.
	 */
	template<>
	class Argument<ArgumentList> : public ArgumentType<ArgumentList>
	{
		typedef ArgumentType<ArgumentList>		parent_type;
	public:
		/** Defines the ArgumentList as a value type. **/
		typedef ArgumentList					value_type;
		/** Defines a pointer to the ArgumentList as a pointer. **/
		typedef ArgumentList*					pointer;
		/** Defines an argument type iterator as an iterator. **/
		typedef parent_type::iterator			iterator;
		/** Defines an argument type constant iterator as a constant iterator. **/
		typedef parent_type::const_iterator		const_iterator;
	public:
		/** Constructs a argument with a description.
		 *
		 * @param val a reference to an ArgumentList.
		 * @param nm a std::string name.
		 * @param ds a std::string description.
		 * @param m is argument added as hybrid master.
		 * @param l a level grouping arguments into views.
		 */
		Argument(ArgumentList& val, std::string nm, std::string ds, bool m, int l) : parent_type(val, nm, ds, m, l)
		{
		}
		/** Destructs an argument. 
		 */
		~Argument()
		{
		}

	public:
		/** Gets a list of sub-argument names.
		 *
		 * @param n index of sub-argument.
		 * @return empty of n exceeds number of sub-argument, otherwise the string label of the sub-argument.
		 */
		const std::string selectedOption(unsigned int n)const
		{
			return parent_type::getpointer()->stringAt(n);
		}
		/** Gets a list sub-argument values.
		 *
		 * @param n index of sub-argument.
		 * @return empty of n exceeds number of sub-argument, otherwise the string value of the sub-argument.
		 */
		const std::string optionAt(unsigned int n)const
		{
			return parent_type::getpointer()->stringAt(n);
		}
	};

};


#endif


