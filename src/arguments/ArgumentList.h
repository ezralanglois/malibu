/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ArgumentList.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ARGUMENTLIST_H
#define _EXEGETE_ARGUMENTLIST_H
#include "ArgumentUtil.h"
#include "ArgumentType.h"
#include "debugutil.h"
#include <string>
#include <vector>

/** @file ArgumentList.h
 * @brief Defines an argument list.
 * 
 * This file contains an ArgumentList as well as several specialized utility
 * classes:
 * 	- ArgumentUtil: serve as an argument map.
 * 	- TypeUtil: do not test conversion.
 * 	- String2Value: do not use stream conversion.
 *
 * @ingroup ExegeteArgs
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Holder for sub-arguments.
	 * 
	 * This class provides an interface to handle multiple sub-arguments in 
	 * the argument value and this list is repeatable.
	 */
	class ArgumentList
	{
		typedef std::pair< std::string, std::string > 	string_pair;
		typedef std::vector< string_pair >				string_vector;
		typedef std::vector< string_vector >			string_vector2d;
		typedef string_vector2d::iterator				string_iterator;
		typedef std::vector< AbstractArgument* > 		argument_vector;
		typedef argument_vector::iterator		 		iterator;
		typedef argument_vector::const_iterator	 		const_iterator;
	public:
		/** Constructs an argument list.
		 */
		ArgumentList()
		{
		}
		/** Destructs an argument list.
		 */
		~ArgumentList()
		{
			for(iterator it = argVec.begin();it != argVec.end();++it) delete (*it);
		}

	public:
		/** Assigns a string to the argument list.
		 *
		 * @param str a string representation of the argument list.
		 * @return a reference to an argument list.
		 */
		bool add(const std::string& str)
		{
			if( !str.empty() )
			{
				strVec.resize(strVec.size()+1);
				if( !stringToValue(str, strVec.back(), ";:")) return false;
				return parse(strVec.back()) == 0;
			}
			return true;
		}
		/** Adds an argument that references the specified variable.
		 *
		 * @param val variable referenced by argument.
		 * @param nm the string name of the argument.
		 * @param ds the string description of the argument.
		 */
		template<class T>
		void operator()(T& val, std::string nm, std::string ds)
		{
			argVec.push_back( new ArgumentType<T>(val, nm, ds, false, 0) );
		}
		/** Gets a string representation for a single value in the list
		 * of arguments.
		 * 
		 * @param n index of value.
		 * @return a string representation of the argument list.
		 */
		const std::string stringAt(unsigned int n)const
		{
			//static std::string last;
			//if( n < argVec.size() )
			//{
			//	std::string line;
			//	valueToString(strVec[n], line, ";:");
			//	return line;
			//}
			//return last;
			std::string line;
			std::string value;
			for(argument_vector::const_iterator beg = argVec.begin(), end = argVec.end();beg != end;++beg)
			{
				(*beg)->get(value);
				line += (*beg)->name() + ":" + value;
				if( (beg+1)!=end) line += ";";
			}
			return line;
		}

	public:
		/** Resets the argument list to first instance.
		 */
		void reset()
		{
			current = strVec.begin();
			if( current != strVec.end() )
			{
				const char* msg = parse(*current);
				ASSERTMSG(msg == 0, msg);
				if( msg != 0 ) std::cerr << "Warning: " << msg << std::endl;
			}
		}
		/** Test whether more instances of the argument exist.
		 *
		 * @return true if more instances exist.
		 */
		bool hasNext()const
		{
			return current != strVec.end();
		}
		/** Moves to next instance of argument.
		 */
		void next()
		{
			++current;
			if( current != strVec.end() )
			{
				const char* msg = parse(*current);
				ASSERT(msg == 0);
			}
		}

	private:
		const char* parse(const string_vector& vec)
		{
			const char* msg;
			for(unsigned int i=0;i<vec.size();++i)
			{
				iterator it = argument(vec[i].first);
				if( it != argVec.end() ) msg=(*it)->set( vec[i].second.c_str() );
				if( msg != 0 ) return msg;
				if( msg != 0 ) std::cerr << "Warning: " << msg << std::endl;
			}
			return 0;
		}
		iterator argument(const std::string& str)
		{
			iterator beg = argVec.begin();
			iterator end = argVec.end();
			for(;beg != end;++beg) if( (*beg)->name() == str ) return beg;
			return end;
		}
		/** Prints the sub-argument name/value pairs.
		 *
		 * @param out an output stream.
		 * @param format an argument list.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const ArgumentList& format)
		{
			std::string value;
			for(argument_vector::const_iterator beg = format.argVec.begin(), end = format.argVec.end();beg != end;++beg)
			{
				(*beg)->get(value);
				out << (*beg)->name() << ":" << value;
				if( (beg+1)!=end) out << ";";
			}
			return out;
		}


	private:
		argument_vector argVec;
		string_vector2d strVec;
		string_iterator current;
	};

	
	/** @brief Interface adds arguments to an ArgumentList.
	 * 
	 * Specializes an argument utility to an argument list.
	 */
	template<>
	class ArgumentUtil<ArgumentList>
	{
	public:
		/** Initialize argument in list.
		 * 
		 * @param map an argument map.
		 * @param val an argument object.
		 * @param nm a name.
		 * @param ds a description.
		 * @param pf a prefix.
		 * @param l a level grouping arguments into views.
		 */
		template<class U>
		static void set(ArgumentList& map, U& val, std::string nm, std::string ds, std::string pf, int l)
		{
			map(val, nm, ds);
		}
		/** Initialize argument in list.
		 * 
		 * @warning Does nothing.
		 * 
		 * @param map an argument list.
		 * @param nm a header.
		 * @param l a level grouping arguments into views.
		 */
		static void set(ArgumentList& map, std::string nm, int l)
		{
		}
	};

};

/** @brief Type utility interface to an ArgumentList.
 * 
 * This class template specialization defines type utility methods
 * for an ArgumentList.
 */
template<>
struct TypeUtil< ::exegete::ArgumentList >
{
	/** Flag as non-primative type.
	 */
	enum{ ispod=false };
	/** Defines an ArgumentList as a value type. **/
	typedef ::exegete::ArgumentList value_type;
	/** Tests if a string can be converted into an ArgumentList.
	 * 
	 * @warning Only return true.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return name of the type
	*/
	static const char* name() 
	{
		return "ArgumentList";
	}
};

/** @brief Converts a string to an ArgumentList.
 * 
 * This class template specialization defines a conversion from an 
 * ArgumentList to std::string.
 */
template<>
struct String2Value< ::exegete::ArgumentList >
{
	/** Defines a TypeUtil of a ArgumentList as type traits. **/
	typedef TypeUtil< ::exegete::ArgumentList > type_traits;
	/** Converts from a c-string to a an ArgumentList.
	 *
	 * @param str the source c-string.
	 * @param val the destination ArgumentList.
	 * @param m a unused list of character separators
	 * @param c clear the object.
	 * @return true if conversion is successful.
	 */
	static bool convert(const char* str, ::exegete::ArgumentList& val, const char* m, bool c)
	{
		return val.add(str);
	}
};

#endif


