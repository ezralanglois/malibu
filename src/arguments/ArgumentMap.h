/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ArgumentMap.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ARGUMENTMAP_H
#define _EXEGETE_ARGUMENTMAP_H
#include "errorutil.h"
#include "fileioutil.h"
#include "Argument.h"
#include "Template.h"
#include "ArgumentTemplate.h"
#include "ArgumentUtil.h"
#include <iostream>
#include <algorithm>
#include <map>

/** @file ArgumentMap.h
 * @brief Defines an argument map.
 * 
 * This file contains the ArgumentMap class and a specialized helper ArgumentUtil. The
 * argument map forms a single interface to every adjustable parameter in a program.
 * 
 * It supports both parsing simple config files and command-line arguments. The config file can be read
 * as a file stream or from the standard input. It also supports writing out argument config files
 * in a variety of formats as specified by a template.
 * 
 * @todo long argument help
 * @todo single argument change boolean
 * @todo non-printable arguments (standard argument maps)
 *
 * @ingroup ExegeteArgs
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section maparglist Argument Map
 * 	- mapformat
 * 		- Description: type of map output format
 * 		- Type: option(Console:0;Compact:1;Framework:2)
 * 		- Viewability: Developer
 * 	- maplevel
 * 		- Description: type of map output format
 * 		- Type: option(None:0;Standard:1;Additional:2;Advanced:3;Developer:4)
 * 		- Viewability: Additional
 * 	- implicit
 * 		- Description: path for implicit config files as well as other files
 * 		- Type: string file path
 * 		- Viewability: Developer
 */


namespace exegete
{
	/** @brief Argument system driver for holding, parsing and writing.
	 * 
	 * The argument map comprises an interface for the argument system. Each argument points to
	 * some value in the greater system through a pointer to an actual value.
	 *  
	 * Features
	 *   - Parses config files.
	 *   - Parses commandline.
	 *   - Parses standard input when a file is redirected.
	 *   - Parses the environment variables.
	 *   - Writes arguments according to a template file for flexible formatting.
	 *
	 * @todo add option, single char e.g -vtk
	 *
	 * @see Argument
	 */
	class ArgumentMap
	{
		typedef std::vector< AbstractArgument* >					argument_vector;
		typedef argument_vector::iterator							iterator;
		typedef std::vector< std::pair<std::string, unsigned int> > group_vector;
	public:
		/** Defines an argument vector constant iterator as a constant iterator. */
		typedef argument_vector::const_iterator						const_iterator;
		/** Defines an argument vector iterator as a iterator. */
		typedef group_vector::const_iterator						const_group_iterator;
		/** Defines a vector to save arguments. **/
		typedef std::vector< std::pair<std::string,std::string> > 	argsave_vector;
		
	public:
		/** Defines five view levels for groups of arguments. **/
		enum{
			NONE=0,
			STANDARD=1,
			ADDITIONAL,
			ADVANCED,
			DEVELOPER
		} level_type;
	public:
		/** Constructs an argument map with default file parsing parameters and
		 *  a default template format for writing arguments.
		 */
		ArgumentMap() : tokenCh(':'), newlineCh('\n'), commentCh('#'), outputTypeInt(1), levelInt(1)
		{
			consoledef += "<!-- map !-->";
			consoledef += "<!-- group !-->";
			consoledef += "<!-- groupheader !-->#<!-- description //!-->\n<!-- groupheader !-->";
			consoledef += "<!-- header !-->\n#<!-- description //!-->\n<!-- header !-->";
			consoledef += "<!-- types !-->";
			consoledef += "<!-- string !--><!-- name //!-->:\t<!-- value //!-->\t#<!-- description //!-->\n<!-- string !-->";
			consoledef += "<!-- choose !--><!-- name //!-->:\t<!-- valsel //!-->\t#<!-- description //!-->\n";
			consoledef += "<!-- options !-->";
			consoledef += "<!-- selected !-->#(<!-- value //!-->)\n<!-- selected !-->";
			consoledef += "<!-- unselected !-->#<!-- value //!-->\n<!-- unselected !-->";
			consoledef += "<!-- options !-->";
			consoledef += "\n<!-- choose !-->";
			consoledef += "<!-- checks !-->";
			consoledef += "<!-- check !--><!-- name //!-->:\t<!-- valsel //!-->\t#<!-- description //!-->\n<!-- check !-->";
			consoledef += "<!-- uncheck !--><!-- name //!-->:\t<!-- valsel //!-->\t#<!-- description //!-->\n<!-- uncheck !-->";
			consoledef += "<!-- checks !-->";
			consoledef += "<!-- types !-->";
			consoledef += "<!-- group !-->";//\n
			consoledef += "<!-- map !-->";
			
			compactdef += "<!-- map !-->";
			compactdef += "<!-- group !-->";
			compactdef += "<!-- groupheader !-->#<!-- description //!-->\n<!-- groupheader !-->";
			compactdef += "<!-- header !-->\n#<!-- description //!-->\n<!-- header !-->";
			compactdef += "<!-- types !-->";
			compactdef += "<!-- string !--><!-- name //!-->:\t<!-- value //!-->\t#<!-- description //!-->\n<!-- string !-->";
			compactdef += "<!-- choose !--><!-- name //!-->:\t<!-- valsel //!-->\t#<!-- description //!-->\n";
			compactdef += "#<!-- options !-->";
			compactdef += "<!-- selected !-->(<!-- value //!-->)<!-- optionlbl , //!--><!-- selected !-->";
			compactdef += "<!-- unselected !--><!-- value //!--><!-- optionlbl , //!--><!-- unselected !-->";
			compactdef += "<!-- options !-->";
			compactdef += "\n<!-- choose !-->";
			compactdef += "<!-- checks !-->";
			compactdef += "<!-- check !--><!-- name //!-->:\t<!-- valsel //!-->\t#<!-- description //!-->\n<!-- check !-->";
			compactdef += "<!-- uncheck !--><!-- name //!-->:\t<!-- valsel //!-->\t#<!-- description //!-->\n<!-- uncheck !-->";
			compactdef += "<!-- checks !-->";
			compactdef += "<!-- types !-->";
			compactdef += "<!-- group !-->";//\n
			compactdef += "<!-- map !-->";

			frameworkdef += "<!-- map !-->";
			frameworkdef += "<!-- group !-->";
			frameworkdef += "<!-- groupheader !-->#<!-- description //!-->\n<!-- groupheader !-->";
			frameworkdef += "<!-- header !-->@@<!-- description //!-->\n<!-- header !-->";
			frameworkdef += "<!-- types !-->";
			frameworkdef += "<!-- string !-->N <!-- name //!-->\nV <!-- value //!-->\nD <!-- description //!-->\nT string\n\n<!-- string !-->";
			frameworkdef += "<!-- choose !-->N <!-- name //!-->\nV <!-- valsel //!-->\nD <!-- description //!-->\nO ";
			frameworkdef += "<!-- options !-->";
			frameworkdef += "<!-- selected !--><!-- value //!-->,<!-- selected !-->";
			frameworkdef += "<!-- unselected !--><!-- value //!-->,<!-- unselected !-->";
			frameworkdef += "<!-- options !-->";
			frameworkdef += "\nT choose\n\n<!-- choose !-->";
			frameworkdef += "<!-- checks !-->";
			frameworkdef += "<!-- check !-->N <!-- name //!-->\nV <!-- valsel //!-->\nD <!-- description //!-->\nT check\n\n<!-- check !-->";
			frameworkdef += "<!-- uncheck !-->N <!-- name //!-->\nV <!-- valsel //!-->\nD <!-- description //!-->\nT check\n\n<!-- uncheck !-->";
			frameworkdef += "<!-- checks !-->";
			frameworkdef += "<!-- types !-->";
			frameworkdef += "<!-- group !-->";
			frameworkdef += "<!-- map !-->";
		}
		/** Destructs an argumentmap deleting all memory. 
		 */
		~ArgumentMap()
		{
			for(iterator it = argVec.begin();it != argVec.end();++it) delete (*it);
		}

	public:
		/** Appends arguments to the argument map.
		 * 
		 * @param map an argument map to update.
		 */
		void init(ArgumentMap& map)
		{
			map("Argument", ArgumentMap::ADDITIONAL);
			map(outputTypeInt, 	"mapformat", 	"type of map output format>Console:0;Compact:1;Framework:2", ArgumentMap::DEVELOPER);
			map(levelInt, 		"maplevel", 	"type of map output format>None:0;Standard:1;Additional:2;Advanced:3;Developer:4", ArgumentMap::ADDITIONAL);
			map(implicitpth, 	"implicit", 	"path for implicit config files", ArgumentMap::DEVELOPER);
		}
		/** Adds a header to an argument group.
		 *
		 * @param ds the string description of argument group.
		 * @param l a level grouping arguments into views.
		 */
		void operator()(std::string ds, int l=NONE)
		{
			grpVec.push_back( std::make_pair(ds, flgVec.size()) );
			argVec.push_back( new AbstractArgument(ds, l) );
			ASSERT(!isnull(argVec.begin(), argVec.end()));
		}
		/** Adds an argument that references the specified variable.
		 *
		 * @param val variable referenced by argument.
		 * @param nm the string name of the argument.
		 * @param ds the string description of the argument.
		 * @param l a level grouping arguments into views.
		 * @param ms add the argument as a master argument.
		 */
		template<class T>
		void operator()(T& val, std::string nm, std::string ds, int l=NONE, bool ms=false)
		{
			ASSERTMSG(argument(nm) == flgVec.end(), nm << " " << ds << " == " << (*argument(nm))->desc() << " & " << ds );
			argVec.push_back( new Argument<T>(val, nm, ds, ms, l) );
			flgVec.push_back( argVec.back() );
			if( ms ) mstVec.push_back( argVec.back() );
			ASSERT(!isnull(argVec.begin(), argVec.end()));
		}
		/** Adds a prefixed argument that references the specified variable.
		 *
		 * @param val variable referenced by argument.
		 * @param prefix the prefix of the argument name.
		 * @param nm the string name of the argument.
		 * @param ds the string description of the argument.
		 * @param l a level grouping arguments into views.
		 */
		template<class T>
		void operator()(T& val, std::string prefix, std::string nm, std::string ds, int l=NONE)
		{
			if( prefix != "" ) nm = prefix + "_" + nm;
			ASSERTMSG(argument(nm) == flgVec.end(), nm << " " << ds << " == " << (*argument(nm))->desc() );
			argVec.push_back( new Argument<T>(val, nm, ds, false, l) );
			flgVec.push_back( argVec.back() );
			ASSERT(!isnull(argVec.begin(), argVec.end()));
		}

	public:
		/** Parses a new write format template.
		 *
		 * @param file a string filename.
		 * @return an error message or NULL.
		 */
		const char* parseTemplate(const std::string& file)
		{
			std::string buffer;
			const char* msg = readfile2buffer(file, buffer);
			if( msg == 0 ) consoledef = buffer;
			return msg;
		}
		/** Parses a redirected configuration file from the standard input (stdin).
		 *
		 * @return an error message or NULL.
		 */
		const char* parse()
		{
			if( isstdin() ) return parse(std::cin);
			return 0;
		}
		/** Parses a set of implicit configuration files.
		 *
		 * @param files a vector of string filenames.
		 * @param pth an implicit configuration file.
		 * @param ext an extension for a configuration file.
		 * @param root path for a root configuration file.
		 * @return an error message or NULL.
		 */
		const char* parse_implicit(const std::vector<std::string>& files, std::string pth, std::string ext, std::string root="")
		{
			const char* msg;
			std::string file;
			if( pth != "" && implicitpth != "" ) pth = join_file(implicitpth.c_str(), pth.c_str());
			else if( pth == "") pth = implicitpth;
			for(std::vector<std::string>::const_iterator beg=files.begin(), end=files.end();beg != end;++beg)
			{
				file=base_name(beg->c_str());
				if( ext != "" && ext != ext_name(file.c_str()) )
					file = replace_ext(file.c_str(), ext.c_str());
				if( pth != "" ) 
					file = join_file(pth.c_str(), file.c_str());
				if( (msg=parse(file, false)) != 0 ) return msg;
			}
			if( root != "" )
			{
				file = join_file(home_path(), root.c_str());
				if( (msg=parse(file, false)) != 0 ) return msg;
			}
			return 0;
		}
		/** Parses a set of implicit configuration files.
		 *
		 * @param files a vector of string filenames.
		 * @param pth an implicit configuration file.
		 * @param ext an extension for a configuration file.
		 * @return an error message or NULL.
		 */
		const char* parse(const std::vector<std::string>& files, std::string pth="", std::string ext="")
		{
			const char* msg;
			std::string file;
			if( pth != "" && implicitpth != "" ) pth = join_file(implicitpth.c_str(), pth.c_str());
			else if( pth == "") pth = implicitpth;
			for(std::vector<std::string>::const_iterator beg=files.begin(), end=files.end();beg != end;++beg)
			{
				file = *beg;
				if( pth != "" && file == dir_name(file.c_str()) )
				{
					file = join_file(pth.c_str(), file.c_str());
				}
				if( ext != "" && *ext_name(file.c_str()) == '\0' )
				{
					file = replace_ext(file.c_str(), ext.c_str());
				}
				if( testfile(file) == 0 )
				{
					if( (msg=parse(file)) != 0 ) return msg;
				}
				else
				{
					if( (msg=parse(*beg)) != 0 ) return msg;
				}
			}
			return 0;
		}
		/** Parses a configuration file.
		 *
		 * @param file a :string filename.
		 * @param force fire an error message if cannot open the file.
		 * @return an error message or NULL.
		 */
		const char* parse(const std::string& file, bool force=true)
		{
			const char* msg;
			std::ifstream fin;
			if( (msg=openfile(fin, file)) != 0 ) return force ? msg : 0;
			if( (msg=parse(fin)) != 0 )	return msg;
			fin.close();
			return 0;
		}
		/** Parses a config configuration from a stream.
		 *
		 * @param in an input stream.
		 * @return an error message or NULL.
		 */
		const char* parse(std::istream& in)
		{
			std::string nam, val;
			const char* msg=0;
			while( !in.eof() )//unescape
			{
				std::getline(in, nam, newlineCh);
				removeComment(nam, commentCh);
				split(nam,val,tokenCh);
				if( nam.empty() ) continue;
				if( !val.empty() )
				{
					iterator it = argument(nam.c_str());
					if( it != flgVec.end() && (msg=(*it)->set( val.c_str() )) != 0 ) return msg;
				}
			}
			return 0;
		}
		/** Parses a set of command line arguments.
		 *
		 * @param argc number of arguments on command line.
		 * @param argv an array of strings from the command line.
		 * @param masteronly if true only parse master arguments.
		 * @return an error message or NULL.
		 */
		const char* parse(int argc, char **argv, bool masteronly=false)
		{
			const char* msg;
			unsigned int m=0;
			for(int i=1;i<argc;++i)
			{
				if( argv[i][0] != '-' )
				{
					if( m < mstVec.size() && (msg=mstVec[m]->set(argv[i])) != 0 ) return msg;
					if( m >= mstVec.size() ) return ERRORMSG("No argument for: " << argv[i] << " only allowed " << mstVec.size() << " master arguments");
					if( !mstVec[m]->ismultiple() ) ++m;
				}
				else if( (i+1) < argc && !masteronly )
				{
					iterator it = argument(argv[i]+1);// flgVec.end();
					if( it != flgVec.end() && (msg=(*it)->set(argv[i+1])) != 0 ) return msg;
					if( it == flgVec.end() ) return ERRORMSG("Cannot find argument for: " << argv[i]);
					++i;
				}
				else if( !masteronly ) return ERRORMSG("Missing value for flag: " << argv[i]);
				else ++i;
			}
			return 0;
		}
		/** Parses environment variables.
		 *
		 * @return an error message or NULL.
		 */
		const char* parse_env()
		{
			const char* msg;
			const char* value=0;
			for(iterator beg=flgVec.begin(), end=flgVec.end();beg != end;++beg)
				if( ((value=std::getenv((*beg)->name().c_str())) != 0) ) 
					if( (msg=(*beg)->set(value)) != 0 ) return msg;
			return 0;
		}
		/** Writes an argument map with the given template to
		 * an output stream.
		 *
		 * @param out an output stream.
		 * @return an error message or NULL.
		 */
		const char* write(std::ostream& out)const
		{
			if( levelInt == 0 ) return 0;
			const char* msg;
			ArgumentTemplate summary(argVec, levelInt);
			if( outputTypeInt == 0 )
			{
				if( (msg=summary.build(&(*consoledef.begin()))) != 0 ) return msg;
			}
			else if( outputTypeInt == 1 )
			{
				if( (msg=summary.build(&(*compactdef.begin()))) != 0 ) return msg;
			}
			else if( outputTypeInt == 2 )
			{
				if( (msg=summary.build(&(*frameworkdef.begin()))) != 0 ) return msg;
			}
			out << summary;
			return 0;
		}
		/** Load arguments into argument map from a vector of arguments.
		 * 
		 * @param args a vector of arguments.
		 * @return an error message or NULL.
		 */
		const char* load(const argsave_vector& args)
		{
			typedef argsave_vector::const_iterator const_load_iterator;
			const char* msg;
			for(const_load_iterator beg=args.begin();beg != args.end();++beg)
			{
				iterator it = argument(beg->first);// flgVec.end();
				if( it != flgVec.end() && (msg=(*it)->set(beg->second.c_str())) != 0 ) return msg;
			}
			return 0;
		}
		/** Save arguments from argument map into a vector of arguments.
		 * 
		 * @param args a vector of arguments.
		 * @return an error message or NULL.
		 */
		const char* save(argsave_vector& args)const
		{
			std::string val;
			for(const_iterator beg = argVec.begin(), end=argVec.end();beg != end;++beg)
			{
				val="";
				(*beg)->get(val);
				if( val != "" )
				{
					args.push_back(std::make_pair( (*beg)->name(), val ));
				}
			}
			return 0;
		}
		/** Test if map is writable.
		 * 
		 * @return true if writable.
		 */
		bool iswritable()const
		{
			return levelInt > 0;
		}
		/** Set the write format of the argument map.
		 * 
		 * @param n a format code.
		 */
		void level(int n)
		{
			levelInt = n;
		}

	protected:
		/** Test to ensure that no argument pointer in a vector is NULL.
		 *
		 * @param beg a constant iterator to start of range.
		 * @param end a constant iterator to end of range.
		 * @return true if any argument pointer is null.
		 */
		bool isnull(const_iterator beg, const_iterator end)
		{
			for(;beg != end;++beg) if( *beg == 0 ) return true;
			return false;
		}
		/** Finds an argument based on a name.
		 *
		 * @param nm a string name.
		 * @return an iterator pointing to argument found or end of range.
		 */
		iterator argument(std::string nm)
		{
			return std::find_first_of(flgVec.begin(), flgVec.end(), &nm, &nm+1, compare);
		}
		/** Compares an argument name to some string.
		 *
		 * @param lhs a pointer to an argument.
		 * @param rhs a string.
		 * @return true if argument name matches the string.
		 */
		static bool compare(AbstractArgument* lhs, const std::string& rhs)
		{
			return lhs->name() == rhs;
		}
		/** Removes a comment from a string. The comment is a single line comment (a single character) 
		 * that extends to the end of line.
		 *
		 * @param str a string from which the comment is removed.
		 * @param ch a comment character.
		 */
		static void removeComment(std::string& str, char ch)
		{
			std::string::size_type n = str.find_first_of(ch);
			if( n != std::string::npos ) str = trim(str.substr(0, n));
		}
		/** Splits the first string into two parts by a single character where the first part 
		 * is stored in the first string and the second in the second.
		 *
		 * @param nam source string and destination of first part of the string.
		 * @param val destination of the second part of the string.
		 * @param ch a comment character.
		 */
		static void split(std::string& nam, std::string& val, char ch)
		{
			std::string::size_type n = nam.find_first_of(ch);
			if( n != std::string::npos ) 
			{
				val = trim(nam.substr(n+1));
				nam = trim(nam.substr(0,n));
			}
		}
		
	public:
		/** Gets a constant iterator to the start of the flag vector.
		 * 
		 * @return start of the flag vector.
		 */
		const_iterator flag_begin()const
		{
			return flgVec.begin();
		}
		/** Gets a constant iterator to the end of the flag vector.
		 * 
		 * @return end of the flag vector.
		 */
		const_iterator flag_end()const
		{
			return flgVec.end();
		}
		/** Gets a constant iterator to the start of the group vector.
		 * 
		 * @return start of the group vector.
		 */
		const_group_iterator group_begin()const
		{
			return grpVec.begin();
		}
		/** Gets a constant iterator to the end of the group vector.
		 * 
		 * @return end of the group vector.
		 */
		const_group_iterator group_end()const
		{
			return grpVec.end();
		}
		/** Gets the implicit path of the configuration files as well as other files.
		 * 
		 * @return an implicit path as a string.
		 */
		const std::string& implicit_path()const
		{
			return implicitpth;
		}

	private:
		argument_vector argVec;
		argument_vector mstVec;
		argument_vector flgVec;
		group_vector grpVec;

	private:
		std::string implicitpth;
		mutable std::string compactdef;
		mutable std::string consoledef;
		mutable std::string frameworkdef;
		char tokenCh;
		char newlineCh;
		char commentCh;
	private:
		int outputTypeInt;
		int levelInt;
	};
	
	/** @brief Interface adds arguments to an ArgumentMap.
	 * 
	 * Specializes an argument utility to an argument map.
	 */
	template<>
	class ArgumentUtil<ArgumentMap>
	{
	public:
		/** An interface for adding arguments to an argument map.
		 * 
		 * @param map an argument map.
		 * @param val an argument value object.
		 * @param pf a prefix or name.
		 * @param nm a name or description.
		 * @param ds a description or empty.
		 * @param l a level grouping arguments into views.
		 */
		template<class U>
		static void set(ArgumentMap& map, U& val, std::string pf, std::string nm, std::string ds, int l)
		{
			if(ds == "") map(val, pf, nm, l);
			else map(val, pf, nm, ds, l);
		}
		/** An interface for adding a header to an argument map.
		 * 
		 * @param map an argument map.
		 * @param nm a name.
		 * @param l a level grouping arguments into views.
		 */
		static void set(ArgumentMap& map, std::string nm, int l)
		{
			map(nm, l);
		}
	};

};

#endif


