/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ArgumentTemplate.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ARGUMENTTEMPLATE_H
#define _EXEGETE_ARGUMENTTEMPLATE_H
#include "Template.h"
#include "Argument.h"

/** @file ArgumentTemplate.h
 * @brief Defines an argument template format.
 * 
 * This file contains the ArgumentTemplate class template and the
 * corresponding ArgumentHandler class. The ArgumentTemplate provides an
 * interface to the template mechanism while the handler provides an interface
 * between the ArgumentMap and the output template. 
 *
 * @ingroup ExegeteArgs
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{

	/** @brief %Template handler for writing arguments.
	 * 
	 * This class handles writing arguments in the template.
	 */
	class ArgumentHandler
	{
		typedef std::vector< const AbstractArgument* >			argument_vector;
		typedef argument_vector::iterator						iterator;
		
	public:
		/** Constructs an argument handler.
		 */
		ArgumentHandler() : optionInt(0), group(false), index(0)
		{
		}
		/** Constructs an argument handler.
		 *
		 * @param map a vector of arguments.
		 * @param level controls which arguments should be printed.
		 */
		ArgumentHandler(const std::vector< AbstractArgument* >& map, int level) : optionInt(0), group(false), index(0)
		{
			typedef std::vector< AbstractArgument* >::const_iterator const_arg_iterator;
			args.resize(map.size());
			iterator it = args.begin();
			for(const_arg_iterator beg=map.begin(), end=map.end();beg != end;++beg)
			{
				if( level >= (*beg)->level() ) {
					*it = *beg;
					++it;
				}
			}
			args.resize(std::distance(args.begin(), it));
		}
		
	private:
		/** Assigns an argument vector to the template.
		 *
		 * @param map an argument vector.
		 * @return reference to this object.
		 */
		ArgumentHandler& operator=(const std::vector< AbstractArgument* >& map)
		{
			args.assign(map.begin(), map.end());
			return *this;
		}

	public:
		/** Called when an element is repeatable to increment to next
		 * element and test for completion.
		 *
		 * @param name the name of template symbol.
		 * @param val argument of the template symbol.
		 * @return true if the symbol should repeat.
		 */
		bool repeatnext(const std::string& name, const char* val)
		{
			if( index == args.size() ) return false;
			if( name == "group" )
			{
				++index;
				return index != args.size();
			}
			else if( name == "options" )
			{
				if( optionInt < args[index]->size() )
				{
					++optionInt;
					if( optionInt < args[index]->size() ) return true;
				}
				optionInt=0;
			}
			else if( name == "string" )
			{
				if( optionInt < args[index]->size() )
				{
					++optionInt;
					if( optionInt < args[index]->size() ) return true;
				}
				optionInt=0;
			}
			return false;
		}
		/** Called when the symbol should be checked as to whether it
		 * should be printed.
		 *
		 * @param name the name of template symbol.
		 * @param val argument of the template symbol.
		 * @return true if symbol should be written.
		 */
		bool labelcheck(const std::string& name, const char* val)
		{
			if( index < args.size() )
			{
				if( !group && name == "types" && (index+2) < args.size() && ( args[index+2]->type() != AbstractArgument::HEAD ) ) group=true;
				if( name == "types" )				return true;
				else if( name == "string" )			return args[index]->type() == AbstractArgument::NONE;
				else if( name == "groupheader" )	return !group && args[index]->type() == AbstractArgument::HEAD;
				else if( name == "header" )			return group && args[index]->type() == AbstractArgument::HEAD;
				else if( name == "choose" )			return args[index]->type() == AbstractArgument::OPTN;
				else if( name == "selected" )		return args[index]->isSelected(optionInt);
				else if( name == "unselected" )		return !args[index]->isSelected(optionInt);
				else if( name == "checks" )			return args[index]->type() == AbstractArgument::BOOL;
				else if( name == "check" )			return args[index]->isSelected(1);
				else if( name == "uncheck" )		return args[index]->isSelected(0);
			}
			return false;
		}
		/** Called when the symbol should be written.
		 *
		 * @param out an output stream.
		 * @param name the name of template symbol.
		 * @param val argument of the template symbol.
		 */
		void labelwrite(std::ostream& out, const std::string& name, const char* val)
		{
			ASSERT(index < args.size());
			if( name == "name" )				out << args[index]->name();
			else if( name == "description" )	out << args[index]->desc();
			else if( name == "value" )			out << args[index]->optionAt(optionInt);
			else if( name == "valsel" )			out << args[index]->selectedOption(optionInt);
			else if( name == "optionlbl" && 
					(optionInt+1) < args[index]->size() ) out << val[0];
		}

	private:
		argument_vector args;
		mutable unsigned int optionInt;
		bool group;
		unsigned int index;
	};

	/** @brief %Template format interface for writing arguments.
	 * 
	 * This class provides an interface to write an argument map in a specified template.
	 */
	class ArgumentTemplate : public ArgumentHandler
	{
		typedef std::vector< const AbstractArgument* >			argument_vector;
		typedef argument_vector::const_iterator					const_iterator;
		typedef TemplateSymbolFunctor< ArgumentHandler >		symbol_type;
		typedef Template< symbol_type >							template_type;
	public:
		/** Constructs an argument template with an argument vector.
		 *
		 * @param map an argument vector.
		 * @param l a level grouping arguments into views.
		 */
		ArgumentTemplate(const std::vector< AbstractArgument* >& map, int l) : ArgumentHandler(map, l)//, format(*this)
		{
			format.add(symbol_repeat("map",			"") );
			format.add(symbol_repeat("group",		"") );
			format.add(symbol_check("header",		"") );
			format.add(symbol_check("groupheader",	"") );
			format.add(symbol_check("types",		"") );

			format.add(symbol_check("string",		"") );
			format.add(symbol_check("choose",		"") );
			format.add(symbol_repeat("options",		"") );
			format.add(symbol_check("selected",		"") );
			format.add(symbol_check("unselected",	"") );

			format.add(symbol_check("checks",		"") );
			format.add(symbol_check("check",		"") );
			format.add(symbol_check("uncheck",		"") );

			format.add(symbol_label("name",			"") );
			format.add(symbol_label("value",		"") );
			format.add(symbol_label("optionlbl",	"") );
			format.add(symbol_label("valsel",		"") );
			format.add(symbol_label("description",	"") );
			//format.add(symbol_label("option",		"") );
			format = *this;
		}

	private:
		symbol_type* symbol_repeat(const char* nm, const char* ds)
		{
			return new symbol_type(nm, ds, 0, &ArgumentHandler::repeatnext, 0);
		}
		symbol_type* symbol_label(const char* nm, const char* ds)
		{
			return new symbol_type(nm, ds, &ArgumentHandler::labelwrite, 0, 0);
		}
		symbol_type* symbol_check(const char* nm, const char* ds)
		{
			return new symbol_type(nm, ds, 0, &ArgumentHandler::repeatnext, &ArgumentHandler::labelcheck);
		}	

	public:	
		/** Writes an argument template to the output stream.
		 *
		 * @param out an output stream.
		 * @param format an argument template.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const ArgumentTemplate& format)
		{
			out << format.format;
			return out;
		}
		/** Reads an argument template from the input stream.
		 *
		 * @param in an input stream.
		 * @param format an argument template.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, ArgumentTemplate& format)
		{
			in >> format.format;
			return in;
		}
		/** Gets an error message.
		 *
		 * @return an error message or NULL.
		 */
		const char* errormsg()const
		{
			return format.errormsg();
		}
		/** Builds a template from a string.
		 *
		 * @param layout a string containing the template layout.
		 * @return an error message or NULL.
		 */
		const char* build(const std::string& layout)
		{
			return format.build(layout);
		}

	private:
		template_type format;
	};



};


#endif


