/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ArgumentType.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ARGUMENTTYPE_H
#define _EXEGETE_ARGUMENTTYPE_H
#include "AbstractArgument.h"
#include "stringutil.h"
#include "debugutil.h"
#include "memoryutil.h"

/** @file ArgumentType.h
 * @brief Defines an argument object.
 * 
 * This file contains a class that provides a concrete argument.
 *
 * @ingroup ExegeteArgs
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Concrete class template implementation of an argument.
	 * 
	 * This class template provides a concrete argument object to proxy
	 * any object type.
	 *	
	 * @see AbstractArgument
	 */
	template<class T>
	class ArgumentType : public AbstractArgument
	{
		typedef AbstractArgument parent_type;
	public:
		/** Defines a flag for a master argument. **/
		enum{ MASTER = IsSameType<T, std::vector<const char*> >::value || IsSameType<T, std::vector<std::string> >::value };
	public:
		/** Defines the first template parameter as a value type. **/
		typedef T										value_type;
		/** Defines a pointer to the first template parameter as a pointer. **/
		typedef T*										pointer;
		/** Defines a constant pointer to the first template parameter as a constant pointer. **/
		typedef const T*								const_pointer;
		/** Defines an abstract argument iterator as an iterator. **/
		typedef typename parent_type::iterator			iterator;
		/** Defines an abstract argument constant iterator as a constant iterator. **/
		typedef typename parent_type::const_iterator	const_iterator;
	public:
		/** Constructs a header argument with a description.
		 *
		 * @param ds a string description.
		 */
		ArgumentType(std::string ds) : parent_type(ds), valuePtr(0)
		{
		}
		/** Constructs a master argument with a description.
		 *
		 * @param val a reference to the value type.
		 * @param ds a string description.
		 */
		ArgumentType(T& val, std::string ds) : parent_type(ds, MASTER==1), valuePtr(&val)
		{
			parent_type::type(parent_type::MSTR);
		}
		/** Constructs an argument with a name and a description.
		 *
		 * @param val a reference to the value type.
		 * @param nm a string name.
		 * @param ds a string description.
		 * @param m is argument added as hybrid master.
		 * @param l a level grouping arguments into views.
		 */
		ArgumentType(T& val, std::string nm, std::string ds, bool m, int l) : parent_type(nm, ds, MASTER==1 && m, l), valuePtr(&val)
		{
		}
		/** Destructs an argument.
		 */
		~ArgumentType()
		{
		}

	public:
		/** Converts the specified string and sets the value of the object
		 *  pointed to by the argument. 
		 * 		- Find a mapping from an option name to a value
		 * 		- Otherwise attempt to convert from string to value
		 * 
		 * @note Returns an error of name/value does not match option list for option type.
		 *
		 * @param str string with new value.
		 * @return an error message or NULL.
		 */
		const char* set(const char* str)
		{
			static const char* FLG=",:";
			ASSERT(valuePtr!=0);
			const_iterator it = parent_type::find(parent_type::begin(), parent_type::end(), str);
			if( it != parent_type::end() )
			{
				if( !stringToValue(it->second, *valuePtr, FLG, !parent_type::ismultiple()) )
					return ERRORMSG("Cannot parse option " << str << " with value " << it->second << " as " << TypeUtil<T>::name() << " for " << parent_type::name());
			}
			else if( !stringToValue(str, *valuePtr, FLG, !parent_type::ismultiple()) )
			{
				if( !parent_type::empty() ) return ERRORMSG("Invalid option: " << str << " for " << parent_type::name());
				else return ERRORMSG("Cannot parse " << str << " as " << TypeUtil<T>::name() << " for " << parent_type::name());
			}
			else if( !parent_type::empty() )
			{
				const_iterator it = find(parent_type::begin(), parent_type::end(), *valuePtr);
				if( it == parent_type::end() && parent_type::back().first != "*" ) return ERRORMSG("Invalid option: " << str << " for " << parent_type::name());
			}
			hack_set(*valuePtr);
			return 0;
		}
		/** Converts the value of the object pointed to by the argument to 
		 *  a string and stores it in the destination string argument.
		 * 		- If option type, translate internal to external
		 * 		- Otherwise translate to string
		 * 
		 * @param str reference to string recieving value.
		 */
		void get(std::string& str)const
		{
			static const char* FLG=",:";
			const_iterator it = find(parent_type::begin(), parent_type::end(), hack_get(*valuePtr));
			if( it != parent_type::end() ) str = it->first;
			else valueToString(hack_get(*valuePtr), str, FLG);
		}
		/** Gets a pointer to the object.
		 *
		 * @return a pointer to the object.
		 */
		pointer getpointer()
		{
			return valuePtr;
		}
		/** Gets a constant pointer to the object.
		 *
		 * @return a pointer to an object.
		 */
		const_pointer getpointer()const
		{
			return valuePtr;
		}

	protected:
		/** Finds an option value in a range of options.
		 *
		 * @param beg a constant option iterator to start of range.
		 * @param end a constant option iterator to end of range.
		 * @param val a value to search.
		 * @return a constant option iterator to position of a value found or an end of range.
		 */
		static const_iterator find(const_iterator beg, const_iterator end, const value_type& val)
		{
			std::string str;
			valueToString(val, str);
			return findvalue(beg, end, str);
		}
		/** Finds the index of an option value in a range of options only if the option type is bool.
		 *
		 * @param beg a constant option iterator to start of range.
		 * @param end a constant option iterator to end of range.
		 * @param val a value to search.
		 * @return an index of the option found or size of the range.
		 */
		static unsigned int indexof(const_iterator beg, const_iterator end, const value_type& val)
		{
			return (unsigned int)std::distance(beg, find(beg,end,val));
		}
		/** Converts '0' to 0 and '1' to 1.
		 *
		 * @param ch a reference to a char.
		 */
		void hack_set(char& ch)const
		{
			if( parent_type::type() == parent_type::BOOL )
			{
				if( ch == '0' ) ch = 0;
				else if( ch == '1' ) ch = 1;
			}
		}
		/** Does nothing.
		 *
		 * @param ch a dummy reference to a char type.
		 */
		template<class U> 
		void hack_set(U& ch)const
		{
		}
		/** Converts 0 to '0' and 1 to '1' only if the option type is bool.
		 *
		 * @param ch a char type.
		 * @return a converted char type.
		 */
		char hack_get(char ch)const
		{
			if( parent_type::type() == parent_type::BOOL )
			{
				if( ch == 0 ) return '0';
				else if( ch == 1 ) return'1';
			}
			return ch;
		}
		/** Does nothing.
		 *
		 * @param ch a reference to a value.
		 * @return the same value.
		 */
		template<class U> 
		U& hack_get(U& ch)const
		{
			return ch;
		}

	private:
		pointer valuePtr;
	};

};


#endif


