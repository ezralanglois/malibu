/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ArgumentUtil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ARGUMENTUTIL_H
#define _EXEGETE_ARGUMENTUTIL_H

/** @file ArgumentUtil.h
 * @brief Defines an argument collection interface.
 * 
 * This file contains the ArgumentUtil class and interface functions. The 
 * ArgumentUtil class template provides a generic interface to both adding
 * arguments to a ArgumentMap or getting/setting values in a vector.
 *
 * @ingroup ExegeteArgs
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Compile-time interface for adding arguments to a map.
	 * 
	 * This class template provides an interface for the argument map or 
	 * some other type of mapping. It has no definition except for 
	 * specializations.
	 */
	template<class T>
	class ArgumentUtil;

	/** @brief Interface function adding a header to a map.
	 * 
	 * Initialize an argument in some map with an header.
	 * 
	 * @param map an argument map.
	 * @param nm a name.
	 * @param l a level grouping arguments into views.
	 */
	template<class T>
	void arginit(T& map, std::string nm, int l=0)
	{
		ArgumentUtil<T>::set(map, nm, l);
	}
	/** @brief Interface function adding an argument to a map.
	 * 
	 * Initialize an argument in some map with an argument.
	 * 
	 * @param map an argument map.
	 * @param val an object value.
	 * @param nm a name.
	 * @param ds a description.
	 * @param l a level grouping arguments into views.
	 */
	template<class T, class U>
	void arginit(T& map, U& val, std::string nm, std::string ds, int l=0)
	{
		ArgumentUtil<T>::set(map, val, nm, ds, "", l);
	}
	/** @brief Interface function adding an argument to a map.
	 * 
	 * Initialize an argument in some map with an argument.
	 * 
	 * @param map an argument map.
	 * @param val an argument object.
	 * @param nm a name.
	 * @param ds a description.
	 * @param pf a description or empty.
	 * @param l a level grouping arguments into views.
	 */
	template<class T, class U>
	void arginit(T& map, U& val, std::string nm, std::string ds, std::string pf, int l=0)
	{
		ArgumentUtil<T>::set(map, val, nm, ds, pf, l);
	}
};

#endif



