/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Attribute.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ATTRIBUTE_H
#define _EXEGETE_ATTRIBUTE_H
#include "debugutil.h"
#include "AttributeTypeUtil.h"
#include <vector>
#include <string>
#include <cmath>

/** @file Attribute.h
 * @brief Attribute column description classes
 * 
 * This file contains two classes to describe an attribute column in a dataset.
 * 	- AttributeImpl: General implementation for any column description
 * 	- Attribute: An implementation that can be customized to an specific attribute type
 *
 * @todo Add discreet attribute description
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{
	/** @brief Attribute column description.
	 * 
	 * This class describes an attribute column in a dataset.
	 */
	template<class X>
	class AttributeImpl : public std::vector<std::string>
	{
	public:
		/** Flags types for each attribute
		 *  - numeric: any real value
		 *  - discreet: any integer value
		 *  - binary: values 0/1
		 *  - nominal: string values (unordered)
		 **/
		enum Type
		{
			NUMERIC,  /** Flags a numeric attribute type. **/
			DISCREET, /** Flags a discreet attribute type. **/
			BINARY,   /** Flags a binary attribute type. **/
			NOMINAL   /** Flags a nominal attribute type. **/
		};
	public:
		/** Redefines an enumerated Type as a data type. **/
		typedef enum Type								data_type;
		/** Defines an attribute type utililty as a type utility. **/
		typedef AttributeTypeUtil<X>					type_util;
		/** Defines a type utility value type as a value type. */
		typedef typename type_util::value_type			value_type;
		/** Defines a type utility attribute type as a attribute type. */
		typedef typename type_util::attribute_type		attribute_type;
		/** Defines a type utility index type as a index type. */
		typedef typename type_util::index_type			index_type;
		/** Defines a type utility limit type as a limit type. */
		typedef typename type_util::limit_type			limit_type;
		/** Defines a std::string as a label type. **/
		typedef std::string								label_type;
		/** Defines a std::vector of labels as a vector type. **/
		typedef std::vector<label_type>					vector_type;
	private:
		typedef std::vector<label_type>					parent_type;
	public:
		/** Defines a vector iterator as an iterator. **/
		typedef typename parent_type::iterator			nominal_iterator;
		/** Defines a vector constant iterator as a constant iterator. **/
		typedef typename parent_type::const_iterator	const_nominal_iterator;

	public:
		/** Constructs an attribute column description with a given name.
		 *
		 * @param nm the name of the attribute.
		 */
		AttributeImpl(const label_type& nm="") : nameStr(nm), missingInt(0), zeroInt(0), 
											 typeInt(BINARY), minAttrFlt(limit_type::max()), 
											 maxAttrFlt(limit_type::min()), mulAttrFlt(0.0f), offAttrFlt(0.0f)
		{
		}
		/** Destructs the attribute column description.
		 */
		~AttributeImpl()
		{
		}

	public:
		/** Assigns a nominal vector to an attribute column description.
		 *
		 * @param v a reference to a label vector.
		 * @return a reference to an AttributeImpl.
		 */
		AttributeImpl& operator=(const parent_type& v)
		{
			parent_type::operator=(v);
			return *this;
		}
		/** Assigns a nominal vector to an attribute column description.
		 *
		 * @param label a constant reference to a label.
		 * @return a reference to an AttributeImpl.
		 */
		AttributeImpl& operator=(const label_type& label)
		{
			nameStr=label;
			return *this;
		}

	public:
		/** Clears an attribute column description.
		 */
		void clear()
		{
			typeInt = BINARY;
			parent_type::clear();
		}
		/** Assigns a nominal collection to attribute column.
		 * 
		 * @param beg begin iterator to nominal collection.
		 * @param end end iterator to nominal collection.
		 */
		void assign(const_nominal_iterator beg, const_nominal_iterator end)
		{
			parent_type::assign(beg, end);
			if(beg != end) typeInt = NOMINAL;
		}
		/** Checks if converted from discreet/binary to nominal.
		 * 
		 * @return true if was discreet/binary and now should be nominal.
		 */
		bool isNominalConvert()const
		{
			return !parent_type::empty() && typeInt != NUMERIC && typeInt != NOMINAL && minAttrFlt != limit_type::max();
		}
		/** Gets a name for the current type.
		 *
		 * @return a string name for type.
		 */
		const char* toStringType()const
		{
			if( typeInt == NUMERIC )		return "Numeric";
			else if( typeInt == DISCREET )	return "Discreet";
			else if( typeInt == NOMINAL )	return "Nominal";
			else							return "Binary";
		}
		/** Tests if the attribute is nominal and not binary.
		 *
		 * @return true if attribute type is nominal and not binary.
		 */
		bool isnominalnotbinary()const
		{
			return typeInt == NOMINAL && parent_type::size() > 2;
		}
		/** Tests if the attribute is nominal.
		 *
		 * @return true if attribute type is nominal.
		 */
		bool isnominal()const
		{
			return typeInt == NOMINAL;
		}
		/** Tests if the attribute is binary.
		 *
		 * @return true if attribute type is binary.
		 */
		bool isbinary()const
		{
			return typeInt == BINARY || parent_type::size() == 2;
		}
		/** Tests if the attribute is unsorted (binary or nominal).
		 *
		 * @return true if attribute type is unsorted.
		 */
		bool isunsorted()const
		{
			return typeInt == BINARY || typeInt == NOMINAL;
		}
		/** Tests if the attribute is discreet.
		 *
		 * @return true if attribute type is discreet.
		 */
		bool isdiscreet()const
		{
			return typeInt == DISCREET;
		}
		/** Tests if the attribute is numeric.
		 *
		 * @return true if attribute type is numeric.
		 */
		bool isnumeric()const
		{
			return typeInt == NUMERIC;
		}
		/** Sets the name of the attribute.
		 *
		 * @param val a string holding the name.
		 */
		void name(const label_type& val)
		{
			nameStr = val;
		}
		/** Gets the name of the attribute.
		 *
		 * @return the string name.
		 */
		const label_type& name()const
		{
			return nameStr;
		}
		/** Gets the minimum value in the attribute column.
		 *
		 * @return minimum value in attribute column.
		 */
		value_type min()const
		{
			return minAttrFlt;
		}
		/** Gets the maximum value in the attribute column.
		 *
		 * @return maximum value in attribute column.
		 */
		value_type max()const
		{
			return maxAttrFlt;
		}
		/** Gets the mulitpler value in the attribute column.
		 *
		 * @return maximum value in attribute column.
		 */
		value_type mult()const
		{
			return mulAttrFlt;
		}
		/** Gets the offset value in the attribute column.
		 *
		 * @return maximum value in attribute column.
		 */
		value_type offs()const
		{
			return offAttrFlt;
		}
		/** Sets the minimum value in the attribute column.
		 *
		 * @param val minimum value in attribute column.
		 */
		void min(value_type val)
		{
			minAttrFlt = val;
		}
		/** Sets the maximum value in the attribute column.
		 *
		 * @param val maximum value in attribute column.
		 */
		void max(value_type val)
		{
			maxAttrFlt = val;
		}
		/** Sets the multiplier value in the attribute column.
		 *
		 * @param val multiplier value in attribute column.
		 */
		void mult(value_type val)
		{
			mulAttrFlt = val;
		}
		/** Sets the offset value in the attribute column.
		 *
		 * @param val offset value in attribute column.
		 */
		void offs(value_type val)
		{
			offAttrFlt = val;
		}
		/** Gets the number of missing values in the attribute column.
		 *
		 * @return number of missing values in attribute column.
		 */
		index_type missing()const
		{
			return missingInt;
		}
		/** Gets the number of zeros in the attribute column.
		 *
		 * @return number of zeros in attribute column.
		 */
		index_type zeros()const
		{
			return zeroInt;
		}
		/** Gets the type of values found in the attribute column.
		 *
		 * @return type of values found in the attribute column.
		 */
		data_type type()const
		{
			return typeInt;
		}
		/** Tests of column description contains the nominal label.
		 *
		 * @param str a string nominal label.
		 * @return true if vector contains nominal label.
		 */
		bool contains(const std::string& str)const
		{
			return std::find(parent_type::begin(), parent_type::end(), str) != parent_type::end();
		}
		/** Gets the index of the label.
		 *
		 * @param str a string label.
		 * @return index of label.
		 */
		size_type indexof(const std::string& str)const
		{
			return size_type(std::distance(parent_type::begin(), std::find(parent_type::begin(), parent_type::end(), str)));
		}
		/** Encodes a nominal label into an internal value.
		 *
		 * @param str a string nominal label.
		 * @param miss a missing value flag.
		 * @param add should add new nominal label.
		 * @return internal representation of label.
		 */
		template<class U>
		U valueOf(const std::string& str, U miss, bool add)
		{
			size_type n = indexof(str);
			if( n == parent_type::size() )
			{
				if( add )
				{
					parent_type::push_back(str);
					return U(parent_type::size()-1);
				}
				return miss;
			}
			return U(n);
		}

	public:
		/** Assesses the statistics of a particular attribute column. Every attribute is assumed to 
		 * be binary or nominal (if the string names are set). This function is run on every value 
		 * in the column to determine the column type. It also records the number of missing 
		 * values, zeros, minimum and maximum values.
		 *
		 * @param val the value to count.
		 */
		void count(value_type val)
		{
			if( val != type_util::missing() )
			{
				if( val < minAttrFlt ) minAttrFlt = val;
				if( val > maxAttrFlt ) maxAttrFlt = val;
				if( !parent_type::empty() ) 
				{
					typeInt=NOMINAL;
				}
				else if( typeInt == NUMERIC );
				else if( value_type(std::floor(double(val))) != val ) typeInt = NUMERIC;
				else if(typeInt == DISCREET);
				else if( val != 1 && val != 0 ) typeInt = DISCREET;
				if( val == value_type(0) ) zeroInt++;
			}
			else missingInt++;
		}
		/** Reset the counts.
		 */
		void reset()
		{
			missingInt = 0;
			zeroInt = 0;
			typeInt = BINARY;
			minAttrFlt = limit_type::max();
			maxAttrFlt = limit_type::min();
		}
		/** Gets the string representation of the column description.
		 * 
		 * @return column description to string.
		 */
		std::string toString()const
		{
			std::ostringstream out;
			out << nameStr << "\t" << toStringType() << "\t";
			out << missingInt << "\t" << zeroInt << "\t";
			out << minAttrFlt << "\t" << maxAttrFlt;
			return out.str();
		}
		/** Writes an attribute description to an output stream.
		 * 
		 * @param out an output stream.
		 * @param h an attribute column description.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const AttributeImpl& h)
		{
			out << h.name();
			return out;
		}
		
	public:
		/** Gets the range of min max values.
		 * 
		 * @return range of min max.
		 */
		value_type range()const
		{
			return maxAttrFlt-minAttrFlt+1;
		}
		/** Tests if attribute column is normalized.
		 * 
		 * @return true if all values fall between 1 and -1.
		 */
		bool isnormalized()const
		{
			return isbinary() || isnominal() || ( max() <= 1.0 && min() >= -1.0 );
		}

	private:
		label_type nameStr;
		index_type missingInt;
		index_type zeroInt;
		data_type typeInt;
		value_type minAttrFlt;
		value_type maxAttrFlt;
		//@todo removed
		value_type mulAttrFlt;
		value_type offAttrFlt;
	};

	/** @brief Attribute column description.
	 * 
	 * This class template by default works as an AttributeHeader.
	 */
	template<class X>
	class Attribute : public AttributeImpl<X>
	{
		template<class X1> friend class Attribute;
		typedef AttributeImpl<X>						parent_type;
	public:
		/** Defines a parent data type as a data type. **/
		typedef typename parent_type::data_type			data_type;
		/** Defines a parent index type as a index type. **/
		typedef typename parent_type::index_type		index_type;
		/** Defines a parent type util as a type util. **/
		typedef typename parent_type::type_util			type_util;
		/** Defines a parent value type as a value type. **/
		typedef typename parent_type::value_type		value_type;
		/** Defines a parent limit type as a limit type. **/
		typedef typename parent_type::limit_type		limit_type;
		/** Defines a parent label type as a label type. **/
		typedef typename parent_type::label_type		label_type;
		/** Defines a parent attribute type as a attribute type. **/
		typedef typename parent_type::attribute_type	attribute_type;
		/** Defines a parent vector type as a vector type. **/
		typedef typename parent_type::vector_type		vector_type;
	public:
		/** Constructs an attribute header.
		 *
		 * @param nm an attribute name.
		 */
		Attribute(const label_type& nm="") : parent_type(nm)
		{
		}
		/** Destructs an attribute header.
		 */
		~Attribute()
		{
		}

	public:
		/** This assignment operator copies a vector for labels to the attribute header.
		 *
		 * @param vec a reference to a label vector.
		 * @return a reference to an Attribute.
		 */
		Attribute& operator=(const vector_type& vec)
		{
			parent_type::operator=(vec);
			return *this;
		}
		/** This assignment operator copies a label to the attribute header.
		 *
		 * @param label a constant reference to a label.
		 * @return a reference to an Attribute.
		 */
		Attribute& operator=(const label_type& label)
		{
			parent_type::name(label);
			return *this;
		}
	};

	/** This specialized class template handles discreet attributes.
	 */
//	template<>
//	class Attribute<unsigned int> : public AttributeImpl<unsigned int>
//	{
//		template<class X1> friend class Attribute;
//		typedef AttributeImpl<unsigned int> parent_type;
//	public:
//		/** Defines a parent data type as a data type. **/
//		typedef parent_type::data_type				data_type;
//		/** Defines a parent type util as a type util. **/
//		typedef parent_type::type_util				type_util;
//		/** Defines a parent value type as a value type. **/
//		typedef parent_type::value_type				value_type;
//		/** Defines a parent index type as a index type. **/
//		typedef parent_type::index_type				index_type;
//		/** Defines a parent limit type as a limit type. **/
//		typedef parent_type::limit_type				limit_type;
//		/** Defines a parent label type as a label type. **/
//		typedef parent_type::label_type				label_type;
//		/** Defines a parent attribute type as a attribute type. **/
//		typedef parent_type::attribute_type			attribute_type;
//		/** Defines a parent vector type as a vector type. **/
//		typedef parent_type::vector_type			vector_type;
//
//	public:
//		/** Defines a float as a float type. **/
//		typedef float								float_type;
//		/** Defines a std::vector<float> as a float vector. **/
//		typedef std::vector<float_type>				float_vector;
//		/** Defines a std::vector<float_vector> as a floatvec vector. **/
//		typedef std::vector<float_vector>			floatvec_vector;
//	public:
//		/** Constructs an attribute header.
//		 *
//		 * @param nm an attribute name.
//		 */
//		Attribute(const label_type& nm="") : parent_type(nm)
//		{
//		}
//		/** Destructs an attribute header.
//		 */
//		~Attribute()
//		{
//		}
//
//	public:
//		/** This assignment operator copies a vector for labels to the attribute header.
//		 *
//		 * @param vec a reference to a label vector.
//		 * @return a reference to an Attribute.
//		 */
//		Attribute& operator=(const vector_type& vec)
//		{
//			parent_type::operator=(vec);
//			return *this;
//		}
//		/** This assignment operator copies a label to the attribute header.
//		 *
//		 * @param label a constant reference to a label.
//		 * @return a reference to an Attribute.
//		 */
//		Attribute& operator=(const label_type& label)
//		{
//			parent_type::name(label);
//			return *this;
//		}
//
//	private:
//		floatvec_vector floatVec; 
//	};

};


#endif




