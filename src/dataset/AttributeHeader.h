/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AttributeHeader.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_ATTRIBUTEHEADER_H
#define _EXEGETE_ATTRIBUTEHEADER_H
#include "ExampleBag.h"
#include "Attribute.h"

/** @file AttributeHeader.h
 * @brief Collection of attribute column descriptions.
 * 
 * This file contains the AttributeHeader class template.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Collection of attribute column descriptions.
	 *  
	 * This class comprises a header describing attribute columns.
	 */
	template<class X, class Y>
	class AttributeHeader
	{
		template<class X1, class Y1> friend class AttributeHeader;
	public:
		/** Defines an AttributeTypeUtil as a type util. **/
		typedef AttributeTypeUtil<X>					type_util;
		/** Defines a type util value as a value type. **/
		typedef typename type_util::value_type			feature_type;
		/** Defines a type util attribute as a attribute type. **/
		typedef typename type_util::attribute_type		attribute_type;
		/** Defines a type util index as a index type. **/
		typedef typename type_util::index_type			index_type;
		/** Defines a size_t as a size type. **/
		typedef size_t									size_type;
	public:
		/** Defines an Attribute as a header type. **/
		typedef Attribute<feature_type>					header_type;
		/** Defines a pointer to a Attribute as a header pointer. **/
		typedef Attribute<feature_type>*				header_pointer;
		/** Defines a pointer to a Attribute as a header iterator. **/
		typedef Attribute<feature_type>*				header_iterator;
		/** Defines a constant pointer to a Attribute as a constant header iterator. **/
		typedef const Attribute<feature_type>*			const_header_iterator;
	public:
		/** Defines a pointer to characters as a label type. **/
		typedef std::string								label_type;
		/** Defines a pointer to a pointer to characters as a label pointer. **/
		typedef label_type*								label_pointer;
		/** Defines a constant pointer to a pointer to characters as a constant label pointer. **/
		typedef const label_type*						const_label_pointer;
		/** Defines a pointer to a pointer to characters as a label iterator. **/
		typedef label_type*								label_iterator;
		/** Defines a constant pointer to a pointer to characters as a constant label iterator. **/
		typedef const label_type*						const_label_iterator;
		/** Defines a label reference. **/
		typedef label_type&								label_reference;
		/** Defines a constant label reference. **/
		typedef const label_type&						const_label_reference;
		/** Flags the class attribute column as real. **/
		enum{ is_regress = type_traits<Y>::is_floating };

	public:
		/** Constructs a default attribute header.
		 * 
		 * @param a number of attributes.
		 * @param l number of labels.
		 */
		AttributeHeader(size_type a=0, size_type l=0) : headerPtr(0), attributeCountInt(a), labelCountInt(l), classCountInt(0), sparseCountInt(0)
		{
		}
		/** Destructs an attribute header.
		 */
		~AttributeHeader()
		{
		}

	public:
		/** Constructs a copy of an attribute header.
		 *
		 * @param h an attribute header to copy.
		 */
		template<class Y1>
		AttributeHeader(const AttributeHeader<X,Y1>& h) : headerPtr(h.headerPtr), attributeCountInt(h.attributeCountInt), labelCountInt(h.labelCountInt), classCountInt(h.classCountInt), sparseCountInt(h.sparseCountInt)
		{
		}
		/** Assigns an attribute header.
		 *
		 * @param h an attribute header.
		 * @return a reference to this object.
		 */
		AttributeHeader& operator=(const AttributeHeader& h)
		{
			headerPtr = h.headerPtr;
			attributeCountInt = h.attributeCountInt;
			labelCountInt = h.labelCountInt;
			classCountInt = h.classCountInt;
			sparseCountInt = h.sparseCountInt;
			return *this;
		}

	public:
		/** Translates an column name to an index.
		 * 
		 * @param name column name.
		 * @return column index.
		 */
		size_type attributeIndex(const std::string& name)const
		{
			for(size_type i=0;i<attributeCountInt;++i)
				if( name == attributeAt(i).name() ) return i;
			return attributeCountInt;
		}
		/** Gets an attribute column description in the specified column.
		 *
		 * @param n the index of the attribute column.
		 * @return an attribute header.
		 */
		const header_type& headerAt(size_type n)const
		{
			ASSERT( headerPtr != 0 );
			ASSERTMSG(n < headerCount(), n << " < " << headerCount() );
			return headerPtr[n];
		}
		/** Gets a class column description.
		 *
		 * @return a column description.
		 */
		const header_type& classHeader()const
		{
			return headerAt(labelCountInt);
		}
		/** Gets a class column description.
		 *
		 * @return a column description.
		 */
		header_type& classHeader()
		{
			return headerAt(labelCountInt);
		}
		/** Gets a class label at a specific index.
		 *
		 * @param n a index to a class.
		 * @return a class label.
		 */
		const label_type& classAt(size_type n)const
		{
			ASSERTMSG(n < classHeader().size(), n << " < " << classHeader().size() << " != " << classCountInt );
			return classHeader()[n];
		}
		/** Gets an attribute description in the specified column.
		 *
		 * @param n the index of the attribute column.
		 * @return an attribute header.
		 */
		const header_type& attributeAt(size_type n)const
		{
			ASSERTMSG(n < attributeCount(), n << " " << attributeCount() << " " << headerPtr );
			return headerAt(n+1+labelCountInt);
		}
		/** Counts the number of example with a specific type.
		 *
		 * @param d an enumerated type.
		 * @return the number of examples with the specified type.
		 */
		size_type countType(int d)const
		{
			size_type cnt=0;
			for(const_header_iterator beg = header_begin(), end = header_end();beg != end;++beg) 
				if( beg->type() == d ) cnt++;
			return cnt;
		}
		/** Gets the number of missing values for every attribute column.
		 *
		 * @return the number of missing values.
		 */
		size_type missing()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = header_begin(), end = header_end();beg != end;++beg) cnt += beg->missing();
			return cnt;
		}
		/** Gets the number of zero values for every attribute column.
		 *
		 * @return the number of zero values.
		 */
		size_type zeros()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = header_begin(), end = header_end();beg != end;++beg) cnt += beg->zeros();
			return cnt;
		}
		/** Determines the max nominal size.
		 *
		 * @return max nominal size.
		 */
		size_type maximumNominal()const
		{
			size_type nom=0;
			for(size_type i=0;i<attributeCount();++i)
				nom = std::max(nom, attributeAt(i).size());
			return nom;
		}

	public:
		/** Gets an iterator to the start of a collection of classes.
		 *
		 * @return a constant iterator to the start of a class collection.
		 */
		const_label_iterator class_begin()const
		{
			return &(*classHeader().begin());
		}
		/** Gets an iterator to the end of a collection of classes.
		 *
		 * @return a constant iterator to the end of a class collection.
		 */
		const_label_iterator class_end()const
		{
			return class_begin()+classCount();
		}
		/** Gets an iterator to the start of a collection of classes.
		 *
		 * @return an iterator to the start of a class collection.
		 */
		label_iterator class_begin()
		{
			return &(*classHeader().begin());
		}
		/** Gets an iterator to the end of a collection of classes.
		 *
		 * @return an iterator to the end of a class collection.
		 */
		label_iterator class_end()
		{
			return class_begin()+classCount();
		}
		/** Gets an iterator to the start of a attribute header collection.
		 *
		 * @return an beginning iterator.
		 */
		header_iterator header_begin()
		{
			return headerPtr+1+labelCountInt;
		}
		/** Gets an iterator to the end of a attribute header collection.
		 *
		 * @return an ending iterator.
		 */
		header_iterator header_end()
		{
			return header_begin()+attributeCountInt;
		}
		/** Gets an iterator to the start of a attribute header collection.
		 *
		 * @return an beginning constant iterator. 
		 */
		const_header_iterator header_begin()const
		{
			return headerPtr+1+labelCountInt;
		}
		/** Gets an iterator to the end of a attribute header collection.
		 *
		 * @return an ending constant iterator.
		 */
		const_header_iterator header_end()const
		{
			return header_begin()+attributeCountInt;
		}

	public:
		/** Gets the number of labels.
		 *
		 * @return the number of labels.
		 */
		size_type labelCount()const
		{
			return labelCountInt;
		}
		/** Gets the number of attributes.
		 *
		 * @return the number of attributes.
		 */
		size_type attributeCount()const
		{
			return attributeCountInt;
		}
		/** Sets the number of attribute columns.
		 * 
		 * @param a number of attributes.
		 */
		void attributeCount(size_type a)
		{
			attributeCountInt = a;
		}
		/** Gets the number of columns.
		 *
		 * @return the number of columns.
		 */
		size_type headerCount()const
		{
			return attributeCountInt+labelCountInt+1;
		}
		/** Gets the number of classes.
		 *
		 * @return the number of classes.
		 */
		size_type classCount()const
		{
			return classCountInt;
		}
		/** Sets the number of classes.
		 * 
		 * @param n number of classes.
		 */
		void classCount(size_type n)
		{
			classCountInt = n;
		}

	public:
		/** Gets an attribute column description for the specified column.
		 *
		 * @param n the index of the attribute column.
		 * @return an attribute column description.
		 */
		header_type& headerAt(size_type n)
		{
			ASSERT( headerPtr != 0 );
			ASSERTMSG(n < headerCount(), n << " < " << headerCount() );
			return headerPtr[n];
		}
		/** Gets an attribute column description for the specified column.
		 *
		 * @param n the index of the attribute column.
		 * @return an attribute column description.
		 */
		header_type& attributeAt(size_type n)
		{
			ASSERTMSG(n < attributeCount(), n << " < " << attributeCount() << " " << headerPtr );
			return headerAt(n+1+labelCountInt);
		}
		/** Tests if every column is normalized.
		 * 
		 * @return true if every column is normalized.
		 */
		bool isnormalized()const
		{
			for(const_header_iterator beg = header_begin(), end = header_end();beg != end;++beg)
				if( !beg->isnormalized() ) return false;
			return true;
		}
		/** Tests if any column has nominal attributes.
		 * 
		 * @return true if a column has nominal attributes.
		 */
		bool hasnominal()const
		{
			for(const_header_iterator beg = header_begin(), end = header_end();beg != end;++beg)
				if( beg->isnominal() ) return true;
			return false;
		}
		/** Tests if any column has numeric or discreet attributes.
		 * 
		 * @return true if a column has numeric or discreet attributes.
		 */
		bool hasnumeric()const
		{
			for(const_header_iterator beg = header_begin(), end = header_end();beg != end;++beg)
				if( beg->isnumeric() || beg->isdiscreet() ) return true;
			return false;
		}
		
		/** Tests if every column is binary attributes.
		 * 
		 * @return false if column is not binary.
		 */
		bool onlybinary()const
		{
			for(const_header_iterator beg = header_begin(), end = header_end();beg != end;++beg)
				if( !beg->isbinary() ) return false;
			return true;
		}
		/** Tests if every column is nominal attributes.
		 * 
		 * @return false if column is not nominal.
		 */
		bool onlynominal()const
		{
			for(const_header_iterator beg = header_begin(), end = header_end();beg != end;++beg)
				if( !beg->isnominal() ) return false;
			return true;
		}
		/** Creates a deep copy of an attribute header.
		 * 
		 * @param h an attribute header.
		 */
		void deep_copy(const AttributeHeader& h)
		{
			classCountInt = h.classCountInt;
			resizeHeader(h.attributeCountInt+1+h.labelCountInt, h.labelCountInt);
			std::copy(h.headerPtr, h.headerPtr+h.attributeCountInt+1+h.labelCountInt, headerPtr);
		}
		/** Clears the attributes and labels allocated in each example.
		 */
		void clear()
		{
			erase(headerPtr);
			attributeCountInt = 0;
			labelCountInt = 0;
			classCountInt = 0;
		}
		
	protected:
		/** Allocates memory for the collection of attribute descriptions.
		 *
		 * @param len number of columns.
		 * @param lbs number of labels.
		 */
		void resizeHeader(size_type len, size_type lbs)
		{
			if( len != (attributeCountInt+lbs+1))
			{
				headerPtr = ::resize(headerPtr, len, attributeCountInt+1+labelCountInt);
				attributeCountInt = len-lbs-1;
				labelCountInt = lbs;
			}
		}
		/** Recalculate attribute statistics.
		 *
		 * @param ptr a pointer to an attribute array.
		 * @param n old length of attribute array.
		 */
		void recalculate(X* ptr, size_type n)
		{
			for(size_type i=n;i<attributeCountInt;++i) type_util::valueOf(ptr[i]) = 0;
			recalculate(ptr);
		}
		/** Recalculate attribute statistics.
		 *
		 * @param it a pointer to an attribute array.
		 */
		void recalculate(X* it)
		{
			for(size_type i=0, j=0;i<attributeCountInt;++i) 
				attributeAt(i).count(type_util::valueOf(it, i, j));
		}
		/** Resets the attribute counts.
		 */
		void reset()
		{
			for(size_type i=0;i<attributeCountInt;++i)
				attributeAt(i).reset();
		}
	public:
		/** Gets the maximum number of non-sparse attributes over all examples.
		 * 
		 * @return max number of non-sparse attributes.
		 */
		size_type maxSparseAttribute()const
		{
			return sparseCountInt;
		}

	protected:
		/** A pointer to an array of attribute headers. **/
		header_pointer headerPtr;
		/** The number of attributes. **/
		size_type attributeCountInt;
		/** The number of labels. **/
		size_type labelCountInt;
		/** The number of classes. **/
		size_type classCountInt;
		/** The number of attributes. **/
		size_type sparseCountInt;
	};

};

#endif

