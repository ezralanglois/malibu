/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AttributeTypeUtil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ATTRIBUTETYPE_H
#define _EXEGETE_ATTRIBUTETYPE_H
#include "typeutil.h"
#include "debugutil.h"
#include "errorutil.h"

/** @file AttributeTypeUtil.h
 * @brief Interface handling both sparse and standard attributes.
 * 
 * This file contains the AttributeUtil and the AttributeTypeUtil class.
 *
 * This file contains the AttributeUtil and the AttributeTypeUtil class, which 
 * provide a standard interface for accessing different attribute types including:
 *  - normal
 *  - sparse
 * 
 * @todo union
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Interface for a standard attribute.
	 * 
	 * This class represents an attribute type corresponding to a column in
	 * the dataset. This is the class to specialize to a new attribute type.
	 */
	template<class X>
	class AttributeUtil
	{
	public:
		/** Indicates the attribute type is not sparse. **/
		enum{IS_SPARSE=false};
	public:
		/** Defines a TypeUtil as limit type. **/
		typedef TypeUtil<X>		limit_type;
		/** Defines the template parameter as a value type. **/
		typedef X				value_type;
		/** Defines the template parameter as an attribute type. **/
		typedef X				attribute_type;
		/** Defines pointer to template parameter as pointer. **/
		typedef X*				pointer;
		/** Defines constant pointer to template parameter as constant pointer. **/
		typedef const X*		const_pointer;
		/** Defines an int as an index type. **/
		typedef int				index_type;
		/** Defines a size_t as a size type. **/
		typedef size_t			size_type;
		/** Defines a pointer difference as a difference type. */
		typedef ptrdiff_t   	difference_type;

	public:
		/** Converts an attribute to a value.
		 * 
		 * @note It does nothing for standard attributes.
		 *
		 * @param attr a constant reference to an attribute type.
		 * @return attribute value
		 */
		static const value_type valueOf(const attribute_type& attr)
		{
			return attr;
		}
		/** Converts an attribute to a value.
		 * 
		 * @note It does nothing for standard attributes.
		 *
		 * @param attr a reference to an attribute type.
		 * @return attribute value
		 */
		static value_type& valueOf(attribute_type& attr)
		{
			return attr;
		}
		/** Converts an attribute to an index. 
		 * 
		 * @note It returns zero for standard attributes.
		 *
		 * @param attr a constant reference to an attribute type.
		 * @return zero
		 */
		static index_type indexOf(const attribute_type& attr)
		{
			return 0;
		}
		/** Converts an attribute to an index.
		 * 
		 * @note It returns zero for standard attributes.
		 *
		 * @param attr a reference to an attribute type.
		 * @return zero
		 */
		static index_type& indexOf(attribute_type& attr)
		{
			static index_type tmpindex=0;
			return tmpindex;
		}
		/** Flags an attribute as last.
		 * 
		 * @note It does nothing for standard attributes.
		 *
		 * @param p a pointer to an attribute.
		 * @param n an index in the attribute vector.
		 */
		static void marklast(pointer p, size_type n)
		{
		}
		/** Gets the internal value of a missing attribute.
		 *
		 * @return maximum value for type.
		 */
		static value_type missing()
		{
			return limit_type::max();
		}
		/** Tests if attribute is last sparse attribute.
		 *
		 * @param attr an attribute type.
		 * @return false
		 */
		static bool islast(const attribute_type& attr)
		{
			return false;
		}
	};

	/** @brief Interface for a general sparse attribute.
	 * 
	 * This class represents an attribute description corresponding to a column 
	 * in the dataset. It specializes the class template to a std::pair<X,int>.
	 */
	template<class X, class I>
	class AttributeUtil< std::pair<X,I> >
	{
	public:
		/** Flags a column as sparse. **/
		enum{IS_SPARSE=true};
	public:
		/** Defines a TypeUtil as a limit type. **/
		typedef TypeUtil<X>				limit_type;
		/** Defines the template parameter of std::pair as a value type. **/
		typedef X						value_type;
		/** Defines the std::pair as an attribute type. **/
		typedef std::pair<X,I>			attribute_type;
		/** Defines a pointer to an attribute type as a pointer. **/
		typedef attribute_type*			pointer;
		/** Defines a constant pointer to attribute type as a constant pointer. **/
		typedef const attribute_type*	const_pointer;
		/** Defines an int as an index type. **/
		typedef I						index_type;
		/** Define sa size_t as a size type. **/
		typedef size_t					size_type;
		/** Defines a pointer difference as a difference type. */
		typedef ptrdiff_t   difference_type;

	public:
		/** Converts an attribute type to a value type. 
		 *
		 * @param attr an attribute.
		 * @return first member of a std::pair.
		 */
		static const value_type& valueOf(const attribute_type& attr)
		{
			return attr.first;
		}
		/** Converts an attribute type to a value type.
		 *
		 * @param attr an attribute.
		 * @return first member of a std::pair.
		 */
		static value_type& valueOf(attribute_type& attr)
		{
			return attr.first;
		}
		/** Converts an attribute type to an index type.
		 *
		 * @param attr an attribute.
		 * @return second member of a std::pair.
		 */
		static index_type indexOf(const attribute_type& attr)
		{
			return attr.second;
		}
		/** Converts an attribute type to an index type.
		 *
		 * @param attr an attribute.
		 * @return second member of a std::pair.
		 */
		static index_type& indexOf(attribute_type& attr)
		{
			return attr.second;
		}
		/** Flags the specified attribute as the last.
		 *	- Increment the index reference.
		 *	- Assign attribute index to -1.
		 *
		 * @param px a pointer to attributes.
		 * @param n index of attribute.
		 */
		static void marklast(pointer px, size_type& n)
		{
			++n;
			px[n].second = -1;
		}
		/** Gets the internal value of a missing attribute.
		 *
		 * @return maximum value of type.
		 */
		static value_type missing()
		{
			return limit_type::max();
		}
		/** Tests if attribute is last sparse attribute.
		 *
		 * @param attr an attribute.
		 * @return true if attribute index is last.
		 */
		static bool islast(const attribute_type& attr)
		{
			return attr.second == -1;
		}
	};
	
	/** @brief Interface for a standard attribute.
	 * 
	 * This helper class template handles standard attribute types. This is 
	 * the class use as an interface in the code. The idea is to transparently
	 * handle both sparse and standard attributes in the code.
	 */
	template<class X, bool S=AttributeUtil<X>::IS_SPARSE>
	class AttributeTypeUtil : public AttributeUtil<X>
	{
	public:
		/** Defines a AttributeTypeUtil as a utility type **/
		typedef AttributeUtil<X> util_type;
	public:
		/** Defines a AttributeTypeUtil limit type as a limit type. **/
		typedef typename util_type::limit_type		limit_type;
		/** Defines a AttributeTypeUtil value type as a value type. **/
		typedef typename util_type::value_type		value_type;
		/** Defines a AttributeTypeUtil attribute type as an attribute type. **/
		typedef typename util_type::attribute_type	attribute_type;
		/** Defines a AttributeTypeUtil pointer as a pointer. **/
		typedef typename util_type::pointer			pointer;
		/** Defines a AttributeTypeUtil constant pointer as a constant pointer. **/
		typedef typename util_type::const_pointer	const_pointer;
		/** Defines a AttributeTypeUtil index type as an index type. **/
		typedef typename util_type::index_type		index_type;
		/** Defines a AttributeTypeUtil size type as a size type. **/
		typedef typename util_type::size_type		size_type;
		/** Defines a AttributeTypeUtil size type as a size type. **/
		typedef typename util_type::difference_type	difference_type;
		/** Defines a constant value type as a constant reference. **/
		typedef const value_type					const_reference;
		/** Defines a constant value type as a constant reference. **/
		typedef value_type&							reference;
		
	public:
		/** Sets the value of an attribute.
		 * 
		 * @param attr an attribute.
		 * @param val a value.
		 */
		static void setValue(attribute_type& attr, const value_type& val)
		{
			valueOf(attr) = val;
		}
		/** Sets the index of an attribute.
		 * 
		 * @param attr an attribute.
		 * @param idx an index.
		 */
		static void setIndex(attribute_type& attr, const index_type& idx)
		{
			util_type::indexOf(attr) = idx;
		}
		/** Gets the value of an attribute.
		 * 
		 * @param attr an attribute.
		 * @return a value.
		 */
		static const value_type getValue(const attribute_type& attr)
		{
			return valueOf(attr);
		}
		/** Gets the index of an attribute.
		 * 
		 * @param attr an attribute.
		 * @param i an index.
		 * @return an index.
		 */
		static const index_type getIndex(const attribute_type& attr, index_type i)
		{
			return i+1;
		}
		/** Gets the value of an attribute.
		 * 
		 * @param attr an attribute.
		 * @return an index.
		 */
		static const index_type getIndex(const attribute_type& attr)
		{
			return util_type::indexOf(attr);
		}
		/** Converts an attribute to a value.
		 *
		 * @param attr an attribute type.
		 * @return a value type.
		 */
		static const_reference valueOf(const attribute_type& attr)
		{
			return util_type::valueOf(attr);
		}
		/** Converts an attribute to a value.
		 *
		 * @param attr an attribute type.
		 * @return a value type.
		 */
		static value_type& valueOf(attribute_type& attr)
		{
			return util_type::valueOf(attr);
		}
		/** Assigns zeros up to the given index and then the given value.
		 *
		 * @param px a pointer to an attribute array.
		 * @param n a reference to an index.
		 * @param val a constant reference to a value.
		 * @param idx the current index of the value.
		 */
		static void set_value_index(pointer px, size_type& n, const value_type& val, size_type idx)
		{
			if( idx > 0 )
			{
				--idx;
				for(size_type i=n;i<idx;++i) util_type::valueOf(px[i])=0;
			}
			util_type::valueOf(px[idx]) = val;
			n = idx;
		}
		/** Assigns zeros up to the given index and then the given value.
		 *
		 * @param pbeg a pointer to an attribute array.
		 * @param pcur a pointer to current location.
		 * @param val a constant reference to a value.
		 * @param idx the current index of the value.
		 */
		/*static pointer set_value_index(pointer pbeg, pointer pcur, const value_type& val, size_type idx)
		{
			if( idx > 0 )
			{
				for(pointer pend = pbeg+idx;pcur < pend;++pcur) ) util_type::valueOf(*pcur) = 0;
			}
			util_type::valueOf(*pcur) = val;
			return pcur;
		}*/
		/** Gets the value of an attribute.
		 *
		 * @param px a constant pointer to the attribute array.
		 * @param n the index.
		 * @param dummy a dummy variable.
		 * @return value of the attribute.
		 */
		static const_reference valueOf(const_pointer px, size_type n, size_type dummy)
		{
			return util_type::valueOf(px[n]);
		}
		/** Tests if the index has reached the end.
		 *
		 * @param p a dummy pointer.
		 * @param n the current index.
		 * @param len the length of the range.
		 * @return true if n has reached len.
		 */
		static bool isnotend(const_pointer p, size_type n, size_type len)
		{
			return n < len;
		}
		/** Gets the maximum index.
		 *
		 * @param p a dummy pointer.
		 * @param n a dummy index.
		 * @param m the maximum index.
		 * @return m.
		 */
		static size_type max(const_pointer p, size_type n, size_type m)
		{
			return m;
		}
	};

	/** @brief Interface for a sparse attribute.
	 * 
	 * This specialized helper class template handles sparse attribute types.
	 */
	template<class X>
	class AttributeTypeUtil<X,true> : public AttributeUtil<X>
	{
	public:
		/** Defines an AttributeUtil as a utility type. **/
		typedef AttributeUtil<X> util_type;
	public:
		/** Defines an AttributeUtil limit type as limit type. **/
		typedef typename util_type::limit_type		limit_type;
		/** Defines an AttributeUtil value type as value type. **/
		typedef typename util_type::value_type		value_type;
		/** Defines an AttributeUtil attribute type as attribute type. **/
		typedef typename util_type::attribute_type	attribute_type;
		/** Defines an AttributeUtil pointer as pointer. **/
		typedef typename util_type::pointer			pointer;
		/** Defines an AttributeUtil constant pointer as a constant pointer. **/
		typedef typename util_type::const_pointer	const_pointer;
		/** Defines an AttributeUtil index type as index type. **/
		typedef typename util_type::index_type		index_type;
		/** Defines an AttributeUtil size type as size type. **/
		typedef typename util_type::size_type		size_type;
		/** Defines an AttributeUtil difference type as difference type. **/
		typedef typename util_type::difference_type	difference_type;
		/** Defines a constant value type as a constant reference. **/
		typedef const value_type&					const_reference;
		/** Defines a constant value type as a constant reference. **/
		typedef value_type&							reference;
		
	public:
		/** Sets the value of an attribute.
		 * 
		 * @param attr an attribute.
		 * @param val a value.
		 */
		static void setValue(attribute_type& attr, const value_type& val)
		{
			valueOf(attr) = val;
		}
		/** Sets the index of an attribute.
		 * 
		 * @param attr an attribute.
		 * @param idx an index.
		 */
		static void setIndex(attribute_type& attr, const index_type& idx)
		{
			util_type::indexOf(attr) = idx;
		}
		/** Gets the value of an attribute.
		 * 
		 * @param attr an attribute.
		 * @return a value.
		 */
		static const value_type getValue(const attribute_type& attr)
		{
			return valueOf(attr);
		}
		/** Gets the index of an attribute.
		 * 
		 * @param attr an attribute.
		 * @param i an index.
		 * @return an index.
		 */
		static const index_type getIndex(const attribute_type& attr, index_type i)
		{
			return util_type::indexOf(attr);
		}
		/** Gets the index of an attribute.
		 * 
		 * @param attr an attribute.
		 * @return an index.
		 */
		static const index_type getIndex(const attribute_type& attr)
		{
			return util_type::indexOf(attr);
		}
		/** Converts an attribute to a value.
		 *
		 * @param attr a constant reference to an attribute type.
		 * @return a constant reference to an value type.
		 */
		static const value_type& valueOf(const attribute_type& attr)
		{
			return util_type::valueOf(attr);
		}
		/** Converts an attribute to a value.
		 *
		 * @param attr a reference to an attribute type.
		 * @return a reference to an value type.
		 */
		static value_type& valueOf(attribute_type& attr)
		{
			return util_type::valueOf(attr);
		}
		/** Assigns an sparse index and value to a sparse attribute.
		 *
		 * @param px a pointer to an attribute array.
		 * @param n an index.
		 * @param val a constant reference to a value.
		 * @param idx the current index of the value.
		 */
		static void set_value_index(pointer px, size_type n, const value_type& val, const size_type idx)
		{
			util_type::valueOf(px[n]) = val;
			util_type::indexOf(px[n]) = idx;
		}
		/** Assigns an sparse index and value to a sparse attribute.
		 *
		 * @param px a pointer to an attribute array.
		 * @param n an index.
		 * @param val a constant reference to a value.
		 * @param idx the current index of the value.
		 */
		/*static pointer set_value_index(pointer pbeg, pointer pcur, const value_type& val, size_type idx)
		{
			util_type::valueOf(*pcur) = val;
			util_type::indexOf(*pcur) = idx+1;
			return pcur;
		}*/
		/** Gets the value of a sparse attribute. 
		 * 
		 * It returns zero if the current index m does not match n+1. If
		 * the current index, m, matches n+1, then m is incremented.
		 *
		 * @param px a constant pointer to the attribute array.
		 * @param n the index of the sparse attribute.
		 * @param m a reference to the current sparse attribute.
		 * @return value of the attribute.
		 */
		static const_reference valueOf(const_pointer px, size_type n, size_type& m)
		{
			static value_type zero=0;
			if( index_type(n+1) == util_type::indexOf(px[m]) )
			{
				m++;
				return util_type::valueOf(px[m-1]);
			}
			return zero;
		}
		/** Tests if the last attribute has been reached.
		 *
		 * @param px a contant pointer to an attribute array.
		 * @param n the current index.
		 * @param len the length of the range.
		 * @return true if the sparse index is not -1.
		 */
		static bool isnotend(const_pointer px, size_type n, size_type len)
		{
			return util_type::indexOf(px[n]) != -1;
		}
		/** Get the maximum index.
		 *
		 * @param px a contant pointer to an attribute array.
		 * @param n the current index.
		 * @param m the maximum index.
		 * @return the index of sparse attribute.
		 */
		static size_type max(const_pointer px, size_type n, size_type m)
		{
			return util_type::indexOf(px[n-1]);
		}
	};

	/** @brief Helper function to test if attribute is missing.
	 * 
	 * Tests if the given value matches the internal representation of 
	 * a missing value.
	 *
	 * @param val a value to test.
	 * @return true if value is missing.
	 */
	template<class T>
	bool is_attribute_missing(T val)
	{
		return AttributeUtil<T>::missing() == AttributeUtil<T>::valueOf(val);
	}

};

/** @brief Define a type utility for an std::pair.
 * 
 * This specialized class template defines a std::pair<T,int> as
 * a pod type and assigns a name.
 */
template<class T>
struct TypeUtil< std::pair<T,int> >
{
	/** Defines a pair(T,int) as a value type. **/
	typedef std::pair<T,int> value_type;
	/** Flags the class as a non-primative. **/
	enum{ispod=true};
	/** Gets the name of the type.
	 *
	 * @return std::pair<T,int>
	*/
	static const char* name() 
	{
		return "std::pair<T,int>";
	}
};


/** @brief Define a type utility for an std::pair.
 * 
 * This specialized class template defines a std::pair<T,int> as
 * a pod type and assigns a name.
 */
template<class T>
struct TypeUtil< std::pair<T,long unsigned int> >
{
	/** Defines a pair(T,int) as a value type. **/
	typedef std::pair<T,unsigned int> value_type;
	/** Flags the class as a non-primative. **/
	enum{ispod=true};
	/** Gets the name of the type.
	 *
	 * @return std::pair<T,unsigned int>
	*/
	static const char* name() 
	{
		return "std::pair<T,unsigned int>";
	}
};

#endif


