/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AttributeVector.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ATTRIBUTEVECTOR_H
#define _EXEGETE_ATTRIBUTEVECTOR_H
#include "AttributeTypeUtil.h"

/** @file AttributeVector.h
 * @brief Defines a vector of attribute values.
 * 
 * This file contains the AttributeVector class, which defines a collection
 * of attribute values.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{
	/** @brief Defines a vector of attribute values.
	 * 
	 * This class defines an interface to a collection of attribute values.
	 */
	template<class X>
	class AttributeVector
	{
	public:
		/** Defines an AttributeTypeUtil as a type utility. **/
		typedef AttributeTypeUtil<X>					type_util;
		/** Defines a type util value as a value type. **/
		typedef typename type_util::value_type			value_type;
		/** Defines a type util attribute as an attribute type. **/
		typedef typename type_util::attribute_type		attribute_type;
		/** Defines a type util index as an index type. **/
		typedef typename type_util::index_type			index_type;
		/** Defines a type util pointer as a pointer. **/
		typedef typename type_util::pointer				pointer;
		/** Defines a type util constant pointer as a constant pointer. **/
		typedef typename type_util::const_pointer		const_pointer;
		/** Defines a type util constant reference as a constant reference. **/
		typedef typename type_util::const_reference		const_reference;
		/** Defines a type util reference as a reference. **/
		typedef typename type_util::reference			reference;
		/** Defines a type util size as a size type. **/
		typedef typename type_util::size_type			size_type;
		/** Defines a type util size as a size type. **/
		typedef typename type_util::difference_type		difference_type;
		/** Defines a pointer as an iterator. **/
		typedef typename type_util::pointer				iterator;
		/** Defines a pointer as an iterator. **/
		typedef typename type_util::const_pointer		const_iterator;
	public:
		/** Constructs an attribute vector.
		 *
		 * @param p a pointer to an attribute vector.
		 */
		AttributeVector(pointer p=0) : xbeg(p)
		{
		}
		/** Destructs an attribute vector.
		 */
		~AttributeVector()
		{
		}

	public:
		/** Constructs a shallow copy of an attribute vector.
		 *
		 * @param av the attribute vector to copy.
		 */
		AttributeVector(const AttributeVector& av) : xbeg(av.xbeg)
		{
		}
		/** Assigns a shallow copy of a concrete attribute vector.
		 *
		 * @param av the attribute vector to copy.
		 * @return a reference to the current object.
		 */
		AttributeVector& operator=(const AttributeVector& av)
		{
			xbeg = av.xbeg;
			return *this;
		}
		/** Constructs a shallow copy of an attribute vector.
		 *
		 * @warning No copied made.
		 * 
		 * @param av attribute vector to copy.
		 */
		template<class X1>
		AttributeVector(const AttributeVector<X1>& av) : xbeg(0)
		{
		}
		/** Assigns a shallow copy of a concrete attribute vector.
		 * 
		 * @warning No copied made.
		 *
		 * @param av the attribute vector to copy.
		 * @return a reference to the current object.
		 */
		template<class X1>
		AttributeVector& operator=(const AttributeVector<X1>& av)
		{
			return *this;
		}
		/** Assigns a shallow copy of a pointer to attributes.
		 *
		 * @param p the attribute pointer to copy.
		 * @return a reference to the current object.
		 */
		AttributeVector& operator=(pointer p)
		{
			xbeg = p;
			return *this;
		}
		/** Shallow copy of the attribute vector. 
		 * 
		 * @note It sets the parameter vector to NULL.
		 *
		 * @param av an attribute vector.
		 */
		void shallow(AttributeVector& av)
		{
			xbeg = av.xbeg;
			av.xbeg=0;
		}

	public:

		/** Tests if the index has reached the last attribute.
		 *
		 * @param n the current index.
		 * @param len the size of the range.
		 * @return true if n reaches the size of the range.
		 */
		bool isnotend(size_type n, size_type len)const
		{
			return type_util::isnotend(xbeg, n, len);
		}
		/** Gets a constant reference to the value of an attribute at 
		 * the specified index.
		 *
		 * @param n the current index.
		 * @return a constant reference to the value.
		 */
		const_reference valueAt(size_type n)const
		{
			return type_util::valueOf(xbeg[n]);
		}
		/** Gets a reference to the value of an attribute at the 
		 * specified index.
		 *
		 * @param n the current index.
		 * @return a reference to the value.
		 */
		value_type& valueAt(size_type n)
		{
			return type_util::valueOf(xbeg[n]);
		}
		/** Gets a constant reference to the value of an attribute at 
		 * the specified index.
		 *
		 * @param n the current index.
		 * @param m a reference to the actual position.
		 * @return a constant reference to the value.
		 */
		const_reference valueAt(size_type n, size_type& m)const
		{
			return type_util::valueOf(xbeg, n, m);
		}
		/** Gets an index of the sparse attribute at the specified index.
		 *
		 * @param n the current index.
		 * @param i an index.
		 * @return the index of the attribute.
		 */
		index_type getIndexAt(size_type n)const
		{
			return type_util::getIndex(xbeg[n], n);
		}
		/** Gets an index of the sparse attribute at the specified index.
		 *
		 * @param n the current index.
		 * @return the index of the attribute.
		 */
		index_type indexAt(size_type n)const
		{
			return type_util::indexOf(xbeg[n]);
		}
		/** Gets an index of the sparse attribute at the specified index.
		 *
		 * @param n the current index.
		 * @return a reference to the index of the attribute.
		 */
		index_type& indexAt(size_type n)
		{
			return type_util::indexOf(xbeg[n]);
		}
		/** Gets the max index of the attribute, used to find the number 
		 * attributes for a sparse attribute vector.
		 *
		 * @param n the sparse index.
		 * @param m the position.
		 * @return maximum attribute index.
		 */
		size_type maxIndex(size_type n, size_type m)const
		{
			return type_util::max(xbeg, n, m);
		}

	public:
		/** Gets a pointer to the first attribute.
		 * 
		 * @return pointer to first attribute.
		 */
		pointer begin()
		{
			return xbeg;
		}
		/** Gets a pointer to the last attribute.
		 * 
		 * @return pointer to first attribute.
		 */
		pointer end()
		{
			return xbeg;
		}
		/** Gets a pointer to the first attribute.
		 * 
		 * @return pointer to first attribute.
		 */
		operator const_pointer()const
		{
			return x();
		}
		/** Gets a pointer to the first attribute.
		 * 
		 * @return reference to pointer to first attribute.
		 */
		pointer& x()
		{
			return xbeg;
		}
		/** Gets a pointer to the first attribute.
		 * 
		 * @return reference to pointer to first attribute.
		 */
		const_pointer x()const
		{
			return xbeg;
		}
		/** Sets a pointer to the first attribute.
		 * 
		 * @param ptr sets a to pointer to the first attribute.
		 */
		void x(pointer ptr)
		{
			xbeg = ptr;
		}
		/** Gets the built-in attribute weight of the example.
		 *
		 * @return weight on example.
		 */
		value_type attr_weight()const
		{
			ASSERT(xbeg != 0);
			return type_util::valueOf(*(xbeg-1));
		}
		/** Gets the built-in attribute weight of the example.
		 *
		 * @return reference to weight on example.
		 */
		value_type& attr_weight()
		{
			ASSERT(xbeg != 0);
			return type_util::valueOf(*(xbeg-1));
		}

	public:
		/** Gets the internal missing attribute flag.
		 *
		 * @return the internal missing attribute flag.
		 */
		static value_type missing()
		{
			return type_util::missing();
		}
		
	protected:
		/** Pointer to attribute array. **/
		pointer xbeg;
	};

};


#endif



