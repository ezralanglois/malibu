/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ConcreteExampleSet.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_CONCRETEEXAMPLESET_H
#define _EXEGETE_CONCRETEEXAMPLESET_H
#include "ExampleSet.h"
#include <map>

/** @file ConcreteExampleSet.h
 * @brief Defines a concrete example set that handles all memory.
 * 
 * This file contains the ConcreteExampleSet class template.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a concrete example set that handles all memory.
	 * 
	 * This class comprises a concrete set of examples and, if present, a set 
	 * of bags. The attribute vector of every example is allocated and 
	 * destroyed in this class.
	 */
	template<class X, class Y>
	class ConcreteExampleSet : public ExampleSet<X,Y,std::string*>
	{
	public:
		/** Defines an ExampleSet as a parent type. **/
		typedef ExampleSet<X,Y,std::string*>				parent_type;
		/** Defines a collection of attribute column descriptions as a attribute header. **/
		typedef typename parent_type::attribute_header		attribute_header;
		/** Defines an attribute column description as a header type. **/
		typedef typename attribute_header::header_type		header_type;
	public:
		/** Defines an example vector. **/
		typedef typename parent_type::example_vector		example_vector;
		/** Defines a bag vector. **/
		typedef typename parent_type::bag_vector			bag_vector;
	public:
		/** Defines an Example as a value type. **/
		typedef typename example_vector::value_type			value_type;
		/** Defines a reference to an example. **/
		typedef typename example_vector::reference			reference;
		/** Defines an iterator to an example. **/
		typedef typename example_vector::iterator			iterator;
		/** Defines a constant iterator to an example. **/
		typedef typename example_vector::const_iterator		const_iterator;
	public:
		/** Defines a bag type. **/
		typedef typename bag_vector::value_type				bag_type;
		/** Defines a reference to a bag. **/
		typedef typename bag_vector::reference				bag_reference;
		/** Defines an iterator to a bag. **/
		typedef typename bag_vector::iterator				bag_iterator;
		/** Defines a constant iterator to a bag. **/
		typedef typename bag_vector::const_iterator			const_bag_iterator;
	public:
		/** Defines an example type utility. **/
		typedef typename example_vector::type_util			type_util;
		/** Defines a size type of an example. **/
		typedef typename example_vector::size_type			size_type;
		/** Defines a difference type of an example. **/
		typedef typename example_vector::difference_type	difference_type;
		/** Defines a class type. **/
		typedef typename example_vector::class_type			class_type;
		/** Defines an example attribute type. **/
		typedef typename example_vector::attribute_type		attribute_type;
		/** Defines an example value type as a feature type. **/
		typedef typename value_type::value_type				feature_type;
		/** Defines an example attribute pointer. **/
		typedef typename value_type::pointer				attribute_pointer;
		/** Defines an example constant attribute pointer. **/
		typedef typename value_type::const_pointer			const_attribute_pointer;
		/** Defines an example index type. **/
		typedef typename example_vector::index_type			index_type;
		/** Defines a constant label reference. **/
		typedef typename attribute_header::const_label_reference const_label_reference;
		/** Defines a label type. **/
		typedef typename attribute_header::label_type		label_type;
	public:
		/** Constructs a default concrete example set.
		 */
		ConcreteExampleSet() : nattr(0), maxnattr(0), lenPtr(0), aoffset(1)
		{
		}
		/** Destructs a concrete example set. Clears all memory including
		 * the memory for each attribute vector.
		 */
		~ConcreteExampleSet()
		{
			clear();
#ifdef _SPARSE
			erase(lenPtr);
#endif
		}
		
	public:
		/** Assigns a concrete example set making a deep copy of the
		 * attribute description collection. It also clears all the
		 * attributes.
		 * 
		 * @param ex a concrete example set to copy.
		 * @return a reference to this object.
		 */
		ConcreteExampleSet& operator=(const ConcreteExampleSet& ex)
		{
			clear();
			if( ex.attributeCount() > 0 ) attribute_header::deep_copy(ex);
			return *this;
		}
		/** Assigns a concrete example set making a deep copy of the
		 * attribute description collection. It also clears all the
		 * attributes.
		 * 
		 * @param ex a concrete example set to copy.
		 * @return a reference to this object.
		 */
		template<class W1>
		ConcreteExampleSet& operator=(const ExampleSet<X,Y,W1>& ex)
		{
			clear();
			attribute_header::deep_copy(ex);
			return *this;
		}
		/** Assigns a concrete example set making a deep copy of the
		 * attribute description collection. It also clears all the
		 * attributes.
		 * 
		 * @param ex a concrete example set to copy.
		 * @return a reference to this object.
		 */
		void copy(const ConcreteExampleSet& ex)
		{
			clear();
			attribute_header::deep_copy(ex);
		}

	public:
		/** Fills in the average value of the column for each missing value.
		 */
		void fillinmissing()
		{
			typedef feature_type float_type;
			float_type avg, inv = 1.0f / float_type(parent_type::size());
			for(unsigned int i=0;i<parent_type::attributeCount();++i)
			{
				avg = 0.0f;
				for(iterator beg = parent_type::begin(), end=parent_type::end();beg != end;++beg) avg += beg->x()[i];
				avg *= inv;
				for(iterator beg = parent_type::begin(), end=parent_type::end();beg != end;++beg)
					if( beg->x()[i] == value_type::missing() ) beg->x()[i] = avg;
			}			
		}
		/** Resizes all memory collections in the concrete example set. It also
		 * initializes every pointer to zero and ensures the first attribute
		 * in each attribute vector is the corresponding index. The pointer is 
		 * incremented past this value.
		 * 
		 * @note This increment past the first value must be handled.
		 * 
		 * @param len number of examples.
		 * @param attrn number of attributes.
		 * @param cl number of classes.
		 * @param bags number of bags.
		 */
		void resizeall(size_type len, size_type attrn, size_type cl, size_type bags)
		{
			parent_type::resize(len);
			resizeAttributes(attrn);
			parent_type::classHeader().resize(cl);
			parent_type::classCount(cl);
			if(bags>0)parent_type::resize_bags(bags);
			for(iterator beg = parent_type::begin(), end=parent_type::end();beg != end;++beg)
			{
				beg->x(0);
				beg->label(0);
				beg->x(::resize(beg->x(), attribute_header::attributeCount()+1+type_util::IS_SPARSE));
				beg->valueAt(0) = feature_type(std::distance(parent_type::begin(), beg));
				beg->x(beg->x()+1);
			}
			for(bag_iterator beg = parent_type::bag_begin(), end=parent_type::bag_end();beg != end;++beg)
			{
				beg->label(0);
			}
		}
		/** Resizes example, bag and attriute memory collections in the concrete 
		 * example set. It also initializes every pointer to zero and ensures the 
		 * first attribute in each attribute vector is the corresponding index. The 
		 * pointer is incremented past this value.
		 * 
		 * @note This increment past the first value must be handled.
		 * 
		 * @param len number of examples.
		 * @param bags number of bags.
		 */
		void resize_body(size_type len, size_type bags)
		{
			parent_type::resize(len);
			if( bags>0 ) parent_type::resize_bags(bags);
			for(iterator beg = parent_type::begin(), end=parent_type::end();beg != end;++beg)
			{
				beg->x(0);
				beg->label(0);
				beg->x(::resize(beg->x(), 1));
				beg->valueAt(0) = feature_type(std::distance(parent_type::begin(), beg));
				beg->x(beg->x()+1);
			}
			for(bag_iterator beg = parent_type::bag_begin(), end=parent_type::bag_end();beg != end;++beg)
			{
				beg->label(0);
			}
		}
		/** Reallocates the memory for attribute description header.
		 * 
		 * @param len number of attributes.
		 */
		void resizeAttributes(size_type len)
		{
			attribute_header::resizeHeader(len+1+attribute_header::labelCountInt, attribute_header::labelCountInt);
		}
		/** Reallocates the memory for attribute description header.
		 * 
		 * @param len number of attributes and class label and label count.
		 */
		void resizeHeader(size_type len)
		{
			attribute_header::resizeHeader(len, attribute_header::labelCountInt);
		}
		/** Reallocates the memory for attribute description header.
		 * 
		 * @param len number of attributes.
		 * @param cl number of classes.
		 */
		void resize_header(size_type len, size_type cl)
		{
			attribute_header::resizeHeader(len+1+attribute_header::labelCountInt, attribute_header::labelCountInt);
			attribute_header::classHeader().resize(cl);
		}
		/** Allocates memory for the attribute description header.
		 *
		 * @param len number of attributes, class attribute, and labels.
		 * @param lbs number of labels.
		 */
		void setupHeader(size_type len, size_type lbs)
		{
			if( lbs == 0 ) 
			{
				lbs = 1;
				len ++;
			}
			attribute_header::resizeHeader(len, lbs);
			maxnattr = 0;
			attribute_header::sparseCountInt = 0;
		}
		/** Tests if the class label exists in the collection.
		 *
		 * @param str a class label.
		 * @return true if the class label is found.
		 */
		bool hasClass(const_label_reference str)const
		{
			return attribute_header::classHeader().contains(str);
		}
		/** Tests if the specified column as the specified nominal value.
		 *
		 * @param str a nominal label.
		 * @param n an attribute index.
		 * @return true if the nominal value was found.
		 */
		bool hasNominal(const_label_reference str, size_type n)const
		{
			if(n >= attribute_header::attributeCount()) return false;
			return attribute_header::attributeAt(n).contains(str);
		}
		/** Inserts a label into the attribute description header.
		 *
		 * @param str an attribute column header name.
		 * @param n index of the attribute column header.
		 */
		void insertHeader(const_label_reference str, size_type n)
		{
			if( n < attribute_header::labelCount() )
			{
				attribute_header::headerAt(n) = str;
			}
			else
			{
				attribute_header::headerAt(n+1) = str;
			}
		}
		/** Inserts a label as the class column header.
		 *
		 * @param str an attribute header name.
		 */
		void insertHeaderClass(const_label_reference str)
		{
			attribute_header::classHeader() = str;
		}
		/** Assign all attribute column headers where the default values 
		 * are put for class and labels.
		 *
		 * @param beg an iterator to start of attribute names.
		 * @param end an iterator to end of attribute names.
		 * @param n number of labels.
		 */
		template<class I>
		void assign_header(I beg, I end, size_type n)
		{
			if( n == 0 ) n = 1;
			setupHeader(end-beg+n+1, n);
			insertHeaderClass("Class");
			unsigned int i=0;
			for(i=0;i<n;++i) insertHeader("Label", i);
			for(;beg != end;++beg, ++i) attribute_header::headerAt(i+1) = *beg;
		}
		/** Assigns class labels to attribute description header.
		 *
		 * @param beg an iterator to start of class labels.
		 * @param end an iterator to end of class labels.
		 */
		template<class I>
		void assign_classes(I beg, I end)
		{
			for(;beg != end;++beg) attribute_header::classHeader().push_back(*beg);
		}

	public:
		/** Inserts a label into an example at a specific index.
		 *
		 * @param ex an example.
		 * @param val a string label.
		 * @param n a label index.
		 */
		void insertLabel(reference ex, const_label_reference val, size_type n)
		{
			ASSERT(ex.label() != 0);
			ex.labelAt(n) = val;
		}
		/** Sets the class value of an example. If label is not found the
		 * add the label otherwise set the example with a missing class 
		 * value.
		 *
		 * @param ex an example to set/add class.
		 * @param val a string value of the attribute.
		 * @param add should add label. 
		 * @param mch an internal missing class code.
		 */
		void insertClass(reference ex, const_label_reference val, bool add, char mch)
		{
			if( val[0] == mch ) ex.y(value_type::missingClass());
			else ex.y(attribute_header::classHeader().valueOf(val, value_type::missingClass(), add));
			//ASSERTMSG(ex.y()==0 || ex.y()==1, ex.y());
		}
		/** Inserts a nominal attribute value into the attribute description column.
		 *
		 * @param val a string value of a nominal attribute.
		 * @param n sparse index.
		 * @param add should add a nominal label to the attribute header.
		 * @param mch a missing character flag.
		 * @return the internal value for the nominal label.
		 */
		feature_type insertNominal(const_label_reference val, size_type n, bool add, char mch)
		{
			ASSERTMSG(n < attribute_header::attributeCount(), n << " < " << attribute_header::attributeCount() << " " << val );
			if( val[0] == mch ) return value_type::missing();
			if( attribute_header::attributeCountInt <= n ) attribute_header::resizeHeader(n+attribute_header::labelCountInt+2, attribute_header::labelCountInt);
			return attribute_header::attributeAt(n).valueOf(val, value_type::missing(), add);
		}
		/** Adds a single example to the example set.
		 */
		void addSingleExample()
		{	
			value_type ex;
			ex.label(::setsize(ex.label(), attribute_header::labelCountInt));
			ex.x(::resize(ex.x(), attribute_header::attributeCount()+1));
			ex.x(ex.x()+1);
			parent_type::append(&ex);
		}
		/** Resize then number of attributes and labels in an example.
		 *
		 * @param ex an example.
		 */
		void setup(reference ex)
		{
			ASSERT(ex.label() == 0);
			ASSERT(ex.x() == 0);
			ASSERT( attribute_header::labelCountInt > 0 );
			ex.label(::setsize(ex.label(), attribute_header::labelCountInt));
			if(nattr>0) ex.x(::setsize(ex.x(), nattr+1));
		}
		/** Resize the number of attributes in an example.
		 *
		 * @param ex an example.
		 * @param n a sparse index.
		 * @param m the actual index.
		 * @param g growth size.
		 */
		void resizeAttributes(reference ex, size_type n, size_type m, size_type g)
		{
			n = type_util::IS_SPARSE?(m+1):n;
			ex.x(::resize(ex.x(), nattr, n+1, g));
		}
		/** Inserts an attribute value into an example.
		 *
		 * @param ex an example.
		 * @param val an attribute value.
		 * @param n a sparse index.
		 * @param m the actual index.
		 */
		void insert_value_index(reference ex, feature_type val, size_type n, size_type& m)
		{
			type_util::set_value_index(ex.x()+1, m, val, n);
		}
		/** Appends an example to the dataset.
		 * 	- Resize attributes
		 * 	- Determine max number of attributes
		 * 	- Setup example (increment first value)
		 * 	- Add length to vector
		 *
		 * @param ex an example to append.
		 * @param n the final attribute count.
		 */
		void append(reference ex, size_type n)
		{
			if( type_util::IS_SPARSE )
			{
				if(n>nattr)ex.x(::resize(ex.x(), n+2));
				type_util::marklast(ex.x(), n);
			}
			nattr = n;
			nattr = type_util::max(ex.x(), nattr, nattr);
			maxnattr = std::max(maxnattr, nattr);
			type_util::valueOf(*ex.x()) = feature_type(parent_type::size());
			ex.x(ex.x()+1);
			parent_type::append(&ex);
			ex.x(0);
			ex.label(0);
			lenPtr = ::resize(lenPtr, parent_type::capacity());
			lenPtr[parent_type::size()-1] = n;
			attribute_header::sparseCountInt = std::max(n, attribute_header::sparseCountInt);
#ifdef _SPARSE
			ASSERTMSG( type_util::indexOf(((parent_type::end()-1))->x()[n-1]) == -1, type_util::indexOf(((parent_type::end()-1))->x()[n]) << ", " << type_util::indexOf(((parent_type::end()-1))->x()[n-1]) );
#endif
		}
		/** Finalize the examples added to the dataset.
		 * 	- Insert the class column
		 * 	- Insert label columns
		 * 	- Insert default example labels
		 * 	- Update dataset statistics
		 *
		 * @param lbs number of labels.
		 */
		void finalize(size_type lbs)
		{
			if( attribute_header::attributeCountInt == 0 || attribute_header::classHeader().name().empty() )
			{
				attribute_header::resizeHeader(maxnattr+1+(lbs==0?1:lbs), lbs==0?1:lbs);
				insertHeaderClass("Class");
				std::string str;
				for(unsigned int i=0;i<attribute_header::headerCount()-1;++i)
				{
					valueToString(i, str);
					if(i < attribute_header::labelCount()) insertHeader("Label"+str, i);
					else insertHeader("Attribute"+str, i);
				}
			}
			if( lbs == 0 )
			{
				std::string str;
				for(unsigned int i=0;i<parent_type::size();++i)
				{
					valueToString(i+1, str);
					ASSERT(parent_type::operator[](i).label() != 0);
					parent_type::operator[](i).labelAt(0) = str;
				}
			}
			parent_type::recalculate(lenPtr, lenPtr+parent_type::size());
			attribute_header::classCount(attribute_header::classHeader().size());
#ifndef _SPARSE
			erase(lenPtr);
#endif
		}
		/** Recalculates attribute statistics.
		 */
		void recalculate()
		{
			iterator beg, end=parent_type::end();
			for(size_type i=0;i<parent_type::attributeCount();++i)
			{
				if( parent_type::attributeAt(i).isNominalConvert() )
				{
					for(beg=parent_type::begin();beg != end;++beg)
					{
						beg->valueAt(i)-=parent_type::attributeAt(i).min();
					}
				}
			}
			parent_type::recalculate();
		}

	public:
		/** Sets a bag to a given range over a collection of examples.
		 * 
		 * @param bag bag index.
		 * @param b start of range.
		 * @param len length of range.
		 */
		void setupbag(size_type bag, size_type b, size_type len)
		{
			ASSERT(parent_type::bag_begin() != 0);
			ASSERT(bag < parent_type::bagCount());
			ASSERT( b < parent_type::size() );
			ASSERT( (b+len) <= parent_type::size() );
			parent_type::bag_begin()[bag].begin(parent_type::begin()+b);
			parent_type::bag_begin()[bag].end(parent_type::begin()+b+len);
		}
		/** Resizes the attributes in each example, more examples will be set to zero.
		 *
		 * @param n a new size.
		 */
		void attribute(size_type n)
		{
			if( n == 0 ) return;
			size_type m = attribute_header::attributeCountInt+n;
			for(iterator ebeg=parent_type::begin(), eend=parent_type::end();ebeg != eend;++ebeg)
			{
				ebeg->x(ebeg->x()-aoffset);
				ebeg->x(::resize(ebeg->x(), m+aoffset));
				ebeg->x(ebeg->x()+aoffset);
				for(size_type i=attribute_header::attributeCountInt;i<m;++i) type_util::valueOf(ebeg->x()[i]) = 0;
			}
		}
		/** Get the number of attributes for example at a specific index.
		 * 
		 * @param i index of example.
		 * @return number of attributes for example.
		 */
		size_type attributeCountAt(size_type i)const
		{
			if(lenPtr==0) return attribute_header::attributeCountInt;
			ASSERT(i<parent_type::size());
			return lenPtr[i];
		}
		/** Resize an example safely.
		 * 
		 * @param ex an example.
		 * @param old its old length.
		 * @param len its new length.
		 */
		void resize_example(reference ex, size_type old, size_type len)
		{
			ASSERT(len > old);
			ex.x(ex.x()-aoffset);
			ex.x(::resize(ex.x(), len+aoffset));
			ex.x(ex.x()+aoffset);
#ifndef _SPARSE
			for(size_type i=old;i<len;++i) type_util::valueOf(ex.x()[i]) = 0;
#endif
		}
		/** Clears all allocated memory in concrete example set.
		 */
		void clear()
		{
			for(bag_iterator it = parent_type::bag_begin(), fin = parent_type::bag_end();it != fin;++it) ::erase(it->label());
			for(iterator it = parent_type::begin(), fin=parent_type::end();it != fin;++it) 
			{
				::erase(it->label());
				::erase((it->x()-=aoffset));
			}
			parent_type::clear();
			attribute_header::clear();
		}
		/** Gets the maximum attribute length.
		 *
		 * @return maximum attribute length.
		 */
		size_type attributeMax()const
		{
			return maxnattr;
		}
		/** Gets the current attribute length.
		 *
		 * @return current attribute length.
		 */
		size_type attributeLength()const
		{
			return nattr;
		}
		/** Gets a string summary of the dataset.
		 *
		 * @param prefix a prefixing comment.
		 * @return a string representation of the dataset summary.
		 */
		std::string toString(std::string prefix)
		{
			return toString(0, prefix);
		}
		/** Gets a string summary of the dataset.
		 *
		 * @param type amount of information.
		 * @param prefix a prefixing comment.
		 * @return a string representation of the dataset summary.
		 */
		std::string toString(int type=0, std::string prefix="")
		{
			std::ostringstream out;
			if( parent_type::bagCount() > 0 ) 
			out << prefix << "Bags:       " << parent_type::bagCount() << "\n";
			out << prefix << "Examples:   " << parent_type::size() << "\n";
			out << prefix << "Attributes: " << parent_type::attributeCount() << "\n";
			out << prefix << "Classes:    " << parent_type::classCount() << "\n";
			out << prefix << "Missing:    " << parent_type::missing() << "\n";
			out << prefix << "Zeros:      " << parent_type::zeros() << "\n";
			out << prefix << "Binary:     " << parent_type::countType(header_type::BINARY) << "\n";
			out << prefix << "Discreet:   " << parent_type::countType(header_type::DISCREET) << "\n";
			out << prefix << "Numeric:    " << parent_type::countType(header_type::NUMERIC) << "\n";
			out << prefix << "Nominal:    " << parent_type::countType(header_type::NOMINAL) << "\n";
			if( parent_type::classCount() == 2 )
			out << prefix << "Positive:   " << parent_type::classAt(1) << "\n";
			if( type_traits<Y>::is_discreet )
			{
				for(unsigned int i=0;i<parent_type::classCount();++i)
				{
					out << prefix << parent_type::classAt(i) << ": " << parent_type::countClass(i);
					if( parent_type::bagCount() > 0 ) out << " & " << parent_type::countBagClass(i) << "\n";
					else out << "\n";
				}
			}
			if( type == 1 )
			{
				for(unsigned int i=0;i<parent_type::attributeCount();++i)
				{
					out << prefix << parent_type::attributeAt(i).toString();
					if( parent_type::attributeAt(i).isnominal() )
					{
						out << ": " << parent_type::attributeAt(i)[0];
						for(unsigned int j=1;j<parent_type::attributeAt(i).size();++j)
							out << ", " << parent_type::attributeAt(i)[j];
					}
					out << "\n";
				}
			}
			if( type == 2 )
			{
				for(const_bag_iterator bbeg=parent_type::bag_begin(), bend=parent_type::bag_end();bbeg != bend;++bbeg)
				{
					out << prefix << bbeg->labelAt(0) << " " << bbeg->y() << "\n";
				}
			}
			out << std::endl;
			return out.str();
		}
		/** Builds a collection of bags based on the label. It uses the label index, and 
		 * substring beg and end to find the unique bag identifier.
		 *
		 * @param bagIdx index in label vector to find unique bag id.
		 * @param bagBeg start of unique bag id substring.
		 * @param bagEnd end of unique bag id substring.
		 * @return error message or NULL.
		 */
		const char* build_bags(size_type bagIdx, size_type bagBeg, size_type bagEnd)
		{
			typename bag_map::iterator it;
			bag_map bagmap;
			std::string str, last;
			iterator ebeg, eend;

			for(ebeg = parent_type::begin(), eend=parent_type::end();ebeg != eend;++ebeg)
			{
				str = ebeg->labelAt(bagIdx);
				if( bagEnd > bagBeg ) str = str.substr(bagBeg, (bagEnd-bagBeg));
				if( (it=bagmap.find(str)) != bagmap.end() ) it->second++;
				else bagmap.insert(std::make_pair(str, 1));
			}
			parent_type::bagVec.resize(bagmap.size());
			bag_iterator bbeg=parent_type::bag_begin();
			bag_iterator bend=parent_type::bag_end();
			class_type cl=0;
			for(ebeg = parent_type::begin(), eend=parent_type::end();ebeg != eend;++ebeg)
			{
				str = ebeg->labelAt(bagIdx);
				if( bagEnd > bagBeg ) str = str.substr(bagBeg, (bagEnd-bagBeg));
				if( str != last ) 
				{
					if( !last.empty() ) 
					{
						bbeg->y( cl );
						bbeg->end(ebeg);
						++bbeg;
						if( bbeg == bend ) return ERRORMSG("Error building bags");
						cl = 0;
					}
					last = str;
					bbeg->begin(ebeg);
					bbeg->label(::setsize(bbeg->label(), 1));
					bbeg->labelAt(0) = str;
				}
				if( ebeg->y() > 0 && cl == 0 ) cl = ebeg->y();
			}
			bbeg->y( cl );
			bbeg->end(ebeg);
			return 0;
		}
		/** Builds a collection of bags based on the label. It uses the label index, and 
		 * substring beg and end to find the unique bag identifier.
		 *
		 * @param bagIdx index in label vector to find unique bag id.
		 * @param bagBeg start of unique bag id substring.
		 * @param bagEnd end of unique bag id substring.
		 */
		void buildBags(size_type bagIdx, size_type bagBeg, size_type bagEnd)
		{
			ASSERT(false);
			bag_map bagmap;
			bag_label(bagmap, bagIdx, bagBeg, bagEnd);
			if( bagmap.size() == 0 || bagmap.size() == parent_type::size() ) return;
			parent_type::bagVec.resize(bagmap.size());
			bag_iterator bbeg = parent_type::bag_begin(), bend = parent_type::bag_end();
			copy_bags(bbeg, bend, bagmap.begin(), parent_type::begin());
			resort(bagIdx, bagBeg, bagEnd);
			//class_type neg = negbag(bbeg, bend, 0, 1);
			//negbag(bbeg, bend, 0, 1);
			labelbag(bbeg, bend, 0, 1);
			ASSERT(aligned_bags(bagIdx, bagBeg, bagEnd));
		}
		/** Relabels the examples in the example set. Any example class label found in the
		 * positive label vector is reassigned to the positive class. In the end, there are only 
		 * two classes following the specified labels.
		 *
		 * @param posclasses a vector of positive class labels.
		 * @param pos a new label for the positive class.
		 * @param neg a new label for the negative class.
		 */
		void labelPositive(std::vector<std::string>& posclasses, const_label_reference pos, const_label_reference neg)
		{
			std::stable_sort(posclasses.begin(), posclasses.end());
			std::vector<class_type> classes(parent_type::classCount());
			for(unsigned int i=0;i<classes.size();++i) classes[i] = std::binary_search(posclasses.begin(), posclasses.end(), parent_type::classAt(i))?1:0;
			for(iterator curr=parent_type::begin(), end=parent_type::end();curr != end;++curr) if( curr->y() != value_type::missingClass() ) curr->y( classes[curr->y()] );
			parent_type::classHeader().clear();
			parent_type::classHeader().push_back(neg);
			parent_type::classHeader().push_back(pos);
			parent_type::classCount(2);
		}
		/** Sets the first attribute as the weight.
		 */
		void setupWeights()
		{
			aoffset++;
			attribute_header::labelCountInt++;
			attribute_header::attributeCountInt--;
			for(iterator ebeg=parent_type::begin(), eend=parent_type::end();ebeg != eend;++ebeg) ebeg->x(ebeg->x()+1);
		}
		/** Ensures the dataset has the correct number of:
		 * 	- Classes
		 * 	- Attributes
		 * 	- Examples
		 * 	- Bags
		 * 
		 * @param ensureVec a vector of values in the above order.
		 * @return an error message if one value does not match otherwise NULL.
		 */
		const char* ensure(std::vector<unsigned int>& ensureVec)const
		{
			if( !ensureVec.empty() )
			{
				if( parent_type::classCount() != ensureVec[0] )		return ERRORMSG("Expected " << ensureVec[0] << " classes but only found " << parent_type::classCount() << " classes");
				if( ensureVec.size() > 1 && 
					parent_type::attributeCount() != ensureVec[1] )	return ERRORMSG("Expected " << ensureVec[1] << " attributes but only found " << parent_type::attributeCount() << " attributes");
				if( ensureVec.size() > 2 && 
					parent_type::size() != ensureVec[2] )			return ERRORMSG("Expected " << ensureVec[2] << " examples but only found " << parent_type::size() << " examples");
				if( ensureVec.size() > 3 && 
					parent_type::bagCount() != ensureVec[3] )		return ERRORMSG("Expected " << ensureVec[3] << " bags but only found " << parent_type::bagCount() << " bags");
			}
			return 0;
		}

	private:
		/**
		 * debugging functions
		 */
		void print_label(std::ostream& out, const_iterator beg, const_iterator end)const
		{
			for(;beg != end;++beg)
			{
				if( parent_type::labelCount() > 0 ) out << beg->labelAt(0);
				for(size_type i=1;i<parent_type::labelCount();++i)
					out << " " << beg->labelAt(i);
				out << " " << beg->y() << "\n";
			}
			out << std::endl;
		}
		/** The following functions help assign bags based on example labels.
		 */
		typedef std::map<std::string, unsigned int> bag_map;
		typedef typename bag_map::iterator bagmap_iterator;
		void bag_label(bag_map& bagmap, size_type bagIdx, size_type bagBeg, size_type bagEnd)
		{
			typename std::map<std::string, unsigned int>::iterator it;
			std::string str;
			for(const_iterator beg=parent_type::begin(), end=parent_type::end();beg != end;++beg)
			{
				ASSERT(beg->label() != 0);
				str = beg->labelAt(bagIdx);
				if( bagEnd > bagBeg ) str = str.substr(bagBeg, (bagEnd-bagBeg));
				if( (it=bagmap.find(str)) != bagmap.end() ) it->second++;
				else bagmap.insert(std::make_pair(str, 1));
			}
		}
		void resort(size_type bagIdx, size_type bagBeg, size_type bagEnd)
		{
			iterator cur1, cur2, end=parent_type::end();
			std::string bagstr, insstr;
			for(bag_iterator bbeg=parent_type::bag_begin(), bend=parent_type::bag_end();bbeg != bend;++bbeg)
			{
				ASSERT(bbeg->label() != 0);
				bagstr = bbeg->labelAt(bagIdx);
				for(cur1=bbeg->begin(), cur2=bbeg->end();cur2 != end;++cur2)
				{
					ASSERT(cur2->label() != 0);
					insstr = cur2->labelAt(bagIdx);
					if( bagEnd > bagBeg) insstr = insstr.substr(bagBeg, (bagEnd-bagBeg));
					if( bagstr == insstr )
					{
						do{
							ASSERT(cur1->label() != 0);
							insstr = cur1->labelAt(bagIdx);
							if( bagEnd > bagBeg) insstr = insstr.substr(bagBeg, (bagEnd-bagBeg));
							if( bagstr == insstr ) ++cur1;
						}while( bagstr == insstr );
						std::swap(*cur1, *cur2);
						++cur1;
					}
				}
			}
		}
		bool aligned_bags(size_type bagIdx, size_type bagBeg, size_type bagEnd)const
		{
			std::string bagstr, insstr;
			const_iterator ebeg, eend;
			for(const_bag_iterator beg=parent_type::bag_begin(), end=parent_type::bag_end();beg != end;++beg)
			{
				ASSERT(beg->label() != 0);
				bagstr = beg->labelAt(bagIdx);
				for(ebeg=beg->begin(), eend=beg->end();ebeg != eend;++ebeg)
				{
					ASSERT(ebeg->label() != 0);
					insstr = ebeg->labelAt(bagIdx);
					if( bagEnd > bagBeg) insstr = insstr.substr(bagBeg, (bagEnd-bagBeg));
					if( insstr != bagstr ) return false;
				}
			}
			return true;
		}
		static void copy_bags(bag_iterator beg, bag_iterator end, bagmap_iterator bagIt, iterator ebeg)
		{
			for(;beg != end;++beg, ++bagIt)
			{
				beg->begin(ebeg);
				ebeg+=bagIt->second;
				beg->end(ebeg);
				beg->label(::setsize(beg->label(), 1));
				beg->labelAt(0) = bagIt->first;
			}
		}
		static class_type negbag(const_bag_iterator bbeg, const_bag_iterator bend, class_type neg, class_type pos)
		{
			const_iterator beg,end;
			for(;bbeg != bend;++bbeg)
			{
				beg=bbeg->begin(); end=bbeg->end();
				if( pos == beg->y() ) for(;beg != end;++beg) if( beg->y() != value_type::missingClass() && beg->y() != pos ) return pos;
				else				  for(;beg != end;++beg) if( beg->y() != value_type::missingClass() && beg->y() != neg ) return neg;
			}
			ASSERT(neg == 0);
			return neg;
		}
		static void labelbag(bag_iterator bbeg, bag_iterator bend, class_type neg, class_type pos)
		{
			const_iterator beg, end;
			for(;bbeg != bend;++bbeg)
			{
				beg=bbeg->begin();
				end=bbeg->end();
				ASSERT(beg != end);
				for(;beg != end;++beg) 
				{
					if( beg->y() == value_type::missingClass() ) continue;
					if( beg->y() != neg ) break;
				}
				bbeg->y( (beg == end) ? neg : pos);
			}
		}

	private:
		size_type nattr;
		size_type maxnattr;
		size_type* lenPtr;
		size_type aoffset;
	};

};


#endif

