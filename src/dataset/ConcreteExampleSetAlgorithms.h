/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ConcreteExampleSetAlgorithms.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_CONCRETEEXAMPLESETALGORITHMS_H
#define _EXEGETE_CONCRETEEXAMPLESETALGORITHMS_H
#include "ExampleSetAlgorithms.h"
#include "ConcreteExampleSet.h"
#include "transform/attribute_transfrom.hpp"
#include "transform/attribute_discreetize.hpp"


/** @file ConcreteExampleSetAlgorithms.h
 * @brief ---Remove this file
 * 
 * This file contains a set of global functions that operate on a concrete example set.
 *
 * @todo remove this file after adding all functions to ConcreteExampleSet
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** Performs minmax normalization on a collection of labeled examples. If the flag array
	 * is empty, every attribute is normalized.
	 *
	 * @todo remove this function
	 * 
	 * @param dataset a reference to concrete example set.
	 * @param flag an array of flags set true if the column should be normalized.
	 */
	template<class X, class Y>
	void normalize_minmax(ConcreteExampleSet<X,Y>& dataset, const std::vector<int>& flag)//@todo ymin, ymax
	{
		typedef typename ConcreteExampleSet<X,Y>::value_type	value_type;
		typedef typename ConcreteExampleSet<X,Y>::feature_type	feature_type;
		typedef typename ConcreteExampleSet<X,Y>::iterator		iterator;

		feature_type ymin = 0, ymax = 1.0, xmin, xmax, xmlt, ymlt=(ymax-ymin);
		for(unsigned int i=0;i<dataset.attributeCount();++i)
		{
			if( !flag.empty() && !flag[i] ) continue;
			xmin = dataset.attributeAt(i).min();
			xmax = dataset.attributeAt(i).max();
			xmlt=ymlt/(xmax-xmin);
			for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				if( beg->x()[i] == value_type::missing() ) continue;
				if( beg->x()[i] == xmin ) beg->x()[i] = ymin;
				else if( beg->x()[i] == xmax ) beg->x()[i] = ymax;
				else beg->x()[i] = ymin + (beg->x()[i]-xmin) * xmlt;
			}
		}
		dataset.recalculate();
	}
	/** Performs z-score normalization on a collection of labeled examples.
	 *
	 * @todo remove this function
	 * 
	 * @param dataset a reference to concrete example set.
	 * @param flag an array of flags set true if the column should be normalized.
	 */
	template<class X, class Y>
	void normalize_zscore(ConcreteExampleSet<X,Y>& dataset, const std::vector<int>& flag)//@todo ymin, ymax
	{
		typedef typename ConcreteExampleSet<X,Y>::value_type	value_type;
		typedef typename ConcreteExampleSet<X,Y>::feature_type	feature_type;
		typedef typename ConcreteExampleSet<X,Y>::iterator		iterator;

		feature_type avgval, stdval;
		for(unsigned int i=0;i<dataset.attributeCount();++i)
		{
			if( !flag.empty() && !flag[i] ) continue;
			avgval = 0.0f;
			avgval = average(dataset.begin(), dataset.end(), i, avgval);
			stdval = stddev(dataset.begin(), dataset.end(), i, avgval);
			for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				if( beg->x()[i] == value_type::missing() ) continue;
				beg->x()[i] = ( beg->x()[i] - avgval ) * stdval;
			}
		}		
		dataset.recalculate();
	}

	/** Performs average z-score normalization on a collection of labeled examples.
	 *
	 * @todo remove this function
	 * 
	 * @param dataset a reference to concrete example set.
	 * @param flag an array of flags set true if the column should be normalized.
	 */
	template<class X, class Y>
	void normalize_zscorea(ConcreteExampleSet<X,Y>& dataset, const std::vector<int>& flag)//@todo ymin, ymax
	{
		typedef typename ConcreteExampleSet<X,Y>::value_type	value_type;
		typedef typename ConcreteExampleSet<X,Y>::feature_type	feature_type;
		typedef typename ConcreteExampleSet<X,Y>::iterator		iterator;

		feature_type avgval, stdval;
		for(unsigned int i=0;i<dataset.attributeCount();++i)
		{
			if( !flag.empty() && !flag[i] ) continue;
			avgval = 0.0f;
			avgval = average(dataset.begin(), dataset.end(), i, avgval);
			stdval = avgdev(dataset.begin(), dataset.end(), i, avgval);
			for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				if( beg->x()[i] == value_type::missing() ) continue;
				beg->x()[i] = ( beg->x()[i] - avgval ) * stdval;
			}
		}		
		dataset.recalculate();
	}

	/** Removes columns from the dataset.
	 *
	 * @todo add to dataset and remove
	 * 
	 * @param dataset a reference to concrete example set.
	 * @param columns column indices to remove.
	 * @return an error message or NULL.
	 */
	template<class X, class Y>
	const char* removeColumns(ConcreteExampleSet<X,Y>& dataset, std::vector<std::string>& columns)
	{
		typedef typename ConcreteExampleSet<X,Y>::attribute_pointer	attribute_pointer;
		typedef typename ConcreteExampleSet<X,Y>::iterator			iterator;
		typedef typename ConcreteExampleSet<X,Y>::header_iterator	header_iterator;
		typedef typename ConcreteExampleSet<X,Y>::header_pointer	header_pointer;
		typedef typename ConcreteExampleSet<X,Y>::header_type		header_type;
		typedef typename ConcreteExampleSet<X,Y>::feature_type		feature_type;
		typedef typename ConcreteExampleSet<X,Y>::type_util			type_util;
		typedef typename ConcreteExampleSet<X,Y>::size_type			size_type;
		typedef typename header_type::const_nominal_iterator		const_nominal_iterator;
		typedef std::vector<size_type> 								size_vector;
		if( columns.size() >= dataset.attributeCount() ) return ERRORMSG("Cannot remove every attribute.");
		size_type o = dataset.attributeCount(), tmp;
		size_type n = dataset.attributeCount()-columns.size();
		size_vector cidx(o, 1);
		for(size_type i=0;i<columns.size();++i)
		{
			if( !stringToValue(columns[i], tmp))
			{
				tmp = dataset.attributeIndex(columns[i]);
				if( tmp == dataset.attributeCount()  ) return ERRORMSG("Attribute name not found: \"" << columns[i] << "\"");
			}
			cidx[tmp] = 0;
		}
		for(size_type i=0, j=0;i<o;++i)
		{
			if( cidx[i] )
			{
				cidx[j] = i;
				if( i != j ) dataset.attributeAt(j) = dataset.attributeAt(i);
				j++;
			}
		}
		cidx.resize(n);
		for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
		{
			for(size_type i=0;i<n;++i) beg->x()[i] = beg->x()[cidx[i]];
		}
		dataset.attributeCount(n);
		return 0;
	}
	/** Discreetizes the first dataset and saved it in the second.
	 *
	 * @todo add to dataset then remove
	 * 
	 * @param dataset a real-valued dataset.
	 * @param newset a new discreet dataset.
	 */
	template<class X1, class X2, class Y>
	void discreetize(ConcreteExampleSet<X1,Y>& dataset, ConcreteExampleSet<X2,Y>& newset)
	{
		typedef typename ConcreteExampleSet<X1,Y>::const_iterator	iterator1;
		typedef typename ConcreteExampleSet<X2,Y>::iterator			iterator2;
		typedef typename ConcreteExampleSet<X1,Y>::feature_type		feature_type1;
		typedef typename ConcreteExampleSet<X2,Y>::feature_type		feature_type2;

		iterator1 beg=dataset.begin(), end=dataset.end(), curr;
		iterator2 beg2=newset.begin();
		feature_type1 last=0;
		feature_type2 idx;
		for(curr=beg;curr != end;++curr, ++last) curr->y(last);
		for(unsigned int i=0;i<dataset.attributeCount();++i)
		{
			if( dataset.attributeAt(i).isnumeric() )
			{
				std::stable_sort( beg, end, CompareExample<iterator1>(i) );
				last = beg->x()[i];
				idx = 0;
				for(curr=beg;curr != end;++curr)
				{
					if( curr->x()[i] != last )
					{
						last = curr->x()[i];
						++idx;
					}
					beg2[curr->y()][i] = idx;
				}
			}
			else for(curr=beg;curr != end;++curr) beg2[curr->y()][i] = curr->x()[i];
		}
		newset.recalculate();
	}
	/** Discreetizes every real value in the dataset.
	 * 
	 * @param dataset a reference to concrete example set.
	 */
	template<class X, class Y>
	void discreetizeByMDL(ConcreteExampleSet<X,Y>& dataset, bool use_discreet=true)
	{
		typedef typename ConcreteExampleSet<X,Y>::iterator		iterator;
		typedef typename ConcreteExampleSet<X,Y>::size_type		size_type;
		typedef typename ConcreteExampleSet<X,Y>::feature_type	feature_type;
		typedef std::vector< feature_type > 					feature_vector;
		typedef typename feature_vector::pointer 				feature_pointer;
		feature_vector cuts(dataset.size());
		feature_pointer fbeg = &(*cuts.begin()), fend;
		iterator beg=dataset.begin(), end;
		attribute_discreetize disc(dataset.classCount());

		for(size_type i=0;i<dataset.attributeCount();++i)
		{
			if( dataset.attributeAt(i).isnumeric() || (use_discreet && dataset.attributeAt(i).isdiscreet()) )
			{
				end=sort_no_missing(beg, dataset.end());
				fend=disc.mdl_kononenkos_cut(fbeg, beg, end, i);
				discreetize_range(beg, end, fbeg, fend, i);
			}
		}
		dataset.recalculate();
	}
	/** Converts nominal attributes to binary.
	 *
	 * @todo add to dataset and remove
	 * 
	 * @param dataset a reference to concrete example set.
	 */
	template<class X, class Y>
	void nominalToBinary(ConcreteExampleSet<X,Y>& dataset)
	{
		typedef typename ConcreteExampleSet<X,Y>::attribute_pointer	attribute_pointer;
		typedef typename ConcreteExampleSet<X,Y>::iterator			iterator;
		typedef typename ConcreteExampleSet<X,Y>::header_iterator	header_iterator;
		typedef typename ConcreteExampleSet<X,Y>::header_pointer	header_pointer;
		typedef typename ConcreteExampleSet<X,Y>::header_type		header_type;
		typedef typename ConcreteExampleSet<X,Y>::feature_type		feature_type;
		typedef typename ConcreteExampleSet<X,Y>::type_util			type_util;
		typedef typename header_type::const_nominal_iterator		const_nominal_iterator;

		feature_type val;
		unsigned int attcnt=0;
		for(header_iterator hbeg=dataset.header_begin(), hend=dataset.header_end();hbeg != hend;++hbeg)
		{
			if( hbeg->isnominal() ) attcnt+=(unsigned int)hbeg->size();
			else attcnt++;
		}
		header_pointer new_header = setsize<header_type>(dataset.attributeCount());
		unsigned int count=(unsigned int)dataset.attributeCount();
		std::copy(dataset.header_begin(), dataset.header_end(), new_header);
		dataset.resizeAttributes(attcnt);
		for(unsigned int i=0, j=0;i<count;++i)
		{
			if( new_header[i].isnominal() )
			{
				for(const_nominal_iterator nbeg = new_header[i].begin(), nend=new_header[i].end();nbeg != nend;++nbeg, ++j) 
					dataset.attributeAt(j) = "IS_"+(*nbeg);
			}
			else
			{
				dataset.attributeAt(j) = new_header[i];
				++j;
			}
		}
		for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
		{
			attribute_pointer last = beg->x(), cur1=last;
			attribute_pointer abeg = setsize(abeg, attcnt+1), cur2=abeg+1;++abeg;
			for(header_iterator hbeg=new_header, hend=new_header+count;hbeg != hend;++hbeg, ++cur1)
			{
				val = type_util::valueOf(*cur1);
				if( hbeg->isnominal() )
				{
					for(const_nominal_iterator nbeg = hbeg->begin(), ncurr=nbeg, nend=hbeg->end();ncurr != nend;++ncurr, ++cur2) 
						*cur2 = (feature_type)((val == (feature_type)(std::distance(nbeg, ncurr)) ? 1 : 0) );
				}
				else 
				{
					*cur2 = val;
					++cur2;
				}
			}
			(*beg) = abeg;
			last--;
			erase(last);
		}

		for(header_iterator hbeg=dataset.header_begin(), hend=dataset.header_end();hbeg != hend;++hbeg)
			if( hbeg->isnominal() ) hbeg->clear();
		dataset.recalculate();
	}
};


#endif


