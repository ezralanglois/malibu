/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * DatasetHandler.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_DATASETHANDLER_H
#define _EXEGETE_DATASETHANDLER_H
#include "errorutil.h"
#include "mathutil.h"
#include "ExampleFormat.h"

/** @file DatasetHandler.h
 * @brief Peforms all operations related to dataset input/output.
 * 
 * This file contains the DatasetHandler class.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section datasethandler Dataset Handler
 * 	- strict
 * 		- Description: strict checking of dataset aganist algorithm
 * 		- Type: boolean
 * 		- Viewability: Additional
 */

namespace exegete
{
	/** @brief Peforms all operations related to dataset input/output.
	 * 
	 * A dataset handler goew beyond simple reading and writing of a dataset. It supports: 
	 * 	- reading and writing files to the implicit path
	 * 	- checking whether dataset is supported by learning algorithm
	 * 	- prepares the learning algorithm for the dataset
	 * 	- reads and writes a dataset specification to a learning model.
	 * 
	 * @see ExampleFormat
	 * @see StandardExampleFormat
	 */
	template<class L, class D=typename L::dataset_type>
	class DatasetHandler : public ExampleFormat<typename D::attribute_type, typename D::class_type>
	{
		typedef ExampleFormat<typename D::attribute_type, typename D::class_type> parent_type;
	public:
		/** Defines a concrete example set. **/
		typedef typename parent_type::concrete_type dataset_type;
		/** Defines an example set. **/
		typedef typename parent_type::dataset_type shallowset_type;
	private:
		typedef typename shallowset_type::attribute_type attribute_type;
		typedef typename shallowset_type::class_type class_type;
		typedef typename shallowset_type::attribute_header attribute_header;
		
	public:
		/** Constructs a dataset handler.
		 * 
		 * @param pfx comment prefix for output (e.g. dataset stats).
		 * @param l level for writing dataset statistics.
		 */
		DatasetHandler(const char* pfx, int l=0) : parent_type(pfx, true, l), strictInt(1)
		{
			handler(this);
		}
		/** Constructs a dataset handler.
		 * 
		 * @param map an argument map for appending arguments.
		 * @param pfx comment prefix for output (e.g. dataset stats).
		 * @param l level for writing dataset statistics.
		 */
		DatasetHandler(ArgumentMap& map, const char* pfx, int l=0) : parent_type(pfx, true, l), strictInt(1)
		{
			handler(this);
			init(map);
		}
		/** Destructs a dataset handler.
		 */
		~DatasetHandler()
		{
		}
		
	public:
		/** Initalizes arguments in the part standard example format
		 *  and the strict checking argument in this class.
		 *
		 * @param map an argument map.
		 * @param l output level: read, write or both.
		 */
		template<class U>
		void init(U& map, int l=0)
		{
			parent_type::init(map, 1, l);
			arginit(map, strictInt,	"strict",	"strict checking of dataset aganist algorithm?", ArgumentMap::ADDITIONAL);
		}
		
	public:
		/** Performs the following operations:
		 * 	- Reads a dataset from a file
		 * 	- Setup the dataset
		 * 	- Setup the learner
		 * 	- Test if learner supports dataset
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a dataset to be read.
		 * @param file a file to read a dataset.
		 * @return an error message or NULL.
		 */
		template<class Lrn>
		const char* read(Lrn& learner, dataset_type& dataset, const std::string& file)
		{
			const char* msg;
			std::string filename = ext(learner, file);
			if( (msg=parent_type::read(filename, dataset)) != 0 ) return msg;
			learner.setup(dataset);
			if( (msg=learner.check(dataset, L::name(), strictInt)) != 0 ) return msg;
			return 0;
		}
		/** Performs the following operations:
		 * 	- Copies header from an existing dataset 
		 * 	- Reads a dataset from a file, tests if header matches
		 * 	- Setup the dataset
		 * 	- Setup the learner
		 * 
		 * @note Not test if learner supports dataset
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a concrete example set to be read.
		 * @param oldset an example set with a header.
		 * @param file a file to read a dataset.
		 * @return an error message or NULL.
		 */
		template<class W1>
		const char* read(L& learner, dataset_type& dataset, ExampleSet<attribute_type,class_type,W1>& oldset, const std::string& file)
		{
			const char* msg;
			std::string filename = ext(learner, file);
			if( oldset.empty() )
			{
				if( (msg=parent_type::read(filename, dataset)) != 0 ) return msg;
			}
			else if( (msg=parent_type::read(filename, dataset, oldset)) != 0 ) return msg;
			learner.setup(dataset, true);
			//if( (msg=learner.check(dataset, L::name(), strictInt)) != 0 ) return msg;
			return 0;
		}
		/** Reads a dataset header from a model.
		 * 
		 * @note only tests against existing header.
		 * 
		 * @param in an input stream.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		const char* read_model(std::istream& in, ExampleSet<attribute_type,class_type,std::string*>& dataset)
		{
			//read_model
			return parent_type::currentFormat().read_model(in, dataset);
		}
		/** Reads a dataset header from a model.
		 * 
		 * @note reads into or tests against existing header.
		 * 
		 * @param in an input stream.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		const char* read_model(std::istream& in, ConcreteExampleSet<attribute_type,class_type>& dataset)
		{
			//read_model
			return parent_type::currentFormat().read_model(in, dataset);
		}
		/** Writes a dataset header into a model
		 * 
		 * @param out an output stream.
		 * @param header an attribute column header.
		 * @return an error message or NULL.
		 */
		const char* write_model(std::ostream& out, const attribute_header& header)const
		{
			return parent_type::currentFormat().write_model(out, header);
		}
		/** Set or get a handler for a reader.
		 * 
		 * @param ptr a pointer to a handler or NULL.
		 * @return a pointer to a handler.
		 */
		static DatasetHandler<L,D>* handler(DatasetHandler<L,D>* ptr=0)
		{
			static DatasetHandler<L,D>* pReader = 0;
			if( ptr != 0 ) pReader = ptr;
			return pReader;
		}
		/** Sets the search path for a dataset.
		 * 
		 * @param str a search path.
		 */
		void add_search_path(const std::string& str)
		{
			implicitPth = str;
		}
		
	private:
		std::string ext(L& learner, const std::string& file)
		{
			std::string tmp = file;
			if( tmp.find('.') == std::string::npos && !testfile(tmp) )
			{
				tmp+='.';
				tmp+=learner.ext();
			}
			if( implicitPth != "" ) tmp = join_file(implicitPth.c_str(), tmp.c_str());
			return tmp;
		}
		
	private:
		int strictInt;
		std::string implicitPth;
	};
};

#endif


