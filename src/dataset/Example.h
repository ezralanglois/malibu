/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Example.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_EXAMPLE_H
#define _EXEGETE_EXAMPLE_H
#include "AttributeVector.h"
#include "ExampleLabel.h"
#include <string>

/** @file Example.h
 * @brief Defines an example
 * 
 * This file contains the Example class template and corresponding TypeUtil.
 * 
 * Its specializations including:
 * 	- A weighted example
 * 	- A labeled example
 * 	- An array of labels in an example
 * 	- An unlabeled and unweighted example
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an example
	 * 
	 * This class template defines an example.
	 */
	template<class X, class Y, class W>
	class Example : public AttributeVector<X>, public ExampleLabel<Y,W>
	{
		typedef ExampleLabel<Y,W> example_label;
		template<class X1, class Y1, class W1> friend class Example;
	public:
		/** Defines template parameter Y as a class type. **/
		typedef typename example_label::class_type			class_type;
		/** Defines template parameter W as a weight type. **/
		typedef typename example_label::weight_type			weight_type;
		/** Defines a label as a label type. **/
		typedef typename example_label::label_type			label_type;
		/** Defines an AttributeVector as an attribute vector. **/
		typedef AttributeVector<X>							attribute_vector;
		/** Defines an AttributeVector type util as a type util. **/
		typedef typename attribute_vector::type_util		type_util;
		/** Defines an AttributeVector value as a value type. **/
		typedef typename attribute_vector::value_type		value_type;
		/** Defines an AttributeVector attribute as a attribute type. **/
		typedef typename attribute_vector::attribute_type	attribute_type;
		/** Defines an AttributeVector index as a index type. **/
		typedef typename attribute_vector::index_type		index_type;
		/** Defines an AttributeVector pointer as a pointer. **/
		typedef typename attribute_vector::pointer			pointer;
		/** Defines an AttributeVector constant pointer as a constant pointer. **/
		typedef typename attribute_vector::const_pointer	const_pointer;
		/** Defines an AttributeVector size type as a size type. **/
		typedef typename attribute_vector::size_type		size_type;
		/** Defines an AttributeVector difference type as a difference type. **/
		typedef typename attribute_vector::difference_type	difference_type;

	public:
		/** Constructs an example.
		 */
		Example()
		{
		}
		/** Destructs an example.
		 */
		~Example()
		{
		}

	public:
		/** Constructs a shallow copy of an attribute vector and label.
		 *
		 * @param e a constant reference to an example.
		 */
		Example(const Example& e) : attribute_vector(e), example_label(e)
		{
		}
		/** Constructs an example with the given pointer.
		 *
		 * @param p an attribute pointer.
		 */
		Example(pointer p) : attribute_vector(p)
		{
		}
		/** Assigns a shallow copy of an attribute vector and label.
		 *
		 * @param e a constant reference to an example.
		 * @return a reference to this example.
		 */
		Example& operator=(const Example& e)
		{
			attribute_vector::operator=(e);
			example_label::operator=(e);
			return *this;
		}
		/** Constructs a shallow copy of an attribute vector and label.
		 *
		 * @param e a constant reference to an example.
		 */
		template<class X1, class W1>
		Example(const Example<X1,Y,W1>& e) : attribute_vector(e), example_label(e)
		{
		}
		/** Assigns a shallow copy of an attribute vector and label.
		 *
		 * @param e a constant reference to an example.
		 * @return a reference to this example.
		 */
		template<class X1, class W1>
		Example& operator=(const Example<X1,Y,W1>& e)
		{
			attribute_vector::operator=(e);
			example_label::operator=(e);
			return *this;
		}
		/** Makes a shallow copy of an example.
		 *
		 * @param ex a refrence to a source example.
		 */
		void shallow(Example& ex)
		{
			attribute_vector::shallow(ex);
			example_label::shallow(ex);
		}

	public:
		/** Assigns an attribute pointer to this example.
		 *
		 * @param p attribute pointer.
		 * @return a reference to this example.
		 */
		Example& operator=(pointer p)
		{
			attribute_vector::operator=(p);
			return *this;
		}
		/** Implicitly converts an example to an attribute pointer.
		 *
		 * @return an attribute pointer.
		 */
		operator pointer()
		{
			return attribute_vector::x();
		}
		/** Implicitly converts an example to an attribute pointer.
		 *
		 * @return an attribute pointer.
		 */
		operator pointer()const
		{
			return const_cast<pointer>(attribute_vector::x());
		}		
		//operator const_pointer()const
		//{
		//	return attribute_vector::x();
		//}
		
		/** Implicitly converts an example to a class type.
		 * 
		 * @note this is dangerous but, currently, a necessary hack.
		 * 
		 * @return a class type.
		 */
		operator class_type()const
		{
			return example_label::y();
		}
		/** This method erases the example.
		 */
		/*void erase()
		{
			attribute_vector::erase();
			example_label::erase();
		}*/
		/** Initializes the pointers to zero.
		 */
		/*void init()
		{
			attribute_vector::init();
			example_label::init();
		}*/
	};

	/** @brief Defines a comparison of two examples
	 * 
	 * This class template is used to compare two examples.
	 */
	template<class T> 
	class CompareExample;

	/** @brief Defines a comparison of two examples
	 * 
	 * This specialized class template is used to compare two examples.
	 */
	template<class A, class C, class W>
	class CompareExample< Example<A,C,W>* >
	{
	public:
		/** Defines an attribute index as a size type. **/
		typedef typename Example<A,C,W>::index_type size_type;
	public:
		/** Constructs a CompareExample with the specified attribute index. 
		 * 
		 * @param i attribute index.
		 */
		CompareExample(size_type i) : index(i)
		{
		}
		/** Compares two examples to determine which is larger.
		 *
		 * @param lhs a constant reference to an example on the left side of the comparision.
		 * @param rhs a constant reference to an example on the right side of the comparision.
		 */
		bool operator()(const Example<A,C,W>& lhs, const Example<A,C,W>& rhs)const 
		{
			return lhs.valueAt(index) < rhs.valueAt(index);
		}
	private:
		size_type index;
	};

};

/** @brief Defines a type utility for an Example
 * 
 * This specialized class template defines a TypeUtil for an Example<X,Y,W>.
 */
template<class X, class Y, class W>
struct TypeUtil< ::exegete::Example<X,Y,W> >
{
	/** Flags the class as POD. **/
	enum{ispod=true};
	/** Defines a Example<X,Y,W> as a value type. **/
	typedef ::exegete::Example<X,Y,W> value_type;
	/** Gets the name of the type.
	 *
	 * @return Example
	*/
	static const char* name() 
	{
		return "Example";
	}
};



#endif


