/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ExampleAlgorithms.h
 * Copyright (C) 2005-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_EXAMPLEALGORITHMS_H
#define _EXEGETE_EXAMPLEALGORITHMS_H
#include "ShuffleAlgorithms.h"
#include <cmath>


/** @file ExampleAlgorithms.h
 * @brief Collection of example utilities.
 * 
 * This file contains a set of global functions operate on a collection of examples.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Randomly initializes a vector of probabilities.
	 * 
	 * Assigns an ascending set of random numbers.
	 *
	 * @param beg a start iterator.
	 * @param end an end iterator.
	 * @param rand a random number generator.
	 */
	void initprob(double* beg, double* end, MTRand& rand)
	{
		double psum=0.0;
		for(double* curr=beg;curr != end;++curr) (*curr) = (psum += rand.rand());
		psum = 1.0 / psum;
		for(double* curr=beg;curr != end;++curr) (*curr) *= psum;
		(*(end-1)) = 1.0000001;
	}
	/** @brief Randomly initializes a vector of probabilities.
	 * 
	 * Assigns an ascending set of random numbers using global random number generator.
	 *
	 * @param beg a start iterator.
	 * @param end an end iterator.
	 */
	void initprob(double* beg, double* end)
	{
		double psum=0.0;
		for(double* curr=beg;curr != end;++curr) (*curr) = (psum += random_double());
		psum = 1.0 / psum;
		for(double* curr=beg;curr != end;++curr) (*curr) *= psum;
		(*(end-1)) = 1.0000001;
	}
	/** @brief Calculates the mean attribute
	 * 
	 * Calculates the mean attribute value for a specific column.
	 *
	 * @param beg an iterator to the start of a collection of examples.
	 * @param end an iterator to the end of a collection of examples.
	 * @param n the attribute index.
	 * @param sum initial value.
	 * @return the mean attribute value.
	 */
	template<class I, class F>
	F average(I beg, I end, unsigned int n, F sum)
	{
		F inv = 1.0f / std::distance(beg, end);
		for(;beg != end;++beg) sum+=beg->x()[n];
		return sum * inv;
	}
	/** @brief Calculates the standard deviation
	 * 
	 * Calculates the standard deviation about the mean for a specific attribute column.
	 *
	 * @param beg an iterator to the start of a collection of examples.
	 * @param end an iterator to the end of a collection of examples.
	 * @param n the attribute index.
	 * @param avg the mean attribute value.
	 * @return standard deviation about the mean attribute value.
	 */
	template<class I, class F>
	F stddev(I beg, I end, unsigned int n, F avg)
	{
		F inv = 1.0f / std::distance(beg, end), sum=0.0f;
		for(;beg != end;++beg) sum+=(beg->x()[n]-avg)*(beg->x()[n]-avg);
		return std::sqrt( sum * inv );
	}
	/** @brief Calculates the average deviation
	 * 
	 * Calculates the average deviation about the mean for a specific attribute column.
	 *
	 * @param beg an iterator to the start of a collection of examples.
	 * @param end an iterator to the end of a collection of examples.
	 * @param n the attribute index.
	 * @param avg the mean attribute value.
	 * @return average deviation about the mean attribute value.
	 */
	template<class I, class F>
	F avgdev(I beg, I end, unsigned int n, F avg)
	{
		F inv = 1.0f / std::distance(beg, end), sum=0.0f;
		for(;beg != end;++beg) sum+=std::abs(beg->x()[n]-avg);
		return ( sum * inv );
	}
};

#endif


