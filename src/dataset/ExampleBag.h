/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ExampleBag.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_BAG_H
#define _EXEGETE_BAG_H
#include "Example.h"

/** @file ExampleBag.h
 * @brief Defines a bag of examples
 * 
 * This file contains the ExampleBag class template and a corresponding TypeUtil.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

	/** @brief Defines a bag of examples
	 * 
	 * This class template defines a bag of examples.
	 */
	template<class X, class Y, class W>
	class ExampleBag : public Example<X,Y,W>
	{
		template<class X1, class Y1, class W1> friend class ExampleBag;
		typedef Example<X, Y, W> parent_type;
	public:
		/** Defines the parent class as a class type. **/
		typedef typename parent_type::class_type			class_type;
		/** Defines the parent weight as a weight type. **/
		typedef typename parent_type::weight_type			weight_type;
		/** Defines the parent attribute vector as a attribute vector. **/
		typedef typename parent_type::attribute_vector		attribute_vector;
		/** Defines the parent attribute as a attribute type. **/
		typedef typename parent_type::attribute_type		attribute_type;
		/** Defines the parent index as a index type. **/
		typedef typename parent_type::index_type			index_type;
		/** Defines the parent size as a size type. **/
		typedef typename parent_type::size_type				size_type;
		/** Defines the parent pointer as a pointer. **/
		typedef typename parent_type::pointer				pointer;
		/** Defines the parent constant pointer as a constant pointer. **/
		typedef typename parent_type::const_pointer			const_pointer;
		/** Defines the parent type utilility as a type utilility. **/
		typedef typename parent_type::type_util				type_util;

	public:
		/** Constructs an example bag.
		 */
		ExampleBag()
		{
		}
		/** Destructs an example bag.
		 */
		~ExampleBag()
		{
		}

	public:
		/** Constructs a shallow copy of an example bag.
		 *
		 * @param ex an example bag to copy.
		 */
		ExampleBag(const ExampleBag& ex) : parent_type(ex), xend(ex.xend)
		{
		}
		/** Assigns a shallow copy of an example.
		 *
		 * @param ex an example bag to copy.
		 */
		ExampleBag& operator=(const ExampleBag& ex)
		{
			parent_type::operator = (ex);
			xend = ex.xend;
			return *this;
		}
		/** Constructs a shallow copy of an example bag.
		 *
		 * @param ex an example bag to copy.
		 */
		template<class X1, class W1>
		ExampleBag(const ExampleBag<X1,Y,W1>& ex) : parent_type(ex)
		{
		}
		/** Assigns a shallow copy of an example.
		 *
		 * @param ex an example bag to copy.
		 */
		template<class X1, class W1>
		ExampleBag& operator=(const ExampleBag<X1,Y,W1>& ex)
		{
			parent_type::operator = (ex);
			return *this;
		}

	public:
		/** Gets a pointer to the first example.
		 *
		 * @return a pointer to the first example.
		 */
		pointer begin()
		{
			return parent_type::x();
		}
		/** Gets a pointer to the last example.
		 *
		 * @return a pointer to the last example.
		 */
		pointer end()
		{
			return xend;
		}
		/** Gets a constant pointer to the first example.
		 *
		 * @return a constant pointer to the first example.
		 */
		const_pointer begin()const
		{
			return parent_type::x();
		}
		/** Gets a constant pointer to the last example.
		 *
		 * @return a constant pointer to the last example.
		 */
		const_pointer end()const
		{
			return xend;
		}
		/** Sets a pointer to the last example.
		 *
		 * @param p a pointer to the last example.
		 */
		void begin(pointer p)
		{
			parent_type::x(p);
		}
		/** Sets a pointer to the last example.
		 *
		 * @param p a pointer to the last example.
		 */
		void end(pointer p)
		{
			xend = p;
		}
		/** Gets the size of the bag.
		 *
		 * @return the number of bags.
		 */
		unsigned int size()const
		{
			return (unsigned int)(end()-begin());
		}
		/** Erases only the label of the example bag.
		 */
		void erase()
		{
			Example<X,Y,W>::erase_label();
		}

	private:
		pointer xend;
	};

};



/** @brief Defines a type utility for an ExampleBag
 * 
 * This specialized class template defines a TypeUtil for an
 * ExampleBag<X,Y,W>.
 */
template<class X, class Y, class W>
struct TypeUtil< ::exegete::ExampleBag<X,Y,W> >
{
	/** Flags the class as POD. **/
	enum{ispod=true};
	/** Defines an ExampleBag as a value type. **/
	typedef ::exegete::ExampleBag<X,Y,W> value_type;
	/** Gets the name of the type.
	 *
	 * @return ExampleBag
	*/
	static const char* name() 
	{
		return "ExampleBag";
	}
};




#endif


