/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ExampleFormat.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_EXAMPLEFORMAT_H
#define _EXEGETE_EXAMPLEFORMAT_H
#include "ConcreteExampleSet.h"
#include "StandardExampleFormat.h"
#include "fileioutil.h"

/** @file ExampleFormat.h
 * @brief Read and setup dataset.
 * 
 * This file contains the ExampleFormat class, which reads and sets up
 * a dataset. The setup portion is primarily handled in this class.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section exampleformat Example Format
 * 	- summary
 * 		- Description: summary verbose output of dataset statistics
 * 		- Type: Option(None:0;Short:1;Long:2;All:3)
 * 		- Viewability: Standard
 * 	- positive
 * 		- Description: list of positive classes
 * 		- Type: list to strings
 * 		- Viewability: Standard
 * 	- bagidx
 * 		- Description: index of bag in labels, must be greater than 0
 * 		- Type: integer
 * 		- Viewability: Standard
 * 	- bagbeg
 * 		- Description: substring begin
 * 		- Type: integer
 * 		- Viewability: Standard
 * 	- bagend
 * 		- Description: substring end
 * 		- Type: integer
 * 		- Viewability: Standard
 * 	- ensure
 * 		- Description: ensure the correct dataset statistics: # classes, # attributes, # examples, # bags
 * 		- Type: integer list
 * 		- Viewability: Developer
 */

namespace exegete
{
	/** @brief Read and setup dataset.
	 * 
	 * This class defines the format for reading and writing an example.
	 */
	template<class X, class Y>
	class ExampleFormat
	{
	public:
		/** Defines a ConcreteExampleSet as a concrete type. **/
		typedef ConcreteExampleSet<X,Y>					concrete_type;
		/** Defines a dataset. **/
		typedef typename concrete_type::parent_type		dataset_type;
		/** Defines a testset. **/
		typedef typename concrete_type::parent_type		testset_type;
		/** Defines a standard example format. **/
		typedef StandardExampleFormat<X,Y> format_type;
		/** Enumerated flags for argument types. **/
		enum{NONE,READ,WRITE,BOTH};

	public:
		/** Constructs an example format.
		 * 
		 * @param cm an output prefix.
		 * @param read a dummy variable.
		 * @param l a level grouping arguments into views.
		 */
		ExampleFormat(std::string cm, bool read=true, int l=0) : commentstr(cm), poslabel("+"), neglabel("-"), bagIdx(0), bagBeg(0), bagEnd(0), verboseType(cm==""?0:1)
		{
		}
		/** Constructs an example format.
		 * 
		 * @param map an argument map.
		 * @param cm an output prefix.
		 * @param read only add read arguments to a map.
		 * @param l a level grouping arguments into views.
		 */
		ExampleFormat(ArgumentMap& map, std::string cm, bool read=true, int l=0) : commentstr(cm), poslabel("+"), neglabel("-"), bagIdx(0), bagBeg(0), bagEnd(0), verboseType(cm==""?0:1)
		{
			init(map, read, l);
		}
		/** Destructs an example format.
		 */
		~ExampleFormat()
		{
		}
	
	public:
		/** Initalizes an example format with a set of arguments.
		 *
		 * @param map an argument map.
		 * @param t if set true only add read arguments.
		 * @param l a level grouping arguments into views.
		 */
		template<class U>
		void init(U& map, int t=0, int l=0)
		{
			format.init(map, t?READ:BOTH, l);
			arginit(map, verboseType,	"summary",	"summary verbose output of dataset statistics>None:0;Short:1;Long:2;All:3");
			arginit(map, poslbs,		"positive", "list of positive classes");
			arginit(map, bagIdx,		"bagidx",	"index of bag in labels, must be greater than 0");
			arginit(map, bagBeg,		"bagbeg",	"substring begin");
			arginit(map, bagEnd,		"bagend",	"substring end");
			arginit(map, ensureVec,		"ensure",  	"ensure the correct dataset statistics: # classes, # attributes, # examples, # bags", ArgumentMap::DEVELOPER);
		}

	public:
		/** Reads a dataset from a file and then performs standard
		 * operations on the dataset including:
		 *	- setting the positive class
		 *	- organizing examples by bag
		 *	- printing dataset statistics or information
		 *	- ensures dataset has proper side (if argument set)
		 *
		 * @param filename the source filename.
		 * @param dataset the destintation dataset.
		 * @return an error message or NULL.
		 */
		const char* read(const std::string& filename, concrete_type& dataset)
		{
			const char* msg;
			if( (msg=readfile(filename, format(dataset))) != 0 ) return msg;
			if( verboseType == 3 )
			{
				std::cout << commentstr << "Filename: " << base_name(filename.c_str()) << std::endl;
				std::cout << dataset.toString(verboseType-1, commentstr) << std::endl;
				std::cout << commentstr;
				for(unsigned int i=0;i<poslbs.size();++i) std::cout << poslbs[i] << ",";
				std::cout << std::endl;
			}
			if( !poslbs.empty() ) dataset.labelPositive(poslbs, poslabel, neglabel);
			if( bagIdx > 0 ) if( (msg=dataset.build_bags(bagIdx-1, bagBeg, bagEnd)) != 0 ) return msg;
			//if( bagIdx > 0 ) dataset.buildBags(bagIdx-1, bagBeg, bagEnd);
			if( verboseType )
			{
				std::cout << commentstr << "Filename: " << base_name(filename.c_str()) << std::endl;
				std::cout << dataset.toString(verboseType-1, commentstr) << std::endl;
			}
			if( (msg=dataset.ensure(ensureVec)) != 0 ) return msg;
			return 0;
		}
		/** Reads a dataset from a file and then performs standard
		 * operations on the dataset including:
		 *	- sets attribute header from existing dataset
		 *	- setting the positive class
		 *	- organizing examples by bag
		 *	- printing dataset statistics or information
		 *	- ensures dataset has proper side (if argument set)
		 *
		 * @param filename the source filename.
		 * @param dataset the destintation dataset.
		 * @param oldset an existing dataset.
		 * @return an error message or NULL.
		 */
		template<class W1>
		const char* read(const std::string& filename, concrete_type& dataset, ExampleSet<X,Y,W1>& oldset)
		{
			const char* msg;
			if( poslbs.empty() ) dataset = oldset;
			if( (msg=readfile(filename, format(dataset))) != 0 ) return msg;
			if( verboseType == 3 )
			{
				std::cout << commentstr << "Filename: " << base_name(filename.c_str()) << std::endl;
				std::cout << dataset.toString(verboseType-1, commentstr) << std::endl;
				std::cout << commentstr;
				for(unsigned int i=0;i<poslbs.size();++i) std::cout << poslbs[i] << ",";
				std::cout << std::endl;
			}
			if( !poslbs.empty() ) dataset.labelPositive(poslbs, poslabel, neglabel);
			if( bagIdx > 0 ) if( (msg=dataset.build_bags(bagIdx-1, bagBeg, bagEnd)) != 0 ) return msg;
			//if( bagIdx > 0 ) dataset.buildBags(bagIdx-1, bagBeg, bagEnd);
			if( verboseType )
			{
				std::cout << commentstr << "Filename: " << base_name(filename.c_str()) << std::endl;
				std::cout << dataset.toString(verboseType-1, commentstr) << std::endl;
			}
			if( (msg=dataset.ensure(ensureVec)) != 0 ) return msg;
			return 0;
		}
		/** Writes a dataset to a file.
		 *
		 * @param filename a file to write the dataset.
		 * @param dataset the source dataset.
		 * @return an error message or NULL.
		 */
		const char* write(const std::string& filename, const dataset_type& dataset)
		{
			const char* msg;
			if( !filename.empty() && (msg=writefile(filename, format(dataset))) != 0 ) return msg;
			return 0;
		}
		/** Sets the command for the verbose output.
		 *
		 * @param str a comment string.
		 */
		void comment(const std::string& str)
		{
			commentstr = str;
		}
		/** Gets the current format of the dataset.
		 *
		 * @return the current format.
		 */
		StandardExampleFormat<X,Y>& currentFormat()
		{
			return format;
		}
		/** Gets the current format of the dataset.
		 *
		 * @return the current format.
		 */
		const StandardExampleFormat<X,Y>& currentFormat()const
		{
			return format;
		}

	private:
		StandardExampleFormat<X,Y> format;
	private:
		std::string commentstr;
		std::string poslabel;
		std::string neglabel;
	private:
		std::vector<unsigned int> ensureVec;
		std::vector<std::string> poslbs;
		unsigned int bagIdx;
		unsigned int bagBeg;
		unsigned int bagEnd;
		int verboseType;
	};
};


#endif


