/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ExampleLabel.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_EXAMPLELABEL_H
#define _EXEGETE_EXAMPLELABEL_H
#include "ClassUtil.h"
#include "AttributeTypeUtil.h"
#include <string>
#include <iostream>
#include <cmath>

/** @file ExampleLabel.h
 * @brief Defines example label and specializations.
 * 
 * This file contains the ExampleLabel class template.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

	/** @brief Defines a weighted example label
	 * 
	 * This class template defines a weighted example label.
	 */
	template<class Y, class W>
	class ExampleLabel
	{
		template<class Y1, class W1> friend class ExampleLabel;
	public:
		/** Defines the template parameter Y as a class type. **/
		typedef Y											class_type;
		/** Defines the template parameter W as a weight type. **/
		typedef W											weight_type;
		/** Defines void as a label type. **/
		typedef void										label_type;

	public:
		/** Constructs an example label.
		 */
		ExampleLabel() : yval(0), wval(0)
		{
		}
		/** Constructs an example label.
		 *
		 * @param v a class label.
		 */
		ExampleLabel(class_type v) : yval(v), wval(0)
		{
		}
		/** Destructs an example label.
		 */
		~ExampleLabel()
		{
		}

	public:
		/** Constructs a shallow copy of an example label.
		 *
		 * @param e an example label.
		 */
		ExampleLabel(const ExampleLabel& e) : yval(e.yval), wval(e.wval)
		{
		}
		/** Assigns a shallow copy of the class label, and weight.
		 *
		 * @param e an example label.
		 * @return a reference to this example label.
		 */
		ExampleLabel& operator=(const ExampleLabel& e)
		{
			yval = e.yval;
			wval = e.wval;
			return *this;
		}
		/** Constructs a shallow copy a class label.
		 *
		 * @param e an example label.
		 */
		template<class W1>
		ExampleLabel(const ExampleLabel<Y,W1>& e) : yval(e.yval), wval(0)
		{
		}
		/** Assigns a shallow copy of a class label.
		 *
		 * @param e an example label.
		 * @return a reference to this example label.
		 */
		template<class W1>
		ExampleLabel& operator=(const ExampleLabel<Y,W1>& e)
		{
			yval = e.yval;
			wval = 0;
			return *this;
		}
		/** Sets a shallow copy of an example label.
		 *
		 * @param ex a reference to a source example label.
		 */
		void shallow(ExampleLabel& ex)
		{
			yval = ex.yval;
			wval = ex.wval;
		}

	public:
		/** Tests if example has unique label.
		 * 
		 * @return false
		 */
		bool haslabel()const
		{
			return false;
		}
		/** Implicitly converts an example (label) to a class type.
		 * 
		 * @note This is dangerous but currently a necessary hack.
		 * 
		 * @return a class type.
		 */
		operator class_type()const
		{
			return yval;
		}
		/** Gets the class label.
		 *
		 * @return a class label.
		 */
		class_type y()const
		{
			return yval;
		}
		/** Sets the class label.
		 *
		 * @param val the new class label.
		 */
		void y(class_type val)
		{
			yval = val;
		}
		/** Get the weight for class.
		 * 
		 * @param p predicted class.
		 * @return weight for class.
		 */
		weight_type weightforclass(class_type p)
		{
			return y > 0 ? 1 : -1;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param w a dummy weight.
		 * @return the weight assigned to this example.
		 */
		weight_type weight(weight_type w)const
		{
			return wval;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @return the weight assigned to this example.
		 */
		weight_type& w_pointer()
		{
			return wval;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @return the weight assigned to this example.
		 */
		weight_type w()const
		{
			return wval;
		}
		/** Sets an example weight.
		 * 
		 * @param v the new weight.
		 */
		void w(weight_type v)
		{
			wval = v;
		}
		/** Multiplicative update weight.
		 *
		 * @param v an update weight.
		 */
		void w_update(weight_type v)
		{
			wval *= v;
		}
		/** Gets an internal code for the class type.
		 *
		 * @return a missing class token.
		 */
		static class_type missingClass()
		{
			return AttributeUtil<class_type>::missing();
		}
		/** Writes an ExampleLabel to the output stream.
		 *
		 * @param out an output stream.
		 * @param label a constant reference to an ExampleLabel.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const ExampleLabel& label)
		{
			out << label.wval << "\t";
			if( is_attribute_missing(label.yval) ) out << "?";
			else out << label.yval;
			return out;
		}
		/** Reads a ExampleLabel from the input stream.
		 *
		 * @param in an input stream.
		 * @param label a reference to an ExampleLabel.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, ExampleLabel& label)
		{
			in >> label.wval;
			in.get();
			if( in.peek() == '?' )
			{
				in.get();
				label.yval = AttributeUtil<class_type>::missing();
			}
			else in >> label.yval;
			return in;
		}

	private:
		class_type yval;
		weight_type wval;
	};

	
	/** @brief Defines an example label with multiple weights.
	 * 
	 * This class template defines an example with multiple weights.
	 */
	template<class Y, class W>
	class ExampleLabel<Y,W*>
	{
		template<class Y1, class W1> friend class ExampleLabel;
	public:
		/** Defines the template parameter Y as a class type. **/
		typedef Y											class_type;
		/** Defines the template parameter W as a weight type. **/
		typedef W											weight_type;
		/** Defines void as a label type. **/
		typedef void										label_type;

	public:
		/** Constructs an example label.
		 */
		ExampleLabel() : yval(0), wval(0)
		{
		}
		/** Constructs an example label.
		 *
		 * @param v a class label.
		 */
		ExampleLabel(class_type v) : yval(v), wval(0)
		{
		}
		/** Destructs an example label.
		 */
		~ExampleLabel()
		{
		}

	public:
		/** Constructs a shallow copy of an example label.
		 *
		 * @param e a constant reference to an example label.
		 */
		ExampleLabel(const ExampleLabel& e) : yval(e.yval), wval(e.wval)
		{
		}
		/** Assigns a shallow copy of the class label, and weight.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		ExampleLabel& operator=(const ExampleLabel& e)
		{
			yval = e.yval;
			wval = e.wval;
			return *this;
		}
		/** Constructs a shallow copy of the class label and weight pointer.
		 *
		 * @param e a constant reference to an example label.
		 */
		ExampleLabel(const ExampleLabel<Y,W>& e) : yval(e.yval), wval(const_cast<W*>(&e.wval))
		{
		}
		/** Assigns a shallow copy of a class label and weight pointer.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		ExampleLabel& operator=(const ExampleLabel<Y,W>& e)
		{
			yval = e.yval;
			wval = const_cast<W*>(&e.wval);
			return *this;
		}
		/** Constructs a shallow copy of the class label.
		 *
		 * @param e a constant reference to an example label.
		 */
		template<class W1>
		ExampleLabel(const ExampleLabel<Y,W1>& e) : yval(e.yval), wval(0)
		{
		}
		/** Assigns a shallow copy of a class label.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		template<class W1>
		ExampleLabel& operator=(const ExampleLabel<Y,W1>& e)
		{
			yval = e.yval;
			wval = 0;
			return *this;
		}
		/** Sets a shallow copy of an example label.
		 *
		 * @param ex a source example label.
		 */
		void shallow(ExampleLabel& ex)
		{
			yval = ex.yval;
			wval = ex.wval;
		}

	public:
		/** Tests if the example has unique label.
		 * 
		 * @return true if it has unique label.
		 */
		bool haslabel()const
		{
			return false;
		}
		/** Implicitly converts an example (label) to a class type.
		 * 
		 * @note This is dangerous but currently a necessary hack.
		 * 
		 * @return a class type.
		 */
		operator class_type()const
		{
			return yval;
		}
		/** Gets the class label.
		 *
		 * @return a class label.
		 */
		class_type y()const
		{
			return yval;
		}
		/** Sets the class label.
		 *
		 * @param val the new class label.
		 */
		void y(class_type val)
		{
			yval = val;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param w a dummy weight.
		 * @return the weight assigned to this example.
		 */
		weight_type weightforclass(class_type p)const
		{
			ASSERT(wval != 0);
			return std::abs(weight_type(wval[yval]-wval[p]));
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param w a dummy weight.
		 * @return the weight assigned to this example.
		 */
		weight_type weight(weight_type w)const
		{
			ASSERT(wval != 0);
			return *wval;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @return the weight assigned to this example.
		 */
		weight_type* w_pointer()const
		{
			return wval;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @return the weight assigned to this example.
		 */
		weight_type*& w_pointer()
		{
			return wval;
		}
		/** Sets an example weight.
		 * 
		 * @param v the new weight.
		 */
		void w_pointer(weight_type* v)
		{
			wval = v;
		}
		/** Multiplicative update weight.
		 *
		 * @param v a update weight.
		 */
		weight_type w()const
		{
			ASSERT(wval != 0);
			return *wval;
		}
		/** Multiplicative update weight.
		 *
		 * @param v a update weight.
		 */
		void w(weight_type v)
		{
			ASSERT(wval != 0);
			*wval = v;
		}
		/** Multiplicative update weight.
		 *
		 * @param v a update weight.
		 */
		void w_update(weight_type v)
		{
			ASSERT(wval != 0);
			*wval *= v;
		}
		/** Gets an internal code for a missing class.
		 *
		 * @return an internal missing class code.
		 */
		static class_type missingClass()
		{
			return AttributeUtil<class_type>::missing();
		}

	private:
		class_type yval;
		weight_type* wval;
	};

	
	/** @brief Defines an example label with a string label.
	 * 
	 * This class template defines a string labeled example label.
	 */
	template<class Y>
	class ExampleLabel<Y,std::string>
	{
		template<class Y1, class W1> friend class ExampleLabel;
	public:
		/** Defines the template parameter Y as a class type. **/
		typedef Y											class_type;
		/** Defines the void as a weight type. **/
		typedef void										weight_type;
		/** Defines a std::string as a label type. **/
		typedef std::string									label_type;

	public:
		/** Constructs an example.
		 */
		ExampleLabel()
		{
		}
		/** Constructs an example label.
		 *
		 * @param v a class label.
		 */
		ExampleLabel(class_type v) : yval(v)
		{
		}
		/** Destructs an example.
		 */
		~ExampleLabel()
		{
		}

	public:
		/** Constructs a shallow copy of the class and string label.
		 *
		 * @param e a constant reference to an example label.
		 */
		ExampleLabel(const ExampleLabel& e) : yval(e.yval), lval(e.lval)
		{
		}
		/** Assigns a shallow copy of a class and string label.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		ExampleLabel& operator=(const ExampleLabel& e)
		{
			yval = e.yval;
			lval = e.lval;
			return *this;
		}
		/** Constructs a shallow copy of the class label.
		 *
		 * @param e a constant reference to an example label.
		 */
		template<class W1>
		ExampleLabel(const ExampleLabel<Y,W1>& e) : yval(e.yval)
		{
		}
		/** Assigns a shallow copy of a class label.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		template<class W1>
		ExampleLabel& operator=(const ExampleLabel<Y,W1>& e)
		{
			yval = e.yval;
			return *this;
		}
		/** Constructs a shallow copy of the class and possibly string label.
		 *
		 * @param e a constant reference to an example label.
		 */
		ExampleLabel(const ExampleLabel<Y,std::string*>& e) : yval(e.yval)
		{
			if( e.lval != 0 ) lval = *e.lval;
		}
		/** Assigns a shallow copy of a class and possibly string label.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		ExampleLabel& operator=(const ExampleLabel<Y,std::string*>& e)
		{
			yval = e.yval;
			ASSERT(e.lval != 0);
			lval = *e.lval;
			return *this;
		}
		/** Makes a shallow copy of an example label.
		 *
		 * @param ex a refrence to a source example label.
		 */
		void shallow(ExampleLabel& ex)
		{
			yval = ex.yval;
			lval = ex.lval;
		}
		/** Tests if the string value is less than some string.
		 * 
		 * @param str a string to test.
		 * @return true if example label is less than specified string.
		 */
		bool operator<(const std::string& str)const 
		{
			return lval < str;
		}
		/** Tests if the string label is less than a specified string.
		 * 
		 * @param str a string to test.
		 * @param ex an example label to test.
		 * @return true if a specified string is less than the example label.
		 */
		friend bool operator<(const std::string& str, const ExampleLabel& ex) 
		{
			return str < ex.lval;
		}

	public:
		/** Tests if an example has a unique label.
		 * 
		 * @return true if has unique label.
		 */
		bool haslabel()const
		{
			return lval != "";
		}
		/** Gets the class value of an example label.
		 *
		 * @return a class value.
		 */
		operator class_type()const
		{
			return yval;
		}
		/** Gets the string label of the example label.
		 *
		 * @return a string label.
		 */
		operator const std::string&()const
		{
			return lval;
		}
		/** Gets the class label.
		 *
		 * @return a class label.
		 */
		class_type y()const
		{
			return yval;
		}
		/** Sets the class label.
		 *
		 * @param val the new class label.
		 */
		void y(class_type val)
		{
			yval = val;
		}
		/** Gets the label assigned to this example.
		 *
		 * @return the string label assigned to this example.
		 */
		const label_type& label()const
		{
			return lval;
		}
		/** Sets the string label.
		 * 
		 * @param v a string label.
		 */
		void label(const label_type& v)
		{
			lval = v;
		}
		/** Gets the a weight.
		 *
		 * @param w a weight.
		 * @return the weight passed to this method.
		 */
		double weight(double w)const
		{
			return w;
		}
		/** Get the weight for class.
		 * 
		 * @param p predicted class.
		 * @return weight for class.
		 */
		weight_type weightforclass(class_type p)
		{
			return y > 0 ? 1 : -1;
		}
		/** Gets an internal code for the missing class value.
		 *
		 * @return a missing class internal code.
		 */
		static class_type missingClass()
		{
			return AttributeUtil<class_type>::missing();
		}
		/** Writes an ExampleLabel to the output stream.
		 *
		 * @param out an output stream.
		 * @param label a constant reference to an ExampleLabel.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const ExampleLabel& label)
		{
			out << label.lval << "\t";
			if( is_attribute_missing(label.yval) ) out << "?";
			else out << label.yval;
			return out;
		}
		/** Reads a ExampleLabel from the input stream.
		 *
		 * @param in an input stream.
		 * @param label a reference to an ExampleLabel.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, ExampleLabel& label)
		{
			std::getline(in, label.lval, '\t');
			if( in.peek() == '?' )
			{
				in.get();
				label.yval = AttributeUtil<class_type>::missing();
			}
			else in >> label.yval;
			return in;
		}

	private:
		class_type yval;
		label_type lval;
	};

	
	/** @brief Defines an example label with multiple string labels.
	 * 
	 * This class template defines an example label with multiple labels.
	 */
	template<class Y>
	class ExampleLabel<Y,std::string*>
	{
		template<class Y1, class W1> friend class ExampleLabel;
	public:
		/** Defines the template parameter Y as a class type. **/
		typedef Y											class_type;
		/** Defines the void as a weight type. **/
		typedef void										weight_type;
		/** Defines a std::string as a label type. **/
		typedef std::string									label_type;
		/** Defines a std::string pointer as a label pointer. **/
		typedef std::string*								label_pointer;
		/** Defines a constant std::string pointer as a constant label pointer. **/
		typedef const std::string*							const_label_pointer;
		/** Define size_t as size_type. **/
		typedef size_t										size_type;

	public:
		/** Constructs an example label.
		 */
		ExampleLabel() : lval(0)
		{
		}
		/** Constructs an example label.
		 *
		 * @param v a class label.
		 */
		ExampleLabel(class_type v) : yval(v), lval(0)
		{
		}
		/** Destructs an example label.
		 */
		~ExampleLabel()
		{
		}

	public:
		/** Constructs a shallow copy of a class label and string label pointer.
		 *
		 * @param e a constant reference to an example label.
		 */
		ExampleLabel(const ExampleLabel& e) : yval(e.yval), lval(e.lval)
		{
		}
		/** Assigns a shallow copy of a class label and string label pointer.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		ExampleLabel& operator=(const ExampleLabel& e)
		{
			yval = e.yval;
			lval = e.lval;
			return *this;
		}
		/** Constructs a shallow copy of a class label.
		 *
		 * @param e a constant reference to an example label.
		 */
		template<class W1>
		ExampleLabel(const ExampleLabel<Y,W1>& e) : yval(e.yval), lval(0)
		{
		}
		/** Assigns a shallow copy of a class label.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		template<class W1>
		ExampleLabel& operator=(const ExampleLabel<Y,W1>& e)
		{
			yval = e.yval;
			lval = 0;
			return *this;
		}
		/** Makes a shallow copy of an example label.
		 *
		 * @param ex a refrence to a source example label.
		 */
		void shallow(ExampleLabel& ex)
		{
			yval = ex.yval;
			lval = ex.lval;
			ex.lval=0;
		}

	public:
		/** Tests if the example has a unique label.
		 * 
		 * @return true if has unique label.
		 */
		bool haslabel()const
		{
			return lval != 0 && *lval != "";
		}
		/** Gets the given weight.
		 *
		 * @param w a weight.
		 * @return the given weight.
		 */
		double weight(double w)const
		{
			return w;
		}
		/** Get the weight for class.
		 * 
		 * @param p predicted class.
		 * @return weight for class.
		 */
		weight_type weightforclass(class_type p)
		{
			return y > 0 ? 1 : -1;
		}
		/** Gets the class label.
		 *
		 * @return a class label.
		 */
		class_type y()const
		{
			return yval;
		}
		/** Sets the class label.
		 *
		 * @param val the new class label.
		 */
		void y(class_type val)
		{
			yval = val;
		}
		/** Gets a pointer to a collection of labels.
		 *
		 * @return a pointer to a collection of labels.
		 */
		const_label_pointer label()const
		{
			return lval;
		}
		/** Gets a pointer to a collection of labels.
		 *
		 * @return a pointer to a collection of labels.
		 */
		label_pointer& label()
		{
			return lval;
		}
		/** Sets a pointer to a collection of labels.
		 * 
		 * @param v a pointer to a collection of labels
		 */
		void label(label_pointer v)
		{
			lval = v;
		}
		/** Gets a label at a specific index.
		 *
		 * @param n an index.
		 * @return a constant reference to a label.
		 */
		const label_type& labelAt(size_type n)const
		{
			ASSERT(lval != 0);
			return lval[n];
		}
		/** Gets a label at a specific index.
		 *
		 * @param n an index.
		 * @return a reference to a label.
		 */
		label_type& labelAt(size_type n)
		{
			ASSERT(lval != 0);
			return lval[n];
		}
		/** Gets an internal code for a missing class.
		 *
		 * @return a internal code for a missing class.
		 */
		static class_type missingClass()
		{
			return AttributeUtil<class_type>::missing();
		}
		/** Writes the first label to the output stream.
		 * 
		 * @param out an output stream.
		 * @param label an example label.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const ExampleLabel& label)
		{
			out << *label.lval;
			return out;
		}

	private:
		class_type yval;
		label_pointer lval;
	};

	/** @brief Defines an example label with just a class label.
	 * 
	 * This class template defines a simple example, no label or weight.
	 */
	template<class Y>
	class ExampleLabel<Y,void>
	{
		template<class Y1, class W1> friend class ExampleLabel;
	public:
		/** Defines the template parameter Y as a class type. **/
		typedef Y											class_type;
		/** Defines the void as a weight type. **/
		typedef void										weight_type;
		/** Defines the void as a label type. **/
		typedef void										label_type;

	public:
		/** Constructs an example label.
		 */
		ExampleLabel()
		{
		}
		/** Constructs an example label.
		 *
		 * @param v a class label.
		 */
		ExampleLabel(class_type v) : yval(v)
		{
		}
		/** Destructs an example label.
		 */
		~ExampleLabel()
		{
		}

	public:
		/** Constructs a shallow copy of the class label.
		 *
		 * @param e a constant reference to an example label.
		 */
		ExampleLabel(const ExampleLabel& e) : yval(e.yval)
		{
		}
		/** Assigns a shallow copy of the class label.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		ExampleLabel& operator=(const ExampleLabel& e)
		{
			yval = e.yval;
			return *this;
		}
		/** Constructs a shallow copy of the class label.
		 *
		 * @param e a constant reference to an example label.
		 */
		template<class W1>
		ExampleLabel(const ExampleLabel<Y,W1>& e) : yval(e.yval)
		{
		}
		/**  Assigns a shallow copy of the class label.
		 *
		 * @param e a constant reference to an example label.
		 * @return a reference to this example label.
		 */
		template<class W1>
		ExampleLabel& operator=(const ExampleLabel<Y,W1>& e)
		{
			yval = e.yval;
			return *this;
		}
		/** Sets a shallow copy of the class label.
		 *
		 * @param ex a refrence to a source example label.
		 */
		void shallow(ExampleLabel& ex)
		{
			yval = ex.yval;
		}

	public:
		/** Tests if example has a unique label.
		 * 
		 * @return true if has unique label.
		 */
		bool haslabel()const
		{
			return false;
		}
		/** Gets the given weight.
		 *
		 * @param w a weight.
		 * @return the given weight.
		 */
		double weight(double w)const
		{
			return w;
		}
		/** Get the weight for class.
		 * 
		 * @param p predicted class.
		 * @return weight for class.
		 */
		int weightforclass(class_type p)
		{
			return (yval > 0) ? 1 : -1;
		}
		/** Gets the class label.
		 *
		 * @return a class label.
		 */
		class_type y()const
		{
			return yval;
		}
		/** Sets the class label.
		 *
		 * @param val the new class label.
		 */
		void y(class_type val)
		{
			yval = val;
		}
		/** Gets an internal code for a missing class.
		 *
		 * @return an internal code for a missing class.
		 */
		static class_type missingClass()
		{
			return AttributeUtil<class_type>::missing();
		}
		/** Writes an ExampleLabel to the output stream.
		 *
		 * @param out an output stream.
		 * @param label a constant reference to an ExampleLabel.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const ExampleLabel& label)
		{
			if( is_attribute_missing(label.yval) ) out << "?";
			else out << label.yval;
			return out;
		}
		/** Reads a ExampleLabel from the input stream.
		 *
		 * @param in an input stream.
		 * @param label a reference to an ExampleLabel.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, ExampleLabel& label)
		{
			if( in.peek() == '?' )
			{
				in.get();
				label.yval = AttributeUtil<class_type>::missing();
			}
			else in >> label.yval;
			return in;
		}

	private:
		class_type yval;
	};

	
	/** @brief Test if attribute is missing.
	 * 
	 * Tests if the given value is an internal representation of a missing value.
	 *
	 * @param val value to test.
	 * @return true if value is missing.
	 */
	template<class T, class W>
	bool is_attribute_missing(const ExampleLabel<T, W>& val)
	{
		return AttributeUtil<T>::missing() == val.y();
	}

	/** @brief Interface utility for ExampleLabel class value.
	 * 
	 * ExampleLabel defines a labeled class object.
	 * 
	 * It supports:
	 * 	- Define class value type.
	 * 	- Test if class has label.
	 */
	template<class T, class U>
	class ClassUtil< ExampleLabel<T,U> >
	{
	public:
		/** Defines a labeled class value type. **/
		typedef ExampleLabel<T,U> value_type;
		/** Defines a class value type. **/
		typedef T class_type;
		/** Test if class value has a label.
		 * 
		 * @param val a class value type.
		 * @return true if has label.
		 */
		static bool haslabel(const value_type& val)
		{
			return val.haslabel();
		}
	};
};

/** @brief Type utility for an ExampleLabel
 * 
 * This class template provides basic type operations for an ExampleLabel.
 */
template<class T, class U>
struct TypeUtil< ::exegete::ExampleLabel<T, U> >
{
	/** Flags an example label as a non-primative type.
	*/
	enum{ ispod=false };
	/** Defines an ExampleLabel as a value type. **/
	typedef ::exegete::ExampleLabel<T, U> value_type;
	/** Tests if a string can be safely converted to an example label.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return ExampleLabel
	*/
	static const char* name() 
	{
		return "ExampleLabel";
	}
};


#endif


