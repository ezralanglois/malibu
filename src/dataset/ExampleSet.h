/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ExampleVector.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_EXAMPLESET_H
#define _EXEGETE_EXAMPLESET_H
#include "ExampleVector.h"
#include "AttributeHeader.h"
#include "Example.h"

/** @file ExampleSet.h
 * @brief Defines a shallow collection of examples.
 * 
 * This file contains the ExampleSet class template.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteDataset Dataset System
 *  This group holds all the dataset related classes.
 */

namespace exegete
{
	/** @brief Defines a shallow collection of examples.
	 * 
	 * This class provides a cheap interface to a set of examples. 
	 * 	e.g. Copies are shallow, pointers to original attribute vectors
	 */
	template<class X,class Y, class W>
	class ExampleSet : public AttributeHeader<X,Y>
	{
		template<class X1, class Y1, class W1> friend class ExampleSet;
	public:
		/** Defines an example vector. **/
		typedef ExampleVector< Example<X,Y,W> >						example_vector;
		/** Defines a bag vector. **/
		typedef ExampleVector< ExampleBag<Example<X,Y,W>, Y, W> >	bag_vector;
	public:
		/** Defines an attribute header. **/
		typedef AttributeHeader<X,Y>						attribute_header;
	public:
		/** Defines an Example as a value type. **/
		typedef typename example_vector::value_type			value_type;
		/** Defines a reference to an example. **/
		typedef typename example_vector::reference			reference;
		/** Defines a constant reference to an example. **/
		typedef typename example_vector::const_reference	const_reference;
		/** Defines an iterator to an example. **/
		typedef typename example_vector::iterator			iterator;
		/** Defines a constant iterator to an example. **/
		typedef typename example_vector::const_iterator		const_iterator;
		/** Defines a weight type. **/
		typedef W weight_type;
	public:
		/** Defines a bag type. **/
		typedef typename bag_vector::value_type				bag_type;
		/** Defines a reference to a bag. **/
		typedef typename bag_vector::reference				bag_reference;
		/** Defines a constant reference to a bag. **/
		typedef typename example_vector::const_reference	const_bag_reference;
		/** Defines an iterator to a bag. **/
		typedef typename bag_vector::iterator				bag_iterator;
		/** Defines a constant iterator to a bag. **/
		typedef typename bag_vector::const_iterator			const_bag_iterator;
	public:
		/** Defines an example type utility. **/
		typedef typename example_vector::type_util			type_util;
		/** Defines a size type of an example. **/
		typedef typename example_vector::size_type			size_type;
		/** **/
		typedef typename example_vector::difference_type	difference_type;
		/** Defines a class type. **/
		typedef typename example_vector::class_type			class_type;
		/** Defines an example attribute type. **/
		typedef typename example_vector::attribute_type		attribute_type;
		/** Defines an example value type as a feature type. **/
		typedef typename example_vector::feature_type		feature_type;
		/** Defines an example attribute pointer. **/
		typedef typename value_type::pointer				attribute_pointer;
		/** Defines an example constant attribute pointer. **/
		typedef typename value_type::const_pointer			const_attribute_pointer;
		/** Defines an example index type. **/
		typedef typename example_vector::index_type			index_type;

	public:
		/** Constructs a default example set.
		 */
		ExampleSet()
		{
		}
		/** Destructs a default example set.
		 */
		~ExampleSet()
		{
		}

	public:
		/** Constructs a shallow copy of an example set.
		 *
		 * @param ex an example set to copy.
		 * @param n number of examples.
		 * @param b number of bags.
		 */
		ExampleSet(const attribute_header& ex, size_type n, size_type b) : attribute_header(ex), exampleVec(n), bagVec(b)
		{
		}
		/** Constructs a shallow copy of an example set.
		 *
		 * @param ex an example set to copy.
		 * @param n number of examples.
		 */
		ExampleSet(const ExampleSet& ex, size_type n=TypeUtil<size_type>::max()) : attribute_header(ex), exampleVec(ex.exampleVec, n), bagVec(ex.bagVec, n)
		{
		}
		/** Assigns a shallow copy of an example set.
		 *
		 * @param ex an ExampleSet to copy.
		 */
		ExampleSet& operator=(const ExampleSet& ex)
		{
			attribute_header::operator=(ex);
			exampleVec.resize(ex.exampleVec.size());
			bagVec.resize(ex.bagVec.size());
			return *this;
		}
		/** Constructs a shallow copy of an example set.
		 * 
		 * @note Does not copy examples or sets number of examples.
		 *
		 * @param ex an example set to copy.
		 * @param n number of examples.
		 */
		template<class Y1, class W1>
		ExampleSet(const ExampleSet<X,Y1,W1>& ex, size_type n=TypeUtil<size_type>::max()) : attribute_header(ex), exampleVec(ex.exampleVec, n), bagVec(ex.bagVec, n)
		{
		}
		/** Assigns a shallow copy of an example set.
		 *
		 * @note Does not copy examples or sets number of examples.
		 * 
		 * @param ex an ExampleSet to copy.
		 */
		template<class W1>
		ExampleSet& operator=(const ExampleSet<X,Y,W1>& ex)
		{
			attribute_header::operator=(ex);
			exampleVec.resize(ex.exampleVec.size());
			bagVec.resize(ex.bagVec.size());
			return *this;
		}

	public:
		/** Clears the example set.
		 */
		void clear()
		{
			exampleVec.clear();
			bagVec.clear();
		}
		/** Resizes the number of bags.
		 *
		 * @param n a new collection size.
		 */
		void resize_bags(size_type n)
		{
			bagVec.resize(n);
		}
		/** Resizes the number of examples.
		 *
		 * @param n a new collection size.
		 */
		void resize(size_type n)
		{
			exampleVec.resize(n);
		}
		/** Resizes the number of examples.
		 *
		 * @param n a new collection size.
		 */
		void setsize(size_type n)
		{
			exampleVec.setsize(n);
		}

	public:
		/** Gets number of bags if greater than zero otherwise number of examples.
		 * 
		 * @return number of bags or examples if no bags.
		 */
		size_type ex_size()const
		{
			if(bagCount()>0)
			{
				return bagCount();
			}
			else
			{
				return size();
			}
		}
		/** Stores the current order of examples into an index array.
		 * 
		 * @param ibeg a pointer to start of the array.
		 * @return a pointer to end of the array.
		 */
		size_type* indices(size_type* ibeg)
		{
			if(bagCount()>0)
			{
				for(bag_iterator beg=bag_begin(), fin=bag_end();beg != fin;++beg,++ibeg)
					*ibeg = indexof(beg);
			}
			else
			{
				for(iterator beg=begin(), fin=end();beg != fin;++beg,++ibeg)
					*ibeg = indexof(beg);
			}
			return ibeg;
		}
		/** Gets an example at the specified index.
		 *
		 * @param n an index of the example.
		 * @return a reference to a specific example.
		 */
		reference operator[](size_type n)
		{
			return exampleVec[n];
		}
		/** Gets a constant example reference at the specified index.
		 *
		 * @param n an index of the example.
		 * @return a constant reference to a specific example.
		 */
		const_reference operator[](size_type n)const
		{
			return exampleVec[n];
		}
		/** Gets the number of examples.
		 *
		 * @return the number of examples.
		 */
		size_type size()const
		{
			return exampleVec.size();
		}
		/** Tests if example set is empty.
		 * 
		 * @return true if example set is empty.
		 */
		bool empty()const
		{
			return exampleVec.empty();
		}
		/** Gets the number of bags.
		 *
		 * @return the number of bags.
		 */
		size_type bagCount()const
		{
			return bagVec.size();
		}
		/** Gets the example capacity of the example set.
		 *
		 * @return capacity of examples.
		 */
		size_type capacity()const
		{
			return exampleVec.capacity();
		}
		/** Gets the bag capacity of the example set.
		 *
		 * @return capacity of bags.
		 */
		size_type bagCapacity()const
		{
			return bagVec.capacity();
		}

	public:
		/** Gets an iterator to the start of the example set.
		 *
		 * @return a example iterator pointing to the start of the collection.
		 */
		iterator begin()
		{
			return exampleVec.begin();
		}
		/** Gets an iterator to the end of the example set.
		 *
		 * @return a example iterator pointing to the end of the collection.
		 */
		iterator end()
		{
			return exampleVec.end();
		}
		/** Gets an iterator to the start of the example set.
		 *
		 * @return a constant example iterator pointing to the start of the collection.
		 */
		const_iterator begin()const
		{
			return exampleVec.begin();
		}
		/** Gets an iterator to the end of the example set.
		 *
		 * @return a constant example iterator pointing to the end of the collection.
		 */
		const_iterator end()const
		{
			return exampleVec.end();
		}
		/** Gets an iterator to the start of a bag collection.
		 *
		 * @return a beginning iterator.
		 */
		bag_iterator bag_begin()
		{
			return bagVec.begin();
		}
		/** Gets an iterator to the end of a bag collection.
		 *
		 * @return a ending iterator.
		 */
		bag_iterator bag_end()
		{
			return bagVec.end();
		}
		/** Gets an iterator to the start of a bag collection.
		 *
		 * @return a beginning constant iterator.
		 */
		const_bag_iterator bag_begin()const
		{
			return bagVec.begin();
		}
		/** Gets an iterator to the end of a bag collection.
		 *
		 * @return a ending constant iterator.
		 */
		const_bag_iterator bag_end()const
		{
			return bagVec.end();
		}
		/** Gets an iterator to the end of a bag collection.
		 *
		 * @return a ending constant iterator.
		 */
		bag_iterator any_end(bag_iterator)
		{
			return bag_end();
		}
		/** Gets an iterator to the end of the example set.
		 *
		 * @return a example iterator pointing to the end of the collection.
		 */
		iterator any_end(iterator)
		{
			return end();
		}
		/** Get the maximum number of instances in a bag.
		 * 
		 * @return maximum number of instances per bag.
		 */
		size_type maximumBagCount()const
		{
			size_type bagcnt = 0;
			for(const_bag_iterator beg = bag_begin(), end = bag_end();beg != end;++beg)
			{
				if( beg->size() > bagcnt ) bagcnt = beg->size();
			}
			return bagcnt;
		}

	public:
		/** Assigns the given example set. If the example set contains
		 * bags then it copies on the bag level.
		 *
		 * @param dataset source example set.
		 */
		template<class W1>
		void assign(ExampleSet<X, Y, W1>& dataset)
		{
			if( dataset.bagCount() > 0) assign(dataset.bag_begin(), dataset.bag_end());
			else assign(dataset.begin(), dataset.end());
		}
		/** Assigns a subset of the given dataset outside the given range.
		 *
		 * @param dataset full dataset.
		 * @param beg start index of subset.
		 * @param end end index of subset.
		 */
		template<class W1>
		void assign_exclude(ExampleSet<X, Y, W1>& dataset, size_t beg, size_t end)
		{
			if( dataset.bagCount() > 0) assign(dataset.bag_begin(), dataset.bag_begin()+beg, dataset.bag_begin()+end, dataset.bag_end());
			else assign(dataset.begin(), dataset.begin()+beg, dataset.begin()+end, dataset.end());
		}
		/** Assigns a subset of the given dataset inside the given range.
		 *
		 * @param dataset full dataset.
		 * @param beg start index of subset.
		 * @param end end index of subset.
		 */
		template<class W1>
		void assign_include(ExampleSet<X, Y, W1>& dataset, size_t beg, size_t end)
		{
			if( dataset.bagCount() > 0) 
			{
				assign(dataset.bag_begin()+beg, dataset.bag_begin()+end);
			}
			else assign(dataset.begin()+beg, dataset.begin()+end);
		}
		/** Assigns a collection of examples.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class W1>
		void assign(Example<X, Y, W1>* beg, Example<X, Y, W1>* end)
		{
			exampleVec.assign(beg, end);
		}
		/** Appends a collection of examples.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class W1>
		void append(Example<X, Y, W1>* beg, Example<X, Y, W1>* end)
		{
			exampleVec.append(beg, end);
		}
		/** Appends an example.
		 *
		 * @param beg an iterator to the start of the collection.
		 */
		template<class W1>
		void append(Example<X, Y, W1>* beg)
		{
			append(beg, beg+1);
		}
		/** Assigns two collections of examples.
		 *
		 * @param beg1 an iterator to the start of the collection.
		 * @param end1 an iterator to the end of the collection.
		 * @param beg2 an iterator to the start of the collection.
		 * @param end2 an iterator to the end of the collection.
		 */
		template<class W1>
		void assign(Example<X, Y, W1>* beg1, Example<X, Y, W1>* end1, Example<X, Y, W1>* beg2, Example<X, Y, W1>* end2)
		{
			assign(beg1, end1);
			if(beg2 != end2) append(beg2, end2);
		}
		/** Assigns two collections of bags.
		 *
		 * @param beg1 an iterator to the start of the collection.
		 * @param end1 an iterator to the end of the collection.
		 * @param beg2 an iterator to the start of the collection.
		 * @param end2 an iterator to the end of the collection.
		 */
		template<class W1>
		void assign(ExampleBag<Example<X, Y, W1>,Y,W1>* beg1, ExampleBag<Example<X, Y, W1>,Y,W1>* end1, ExampleBag<Example<X, Y, W1>,Y,W1>* beg2, ExampleBag<Example<X, Y, W1>,Y,W1>* end2)
		{
			assign(beg1, end1);
			if(beg2 != end2) append(beg2, end2);
		}
		/** Appends a collection of bags.
		 *
		 * @param beg an iterator to the start of the collection.
		 */
		template<class W1>
		void append(ExampleBag<Example<X, Y, W1>,Y,W1>* beg)
		{
			append(beg, beg+1);
		}
		/** Assigns a collection of bags .
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class W1>
		void assign(ExampleBag<Example<X, Y, W1>,Y,W1>* beg, ExampleBag<Example<X, Y, W1>,Y,W1>* end)
		{
			bagVec.reset();
			exampleVec.reset();
			if(beg != end) append(beg, end);
		}
		/** Assigns a subset of examples based on a class flag map only if the value
		 * in the map is not zero. If the map holds a 1 assign example as positive.
		 * 
		 * @note This is the only assignment that changes the class value of an example.
		 * 
		 * @param dataset a source dataset.
		 * @param classes a class flag map.
		 */
		template<class W1, class A>
		void assign_by_class(ExampleSet<X, Y, W1>& dataset, const A& classes)
		{
			ASSERT(bagVec.size() == 0);
			exampleVec.reset();
			class_type y;
			for(iterator beg=dataset.begin(),end=dataset.end();beg != end;++beg)
				if( (y=classes[beg->y()]) != 0 ) exampleVec.append(beg, y==1);
		}
		/** Assigns a collection of bags.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class W1>
		void append(ExampleBag<Example<X, Y, W1>,Y,W1>* beg, ExampleBag<Example<X, Y, W1>,Y,W1>* end)
		{
			size_type n = bagVec.size()+(end-beg);
			bagVec.setsize(n);
			for(bag_iterator it=bagVec.end();beg != end;++beg, ++it)
			{
				*it = *beg;
				exampleVec.append(beg->begin(), beg->end());
				it->begin(exampleVec.end()-beg->size());
				it->end(exampleVec.end());
			}
			bagVec.resize(n);
			copy_bag_pos(bag_begin(), bag_end(), begin());
		}
		/** Assigns a collection of bags based on a collection of indices. This
		 * ensures the bags are added in the given order.
		 *
		 * @param bbeg an iterator to the start of the collection.
		 * @param bend an iterator to the end of the collection.
		 * @param beg a start iterator to a collection of indices to use.
		 * @param end an end iterator to a collection of indices to use.
		 */
		template<class W1, class I>
		void assign(W1 bbeg, W1 bend, I beg, I end)
		{
			W1 tmp;
			size_type n = (end-beg);
			bagVec.setsize(n);
			exampleVec.reset();
			for(bag_iterator it=bagVec.begin();beg != end;++beg, ++it)
			{
				tmp = bbeg+indexof(*beg);
				exampleVec.append(tmp->begin(), tmp->end());
				it->begin(exampleVec.end()-tmp->size());
				it->end(exampleVec.end());
			}
			bagVec.resize(n);
			copy_bag_pos(bag_begin(), bag_end(), begin());
		}
		/** Assigns an ordered sample of the dataset. The order is
		 * given by the index collection.
		 *
		 * @param dataset a source dataset.
		 * @param ibeg an iterator to start of index collection.
		 * @param iend an iterator to end of index collection.
		 */
		template<class W1, class I>
		void assignidx(ExampleSet<X, Y, W1>& dataset, I ibeg, I iend)
		{
			typedef typename ExampleSet<X, Y, W1>::bag_iterator obag_iterator;
			//ASSERT(std::distance(ibeg,iend) == size());
			if( dataset.bagCount() > 0 )
			{
				obag_iterator it;
				exampleVec.reset();
				resize_bags(std::distance(ibeg,iend));
				for(bag_iterator beg=bag_begin(), end=bag_end();beg != end;++beg, ++ibeg)
				{
					it = dataset.bagAtIndex(*ibeg);
					*beg = *it;
					exampleVec.append(it->begin(), it->end());
					beg->begin(exampleVec.end()-it->size());
					beg->end(exampleVec.end());
				}
				copy_bag_pos(bag_begin(), bag_end(), begin());
			}
			else
			{
				resize(std::distance(ibeg,iend));
				for(iterator beg1 = begin(), end1=end();beg1 != end1;++beg1, ++ibeg)
				{
					*beg1 = dataset[*ibeg];
					ASSERT( type_util::valueOf(*(dataset[*ibeg].x()-1)) == *ibeg );
				}
			}
		}
		
	private:
		static void copy_bag_pos(bag_iterator beg, bag_iterator end, iterator ebeg)
		{
			size_type n;
			for(;beg != end;++beg)
			{
				n = beg->size();
				beg->begin(ebeg);
				ebeg += n;
				beg->end(ebeg);
			}
		}
		static size_type indexof(size_type n)
		{
			return n;
		}
		template<class W1>
		static size_type indexof(std::pair<size_type, W1>& n)
		{
			return n.first;
		}

	public:
		/** Counts the number of examples that have a missing class value.
		 *
		 * @return the number of examples having a missing class.
		 */
		size_type countClassMissing()const
		{
			return exampleVec.countClassMissing();
		}
		/** Counts the number of examples that have a particular class label.
		 *
		 * @param y an internal class label.
		 * @return the number of examples having a particular class.
		 */
		size_type countClass(class_type y)const
		{
			return exampleVec.countClass(y);
		}
		/** Determines if this set has zero examples for a class.
		 *
		 * @return true if set has class with no examples
		 */
		bool hasZeroClass()const
		{
			class_type n = class_type(attribute_header::classCount());
			//if( n == 0 ) n = 2; // Hack for quanting algorithm: Regression has 0 classes, but classifier needs 2 classes
			for(class_type i=0;i<n;++i)
				if( countClass(i) == 0 ) return true;
			return false;
		}
		/** Counts the number of bags a missing class.
		 *
		 * @return the number of bags a missing class.
		 */
		size_type countBagClassMissing()const
		{
			return bagVec.countClassMissing();
		}
		/** Counts the number of bags with a particular class label.
		 *
		 * @param y an internal class label.
		 * @return the number of bags having a particular class.
		 */
		size_type countBagClass(class_type y)const
		{
			return bagVec.countClass(y);
		}
		/** Sets every instance in a positive bag as positive.
		 */
		void setPositive()
		{
			for(bag_iterator beg = bag_begin(), end = bag_end();beg != end;++beg)
			{
				if( beg->y() == bag_type::missingClass() ) continue;
				if( beg->y() > 0 )
				{
					for(iterator fbeg = beg->begin(), fend = beg->end();fbeg != fend;++fbeg) fbeg->y(1);
				}
			}
		}
		/** Gets the original index of an example.
		 *
		 * @param it an iterator pointing to an example.
		 * @return original index of the example.
		 */
		size_type indexof(const_iterator it)const
		{
			//ASSERT(bagCount() == 0);
			ASSERT(it != 0);
			ASSERT(it->x() != 0);
			return size_type( type_util::valueOf(*(it->x()-1)) );
		}
		/** Gets the original index of the bag.
		 *
		 * @param it an iterator pointing to a bag.
		 * @return original index of the bag.
		 */
		size_type indexof(const_bag_iterator it)const
		{
			ASSERT(it != 0);
			return indexof(it->x());
		}
		/** Gets an example at the absolute index.
		 *
		 * @param n absolute index.
		 * @return iterator to example at absolute index.
		 */
		const_iterator valueAtIndex(size_type n)const
		{
			const_iterator curr = begin();
			for(const_iterator fin=end();curr  != fin;++curr)
				if( indexof(curr) == n ) break;
			return curr;
		}
		/** Gets an example at the absolute index.
		 *
		 * @param n absolute index.
		 * @return iterator to example at absolute index.
		 */
		iterator valueAtIndex(size_type n)
		{
			iterator curr = begin();
			for(iterator fin=end();curr  != fin;++curr)
				if( indexof(curr) == n ) break;
			return curr;
		}
		/** Gets a bag at the absolute index.
		 *
		 * @param n absolute index.
		 * @return iterator to example at absolute index.
		 */
		bag_iterator bagAtIndex(size_type n)
		{
			bag_iterator curr = bag_begin();
			for(bag_iterator fin=bag_end();curr != fin;++curr)
				if( indexof(curr) == n ) break;
			return curr;
		}
		/** Orders a dataset using the source index collection.
		 *
		 * @param ibeg an iterator to start of index collection.
		 * @param iend an iterator to end of index collection.
		 */
		template<class I>
		void order(I ibeg, I iend)
		{
			ASSERT(std::distance(ibeg,iend) == size());
			for(iterator beg1 = begin(), end1=end(), curr;beg1 != end1;++beg1, ++ibeg)
			{
				curr=valueAtIndex(*ibeg);
				ASSERT(curr != end1);
				if( curr != beg1 ) std::swap(*beg1, *curr);
			}
		}
		

	public:
		/** Gets a string value of an attribute with the given value at the given index.
		 *
		 * @param val a attribute value.
		 * @param i an attribute index.
		 * @param mis a missing character.
		 * @return a string value.
		 */
		std::string getAttributeAt(feature_type val, size_type i, char mis)const
		{
			if( val == value_type::missing() )
			{
				std::string tmp(1, mis);
				return tmp;
			}
			else if( attributeAt(i).isnominal() )
			{
				ASSERTMSG( size_type(val) < attributeAt(i).size(), attributeAt(i).toString() << " - " << size_type(val) << " < " << attributeAt(i).size() << " - " << i  );
				return attributeAt(i)[ size_type(val) ];
			}
			else
			{
				std::string tmp;
				valueToString(val, tmp);
				return tmp;
			}
		}
		/** Gets a string value of a class for an example.
		 *
		 * @param ex an example.
		 * @param mis a missing character.
		 * @return a string value.
		 */
		std::string getClassAt(const_reference ex, char mis)const
		{
			if( ex.y() == value_type::missingClass() )
			{
				std::string tmp(1, mis);
				return tmp;
			}
			return classAt(ex.y());
		}
		/* Assigns a sample of the dataset to the current example set.
		 *
		 * @param dataset a source dataset.
		 * @param beg an iterator to start of index collection.
		 * @param end an iterator to end of index collection.
		 */
		/*template<class W1, class I>
		void assign(ExampleSet<X, Y, W1>& dataset, I ibeg, I iend)
		{
			ASSERT(std::distance(ibeg,iend) == size());
			resize(std::distance(ibeg,iend));
			for(iterator beg1 = begin(), end1=end();beg1 != end1;++beg1, ++ibeg)
				*beg1 = *dataset.valueAtIndex(*ibeg);
		}*/

		/** Write debug statistics to output stream.
		 *
		 * @param out an output stream.
		 * @param head a string header.
		 */
		void debugout(std::ostream& out, std::string head="")
		{
			std::string prefix = "# ";
			if( head != "" ) out << head << "\n";
			if(bagCount()>0)out << prefix << "Bags:       " << bagCount() << "\n";
			out << prefix << "Examples:   " << size() << "\n";
			out << prefix << "Attributes: " << attribute_header::attributeCount() << "\n";
			out << prefix << "Classes:    " << attribute_header::classCount() << "\n";
			for(size_type i=0;i<attribute_header::classCount();++i)
			{
				out << prefix << attribute_header::classAt(i) << ": " << countClass(i);
				if( bagCount() > 0 ) out << " & " << countBagClass(i) << "\n";
				else out << "\n";
			}
			out << std::endl;
		}

	protected:
		/** Recalculates the attribute statistics.
		 */
		void recalculate()
		{
			attribute_header::reset();
			for(iterator it = begin(), fin=end();it != fin;++it)
				attribute_header::recalculate(it->x());
		}
		/** Resizes attributes and recalculates attribute statistics.
		 *
		 * @param beg start of length array.
		 * @param end end of length array.
		 */
		void recalculate(size_type* beg, size_type* end)
		{
			if( !type_util::IS_SPARSE )
			{
				attribute_pointer abeg, aend;
				for(iterator it=begin();beg != end;++beg, ++it)
				{
					if( (*beg) < attribute_header::attributeCountInt )
					{
						it->x(it->x()-1);
						it->x(::resize(it->x(), attribute_header::attributeCountInt+1));
						it->x(it->x()+1);
						abeg = it->x()+(*beg);
						aend = it->x()+attribute_header::attributeCountInt;
						for(;abeg != aend;++abeg) type_util::setValue(*abeg, 0);
					}
					else if( (*beg) > attribute_header::attributeCountInt )
						std::cerr << "Warning: Trunacated attribute vector for example: " << (std::distance(begin(), it)+1) << " - Expected: " << attribute_header::attributeCountInt << " and Found: " << (*beg) <<  std::endl;
					attribute_header::recalculate(it->x(), *beg);
				}
			}
			else recalculate();
		}

	protected:
		example_vector exampleVec;
		bag_vector bagVec;
	};
	
	/** @brief Contains saved attributes
	 * 
	 * This class saves attribute vectors that change over time and the 
	 * classifier maintains only pointers to these vectors, e.g. LibSVM or CoverTree.
	 */
	template<class S, int USE>
	class example_pool
	{
		typedef typename S::value_type	 			value_type;
		typedef typename value_type::type_util		type_util;
		typedef typename value_type::value_type 	attribute_type;
		typedef typename value_type::pointer 		attribute_pointer;
		typedef typename value_type::const_pointer 	const_attribute_pointer;
		typedef attribute_pointer* 			 		set_pointer;
		typedef set_pointer* 			 			pool_pointer;
		typedef typename value_type::size_type 		size_type;
	public:
		/** Constructs an example pool.
		 * 
		 * @param n number of examples.
		 * @param a number of attributes.
		 * @param p number of pooled sets.
		 */
		example_pool(size_type n=0, size_type a=0, size_type p=1) : data(0), exampleCnt(n), attribCnt(a), poolCnt(p)
		{
			if( n > 0) resize_impl(n, p);
		}
		/** Destructs an example pool.
		 */
		~example_pool()
		{
			if( data != 0 ) 
			{
				erase_all(*data, (*data) + (exampleCnt*poolCnt));
				::erase(*data);
			}
			::erase(data);
		}
		
	private:
		static void init_all(set_pointer beg, set_pointer end)
		{
			for(;beg != end;++beg) *beg = 0;
		}
		static void erase_all(set_pointer beg, set_pointer end)
		{
			for(;beg != end;++beg) ::erase(*beg);
		}
		void resize_impl(size_type n, size_type p)
		{
			set_pointer val = 0;
			size_type new_size = n*p;
			size_type old_size = exampleCnt*poolCnt;
			if( data != 0 && new_size != old_size ) 
			{
				val = *data;
				if( new_size < old_size ) erase_all(val+new_size, val+old_size);
			}
			data  = ::resize(data, p);
			*data = val;
			if(new_size != old_size) *data = ::resize(*data, new_size);
			for(size_type i=1;i<p;++i) data[i] = data[i-1]+n;
			val = *data;
			if( new_size > old_size ) init_all(val+old_size, val+new_size);
		}
		
	public:
		/** Resize the example pool.
		 * 
		 * @param n number of examples.
		 * @param a number of attributes.
		 * @param p number of pooled sets.
		 */
		void resize(size_type n, size_type a, size_type p=1)
		{
			resize_impl(n, p);
			exampleCnt = n;
			poolCnt = p;
			attribCnt = a;
		}
		
	public:
		/** Appends an attribute vector to the pool.
		 * 
		 * @param from a source attribute vector.
		 * @param to a destination attribute vector.
		 * @param example index.
		 * @param pool index.
		 */
		attribute_pointer append(const_attribute_pointer from, size_type n, size_type p)
		{
			ASSERT(n < exampleCnt);
			ASSERT(p < poolCnt);
			attribute_pointer ptr = data[p][n];
			ptr = ::resize(ptr, attribCnt+1+type_util::IS_SPARSE)+1;
			size_type i=0;
			for(;type_util::isnotend(from, i, attribCnt);++i)
			{
				ASSERT(type_util::indexOf(from[i]) >= 0);
				type_util::set_value_index(ptr, i, type_util::valueOf(from[i]), type_util::indexOf(from[i]));
			}
			from--; ptr--;
			type_util::set_value_index(ptr, 0, type_util::valueOf(from[0]), type_util::indexOf(from[0]));
			ptr++;
			ASSERT(i<attribCnt);
			if( i < attribCnt ) 
			{
				--ptr;
				ptr = ::resize(ptr, i+1+type_util::IS_SPARSE)+1;
			}
			--i;
			type_util::marklast(ptr, i);
			data[p][n] = ptr-1;
			return ptr;
		}
		
	private:
		pool_pointer data;
		size_type exampleCnt;
		size_type attribCnt;
		size_type poolCnt;
	};
	/** @brief Contains saved attributes
	 * 
	 * This class saves attribute vectors that change over time and the 
	 * classifier maintains only pointers to these vectors, e.g. LibSVM or CoverTree.
	 */
	template<class S>
	class example_pool<S, 0>
	{
		typedef typename S::value_type	 			value_type;
		typedef typename value_type::pointer 		attribute_pointer;
		typedef typename value_type::size_type  	size_type;
	public:
		/** Constructs an example pool.
		 */
		example_pool()
		{
		}
		/** Resize the example pool.
		 * 
		 * @param n number of examples.
		 * @param a number of attributes.
		 * @param p number of pooled sets.
		 */
		void resize(size_type n, size_type a, size_type p=1)
		{
		}
		/** Appends an attribute vector to the pool.
		 * 
		 * @param from a source attribute vector.
		 * @param to a destination attribute vector.
		 * @param example index.
		 * @param pool index.
		 */
		attribute_pointer append(attribute_pointer from, size_type n, size_type p)
		{
			return from;
		}
	};
	
};

#endif

