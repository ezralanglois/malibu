/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ExampleSetAlgorithms.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_EXAMPLESETALGORITHMS_H
#define _EXEGETE_EXAMPLESETALGORITHMS_H
#include "debugutil.h"
#include "ExampleSet.h"
#include "ExampleAlgorithms.h"

/** @file ExampleSetAlgorithms.h
 * @brief Defines routines for sampling and shuffling.
 * 
 * This file contains a set of classes and global functions that shuffle or sample
 * a set of examples or bags.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Helper random number generator.
	 * 
	 * This subclass of the MersenneTwister class that holds extra data for shuffling 
	 * or sampling.
	 */
	class SampleRand : public MTRand
	{
	public:
		/** Constructs a random number generator.
		 */
		SampleRand() : alphaFlt(0.0f), errmsg(0)
		{
		}
		/** Gets an alpha parameter between 0 and 1.
		 * 
		 * @return alpha parameter.
		 */
		float& alpha()
		{
			return alphaFlt;
		}
		/** Gets an alpha parameter between 0 and 1.
		 * 
		 * @return alpha parameter.
		 */
		float alpha()const
		{
			return alphaFlt;
		}
		/** Sets the alpha parameter between 0 and 1.
		 * 
		 * @param f an alpha parameter.
		 */
		void alpha(float f)
		{
			alphaFlt = f;
		}
		/** Gets an error message.
		 * 
		 * @return an error message or NULL.
		 */
		const char* errormsg()const
		{
			return errmsg;
		}

	private:
		MTRand& parent() { return *this; }
		const MTRand& parent()const { return *this; }
		template<class S>
		S& errormsg(S& s, const char* m)const
		{
			errmsg = m;
			s.setstate( std::ios::failbit );
			return s;
		}
		/** Reads a random number generator state from an input stream.
		 * 
		 * @param in an input stream.
		 * @param rand a random number generator.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, SampleRand& rand)
		{
			if( in.get() != '!' ) return rand.errormsg(in, ERRORMSG("Missing start character"));
			if( in.get() != '\t' ) return rand.errormsg(in, ERRORMSG("Missing start separator"));
			in >> rand.parent();
			if( in.get() != '\t' ) return rand.errormsg(in, ERRORMSG("Missing end separator"));
			if( in.get() != '!' ) return rand.errormsg(in, ERRORMSG("Missing end character"));
			return in;
		}
		/** Writes a random number generator state to an output stream.
		 * 
		 * @param out an output stream.
		 * @param rand a random number generator.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const SampleRand& rand)
		{
			out << "!\t";
			out << rand.parent();
			out << "\t!";
			return out;
		}
	private:
		float alphaFlt;
		mutable const char* errmsg;
	};
	/** @brief Helper utility to assigns an example index.
	 * 
	 * This helper class template handles assignment of a value when sampling. This 
	 * default template assigns the index of the example to the destination iterator.
	 */
	template<class T, class U>
	class ExampleAssign
	{
		typedef typename T::iterator iterator1;
		typedef typename U::const_iterator iterator2;
	public:
		/** Assigns the index of the source iterator to a destination iterator.
		 *
		 * @param it1 destination iterator.
		 * @param beg2 a source start iterator.
		 * @param it2 a source iterator.
		 */
		static void assign(iterator1 it1, iterator2 beg2, iterator2 it2)
		{
			(*it1) = std::distance(beg2, it2);
		}
	};
	/** @brief Helper utility to assigns an example index to a pair.
	 * 
	 * This helper class template handles assignment when sampling. This default
	 * template assigns the index of the example to the destination pair iterator.
	 */
	template<class T, class U1, class U2>
	class ExampleAssign<T, std::vector< std::pair< U1,U2> > >
	{
		typedef typename T::iterator iterator1;
		typedef typename std::vector< std::pair<U1,U2> >::const_iterator iterator2;
	public:
		/** Assigns the index of the source iterator to a destination iterator pair.
		 *
		 * @param it1 destination iterator.
		 * @param beg2 a source start iterator.
		 * @param it2 a source iterator.
		 */
		static void assign(iterator1 it1, iterator2 beg2, iterator2 it2)
		{
			it1->first = std::distance(beg2, it2);
		}
		/** Gets weight of iterator value pair.
		 * 
		 * @param it a source iterator.
		 * @return weight of example.
		 */
		static U2 weightof(iterator2 it)
		{
			return it->second;
		}
	};
	/** @brief Helper utility to assign an example to another example.
	 * 
	 * This helper class template handles assignment when sampling. This specialized 
	 * template is used when both templates types are equivalent example sets.
	 */
	template<class X, class Y, class W1, class W2>
	class ExampleAssign< ExampleSet<X,Y,W1>, ExampleSet<X,Y,W2> >
	{
		typedef typename ExampleSet<X,Y,W1>::iterator iterator1;
		typedef typename ExampleSet<X,Y,W2>::const_iterator iterator2;
	public:
		/** Assigns the source iterator to a destination iterator.
		 *
		 * @param it1 destination iterator.
		 * @param beg2 a source start iterator.
		 * @param it2 a source iterator.
		 */
		static void assign(iterator1 it1, iterator2 beg2, iterator2 it2)
		{
			(*it1) = (*it2);
		}
		/** Gets weight of iterator value.
		 * 
		 * @param it a source iterator.
		 * @return weight of example.
		 */
		static W2 weightof(iterator2 it)
		{
			return it->w();
		}
	};


	/** Samples a source dataset without replacement:
	 * 	- Selected examples saved to subset1
	 * 	- Unselected examples saved to subset2
	 * 
	 * The proportion of selected examples is controlled by the alpha parameter.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 * @param alpha the proportion of examples to select.
	 */
	template<class T, class U1, class U2>
	void sample(const T& dataset, U1& subset1, U2& subset2, float alpha)
	{
		typedef ExampleAssign< U1, T > assign_type1;
		typedef ExampleAssign< U2, T > assign_type2;
		typedef typename T::const_iterator const_iterator;
		typedef typename U1::iterator iterator1;
		typedef typename U2::iterator iterator2;
		subset1.resize( dataset.size() );
		subset2.resize( dataset.size() );

		iterator1 it1 = subset1.begin();
		iterator2 it2 = subset2.begin();
		for(const_iterator b=dataset.begin(), e=dataset.end();b != e;++b)
		{
			if( random_double() < alpha )
			{
				assign_type1::assign(it1, dataset.begin(), b);
				++it1;
			}
			else
			{
				assign_type2::assign(it2, dataset.begin(), b);
				++it2;
			}
		}
		subset1.resize( std::distance(subset1.begin(), it1) );
		subset2.resize( std::distance(subset2.begin(), it2) );
	}

	/** Samples a source dataset with replacement:
	 * 	- Selected examples are saved to subset1
	 * 	- Unselected examples are saved to subset2
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 */
	template<class T, class U1, class U2>
	void sample(const T& dataset, U1& subset1, U2& subset2)
	{
		typedef ExampleAssign< U1, T > assign_type1;
		typedef ExampleAssign< U2, T > assign_type2;
		typedef typename T::const_iterator const_iterator;
		typedef typename U1::iterator iterator1;
		typedef typename U2::iterator iterator2;
		subset1.resize( dataset.size() );
		subset2.resize( dataset.size() );
		iterator1 it1 = subset1.begin(), end1=subset1.end();
		iterator2 it2 = subset2.begin();
		double psum, csum = 0.0, delta = 1.0 / double(dataset.size());
		double* prob = new double[dataset.size()], *pbeg=prob;
		initprob(prob, prob+dataset.size());
		for(const_iterator b=dataset.begin(), e=dataset.end();b != e;++b, ++pbeg)
		{
			psum = *pbeg;
			if( csum >= psum )
			{
				assign_type2::assign(it2, dataset.begin(), b);
				++it2;
			}
			for(;it1 != end1 && csum < psum;csum+=delta,++it1) assign_type1::assign(it1, dataset.begin(), b);
			if( it1 == end1 ) csum=2.0;
		}
		delete[] prob;
		subset2.resize( std::distance(subset2.begin(), it2) );
		ASSERT(it1 == end1);
	}

	
	/** Samples a source dataset without replacement:
	 * 	- Selected examples saved to subset1
	 * 	- Unselected examples saved to subset2
	 * 
	 * The proportion of selected examples is controlled by the alpha parameter.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 * @param rand a random number generator and the proportion of examples to select.
	 */
	template<class T, class U1, class U2>
	void sample_without_replacement(const T& dataset, U1& subset1, U2& subset2, SampleRand& rand)
	{
		typedef ExampleAssign< U1, T > assign_type1;
		typedef ExampleAssign< U2, T > assign_type2;
		typedef typename T::const_iterator const_iterator;
		typedef typename U1::iterator iterator1;
		typedef typename U2::iterator iterator2;
		subset1.resize( dataset.size() );
		subset2.resize( dataset.size() );
		float alpha = rand.alpha();

		iterator1 it1 = subset1.begin();
		iterator2 it2 = subset2.begin();
		for(const_iterator b=dataset.begin(), e=dataset.end();b != e;++b)
		{
			if( rand.rand() < alpha ) 
			{
				assign_type1::assign(it1, dataset.begin(), b);
				++it1;
			}
			else
			{
				assign_type2::assign(it2, dataset.begin(), b);
				++it2;
			}
		}
		subset1.resize( std::distance(subset1.begin(), it1) );
		subset2.resize( std::distance(subset2.begin(), it2) );
	}

	/** Samples a source dataset with replacement:
	 * 	- Selected examples are saved to subset1
	 * 	- Unselected examples are saved to subset2
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 * @param rand a random number generator and the proportion of examples to select.
	 */
	template<class T, class U1, class U2>
	void sample_with_replacement(const T& dataset, U1& subset1, U2& subset2, SampleRand& rand)
	{
		typedef ExampleAssign< U1, T > assign_type1;
		typedef ExampleAssign< U2, T > assign_type2;
		typedef typename T::const_iterator const_iterator;
		typedef typename U1::iterator iterator1;
		typedef typename U2::iterator iterator2;
		subset1.resize( dataset.size() );
		subset2.resize( dataset.size() );
		iterator1 it1 = subset1.begin(), end1=subset1.end();
		iterator2 it2 = subset2.begin();
		double psum, csum = 0.0, delta = 1.0 / double(dataset.size());
		double* prob = new double[dataset.size()], *pbeg=prob;
		initprob(prob, prob+dataset.size(), rand);
		for(const_iterator b=dataset.begin(), e=dataset.end();b != e;++b, ++pbeg)
		{
			psum = *pbeg;
			if( csum >= psum )
			{
				assign_type2::assign(it2, dataset.begin(), b);
				++it2;
			}
			for(;it1 != end1 && csum < psum;csum+=delta,++it1) assign_type1::assign(it1, dataset.begin(), b);
			if( it1 == end1 ) csum=2.0;
		}
		delete[] prob;
		subset2.resize( std::distance(subset2.begin(), it2) );
		ASSERT(it1 == end1);
	}

	/** Samples a source dataset without replacement:
	 * 	- Selected examples are saved to subset1 
	 * 
	 * The proportion of selected examples is controlled by the alpha parameter.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param alpha the proportion of examples to select.
	 */
	template<class T, class U>
	void sample(const T& dataset, U& subset1, double alpha)
	{
		typedef ExampleAssign< U, T > assign_type;
		typedef typename T::const_iterator const_iterator;
		typedef typename U::iterator iterator;
		subset1.resize( dataset.size() );

		iterator it1 = subset1.begin();
		for(const_iterator b=dataset.begin(), e=dataset.end();b != e;++b)
		{
			if( random_double() < alpha ) 
			{
				assign_type::assign(it1, dataset.begin(), b);
				++it1;
			}
		}
		subset1.resize( std::distance(subset1.begin(), it1) );
	}

	/** Samples a source dataset with replacement:
	 * 	- Selected examples are saved to subset1
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 */
	template<class T, class U>
	void sample(const T& dataset, U& subset1)
	{
		typedef ExampleAssign< U, T > assign_type1;
		typedef typename T::const_iterator const_iterator;
		typedef typename U::iterator iterator;
		subset1.resize( dataset.size() );

		iterator it1 = subset1.begin(), end1=subset1.end();
		double psum, csum = 0.0, delta = 1.0 / double(dataset.size());
		double* prob = new double[dataset.size()], *pbeg=prob;
		initprob(prob, prob+dataset.size());
		for(const_iterator b=dataset.begin(), e=dataset.end();b != e;++b, ++pbeg)
		{
			psum = *pbeg;
			for(;it1 != end1 && csum < psum;csum+=delta,++it1) assign_type1::assign(it1, dataset.begin(), b);
		}
		delete[] prob;
		ASSERT(it1 == end1);
	}

	/** Samples without replacement a source dataset with a weighted bias:
	 * 	- Selected examples are saved to subset1
	 * 	- Unselected examples are saved to subset2
	 * 
	 * The proportion of examples is determined by the example weights and the 
	 * alpha parameter acts as a normalization parameter.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class T, class U1, class U2>
	void wsample(const T& dataset, U1& subset1, U2& subset2, double alpha)
	{
		typedef ExampleAssign< U1, T > assign_type1;
		typedef ExampleAssign< U2, T > assign_type2;
		typedef typename T::const_iterator const_iterator;
		typedef typename U1::iterator iterator1;
		typedef typename U2::iterator iterator2;
		subset1.resize( dataset.size() );
		subset2.resize( dataset.size() );

		iterator1 it1 = subset1.begin();
		iterator2 it2 = subset2.begin();
		for(const_iterator b=dataset.begin(), e=dataset.end();b != e;++b)
		{
			if( random_double() < assign_type1::weightof(b)*alpha )
			{
				assign_type1::assign(it1, dataset.begin(), b);
				++it1;
			}
			else
			{
				assign_type2::assign(it2, dataset.begin(), b);
				++it2;
			}
		}
		subset1.resize( std::distance(subset1.begin(), it1) );
		subset2.resize( std::distance(subset2.begin(), it2) );
	}

	/** Samples a source dataset with replacement biased with weights:
	 * 	- Selected saved in subset1.
	 * 	- Unselected saved in subset2.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 */
	template<class T, class U1, class U2>
	void wsample(const T& dataset, U1& subset1, U2& subset2)
	{
		typedef ExampleAssign< U1, T > assign_type1;
		typedef ExampleAssign< U2, T > assign_type2;
		typedef typename T::const_iterator const_iterator;
		typedef typename U1::iterator iterator1;
		typedef typename U2::iterator iterator2;
		subset1.resize( dataset.size() );
		subset2.resize( dataset.size() );

		iterator1 it1 = subset1.begin(), end1=subset1.end();
		iterator2 it2 = subset2.begin();
		double psum = 0.0, csum = 0.0;
		double* prob = new double[dataset.size()], *pbeg=prob;
		initprob(prob, prob+dataset.size());
		prob[dataset.size()-1] = 0.9999;
		const_iterator b=dataset.begin(), e=dataset.end();
		for(;b != e;++b)
		{
			psum+=assign_type1::weightof(b);
			if( csum >= psum )
			{
				assign_type2::assign(it2, dataset.begin(), b);
				++it2;
			}
			for(;it1 != end1 && (*pbeg) < psum;++pbeg,++it1) assign_type1::assign(it1, dataset.begin(), b);
		}
		for(--b;it1 != end1;++it1) assign_type1::assign(it1, dataset.begin(), b);
		delete[] prob;
		ASSERT(it1 == end1);
	}

	/** Samples a source dataset without replacement biased with weights:
	 * 	- Selected saved in subset1.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class T, class U1>
	void wsample(const T& dataset, U1& subset1, double alpha)
	{
		typedef ExampleAssign< U1, T > assign_type1;
		typedef typename T::const_iterator const_iterator;
		typedef typename U1::iterator iterator1;
		subset1.resize( dataset.size() );

		iterator1 it1 = subset1.begin();
		for(const_iterator b=dataset.begin(), e=dataset.end();b != e;++b)
		{
			if( random_double() < assign_type1::weightof(b)*alpha )
			{
				assign_type1::assign(it1, dataset.begin(), b);
				++it1;
			}
		}
		subset1.resize( std::distance(subset1.begin(), it1) );
	}

	/** Samples a source dataset with replacement biased with weights:
	 * 	- Selected saved in subset1.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 */
	template<class T, class U1>
	void wsample(const T& dataset, U1& subset1)
	{
		typedef ExampleAssign< U1, T > assign_type1;
		typedef typename T::const_iterator const_iterator;
		typedef typename U1::iterator iterator1;
		subset1.resize( dataset.size() );

		iterator1 it1 = subset1.begin(), end1=subset1.end();
		double psum = 0.0;//, csum = 0.0;
		double* prob = new double[dataset.size()], *pbeg=prob;
		initprob(prob, prob+dataset.size());
		prob[dataset.size()-1] = 0.9999;
		const_iterator b=dataset.begin(), e=dataset.end();
		for(;b != e;++b)
		{
			psum+=assign_type1::weightof(b);
			for(;it1 != end1 && (*pbeg) < psum;++pbeg,++it1) assign_type1::assign(it1, dataset.begin(), b);
		}
		for(--b;it1 != end1;++it1) assign_type1::assign(it1, dataset.begin(), b);
		delete[] prob;
		ASSERT(it1 == end1);
	}
	/** Samples only instances in a source dataset with or without replacement biased with weights:
	 * 	- Selected saved in subset1.
	 * 
	 * If alpha is zero then sample with replacement.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class X, class Y, class W, class U1>
	void instance_wsample(const ExampleSet<X,Y,W>& dataset, U1& subset1, double alpha)
	{
		if( alpha > 0.0 ) wsample(dataset, subset1, alpha);
		else wsample(dataset, subset1);
	}
	/** Samples only instances in a source dataset with or without replacement biased with weights:
	 * 	- Selected saved in subset1.
	 * 	- Unselected saved in subset2.
	 * 
	 * If alpha is zero then sample with replacement.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class X, class Y, class W, class U1, class U2>
	void instance_wsample(const ExampleSet<X,Y,W>& dataset, U1& subset1, U2& subset2, double alpha)
	{
		if( alpha > 0.0 ) wsample(dataset, subset1, subset2, alpha);
		else wsample(dataset, subset1, subset2);
	}
	
	/** Samples only instances in a source dataset with or without replacement:
	 * 	- Selected saved in subset1.
	 * 
	 * If alpha is zero then sample with replacement.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class X, class Y, class W, class U1>
	void instance_sample(const ExampleSet<X,Y,W>& dataset, U1& subset1, double alpha)
	{
		if( alpha > 0.0 ) sample(dataset, subset1, alpha);
		else sample(dataset, subset1);
	}
	/** Samples only instances in a source dataset with or without replacement:
	 * 	- Selected saved in subset1.
	 * 	- Unselected saved in subset2.
	 * 
	 * If alpha is zero then sample with replacement.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class X, class Y, class W, class U1, class U2>
	void instance_sample(const ExampleSet<X,Y,W>& dataset, U1& subset1, U2& subset2, double alpha)
	{
		if( alpha > 0.0 ) sample(dataset, subset1, subset2, alpha);///???
		else sample(dataset, subset1, subset2);
	}
	
	/** Samples only bags in a source dataset with or without replacement:
	 * 	- Selected saved in subset1.
	 * 	- Unselected saved in subset2.
	 * 
	 * If alpha is zero then sample with replacement biased with weights.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class X, class Y, class W, class U1, class U2>
	void bag_wsample(const ExampleSet<X,Y,W>& dataset, U1& subset1, U2& subset2, double alpha)
	{
		typedef std::vector< std::pair< typename ExampleSet<X,Y,W>::size_type, typename ExampleSet<X,Y,W>::weight_type> > index_vector;
		index_vector dataset_tmp(dataset.bagCount()), subset_tmp1, subset_tmp2;
		for(typename ExampleSet<X,Y,W>::size_type i=0;i<dataset_tmp.size();++i) dataset_tmp[i] = std::make_pair(i,dataset[i].w());
		if( alpha > 0.0 ) wsample(dataset_tmp, subset_tmp1, subset_tmp2, alpha);
		else wsample(dataset_tmp, subset_tmp1, subset_tmp2);
		subset1.assign(dataset.bag_begin(), dataset.bag_end(), subset_tmp1.begin(), subset_tmp1.end());
		subset2.assign(dataset.bag_begin(), dataset.bag_end(), subset_tmp2.begin(), subset_tmp2.end());
	}
	/** Samples only bags in a source dataset with or without replacement biased with weights:
	 * 	- Selected saved in subset1.
	 * 
	 * If alpha is zero then sample with replacement.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class X, class Y, class W, class U1>
	void bag_wsample(const ExampleSet<X,Y,W>& dataset, U1& subset1, double alpha)
	{
		typedef std::vector< std::pair< typename ExampleSet<X,Y,W>::size_type, typename ExampleSet<X,Y,W>::weight_type> > index_vector;
		typedef IsVoid<W> ISVOID;
		BOOST_STATIC_ASSERT( !ISVOID::value );
		index_vector dataset_tmp(dataset.bagCount()), subset_tmp;
		for(typename ExampleSet<X,Y,W>::size_type i=0;i<dataset_tmp.size();++i) dataset_tmp[i] = std::make_pair(i,dataset[i].w());
		if( alpha > 0.0 ) wsample(dataset_tmp, subset_tmp, alpha);
		else wsample(dataset_tmp, subset_tmp);
		subset1.assign(dataset.bag_begin(), dataset.bag_end(), subset_tmp.begin(), subset_tmp.end());
	}
	/** Samples a source dataset with or without replacement biased with weights:
	 * 	- Selected saved in subset1.
	 * 
	 * If alpha is zero then sample with replacement. Also, if the dataset has bags then
	 * sample bags otherwise instances.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param alpha a normalization parameter.
	 */
	template<class X, class Y, class W, class U>
	void standard_sample(ExampleSet<X,Y,W>& dataset, U& subset1, double alpha)
	{
		typedef std::vector< typename ExampleSet<X,Y,W>::size_type > index_vector;
		if( dataset.bagCount() > 0 )
		{
			index_vector dataset_tmp(dataset.bagCount()), subset_tmp;
			for(unsigned int i=0;i<dataset_tmp.size();++i) dataset_tmp[i] = i;
			if( alpha > 0.0 ) sample(dataset_tmp, subset_tmp, alpha);
			else sample(dataset_tmp, subset_tmp);
			subset1.assign(dataset.bag_begin(), dataset.bag_end(), subset_tmp.begin(), subset_tmp.end());
		}
		else
		{
			if( alpha > 0.0 ) sample(dataset, subset1, alpha);
			else sample(dataset, subset1);
		}
	}
	
	/** Samples a source dataset with or without replacement biased with weights:
	 * 	- Selected saved in subset1.
	 * 	- Unselected saved in subset2.
	 * 
	 * If alpha is zero then sample with replacement. Also, if the dataset has bags then
	 * sample bags otherwise instances.
	 *
	 * @param dataset a reference to a source example set.
	 * @param subset1 a reference to a destination example set for selected examples.
	 * @param subset2 a reference to a destination example set for unselected examples.
	 * @param rand a random number generator and a sampling parameter.
	 */
	template<class X, class Y, class W, class U1, class U2>
	void standard_sample(ExampleSet<X,Y,W>& dataset, U1& subset1, U2& subset2, SampleRand& rand)
	{
		typedef std::vector< typename ExampleSet<X,Y,W>::size_type > index_vector;
		if( dataset.bagCount() > 0 )
		{
			index_vector dataset_tmp(dataset.bagCount()), subset_tmp1, subset_tmp2;
			for(unsigned int i=0;i<dataset_tmp.size();++i) dataset_tmp[i] = i;
			if( rand.alpha() > 0.0 ) sample_without_replacement(dataset_tmp, subset_tmp1, subset_tmp2, rand);
			else sample_with_replacement(dataset_tmp, subset_tmp1, subset_tmp2, rand);
			subset1.assign(dataset.bag_begin(), dataset.bag_end(), subset_tmp1.begin(), subset_tmp1.end());
			subset2.assign(dataset.bag_begin(), dataset.bag_end(), subset_tmp2.begin(), subset_tmp2.end());
		}
		else
		{
			if( rand.alpha() > 0.0 ) sample_without_replacement(dataset, subset1, subset2, rand);
			else sample_with_replacement(dataset, subset1, subset2, rand);
		}
	}
	
	/** Shuffles a source dataset using the size of the narray vector as the number of folds. If the 
	 * example set has bags it shuffles on the bag level otherwise on the example level. A bool flag 
	 * denotes whether stratification should be used.
	 *
	 * @param dataset a reference to a source example set.
	 * @param narray a reference to a vector that holds the partitions.
	 * @param rand a random number generator.
	 * @param stratify set true to stratify the class distribution across partitions.
	 */
	template<class X, class Y, class W>
	void shuffle(ExampleSet<X,Y,W>& dataset, std::vector<typename ExampleSet<X,Y,W>::size_type>& narray, MTRand& rand, bool stratify)
	{
		typedef typename ExampleSet<X,Y,W>::size_type size_type;
		if( dataset.bagCount() > 0 )
		{
			if(stratify) 
			{
				size_type p = dataset.countBagClass(1);
				stratify = (p > narray.size() && (dataset.bagCount()-p) > narray.size());
			}
			if(stratify) stratify_shuffle(dataset.bag_begin(), dataset.bag_end(), narray, rand);
			else		 standard_shuffle(dataset.bag_begin(), dataset.bag_end(), narray, rand);
		}
		else
		{
			if(stratify) 
			{
				size_type p = dataset.countClass(1);
				stratify = (p > narray.size() && (dataset.size()-p) > narray.size());
			}
			if(stratify) stratify_shuffle(dataset.begin(), dataset.end(), narray, rand);
			else		 standard_shuffle(dataset.begin(), dataset.end(), narray, rand);
		}
	}
};

#endif


