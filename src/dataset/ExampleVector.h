/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ExampleVector.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_EXAMPLEVECTOR_H
#define _EXEGETE_EXAMPLEVECTOR_H
#include "memoryutil.h"
#include "errorutil.h"

/** @file ExampleVector.h
 * @brief Defines a collection of examples.
 * 
 * This file contains the ExampleVector class template.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a collection of examples.
	 * 
	 * This class holds a vector of examples.
	 */
	template<class E>
	class ExampleVector
	{
		template<class E1> friend class ExampleVector;
	public:
		/** Defines an Example as a value type. **/
		typedef E			value_type;
		/** Defines a pointer to an Example as a pointer type. **/
		typedef E*			pointer;
		/** Defines a constant pointer to an Example as a constant pointer type. **/
		typedef const E*	const_pointer;
		/** Defines a pointer to an Example as an iterator type. **/
		typedef E*			iterator;
		/** Defines a constant pointer to an Example as a constant iterator type. **/
		typedef const E*	const_iterator;
		/** Defines a constant reference to an Example as a constant reference type. **/
		typedef const E&	const_reference;
		/** Defines a reference to an Example as a reference type. **/
		typedef E&			reference;
		/** Defines a size_t as a size type. **/
		typedef size_t		size_type;
		/** Defines a pointer difference as a difference type. */
		typedef ptrdiff_t   difference_type;
	public:
		/** Defines an example type utility as a type utility. **/
		typedef typename value_type::type_util			type_util;
		/** Defines an example class type. **/
		typedef typename value_type::class_type			class_type;
		/** Defines an example attribute type as an attribute type. **/
		typedef typename value_type::attribute_type		attribute_type;
		/** Defines an example value type as a feature type. **/
		typedef typename value_type::value_type			feature_type;
		/** Defines an example attribute pointer as an attribute pointer. **/
		typedef typename value_type::pointer			attribute_pointer;
		/** Defines an example constant attribute pointer as a constant attribute pointer. **/
		typedef typename value_type::const_pointer		const_attribute_pointer;
		/** Defines an example index type as an index type. **/
		typedef typename value_type::index_type			index_type;

	public:
		/** Constructs an example vector.
		 * 
		 * @param n number of examples.
		 */
		ExampleVector(size_type n=0) : ptr(0), len(0), maxlen(0)
		{
			if( n > 0 ) ptr = ::resize(ptr, n, maxlen);
		}
		/** Destructs an example vector.
		 */
		~ExampleVector()
		{
			::erase(ptr);
		}

	public:
		/** Constructs a copy of an example vector.
		 *
		 * @param vec an example vector to copy.
		 * @param n number of examples.
		 */
		template<class E1>
		ExampleVector(const ExampleVector<E1>& vec, size_type n=TypeUtil<size_type>::max()) : ptr(0), len(vec.len), maxlen(0)
		{
			if( n == TypeUtil<size_type>::max() ) ptr = ::resize(ptr, len, maxlen);			
			else resize(n);
		}
		/** Assigns a copy of an example vector.
		 *
		 * @param vec an example vector to copy.
		 * @return a reference to this object.
		 */
		template<class E1>
		ExampleVector& operator=(const ExampleVector<E1>& vec)
		{
			maxlen = len = vec.len;
			ptr = ::resize(ptr, len, maxlen);
			//std::copy(vec.ptr, vec.ptr+len, ptr);
			return *this;
		}

	public:
		/** Gets an example at the specified index.
		 *
		 * @param n an index of the example.
		 * @return a reference to a specific example.
		 */
		reference operator[](size_type n)
		{
			ASSERT( ptr != 0 );
			ASSERTMSG(n < len, n << " < " << len);
			return ptr[n];
		}
		/** Gets a constant example reference at the specified index.
		 *
		 * @param n an index of the example.
		 * @return a constant reference to a specific example.
		 */
		const_reference operator[](size_type n)const
		{
			ASSERT( ptr != 0 );
			ASSERTMSG(n < len, n << " < " << len);
			return ptr[n];
		}
		/** Gets the number of examples.
		 *
		 * @return the number of examples.
		 */
		size_type size()const
		{
			return len;
		}
		/** Tests if an example vector is empty.
		 * 
		 * @return true if example vector is empty.
		 */
		bool empty()const
		{
			return len == 0;
		}
		/** Gets the possible number of examples.
		 *
		 * @return the example capacity size.
		 */
		size_type capacity()const
		{
			return maxlen;
		}

	public:
		/** Gets an iterator to the start of an example collection.
		 *
		 * @return a begin example iterator.
		 */
		iterator begin()
		{
			return ptr;
		}
		/** Gets an iterator to the end of example collection.
		 *
		 * @return an end example iterator.
		 */
		iterator end()
		{
			return ptr+len;
		}
		/** Gets an iterator to the start of example collection.
		 *
		 * @return a begin example iterator.
		 */
		const_iterator begin()const
		{
			return ptr;
		}
		/** Gets an iterator to the end of an example collection.
		 *
		 * @return an end example iterator.
		 */
		const_iterator end()const
		{
			return ptr+len;
		}

	public:
		/** Resets the length to zero.
		 */
		void reset()
		{
			len = 0;
		}
		/** Sets the size of the example vector.
		 *
		 * @param n the new size of the vector.
		 */
		void setsize(size_type n)
		{
			ptr = ::resize(ptr, n, maxlen);
			maxlen = n;
		}
		/** Resizes the example collection to the specific size.
		 *
		 * @param n a new size.
		 */
		void resize(size_type n)
		{
			if( n != maxlen ) setsize(n);
			len = n;
		}
		/** Clears the memory in the example vector.
		 */
		void clear()
		{
			erase(ptr);
			len = 0;
			maxlen = 0;
		}
		/** Assigns a collection of examples to the ExampleSet.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class E1>
		void assign(E1* beg, E1* end)
		{
			len=0;
			append(beg, end);
		}
		/** Appends a collection of examples to the ExampleSet.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param fin an iterator to the end of the collection.
		 */
		template<class E1>
		void append(E1* beg, E1* fin)
		{
			size_type n = (size_type)(len+(fin-beg));
			if( maxlen < n ) setsize(n);
			std::copy(beg, fin, end());
			len=n;
		}
		/** Appends an example to the ExampleSet and changes the class.
		 *
		 * @param curr iterator to example to add.
		 * @param y a class value.
		 */
		template<class E1>
		void append(E1* curr, class_type y)
		{
			*end() = *curr;
			end()->y(y);
			len++;
		}

	public:
		/** Counts the number of examples that have a particular class label.
		 *
		 * @param y an internal class label.
		 * @return the number of examples having a particular class.
		 */
		size_type countClass(class_type y)const
		{
			size_type tot = 0;
			for(const_iterator it=begin();it != end();++it) if( it->y() == y ) tot++;
			return tot;
		}
		/** Counts the number of examples that have a missing class value.
		 *
		 * @return the number of examples having a missing class.
		 */
		size_type countClassMissing()const
		{
			return countClass(value_type::missingClass());
		}

	private:
		pointer ptr;
		size_type len;
		size_type maxlen;
	};
};

#endif


