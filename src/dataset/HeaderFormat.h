/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * HeaderFormat.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_HEADERFORMAT_H
#define _EXEGETE_HEADERFORMAT_H
#include "ConcreteExampleSet.h"
#include <fstream>
#include <vector>

/** @file HeaderFormat.h
 * @brief A format describing an attribute description format.
 * 
 * This file contains the HeaderFormat class, which defines the format 
 * of a set of examples.
 *
 * @todo Remove this class or use it
 * 
 * @warning Unused!
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief A format describing an attribute description format.
	 * 
	 * This class defines the format for a dataset header.
	 * 
	 * @warning Unused!
	 */
	template<class X, class Y>
	class HeaderFormat
	{
		typedef ConcreteExampleSet<X,Y> dataset_type;
		typedef std::vector< std::string > string_vector;
	public:
		/** Constructs a header format.
		 * 
		 * @param map an argument map.
		 */
		HeaderFormat(ArgumentMap& map)
		{
		}
		/** Destructs a header format.
		 */
		~HeaderFormat()
		{
		}
		
	public:
		/** Reads a header format from a file.
		 * 
		 * 1. Sets headers names (if necessary)
		 * 2. Sets nominal attributes
		 * 3. Converts binary to nominal?
		 * 
		 * @param file a header file.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		const char* read(const std::string& file, dataset_type& dataset)
		{
			const char* msg;
			std::ifstream fin;
			std::string name, line, lbl;
			string_vector ar;
			unsigned int attrn=0,i;//,labln=0;
			if( (msg=openfile(fin, file)) != 0 ) return msg;
			while( !fin.eof() )
			{
				std::getline(fin, line);
				if( fin.peek() == -1 ) fin.setstate( std::ios::eofbit );
				if( line.empty() ) continue;
				stringToValue(line, ar, ":");
				if( ar.size() == 2 )
				{
					name = ar[0];
					line = trim(ar[1]);
					if( *line.rbegin() == '.') line.erase(--line.end());
				}
				else return ERRORMSG("Each line should have : as a separator - " << line);
				if( stricmpn(name.c_str(), "class", 5) )
				{
					//dataset.classHeader()
				}
				else
				{
					valueToString(attrn+1, lbl);
					lbl = "Attribute_" + lbl;
					if( dataset.attributeAt(attrn).name() == lbl || dataset.attributeAt(attrn).name() == "" )
					{
						dataset.attributeAt(attrn) = name;
						i=attrn;
						attrn++;
					}
					else
					{
						for(i=0;i<dataset.attributeCount();++i)
							if( dataset.attributeAt(i).name() == name ) break;
					}
					if( i >= dataset.attributeCount() ) return ERRORMSG("Could not find attribute with name " << name << " " << i << " " << attrn << " " << dataset.attributeCount() << " -> " << dataset.attributeAt(attrn).name());
					if( line == "label" || line == "continuous" ) continue;
					if( !dataset.attributeAt(i).empty() ) continue;// return ERRORMSG("Reordering attributes not support: Attribute " << name << " not empty");
					stringToValue(line, ar, ",");
					if( ar.size() < 2 ) return ERRORMSG("Nominal parsing failed for Attribute " << name << ", found = " << line);
					if( ar.size() != dataset.attributeAt(i).range() ) 
					{
						return ERRORMSG("Number of nominal values not match for attribute " << i << ": dataset(" << dataset.attributeAt(i).range() << ") and header(" << ar.size() << " - " << line << ")");
					}
					dataset.attributeAt(i).assign(ar.begin(), ar.end());
				}
			}
			fin.close();
			dataset.recalculate();
			return 0;
		}
	};
	
};


#endif


