/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ShuffleAlgorithms.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_SHUFFLEALGORITHMS_H
#define _EXEGETE_SHUFFLEALGORITHMS_H
#include "mathutil.h"
#include "MersenneTwister.h"
#include <vector>
#include <algorithm>


/** @file ShuffleAlgorithms.h
 * @brief Defines algorithms for shuffling a collection.
 * 
 * This file contains a set of global functions that shuffle a collection of examples.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** Shuffles a collection of examples randomly.
	 *
	 * @param beg a start iterator to a collection of examples.
	 * @param end a end iterator to a collection of examples.
	 */
	template<class I>
	void shuffle(I beg, I end)
	{
		unsigned int total = (unsigned int)std::distance(beg, end);
		for(I curr1=beg, curr2;curr1 != end;++curr1)
		{
			curr2 = beg+random_int(total-std::distance(beg, curr1)-1);
			std::swap(*curr1, *curr2);
		}
	}
	/** Shuffles a collection of examples randomly.
	 *
	 * @param beg a start iterator to a collection of examples.
	 * @param end a end iterator to a collection of examples.
	 * @param mtrand a random number generator.
	 */
	template<class I>
	void shuffle(I beg, I end, MTRand& mtrand)
	{
		unsigned int total = (unsigned int)std::distance(beg, end);
		for(I curr1=beg, curr2;curr1 != end;++curr1)
		{
			curr2 = beg+mtrand.randInt(total-std::distance(beg, curr1)-1);
			std::swap(*curr1, *curr2);
		}
	}
	
	/** Calculates the starting position for each class partition based 
	 *  on the size of the paritions.
	 *
	 * @param start a reference to the output start value array.
	 * @param count a constant reference to size array.
	 * @param nfold_class number of classes.
	 * @param i start index.
	 */
	template<class U> 
	void find_start(std::vector<U>& start, const std::vector<U>& count, typename std::vector<U>::size_type nfold_class, typename std::vector<U>::size_type i=1)
	{
		start[0] = 0;
		for(;i<nfold_class;++i) start[i] = start[i-1]+count[i-1];
	}
	/** Counts the number of examples for each class and sets the class index for each example.
	 *
	 * @param beg a start iterator to a collection of examples.
	 * @param end a end iterator to a collection of examples.
	 * @param count a reference to a vector with the number of examples for each class.
	 * @param index a reference to a vector that tracks the class value for each example counted.
	 */
	template<class I, class U>
	void class_distribution(I beg, I end, std::vector<U>& count, std::vector<U>& index)
	{
		typedef U index_type;
		typedef std::vector<U> index_vector;
		typedef typename index_vector::size_type size_type;
		typedef typename index_vector::iterator iterator;
		index_type nfold_class=0;
		size_type pos;
		index_vector label(2);
		count.resize(2);
		for(I curr=beg;curr != end;++curr)
		{
			ASSERTMSG(nfold_class<=label.size(), nfold_class<< " <= " << label.size());
			pos = std::distance(label.begin(), std::find(label.begin(), label.begin()+nfold_class, U(curr->y())));
			ASSERT(pos <= label.size());
			ASSERT(size_type(std::distance(beg,curr)) < index.size());
			index[std::distance(beg,curr)]=U(pos);
			//ASSERTMSG(pos < 2, (int)std::distance(beg,curr) << " - " << (unsigned int)pos << "  " << curr->y() << "  " << (unsigned int)label.size() << "  " << nfold_class );
			if( pos == nfold_class ) 
			{
				if( nfold_class >= label.size() )
				{
					count.resize(nfold_class*2);
					label.resize(nfold_class*2);
				}
				ASSERTMSG(nfold_class < label.size(), nfold_class << " < " << label.size());
				label[nfold_class] = curr->y();
				ASSERTMSG(nfold_class < count.size(), nfold_class << " < " << count.size());
				count[nfold_class] = 1;
				++nfold_class;
			}
			else 
			{
				ASSERT(pos<count.size());
				++count[pos];
			}
		}
		count.resize(nfold_class);
		//ASSERT(nfold_class < 3);
	}
	/** Shuffles an index vector within a parition denoted by a start vector and a count vector.
	 *
	 * @param count a constant reference to a vector holding the size of each partition.
	 * @param index a reference to an the index vector.
	 * @param start a constant reference to a vector holding the start of each partition.
	 * @param mtrand a random number generator.
	 */
	template<class U>
	void shuffle(const std::vector<U>& count, std::vector<U>& index, const std::vector<U>& start, MTRand& mtrand)
	{
		for(typename std::vector<U>::size_type i=0;i<count.size();++i)
			shuffle(index.begin()+start[i], index.begin()+start[i]+count[i], mtrand);
	}
	/** Determines the start of each parition for a fold of cross-validation.
	 *
	 * @param fold_count a reference to a vector that holds the start of each fold.
	 * @param count a constant reference to a vector that holds the size of each fold parition by class.
	 * @param nfold_class the number of classes.
	 * @param nfold the number of folds.
	 */
	template<class U>
	void find_count(std::vector<U>& fold_count, const std::vector<U>& count, typename std::vector<U>::size_type nfold_class, typename std::vector<U>::size_type nfold)
	{
		for(typename std::vector<U>::size_type i=0, j;i<nfold;++i)
		{
			fold_count[i] = 0;
			for(j=0;j<nfold_class;++j) fold_count[i]+=U( (i+1)*count[j]/nfold-i*count[j]/nfold );
		}
	}
	/** Permute a vector organizing examples by partition.
	 *
	 * @param perm a reference to a permuation vector.
	 * @param fold_start a reference to a partition start vector.
	 * @param index constant reference to a index vector holding class information.
	 * @param count constant reference to a count vector holding the number of examples per class.
	 */
	template<class U>
	void permute(std::vector<U>& perm, std::vector<U>& fold_start, const std::vector<U>& index, const std::vector<U>& count)
	{
		typedef typename std::vector<U>::size_type size_type;
		size_type nfold = fold_start.size()-1;
		std::vector<U> start(count.size());
		find_start(start, count, count.size());
		for(size_type i=0,j,b,e,k;i<count.size();++i)
		{
			for(j=0;j<nfold;++j)
			{
				b = start[i] + j*count[i]/nfold;
				e = start[i] + (j+1)*count[i]/nfold;
				for(k=b;k<e;++k)
				{
					perm[fold_start[j]] = index[k];
					fold_start[j]++;
				}
			}
		}
	}
	/** Initializes a permutation vector.
	 *
	 * @param perm a reference to a permuation vector.
	 * @param start a reference to a partition by class start vector.
	 * @param index a constant reference to a index vector holding class information.
	 * @param total the number of examples.
	 */
	template<class U> 
	void align_start(std::vector<U>& perm, std::vector<U>& start, const std::vector<U>& index, typename std::vector<U>::size_type total)
	{
		for(typename std::vector<U>::size_type i=0;i<total;++i)
		{
			ASSERTMSG(i < index.size(), (unsigned int)i << " " << (unsigned int)index.size());
			ASSERTMSG(index[i] < start.size(), (unsigned int)i << " " << index[i] << " " << (unsigned int)start.size());
			ASSERTMSG(start[index[i]] < perm.size(), start[index[i]] << " " << (unsigned int)perm.size());
			perm[start[index[i]]] = U(i);
			++start[index[i]];
		}
	}
	/** Rearranges a set of examples based on a permutation vector.
	 *
	 * @param beg a start iterator to a collection of examples.
	 * @param end a end iterator to a collection of examples.
	 * @param perm a reference to a randomized permutation vector.
	 * @param total the total number of examples.
	 */
	template<class I, class U>
	void rearrange(I beg, I end, std::vector<U>& perm, typename std::vector<U>::size_type total)
	{
		typedef typename std::vector<U>::size_type size_type;
		std::vector<bool> check(total, false);

		for(size_type i=0, k=0, j;i<total;++i)
		{
			j=size_type(perm[k]);
			if( j == k || check[k] )
			{
				for(k=0;k<total;++k) if(!check[k]) break;
				j=size_type(perm[k]);
			}
			check[k] = true;
			std::swap( *(beg+k), *(beg+j) );
			k=j;
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

	/** Shuffles a collection of examples and sets a parition in fold_start.
	 *
	 * @param beg a start iterator to a collection of examples.
	 * @param end a end iterator to a collection of examples.
	 * @param fold_start a reference to a vector of index partitions.
	 * @param mtrand a random number generator.
	 */
	template<class I, class U>
	void standard_shuffle(I beg, I end, std::vector<U>& fold_start, MTRand& mtrand)
	{
		typedef U index_type;
		typedef std::vector<U> index_vector;
		typedef typename index_vector::size_type size_type;
		size_type nfold = fold_start.size()-1;
		size_type total = size_type(std::distance(beg,end));
		if( nfold != total ) shuffle(beg, end, mtrand);
		for(size_type i=0;i<=nfold;++i) fold_start[i] = index_type( i * total/nfold );
	}
	/** Shuffles a collection of examples and sets a parition in fold_start. It maintains
	 * the class distribution in each partition.
	 *
	 * @param beg a start iterator to a collection of examples.
	 * @param end a end iterator to a collection of examples.
	 * @param fold_start a reference to a vector of index partitions.
	 * @param mtrand a random number generator.
	 */
	template<class I, class U>
	void stratify_shuffle(I beg, I end, std::vector<U>& fold_start, MTRand& mtrand)
	{
		typedef U index_type;
		typedef std::vector<U> index_vector;
		typedef typename index_vector::size_type size_type;
		size_type nfold = fold_start.size()-1;
		size_type total = size_type(std::distance(beg,end));
		if( nfold == total )
		{
			for(size_type i=0;i<=nfold;++i) fold_start[i] = index_type( i * total/nfold );
			return;
		}
		index_vector count(2);
		index_vector index(total);
		index_vector perm(total);
		index_vector fold_count(nfold);
		index_vector start;
		class_distribution(beg, end, count, index);
		start.resize(count.size());
		find_start(start, count, count.size());
		align_start(perm, start, index, total);
		find_start(start, count, count.size());
		std::copy(perm.begin(), perm.end(), index.begin());
		shuffle(count, index, start, mtrand);
		find_count(fold_count, count, count.size(), nfold);
		find_start(fold_start, fold_count, nfold+1);
		permute(perm, fold_start, index, count);
		find_start(fold_start, fold_count, nfold+1);
		rearrange(beg, end, perm, total);
	}
};

#endif


