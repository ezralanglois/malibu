/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * StandardExampleFormat.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_STANDARDEXAMPLEFORMAT_H
#define _EXEGETE_STANDARDEXAMPLEFORMAT_H
#include "ConcreteExampleSet.h"
#include "AbstractFormatProxy.h"
#include "ArgumentMap.h"

/** @file StandardExampleFormat.h
 * @brief Read and write a dataset to a stream.
 * 
 * This file contains the StandardExampleFormat class, which defines 
 * the format of a set of examples.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section stdformarglist Standard Example Format
 * 	- header
 * 		- Description: parse header
 * 		- Type: Option(Auto:0;Yes:1;No:2)
 * 		- Viewability: Variable
 * 	- write
 * 		- Description: write format
 * 		- Type: Option(None:0;CSV:1)
 * 		- Viewability: Variable
 * 	- label
 * 		- Description: number of labels prefixing line
 * 		- Type: integer
 * 		- Viewability: Variable
 * 	- class
 * 		- Description: class position
 * 		- Type: integer
 * 		- Viewability: Variable
 * 	- writesparse
 * 		- Description: write examples out in sparse format
 * 		- Type: Boolean
 * 		- Viewability: Variable
 */

namespace exegete
{
	/** @brief Read and write a dataset to a stream.
	 * 
	 * This class defines the format for reading and writing a set of 
	 * examples in a specific format.
	 *
	 * @todo control number of labels to print
	 * @todo fix bug allows nominal and reall in same column!
	 */
	template<class X, class Y>
	class StandardExampleFormat : public AbstractFormatProxy< ConcreteExampleSet<X,Y>, typename ConcreteExampleSet<X,Y>::parent_type >
	{
		typedef const char* string_iterator;
	public:
		/** Defines a ConcreteExampleSet as a concrete type. **/
		typedef ConcreteExampleSet<X,Y>					concrete_type;
		/** Defines an ExampleSet as a dataset type. **/
		typedef typename concrete_type::parent_type		dataset_type;
		/** Defines an ExampleSet as a testset type. **/
		typedef typename concrete_type::parent_type		testset_type;
		/** Defines a concrete header as an attribute header. **/
		typedef typename concrete_type::attribute_header attribute_header;
		/** Defines a labeled example as an example type. **/
		typedef typename concrete_type::value_type		example_type;
		/** Defines an example class as a class type. **/
		typedef typename example_type::class_type		class_type;
		/** Defines an example value as a value type. **/
		typedef typename example_type::value_type		value_type;
		/** Defines an example index as an index type. **/
		typedef typename example_type::index_type		index_type;
		/** Defines an example type utility as a type utility. **/
		typedef typename example_type::type_util		type_util;
		/** Defines an example size as a size type. **/
		typedef typename example_type::size_type		size_type;
		/** Defines an example attribute as a size type. **/
		typedef typename example_type::attribute_type	attribute_type;
		/** Defines a constant example iterator. **/
		typedef typename concrete_type::const_iterator	const_iterator;
		/** Defines a constant example reference. **/
		typedef typename concrete_type::const_reference	const_reference;
		/** Enumerated argument types. **/
		enum{NONE,READ,WRITE,BOTH};
		/** Enumerated output styles **/
		enum{NOHEADER,HEADER};

	public:
		/** Constructs a standard example format.
		 */
		StandardExampleFormat() : attrTokenCh(','), sparTokenCh(':'), missingCh('?'), growthInt(50), labelInt(1), classPositionInt(0), writeSparseBool(0), styleInt(1), headerInt(1), parseHeaderInt(0)
		{
		}
		/** Constructs a standard example format.
		 *
		 * @param map an argument map.
		 * @param t a parameter type.
		 * @param l a argument view level.
		 */
		StandardExampleFormat(ArgumentMap& map, int t, int l=0) : attrTokenCh(','), sparTokenCh(':'), missingCh('?'), growthInt(50), labelInt(1), classPositionInt(0), writeSparseBool(0), styleInt(1), headerInt(1), parseHeaderInt(0)
		{
			init(map, t);
		}
		/** Destructs an standard example format.
		 */
		~StandardExampleFormat()
		{
		}
		
	public:
		/** Initalizes the map with arguments from the standard example format.
		 *
		 * @param map an argument map.
		 * @param t view read or both type arguments
		 * @param l a argument view level.
		 */
		template<class U>
		void init(U& map, int t=0, int l=0)
		{
			if( t != NONE && t != WRITE ) arginit(map, parseHeaderInt,		"header", 		"parse header>Auto:0;Yes:1;No:2", l);
			if( t != NONE && t != READ )  arginit(map, styleInt,			"write",  		"write format>None:0;CSV:1;Rule:2;Sparse:3", l);
			if( t != NONE && t != WRITE)  arginit(map, labelInt,			"label",  		"number of labels prefixing line", l);
			if( t != NONE && t != WRITE)  arginit(map, classPositionInt,	"class",  		"class position", l);
			if( t != NONE && t != READ)   arginit(map, writeSparseBool,		"writesparse", 	"write examples out in sparse format?", l);
		}

	public:
		/** Reads a concrete example set from the input stream.
		 *
		 * @param in an input stream.
		 * @param dataset a dataset.
		 * @return a error message otherwise NULL.
		 */
		const char* read(std::istream& in, concrete_type& dataset)
		{
			const char* msg;
			std::string line;
			bool nextline;
			size_type linenum=0;
			headerInt = (dataset.classCount() > 0) ? 2 : 1;
			if( (msg=read_header(in, dataset, line, linenum)) != 0 ) return ERRORADD(" on line " << linenum);
			nextline=line.empty();
			while( !in.eof() )
			{
				if(nextline) std::getline(in, line); else nextline=true;
				if( in.peek() == -1 ) in.setstate( std::ios::eofbit );
				++linenum;
				if( line.empty() ) continue;
				if( dataset.capacity() <= dataset.size() ) dataset.setsize(dataset.capacity()+growthInt);
				if( (msg=read(line.c_str(), dataset)) != 0 ) return ERRORADD(" on line " << linenum);//msg;
			}
			dataset.finalize(labelInt);
			if( dataset.classCount() == 0 )					return ERRORMSG("No classes");
			if( dataset.classCount() == dataset.size() )	return ERRORMSG("Class count == number of examples");
			if( dataset.attributeCount() == 0 )				return ERRORMSG("No attributes");
			return 0;
		}
		/** Writes a dataset to the output stream.
		 *
		 * @param out an output stream.
		 * @param dataset a dataset.
		 * @return a error message otherwise NULL.
		 */
		const char* write(std::ostream& out, const dataset_type& dataset)const
		{
			const char* msg;
			
			if( styleInt == 2 )
			{
				for(const_iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
				{
					if( (msg=write_rule(out, *beg, dataset)) != 0 ) return msg;
					out << "\n";
				}
				return 0;
			}

			if( styleInt == 3 )
			{
				for(const_iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
				{
					if( (msg=write_sparse(out, *beg, dataset)) != 0 ) return msg;
					out << "\n";
				}
				return 0;
			}
			
			if( styleInt != 0 || headerInt != 1 )
			{
				if( (msg=write_header(out, dataset)) != 0 ) return msg;
			}
			for(const_iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				if( styleInt != 0 || labelInt > 0 )
				{
					if( (msg=write_labels(out, *beg, dataset)) != 0 ) return msg;
				}
				if( writeSparseBool )
				{
					if( (msg=write_sparse(out, *beg, dataset)) != 0 ) return msg;
				}
				else
				{
					if( (msg=write_standard(out, *beg, dataset)) != 0 ) return msg;
				}
				out << "\n";
			}
			return 0;
		}

	protected:
		/** Writes labels to the output stream.
		 *
		 * @param out an output stream.
		 * @param ex an example.
		 * @param dataset a dataset.
		 * @return a error message otherwise NULL.
		 */
		const char* write_labels(std::ostream& out, const_reference ex, const dataset_type& dataset)const
		{
			for(size_type i=0;i<dataset.labelCount();++i) out << ex.labelAt(i) << attrTokenCh;
			return 0;
		}
		/** Writes a rule example to the output stream.
		 *
		 * @param out an output stream.
		 * @param ex an example.
		 * @param dataset a dataset.
		 * @return a error message otherwise NULL.
		 */
		const char* write_rule(std::ostream& out, const_reference ex, const dataset_type& dataset)const
		{
			value_type val;
			size_type n = dataset.attributeCount(), i;
			for(i=0;ex.isnotend(i,n);++i)
			{
				if( (val=ex.valueAt(i)) != 0 ) 
					out << ex.getIndexAt(i) << " ";
			}
			out << dataset.getClassAt(ex, missingCh);
			return 0;
		}
		/** Writes a sparse example to the output stream.
		 *
		 * @param out an output stream.
		 * @param ex an example.
		 * @param dataset a dataset.
		 * @return a error message otherwise NULL.
		 */
		const char* write_sparse(std::ostream& out, const_reference ex, const dataset_type& dataset)const
		{
			value_type val;
			size_type n = dataset.attributeCount(), i=0;
			bool flag=true;
			out << dataset.getClassAt(ex, missingCh);
			for(;ex.isnotend(i,n);++i)
			{
				if( (val=ex.valueAt(i)) != 0 ) 
				{
					out << attrTokenCh << ex.getIndexAt(i) << sparTokenCh << dataset.getAttributeAt(val, i, missingCh);
					if(flag)flag=false;
				}
			}
			if(flag)
			{
				out << attrTokenCh << '1' << sparTokenCh << '0';
			}
			return 0;
		}
		/** Writes an example to the output stream.
		 *
		 * @param out an output stream.
		 * @param ex an example.
		 * @param dataset a dataset.
		 * @return a error message otherwise NULL.
		 */
		const char* write_standard(std::ostream& out, const_reference ex, const dataset_type& dataset)const
		{
			value_type val;
			size_type n = dataset.attributeCount(), i, k=0;
			size_type cp = (styleInt==1)?0:classPositionInt;
			for(i=0;i<n;++i)
			{
				if( i == cp ) out << dataset.getClassAt(ex, missingCh) << attrTokenCh;
				val = ex.valueAt(i,k);
				out << dataset.getAttributeAt(val, i, missingCh);
				if( i != (n-1) ) out << attrTokenCh;
			}
			if( i <= cp ) out << attrTokenCh << dataset.getClassAt(ex, missingCh);
			return 0;
		}

		/** Reads a single example from the input stream.
		 *
		 * @param it an iterator position in a string.
		 * @param dataset the destination concrete dataset.
		 * @return a error message otherwise NULL.
		 */
		const char* read(string_iterator it, concrete_type& dataset)
		{
			size_type cp = classPositionInt<1?1:classPositionInt;
			std::string value, index;
			size_type i, j, k=0;
			bool foundClass=false;
			bool foundSparse=false;
			example_type ex;
			const char* msg;
			value_type val;

			dataset.setup(ex);
			for(i=0;*it != '\0' && i<labelInt;++i)
			{
				it=read_next(it, value);
				if( (msg=isValidToken(value)) != 0 ) return msg;
				dataset.insertLabel(ex, value, i);
			}
			for(i=0;*it != '\0';++i)
			{
				it = read_token(it, value, index);
				if( value.empty() ) break;
				if( (msg=isValidToken(value)) != 0 ) return msg;
				if( !index.empty() ) 
				{
					if( !stringToValue(index, j) )
						return ERRORMSG("Unable to parse sparse index, expected integer: " << index);
					foundSparse=true;
				}
				else j = k + 1;
				if( !foundClass && (j == cp || *it == '\0') )
				{
					if( headerInt == 3 )
					{
						if( !dataset.hasClass(value) ) 
						{
							std::string list;
							if( dataset.classCount() > 0 )
							{
								list=dataset.classAt(0);
								for(j=1;j<dataset.classCount();++j)
									list+=","+dataset.classAt(j);
							}
							return ERRORMSG("Found new class value for column " << k << ": " << value << " in " << list);
						}
					}
					if( concrete_type::is_regress )
					{
						class_type cl;
						if( !stringToValue(value, cl) ) return ERRORMSG("Regression dataset requires floating point class, found: " << value);
						ex.y(cl);
					}
					else dataset.insertClass(ex, value, headerInt!=2, missingCh);
					foundClass=true;
				}
				else 
				{
					if( !stringToValue(value, val) )
					{
						if( headerInt == 3 )
						{
//----------------------------------------------------------------------------------------------------------------
							if( value != "?" && k < dataset.attributeCount() && !dataset.hasNominal(value, k) ) 
							{
//----------------------------------------------------------------------------------------------------------------
								std::string list;
								if( dataset.attributeAt(k).size() > 0 )
								{
									list=dataset.attributeAt(k)[0];
									for(j=1;j<dataset.attributeAt(k).size();++j)
										list+=","+dataset.attributeAt(k)[j];
								}
								return ERRORMSG("Found new nominal value for column " << k << ": " << value << " expected: " << list);
							}
						}
//----------------------------------------------------------------------------------------------------------------
						if( !foundSparse || headerInt==0 || k < dataset.attributeCount() )
						val = dataset.insertNominal(value, k, headerInt!=2, missingCh);
						//----------------------------------------------------------------------------------------------------------------
					}
					dataset.resizeAttributes(ex, j, k, growthInt);
					dataset.insert_value_index(ex, val, j, k);//k++
//----------------------------------------------------------------------------------------------------------------
					if( foundSparse && headerInt==0 && k >= (dataset.attributeCount()-1) )
					{
						if( foundClass ) 
						{
							++k;
							break;
						}
						else continue;
					}
//----------------------------------------------------------------------------------------------------------------
					++k;
				}
			}
			if( !foundClass ) return ERRORMSG("Could not find class attribute");
			ASSERT(ex.x()!=0);
			dataset.append(ex, k);
			if( !foundSparse )
			{ 
				if( dataset.attributeLength() > 0 && dataset.attributeMax() != dataset.attributeLength() ) return ERRORMSG("Attribute length changed: " << dataset.attributeLength() << " != " << dataset.attributeMax());
				if( dataset.attributeCount() > 0 && dataset.attributeCount() != dataset.attributeLength() ) return ERRORMSG("Attribute length not match header: " << dataset.attributeLength() << " != " << dataset.attributeCount() << " - " << dataset.attributeMax() << " - " << dataset[0].indexAt(0) << " - " << dataset[0].indexAt(dataset.attributeLength()));
			}
			else
			{
				if( headerInt == 0 && dataset.attributeCount() > 0 && dataset.attributeLength() > dataset.attributeCount() ) return ERRORMSG("Attribute length exceeds header: " << dataset.attributeCount() << " < " << dataset.attributeLength() );
			}
			return 0;
		}
		/** Reads the next token from the line.
		 *
		 * @param it an iterator to position in the line.
		 * @param value a reference to the destination value token.
		 * @param index a reference to the destination index token.
		 * @return an iterator to next position on the line.
		 */
		string_iterator read_token(string_iterator it, std::string& value, std::string& index)const
		{
			it=read_next(it, value, sparTokenCh);
			if( *it != '\0' && *it == sparTokenCh )
			{
				index=value;
				it=eat_tokens(it);
				it=read_next(it, value, sparTokenCh);
			}
			else if( !index.empty() ) index="";
			return eat_tokens(it);
		}
		/** Tests if a character is a token.
		 *
		 * @param ch character to test.
		 * @param sp the sparse character.
		 * @return true if ch is a space, an attribute token, and a space character.
		 */
		bool isToken(char ch, char sp)const
		{
			return IS_SPACE(ch) || ch == attrTokenCh || ch == sp;
		}
		/** Reads the next token from the line.
		 *
		 * @param it an iterator to position in the line.
		 * @param tok a reference to the destination token.
		 * @return an iterator to next position on the line.
		 */
		string_iterator read_next(string_iterator it, std::string& tok)const
		{
			return eat_tokens(read_next(it, tok, attrTokenCh));
		}
		/** Reads the next token from the line.
		 *
		 * @param it an iterator to position in the line.
		 * @param tok a reference to the destination token.
		 * @param sparseCh the sparse token characeter.
		 * @return an iterator to next position on the line.
		 */
		string_iterator read_next(string_iterator it, std::string& tok, char sparseCh)const
		{
			tok="";
			for(;*it != '\0' && !isToken(*it, sparseCh) ;++it)
			{
				if( *it == '\"' ) it=read_quote(it, tok, '\"');
				else tok+=*it;
			}
			return it;
		}
		/** Reads a quoted token from the string.
		 *
		 * @param it an iterator to start of line.
		 * @param tok a reference to the destination token.
		 * @param quote the quote token.
		 * @return an iterator to next position on the line.
		 */
		string_iterator read_quote(string_iterator it, std::string& tok, char quote)const
		{
			do{
				tok+=*it; ++it;
			}while( *it != '\0' && *it != quote);
			if(*it == quote) tok+=*it;
			return it;
		}
		/** Eats white space tokens as well as other tokens.
		 *
		 * @param it an iterator to start of line.
		 * @return an iterator to next position on the line.
		 */
		string_iterator eat_tokens(string_iterator it)const
		{
			while( *it != '\0' && IS_SPACE(*it) ) ++it;
			if( *it == '\0' ) return it;
			if( *it == attrTokenCh ) ++it;
			if( *it == '\0' ) return it;
			if( *it == sparTokenCh ) ++it;
			while( *it != '\0' && IS_SPACE(*it) ) ++it;
			return it;
		}
		/** Tests if a token is valid.
		 *
		 * @param tok the token to check.
		 * @return an error message other NULL.
		 */
		const char* isValidToken(const std::string& tok)const
		{
			if( !tok.empty() )
			{
				if( (tok[0] == '\"' && tok[tok.length()-1] != '\"') || 
					(tok[0] != '\"' && tok[tok.length()-1] == '\"') ||
						(tok[0] == '\"' && tok.length() == 1) )
					return ERRORMSG("Mismatched quotes: -" << tok << "-");
			}
			return 0;
		}

	protected:
		/** Reads a header from the input stream.
		 *
		 * @param out a reference to the output stream.
		 * @param header a reference to an attribute header.
		 * @return an error message or NULL.
		 */
		const char* write_header(std::ostream& out, const dataset_type& header)const
		{
			if(styleInt == 0) return 0;
			if(styleInt == 1 ) return write_csv(out, header);
			return 0;
		}
		/** Reads an attribute header from the input stream.
		 *
		 * @param in an input stream.
		 * @param header a reference to an attribute header.
		 * @param line the first line
		 * @param linenum the current line number.
		 * @return an error message or NULL.
		 */
		const char* read_header(std::istream& in, concrete_type& header, std::string& line, size_type& linenum)
		{
			std::getline(in, line);
			if( parseHeaderInt < 2 )
			{
				if( parseHeaderInt == 1 || testcsv(line.c_str()) ) return read_csv(in, header, line, linenum);
			}
			if( headerInt != 2 )
			{
				size_type len = count(line.c_str());
				//parseHeaderInt == 1
				header.setupHeader(len, labelInt);
			}
			return 0;
		}
		/** Writes a header in CSV format.
		 *
		 * @param out an output stream.
		 * @param header a reference to an attribute header.
		 * @return an error message or NULL.
		 */
		const char* write_csv(std::ostream& out, const dataset_type& header)const
		{
			size_type cp = (styleInt==1)?0:classPositionInt, i=0;
			std::string nm;
			for(;i<header.labelCount();++i)
			{
				nm = header.headerAt(i).name();
				if( nm == "" ) out << "Label_" << (i+1) << attrTokenCh;
				else out << nm << attrTokenCh;
			}
			if(cp == 0)
			{
				nm = header.classHeader().name();
				if( nm == "" ) out << "Class" << attrTokenCh;
				else out << header.classHeader().name() << attrTokenCh;
			}
			for(i=0;i<header.attributeCount();++i)
			{
				nm = header.attributeAt(i).name();
				if( nm == "" ) out << "Attribute_" << (i+1);
				else out << nm;
				if( (i+1) != header.attributeCount() ) out << attrTokenCh;
			}
			if( i <= cp ) 
			{
				nm = header.classHeader().name();
				if( nm == "" ) out << attrTokenCh << "Class";
				else out << attrTokenCh << nm;
			}
			out << "\n";
			return 0;
		}
		/** Reads a header from the input stream.
		 *
		 * @param in an input stream.
		 * @param header a reference to an attribute header.
		 * @param line the first line
		 * @param linenum the current line number.
		 * @return an error message or NULL.
		 */
		const char* read_csv(std::istream& in, concrete_type& header, std::string& line, size_type& linenum)
		{
			bool test = false;
			size_type i,j=0,cp=labelInt+classPositionInt;
			size_type len = count(line.c_str());
			if( len < 2 ) return ERRORMSG("Unable to parse header");
			if( header.attributeCount() > 0 )
			{
				test = true;
//----------------------------------------------------------------------------------------------------------------
				if( (len-labelInt-1) > header.attributeCount() ) 
					return ERRORMSG("Dataset header and new file have different number of attributes - expected: " << header.attributeCount() << " and found: " << (len-labelInt-1) << " " << len << " " << labelInt << " " << line );
//----------------------------------------------------------------------------------------------------------------
			}
			else header.setupHeader(len, labelInt);
			string_iterator it=line.c_str();
			bool foundClass=false;
			const char* msg;

			std::string val, idx;
			for( i=0;*it != '\0';++i)
			{
				it = read_token(it, val, idx);
				if( (msg=isValidToken(val)) != 0 ) return msg;
				if( i != cp || (!foundClass && *it =='\0') )
				{
					if( test && i >= labelInt )
					{
//----------------------------------------------------------------------------------------------------------------
						if( (j-labelInt) >= header.attributeCount() ) return ERRORMSG("Unexpected");
//----------------------------------------------------------------------------------------------------------------
						if( header.attributeAt(j-labelInt).name() != val )
						{
							return ERRORMSG("Attribute header at column (not counting class) " << i << " does not match current dataset - expected: " << header.attributeAt(j-labelInt).name() << " and found: " << val);
						}
					}
					else header.insertHeader(val, j);
					j++;
				}
				else
				{
					header.insertHeaderClass(val);
					foundClass=true;
				}
			}
			linenum=1;
			line="";
			if( headerInt != 2 ) headerInt=0;
			return 0;
		}
		/** Count the number of elements on a line.
		 *
		 * @param it an iterator to start of the line.
		 * @return number of elements.
		 */
		size_type count(string_iterator it)
		{
			std::string val, idx;
			size_type tot=0, i;
			for( i=0;*it != '\0';++i, ++tot) it = read_token(it, val, idx);
			return tot;
		}
		/** Tests if every element on the first line is a non-attribute value.
		 *
		 * @param it an iterator to start of the line.
		 * @return true if every element in non-attribute value.
		 */
		bool testcsv(string_iterator it)
		{
			std::string val, idx;
			value_type attr;
			while(*it != '\0')
			{
				it = read_token(it, val, idx);
				if( stringToValue(val, attr) ) return false;
			}
			return true;
		}
		
	public:
		/** Writes an attribute header before a model.
		 *
		 * @param out an output stream.
		 * @param header a reference to an attribute header.
		 * @return an error message or NULL.
		 */
		const char* write_model(std::ostream& out, const attribute_header& header)const
		{
			std::string tmp;
			size_type i=0;
			if( labelInt < header.labelCount() ) i+= (header.labelCount()-labelInt);
			for(;i<header.headerCount();++i)
			{
				out << ((i<labelInt)?"@LABEL":"@ATTRIBUTE") << " ";
				out << header.headerAt(i).name();
				for(size_type j=0;j<header.headerAt(i).size();++j)
					out << "," << header.headerAt(i)[j];
				out << "\n";
			}
			out << "@data\n";
			return 0;
		}
		/** Tests if an attribute header from the input stream matches the given attribute header.
		 *
		 * @param in an input stream.
		 * @param header a constant reference to an attribute header.
		 * @return an error message or NULL.
		 */
		const char* read_model(std::istream& in, const typename concrete_type::parent_type& header)
		{
			std::string line;
			std::vector<std::string> headVec, nomVec;
			size_type lblcnt=0;
			bool foundClass=false, test=header.attributeCount()>0;
			while( !in.eof() )
			{
				std::getline(in, line);
				if( line == "@data" ) break;
				if( strcmpn(line.c_str(), "@LABEL", 6)) lblcnt++;
				line = line.substr(line.find(' ')+1);
				headVec.push_back(line);
			}
			
			if( labelInt != lblcnt ) 
				return ERRORMSG("Number of labels in dataset not match model, require " << lblcnt);
			
			//header.setupHeader(headVec.size(), labelInt);
			std::string val;
			for(size_type i=0,j=0;i<headVec.size();++i)
			{
				stringToValue(headVec[i], nomVec, ",");
				if( nomVec.empty() ) return ERRORMSG("Empty line: " << i << " " << headVec[i]);
				val = nomVec.front();
				if( i != labelInt || (!foundClass && (i+1) == headVec.size() ) )
				{
					if( test && j >= labelInt )
					{//j-labelInt
						if( header.attributeAt(j-labelInt).name() != val )
							return ERRORMSG("Attribute header at column (not counting class) " << i << " does not match current dataset - expected: " << header.attributeAt(j-labelInt).name() << " and found: " << val);
					}
					else 
					{
						if( val == "Class") return ERRORMSG("Class cannot be attribute: " << val << " " << i << " " << j);
						//header.insertHeader(val, j);
						//if( j >= labelInt ) header.attributeAt(j-labelInt).assign(nomVec.begin()+1, nomVec.end());
					}
					j++;
				}
				else
				{
					//header.insertHeaderClass(val);
					foundClass=true;
					//header.headerAt(labelInt).assign(nomVec.begin()+1, nomVec.end());
				}
				nomVec.clear();
			}
			headVec.clear();
			return 0;
		}
		/** Reads an attribute header from the input stream.
		 *
		 * @param in an input stream.
		 * @param header a reference to an attribute header.
		 * @return an error message or NULL.
		 */
		const char* read_model(std::istream& in, concrete_type& header)
		{
			std::string line;
			std::vector<std::string> headVec, nomVec;
			size_type lblcnt=0;
			bool foundClass=false, test=header.attributeCount()>0;
			while( !in.eof() )
			{
				std::getline(in, line);
				if( line == "@data" ) break;
				if( strcmpn(line.c_str(), "@LABEL", 6)) lblcnt++;
				line = line.substr(line.find(' ')+1);
				headVec.push_back(line);
			}
			
			if( labelInt != lblcnt ) 
				return ERRORMSG("Number of labels in dataset not match model, require " << lblcnt);
			
			header.setupHeader(headVec.size(), labelInt);
			std::string val;
			for(size_type i=0,j=0;i<headVec.size();++i)
			{
				stringToValue(headVec[i], nomVec, ",");
				if( nomVec.empty() ) return ERRORMSG("Empty line: " << i << " " << headVec[i]);
				val = nomVec.front();
				if( i != labelInt || (!foundClass && (i+1) == headVec.size() ) )
				{
					if( test && j >= labelInt )
					{
						if( header.attributeAt(j-labelInt).name() != val )
							return ERRORMSG("Attribute header at column (not counting class) " << i << " does not match current dataset - expected: " << header.attributeAt(i-labelInt).name() << " and found: " << val);
					}
					else 
					{
						if( val == "Class") return ERRORMSG("Class cannot be attribute: " << val << " " << i << " " << j << " " << labelInt);
						header.insertHeader(val, j);
						if( j >= labelInt ) header.attributeAt(j-labelInt).assign(nomVec.begin()+1, nomVec.end());
					}
					j++;
				}
				else
				{
					header.insertHeaderClass(val);
					foundClass=true;
					header.headerAt(labelInt).assign(nomVec.begin()+1, nomVec.end());
				}
				nomVec.clear();
			}
			headVec.clear();
			return 0;
		}

	public:
		/** Set the output style.
		 *
		 * @param s an output style.
		 */
		void style(int s)
		{
			styleInt = s;
		}
		/** Set an attribute separation character.
		 *
		 * @param ch an attribute separation character.
		 */
		void attrToken(char ch)
		{
			attrTokenCh = ch;
		}

	private:
		char attrTokenCh;
		char sparTokenCh;
		char missingCh;
	private:
		unsigned int growthInt;
		unsigned int labelInt;
		unsigned int classPositionInt;
	private:
		int writeSparseBool;
		int styleInt;
		int headerInt;
		int parseHeaderInt;
	};

};


#endif

