/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * aspen_attribute_allocator.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_STANDARD_ATTRIBUTE_ALLOCATOR_HPP
#define _EXEGETE_STANDARD_ATTRIBUTE_ALLOCATOR_HPP
#include "../attribute_utility.hpp"

/** @file aspen_attribute_allocator.hpp
 * @brief Aspen attribute allocator
 * 
 * This file contains the an allocator for aspen sparse attributes.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

	/** @brief Aspen sparse attribute allocator
	 * 
	 * This class defines the attribute allocator for aspen.
	 */
	template<class A>
	class aspen_attribute_allocator_base
	{
	public:
		/** Defines an attribute type utility. **/
		typedef attribute_utility<A> type_util;
		/** Defines a size type. **/
		typedef typename type_util::size_type size_type;
		/** Defines a value type. **/
		typedef typename type_util::value_type value_type;
		/** Defines a pointer. **/
		typedef typename type_util::pointer pointer;
		/** Defines a constanct pointer. **/
		typedef typename type_util::const_pointer const_pointer;
		
	public:
		/** Defines an attribute pointer. **/
		typedef pointer* attribute_pointer;
		/** Defines a constant attribute pointer. **/
		typedef const const_pointer* const_attribute_pointer;
		/** Defines an attribute pointer. **/
		typedef pointer feature_pointer;
		/** Defines a constant attribute pointer. **/
		typedef const_pointer const_feature_pointer;
		/** Defines a size pointer. **/
		typedef size_type* size_pointer;
		
	public:
		/** Constructs an attribute allocator.
		 */
		aspen_attribute_allocator_base() : a_ptr(0), s_ptr(0), a_cnt(0), a_tot(0)
		{
		}
		
	public:
		/** Get number of attributes.
		 * 
		 * @return number of attributes.
		 */
		size_type attribute_total()const
		{
			return a_tot;
		}
		
	public:
		/** Get start of attribute vector.
		 * 
		 * @return start of attribute vector.
		 */
		feature_pointer feature_begin()
		{
			if( a_ptr == 0 ) return 0;
			return *a_ptr;
		}
		/** Get end of attribute vector.
		 * 
		 * @return end of attribute vector.
		 */
		feature_pointer feature_end()
		{
			return feature_begin()+a_tot;
		}
		/** Get start of attribute vector.
		 * 
		 * @return start of attribute vector.
		 */
		const_feature_pointer feature_begin()const
		{
			if( a_ptr == 0 ) return 0;
			return *a_ptr;
		}
		/** Get end of attribute vector.
		 * 
		 * @return end of attribute vector.
		 */
		const_feature_pointer feature_end()const
		{
			return feature_begin()+a_tot;
		}
		
	protected:
		/** Holds an attribute pointer. **/
		attribute_pointer a_ptr;
		/** Holds a size pointer. **/
		size_pointer s_ptr;
		/** Holds number of attribute columns. **/
		size_type a_cnt;
		/** Holds total number of attribute. **/
		size_type a_tot;
	};
	/** @brief Aspen sparse attribute allocator
	 * 
	 * This class defines the attribute allocator for aspen.
	 * 
	 * @todo sort?
	 */
	template<class A>
	class aspen_attribute_allocator : public aspen_attribute_allocator_base<A>
	{
	public:
		/** Defines a base type. **/
		typedef aspen_attribute_allocator_base<A> base_type;
		/** Defines an attribute type utility. **/
		typedef typename base_type::type_util type_util;
		/** Defines a size type. **/
		typedef typename type_util::size_type size_type;
		/** Defines a value type. **/
		typedef typename type_util::value_type value_type;
		/** Defines a pointer. **/
		typedef typename type_util::pointer pointer;
		/** Defines a constanct pointer. **/
		typedef typename type_util::const_pointer const_pointer;
		/** Defines a feature type. **/
		typedef typename type_util::feature_type feature_type;
		/** Defines a index type. **/
		typedef typename type_util::index_type index_type;
		
	public:
		/** Defines an attribute pointer. **/
		typedef typename base_type::attribute_pointer attribute_pointer;
		/** Defines a constant attribute pointer. **/
		typedef typename base_type::const_attribute_pointer const_attribute_pointer;
		/** Defines a size pointer. **/
		typedef typename base_type::size_pointer size_pointer;
		
	public:
		/** Constructs an attribute allocator.
		 */
		aspen_attribute_allocator()
		{
		}
		/** Destructs an attribute allocator.
		 */
		~aspen_attribute_allocator()
		{
			if( base_type::a_ptr != 0 ) ::erase(*base_type::a_ptr);
			::erase(base_type::a_ptr);
			::erase(base_type::s_ptr);
		}
		
	public:
		/** Constructs an attribute allocator.
		 * 
		 * @param alloc allocator to copy.
		 */
		aspen_attribute_allocator(const aspen_attribute_allocator& alloc)
		{
			if( base_type::a_cnt > 0 )
			{
				resize(alloc.a_tot, alloc.a_cnt);
				std::copy(alloc.s_ptr, alloc.s_ptr+base_type::a_cnt, base_type::s_ptr);
				std::copy((*alloc.a_ptr), (*alloc.a_ptr)+base_type::a_tot, *base_type::a_ptr);
				for(size_type i=1;i<base_type::a_cnt+1;++i) base_type::a_ptr[i] = base_type::a_ptr[i-1]+base_type::s_ptr[i-1];
			}
		}
		/** Constructs an attribute allocator.
		 * 
		 * @param alloc allocator to copy.
		 */
		aspen_attribute_allocator& operator=(const aspen_attribute_allocator& alloc)
		{
			clear();
			resize(alloc.a_tot, alloc.a_cnt);
			std::copy(alloc.s_ptr, alloc.s_ptr+base_type::a_cnt, base_type::s_ptr);
			std::copy((*alloc.a_ptr), (*alloc.a_ptr)+base_type::a_tot, *base_type::a_ptr);
			for(size_type i=1;i<base_type::a_cnt+1;++i) base_type::a_ptr[i] = base_type::a_ptr[i-1]+base_type::s_ptr[i-1];
			return *this;
		}
		
	public:
		/** Add attribute value, index along with example.
		 * 
		 * @param pb a start attribute pointer.
		 * @param px an attribute pointer.
		 * @param val a feature value.
		 * @param n a attribute index.
		 * @param e an example index.
		 * @return attribute pointer.
		 */
		pointer append_attribute(pointer pb, pointer px, feature_type val, index_type n, size_type e)
		{
			type_util::indexOf(base_type::a_ptr[n][base_type::s_ptr[n]]) = e;
			type_util::valueOf(base_type::a_ptr[n][base_type::s_ptr[n]]) = val;
			base_type::s_ptr[n]++;
			return px;
		}
		/** Mark the last sparse attribute.
		 * 
		 * @param px an attribute pointer.
		 * @return an attribute pointer.
		 */
		inline pointer mark_last_attribute(pointer px)
		{
			//type_util::indexOf(*px) = -1;
			return px;
		}
		/** Number of attributes to mark.
		 * 
		 * @return 0
		 */
		static size_type mark_count()
		{
			return 0;
		}
		/** Increment the number of examples per attribute.
		 * 
		 * @param a attribute index.
		 */
		void increment_attribute(size_type a)
		{
			if( a >= base_type::a_tot )
			{
				size_type prev=base_type::a_tot;
				base_type::a_tot = (a+1)*2;
				base_type::s_ptr = ::resize(base_type::s_ptr, base_type::a_tot);
				std::fill(base_type::s_ptr+prev, base_type::s_ptr+base_type::a_tot, 0);
				base_type::a_cnt = a;
			}
			else if ( a > base_type::a_cnt ) base_type::a_cnt = a;
			base_type::s_ptr[a]++;
		}

	public:
		/** Resize the attribute pointer.
		 * 
		 * @param n total number of attributes.
		 * @param a number of attribute columns.
		 * @param e number of examples.
		 */
		void resize(size_type n, size_type a, size_type e)
		{
			base_type::s_ptr = ::resize(base_type::s_ptr, a);
			std::fill(base_type::s_ptr, base_type::s_ptr+a, 0);
			pointer tmp = 0;
			if( base_type::a_ptr != 0 ) tmp = *base_type::a_ptr;
			base_type::a_ptr = ::resize(base_type::a_ptr, a+1);
			*base_type::a_ptr = tmp;
			*base_type::a_ptr = ::resize(*base_type::a_ptr, n);
			base_type::a_tot = n;
			base_type::a_cnt = a;
			for(size_type i=1;i<base_type::a_cnt+1;++i) base_type::a_ptr[i] = base_type::a_ptr[i-1]+base_type::s_ptr[i-1];
		}
		/** Clear the attribute pointer.
		 */
		void clear()
		{
			if( base_type::a_ptr != 0 ) ::erase(*base_type::a_ptr);
			::erase(base_type::a_ptr);
			::erase(base_type::s_ptr);
			base_type::a_cnt = 0;
			base_type::a_tot = 0;
		}
		
	protected:
		/** Estimate attribute statistics.
		 * 
		 * @param beg start example iterator.
		 * @param end end example iterator.
		 * @param hbeg start header iterator.
		 * @param hend end header iterator.
		 */
		template<class E, class H>
		void estimate_statistics(E ebeg, E eend, H hbeg, H hend)
		{
			for(const_attribute_pointer a = base_type::a_ptr;hbeg != hend;++hbeg, ++a)
			{
				estimate(*a, *(a+1), *hbeg);
			}
		}
		
	private:
		/** Estimate statistics for attribute column.
		 * 
		 * @param a an attribute pointer.
		 * @param hbeg start header iterator.
		 */
		template<class H>
		static void estimate(const_pointer beg, const_pointer end, H& h)
		{
			for(;beg != end;++beg) h.count( type_util::valueOf(*beg) );
		}
	};

};

#endif


