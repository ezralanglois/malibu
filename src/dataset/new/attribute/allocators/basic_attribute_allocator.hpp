/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * basic_attribute_allocator.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_BASIC_ATTRIBUTE_ALLOCATOR_HPP
#define _EXEGETE_BASIC_ATTRIBUTE_ALLOCATOR_HPP
#include "../attribute_utility.hpp"

/** @file basic_attribute_allocator.hpp
 * @brief Basic attribute allocator
 * 
 * This file contains the an allocator for basic attributes.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Basic attribute allocator
	 * 
	 * This class defines an attribute allocator for basic attributes.
	 */
	template<class A>
	class basic_attribute_allocator_base
	{//attribute_utility
	public:
		/** Defines an attribute type utility. **/
		typedef attribute_utility<A> type_util;
		/** Defines a size type. **/
		typedef typename type_util::size_type size_type;
		/** Defines a value type. **/
		typedef typename type_util::value_type value_type;
		/** Defines a pointer. **/
		typedef typename type_util::pointer pointer;
		/** Defines a constanct pointer. **/
		typedef typename type_util::const_pointer const_pointer;
	public:
		/** Defines a feature pointer. **/
		typedef pointer feature_pointer;
		/** Defines a constant feature pointer. **/
		typedef const_pointer const_feature_pointer;
		
	public:
		/** Constructs an attribute allocator.
		 */
		basic_attribute_allocator_base() : attr_ptr(0), attr_tot(0)
		{
		}
		
	public:
		/** Get number of attributes.
		 * 
		 * @return number of attributes.
		 */
		size_type attribute_total()const
		{
			return attr_tot;
		}
		
	public:
		/** Get start of attribute vector.
		 * 
		 * @return start of attribute vector.
		 */
		feature_pointer feature_begin()
		{
			return attr_ptr;
		}
		/** Get end of attribute vector.
		 * 
		 * @return end of attribute vector.
		 */
		feature_pointer feature_end()
		{
			return attr_ptr+attr_tot;
		}
		/** Get start of attribute vector.
		 * 
		 * @return start of attribute vector.
		 */
		const_feature_pointer feature_begin()const
		{
			return attr_ptr;
		}
		/** Get end of attribute vector.
		 * 
		 * @return end of attribute vector.
		 */
		const_feature_pointer feature_end()const
		{
			return attr_ptr+attr_tot;
		}
		
	protected:
		/** Holds a pointer to an attribute collection. **/
		pointer attr_ptr;
		/** Holds number of attributes. **/
		size_type attr_tot;
	};
	

	/** @brief Basic attribute allocator
	 * 
	 * This class defines an attribute allocator for basic attributes.
	 */
	template<class A>
	class basic_attribute_allocator : public basic_attribute_allocator_base<A>
	{
	public:
		/** Defines a base type. **/
		typedef basic_attribute_allocator_base<A> base_type;
		/** Defines an attribute type utility. **/
		typedef typename base_type::type_util type_util;
		/** Defines a size type. **/
		typedef typename type_util::size_type size_type;
		/** Defines a value type. **/
		typedef typename type_util::value_type value_type;
		/** Defines a pointer. **/
		typedef typename type_util::pointer pointer;
		/** Defines a constanct pointer. **/
		typedef typename type_util::const_pointer const_pointer;
		
	public:
		/** Constructs an attribute allocator.
		 */
		basic_attribute_allocator()
		{
		}
		/** Destructs an attribute allocator.
		 */
		~basic_attribute_allocator()
		{
			::erase(base_type::attr_ptr);
		}
		
	public:
		/** Constructs an attribute allocator.
		 * 
		 * @param alloc allocator to copy.
		 */
		basic_attribute_allocator(const basic_attribute_allocator& alloc)
		{
			base_type::attr_ptr = 0;
			resize(alloc.attr_tot, 0, 0);
			std::copy(alloc.attr_ptr, alloc.attr_ptr+base_type::attr_tot, base_type::attr_ptr);
		}
		/** Constructs an attribute allocator.
		 * 
		 * @param alloc allocator to copy.
		 */
		basic_attribute_allocator& operator=(const basic_attribute_allocator& alloc)
		{
			clear();
			resize(alloc.attr_tot, 0, 0);
			std::copy(alloc.attr_ptr, alloc.attr_ptr+base_type::attr_tot, base_type::attr_ptr);
			return *this;
		}
		
	public:
		/** Resize the attribute pointer.
		 * 
		 * @param n total number of attributes.
		 * @param a number of attribute columns.
		 * @param e number of examples.
		 */
		void resize(size_type n, size_type a, size_type e)
		{
			base_type::attr_ptr = ::resize(base_type::attr_ptr, n);
			base_type::attr_tot = n;
		}
		/** Clear the attribute pointer.
		 */
		void clear()
		{
			::erase(base_type::attr_ptr);
			base_type::attr_tot = 0;
		}
	};
};

#endif

