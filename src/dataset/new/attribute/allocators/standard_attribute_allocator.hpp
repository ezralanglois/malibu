/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * standard_attribute_allocator.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_STANDARD_ATTRIBUTE_ALLOCATOR_HPP
#define _EXEGETE_STANDARD_ATTRIBUTE_ALLOCATOR_HPP
#include "basic_attribute_allocator.hpp"

/** @file standard_attribute_allocator.hpp
 * @brief Standard attribute allocator
 * 
 * This file contains the an allocator for standard attributes.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Standard attribute allocator
	 * 
	 * This class defines an attribute allocator for standard attributes.
	 */
	template<class A, int S=attribute_utility<A>::IS_SPARSE>
	class standard_attribute_allocator_impl : public basic_attribute_allocator<A>
	{
	public:
		/** Defines a base type for the allocator. **/
		typedef basic_attribute_allocator<A> base_type;
	public:
		/** Defines an attribute type utility. **/
		typedef typename base_type::type_util type_util;
		/** Defines a size type. **/
		typedef typename type_util::size_type size_type;
		/** Defines a value type. **/
		typedef typename type_util::value_type value_type;
		/** Defines a index type. **/
		typedef typename type_util::index_type index_type;
		/** Defines a feature type. **/
		typedef typename type_util::feature_type feature_type;
		/** Defines a pointer. **/
		typedef typename type_util::pointer pointer;
		
	public:
		/** Resize the attribute pointer.
		 * 
		 * @param n total number of attributes.
		 * @param a number of attribute columns.
		 * @param e number of examples.
		 */
		void resize(size_type n, size_type a, size_type e)
		{
			n = a*e;
			base_type::attr_ptr = ::calloc(base_type::attr_ptr, n);
			base_type::attr_tot = n;
		}
		/** Add attribute value, index along with example.
		 * 
		 * @param pb a start attribute pointer.
		 * @param px an attribute pointer.
		 * @param val a feature value.
		 * @param idx a attribute index.
		 * @param e an example index.
		 * @return attribute pointer.
		 */
		inline pointer append_attribute(pointer pb, pointer px, feature_type val, index_type idx, size_type e)
		{
			ASSERTMSG((pb+idx) < base_type::feature_end(), std::distance(base_type::feature_begin(), pb+idx) << " < " << base_type::attribute_total());
			pb[idx] = val;
			px = pb + idx;
			return px;
		}
		/** Mark the last sparse attribute.
		 * 
		 * @note Does nothing.
		 * 
		 * @param px an attribute pointer.
		 * @return an attribute pointer.
		 */
		inline pointer mark_last_attribute(pointer px)
		{
			return px;
		}
		/** Number of attributes to mark.
		 * 
		 * @return 0
		 */
		static size_type mark_count()
		{
			return 0;
		}
		/** Increment the number of examples per attribute.
		 * 
		 * @param a attribute index.
		 */
		void increment_attribute(size_type a)
		{
		}
	};
	/** @brief Sparse attribute allocator
	 * 
	 * This class defines an attribute allocator for standard attributes.
	 */
	template<class A>
	class standard_attribute_allocator_impl<A, 1> : public basic_attribute_allocator<A>
	{
	public:
		/** Defines a base type for the allocator. **/
		typedef basic_attribute_allocator<A> base_type;
	public:
		/** Defines an attribute type utility. **/
		typedef typename base_type::type_util type_util;
		/** Defines a size type. **/
		typedef typename type_util::size_type size_type;
		/** Defines a value type. **/
		typedef typename type_util::value_type value_type;
		/** Defines a index type. **/
		typedef typename type_util::index_type index_type;
		/** Defines a feature type. **/
		typedef typename type_util::feature_type feature_type;
		/** Defines a pointer. **/
		typedef typename type_util::pointer pointer;
		
	public:
		/** Add attribute value, index along with example.
		 * 
		 * @param pb a start attribute pointer.
		 * @param px an attribute pointer.
		 * @param val a feature value.
		 * @param idx a attribute index.
		 * @param e an example index.
		 * @return attribute pointer.
		 */
		inline pointer append_attribute(pointer pb, pointer px, feature_type val, index_type idx, size_type e)
		{
			type_util::valueOf(*px) = val;
			type_util::indexOf(*px) = idx+1;
			return px;
		}
		/** Mark the last sparse attribute.
		 * 
		 * @param px an attribute pointer.
		 * @return an attribute pointer.
		 */
		inline pointer mark_last_attribute(pointer px)
		{
			type_util::indexOf(*px) = -1;
			return px+1;
		}
		/** Number of attributes to mark.
		 * 
		 * @return 1
		 */
		static size_type mark_count()
		{
			return 1;
		}
		/** Increment the number of examples per attribute.
		 * 
		 * @param a attribute index.
		 */
		void increment_attribute(size_type a)
		{
		}
	};

	/** @brief Standard attribute allocator
	 * 
	 * This class defines an attribute allocator for standard attributes.
	 */
	template<class A>
	class standard_attribute_allocator : public standard_attribute_allocator_impl<A> 
	{
		typedef standard_attribute_allocator_impl<A>  parent_type;
	public:
		/** Defines an attribute type utility. **/
		typedef typename parent_type::type_util type_util;
		/** Defines a constant pointer. **/
		typedef typename type_util::const_pointer const_pointer;
	protected:
		/** Estimate attribute statistics.
		 * 
		 * @param beg start example iterator.
		 * @param end end example iterator.
		 * @param hbeg start header iterator.
		 * @param hend end header iterator.
		 */
		template<class E, class H>
		static void estimate_statistics(E beg, E end, H hbeg, H hend)
		{
			for(;beg != end;++beg) estimate(beg->x(), hbeg, hend);
		}
		/** Estimate attributes statistics for example.
		 * 
		 * @param a an attribute pointer.
		 * @param hbeg start header iterator.
		 * @param hend end header iterator.
		 */
		template<class H>
		static void estimate(const_pointer a, H hbeg, H hend)
		{
			typedef attribute_iterator_utility<A> iter_util;
			for(const_pointer e = a+std::distance(hbeg, hend), b = a;iter_util::not_end(a, e);++a)
			{
				hbeg[ iter_util::distance(b, a) ].count( type_util::valueOf(*a) );
			}
		}
	};
};

#endif

