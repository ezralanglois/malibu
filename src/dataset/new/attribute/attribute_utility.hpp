/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_utility.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ATTRIBUTE_UTILITY_HPP
#define _EXEGETE_ATTRIBUTE_UTILITY_HPP
#include "typeutil.h"
#include "debugutil.h"
#include "errorutil.h"

/** @file attribute_utility.hpp
 * 
 * @brief Interface handling complex attributes
 * 
 * This file contains the attribute utility template classes to handel complex
 * attribute types such as sparse attributes.
 * 
 * @todo union
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

	/** @brief Interface handling a basic attribute
	 * 
	 * This class represents an attribute type corresponding to a column in
	 * the dataset. This is the class to specialize to a new attribute type.
	 */
	template<class A, class F, class I, bool S>
	class basic_attribute_utility
	{
	public:
		/** Indicates the attribute type is not sparse. **/
		enum{IS_SPARSE=S};
		
	public:
		/** Defines a TypeUtil as limit type. **/
		typedef TypeUtil<F>		limit_type;
		/** Defines the template parameter as a value type. **/
		typedef A				value_type;
		/** Defines the template parameter as an attribute type. **/
		typedef F				feature_type;
		/** Defines pointer to template parameter as pointer. **/
		typedef A*				pointer;
		/** Defines constant pointer to template parameter as constant pointer. **/
		typedef const A*		const_pointer;
		/** Defines an int as an index type. **/
		typedef I				index_type;
		/** Defines a size_t as a size type. **/
		typedef size_t			size_type;
		/** Defines a pointer difference as a difference type. */
		typedef ptrdiff_t   	difference_type;
		
	public:
		/** Defines an attribute reference. **/
		typedef A& reference;
		/** Defines a constant attribute reference. **/
		typedef const A& const_reference;
		/** Defines an feature reference. **/
		typedef F& feature_reference;
		/** Defines a constant feature reference. **/
		typedef const F& const_feature_reference;
		/** Defines an index reference. **/
		typedef I& index_reference;
		/** Defines a constant index reference. **/
		typedef const I& const_index_reference;
		
	public:
		/** Gets the internal value of a missing attribute.
		 *
		 * @return maximum value for type.
		 */
		static feature_type missing()
		{
			return limit_type::max();
		}
	};
	/** @brief Interface handling a basic attribute
	 * 
	 * This class represents an attribute type corresponding to a column in
	 * the dataset. This is the class to specialize to a new attribute type.
	 */
	template<class A>
	class attribute_utility : public basic_attribute_utility<A, A, int, 0>
	{
		typedef basic_attribute_utility<A, A, int, 0> util_type;
	public:
		/** Defines a value type. **/
		typedef typename util_type::value_type value_type;
		/** Defines a feature value type. **/
		typedef typename util_type::feature_type feature_type;
		/** Defines an index type. **/
		typedef typename util_type::index_type index_type;
		/** Defines a reference. **/
		typedef typename util_type::reference reference;
		/** Defines a constant reference. **/
		typedef typename util_type::const_reference const_reference;
		/** Defines a feature reference. **/
		typedef typename util_type::feature_reference feature_reference;
		/** Defines a constant feature reference. **/
		typedef typename util_type::const_feature_reference const_feature_reference;
		/** Defines a index reference. **/
		typedef typename util_type::index_reference index_reference;
		/** Defines a constant index reference. **/
		typedef typename util_type::const_index_reference const_index_reference;
		
	public:
		/** Converts an attribute to a value.
		 * 
		 * @note It does nothing for standard attributes.
		 *
		 * @param attr a constant reference to an attribute type.
		 * @return attribute value
		 */
		static const_feature_reference valueOf(const_reference attr)
		{
			return attr;
		}
		/** Converts an attribute to a value.
		 * 
		 * @note It does nothing for standard attributes.
		 *
		 * @param attr a reference to an attribute type.
		 * @return attribute value
		 */
		static feature_reference valueOf(reference attr)
		{
			return attr;
		}
		/** Converts an attribute type to an index type.
		 *
		 * @param attr an attribute.
		 * @return second member of a std::pair.
		 */
		static const_index_reference indexOf(const_reference attr)
		{
			return 0;
		}
		/** Converts an attribute type to an index type.
		 *
		 * @param attr an attribute.
		 * @return second member of a std::pair.
		 */
		static const_index_reference indexOf(reference attr)
		{
			return 0;
		}
	};
	
	/** @brief Interface handling a sparse attribute
	 * 
	 * This class represents an attribute type corresponding to a column in
	 * the dataset.
	 */
	template<class A, class I>
	class attribute_utility< std::pair<A, I> > : public basic_attribute_utility<std::pair<A, I>, A, I, 1>
	{		
		typedef basic_attribute_utility< std::pair<A, I>, A, int, 0> util_type;
	public:
		/** Defines a value type. **/
		typedef typename util_type::value_type value_type;
		/** Defines a feature value type. **/
		typedef typename util_type::feature_type feature_type;
		/** Defines an index type. **/
		typedef typename util_type::index_type index_type;
		/** Defines a reference. **/
		typedef typename util_type::reference reference;
		/** Defines a constant reference. **/
		typedef typename util_type::const_reference const_reference;
		/** Defines a feature reference. **/
		typedef typename util_type::feature_reference feature_reference;
		/** Defines a constant feature reference. **/
		typedef typename util_type::const_feature_reference const_feature_reference;
		/** Defines a index reference. **/
		typedef typename util_type::index_reference index_reference;
		/** Defines a constant index reference. **/
		typedef typename util_type::const_index_reference const_index_reference;
		
	public:
		/** Converts an attribute to a value.
		 * 
		 * @note It does nothing for standard attributes.
		 *
		 * @param attr a constant reference to an attribute type.
		 * @return attribute value
		 */
		static const_feature_reference valueOf(const_reference attr)
		{
			return attr.first;
		}
		/** Converts an attribute to a value.
		 * 
		 * @note It does nothing for standard attributes.
		 *
		 * @param attr a reference to an attribute type.
		 * @return attribute value
		 */
		static feature_reference valueOf(reference attr)
		{
			return attr.first;
		}
		/** Converts an attribute type to an index type.
		 *
		 * @param attr an attribute.
		 * @return second member of a std::pair.
		 */
		static const_index_reference indexOf(const_reference attr)
		{
			return attr.second;
		}
		/** Converts an attribute type to an index type.
		 *
		 * @param attr an attribute.
		 * @return second member of a std::pair.
		 */
		static index_reference indexOf(reference attr)
		{
			return attr.second;
		}
	};
	
	/** @brief Defines an iterator utility for non-sparse types.
	 * 
	 * This class defines an iterator utility for non-sparse types.
	 */
	template<class A, int I = attribute_utility<A>::IS_SPARSE>
	class attribute_iterator_utility
	{
	public:
		/** Tests if iterator has reached the end.
		 * 
		 * @param beg start iterator.
		 * @param end end iterator.
		 * @return true if not end.
		 */
		static bool not_end(const A* beg, const A* end)
		{
			return beg != end;
		}
		/** Gets distance between iterators.
		 * 
		 * @param beg start iterator.
		 * @param end end iterator.
		 * @return distance between iterators.
		 */
		static ptrdiff_t distance(const A* beg, const A* end)
		{
			return std::distance(beg, end);
		}
	};
	/** @brief Defines an iterator utility for sparse types.
	 * 
	 * This class defines an iterator utility for sparse types.
	 */
	template<class A>
	class attribute_iterator_utility<A, 1>
	{
	public:
		/** Tests if iterator has reached the end.
		 * 
		 * @param beg start iterator.
		 * @param end end iterator.
		 * @return true if not end.
		 */
		static bool not_end(const A* beg, const A* end)
		{
			return attribute_utility<A>::indexOf(*beg) != -1;
		}
		/** Gets distance between iterators.
		 * 
		 * @param beg start iterator.
		 * @param end end iterator.
		 * @return distance between iterators.
		 */
		static ptrdiff_t distance(const A* beg, const A* end)
		{
			return attribute_utility<A>::indexOf(*end)-1;
		}
	};
};


#endif



