/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * dataset.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_DATASET_HPP
#define _EXEGETE_DATASET_HPP
#include "header/attribute_header_set.hpp"
#include "example/example_set.hpp"
#include "example/example.hpp"
#include "example/bag.hpp"

/** @file dataset.hpp
 * 
 * @brief Defines a dataset
 * 
 * This file contains a dataset.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a dataset
	 * 
	 * This class template defines a dataset.
	 */
	template
	< 
		class A, class Y, class W, 
		template<class> class alloc_type, 
		class alloc_base = typename alloc_type<A>::base_type, 
		class header_base = attribute_header_base< A, Y > 
	>
	class dataset : public alloc_base, public header_base
	{
		template<class A1, class Y1, class W1, template<class> class AL, class AB, class HB> friend class dataset;
		typedef example_set< example<A, Y, W> > 	example_vec;
		typedef example_set< bag<A, Y, W> > 		bag_vec;
	public:
		/** Defines an attribute allocator type. **/
		typedef alloc_type< A > 				allocator_type;
		/** Defines an attribute header set. **/
		typedef attribute_header_set< A, Y > 	header_set;
	public:
		/** Defines an Example as a value type. **/
		typedef typename example_vec::value_type		value_type;
		/** Defines a reference to an example. **/
		typedef typename example_vec::reference			reference;
		/** Defines a constant reference to an example. **/
		typedef typename example_vec::const_reference	const_reference;
		/** Defines an iterator to an example. **/
		typedef typename example_vec::iterator			iterator;
		/** Defines a constant iterator to an example. **/
		typedef typename example_vec::const_iterator	const_iterator;
	public:
		/** Defines a bag type. **/
		typedef typename bag_vec::value_type			bag_type;
		/** Defines a reference to a bag. **/
		typedef typename bag_vec::reference				bag_reference;
		/** Defines a constant reference to a bag. **/
		typedef typename bag_vec::const_reference		const_bag_reference;
		/** Defines an iterator to a bag. **/
		typedef typename bag_vec::iterator				bag_iterator;
		/** Defines a constant iterator to a bag. **/
		typedef typename bag_vec::const_iterator		const_bag_iterator;
	public:
		/** Defines a header iterator. **/
		typedef typename header_base::header_iterator		header_iterator;
		/** Defines a constant header iterator. **/
		typedef typename header_base::const_header_iterator	const_header_iterator;
		/** Defines a label iterator. **/
		typedef typename header_base::label_iterator		label_iterator;
		/** Defines a constant label iterator. **/
		typedef typename header_base::const_label_iterator	const_label_iterator;
	public:
		/** Defines an example type utility. **/
		typedef typename allocator_type::type_util			type_util;
		/** Defines a size type of an example. **/
		typedef typename type_util::size_type				size_type;
		/** Defines a difference type. **/
		typedef typename type_util::difference_type			difference_type;
		/** Defines an example attribute type. **/
		typedef typename type_util::value_type				attribute_type;
		/** Defines an example value type as a feature type. **/
		typedef typename type_util::feature_type			feature_type;
		/** Defines an example attribute pointer. **/
		typedef typename type_util::pointer					attribute_pointer;
		/** Defines an example constant attribute pointer. **/
		typedef typename type_util::const_pointer			const_attribute_pointer;
		/** Defines an example index type. **/
		typedef typename type_util::index_type				index_type;
	public:
		/** Defines a class type. **/
		typedef typename value_type::class_type				class_type;
		/** Defines a weight type. **/
		typedef typename value_type::weight_type 			weight_type;
		
	public:
		/** Constructs a dataset.
		 */
		dataset()
		{
		}
		/** Destructs a dataset.
		 */
		~dataset()
		{
		}
		
	public:
		/** Constructs a copy of the dataset.
		 * 
		 * @param d a dataset.
		 */
		dataset(const dataset& d) : alloc_base(d), header_base(d), e_set(d.e_set), b_set(d.b_set)
		{
		}
		/** Assigns a copy of the dataset.
		 * 
		 * @param d a dataset.
		 * @return this object.
		 */
		dataset& operator=(const dataset& d)
		{
			alloc_base::operator=(d);
			header_base::operator=(d);
			e_set = d.e_set;
			b_set = d.b_set;
			return *this;
		}
		/** Constructs a copy of the dataset.
		 * 
		 * @param d a dataset.
		 */
		template<class AB, class HB>
		dataset(const dataset<A,Y,W,alloc_type,AB,HB>& d) : alloc_base(d), header_base(d), e_set(d.e_set), b_set(d.b_set)
		{
		}
		/** Assigns a copy of the dataset.
		 * 
		 * @param d a dataset.
		 * @return this object.
		 */
		template<class AB, class HB>
		dataset& operator=(const dataset<A,Y,W,alloc_type,AB,HB>& d)
		{
			alloc_base::operator=(d);
			header_base::operator=(d);
			e_set = d.e_set;
			b_set = d.b_set;
			return *this;
		}
		
	public:
		/** Resizes the number of examples.
		 *
		 * @param n a new collection size.
		 */
		void resize(size_type n)
		{
			e_set.resize(n);
		}
		/** Gets an example at the specified index.
		 *
		 * @param n an index of the example.
		 * @return a reference to a specific example.
		 */
		reference operator[](size_type n)
		{
			return e_set[n];
		}
		/** Gets a constant example reference at the specified index.
		 *
		 * @param n an index of the example.
		 * @return a constant reference to a specific example.
		 */
		const_reference operator[](size_type n)const
		{
			return e_set[n];
		}
		/** Clear the example set.
		 */
		void clear()
		{
			e_set.clear();
			b_set.clear();
		}
		/** Gets the number of examples.
		 *
		 * @return the number of examples.
		 */
		size_type size()const
		{
			return e_set.size();
		}
		/** Tests if example set is empty.
		 * 
		 * @return true if example set is empty.
		 */
		bool empty()const
		{
			return e_set.empty();
		}
		/** Gets an iterator to the start of the example set.
		 *
		 * @return a example iterator pointing to the start of the collection.
		 */
		iterator begin()
		{
			return e_set.begin();
		}
		/** Gets an iterator to the end of the example set.
		 *
		 * @return a example iterator pointing to the end of the collection.
		 */
		iterator end()
		{
			return e_set.end();
		}
		/** Gets an iterator to the start of the example set.
		 *
		 * @return a constant example iterator pointing to the start of the collection.
		 */
		const_iterator begin()const
		{
			return e_set.begin();
		}
		/** Gets an iterator to the end of the example set.
		 *
		 * @return a constant example iterator pointing to the end of the collection.
		 */
		const_iterator end()const
		{
			return e_set.end();
		}
		
	public:
		/** Resizes the number of bags.
		 *
		 * @param n a new collection size.
		 */
		void resize_bags(size_type n)
		{
			b_set.resize(n);
		}
		/** Gets the number of bags.
		 *
		 * @return the number of bags.
		 */
		size_type bag_count()const
		{
			return b_set.size();
		}
		/** Gets an iterator to the start of a bag collection.
		 *
		 * @return a beginning iterator.
		 */
		bag_iterator bag_begin()
		{
			return b_set.begin();
		}
		/** Gets an iterator to the end of a bag collection.
		 *
		 * @return a ending iterator.
		 */
		bag_iterator bag_end()
		{
			return b_set.end();
		}
		/** Gets an iterator to the start of a bag collection.
		 *
		 * @return a beginning constant iterator.
		 */
		const_bag_iterator bag_begin()const
		{
			return b_set.begin();
		}
		/** Gets an iterator to the end of a bag collection.
		 *
		 * @return a ending constant iterator.
		 */
		const_bag_iterator bag_end()const
		{
			return b_set.end();
		}
		
	public:
		/** Gets a string summary of the dataset.
		 *
		 * @param prefix a prefixing comment.
		 * @param l a level.
		 * @return a string representation of the dataset summary.
		 */
		std::string summary(std::string prefix, int l=0)
		{
			std::ostringstream out;
			write_summary(out, prefix, l);
			out << std::endl;
			return out.str();
		}
		/** Write a summary of the dataset.
		 *
		 * @param out an output stream.
		 * @param prefix a prefixing comment.
		 * @param l a level.
		 * @return a string representation of the dataset summary.
		 */
		void write_summary(std::ostream& out, std::string prefix, int l=0)const
		{
			if( bag_count() > 0 ) 
			out << prefix << "Bags:       " << bag_count() << "\n";
			out << prefix << "Examples:   " << size() << "\n";
			out << prefix << "Attributes: " << header_base::attribute_count() << "\n";
			out << prefix << "Classes:    " << header_base::class_count() << "\n";
			out << prefix << "Binary:     " << header_base::binary_count() << "\n";
			out << prefix << "Discreet:   " << header_base::discreet_count() << "\n";
			out << prefix << "Numeric:    " << header_base::numeric_count() << "\n";
			out << prefix << "Nominal:    " << header_base::nominal_count() << "\n";
			out << prefix << "Missing:    " << header_base::missing_count() << "\n";
			out << prefix << "Normalized: " << header_base::normalized_count() << "\n";
			
			if( header_base::class_count() == 2 )
			out << prefix << "Positive:   " << header_base::class_begin()[1] << "\n";
			
			if( type_traits<Y>::is_discreet )
			{
				for(const_label_iterator beg = header_base::class_begin(), curr=beg, end=header_base::class_end();curr != end;++curr)
				{
					out << prefix << *curr << ": " << count_examples_for_class(std::distance(beg, curr));
					if( bag_count() > 0 ) out << " & " << count_bags_for_class(std::distance(beg, curr)) << "\n";
					else out << "\n";
				}
			}
			if( l < 1 ) return;
			for(const_header_iterator curr = header_base::attribute_begin(), beg=curr, end=header_base::attribute_end();curr != end;++curr)
			{
				curr->write_summary(out, prefix, std::distance(beg, curr));
			}
		}
		/** Count number of examples for specified class.
		 * 
		 * @param y a class index.
		 * @return number of examples.
		 */
		size_type count_examples_for_class(class_type y)const
		{
			size_type cnt=0;
			for(const_iterator beg = begin(), fin=end();beg != fin;++beg)
				if(beg->y() == y) cnt++;
			return cnt;
		}
		/** Count number of bags for specified class.
		 * 
		 * @param y a class index.
		 * @return number of bags.
		 */
		size_type count_bags_for_class(class_type y)const
		{
			size_type cnt=0;
			for(const_bag_iterator beg = bag_begin(), fin=bag_end();beg != fin;++beg)
				if(beg->y() == y) cnt++;
			return cnt;
		}
		
	public:
		/** Assigns the given example set. If the example set contains
		 * bags then it copies on the bag level.
		 *
		 * @param data source example set.
		 */
		template<class W1, class AB, class HB>
		void assign(dataset<A, Y, W1, alloc_type, AB, HB>& data)
		{
			if( data.bag_count() > 0) assign(data.bag_begin(), data.bag_end());
			else assign(data.begin(), data.end());
		}
		/** Assigns a subset of the given dataset outside the given range.
		 *
		 * @param data full dataset.
		 * @param beg start index of subset.
		 * @param end end index of subset.
		 */
		template<class W1, class AB, class HB>
		void assign_exclude(dataset<A, Y, W1, alloc_type, AB, HB>& data, size_type beg, size_type end)
		{
			if( data.bag_count() > 0) assign(data.bag_begin(), data.bag_begin()+beg, data.bag_begin()+end, data.bag_end());
			else assign(data.begin(), data.begin()+beg, data.begin()+end, data.end());
		}
		/** Assigns a subset of the given dataset inside the given range.
		 *
		 * @param data full dataset.
		 * @param beg start index of subset.
		 * @param end end index of subset.
		 */
		template<class W1, class AB, class HB>
		void assign_include(dataset<A, Y, W1, alloc_type, AB, HB>& data, size_type beg, size_type end)
		{
			if( data.bag_count() > 0) 
			{
				assign(data.bag_begin()+beg, data.bag_begin()+end);
			}
			else assign(data.begin()+beg, data.begin()+end);
		}
		
	public:
		/** Assigns a collection of examples.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class W1>
		void assign(example<A, Y, W1>* beg, example<A, Y, W1>* end)
		{
			e_set.assign(beg, end);
		}
		/** Appends a collection of examples.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class W1>
		void append(example<A, Y, W1>* beg, example<A, Y, W1>* end)
		{
			e_set.append(beg, end);
		}
		/** Assigns two collections of examples.
		 *
		 * @param beg1 an iterator to the start of the collection.
		 * @param end1 an iterator to the end of the collection.
		 * @param beg2 an iterator to the start of the collection.
		 * @param end2 an iterator to the end of the collection.
		 */
		template<class W1>
		void assign(example<A, Y, W1>* beg1, example<A, Y, W1>* end1, example<A, Y, W1>* beg2, example<A, Y, W1>* end2)
		{
			assign(beg1, end1);
			if(beg2 != end2) append(beg2, end2);
		}
		
	public:
		/** Assigns a collection of examples.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class W1>
		void append(bag<A, Y, W1>* beg, bag<A, Y, W1>* end)
		{
			size_type n = b_set.size()+(end-beg);
			b_set.setsize(n);
			for(bag_iterator it=b_set.end();beg != end;++beg, ++it)
			{
				*it = *beg;
				e_set.append(beg->begin(), beg->end());
				it->begin(e_set.end()-beg->size());
				it->end(e_set.end());
			}
			b_set.resize(n);
			copy_bag_pos(bag_begin(), bag_end(), begin());
		}
		/** Appends a collection of examples.
		 *
		 * @param beg an iterator to the start of the collection.
		 * @param end an iterator to the end of the collection.
		 */
		template<class W1>
		void assign(bag<A, Y, W1>* beg, bag<A, Y, W1>* end)
		{
			b_set.reset();
			b_set.reset();
			append(beg, end);
		}
		/** Assigns two collections of examples.
		 *
		 * @param beg1 an iterator to the start of the collection.
		 * @param end1 an iterator to the end of the collection.
		 * @param beg2 an iterator to the start of the collection.
		 * @param end2 an iterator to the end of the collection.
		 */
		template<class W1>
		void assign(bag<A, Y, W1>* beg1, bag<A, Y, W1>* end1, bag<A, Y, W1>* beg2, bag<A, Y, W1>* end2)
		{
			assign(beg1, end1);
			if(beg2 != end2) append(beg2, end2);
		}
		
	private:
		static void copy_bag_pos(bag_iterator beg, bag_iterator end, iterator ebeg)
		{
			size_type n;
			for(;beg != end;++beg)
			{
				n = beg->size();
				beg->begin(ebeg);
				ebeg += n;
				beg->end(ebeg);
			}
		}
		
	private:
		example_vec e_set;
		bag_vec b_set;
	};

#include <map>
	/** @brief Defines a concrete dataset
	 * 
	 * This class template defines a concrete dataset.
	 */
	template<class A, class Y, class W, template<class> class alloc_type >
	class concrete_dataset : public dataset<A, Y, W, alloc_type, alloc_type<A>, attribute_header_set<A, Y> >
	{
		typedef dataset<A, Y, W, alloc_type, alloc_type<A>, attribute_header_set<A, Y> > parent_type;
	public:
		/** Defines a shallow set. **/
		typedef dataset<A, Y, W, alloc_type> 		shallow_set;
		/** Defines a value type. **/
		typedef typename parent_type::value_type 	value_type;
		/** Defines a size type. **/
		typedef typename parent_type::size_type 	size_type;
		/** Defines a class type. **/
		typedef typename parent_type::class_type 	class_type;
		/** Defines an attribute allocator type. **/
		typedef typename parent_type::allocator_type allocator_type;
		/** Defines an attribute header set. **/
		typedef typename parent_type::header_set 	header_set;
		/** Defines a label type. **/
		typedef typename header_set::label_type		label_type;
		/** Defines an iterator. **/
		typedef typename parent_type::iterator 		iterator;
		/** Defines a bag iterator. **/
		typedef typename parent_type::bag_iterator 	bag_iterator;
		/** Defines an attribute pointer. **/
		typedef typename parent_type::attribute_pointer attribute_pointer;
		
	public:
		/** Construct a concrete dataset.
		 */
		concrete_dataset()
		{
		}
		
	public:
		/** Resize the header and number of attributes for a dataset.
		 * 
		 * @param beg start of name collection.
		 * @param end end of name collection.
		 */
		template<class I>
		void reassign_header(I beg, I end)
		{
			allocator_type::resize(parent_type::size()*std::distance(beg, end), std::distance(beg, end), parent_type::size());
			header_set::reassign_header(beg, end);
			attribute_pointer fbeg = allocator_type::feature_begin();
			for(iterator beg = parent_type::begin(), end = parent_type::end();beg != end;++beg)
			{
				beg->x(fbeg);
				fbeg += parent_type::attribute_count();
			}
		}
		/** Resize all data.
		 * 
		 * @param e number of examples.
		 * @param t total number of attributes.
		 * @param a number of attribute columns.
		 * @param l number of labels.
		 */
		void resize(size_type e, size_type t, size_type a, size_type l)
		{
			allocator_type::resize(t, a, e);
			header_set::resize_header(a, e, l);
			parent_type::resize(e);
		}
		/** Clear the dataset.
		 */
		void clear()
		{
			parent_type::clear();
			header_set::clear();
			allocator_type::clear();
		}
		/** Finalize the dataset.
		 */
		void finalize()
		{
			header_set::class_count( (class_type)header_set::class_header().size() );
			header_set::reset_statistics();
			allocator_type::estimate_statistics(parent_type::begin(), parent_type::end(), header_set::attribute_begin(), header_set::attribute_end());
			header_set::finalize_statistics(parent_type::size());
		}
		
	public:
		/** Relabels the examples in the example set. Any example class label found in the
		 * positive label vector is reassigned to the positive class. In the end, there are only 
		 * two classes following the specified labels.
		 *
		 * @param posclasses a vector of positive class labels.
		 * @param pos a new label for the positive class.
		 * @param neg a new label for the negative class.
		 */
		void label_positive(std::vector<std::string>& posclasses, const label_type& pos, const label_type& neg)
		{
			std::stable_sort(posclasses.begin(), posclasses.end());
			std::vector<class_type> classes(parent_type::class_count());
			for(unsigned int i=0;i<classes.size();++i) classes[i] = std::binary_search(posclasses.begin(), posclasses.end(), parent_type::class_begin()[i])?1:0;
			for(iterator curr=parent_type::begin(), end=parent_type::end();curr != end;++curr) if( curr->y() != value_type::missing_class() ) curr->y( classes[curr->y()] );
			parent_type::class_header().clear();
			parent_type::class_header().push_back(neg);
			parent_type::class_header().push_back(pos);
			parent_type::class_count(2);
		}
		/** Ensures the dataset has the correct number of:
		 * 	- Classes
		 * 	- Attributes
		 * 	- Examples
		 * 	- Bags
		 * 
		 * @param ensureVec a vector of values in the above order.
		 * @return an error message if one value does not match otherwise NULL.
		 */
		const char* ensure(std::vector<unsigned int>& ensureVec)const
		{
			if( !ensureVec.empty() )
			{
				if( parent_type::class_count() != ensureVec[0] )		return ERRORMSG("Expected " << ensureVec[0] << " classes but only found " << parent_type::class_count() << " classes");
				if( ensureVec.size() > 1 && 
					parent_type::attribute_count() != ensureVec[1] )	return ERRORMSG("Expected " << ensureVec[1] << " attributes but only found " << parent_type::attribute_count() << " attributes");
				if( ensureVec.size() > 2 && 
					parent_type::size() != ensureVec[2] )			return ERRORMSG("Expected " << ensureVec[2] << " examples but only found " << parent_type::size() << " examples");
				if( ensureVec.size() > 3 && 
					parent_type::bag_count() != ensureVec[3] )		return ERRORMSG("Expected " << ensureVec[3] << " bags but only found " << parent_type::bag_count() << " bags");
			}
			return 0;
		}
		/** Builds a collection of bags based on the label. It uses the label index, and 
		 * substring beg and end to find the unique bag identifier.
		 *
		 * @param bagIdx index in label vector to find unique bag id.
		 * @param bagBeg start of unique bag id substring.
		 * @param bagEnd end of unique bag id substring.
		 * @return error message or NULL.
		 */
		const char* build_bags(size_type bagIdx, size_type bagBeg, size_type bagEnd)
		{
			typedef std::map<std::string, unsigned int> bag_map;
			typedef typename header_set::label_type* label_pointer;
			typename bag_map::iterator it;
			bag_map bagmap;
			std::string str, last;
			iterator ebeg, eend;

			for(ebeg = parent_type::begin(), eend=parent_type::end();ebeg != eend;++ebeg)
			{
				str = ebeg->labelAt(bagIdx);
				if( bagEnd > bagBeg ) str = str.substr(bagBeg, (bagEnd-bagBeg));
				if( (it=bagmap.find(str)) != bagmap.end() ) it->second++;
				else bagmap.insert(std::make_pair(str, 1));
			}
			parent_type::resize_bags(bagmap.size());
			resize_bag_label( bagmap.size() );
			bag_iterator bbeg=parent_type::bag_begin();
			bag_iterator bend=parent_type::bag_end();
			class_type cl=0;
			label_pointer l_ptr = header_set::label_start()+(parent_type::size()*header_set::label_count());
			for(ebeg = parent_type::begin(), eend=parent_type::end();ebeg != eend;++ebeg)
			{
				str = ebeg->labelAt(bagIdx);
				if( bagEnd > bagBeg ) str = str.substr(bagBeg, (bagEnd-bagBeg));
				if( str != last ) 
				{
					if( !last.empty() ) 
					{
						bbeg->y( cl );
						bbeg->end(ebeg);
						++bbeg;
						if( bbeg == bend ) return ERRORMSG("Error building bags");
						cl = 0;
					}
					last = str;
					bbeg->begin(ebeg);
					bbeg->label(l_ptr);
					*l_ptr = str;
					++l_ptr;
				}
				if( ebeg->y() > 0 && cl == 0 ) cl = ebeg->y();
			}
			bbeg->y( cl );
			bbeg->end(ebeg);
			return 0;
		}
		
	protected:
		/** Resize the number of labels to include bags.
		 * 
		 * @param b number of bags.
		 */
		void resize_bag_label(size_type b)
		{
			typedef typename header_set::label_type* label_pointer;
			parent_type::resize_labels(parent_type::size(), b);
			label_pointer lbeg=header_set::label_start();
			for(iterator beg = parent_type::begin(), end=parent_type::end();beg != end;++beg)
			{
				beg->label(lbeg);
				lbeg+=header_set::label_count();
			}
		}
	};
};

#endif

