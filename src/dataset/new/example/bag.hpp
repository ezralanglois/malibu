/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * bag.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_BAG_HPP
#define _EXEGETE_BAG_HPP
#include "example.hpp"

/** @file bag.hpp
 * 
 * @brief Defines a bag of examples.
 * 
 * This file contains a bag of examples.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an bag label
	 * 
	 * This class template defines an bag label.
	 */
	template<class A, class Y, class W>
	class bag : public example_label<Y, W>
	{
		typedef example_label<Y, W> parent_type;
	public:
		/** Defines a value type. **/
		typedef example<A, Y, W> value_type;
		/** Defines a pointer. **/
		typedef example<A, Y, W>* pointer;
		/** Defines a constant pointer. **/
		typedef const example<A, Y, W>* const_pointer;
	public:
		/** Defines a class type. **/
		typedef typename parent_type::class_type class_type;
		
	public:
		/** Construct an attribute vector.
		 */
		bag() : e_beg(0), e_end()
		{
		}
		
	public:
		/** Constructs a shallow copy of a bag of examples.
		 *
		 * @param b a constant reference to an bag.
		 */
		bag(const bag& b) : parent_type(b), e_beg(b.e_beg), e_end(b.e_end)
		{
		}
		/** Assigns a shallow copy  of a bag of examples.
		 *
		 * @param b a constant reference to an bag.
		 * @return a reference to this bag.
		 */
		bag& operator=(const bag& b)
		{
			parent_type::operator=(b);
			e_beg = b.e_beg;
			e_end = b.e_end;
			return *this;
		}
		/** Constructs a shallow copy of an attribute vector and label.
		 *
		 * @param b a constant reference to an bag.
		 */
		template<class X1, class W1>
		bag(const bag<X1,Y,W1>& b) : parent_type(b), e_beg(0), e_end(0)
		{
		}
		/** Assigns a shallow copy of an attribute vector and label.
		 *
		 * @param b a constant reference to an bag.
		 * @return a reference to this bag.
		 */
		template<class X1, class W1>
		bag& operator=(const bag<X1,Y,W1>& b)
		{
			parent_type::operator=(b);
			return *this;
		}
		
	public:
		/** Gets a pointer to the first example.
		 *
		 * @return a pointer to the first example.
		 */
		pointer begin()
		{
			return e_beg;
		}
		/** Gets a pointer to the last example.
		 *
		 * @return a pointer to the last example.
		 */
		pointer end()
		{
			return e_end;
		}
		/** Gets a constant pointer to the first example.
		 *
		 * @return a constant pointer to the first example.
		 */
		const_pointer begin()const
		{
			return e_beg;
		}
		/** Gets a constant pointer to the last example.
		 *
		 * @return a constant pointer to the last example.
		 */
		const_pointer end()const
		{
			return e_end;
		}
		/** Sets a pointer to the last example.
		 *
		 * @param p a pointer to the last example.
		 */
		void begin(pointer p)
		{
			e_beg = p;
		}
		/** Sets a pointer to the last example.
		 *
		 * @param p a pointer to the last example.
		 */
		void end(pointer p)
		{
			e_end = p;
		}
		/** Gets the size of the bag.
		 *
		 * @return the number of bags.
		 */
		unsigned int size()const
		{
			return (unsigned int)(e_end-e_beg);
		}
		/** Implicitly converts an bag to a class type.
		 * 
		 * @note this is dangerous but, currently, a necessary hack.
		 * 
		 * @return a class type.
		 */
		operator class_type()const
		{
			return parent_type::y();
		}
		
	private:
		pointer e_beg;
		pointer e_end;
	};
};

/** @brief Defines a type utility for an bag
 * 
 * This specialized class template defines a TypeUtil for an bag<X,Y,W>.
 */
template<class X, class Y, class W>
struct TypeUtil< ::exegete::bag<X,Y,W> >
{
	/** Flags the class as POD. **/
	enum{ispod=true};
	/** Defines a bag<X,Y,W> as a value type. **/
	typedef ::exegete::bag<X,Y,W> value_type;
	/** Gets the name of the type.
	 *
	 * @return bag
	*/
	static const char* name() 
	{
		return "bag";
	}
};

#endif


