/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * example.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_EXAMPLE_HPP
#define _EXEGETE_EXAMPLE_HPP
#include "example_label.hpp"
#include "../attribute/attribute_utility.hpp"

/** @file example.hpp
 * 
 * @brief Defines example label and specializations.
 * 
 * This file contains the ExampleLabel class template.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an example label
	 * 
	 * This class template defines an example label.
	 */
	template<class A, class Y, class W>
	class example : public example_label<Y, W>
	{
		typedef example_label<Y, W> parent_type;
	public:
		/** Defines a type utility. **/
		typedef attribute_utility<A> type_util;
		/** Defines an attribute pointer. **/
		typedef typename type_util::pointer pointer;
		/** Defines a class type. **/
		typedef typename parent_type::class_type class_type;
		/** Defines a reference. **/
		typedef typename type_util::reference reference;
		/** Defines a constant reference. **/
		typedef typename type_util::const_reference const_reference;
		/** Defines a size type. **/
		typedef typename type_util::size_type size_type;
		
	public:
		/** Construct an attribute vector.
		 */
		example() : a_val(0)
		{
		}
		
	public:
		/** Constructs a shallow copy of an attribute vector and label.
		 *
		 * @param e a constant reference to an example.
		 */
		example(const example& e) : parent_type(e), a_val(e.a_val)
		{
		}
		/** Assigns a shallow copy of an attribute vector and label.
		 *
		 * @param e a constant reference to an example.
		 * @return a reference to this example.
		 */
		example& operator=(const example& e)
		{
			parent_type::operator=(e);
			a_val = e.a_val;
			return *this;
		}
		/** Constructs a shallow copy of an attribute vector and label.
		 *
		 * @param e a constant reference to an example.
		 */
		template<class X1, class W1>
		example(const example<X1,Y,W1>& e) : parent_type(e), a_val(0)
		{
		}
		/** Assigns a shallow copy of an attribute vector and label.
		 *
		 * @param e a constant reference to an example.
		 * @return a reference to this example.
		 */
		template<class X1, class W1>
		example& operator=(const example<X1,Y,W1>& e)
		{
			parent_type::operator=(e);
			return *this;
		}
		
	public:
		/** Get attribute at index.
		 * 
		 * @param n specified index.
		 * @return attribute value.
		 */
		reference operator[](size_type n)
		{
			return a_val[n];
		}
		/** Get attribute at index.
		 * 
		 * @param n specified index.
		 * @return attribute value.
		 */
		const_reference operator[](size_type n)const
		{
			return a_val[n];
		}
		
	public:
		/** Shallow copy of the attribute vector. 
		 * 
		 * @note It sets the parameter vector to NULL.
		 *
		 * @param av an attribute vector.
		 */
		void shallow(example& v)
		{
			parent_type::operator=(v);
			a_val = v.a_val;
			v.a_val = 0;
		}
		/** Set the attribute vector and class labe.
		 * 
		 * @param xval attribute vector.
		 * @param yval class label.
		 */
		void set(pointer xval, class_type yval)
		{
			a_val = xval;
			parent_type::y( yval );
		}
		
	public:
		/** Implicitly converts an example to an attribute pointer.
		 *
		 * @return an attribute pointer.
		 */
		operator pointer()
		{
			return a_val;
		}
		/** Implicitly converts an example to an attribute pointer.
		 *
		 * @return an attribute pointer.
		 */
		operator pointer()const
		{
			return const_cast<pointer>(a_val);
		}
		/** Implicitly converts an example to a class type.
		 * 
		 * @note this is dangerous but, currently, a necessary hack.
		 * 
		 * @return a class type.
		 */
		operator class_type()const
		{
			return parent_type::y();
		}
		/** Get the attribute pointer.
		 * 
		 * @param a a source attribute pointer.
		 */
		void x(pointer a)
		{
			a_val = a;
		}
		/** Get the attribute pointer.
		 * 
		 * @return attribute pointer.
		 */
		pointer& x()
		{
			return a_val;
		}
		/**Get the attribute pointer.
		 * 
		 * @return attribute pointer.
		 */
		pointer x()const
		{
			return a_val;
		}
		
	private:
		pointer a_val;
	};

	template<class E> class attribute_less_than;
	
	template<class A, class Y, class W>
	class attribute_less_than< example<A, Y, W> >
	{
	public:
		/** Defines an example as a value type. **/
		typedef example<A, Y, W> value_type;
		/** Defines a constant reference to an example. **/
		typedef const example<A, Y, W>& const_reference;
		/** Defines an attribute index as a size type. **/
		typedef typename value_type::size_type size_type;
		/** Defines a type utility. **/
		typedef typename value_type::type_util type_util;
	public:
		/** Constructs an attribute comparison with the specified attribute index. 
		 * 
		 * @param i attribute index.
		 */
		attribute_less_than(size_type i) : index(i)
		{
		}
		/** Compares two examples to determine which is larger.
		 *
		 * @param lhs the left hand side source example.
		 * @param rhs the right hand side source example.
		 * @return true of left is less than right.
		 */
		bool operator()(const_reference lhs, const_reference rhs)const 
		{
			return type_util::valueOf( lhs[index] ) < type_util::valueOf( rhs[index] );
		}
		
	private:
		size_type index;
	};

	template<class A, class Y, class W>
	class attribute_less_than< example<A, Y, W>* > : public attribute_less_than< example<A, Y, W> >
	{
		typedef typename example<A, Y, W>::size_type size_type;
	public:
		/** Constructs an attribute comparison with the specified attribute index. 
		 * 
		 * @param i attribute index.
		 */
		attribute_less_than(size_type n) : attribute_less_than< example<A, Y, W> >(n)
		{
		}
	};
};

/** @brief Defines a type utility for an Example
 * 
 * This specialized class template defines a TypeUtil for an Example<X,Y,W>.
 */
template<class X, class Y, class W>
struct TypeUtil< ::exegete::example<X,Y,W> >
{
	/** Flags the class as POD. **/
	enum{ispod=true};
	/** Defines a Example<X,Y,W> as a value type. **/
	typedef ::exegete::example<X,Y,W> value_type;
	/** Gets the name of the type.
	 *
	 * @return Example
	*/
	static const char* name() 
	{
		return "Example";
	}
};

#endif


