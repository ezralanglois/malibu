/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * example_label.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_EXAMPLE_LABEL_HPP
#define _EXEGETE_EXAMPLE_LABEL_HPP
#include "example_weight.hpp"

/** @file example_label.hpp
 * 
 * @brief Defines example label and specializations.
 * 
 * This file contains the ExampleLabel class template.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an example label
	 * 
	 * This class template defines an example label.
	 */
	template<class Y, class W>
	class example_label : public example_weight<W>
	{
		typedef example_weight<W> parent_type;
	public:
		/** Defines Y as a class type. **/
		typedef Y											class_type;
		/** Defines a weight type. **/
		typedef typename parent_type::weight_type			weight_type;
		/** Defines a label type. **/
		typedef typename parent_type::label_type			label_type;
		
	public:
		/** Constructs an example label.
		 */
		example_label(class_type y=0) : y_val(y)
		{
		}
		
	public:
		/** Constructs a copy of an example label.
		 * 
		 * @param e an example label.
		 */
		example_label(const example_label& e) : parent_type(e), y_val(e.y_val)
		{
		}
		/** Constructs an example label.
		 * 
		 * @param e an example label.
		 */
		template<class W1>
		example_label(const example_label<Y, W1>& e) : parent_type(e), y_val(e.y_val)
		{
		}
		/** Constructs a copy of an example label.
		 * 
		 * @param e an example label.
		 * @return this object.
		 */
		example_label& operator=(const example_label& e)
		{
			y_val = e.y_val;
			parent_type::operator=(e);
			return *this;
		}
		/** Constructs an example label.
		 * 
		 * @param e an example label.
		 * @return this object.
		 */
		template<class W1>
		example_label& operator=(const example_label<Y, W1>& e)
		{
			y_val = e.y_val;
			parent_type::operator=(e);
			return *this;
		}
		
	public:
		/** Get the weight for class.
		 * 
		 * @param p actual class.
		 * @return weight for class.
		 */
		template<class I>
		weight_type weightforclass(I p)
		{
			return parent_type::weightforclass(y_val, p);
		}
		
	public:
		/** Implicitly converts an example (label) to a class type.
		 * 
		 * @note This is dangerous but currently a necessary hack.
		 * 
		 * @return a class type.
		 */
		operator class_type()const
		{
			return y_val;
		}
		/** Gets the class label.
		 *
		 * @return a class label.
		 */
		class_type y()const
		{
			return y_val;
		}
		/** Sets the class label.
		 *
		 * @param val the new class label.
		 */
		void y(class_type val)
		{
			y_val = val;
		}
		/** Gets an internal code for the class type.
		 *
		 * @return a missing class token.
		 */
		static class_type missing_class()
		{
			return TypeUtil<class_type>::max();
		}
		
	public:
		/** Writes an example label to the output stream.
		 *
		 * @param out an output stream.
		 * @param label a constant reference to an ExampleLabel.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const example_label& label)
		{
			label.write(out);
			if( label.yval == missing_class() ) out << "?";
			else out << label.yval;
			return out;
		}
		/** Reads a example label from the input stream.
		 *
		 * @param in an input stream.
		 * @param label a reference to an ExampleLabel.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, example_label& label)
		{
			label.read(in);
			if( in.peek() == '?' )
			{
				in.get();
				label.yval = missing_class();
			}
			else in >> label.yval;
			return in;
		}
		
	private:
		class_type y_val;
	};
};

#endif



