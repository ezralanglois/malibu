/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * example_weight.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_EXAMPLE_WEIGHT_HPP
#define _EXEGETE_EXAMPLE_WEIGHT_HPP
#include <string>

/** @file example_weight.hpp
 * 
 * @brief Defines example weight and specializations.
 * 
 * This file contains the example weight class template.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an example weight
	 * 
	 * This class template defines an example weight.
	 */
	template<class W>
	class example_weight
	{
		template<class W1> friend class example_weight;
	public:
		/** Defines W as a weight type. **/
		typedef W											weight_type;
		/** Defines void as a label type. **/
		typedef void										label_type;
	
	public:
		/** Constructs an example weight.
		 */
		example_weight() : w_val(0)
		{
		}
		
	public:
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 */
		example_weight(const example_weight& e) : w_val(e.w_val)
		{
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 */
		template<class W1>
		example_weight(const example_weight<W1>& e) : w_val(0)
		{
		}
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		example_weight& operator=(const example_weight& e)
		{
			w_val = e.w_val;
			return *this;
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		template<class W1>
		example_weight& operator=(const example_weight<W1>& e)
		{
			return *this;
		}
		
	public:
		/** Tests if an example has a unique label.
		 * 
		 * @return true if has unique label.
		 */
		bool haslabel()const
		{
			return false;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param y an actual class index.
		 * @param p an predicted class index.
		 * @return the weight assigned to this example.
		 */
		template<class I>
		weight_type weightforclass(I y, I p)const
		{
			return y > 0 ? 1 : -1;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param w a dummy weight.
		 * @return the weight assigned to this example.
		 */
		weight_type weight(weight_type w)const
		{
			return w_val;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @return the weight assigned to this example.
		 */
		weight_type w()const
		{
			return w_val;
		}
		/** Sets an example weight.
		 * 
		 * @param v the new weight.
		 */
		void w(weight_type v)
		{
			w_val = v;
		}
		/** Multiplicative update weight.
		 *
		 * @param v an update weight.
		 */
		void w_update(weight_type v)
		{
			w_val *= v;
		}
		
	protected:
		/** Read a weight from an input stream.
		 * 
		 * @param in an input stream.
		 */
		template<class stream>
		void read(stream& in)
		{
			in >> w_val;
			in.get();
		}
		/** Write a weight to an output stream.
		 * 
		 * @param out an output stream.
		 */
		template<class stream>
		void write(stream& out)
		{
			out << w_val << "\t";
		}
		
	private:
		weight_type w_val;
	};

	/** @brief Defines an example weight
	 * 
	 * This class template defines an example weight.
	 */
	template<class W>
	class example_weight<W*>
	{
		template<class W1> friend class example_weight;
	public:
		/** Defines W as a weight type. **/
		typedef W											weight_type;
		/** Defines void as a label type. **/
		typedef void										label_type;
	
	public:
		/** Constructs an example weight.
		 */
		example_weight() : w_val(0)
		{
		}
		
	public:
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 */
		example_weight(const example_weight& e) : w_val(e.w_val)
		{
		}
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 */
		example_weight(const example_weight<W>& e) : w_val(const_cast<W*>(&e.w_val))
		{
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 */
		template<class W1>
		example_weight(const example_weight<W1>& e) : w_val(0)
		{
		}
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		example_weight& operator=(const example_weight& e)
		{
			w_val = e.w_val;
			return *this;
		}
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		example_weight& operator=(const example_weight<W>& e)
		{
			w_val = const_cast<W*>(&e.w_val);
			return *this;
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		template<class W1>
		example_weight& operator=(const example_weight<W1>& e)
		{
			return *this;
		}
		
	public:
		/** Tests if an example has a unique label.
		 * 
		 * @return true if has unique label.
		 */
		bool haslabel()const
		{
			return false;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param y an actual class index.
		 * @param p an predicted class index.
		 * @return the weight assigned to this example.
		 */
		template<class I>
		weight_type weightforclass(I y, I p)const
		{
			ASSERT(w_val != 0);
			return std::abs(weight_type(w_val[y]-w_val[p]));
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param w a dummy weight.
		 * @return the weight assigned to this example.
		 */
		weight_type weight(weight_type w)const
		{
			ASSERT(w_val != 0);
			return *w_val;
		}
		/** Multiplicative update weight.
		 *
		 * @param v a update weight.
		 */
		weight_type w()const
		{
			ASSERT(w_val != 0);
			return *w_val;
		}
		/** Multiplicative update weight.
		 *
		 * @param v a update weight.
		 */
		void w(weight_type v)
		{
			ASSERT(w_val != 0);
			*w_val = v;
		}
		/** Multiplicative update weight.
		 *
		 * @param v a update weight.
		 */
		void w_update(weight_type v)
		{
			ASSERT(w_val != 0);
			*w_val *= v;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @return the weight assigned to this example.
		 */
		weight_type* w_pointer()const
		{
			return w_val;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @return the weight assigned to this example.
		 */
		weight_type*& w_pointer()
		{
			return w_val;
		}
		/** Sets an example weight.
		 * 
		 * @param v the new weight.
		 */
		void w_pointer(weight_type* v)
		{
			w_val = v;
		}
		
	protected:
		/** Read a weight from an input stream.
		 * 
		 * @param in an input stream.
		 */
		template<class stream>
		void read(stream& in)
		{
		}
		/** Write a weight to an output stream.
		 * 
		 * @param out an output stream.
		 */
		template<class stream>
		void write(stream& out)
		{
		}
		
	private:
		weight_type* w_val;
	};
	/** @brief Defines an example weight
	 * 
	 * This class template defines an example weight.
	 */
	template<>
	class example_weight<std::string*>
	{
		template<class W1> friend class example_weight;
	public:
		/** Defines void as a weight type. **/
		typedef void										weight_type;
		/** Defines std::string as a label type. **/
		typedef std::string									label_type;
	
	public:
		/** Constructs an example weight.
		 */
		example_weight() : l_val(0)
		{
		}

	public:
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 */
		example_weight(const example_weight& e) : l_val(e.l_val)
		{
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 */
		template<class W1>
		example_weight(const example_weight<W1>& e) : l_val(0)
		{
		}
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		example_weight& operator=(const example_weight& e)
		{
			l_val = e.l_val;
			return *this;
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		template<class W1>
		example_weight& operator=(const example_weight<W1>& e)
		{
			return *this;
		}
		
	public:
		/** Tests if an example has a unique label.
		 * 
		 * @return true if has unique label.
		 */
		bool haslabel()const
		{
			return l_val != 0 && *l_val != "";
		}
		/** Gets the label assigned to this example.
		 *
		 * @return the string label assigned to this example.
		 */
		template<class I>
		const label_type& labelAt(I n)const
		{
			ASSERT(l_val != 0 );
			return l_val[n];
		}
		/** Gets the label assigned to this example.
		 *
		 * @return the string label assigned to this example.
		 */
		template<class I>
		label_type& labelAt(I n)
		{
			ASSERT(l_val != 0 );
			return l_val[n];
		}
		/** Sets the string label.
		 * 
		 * @param v a string label.
		 */
		void label(label_type* v)
		{
			l_val = v;
		}
		/** Gets the a weight.
		 *
		 * @param w a weight.
		 * @return the weight passed to this method.
		 */
		double weight(double w)const
		{
			return w;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param y an actual class index.
		 * @param p an predicted class index.
		 * @return the weight assigned to this example.
		 */
		template<class I>
		weight_type weightforclass(I y, I p)const
		{
			return y > 0 ? 1 : -1;
		}
		
	protected:
		/** Read a weight from an input stream.
		 * 
		 * @param in an input stream.
		 */
		template<class stream>
		void read(stream& in)
		{
		}
		/** Write a weight to an output stream.
		 * 
		 * @param out an output stream.
		 * @todo write ALL LABELS
		 */
		template<class stream>
		void write(stream& out)
		{
			if( l_val != 0 ) out << *l_val << "\t";
		}
		
	private:
		label_type* l_val;
	};
	/** @brief Defines an example weight
	 * 
	 * This class template defines an example weight.
	 */
	template<>
	class example_weight<std::string>
	{
		template<class W1> friend class example_weight;
	public:
		/** Defines void as a weight type. **/
		typedef void										weight_type;
		/** Defines std::string as a label type. **/
		typedef std::string									label_type;
	
	public:
		/** Constructs an example weight.
		 */
		example_weight()
		{
		}

	public:
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 */
		example_weight(const example_weight& e) : l_val(e.l_val)
		{
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 */
		example_weight(const example_weight<label_type*>& e)
		{
			if( e.l_val != 0 ) l_val = *e.l_val;
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 */
		template<class W1>
		example_weight(const example_weight<W1>& e)
		{
		}
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		example_weight& operator=(const example_weight& e)
		{
			l_val = e.l_val;
			return *this;
		}
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		example_weight& operator=(const example_weight<label_type*>& e)
		{
			if( e.l_val != 0 ) l_val = *e.l_val;
			return *this;
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		template<class W1>
		example_weight& operator=(const example_weight<W1>& e)
		{
			return *this;
		}
		
	public:
		/** Tests if an example has a unique label.
		 * 
		 * @return true if has unique label.
		 */
		bool haslabel()const
		{
			return l_val != "";
		}
		/** Gets the label assigned to this example.
		 *
		 * @return the string label assigned to this example.
		 */
		const label_type& label()const
		{
			return l_val;
		}
		/** Sets the string label.
		 * 
		 * @param v a string label.
		 */
		void label(const label_type& v)
		{
			l_val = v;
		}
		/** Gets the a weight.
		 *
		 * @param w a weight.
		 * @return the weight passed to this method.
		 */
		double weight(double w)const
		{
			return w;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param y an actual class index.
		 * @param p an predicted class index.
		 * @return the weight assigned to this example.
		 */
		template<class I>
		weight_type weightforclass(I y, I p)const
		{
			return y > 0 ? 1 : -1;
		}
		
	protected:
		/** Read a weight from an input stream.
		 * 
		 * @param in an input stream.
		 */
		template<class stream>
		void read(stream& in)
		{
			std::getline(in, l_val, '\t');
		}
		/** Write a weight to an output stream.
		 * 
		 * @param out an output stream.
		 */
		template<class stream>
		void write(stream& out)
		{
			out << l_val << "\t";
		}
		
	private:
		label_type l_val;
	};
	/** @brief Defines an example weight
	 * 
	 * This class template defines an example weight.
	 */
	template<>
	class example_weight<void>
	{
		template<class W1> friend class example_weight;
	public:
		/** Defines void as a weight type. **/
		typedef void										weight_type;
		/** Defines void as a label type. **/
		typedef void										label_type;
	public:
		/** Constructs an example weight.
		 */
		example_weight()
		{
		}
		
	public:
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 */
		example_weight(const example_weight& e)
		{
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 */
		template<class W1>
		example_weight(const example_weight<W1>& e)
		{
		}
		/** Constructs a copy of an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		example_weight& operator=(const example_weight& e)
		{
			return *this;
		}
		/** Constructs an example weight.
		 * 
		 * @param e an example weight.
		 * @return this object.
		 */
		template<class W1>
		example_weight& operator=(const example_weight<W1>& e)
		{
			return *this;
		}
		
	public:
		/** Tests if an example has a unique label.
		 * 
		 * @return true if has unique label.
		 */
		bool haslabel()const
		{
			return false;
		}
		/** Gets the a weight.
		 *
		 * @param w a weight.
		 * @return the weight passed to this method.
		 */
		double weight(double w)const
		{
			return w;
		}
		/** Gets the weight assigned to this example.
		 *
		 * @param y an actual class index.
		 * @param p an predicted class index.
		 * @return the weight assigned to this example.
		 */
		template<class I>
		weight_type weightforclass(I y, I p)const
		{
			return y > 0 ? 1 : -1;
		}
		
	protected:
		/** Read a weight from an input stream.
		 * 
		 * @param in an input stream.
		 */
		template<class stream>
		void read(stream& in)
		{
		}
		/** Write a weight to an output stream.
		 * 
		 * @param out an output stream.
		 * @todo write ALL LABELS
		 */
		template<class stream>
		void write(stream& out)
		{
		}
	};
};

#endif



