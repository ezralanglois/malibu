/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * dataset_parser.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_DATASET_PARSER_HPP
#define _EXEGETE_DATASET_PARSER_HPP
#include "charutil.h"
#include "parsers/standard_example_parser.hpp"
#include "parsers/csv_header_parser.hpp"
#include <cstdio>

/** @file dataset_parser.hpp
 * @brief Parse a dataset from the input stream
 * 
 * This file contains a class to parse a dataset from the input stream.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Parse a dataset from the input stream
	 * 
	 * This class defines dataset parser from the input stream.
	 */
	template<class dataset_type>
	class dataset_parser
	{
		typedef FILE* 									stream;
		typedef abstract_parser<dataset_type, stream> 	parser_type;
		typedef parser_type* 							parser_pointer;
		typedef std::vector< parser_pointer > 			parser_vector;
		typedef typename parser_vector::iterator 		iterator;
		typedef typename parser_vector::const_iterator 	const_iterator;
		typedef typename parser_vector::size_type 		size_type;
	public:
		/** Constructs a dataset parser.
		 */
		dataset_parser(const std::string& cm) : bag_idx(0), bag_beg(0), bag_end(0), verbose(1), commentstr(cm), poslabel("+"), neglabel("-")
		{
			hparsers.push_back( new csv_header_parser<dataset_type, stream>() );
			aparsers.push_back( new standard_example_parser<dataset_type, stream>() );
		}
		/** Destructs a dataset parser.
		 */
		~dataset_parser()
		{
			for(iterator beg=hparsers.begin(), end=hparsers.end();beg != end;++beg) delete ( *beg );
			for(iterator beg=aparsers.begin(), end=aparsers.end();beg != end;++beg) delete ( *beg );
		}
		
	public:
		/** Add arguments to an argument map.
		 * 
		 * @param map an argument map.
		 * @param v level of visibility.
		 */
		template<class U>
		void add_arguments(U& map, int v)
		{
			arginit(map, "Dataset");
			data.add_arguments(map, v);
			arginit(map, verbose,		"summary",	 "summary verbose output of dataset statistics>None:0;Short:1;Long:2;All:3");
			arginit(map, pos_vec,		"positive",  "list of positive classes");
			arginit(map, bag_idx,		"bagidx",	 "index of bag in labels, must be greater than 0");
			arginit(map, bag_beg,		"bagbeg",	 "substring begin of bag label");
			arginit(map, bag_end,		"bagend",	 "substring end of bag label");
			arginit(map, ens_vec,		"ensure",  	 "ensure the correct dataset statistics: # classes, # attributes, # examples, # bags", ArgumentMap::DEVELOPER);
			arginit(map, poslabel,		"poslabel",  "set the positive label (used with -positive)", ArgumentMap::ADVANCED);
			arginit(map, neglabel,		"neglabel",  "set the negative label (used with -positive)", ArgumentMap::ADVANCED);
			
		}
		/** Read a dataset from a file.
		 * 
		 * @param filename a file name.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		const char* read(const std::string& filename, dataset_type& dataset)
		{
			const char* msg;
			if( (msg=read_file(filename, dataset)) != 0 ) return msg;
			if( !pos_vec.empty() ) dataset.label_positive(pos_vec, poslabel, neglabel);
			if( bag_idx > 0 ) if( (msg=dataset.build_bags(bag_idx-1, bag_end, bag_end)) != 0 ) return msg;
			if( verbose )
			{
				std::cout << commentstr << "Filename: " << base_name(filename.c_str()) << std::endl;
				dataset.write_summary(std::cout, commentstr, verbose-1);
			}
			if( (msg=dataset.ensure(ens_vec)) != 0 ) return msg;
			return 0;
		}
		
	protected:
		/** Read a dataset from a file.
		 * 
		 * @param filename a file name.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		const char* read_file(const std::string& filename, dataset_type& dataset)
		{
			const char* msg = 0;
			FILE* fp;
			if( filename.empty() ) return ERRORMSG("Cannot open empty file name");
			fp = fopen(filename.c_str(), "r");
			if( fp != 0 )
			{
				msg=parse(fp, dataset);
				fclose(fp);
			}
			else msg = ERRORMSG("Cannot open file: " << filename);
			return msg;
		}
		/** Parse a dataset from an input stream.
		 * 
		 * @param fp a stream.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		template<class stream>
		const char* parse(stream fp, dataset_type& dataset)
		{
			const char* msg;
			size_type idx = 0;
			if( data.parse_header() )
			{
				iterator beg=hparsers.begin();
				iterator end=hparsers.end();
				if( hparsers.size() > 0 )
				{
					for(;beg != end;++beg) 
					{
						if( (msg=(*beg)->parse(fp, dataset, data)) != 0 ) return msg;
						if( (*beg)->is_valid() ) 
						{
							data = **beg;
							break;
						}
					}
				}
			}
			return aparsers[idx]->parse(fp, dataset, data, true);
		}
		
	private:
		parser_vector hparsers;
		parser_vector aparsers;
		parser_impl data;
		
	private:
		std::vector<unsigned int> ens_vec;
		std::vector<std::string> pos_vec;
		unsigned int bag_idx;
		unsigned int bag_beg;
		unsigned int bag_end;
		int verbose;
		
	private:
		std::string commentstr;
		std::string poslabel;
		std::string neglabel;
	};
};

#endif

