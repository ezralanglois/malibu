/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * csv_header_parser.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ABSTRACT_PARSER_HPP
#define _EXEGETE_ABSTRACT_PARSER_HPP
#include "charutil.h"

/** @file abstract_parser.hpp
 * @brief Defines a abstract dataset parser.
 * 
 * This file contains an abstract dataset parser.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section stdformarglist Standard Example Format
 * 	- header
 * 		- Description: parse header
 * 		- Type: Option(Auto:0;Yes:1;No:2)
 * 		- Viewability: Variable
 */

namespace exegete
{
	/** @brief Hold parser data
	 * 
	 * This class holds parser data.
	 */
	class parser_impl
	{
	public:
		/** Defines a character type. **/
		typedef char char_type;
		/** Defines a size type. **/
		typedef size_t size_type;
	public:
		/** Constructs an abstract parser.
		 */
		parser_impl() : attrTokenCh(','), sparTokenCh(':'), missingCh('?'), quoteCh('\"'), newlineCh('\n'), label_cnt(1), class_pos(0), header_typ(0), skipnumch(0)
		{
		}
		
	public:
		/** Add arguments to an argument map.
		 * 
		 * @param map an argument map.
		 * @param v level of visibility.
		 */
		template<class U>
		void add_arguments(U& map, int v)
		{
			arginit(map, header_typ, "header", "parse header>Auto:0;Yes:1;No:2", v);
			arginit(map, label_cnt,  "label",  "number of labels prefixing line", v);
			arginit(map, class_pos,  "class",  "class position", v);
		}
		
	public:
		/** Test if should parse header.
		 * 
		 * @return should parser header.
		 */
		bool parse_header()const
		{
			return header_typ < 2;
		}
		/** Test if should force header parser.
		 * 
		 * @return should force header parser.
		 */
		bool force_header()const
		{
			return header_typ == 1;
		}
		
	protected:
		/** Holds attribute separator. **/
		char_type attrTokenCh;
		/** Holds sparse attribute separator. **/
		char_type sparTokenCh;
		/** Holds missing character. **/
		char_type missingCh;
		/** Holds quote character. **/
		char_type quoteCh;
		/** Holds newline character. **/
		char_type newlineCh;
		
	protected:
		/** Holds number of labels. **/
		size_type label_cnt;
		/** Holds class position. **/
		size_type class_pos;
		/** Holds header type. **/
		char_type header_typ;
		
	protected:
		size_type skipnumch;
	};
	/** @brief Parse a dataset header from an input stream
	 * 
	 * This class defines a comma-separated-value (CSV) header parser.
	 */
	template<class dataset_type, class stream>
	class abstract_parser : public parser_impl
	{
		enum{ BUF_SIZE = 4096 };
	protected:
		/** Define a size type. **/
		typedef typename parser_impl::size_type size_type;
		/** Define a character type. **/
		typedef typename parser_impl::char_type char_type;
		/** Define a character buffer. **/
		typedef char_type buffer_type[BUF_SIZE];
		/** Define a constant character iterator. **/
		typedef const char_type* const_char_iterator;
		/** Define a character iterator. **/
		typedef char_type* char_iterator;
		
	public:
		/** Constructs an abstract parser.
		 */
		abstract_parser()
		{
		}
		/** Destructs an abstract parser.
		 */
		virtual ~abstract_parser()
		{
		}
		
	public:
		/** Parse a dataset from an input stream.
		 * 
		 * @param fp a stream.
		 * @param dataset a dataset.
		 * @param parsing parameters.
		 * @return an error message or NULL.
		 */
		const char* parse(stream fp, dataset_type& dataset, const parser_impl& data, bool last=false)
		{
			parser_impl::operator=(data);
			const char* msg = parse(fp, dataset);
			if( !last && msg == 0 ) restart(fp);
			return msg;
		}
		/** Test if stream matches parser format.
		 * 
		 * @param fp a stream.
		 * @return true if format matches.
		 */
		virtual bool is_valid()const
		{
			return true;
		}
		
	protected:
		/** Parse a dataset from an input stream.
		 * 
		 * @param fp a stream.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		virtual const char* parse(stream fp, dataset_type& dataset)=0;
		
	protected:
		/** Skip over token(s) in char buffer.
		 * 
		 * @param beg start of char buffer.
		 * @param fin end of char buffer.
		 * @return current position in buffer (or zero if newline encountered).
		 */
		const_char_iterator skip_token(const_char_iterator beg, const_char_iterator end)
		{
			while(beg != end && is_token(*beg) && *beg != parser_impl::newlineCh ) ++beg;
			if( beg != end && *beg == parser_impl::newlineCh ) return 0;
			return beg;
		}
		/** Read (or skip) a value in the char buffer.
		 * 
		 * @param beg start of char buffer.
		 * @param fin end of char buffer.
		 * @param it buffer for value.
		 * @return current position in buffer.
		 */
		const_char_iterator read_value(const_char_iterator beg, const_char_iterator end, char_iterator beg2, char_iterator& curr2)
		{
			char_iterator it = curr2;
			char_type ch=0;
			char findquote=0;
			if( beg2 != it ) findquote = (*beg2 == parser_impl::quoteCh);
			ASSERT(findquote==0);
			while(beg != end)
			{
				ch = *beg;
				if( !findquote && is_token(ch) ) break;
				if( ch == parser_impl::newlineCh ) break;
				*it = ch;
				++it;
				if( ch == parser_impl::quoteCh )
				{
					if( findquote ) 
					{
						findquote = 0;
						++beg;
						break;
					}
					else findquote = 1;
				}
				++beg;
			}
			if( beg != end && findquote ) return 0;
			*it = '\0';
			curr2 = it;
			return beg;
		}
		/** Tests if character is token.
		 * 
		 * @param ch character to test.
		 * @return true if character is token.
		 */
		bool is_token(char ch)const
		{
			return IS_SPACE(ch) || ch == parser_impl::attrTokenCh || ch == parser_impl::sparTokenCh;
		}
		
	protected:
		/** Go to start of stream.
		 * 
		 * @param fp a file pointer.
		 */
		inline void restart(FILE* fp)
		{
			 rewind(fp);
		}
		/** Read characters from a stream.
		 * 
		 * @param fp a file pointer.
		 * @param buf a character buffer.
		 */
		inline size_type read(FILE* fp, char_type* buf)
		{
			return read(fp, buf, BUF_SIZE);
		}
		/** Read characters from a stream.
		 * 
		 * @param fp a file pointer.
		 * @param buf a character buffer.
		 */
		inline size_type read(FILE* fp, char_type* buf, size_type n)
		{
			return fread(buf, sizeof(char_type), n, fp);
		}
	};
};

#endif


