/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * csv_header_parser.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_CSV_HEADER_PARSER_HPP
#define _EXEGETE_CSV_HEADER_PARSER_HPP
#include "abstract_parser.hpp"
#include <vector>

/** @file csv_header_parser.hpp
 * @brief Parse a dataset header from an input stream
 * 
 * This file contains the comma-separated-value (CSV) header parser class.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Parse a dataset header from an input stream
	 * 
	 * This class defines a comma-separated-value (CSV) header parser.
	 * 
	 * @todo determine class position, labels from header
	 */
	template<class dataset_type, class stream>
	class csv_header_parser : public abstract_parser<dataset_type, stream>
	{
		typedef abstract_parser<dataset_type, stream> parent_type;
		typedef typename parent_type::size_type size_type;
		typedef typename parent_type::char_type char_type;
		typedef typename parent_type::buffer_type buffer_type;
		typedef typename parent_type::const_char_iterator const_char_iterator;
		typedef typename parent_type::char_iterator char_iterator;
		typedef typename dataset_type::feature_type feature_type;
	public:
		/** Construct a csv header parser.
		 */
		csv_header_parser() : valid(false)
		{
		}
		
	public:
		/** Parse a dataset from an input stream.
		 * 
		 * @param fp a stream.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		const char* parse(stream fp, dataset_type& dataset)
		{
			typedef std::vector< std::string > header_vector;
			valid = false;
			const_char_iterator beg;
			const_char_iterator end;
			buffer_type buf;
			buffer_type val;
			size_type len;
			size_type skipnum=0;
			char_iterator cval;
			feature_type fval;
			header_vector headers;
			parent_type::skipnumch = 0;

			while( (len=parent_type::read(fp, buf)) > 0 )
			{
				end=buf+len;
				beg=buf;
				while( beg != end )
				{
					if( *beg != parent_type::newlineCh )
					{
						while(1)
						{
							beg = parent_type::skip_token(beg, end); // end to determine which?
							if( beg != end ) break;
							skipnum += len;
							len=parent_type::read(fp, buf);
							if( len == 0 ) break;
							end=buf+len;
							beg=buf;
						}
						if( beg == 0 ) return ERRORMSG("Expected value not newline or EOF for header");
					}
					if( beg != end && *beg != parent_type::newlineCh )
					{
						cval = val;
						while(1)
						{
							beg = parent_type::read_value(beg, end, val, cval);
							if( beg != end ) break;
							skipnum += len;
							len=parent_type::read(fp, buf);
							if( len == 0 ) break;
							end=buf+len;
							beg=buf;
						}
						if( val == cval ) return ERRORMSG("Empty value for header");
						if( beg == 0 ) return ERRORMSG("Expected double quote found newline or EOF for header");
						if( beg != end && *beg == parent_type::sparTokenCh ) return 0;
						if( stringToValue(val, fval) && !parent_type::force_header() ) return 0;
						headers.push_back(val);
					}
					if( beg != end && *beg == parent_type::newlineCh ) break;
				}
				if( len == 0 || (beg != end && *beg == parent_type::newlineCh) ) break;
			}
			skipnum += (std::distance((const_char_iterator)buf, beg)+1);
			if( dataset.attribute_count() > 0 )
			{
				typedef typename header_vector::iterator iterator;
				typename dataset_type::header_iterator hbeg = dataset.header_begin();
				typename dataset_type::header_iterator hend = dataset.header_end();
				if( size_type(std::distance(hbeg, hend)) != headers.size() ) 
					return ERRORMSG("Number of headers does not match dataset - " << headers.size() << " != " << std::distance(hbeg, hend) );
				for(iterator beg = headers.begin(), end = headers.end(); beg != end;++beg, ++hbeg)
					if( *beg != hbeg->name() )
						return ERRORMSG("Header does not match dataset - " << *beg << "(" << std::distance(dataset.header_begin(), hbeg) << ") != " << hbeg->name() << "(" << std::distance(headers.begin(), beg) << ")" );
			}
			else 
			{
				//dataset.assign_header(headers.begin(), headers.end(), parent_type::class_pos, parent_type::label_cnt);
				size_type cl = parent_type::class_pos;
				if( cl > (headers.size()-parent_type::label_cnt) ) cl = headers.size()-parent_type::label_cnt-1;
				dataset.assign_header(headers.begin(), headers.begin()+parent_type::label_cnt, headers.begin()+cl+parent_type::label_cnt, headers.end());
			}
			valid = true;
			parent_type::skipnumch = skipnum;
			return 0;
		}
		/** Test if stream matches parser format.
		 * 
		 * @param fp a stream.
		 * @return true if format matches.
		 */
		bool is_valid()const
		{
			return valid;
		}
		
	private:
		bool valid;
	};
};

#endif

