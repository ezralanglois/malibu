/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * standard_example_parser.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_STANDARD_EXAMPLE_PARSER_HPP
#define _EXEGETE_STANDARD_EXAMPLE_PARSER_HPP
#include "abstract_parser.hpp"

/** @file standard_example_parser.hpp
 * @brief Read and write a dataset to a stream.
 * 
 * This file contains the standard example parser class, which defines 
 * a parser for standard examples.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Read and write a dataset to a stream
	 * 
	 * This class defines the format for reading and writing a set of 
	 * examples in a specific format.
	 *
	 * @todo control number of labels to print
	 * @todo fix bug allows nominal and reall in same column!
	 */
	template<class dataset_type, class stream>
	class standard_example_parser : public abstract_parser<dataset_type, stream>
	{
		typedef abstract_parser<dataset_type, stream> parent_type;
		typedef typename parent_type::size_type size_type;
		typedef typename parent_type::char_type char_type;
		typedef typename parent_type::buffer_type buffer_type;
		typedef typename parent_type::const_char_iterator const_char_iterator;
		typedef typename parent_type::char_iterator char_iterator;
		typedef std::vector< std::string > string_vector;
	public:
		/** Constructs a standard example parser.
		 */
		standard_example_parser() : debug_print(1)
		{
		}
		/** Destruct a standard example parser.
		 */
		~standard_example_parser()
		{
		}
		
	public:
		/** Parse a dataset from an input stream.
		 * 
		 * @param fp a stream.
		 * @param dataset a dataset.
		 * @return an error message or NULL.
		 */
		const char* parse(stream fp, dataset_type& dataset)
		{
			const char* msg;
			buffer_type buf;
			buffer_type val;

			//reallocate memory based on learner
			dataset.clear();
			if( (msg=parse_standard(fp, buf, dataset, val)) != 0 ) return msg;
			parent_type::restart(fp);
			if( (msg=parse_standard(fp, buf, dataset, val)) != 0 ) return msg;
			dataset.finalize();
			return 0;
		}
		
	protected:
		/** Parse a dataset from an input stream. If the dataset is empty, count the number
		 * of attributes and examples.
		 * 
		 * @param fp an input stream.
		 * @param buf a buffer.
		 * @param dataset a dataset.
		 * @param val a value buffer.
		 * @return an error message or NULL.
		 */
		const char* parse_standard(stream fp, buffer_type buf, dataset_type& dataset, char_iterator val)
		{
			typedef typename dataset_type::iterator example_iterator;
			typedef typename dataset_type::feature_pointer attribute_pointer;
			typedef typename dataset_type::label_type* label_pointer;
			typedef typename dataset_type::index_type index_type;
			typedef typename dataset_type::feature_type feature_type;
			typedef typename dataset_type::class_type class_type;
			typedef typename dataset_type::type_util type_util;
			attribute_pointer px = dataset.feature_begin(), pbeg=px;
			example_iterator e_ptr = dataset.begin();
			label_pointer l_ptr = dataset.label_start();
			bool count = dataset.empty();
			bool sparse = type_util::IS_SPARSE;
			bool add = (dataset.class_count()==0);
			bool found_sparse = false;
			const_char_iterator beg;
			const_char_iterator end;
			string_vector nomvec;
			char_iterator cval;
			size_type len;
						
			size_type e_cnt=0;
			size_type l_cnt=0;
			size_type a_max=0;
			size_type z_cnt=0;
			size_type a_lim=TypeUtil<size_type>::max();
			size_type skip=parent_type::skipnumch;
			char y_cnt=0;
			
			index_type idx;
			feature_type fval;
			class_type y;
			
			if( !count ) a_lim = dataset.attribute_count();
			if( count ) px = pbeg = 0;
			while( (len=parent_type::read(fp, buf)) > 0 )
			{
				while( len < skip && (len=parent_type::read(fp, buf)) > 0 )
					skip -= len;
				end=buf+len;
				beg=buf+skip;
				skip=0;
				while( beg != end )
				{
					if( *beg != parent_type::newlineCh )
					{
						while(1)
						{
							ASSERT(beg != end);
							beg = parent_type::skip_token(beg, end); // end to determine which?
							if( beg != end ) break;
							len=parent_type::read(fp, buf);
							if( len == 0 ) break;
							end=buf+len;
							beg=buf;
						}
						if( beg == 0 ) return ERRORMSG("Expected value not newline or EOF for example: " << e_cnt);
					}
					if( beg != end && *beg != parent_type::newlineCh )
					{
						cval = val;
						while(1)
						{
							ASSERT(beg != end);
							beg = parent_type::read_value(beg, end, val, cval);
							if( beg != end ) break;
							if( (len=parent_type::read(fp, buf)) == 0 ) break;
							end=buf+len;
							beg=buf;
							if( parent_type::is_token(*beg) ) break;
						}
						if( val == cval ) return ERRORMSG("Empty value for example: " << e_cnt);
						if( beg == 0 ) return ERRORMSG("Expected double quote found newline or EOF for example: " << e_cnt);
						if( beg != end && *beg == parent_type::sparTokenCh )
						{
							if(!stringToValue(val, idx)) return ERRORMSG("Expected index, found: " << val);// make faster, cstdlib atof, atoi, atol, atol, atoll
							if( idx < 1 ) return ERRORMSG("Index must be greater than 0 for example: " << e_cnt);
							idx--;
							++beg;
							if( beg == end )
							{
								len=parent_type::read(fp, buf);
								if( len == 0 ) return ERRORMSG("Unexpected end of file for example: " << e_cnt);
								end=buf+len;
								beg=buf;
							}
							cval = val;
							while(1)
							{
								ASSERT(beg != end);
								beg = parent_type::read_value(beg, end, val, cval);
								if( beg != end ) break;
								len=parent_type::read(fp, buf);
								if( len == 0 ) return ERRORMSG("Unexpected end of file for example: " << e_cnt);
								end=buf+len;
								beg=buf;
								if( parent_type::is_token(*beg) ) break;
							}
							if( val == cval ) return ERRORMSG("Empty value for example: " << e_cnt);
							if( beg == 0 ) return ERRORMSG("Expected double quote found newline or EOF for example: " << e_cnt);
							if( !found_sparse ) found_sparse = true;
						}
						else if( beg == end && found_sparse )
						{
							ASSERT(false);
							if( !count )
							{
								if( !y_cnt )
								{
									y = dataset.insert_class(val, parent_type::missingCh, add);
									y_cnt=1;
								}
								else return ERRORMSG("Expected sparse index, for (example, attribute)=(" << e_cnt << ", " << std::distance(pbeg,px) << ")");
							}
							idx = index_type(std::distance(pbeg,px));
						}
						else idx = index_type(std::distance(pbeg,px))+z_cnt;
						if( l_cnt >= parent_type::label_cnt && ( y_cnt > 0 || size_type(idx) != parent_type::class_pos ) )
						{
							if( val[0] == parent_type::missingCh ) fval = type_util::missing();
							else if( !stringToValue(val, fval) ) // make faster
							{
								if( count ) 
								{
									if( !found_sparse && sparse )
									{
										if( size_type(idx) >= nomvec.size() ) nomvec.resize((idx+1)*2);
										if( nomvec[idx] == "" ) nomvec[idx] = val;
										fval = ( nomvec[idx] != val ) ? 1.0 : 0.0;
									}
									else fval = 1.0;
								}
								else 
								{
									if( size_type(idx) < a_lim )
									{
										ASSERTMSG(size_type(idx) < dataset.attribute_count(), idx << " < " << dataset.attribute_count() << " " << l_cnt << " " << int(y_cnt) << " " << val );
										fval = dataset.insert_nominal(val, idx, found_sparse, add);
									}
								}
							}
							if( !sparse || fval != 0.0 )
							{
								if( size_type(idx) < a_lim )
								{
									if(!count) px=dataset.append_attribute(pbeg, px, fval, idx, e_cnt);
									ASSERT(size_type(idx)>=z_cnt);
									dataset.increment_attribute(idx);
									px++;
								}
								else if( !found_sparse && y_cnt == 1 ) return ERRORMSG("Attribute exceeds limit: " << idx << " < " << a_lim+1);
							}
							else 
							{
								ASSERT(sparse || found_sparse);
								z_cnt++;
							}
							if(size_type(idx) < a_lim && size_type(idx) > a_max ) a_max = idx;
						}
						else if( l_cnt < parent_type::label_cnt )
						{
							if(!count) 
							{
								ASSERTMSG(l_ptr < dataset.label_start()+(dataset.size()*parent_type::label_cnt), " val: " << val << " - " << dataset.size() << " " << std::distance(dataset.label_start(), l_ptr) );
								*l_ptr = val;
								++l_ptr;
							}
							l_cnt++;
						}
						else 
						{
							if( !count ) y = dataset.insert_class(val, parent_type::missingCh, add);
							y_cnt=1;
						}
					}
					if( len==0 || (beg != end && *beg == parent_type::newlineCh) )
					{
						if( !y_cnt && !found_sparse )
						{
							if( !count ) y = dataset.insert_class(val, parent_type::missingCh, add);
							y_cnt=1;
							if( count )
							{
								if( px > pbeg && (!sparse || fval != 0) )
								{
									a_max--;
									px--;
								}
								if(sparse && fval==0) z_cnt--;
							}
							idx--;
						}
						if( !count )
						{
							if( !sparse && found_sparse && px < pbeg+a_lim ) px = pbeg+a_lim;
							e_ptr->set(pbeg, y);
							e_ptr->label(l_ptr-parent_type::label_cnt);
							//save sparse? pbeg -> px
							px = dataset.mark_last_attribute(px);
						}
						else px += dataset_type::mark_count();
						if( !found_sparse && dataset.attribute_count() > 0 && dataset.attribute_count() != size_type(idx+1) )
							return ERRORMSG("Attribute count mismatch: " << dataset.attribute_count() << " != " << (idx+1) << " (" << (a_max+1) << ") for example " << e_cnt << " (" << z_cnt << " + " << std::distance(pbeg, px) << " " << idx << " len: " << len << ")");
						
						e_cnt++;
						e_ptr++;
						pbeg=px;
						l_cnt=0;
						y_cnt=0;
						z_cnt=0;
						y = 0;
					}
					++beg;
					while(beg!=end && *beg == parent_type::newlineCh) ++beg;
				}
				if( len == 0 ) break;
			}
			ASSERT(beg==end);
			if( count ) 
			{
				if( !found_sparse && dataset.attribute_count() > 0 && dataset.attribute_count() != (a_max+1) )
				{
					return ERRORMSG("Attribute count does not match previous dataset or header: " << dataset.attribute_count() << " != " << (a_max+1));
				}
				DBG_PRINT(debug_print > 0, e_cnt << ", " << std::distance((attribute_pointer)0, px) << ", " << (a_max+1) << ", " << parent_type::label_cnt);
				if( e_cnt < 1 ) return ERRORMSG("Found 0 examples.");
				if( a_max < 1 ) return ERRORMSG("Found 0 attribute columns.");
				if( std::distance((attribute_pointer)0, px) < 1 ) return ERRORMSG("Found 0 attributes.");
				dataset.resize(e_cnt, std::distance((attribute_pointer)0, px), a_max+1, parent_type::label_cnt);
			}
			else
			{
				ASSERTMSG(dataset.feature_end() == px, std::distance(dataset.feature_begin(), px) << " != " << dataset.attribute_total() << " " << e_cnt << " " << a_lim);
			}
			return 0;
		}
		
	private:
		int debug_print;
	};
};

#endif

