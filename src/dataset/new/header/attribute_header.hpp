/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_header.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_ATTRIBUTE_HEADER_HPP
#define _EXEGETE_ATTRIBUTE_HEADER_HPP
#include "../attribute/attribute_utility.hpp"
#include <vector>
#include <string>
#include <cmath>

/** @file attribute_header.hpp
 * 
 * @brief Defines an attribute header
 * 
 * This file contains an attribute header.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an attribute header
	 * 
	 * This class template defines an attribute header.
	 */
	template<class A>
	class attribute_header : public std::vector< std::string >
	{
		typedef std::vector< std::string > parent_type;
	public:
		/** Flags types for each attribute
		 *  - numeric: any real value
		 *  - discreet: any integer value
		 *  - binary: values 0/1
		 *  - nominal: string values (unordered)
		 **/
		enum Type
		{
			NUMERIC,  /** Flags a numeric attribute type. **/
			DISCREET, /** Flags a discreet attribute type. **/
			BINARY,   /** Flags a binary attribute type. **/
			NOMINAL   /** Flags a nominal attribute type. **/
		};
		/** Redefines an enumerated Type as a data type. **/
		typedef enum Type								data_type;
		/** Defines a type utility. **/
		typedef attribute_utility<A>					type_util;
		/** Defines a value type. **/
		typedef typename type_util::feature_type		value_type;
		/** Defines a limit type. **/
		typedef typename type_util::limit_type			limit_type;
		/** Defines a index type. **/
		typedef typename type_util::size_type			size_type;
		/** Defines a label type. **/
		typedef std::string 							label_type;
		
	public:
		attribute_header(std::string nm="") : nameStr(nm), missingInt(0), 
											  zeroInt(0), typeInt(BINARY), 
											  minAttrFlt(limit_type::max()),
											  maxAttrFlt(-limit_type::max())
		{
		}
		
	public:
		/** Assign name to header.
		 * 
		 * @param l a name.
		 * @return this object.
		 */
		attribute_header& operator=(const label_type& l)
		{
			nameStr = l;
			return *this;
		}
		
	public:
		/** Reset the counts.
		 */
		void reset()
		{
			missingInt = 0;
			zeroInt = 0;
			typeInt = BINARY;
			minAttrFlt = limit_type::max();
			maxAttrFlt = -limit_type::max();
		}
		/** Clears an attribute column description.
		 */
		void clear()
		{
			nameStr = "";
			reset();
			parent_type::clear();
		}
		
	public:
		/** Sets the name of the attribute.
		 *
		 * @param val a string holding the name.
		 */
		void name(const label_type& val)
		{
			nameStr = val;
		}
		/** Assesses the statistics of a particular attribute column. Every attribute is assumed to 
		 * be binary or nominal (if the string names are set). This function is run on every value 
		 * in the column to determine the column type. It also records the number of missing 
		 * values, zeros, minimum and maximum values.
		 *
		 * @param val the value to count.
		 */
		void count(value_type val)
		{
			if( val != type_util::missing() )
			{
				if( val < minAttrFlt ) minAttrFlt = val;
				if( val > maxAttrFlt ) maxAttrFlt = val;
				if( !parent_type::empty() ) 
				{
					typeInt=NOMINAL;
				}
				else if( typeInt == NUMERIC );
				else if( value_type(std::floor(double(val))) != val ) typeInt = NUMERIC;
				else if(typeInt == DISCREET);
				else if( val != 1 && val != 0 ) typeInt = DISCREET;
				if( val != value_type(0) ) zeroInt++;
			}
			else missingInt++;
		}
		/** Finalize statistics.
		 * 
		 * @param e example count.
		 */
		void finalize(size_type e)
		{
			zeroInt = e - zeroInt - missingInt;
		}
		
	public:
		/** Gets the number of missing values in the attribute column.
		 *
		 * @return number of missing values in attribute column.
		 */
		size_type missing()const
		{
			return missingInt;
		}
		/** Gets the number of zeros in the attribute column.
		 *
		 * @return number of zeros in attribute column.
		 */
		size_type zeros()const
		{
			return zeroInt;
		}
		/** Gets the type of values found in the attribute column.
		 *
		 * @return type of values found in the attribute column.
		 */
		data_type type()const
		{
			return typeInt;
		}
		/** Gets the name of the attribute.
		 *
		 * @return the string name.
		 */
		const label_type& name()const
		{
			return nameStr;
		}
		/** Gets the minimum value in the attribute column.
		 *
		 * @return minimum value in attribute column.
		 */
		value_type min()const
		{
			return minAttrFlt;
		}
		/** Gets the maximum value in the attribute column.
		 *
		 * @return maximum value in attribute column.
		 */
		value_type max()const
		{
			return maxAttrFlt;
		}
		/** Gets the value range in the attribute column.
		 *
		 * @return value range in attribute column.
		 */
		value_type range()const
		{
			return maxAttrFlt-minAttrFlt;
		}
		/** Gets a name for the current type.
		 *
		 * @return a string name for type.
		 */
		const char* toStringType()const
		{
			if( typeInt == NUMERIC )		return "Numeric";
			else if( typeInt == DISCREET )	return "Discreet";
			else if( typeInt == NOMINAL )	return "Nominal";
			else							return "Binary";
		}
		
	public:
		/** Tests if the attribute is nominal.
		 *
		 * @return true if attribute type is nominal.
		 */
		bool isnominal()const
		{
			return typeInt == NOMINAL;
		}
		/** Tests if the attribute is binary.
		 *
		 * @return true if attribute type is binary.
		 */
		bool isbinary()const
		{
			return typeInt == BINARY || parent_type::size() == 2;
		}
		/** Tests if the attribute is discreet.
		 *
		 * @return true if attribute type is discreet.
		 */
		bool isdiscreet()const
		{
			return typeInt == DISCREET;
		}
		/** Tests if the attribute is numeric.
		 *
		 * @return true if attribute type is numeric.
		 */
		bool isnumeric()const
		{
			return typeInt == NUMERIC;
		}
		/** Tests if the attribute is unsorted (binary or nominal).
		 *
		 * @return true if attribute type is unsorted.
		 */
		bool isunsorted()const
		{
			return typeInt == BINARY || typeInt == NOMINAL;
		}
		/** Tests if attribute column is normalized.
		 * 
		 * @return true if all values fall between 1 and -1.
		 */
		bool isunnormalized()const
		{
			return !isbinary() && ( max() > 1.0 || min() < -1.0 );
		}
		/** Write a summary of the attribute header.
		 * 
		 * @param out an output stream.
		 * @param pfx a prefix.
		 */
		void write_summary(std::ostream& out, const std::string& pfx, size_type idx)const
		{
			typedef typename parent_type::const_iterator const_iterator;
			out << pfx;
			if( nameStr == "" ) out << idx;
			else out << nameStr;
			out << "\t" << toStringType() << "\tmissing: " << missingInt;
			if( typeInt == NOMINAL )
			{
				out << " values:";
				for(const_iterator beg = parent_type::begin(), end=parent_type::end();beg != end;++beg)
					out << " " << *beg;
			}
			else if( typeInt != BINARY )
			{
				out << ": " << minAttrFlt << " - " << maxAttrFlt;
			}
			out << "\n";
		}
		
	public:
		/** Encodes a nominal label into an internal value.
		 *
		 * @param str a string nominal label.
		 * @param miss a missing value flag.
		 * @param spr is sparse addition.
		 * @param add should add new nominal label.
		 * @return internal representation of label.
		 */
		template<class U>
		U valueOf(const std::string& str, U miss, bool spr, bool add)
		{
			size_type n = indexof(str);
			if( n == parent_type::size() )
			{
				if( add )
				{
					if( spr && parent_type::empty() ) parent_type::push_back("0");
					parent_type::push_back(str);
					return U(parent_type::size()-1);
				}
				return miss;
			}
			return U(n);
		}
		/** Gets the index of the label.
		 *
		 * @param str a string label.
		 * @return index of label.
		 */
		size_type indexof(const std::string& str)const
		{
			return size_type(std::distance(parent_type::begin(), std::find(parent_type::begin(), parent_type::end(), str)));
		}
		
	private:
		label_type nameStr;
		size_type missingInt;
		size_type zeroInt;
		data_type typeInt;
		value_type minAttrFlt;
		value_type maxAttrFlt;
	};
	
};


#endif

