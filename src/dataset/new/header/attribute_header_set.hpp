/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_header_set.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_ATTRIBUTE_HEADER_SET_HPP
#define _EXEGETE_ATTRIBUTE_HEADER_SET_HPP
#include "attribute_header.hpp"

/** @file attribute_header_set.hpp
 * 
 * @brief Defines an attribute header set
 * 
 * This file contains an attribute header set.
 *
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an attribute header set
	 * 
	 * This class template defines an attribute header set.
	 */
	template<class A, class Y>
	class attribute_header_base
	{
	public:
		/** Defines a value type. */
		typedef attribute_header<A> 		header_type;
		/** Defines a header pointer. */
		typedef attribute_header<A>* 		header_pointer;
		/** Defines a constant header pointer. */
		typedef const attribute_header<A>* 	const_header_pointer;
		/** Defines a reference. */
		typedef attribute_header<A>& 		header_reference;
		/** Defines a constant reference. */
		typedef const attribute_header<A>& 	const_header_reference;
		/** Defines a iterator. */
		typedef attribute_header<A>* 		header_iterator;
		/** Defines a constant iterator. */
		typedef const attribute_header<A>* 	const_header_iterator;
		
	public:
		/** Defines a label type. **/
		typedef typename header_type::label_type 	label_type;
		/** Defines a label header_iterator. */
		typedef label_type* 						label_iterator;
		/** Defines a constant label header_iterator. */
		typedef const label_type* 					const_label_iterator;
	public:
		/** Defines a size type. **/
		typedef typename header_type::size_type 	size_type;
		/** Defines a class type. **/
		typedef Y 									class_type;
		/** Defines a type utility. **/
		typedef typename header_type::type_util 	type_util;
		
	public:
		/** Constructs an attribute header set.
		 */
		attribute_header_base() : h_ptr(0), a_cnt(0), l_cnt(0), y_cnt(0)
		{
		}
		
	public:
		/** Get the attribute header at specified index.
		 * 
		 * @param n specified index.
		 * @return attribute header.
		 */
		header_reference attribute_at(size_type n)
		{
			return attribute_begin()[n];
		}
		/** Get the attribute header at specified index.
		 * 
		 * @param n specified index.
		 * @return attribute header.
		 */
		const_header_reference attribute_at(size_type n)const
		{
			return attribute_begin()[n];
		}
		/** Gets an iterator to the start of a collection of headers.
		 *
		 * @return an iterator to the start of a header collection.
		 */
		header_iterator attribute_begin()
		{
			return h_ptr+1+l_cnt;
		}
		/** Gets an iterator to the end of a collection of headers.
		 *
		 * @return an iterator to the end of a header collection.
		 */
		header_iterator attribute_end()
		{
			return attribute_begin()+a_cnt;
		}
		/** Gets an iterator to the start of a collection of headers.
		 *
		 * @return an iterator to the start of a header collection.
		 */
		const_header_iterator attribute_begin()const
		{
			return h_ptr+1+l_cnt;
		}
		/** Gets an iterator to the end of a collection of headers.
		 *
		 * @return an iterator to the end of a header collection.
		 */
		const_header_iterator attribute_end()const
		{
			return attribute_begin()+a_cnt;
		}
		
	public:
		/** Gets an iterator to the start of a collection of headers.
		 *
		 * @return an iterator to the start of a header collection.
		 */
		header_iterator header_begin()
		{
			return h_ptr;
		}
		/** Gets an iterator to the end of a collection of headers.
		 *
		 * @return an iterator to the end of a header collection.
		 */
		header_iterator header_end()
		{
			return h_ptr+header_count();
		}
		/** Gets an iterator to the start of a collection of headers.
		 *
		 * @return an iterator to the start of a header collection.
		 */
		const_header_iterator header_begin()const
		{
			return h_ptr;
		}
		/** Gets an iterator to the end of a collection of headers.
		 *
		 * @return an iterator to the end of a header collection.
		 */
		const_header_iterator header_end()const
		{
			return h_ptr+header_count();
		}
		
	public:
		/** Gets an iterator to the start of a collection of class labels.
		 *
		 * @return an iterator to the start of a class labels collection.
		 */
		label_iterator class_begin()
		{
			return &(*class_header().begin());
		}
		/** Gets an iterator to the end of a collection of class labels.
		 *
		 * @return an iterator to the end of a class label collection.
		 */
		label_iterator class_end()
		{
			return &(*class_header().end());
		}
		/** Gets an iterator to the start of a collection of class labels.
		 *
		 * @return an iterator to the start of a class label collection.
		 */
		const_label_iterator class_begin()const
		{
			return &(*class_header().begin());
		}
		/** Gets an iterator to the end of a collection of class labels.
		 *
		 * @return an iterator to the end of a class label collection.
		 */
		const_label_iterator class_end()const
		{
			return &(*class_header().end());
		}
		
		
	public:
		/** Sets the number of attribute columns.
		 * 
		 * @param a number of attributes.
		 */
		void attribute_count(size_type a)
		{
			a_cnt = a;
		}
		/** Sets the number of classes.
		 * 
		 * @param n number of classes.
		 */
		void class_count(size_type n)
		{
			y_cnt = n;
		}
		/** Gets the number of labels.
		 *
		 * @return the number of labels.
		 */
		size_type label_count()const
		{
			return l_cnt;
		}
		/** Gets the number of attributes.
		 *
		 * @return the number of attributes.
		 */
		size_type attribute_count()const
		{
			return a_cnt;
		}
		/** Gets the number of columns.
		 *
		 * @return the number of columns.
		 */
		size_type header_count()const
		{
			return a_cnt+l_cnt+1;
		}
		/** Gets the number of classes.
		 *
		 * @return the number of classes.
		 */
		size_type class_count()const
		{
			return y_cnt;
		}
		
	public:
		/** Gets the number of missing values for every attribute column.
		 *
		 * @return the number of missing values.
		 */
		size_type missing()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg) cnt += beg->missing();
			return cnt;
		}
		/** Gets the number of zero values for every attribute column.
		 *
		 * @return the number of zero values.
		 */
		size_type zeros()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg) cnt += beg->zeros();
			return cnt;
		}
		/** Count the number of columns with missing attributes.
		 * 
		 * @return missing column count.
		 */
		size_type missing_count()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg) 
			{
				if( beg->missing() > 0 ) cnt++;
			}
			return cnt;
		}
		/** Count the number of columns with binary attributes.
		 * 
		 * @return binary column count.
		 */
		size_type binary_count()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg) 
			{
				if( beg->isbinary() ) cnt++;
			}
			return cnt;
		}
		/** Count the number of columns with discreet attributes.
		 * 
		 * @return discreet column count.
		 */
		size_type discreet_count()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg) 
			{
				if( beg->isdiscreet() ) cnt++;
			}
			return cnt;
		}
		/** Count the number of columns with discreet attributes.
		 * 
		 * @return discreet column count.
		 */
		size_type numeric_count()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg) 
			{
				if( beg->isnumeric() ) cnt++;
			}
			return cnt;
		}
		/** Count the number of columns with discreet attributes.
		 * 
		 * @return discreet column count.
		 */
		size_type nominal_count()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg) 
			{
				if( beg->isnominal() ) cnt++;
			}
			return cnt;
		}
		/** Count the number of columns with normalized attributes.
		 * 
		 * @return normalized column count.
		 */
		size_type normalized_count()const
		{
			size_type cnt=0;
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg) 
			{
				if( !beg->isunnormalized() ) cnt++;
			}
			return cnt;
		}
		
	public:
		/** Tests if every column is normalized.
		 * 
		 * @return true if every column is normalized.
		 */
		bool isnormalized()const
		{
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg)
				if( !beg->isunnormalized() ) return false;
			return true;
		}
		/** Tests if any column has nominal attributes.
		 * 
		 * @return true if a column has nominal attributes.
		 */
		bool hasnominal()const
		{
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg)
				if( beg->isnominal() ) return true;
			return false;
		}
		/** Tests if any column has numeric or discreet attributes.
		 * 
		 * @return true if a column has numeric or discreet attributes.
		 */
		bool hasnumeric()const
		{
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg)
				if( beg->isnumeric() || beg->isdiscreet() ) return true;
			return false;
		}
		/** Tests if every column is binary attributes.
		 * 
		 * @return false if column is not binary.
		 */
		bool onlybinary()const
		{
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg)
				if( !beg->isbinary() ) return false;
			return true;
		}
		/** Tests if every column is nominal attributes.
		 * 
		 * @return false if column is not nominal.
		 */
		bool onlynominal()const
		{
			for(const_header_iterator beg = attribute_begin(), end = attribute_end();beg != end;++beg)
				if( !beg->isnominal() ) return false;
			return true;
		}
		
	protected:
		/** Get the class header.
		 * 
		 * @return header value.
		 */
		header_reference class_header()
		{
			return h_ptr[l_cnt];
		}
		/** Get the class header.
		 * 
		 * @return header value.
		 */
		const_header_reference class_header()const
		{
			return h_ptr[l_cnt];
		}
		
	protected:
		/** Holds a pointer to attribute headers. **/
		header_pointer h_ptr;
		/** Holds number of attributes. **/
		size_type a_cnt;
		/** Holds number of labels. **/
		size_type l_cnt;
		/** Holds number of classes. **/
		size_type y_cnt;
	};
	

	/** @brief Defines an attribute header set
	 * 
	 * This class template defines an attribute header set.
	 */
	template<class A, class Y>
	class attribute_header_set : public attribute_header_base<A,Y>
	{
		typedef attribute_header_base<A,Y> base_type;
	public:
		/** Defines a header iterator. **/
		typedef typename base_type::header_iterator header_iterator;
		/** Defines a size type. **/
		typedef typename base_type::size_type size_type;
		/** Defines a class type. **/
		typedef typename base_type::class_type class_type;
		/** Defines a label type. **/
		typedef typename base_type::label_type label_type;
		/** Defines a type utility. **/
		typedef typename base_type::type_util type_util;
		/** Defines a index type. **/
		typedef typename type_util::index_type index_type;
		/** Defines a feature type. **/
		typedef typename type_util::feature_type feature_type;
		/** Defines a constant attribute pointer. **/
		typedef typename type_util::const_pointer const_attribute_pointer;
		/** Defines a header pointer. **/
		typedef typename base_type::header_pointer header_pointer;
		/** Defines a header type. **/
		typedef typename base_type::header_type header_type;
		
	public:
		/** Construct a header set.
		 */
		attribute_header_set() : l_ptr(0)
		{
		}
		/** Destruct a header set.
		 */
		~attribute_header_set()
		{
			::erase(base_type::h_ptr);
			::erase(l_ptr);
		}
		
	public:
		/** Construct a deep copy of an attribute header set.
		 * 
		 * @param set source header set.
		 */
		attribute_header_set(const attribute_header_set& set) : l_ptr(0)
		{
			deep_copy_impl(set);
		}
		/** Assign a deep copy of an attribute header set.
		 * 
		 * @param set source header set.
		 */
		attribute_header_set& operator=(const attribute_header_set& set)
		{
			clear();
			deep_copy_impl(set);
			return *this;
		}
		/** Create a shallow copy of an attribute header set.
		 * 
		 * @param set source header set.
		 */
		void shallow_copy(attribute_header_set& set)
		{
			clear();
			base_type::h_ptr = set.h_ptr; set.h_ptr = 0;
			base_type::a_cnt = set.a_cnt; set.a_cnt = 0;
			base_type::l_cnt = set.l_cnt; set.l_cnt = 0;
			base_type::y_cnt = set.y_cnt; set.y_cnt = 0;
		}
		
	protected:
		/** Create a deep copy of an attribute header set.
		 * 
		 * @param set source header set.
		 */
		void deep_copy_impl(const attribute_header_set& set)
		{
			base_type::h_ptr = ::setsize(base_type::h_ptr, set.header_count());
			std::copy(set.header_begin(), set.header_end(), base_type::h_ptr);
			base_type::a_cnt = set.a_cnt;
			base_type::l_cnt = set.l_cnt;
			base_type::y_cnt = set.y_cnt;
		}
		
	public:
		/** Clear the header set.
		 */
		void clear()
		{
			::erase(l_ptr);
			::erase(base_type::h_ptr);
			base_type::a_cnt=0;
			base_type::l_cnt=0;
			base_type::y_cnt=0;
		}
		/** Assign an attribute header form a collection of string names.
		 * 
		 * @param beg start of string collection.
		 * @param end end of string collection.
		 * @param c class position.
		 * @param l number of labels.
		 */
		template<class I>
		void assign_header(I beg, I end, size_type c, size_type l)
		{
			::erase(base_type::h_ptr);
			base_type::h_ptr = ::setsize(base_type::h_ptr, std::distance(beg, end));
			base_type::l_cnt = l;
			base_type::a_cnt = std::distance(beg, end) - l - 1;
			if( c >= (std::distance(beg, end)-l) ) c = std::distance(beg, end)-l-1;
			if( c==0 ) 
			{
				std::copy(beg, end, base_type::h_ptr);
			}
			else if( c>=(std::distance(beg, end)-l))
			{
				std::copy(beg, beg+l, base_type::h_ptr);
				base_type::h_ptr[l] = *(beg+l+base_type::a_cnt);
				std::copy(beg+l, beg+l+base_type::a_cnt, base_type::h_ptr+l+1);
			}
			else
			{
				std::copy(beg, beg+l, base_type::h_ptr);
				base_type::h_ptr[l] = *(beg+l+c);
			}
		}
		/** Assign a headers to this set.
		 * 
		 * @param lbeg iterator to start of label collection.
		 * @param lend iterator to end of label collection.
		 * @param cbeg iterator to start of class collection.
		 * @param hend iterator to start of end header collection.
		 */
		template<class I>
		void assign_header(I lbeg, I lend, I cbeg, I hend)
		{
			::erase(base_type::h_ptr);
			base_type::h_ptr = ::setsize(base_type::h_ptr, std::distance(lbeg, hend));
			base_type::l_cnt = std::distance(lbeg, lend);
			base_type::a_cnt = std::distance(lbeg, hend) - base_type::l_cnt - 1;
			
			header_pointer h_beg = base_type::h_ptr;
			std::copy(lbeg, lend, h_beg); 
			h_beg+=std::distance(lbeg, lend);
			std::copy(lend, cbeg, h_beg);  
			h_beg+=std::distance(lend, cbeg);
			*h_beg = *cbeg; ++h_beg;
			std::copy(cbeg+1, hend, h_beg);
		}
		/** Reassign the attribute header form a collection of string names.
		 * 
		 * @param beg start of string collection.
		 * @param end end of string collection.
		 */
		template<class I>
		void reassign_header(I beg, I end)
		{
			header_pointer tmp = ::setsize<header_type>(std::distance(beg, end)+base_type::l_cnt+1);
			std::copy(base_type::h_ptr, base_type::h_ptr+base_type::l_cnt+1, tmp);
			std::copy(beg, end, tmp+base_type::l_cnt+1);
			::erase(base_type::h_ptr);
			base_type::a_cnt = std::distance(beg, end);
			base_type::h_ptr = tmp;
		}
		/** Insert a nominal value into the header.
		 * 
		 * @param val an attribute name.
		 * @param idx attribute index.
		 * @param spar is sparse.
		 * @param add should add new value.
		 * @return feature value.
		 */
		feature_type insert_nominal(std::string val, index_type idx, bool spr, bool add)
		{
			ASSERT( size_type(idx) < base_type::attribute_count() );
			return base_type::attribute_begin()[idx].valueOf(val, type_util::missing(), spr, add);
		}
		/** Insert a class label into the header.
		 * 
		 * @param val an attribute name.
		 * @param mis missing character.
		 * @param add should add new value.
		 * @return class value.
		 */
		class_type insert_class(std::string val, char mis, bool add)
		{
			typedef TypeUtil<class_type> class_util;
			ASSERT( base_type::header_begin() != base_type::header_end() );
			if( val[0] == mis && val.length() == 1 ) return class_util::max();
			return base_type::class_header().valueOf(val, class_util::max(), false, add);
		}
		/** Get start of label vector.
		 * 
		 * @return label pointer.
		 */
		label_type* label_start()
		{
			return l_ptr;
		}
		/** Resize the header.
		 * 
		 * @param a number of attributes.
		 * @param e number of examples.
		 * @param l number of labels.
		 */
		void resize_header(size_type a, size_type e, size_type l)
		{
			if( base_type::a_cnt == 0 )
			{
				::erase(base_type::h_ptr);
				base_type::h_ptr = ::setsize(base_type::h_ptr, a+l+1);
				base_type::l_cnt = l;
				base_type::a_cnt = a;
			}
			if( l > 0 ) 
			{
				::erase(l_ptr);
				l_ptr = ::setsize(l_ptr, l*e);
			}
		}
		/** Resize the number of labels
		 * 
		 * @param e number of examples.
		 * @param b number of bags.
		 */
		void resize_labels(size_type e, size_type b)
		{
			if( b > 0 && base_type::l_cnt > 0 ) 
			{
				label_type* tmp = ::setsize<label_type>((base_type::l_cnt*e)+b);
				std::copy(l_ptr, l_ptr+(base_type::l_cnt*e), tmp);
				::erase(l_ptr);
				l_ptr = tmp;
			}
		}
		
	protected:
		/** Reset the statistics for each column.
		 */
		void reset_statistics()
		{
			for(header_iterator beg = base_type::attribute_begin(), end = base_type::attribute_end();beg != end;++beg)
				beg->reset();
		}
		/** Esimate the statistics for each column.
		 * 
		 * @param e example count.
		 */
		void finalize_statistics(size_type e)
		{
			for(header_iterator beg = base_type::attribute_begin(), end = base_type::attribute_end();beg != end;++beg)
				beg->finalize(e);
		}
		
	private:
		label_type* l_ptr;
	};
};

#endif


