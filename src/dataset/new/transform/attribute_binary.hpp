/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_binary.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ATTRIBUTE_BINARY_HPP
#define _EXEGETE_ATTRIBUTE_BINARY_HPP
#include "format_utility.hpp"


/** @file attribute_binary.hpp
 * @brief Convert attributes to binary
 * 
 * This file contains a class that converts attributes to binary.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Convert attributes to binary
	 * 
	 * This class converts attributes to binary
	 * 
	 * @todo add read and write state
	 */
	template<class dataset_type>
	class attribute_binary : public base_format_utility
	{
	private:
		typedef typename dataset_type::feature_type		feature_type;
		typedef typename dataset_type::iterator			iterator;
		typedef typename dataset_type::size_type 		size_type;
		typedef std::vector<feature_type>				feature_vector;
		typedef std::vector<size_type>					cut_vector;
		typedef typename cut_vector::iterator 			cut_iterator;
		typedef std::vector<cut_vector>					feature_cut_vector;
		typedef std::vector<std::string>				header_vector;
		typedef typename feature_cut_vector::const_reverse_iterator 	const_iterator;
		typedef typename cut_vector::const_reverse_iterator 			const_rcut_iterator;
		typedef typename dataset_type::attribute_pointer attribute_pointer;
		
	public:
		/** Constructs an attribute binary transform.
		 */
		attribute_binary() : total(0), only_nominalBool(false)
		{
		}
		/** Destructs an attribute binary transform.
		 */
		~attribute_binary()
		{
		}
		
	public:
		/** Initalizes the filter.
		 *
		 * @param map an argument map.
		 * @param t a flag.
		 */
		template<class U>
		void add_arguments(U& map, int t)
		{
		}
		/** Converts attributes to binary
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		const char* transform(dataset_type& dataset)
		{
			if( cuts.empty() )
			{
				total = 0;
				cuts.resize(dataset.attribute_count());
				setup(dataset);
			}
			if( cuts.size() != dataset.attribute_count() ) return ERRORMSG("Number of attributes does not match model: " << cuts.size() << " != " << dataset.attribute_count());
			tobinary(dataset);
			dataset.finalize();
			return 0;
		}
		/** Test is model is empty.
		 * 
		 * @return true if empty.
		 */
		bool empty()const
		{
			return cuts.empty();
		}
		/** Flag whether to convert only nominal to binary.
		 * 
		 * @param b a flag.
		 */
		void only_nominal(bool b)
		{
			only_nominalBool = b;
		}
		
	protected:
		/** Convert dataset to binary using mapping.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void tobinary(dataset_type& dataset)
		{
			typedef typename dataset_type::type_util type_util;
			size_type off = dataset.attribute_total(), attrn=dataset.attribute_count();
			dataset.reassign_header(headers.begin(), headers.end());
			
			attribute_pointer aend = dataset.feature_begin()+off;
			attribute_pointer abeg = aend;
			for(iterator beg=dataset.end()-1, end=dataset.begin()-1;beg != end;--beg)
			{
				abeg-=attrn;
				copy_cut(aend-1, abeg-1, beg->x()+total-1, cuts.rbegin());
				aend=abeg;
			}
		}
		/** Convert attribute row from nominal to binary.
		 * 
		 * @param beg start of old attribute vector.
		 * @param end end of old attribute vector.
		 * @param it start of new attribute vector.
		 * @param pcut start of cut vector.
		 */
		void copy_cut(attribute_pointer beg, attribute_pointer end, attribute_pointer it, const_iterator pcut)
		{
			for(;beg != end;--beg, ++pcut)
			{
				if( pcut->empty() )
				{
					*it = *beg;
					--it;
				}
				else it = copy_cut(pcut->rbegin(), pcut->rend(), it, *beg);
			}
		}
		/** Copy nominal to binary.
		 * 
		 * @param beg start of nominal cut values.
		 * @param end end of nominal cut values.
		 * @param it new attribute iterator.
		 * @param val value to test.
		 * @return new attribute iterator.
		 */
		attribute_pointer copy_cut(const_rcut_iterator beg, const_rcut_iterator end, attribute_pointer it, feature_type val)
		{
			for(;beg != end;++beg, --it)  *it = ( (size_type(val) == *beg ) ? 1 : 0 );
			return it;
		}
		
	protected:
		/** Setup binary mapping.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void setup(dataset_type& dataset)
		{
			typedef typename dataset_type::const_header_iterator const_header_iterator;
			typedef const typename dataset_type::header_type header_type;
			typedef typename header_type::const_iterator const_nominal_iterator;
			cuts.resize( dataset.attribute_count() );
			for(size_type i=0;i<dataset.attribute_count();++i)
			{
				ASSERT(i<dataset.attribute_count());
				if( dataset.attribute_at(i).isnumeric() || dataset.attribute_at(i).isbinary() ) 
				{
					total++;
					continue;
				}
				if( dataset.attribute_at(i).isnominal() ) setup_nominal(dataset, i);
				else if( !only_nominalBool ) 			  setup_discreet(dataset, i);
			}
			headers.resize( total );
			size_type j=0, i=0;
			for(const_header_iterator beg = dataset.attribute_begin(), end = dataset.attribute_end();beg != end;++beg, ++i)
			{
				if( beg->isnominal() )
				{
					for(const_nominal_iterator nbeg = beg->begin(), nend = beg->end();nbeg != nend;++nbeg, ++j) 
					{
						ASSERTMSG(j<headers.size(), j << " < " << headers.size() );
						headers[j] = "IS_"+(*nbeg);
					}
				}
				else if( !only_nominalBool && beg->isdiscreet() )
				{
					std::string start = beg->name(), val;
					start+="=";
					for(size_type k=0;k<cuts[i].size();++k,++j)
					{
						ASSERTMSG(j<headers.size(), j << " < " << headers.size() );
						valueToString(cuts[i][k], val);
						headers[j] = start + val;
					}
				}
				else
				{
					headers[j] = beg->name();
					++j;
				}
			}
		}
		/** Setup binary mapping for nominal attributes.
		 * 
		 * @param dataset a reference to concrete example set.
		 * @param n attribute index.
		 */
		void setup_nominal(dataset_type& dataset, size_type n)
		{
			ASSERT(n<dataset.attribute_count());
			cuts[n].resize( dataset.attribute_at(n).size() );
			total+=dataset.attribute_at(n).size();
			for(size_type i=0;i<dataset.attribute_at(n).size();++i) cuts[n][i] = i;
		}
		/** Setup binary mapping for discreet attributes.
		 * 
		 * @param dataset a reference to concrete example set.
		 * @param n attribute index.
		 */
		void setup_discreet(dataset_type& dataset, size_type n)
		{
			typedef typename dataset_type::shallow_set example_set;
			
			example_set exset(dataset);
			exset.assign(dataset);
			
			ASSERT(n < dataset.attribute_count());
			iterator end = sort_no_missing(exset.begin(), exset.end(), n);
			ASSERT(exset.begin() != end);
			size_type i=0, last;
			for(iterator beg = exset.begin();beg != end;)
			{
				if( i >= cuts[n].size() ) cuts[n].resize((i+1)*2);
				last = size_type(beg->x()[n]);
				while( beg != end && last == size_type(beg->x()[n]) ) ++beg;
				ASSERT(n < cuts.size());
				ASSERTMSG(i < cuts[n].size(), i << " < " << cuts[n].size());
				cuts[n][i] = last;
				++i;
			}
			cuts[n].resize(i);
			total+=i;
		}
		/** Removes missing from end of example collection.
		 * 
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param i attribute index.
		 * @return end of attribute collection without missing.
		 */
		iterator no_missing_end(iterator beg, iterator end, size_type i)
		{
			--end;
			for(;beg != end;--end) if( !is_attribute_missing(end->x()[i])  ) break;
			++end;
			return end;
		}
		/** Sorts a set of example by some attribute in ascending order.
		 * 
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param i attribute index.
		 * @return end of attribute collection without missing.
		 */
		iterator sort_no_missing(iterator beg, iterator end, size_type i)
		{
			std::stable_sort( beg, end, attribute_less_than<iterator>( i ) );
			return no_missing_end(beg, end, i);
		}
		
	public:
		/** Reads an discreet model from the input stream.
		 *
		 * @param in an input stream.
		 * @param filter a filter.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, attribute_binary& filter)
		{
			size_type n,m;
			in >> n;
			if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row count");
			filter.cuts.resize(n);
			for(size_type i=0;i<n;++i)
			{
				in >> m;
				for(size_type j=0;j<m;++j)
				{
					if( in.get() != ',' ) return base_format_utility::fail(in, "Missing comma after " << j);
					in >> filter.cuts[i][j];
				}
				filter.total += m;
				if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row");
			}
			return in;
		}
		/** Writes an discreet model to the output stream.
		 *
		 * @param out an output stream.
		 * @param filter a filter.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const attribute_binary& filter)
		{
			out << filter.cuts.size() << "\n";
			for(size_type i=0;i<filter.cuts.size();++i)
			{
				out << filter.cuts[i].size();
				for(size_type j=0;j<filter.cuts[i].size();++j)
					out << "," << filter.cuts[i][j];
				out << "\n";
			}
			return out;
		}
		
	private:
		header_vector headers;
		feature_cut_vector cuts;
		size_type total;
		
	private:
		bool only_nominalBool;
	};
};

#endif

