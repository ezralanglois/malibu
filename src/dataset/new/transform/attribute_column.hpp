/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_column.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ATTRIBUTE_COLUMN_HPP
#define _EXEGETE_ATTRIBUTE_COLUMN_HPP
#include "format_utility.hpp"


/** @file attribute_column.hpp
 * @brief Removes attribute columns
 * 
 * This file contains a class that removes attribute columns.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Remove attribute columns
	 * 
	 * This class removes attribute columns.
	 * 
	 * @todo should deallocate memory?
	 */
	template<class dataset_type>
	class attribute_column : public base_format_utility
	{
	private:
		typedef typename dataset_type::feature_type		feature_type;
		typedef typename dataset_type::value_type		example_type;
		typedef typename dataset_type::iterator			iterator;
		typedef typename dataset_type::const_iterator	const_iterator;
		typedef typename dataset_type::size_type 		size_type;
		typedef std::vector<std::string>				column_vector;
		typedef std::vector<size_type> 					size_vector;
		
	public:
		/** Constructs an attribute column transform.
		 */
		attribute_column()
		{
		}
		/** Destructs an attribute column transform.
		 */
		~attribute_column()
		{
		}
		
	public:
		/** Initalizes the filter.
		 *
		 * @param map an argument map.
		 * @param t a flag.
		 */
		template<class U>
		void add_arguments(U& map, int t)
		{
			arginit(map, "Column Filter");
			map(columns, "columns", "columns to process (remove) either indices or labels");
		}
		/** Converts attributes to binary
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		const char* transform(dataset_type& dataset)
		{
			if( columns.empty() ) return 0;
			if( columns.size() >= dataset.attribute_count() ) return ERRORMSG("Cannot remove every attribute.");
			
			size_type o = dataset.attribute_count(), tmp;
			size_type n = dataset.attribute_count()-columns.size();
			size_vector cidx(o, 1);
			for(size_type i=0;i<columns.size();++i)
			{
				if( !stringToValue(columns[i], tmp))
				{
					tmp = index_of( dataset.attribute_begin(), dataset.attribute_end(), columns[i]);
					if( tmp == dataset.attribute_count()  ) return ERRORMSG("Attribute name not found: \"" << columns[i] << "\"");
				}
				cidx[tmp] = 0;
			}
			for(size_type i=0, j=0;i<o;++i)
			{
				if( cidx[i] )
				{
					cidx[j] = i;
					if( i != j ) dataset.attribute_at(j) = dataset.attribute_at(i);
					j++;
				}
			}
			cidx.resize(n);
			for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				for(size_type i=0;i<n;++i) beg->x()[i] = beg->x()[cidx[i]];
			}
			dataset.attribute_count(n);
			
			return 0;
		}
		/** Test is model is empty.
		 * 
		 * @return true if empty.
		 */
		bool empty()const
		{
			return columns.empty();
		}
		
	private:
		typedef typename dataset_type::const_header_iterator const_header_iterator;
	protected:
		/** Get index of header for name.
		 * 
		 * @param beg start of header collection.
		 * @param end end of header collection.
		 * @param nm source name.
		 * @return index of name.
		 */
		static size_type index_of(const_header_iterator beg, const_header_iterator end, const std::string& nm)
		{
			const_header_iterator cur=beg;
			for(;cur != end;++cur) if( nm == cur->name() ) break;
			return size_type(std::distance(beg, cur));
		}
		
	public:
		/** Reads an filter model from the input stream.
		 *
		 * @param in an input stream.
		 * @param filter a filter.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, attribute_column& filter)
		{
			return in;
		}
		/** Writes an filter model to the output stream.
		 *
		 * @param out an output stream.
		 * @param filter a filter.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const attribute_column& filter)
		{
			return out;
		}
		
	private:
		std::vector< std::string > columns;
	};
};

#endif

