
/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_normalization.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ATTRIBUTE_NORMALIZATION_HPP
#define _EXEGETE_ATTRIBUTE_NORMALIZATION_HPP
#include "format_utility.hpp"


/** @file attribute_normalization.hpp
 * @brief Normalize attributes
 * 
 * This file contains a class that normalize attributes.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Normalize attributes
	 * 
	 * This class normalizes attributes.
	 * 
	 * @todo add ymin, ymax
	 */
	template<class dataset_type>
	class attribute_normalization : public base_format_utility
	{
	private:
		typedef typename dataset_type::feature_type		feature_type;
		typedef typename dataset_type::type_util		type_util;
		typedef typename dataset_type::iterator			iterator;
		typedef typename dataset_type::const_iterator	const_iterator;
		typedef typename dataset_type::size_type 		size_type;
		typedef std::pair<feature_type,feature_type>	norm_pair;
		typedef std::vector<norm_pair>					feature_norm_vector;
		
	public:
		/** Constructs an attribute binary transform.
		 */
		attribute_normalization() : modeCh('m'), zeroCh(1)
		{
		}
		/** Destructs an attribute binary transform.
		 */
		~attribute_normalization()
		{
		}
		
	public:
		/** Initalizes the filter.
		 *
		 * @param map an argument map.
		 * @param t a flag.
		 */
		template<class U>
		void add_arguments(U& map, int t)
		{
			arginit(map, "Normalization Filter");
			arginit(map, modeCh, "normalize","normalization type>MinMax:m;ZNorm:z;ZANorm:a");
			arginit(map, zeroCh, "keep_zero", "translate normalization to maintain zero?");
		}
		/** Converts attributes to binary
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		const char* transform(dataset_type& dataset)
		{
			if( cuts.empty() )
			{
				total = 0;
				cuts.resize(dataset.attribute_count(), norm_pair(0,0));
				if( zeroCh == 1 ) 
				{
					ncuts.resize(dataset.attribute_count(), norm_pair(0,0));
					setup_zero(dataset);
				}
				else setup(dataset);
			}
			if( cuts.size() != dataset.attribute_count() ) return ERRORMSG("Number of attributes does not match model: " << cuts.size() << " != " << dataset.attribute_count());
			if( zeroCh == 1 ) normalize_zero(dataset);
			else normalize(dataset);
			dataset.finalize();
			return 0;
		}
		/** Test is model is empty.
		 * 
		 * @return true if empty.
		 */
		bool empty()const
		{
			return cuts.empty();
		}
		
	protected:
		/** Normalize non-nominal values in dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void normalize_zero(dataset_type& dataset)
		{
			feature_type poff, pmlt;
			feature_type noff, nmlt;
			for(size_type i=0;i<dataset.attribute_count();++i)
			{
				pmlt = cuts[i].first;
				nmlt = ncuts[i].first;
				if( pmlt == 0 && nmlt == 0) continue;
				poff = cuts[i].second;
				noff = ncuts[i].second;
				for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
				{
					if( beg->x()[i] == type_util::missing() ) continue;
					if( beg->x()[i] == 0 ) continue;
					if( beg->x()[i] > 0 && pmlt > 0 )
					{
						beg->x()[i] = (beg->x()[i]-poff) * pmlt;
					}
					else if( nmlt > 0 )
					{
						beg->x()[i] = (beg->x()[i]-noff) * nmlt;
					}
				}
			}
		}
		/** Normalize non-nominal values in dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void normalize(dataset_type& dataset)
		{
			feature_type xoff, xmlt;
			for(size_type i=0;i<dataset.attribute_count();++i)
			{
				xmlt = cuts[i].first;
				if( xmlt == 0 ) continue;
				xoff = cuts[i].second;
				for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
				{
					if( beg->x()[i] == type_util::missing() ) continue;
					beg->x()[i] = (beg->x()[i]-xoff) * xmlt;
				}
			}
		}
		
	protected:
		/** Setup normalization mapping.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void setup_zero(dataset_type& dataset)
		{
			typedef typename dataset_type::header_iterator header_iterator;
			feature_type pmin=0, pmlt=0, tmp;
			feature_type nmin=0, nmlt=0;
			size_type i=0;
			for(header_iterator beg = dataset.attribute_begin(), end = dataset.attribute_end();beg != end;++beg, ++i)
			{
				if( beg->isnominal() || beg->isbinary() ) continue;
				if( modeCh == 'm' )
				{
					nmin = beg->min();
					pmlt=1.0f/(beg->max()-0);
					if(nmin < 0) nmlt=1.0f/(0-nmin);
				}
				else if( modeCh == 'z' )
				{
					pmlt = pmin = 0.0f;
					nmlt = nmin = 0.0f;
					for(const_iterator beg = dataset.begin(), end=dataset.end(); beg != end;++beg)
					{
						tmp = beg->x()[i];
						if( tmp == type_util::missing() ) continue;
						if( tmp < 0 )
						{
							nmin += tmp;
							nmlt += tmp*tmp;
						}
						else if ( tmp > 0 )
						{
							pmin += tmp;
							pmlt += tmp*tmp;
						}
					}
					tmp = 1.0f / feature_type(dataset.size());
					pmin *= tmp;
					nmin *= tmp;
					pmlt = tmp*pmlt - pmin*pmin;
					nmlt = tmp*nmlt - nmin*nmin;
				}
				else if( modeCh == 'a' )
				{
					pmlt = pmin = 0.0f;
					nmlt = nmin = 0.0f;
					for(const_iterator beg = dataset.begin(), end=dataset.end(); beg != end;++beg)
					{
						tmp = beg->x()[i];
						if( tmp == type_util::missing() ) continue;
						if( tmp < 0 )
						{
							nmin += tmp;
							nmlt += std::abs(tmp);
						}
						else if( tmp > 0 )
						{
							pmin += tmp;
							pmlt += std::abs(tmp);
						}
					}
					tmp = 1.0f / feature_type(dataset.size());
					pmin *= tmp;
					nmin *= tmp;
					pmlt = tmp*pmlt - pmin;
					nmlt = tmp*nmlt - nmin;
				}
				else continue;
				cuts[i] = std::make_pair(pmlt, pmin);
				ncuts[i] = std::make_pair(nmlt, nmin);
			}
		}
		/** Setup normalization mapping.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void setup(dataset_type& dataset)
		{
			typedef typename dataset_type::header_iterator header_iterator;
			feature_type xmin=0, xmlt=0, tmp;
			size_type i=0;
			
			for(header_iterator beg = dataset.attribute_begin(), end = dataset.attribute_end();beg != end;++beg, ++i)
			{
				if( beg->isnominal() || beg->isbinary() ) continue;
				if( modeCh == 'm' )
				{
					xmin = beg->min();
					xmlt=1.0f/(beg->range());
				}
				else if( modeCh == 'z' )
				{
					xmlt = xmin = 0.0f;
					for(const_iterator beg = dataset.begin(), end=dataset.end(); beg != end;++beg)
					{
						tmp = beg->x()[i];
						if( tmp == type_util::missing() ) continue;
						xmin += tmp;
						xmlt += tmp*tmp;
					}
					tmp = 1.0f / feature_type(dataset.size());
					xmin *= tmp;
					xmlt = tmp*xmlt - xmin*xmin;
				}
				else if( modeCh == 'a' )
				{
					xmlt = xmin = 0.0f;
					for(const_iterator beg = dataset.begin(), end=dataset.end(); beg != end;++beg)
					{
						tmp = beg->x()[i];
						if( tmp == type_util::missing() ) continue;
						xmin += tmp;
						xmlt += std::abs(tmp);
					}
					tmp = 1.0f / feature_type(dataset.size());
					xmin *= tmp;
					xmlt = tmp*xmlt - xmin;
				}
				else continue;
				cuts[i] = std::make_pair(xmlt, xmin);
			}
		}
		
	public:
		/** Reads an filter model from the input stream.
		 *
		 * @param in an input stream.
		 * @param filter a filter.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, attribute_normalization& filter)
		{
			size_type n,m;
			in >> n;
			if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row count");
			filter.cuts.resize(n);
			for(size_type i=0;i<n;++i)
			{
				in >> filter.cuts[i].first;
				if( in.get() != ',' ) return base_format_utility::fail(in, "Missing comma");
				in >> filter.cuts[i].second;
				if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row");
			}
			return in;
		}
		/** Writes an filter model to the output stream.
		 *
		 * @param out an output stream.
		 * @param filter a filter.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const attribute_normalization& filter)
		{
			out << filter.cuts.size() << "\n";
			for(size_type i=0;i<filter.cuts.size();++i)
			{
				out << filter.cuts[i].first << "," << filter.cuts[i].second << "\n";
			}
			return out;
		}
		
	private:
		feature_norm_vector cuts;
		feature_norm_vector ncuts;
		size_type total;
		
	private:
		char modeCh;
		char zeroCh;
	};
};

#endif

