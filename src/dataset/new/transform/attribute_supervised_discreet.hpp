/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_supervised_discreet.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ATTRIBUTE_SUPERVISED_DISCREETIZE_HPP
#define _EXEGETE_ATTRIBUTE_SUPERVISED_DISCREETIZE_HPP
#include "contingency_metrics.hpp"


/** @file attribute_supervised_discreet.hpp
 * @brief Discreetize attributes with supervision
 * 
 * This file contains a class that discreetize attributes with supervision.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Discreetize attributes with supervision
	 * 
	 * This class discreetizes real attributes with supervision.
	 */
	template<class E, class F>
	class attribute_supervised_discreet
	{
	public:
		/** Defines an example. **/
		typedef E 									example_type;
		/** Defines an example iterator. **/
		typedef E* 									example_iterator;
		/** Defines a float type. **/
		typedef F 									float_type;
		/** Defines a float vector. **/
		typedef std::vector<float_type> 			float_vector;
		/** Defines a float iterator. **/
		typedef typename float_vector::pointer 		float_pointer;
		/** Defines a float iterator. **/
		typedef typename float_vector::size_type 	size_type;
		
	public:
		/** Constructs a supervised attribute discreet transform.
		 */
		attribute_supervised_discreet(size_type cl) : curr_count(2, cl), 
			prior_count(cl), best_count(2, cl)
		{
		}
		/** Destructs a supervised attribute discreet transform.
		 */
		~attribute_supervised_discreet()
		{
		}
		
	public:			
		/** Determine discreet cuts using a supervised MDL criterion.
		 * 
		 * @param cuts discreet cuts.
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param n attribute index.
		 * @return iterator to end of cuts vector.
		 */
		float_pointer mdl_discreet_cut(float_pointer pcuts, example_iterator beg, example_iterator end, size_type n)
		{
			if( std::distance(beg, end) < 2 )  return 0;
			curr_count.fill(0.0);
			prior_count.fill(0.0);

			float_type frac = 1.0;// / std::distance(beg,end);
			double totwgt = build_contingency(beg, end, prior_count, frac);
			prior_ent = entropy(prior_count);
			best_ent=prior_ent;
			std::copy(prior_count.begin(), prior_count.end(), curr_count.row_at(1));

			size_type cuts;
			example_iterator best_idx = split(beg, end, n, cuts);
			double gain = prior_ent - best_ent;
			if( gain <= 0.0 ) return 0;
			if( mdl_bound(prior_count, best_count, totwgt, cuts) )
			{
				float_type bstcut = best_cut;
				float_pointer tmp;
				tmp = mdl_discreet_cut(pcuts, beg, best_idx+1, n);
				if( tmp != 0 ) pcuts = tmp;
				*pcuts = bstcut; ++pcuts;
				tmp = mdl_discreet_cut(pcuts, best_idx+1, end, n);
				if( tmp != 0 ) pcuts = tmp;
				return pcuts;
			}
			else return 0;
		}
		
	protected:
		/** Determine the best cut using gain.
		 * 
		 * @param cuts discreet cuts.
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param n attribute index.
		 * @return iterator to end of cuts vector.
		 */
		example_iterator split(example_iterator beg, example_iterator end, size_type n, size_type& rcuts)
		{
			float_type frac = 1.0;// / std::distance(beg,end);
			float_type cut= TypeUtil<float_type>::min();
			example_iterator bst = beg;
			float_type ent;
			size_type cuts=0;
			if(beg != end) --end;
			double* rhs = ((double**)curr_count)[0];
			double* lhs = ((double**)curr_count)[1];
			for(;beg != end;++beg)
			{
				if( is_attribute_missing(beg->y()) ) continue;
				rhs[beg->y()] += beg->weight(frac);
				lhs[beg->y()] -= beg->weight(frac);
				if( beg->x()[n] < (beg+1)->x()[n] )
				{
					cut = 0.5 * (beg->x()[n] + (beg+1)->x()[n]);
					ent = entropy_column_on_row(curr_count);
					if( ent < best_ent )
					{
						best_cut = cut;
						best_ent = ent;
						bst = beg;
						best_count = curr_count;
					}
					++cuts;
				}
			}
			rcuts = cuts;
			return bst;
		}
		/** Test using an MDL criterion.
		 * 
		 * @param prior prior counts.
		 * @param best best counts.
		 * @param totwgt total weight.
		 * @param cuts number of cuts.
		 * @return true if should split.
		 */
		bool mdl_bound(const contingency_array<double>& prior, const contingency_table<double>& best, float_type totwgt, size_type cuts)
		{
			//return mdl_kononenkos(prior, best, totwgt, cuts);
			return mdl_fayyad_irani(prior, best, totwgt, cuts);
		}
		/** Test using Kononenko's MDL criterion.
		 * 
		 * @todo fix this, does not work
		 * 
		 * @param prior prior counts.
		 * @param best best counts.
		 * @param totwgt total weight.
		 * @param cuts number of cuts.
		 * @return true if should split.
		 */
		bool mdl_kononenkos(const contingency_array<double>& prior, const contingency_table<double>& best, float_type totwgt, size_type cuts)
		{
			size_type cl_cnt = class_count(prior, prior.length());
			float_type bound = log_to_binomial( totwgt + cl_cnt - 1, cl_cnt - 1 ) +
							   log_to_multinomial( totwgt, prior, prior.length() );
			float_type value = log2(cuts), tot;
			for(size_type i=0;i<best.rows();++i)
			{
				tot = sum_row(best[i], best.columns());
				value += log_to_binomial(tot + cl_cnt - 1, cl_cnt - 1);
			    value += log_to_multinomial(tot, best[i], best.columns());
			}
			return bound > value;
		}
		/** Test using Fayyad and Irani's MDL criterion.
		 * 
		 * @param prior prior counts.
		 * @param best best counts.
		 * @param totwgt total weight.
		 * @param cuts number of cuts.
		 * @return true if should split.
		 */
		bool mdl_fayyad_irani(const contingency_array<double>& prior, const contingency_table<double>& best, float_type totwgt, size_type cuts)
		{
			double gain = prior_ent - best_ent;
			size_type cl_cnt = class_count(prior, prior.length());
			size_type cl_lhs_cnt = class_count(best.row_at(0), best.columns());
			size_type cl_rhs_cnt = class_count(best.row_at(1), best.columns());
			double ent_lhs = entropy(best.row_at(0), best.columns());
			double ent_rhs = entropy(best.row_at(1), best.columns());
			double shift = log2( cl_cnt ) + log2( std::pow(3.0, double(cl_cnt))-2 ) - ( ( cl_cnt*prior_ent ) - (cl_lhs_cnt*ent_lhs) - (cl_rhs_cnt*ent_rhs) );
			return gain > ( shift / totwgt  );
		}
		/** Sums a row in a table.
		 * 
		 * @param arr a row in the table.
		 * @param n number of columns in row.
		 * @return sum over row.
		 */
		static double sum_row(const double* arr, size_type n)
		{
			double sum = 0.0;
			for(size_type i=0;i<n;++i) sum += arr[i];
			return sum;
		}
		/** Counts the number of classes under consideration.
		 * 
		 * @param prior counts for each class.
		 * @return number of classes.
		 */
		static size_type class_count(const double* prior, size_type n)
		{
			size_type cl_cnt = 0;
			for(size_type i=0;i<n;++i)
				if( prior[i] > 0 ) cl_cnt++;
			return cl_cnt;
		}
		
	private:
		contingency_table<double> curr_count;
		contingency_array<double> prior_count;
		contingency_table<double> best_count;
		
	private:
		float_type best_cut;
		float_type best_ent;
		float_type prior_ent;
	};
};

#endif

