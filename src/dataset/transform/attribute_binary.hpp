/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_binary.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ATTRIBUTE_BINARY_HPP
#define _EXEGETE_ATTRIBUTE_BINARY_HPP
#include "format_utility.hpp"
#include "ConcreteExampleSet.h"


/** @file attribute_binary.hpp
 * @brief Convert attributes to binary
 * 
 * This file contains a class that converts attributes to binary.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Convert attributes to binary
	 * 
	 * This class converts attributes to binary
	 * 
	 * @todo add read and write state
	 */
	template<class X, class Y>
	class attribute_binary : public base_format_utility
	{
	public:
		/** Defines a concrete example set. **/
		typedef ConcreteExampleSet<X,Y>					concrete_set;
	private:
		typedef typename concrete_set::feature_type		feature_type;
		typedef typename concrete_set::iterator			iterator;
		typedef typename concrete_set::size_type 		size_type;
		typedef std::vector<feature_type>				feature_vector;
		typedef std::vector<size_type>					cut_vector;
		typedef typename cut_vector::iterator 			cut_iterator;
		typedef std::vector<cut_vector>					feature_cut_vector;
		
	public:
		/** Constructs an attribute binary transform.
		 */
		attribute_binary() : total(0), only_nominalBool(false)
		{
		}
		/** Destructs an attribute binary transform.
		 */
		~attribute_binary()
		{
		}
		
	public:
		/** Initalizes the filter.
		 *
		 * @param map an argument map.
		 * @param t a flag.
		 */
		template<class U>
		void init(U& map, int t)
		{
		}
		/** Converts attributes to binary
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		const char* transform(concrete_set& dataset)
		{
			if( cuts.empty() )
			{
				total = 0;
				cuts.resize(dataset.attributeCount());
				setup(dataset);
			}
			if( cuts.size() != dataset.attributeCount() ) return ERRORMSG("Number of attributes does not match model: " << cuts.size() << " != " << dataset.attributeCount());
			tobinary(dataset);
			dataset.recalculate();
			return 0;
		}
		/** Test is model is empty.
		 * 
		 * @return true if empty.
		 */
		bool empty()const
		{
			return cuts.empty();
		}
		/** Flag whether to convert only nominal to binary.
		 * 
		 * @param b a flag.
		 */
		void only_nominal(bool b)
		{
			only_nominalBool = b;
		}
		
	protected:
		/** Convert dataset to binary using mapping.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void tobinary(concrete_set& dataset)
		{
			/*
		for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
		{
			attribute_pointer last = beg->x(), cur1=last;
			attribute_pointer abeg = setsize(abeg, attcnt+1), cur2=abeg+1;++abeg;
			for(header_iterator hbeg=new_header, hend=new_header+count;hbeg != hend;++hbeg, ++cur1)
			{
				val = type_util::valueOf(*cur1);
				if( hbeg->isnominal() )
				{
					for(const_nominal_iterator nbeg = hbeg->begin(), ncurr=nbeg, nend=hbeg->end();ncurr != nend;++ncurr, ++cur2) 
						*cur2 = (feature_type)((val == (feature_type)(std::distance(nbeg, ncurr)) ? 1 : 0) );
				}
				else 
				{
					*cur2 = val;
					++cur2;
				}
			}
			(*beg) = abeg;
			last--;
			erase(last);
		}
			 */
			typedef typename concrete_set::type_util type_util;
			create_header(dataset, dataset.attributeCount());
			feature_vector feat(cuts.size());
			feature_type val;
			for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				std::copy(beg->x(), beg->x()+feat.size(), feat.begin());
				beg->x(::resize(beg->x()-1, total+1+type_util::IS_SPARSE)+1);
				for(size_type i=0, j=0,k;i<cuts.size();++i)
				{
					val = feat[i];
					if(cuts[i].empty())
					{
						beg->x()[j] = val;
						++j;
					}
					else
					{
						for(k=0;k<cuts[i].size();++k,++j)
							beg->x()[j] = ( size_type(val) == cuts[i][k]) ? 1 : 0;
					}
				}
			}
			/*
			for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				beg->x(::resize(beg->x()-1, total+1+type_util::IS_SPARSE)+1);
				for(size_type i=cuts.size(), j=total;i>0;--i)
				{
					val = type_util::valueOf(beg->x()[i-1]);
					if( cuts[i-1].empty() )
					{
						type_util::valueOf(beg->x()[j-1]) = val;
						--j;
					}
					else
					{
						for(size_type k=cuts[i-1].size();k>0;--k,--j)
						{
							ASSERTMSG((j-1) < dataset.attributeCount(), j << " - " << dataset.attributeCount() << " = " << total );
							ASSERT((i-1) < cuts.size());
							ASSERT((k-1) < cuts[i-1].size());
							type_util::valueOf(beg->x()[j-1]) = ( size_type(val) == cuts[i-1][k-1] ? 1 : 0 );
						}
					}
				}
			}
			*/
		}
		/** Create new header for dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void create_header(concrete_set& dataset, size_type count)
		{
			typedef typename concrete_set::header_pointer			header_pointer;
			typedef typename concrete_set::header_type				header_type;
			typedef typename header_type::const_nominal_iterator	const_nominal_iterator;
			
			header_pointer old_header = setsize<header_type>(count);
			std::copy(dataset.header_begin(), dataset.header_end(), old_header);
			ASSERTMSG(total>dataset.attributeCount(), total << " > " << dataset.attributeCount());
			dataset.resizeAttributes(total);
			
			for(size_type i=0, j=0;i<count;++i)
			{
				if( old_header[i].isnominal() )
				{
					for(const_nominal_iterator nbeg = old_header[i].begin(), nend=old_header[i].end();nbeg != nend;++nbeg, ++j) 
					{
						ASSERTMSG(j<dataset.attributeCount(), j << " < " << dataset.attributeCount() );
						dataset.attributeAt(j) = "IS_"+(*nbeg);
					}
					ASSERT(i<dataset.attributeCount());
					dataset.attributeAt(i).clear();
				}
				else if( old_header[i].isdiscreet() )
				{
					std::string start = old_header[i].name(), val;
					start+="=";
					for(size_type k=0;k<cuts[i].size();++k,++j)
					{
						ASSERT(j<dataset.attributeCount());
						valueToString(cuts[i][k], val);
						dataset.attributeAt(j) = start + val;
					}
				}
				else 
				{
					ASSERT(j<dataset.attributeCount());
					dataset.attributeAt(j) = old_header[i].name();
					++j;
				}
			}
			::erase(old_header);
		}
		
	protected:
		/** Setup binary mapping.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void setup(concrete_set& dataset)
		{
			cuts.resize(dataset.attributeCount());
			for(size_type i=0;i<dataset.attributeCount();++i)
			{
				ASSERT(i<dataset.attributeCount());
				if( dataset.attributeAt(i).isnumeric() || dataset.attributeAt(i).isbinary() ) 
				{
					total++;
					continue;
				}
				if( dataset.attributeAt(i).isnominal() ) setup_nominal(dataset, i);
				else if( !only_nominalBool ) 			 setup_discreet(dataset, i);
			}
		}
		/** Setup binary mapping for nominal attributes.
		 * 
		 * @param dataset a reference to concrete example set.
		 * @param n attribute index.
		 */
		void setup_nominal(concrete_set& dataset, size_type n)
		{
			ASSERT(n<dataset.attributeCount());
			cuts[n].resize( dataset.attributeAt(n).size() );
			total+=dataset.attributeAt(n).size();
			for(size_type i=0;i<dataset.attributeAt(n).size();++i) cuts[n][i] = i;
		}
		/** Setup binary mapping for discreet attributes.
		 * 
		 * @param dataset a reference to concrete example set.
		 * @param n attribute index.
		 */
		void setup_discreet(concrete_set& dataset, size_type n)
		{
			typedef typename concrete_set::parent_type example_set;
			
			example_set exset(dataset);
			exset.assign(dataset);
			
			ASSERT(n < dataset.attributeCount());
			iterator end = sort_no_missing(exset.begin(), exset.end(), n);
			ASSERT(exset.begin() != end);
			size_type i=0, last;
			for(iterator beg = exset.begin();beg != end;)
			{
				if( i >= cuts[n].size() ) cuts[n].resize((i+1)*2);
				last = size_type(beg->x()[n]);
				while( beg != end && last == size_type(beg->x()[n]) ) ++beg;
				ASSERT(n < cuts.size());
				ASSERTMSG(i < cuts[n].size(), i << " < " << cuts[n].size());
				cuts[n][i] = last;
				++i;
			}
			cuts[n].resize(i);
			total+=i;
		}
		/** Removes missing from end of example collection.
		 * 
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param i attribute index.
		 * @return end of attribute collection without missing.
		 */
		iterator no_missing_end(iterator beg, iterator end, size_type i)
		{
			--end;
			for(;beg != end;--end) if( !is_attribute_missing(end->x()[i])  ) break;
			++end;
			return end;
		}
		/** Sorts a set of example by some attribute in ascending order.
		 * 
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param i attribute index.
		 * @return end of attribute collection without missing.
		 */
		iterator sort_no_missing(iterator beg, iterator end, size_type i)
		{
			std::stable_sort( beg, end, CompareExample<iterator>( i ) );
			return no_missing_end(beg, end, i);
		}
		
	public:
		/** Reads an discreet model from the input stream.
		 *
		 * @param in an input stream.
		 * @param filter a filter.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, attribute_binary& filter)
		{
			size_type n,m;
			in >> n;
			if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row count");
			filter.cuts.resize(n);
			for(size_type i=0;i<n;++i)
			{
				in >> m;
				for(size_type j=0;j<m;++j)
				{
					if( in.get() != ',' ) return base_format_utility::fail(in, "Missing comma after " << j);
					in >> filter.cuts[i][j];
				}
				filter.total += m;
				if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row");
			}
			return in;
		}
		/** Writes an discreet model to the output stream.
		 *
		 * @param out an output stream.
		 * @param filter a filter.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const attribute_binary& filter)
		{
			out << filter.cuts.size() << "\n";
			for(size_type i=0;i<filter.cuts.size();++i)
			{
				out << filter.cuts[i].size();
				for(size_type j=0;j<filter.cuts[i].size();++j)
					out << "," << filter.cuts[i][j];
				out << "\n";
			}
			return out;
		}
		
	private:
		feature_cut_vector cuts;
		size_type total;
		
	private:
		bool only_nominalBool;
	};
};

#endif

