/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_discreet.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ATTRIBUTE_DISCREETIZE_HPP
#define _EXEGETE_ATTRIBUTE_DISCREETIZE_HPP
#include "format_utility.hpp"
#include "attribute_supervised_discreet.hpp"
#include "ConcreteExampleSet.h"


/** @file attribute_discreet.hpp
 * @brief Discreetize attributes
 * 
 * This file contains a class that discreetize attributes.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Discreetize attributes
	 * 
	 * This class discreetizes real attributes.
	 * 
	 * @todo add read and write state
	 */
	template<class X, class Y>
	class attribute_discreet : public base_format_utility
	{
	public:
		/** Defines a concrete example set. **/
		typedef ConcreteExampleSet<X,Y>					concrete_set;
	private:
		typedef typename concrete_set::feature_type		feature_type;
		typedef typename concrete_set::iterator			iterator;
		typedef typename concrete_set::value_type		example_type;
		typedef std::vector<feature_type>				cut_vector;
		typedef typename cut_vector::iterator 			cut_iterator;
		typedef typename cut_vector::pointer 			cut_pointer;
		typedef std::vector<cut_vector>					feature_cut_vector;
		typedef typename concrete_set::size_type 		size_type;
		
	public:
		/** Constructs an attribute discreetize transform.
		 */
		attribute_discreet() : use_discreet(true), modeInt(1), bin_count(10)
		{
		}
		/** Destructs an attribute discreetize transform.
		 */
		~attribute_discreet()
		{
		}
		
	public:
		/** Initalizes the filter.
		 *
		 * @param map an argument map.
		 * @param t a flag.
		 */
		template<class U>
		void init(U& map, int t)
		{
			arginit(map, "Discreet Filter");
			arginit(map, modeInt, 	   "discreet_mode",		"method>Supervised:0;Width:1;Unique:2");
			arginit(map, use_discreet, "discreet_again",	"should discreetize discreet values?");
			arginit(map, bin_count,    "discreet_bin",		"number of bins for equal width");
		}
		/** Discreetizes every real value in the dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		const char* transform(concrete_set& dataset)
		{
			if( cuts.empty() )
			{
				cuts.resize(dataset.attributeCount());
				discreetizeByMDL(dataset);
			}
			else
			{
				if( cuts.size() != dataset.attributeCount() ) return ERRORMSG("Number of attributes does not match model: " << cuts.size() << " != " << dataset.attributeCount());
				discreetize(dataset);
			}
			dataset.recalculate();
			return 0;
		}
		/** Test is model is empty.
		 * 
		 * @return true if empty.
		 */
		bool empty()const
		{
			return cuts.empty();
		}
		
	protected:
		/** Discreetizes every real value in the dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void discreetize(concrete_set& dataset)
		{
			for(size_type i=0;i<dataset.attributeCount();++i)
			{
				if( cuts[i].empty() ) continue;
				discreetize_range(dataset.begin(), dataset.end(), &(*cuts[i].begin()), &(*cuts[i].end()), i);
			}
		}
		/** Discreetizes every real value in the dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void discreetizeByMDL(concrete_set& dataset)
		{
			typedef typename concrete_set::iterator			iterator;
			typedef typename concrete_set::size_type		size_type;
			typedef typename cut_vector::pointer 			cut_pointer;
			typedef typename concrete_set::parent_type		example_set;
			
			example_set exset(dataset);
			exset.assign(dataset);
			
			cut_pointer fbeg, fend;
			iterator beg=exset.begin(), end, fin=exset.end();
			attribute_supervised_discreet<example_type,feature_type> disc(dataset.classCount());

			for(size_type i=0;i<dataset.attributeCount();++i)
			{
				if( dataset.attributeAt(i).isnumeric() || (use_discreet && dataset.attributeAt(i).isdiscreet()) )
				{
					cuts[i].resize(dataset.size());
					fbeg = &(*cuts[i].begin());
					end = sort_no_missing(beg, fin, i);
					if( modeInt == 0 )
					{
						fend = disc.mdl_discreet_cut(fbeg, beg, end, i);
						if( fend == 0 ) fend = fbeg;
						if( fbeg == fend )
						{
							std::cerr << "Warning: Failed to discreetize column " << (i+1) << ", instead using fixed width" << std::endl;
							fend=equal_distance_bins(fbeg, dataset, i);
						}
					}
					else if( modeInt == 1 )
					{
						fend=equal_distance_bins(fbeg, dataset, i);
					}
					else
					{
						fend=unique_bins(fbeg, beg, end, i);
					}
					if( fend == 0 ) fend = fbeg;
					ASSERT(fbeg != fend );
					discreetize_range(beg, end, fbeg, fend, i);
					cuts[i].resize(std::distance(fbeg, fend));
				}
			}
		}
		/** Determine discreet cuts for equal distances.
		 * 
		 * @param cuts discreet cuts.
		 * @param dataset a reference to concrete example set.
		 * @param n attribute index.
		 * @return iterator to end of cuts vector.
		 */
		typename cut_vector::pointer equal_distance_bins(typename cut_vector::pointer pcuts, concrete_set& dataset, size_type n)
		{
			feature_type width = feature_type(dataset.attributeAt(n).range()) / bin_count;
			for(size_type i=1;i<bin_count;++i, ++pcuts)
			{
				*pcuts = dataset.attributeAt(n).min() + width * i;
			}
			return pcuts;
		}
		/** Determine discreet cuts for unique attributes.
		 * 
		 * @param cuts discreet cuts.
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param n attribute index.
		 * @return iterator to end of cuts vector.
		 */
		typename cut_vector::pointer unique_bins(typename cut_vector::pointer pcuts, iterator beg, iterator end, size_type n)
		{
			feature_type last = TypeUtil<feature_type>::max();
			for(;beg != end;++beg)
			{
				if( last != beg->x()[n] )
				{
					last = beg->x()[n];
					*pcuts = last;
					++pcuts;
				}
			}
			return pcuts;
		}
		/** Removes missing from end of example collection.
		 * 
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param i attribute index.
		 * @return end of attribute collection without missing.
		 */
		iterator no_missing_end(iterator beg, iterator end, size_type i)
		{
			--end;
			for(;beg != end;--end) 
			{
				if( !is_attribute_missing(end->x()[i])  ) break;
			}
			++end;
			return end;
		}
		/** Sorts a set of example by some attribute in ascending order.
		 * 
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param i attribute index.
		 * @return end of attribute collection without missing.
		 */
		iterator sort_no_missing(iterator beg, iterator end, size_type i)
		{
			std::stable_sort( beg, end, CompareExample<iterator>( i ) );
			return no_missing_end(beg, end, i);
		}
		/** Discreetize a collection of examples at a specific index.
		 * 
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @param cbeg start of cut collection.
		 * @param cend end of cut collection.
		 * @param n current attribute index.
		 */
		void discreetize_range(iterator beg, iterator end, cut_pointer cbeg, cut_pointer cend, size_type n)
		{
			for(;beg != end;++beg)
			{
				if( is_attribute_missing(beg->x()[n]) ) continue;
				cut_pointer it = std::lower_bound(cbeg, cend, beg->x()[n]);
				beg->x()[n] = std::distance(cbeg, it);
			}
		}
		
	public:
		/** Reads an discreet model from the input stream.
		 *
		 * @param in an input stream.
		 * @param filter a filter.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, attribute_discreet& filter)
		{
			size_type n,m;
			in >> n;
			if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row count");
			filter.cuts.resize(n);
			for(size_type i=0;i<n;++i)
			{
				in >> m;
				for(size_type j=0;j<m;++j)
				{
					if( in.get() != ',' ) return base_format_utility::fail(in, "Missing comma after " << j);
					in >> filter.cuts[i][j];
				}
				if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row");
			}
			return in;
		}
		/** Writes an discreet model to the output stream.
		 *
		 * @param out an output stream.
		 * @param filter a filter.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const attribute_discreet& filter)
		{
			out << filter.cuts.size() << "\n";
			for(size_type i=0;i<filter.cuts.size();++i)
			{
				out << filter.cuts[i].size();
				for(size_type j=0;j<filter.cuts[i].size();++j)
					out << "," << filter.cuts[i][j];
				out << "\n";
			}
			return out;
		}
		
	private:
		feature_cut_vector cuts;
		
	private:
		bool use_discreet;
		int modeInt;
		size_type bin_count;
	};
};

#endif

