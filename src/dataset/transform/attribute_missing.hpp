/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_missing.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ATTRIBUTE_MISSING_HPP
#define _EXEGETE_ATTRIBUTE_MISSING_HPP
#include "format_utility.hpp"
#include "ConcreteExampleSet.h"


/** @file attribute_missing.hpp
 * @brief Replace missing attributes
 * 
 * This file contains a class that replaces missing attributes.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Replace missing attributes
	 * 
	 * This class replaces missing attributes
	 * 
	 * @todo add read and write state
	 */
	template<class X, class Y>
	class attribute_missing : public base_format_utility
	{
	public:
		/** Defines a concrete example set. **/
		typedef ConcreteExampleSet<X,Y>					concrete_set;
	private:
		typedef typename concrete_set::feature_type		feature_type;
		typedef typename concrete_set::value_type		example_type;
		typedef typename concrete_set::iterator			iterator;
		typedef typename concrete_set::const_iterator	const_iterator;
		typedef std::vector<feature_type>				feature_vector;
		typedef typename concrete_set::size_type 		size_type;
		
	public:
		/** Constructs an attribute missing transform.
		 */
		attribute_missing()
		{
		}
		/** Destructs an attribute missing transform.
		 */
		~attribute_missing()
		{
		}
		
	public:
		/** Initalizes the filter.
		 *
		 * @param map an argument map.
		 * @param t a flag.
		 */
		template<class U>
		void init(U& map, int t)
		{
		}
		/** Replace missing values in the dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		const char* transform(concrete_set& dataset)
		{
			if( cuts.empty() )
			{
				cuts.resize(dataset.attributeCount());
				setup(dataset);
			}
			if( cuts.size() != dataset.attributeCount() ) return ERRORMSG("Number of attributes does not match model: " << cuts.size() << " != " << dataset.attributeCount());
			replace_missing(dataset);
			dataset.recalculate();
			return 0;
		}
		/** Test is model is empty.
		 * 
		 * @return true if empty.
		 */
		bool empty()const
		{
			return cuts.empty();
		}
		
	protected:
		/** Estimate missing values in the dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void setup(concrete_set& dataset)
		{
			std::vector< feature_vector > count(dataset.attributeCount());
			feature_vector sum(dataset.attributeCount(), 0.0f);
			feature_type wsum = sum_weights(dataset.begin(), dataset.end());
			for(size_type i=0;i<dataset.attributeCount();++i)
			{
				if( dataset.attributeAt(i).isnominal() )
				{
					count[i].resize( dataset.attributeAt(i).size(), 0.0f );
				}
				else count[i].resize(1, 0.0f);
				sum[i] = wsum;
			}
			float frac = 1.0f / dataset.size();
			for(const_iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				for(size_type i=0;i<dataset.attributeCount();++i)
				{
					if( beg->x()[i] == example_type::missing() )
					{
						if( !dataset.attributeAt(i).isnominal() )
						{
							sum[i] -= beg->weight(frac);
						}
					}
					else
					{
						if( dataset.attributeAt(i).isnominal() )
						{
							count[i][size_type(beg->x()[i])]+= beg->weight(frac);
						}
						else
						{
							count[i][0] += beg->weight(frac) * beg->x()[i];
						}
					}
				}
			}
			for(size_type i=0;i<dataset.attributeCount();++i)
			{
				if( dataset.attributeAt(i).isnominal() )
				{
					cuts[i] = max_index(count[i]);
				}
				else
				{
					cuts[i] = divide(count[i][0], sum[i]);
				}
			}
		}
		/** Replace missing values in the dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		void replace_missing(concrete_set& dataset)
		{
			for(iterator beg=dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				for(size_type i=0;i<dataset.attributeCount();++i)
				{
					if( beg->x()[i] == example_type::missing() ) beg->x()[i] = cuts[i];
				}
			}
		}
		/** Sum weights over examples.
		 * 
		 * @param beg start of example collection.
		 * @param end end of example collection.
		 * @return total weight.
		 */
		feature_type sum_weights(iterator beg, iterator end)
		{
			feature_type w = 0.0f, frac=1.0f/ std::distance(beg, end);
			for(;beg != end;++beg) w += beg->weight(frac);
			return w;
		}
		/** Get index of maximum value in array.
		 * 
		 * @param ar an array.
		 * @return index of maximum value.
		 */
		size_type max_index(const feature_vector& ar)
		{
			feature_type m=0.0f;
			size_type bst=0;
			for(size_type i=0;i<ar.size();++i)
			{
				if( ar[i] > m )
				{
					m = ar[i];
					bst=i;
				}
			}
			return bst;
		}
		
	public:
		/** Reads an discreet model from the input stream.
		 *
		 * @param in an input stream.
		 * @param filter a filter.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, attribute_missing& filter)
		{
			size_type n;
			in >> n;
			filter.cuts.resize(n);
			for(size_type i=0;i<n;++i)
			{
				if( in.get() != ',' ) return base_format_utility::fail(in, "Missing comma");
				in >> filter.cuts[i];
			}
			if( in.get() != '\n' ) return base_format_utility::fail(in, "Missing newline after row");
			return in;
		}
		/** Writes an discreet model to the output stream.
		 *
		 * @param out an output stream.
		 * @param filter a filter.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const attribute_missing& filter)
		{
			out << filter.cuts.size();
			for(size_type i=0;i<filter.cuts.size();++i)
			{
				out << "," << filter.cuts[i];
			}
			out << "\n";
			return out;
		}
		
	private:
		feature_vector cuts;
	};
};

#endif

