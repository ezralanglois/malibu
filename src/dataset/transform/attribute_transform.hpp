/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * attribute_transform.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ATTRIBUTE_TRANSFORM_HPP
#define _EXEGETE_ATTRIBUTE_TRANSFORM_HPP
#include "attribute_discreet.hpp"
#include "attribute_binary.hpp"
#include "attribute_normalization.hpp"
#include "attribute_column.hpp"
#include "attribute_missing.hpp"


/** @file attribute_transform.hpp
 * @brief Transforms attributes
 * 
 * This file contains an interface to transform attributes in a dataset.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/**@brief Transforms attributes
	 * 
	 * This class transforms attributes in a dataset.
	 * 
	 * @todo add read and write state
	 */
	template<class X, class Y>
	class attribute_transform
	{
	public:
		/** Defines a concrete example set. **/
		typedef ConcreteExampleSet<X,Y>					concrete_set;
		
	public:
		/** Constructs an attribute transform.
		 */
		attribute_transform() : modeCh('p'), errorInt(1)
		{
		}
		/** Destructs an attribute transform.
		 */
		~attribute_transform()
		{
		}
		
	public:
		/** Initalizes the transform.
		 *
		 * @param map an argument map.
		 * @param t a flag.
		 */
		template<class U>
		void init(U& map, int t)
		{
			arginit(map, "Attribute Transform");
			arginit(map, modeCh, 	"mode",		"mode of transform>Print:p;Distance:d;Discreet:i;Binary:b;Remove:r;Format:f");
			arginit(map, errorInt, 	"onerror", 	"should exit on error?");
			discreet.init(map, t);
			binary.init(map, t);
			norm.init(map, t);
			column.init(map, t);
			missing.init(map, t);
		}
		/** Transforms a dataset.
		 * 
		 * @param dataset a reference to concrete example set.
		 */
		const char* transform(concrete_set& dataset)
		{
			     if( modeCh == 'd') return distance_transform(dataset);
			else if( modeCh == 'i') return discreet_transform(dataset);
			else if( modeCh == 'b') return binary_transform(dataset);
			else if( modeCh == 'r') return column_transform(dataset);
			else if( modeCh == 'p') dataset.clear();
			return 0;
		}
		/** Get extension for transform.
		 * 
		 * @return a file extension.
		 */
		std::string extension()const
		{
			     if( modeCh == 'd') return "dst";
			else if( modeCh == 'i') return "int";
			else if( modeCh == 'b') return "bin";
			else if( modeCh == 'r') return "col";
			return "";
		}
		
	private:
		/** 
		 */
		const char* distance_transform(concrete_set& dataset)
		{
			const char* msg;
			if( dataset.isnormalized() && !dataset.hasnominal() && dataset.missing() == 0 ) 
			{
				if(!errorInt) return 0;
				return ERRORMSG("No columns to make normal");
			}
			if( !dataset.isnormalized() && (msg=norm.transform(dataset)) != 0) return msg;
			binary.only_nominal(true);
			if( dataset.hasnominal() && (msg=binary.transform(dataset)) != 0) return msg;
			if(dataset.missing() > 0 && (msg=missing.transform(dataset)) != 0 ) return msg;
			return 0;
		}
		/**
		 */
		const char* discreet_transform(concrete_set& dataset)
		{
			if( dataset.onlynominal() ) 
			{
				if(!errorInt) return 0;
				return ERRORMSG("No columns to discreetize");
			}
			return discreet.transform(dataset);
		}
		/**
		 */
		const char* binary_transform(concrete_set& dataset)
		{
			const char* msg;
			if( dataset.onlybinary() ) 
			{
				if(!errorInt) return 0;
				return ERRORMSG("No columns to make binary");
			}
			binary.only_nominal(false);
			if(dataset.hasnumeric() && (msg=discreet.transform(dataset)) != 0 ) return msg;
			if((msg=binary.transform(dataset)) != 0 ) return msg;
			return 0;
		}
		/**
		 */
		const char* column_transform(concrete_set& dataset)
		{
			return column.transform(dataset);
		}
		
	private:
		attribute_discreet<X,Y> discreet;
		attribute_binary<X,Y> binary;
		attribute_normalization<X,Y> norm;
		attribute_column<X,Y> column;
		attribute_missing<X,Y> missing;
		
	private:
		char modeCh;
		int errorInt;
	};
};

#endif

