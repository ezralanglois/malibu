/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * alphabet.hpp
 * Copyright (C) 2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_DOMAIN_ALPHABET_H
#define _EXEGETE_DOMAIN_ALPHABET_H
#include "symbol.h"
#include "stringutil.h"
#include "errorutil.h"

/** @file alphabet.hpp
 * @brief Defines a alphabet of symbols
 * 
 * This file contains the alphabet class.
 *
 * @author Robert Ezra Langlois (rlangl1@uic.edu)
 * @version 1.0
 */

namespace exegete
{
namespace domain
{
	/** @brief Defines a alphabet of symbols
	 * 
	 * This class template defines an alphabet of biological symbols.
	 */
	template<class T>
	class alphabet : public std::vector< symbol<T> >
	{
	public:
		/** Defines a symbol as a symbol type. **/
		typedef symbol<T> symbol_type;
		/** Defines a symbol hash type as a hash type. **/
		typedef typename symbol_type::hash_type hash_type;
		/** Defines a symbol string type as a string type. **/
		typedef typename symbol_type::string_type string_type;
		/** Defines a symbol property type as a property type. **/
		typedef typename symbol_type::property_type property_type;
	private:
		typedef std::vector< symbol_type > parent_type;
		typedef std::vector< string_type > string_vector;
	public:
		/** Defines a parent size type as a size type. **/
		typedef typename parent_type::size_type size_type;
		/** Defines a parent value type as a value type. **/
		typedef typename parent_type::value_type value_type;
		/** Defines a parent iterator as an iterator. **/
		typedef typename parent_type::iterator iterator;
		/** Defines a parent constant iterator as a constant iterator. **/
		typedef typename parent_type::const_iterator const_iterator;
		/** Defines a parent reference as a reference. **/
		typedef typename parent_type::reference reference;
		/** Defines a parent constant reference as a constant reference. **/
		typedef typename parent_type::const_reference const_reference;
		/** Defines a parent constant pointer as a constant pointer. **/
		typedef typename parent_type::const_pointer const_pointer;
		/** Defines a parent pointer as a pointer. **/
		typedef typename parent_type::pointer pointer;

	public:
		/** Constructs an alphabet.
		 */
		alphabet() : errmsg(0)
		{
		}
		/** Destructs an alphabet.
		 */
		~alphabet()
		{
		}

	public:
		/** Gets an index of the symbol.
		 *
		 * @param n pointer to a symbol.
		 * @return index of symbol.
		 */
		size_type indexOf(const_pointer n)const
		{
			return size_type( std::distance(&(*parent_type::begin()), n) );
		}
		/** Gets an index of the hash.
		 *
		 * @param n a hash value.
		 * @return index of hash.
		 */
		size_type indexOf(hash_type n)const
		{
			return size_type( std::distance(parent_type::begin(), findi(n)) );
		}
		/** Gets a constant pointer to symbol for given hash.
		 *
		 * @param n a hash value.
		 * @return pointer to a symbol.
		 */
		const_pointer find(hash_type n)const
		{
			return &*std::find(parent_type::begin(), parent_type::end(), n);
		}
		/** Gets a constant pointer to a symbol for a given code.
		 *
		 * @param n a string code.
		 * @return pointer to a symbol.
		 */
		const_pointer find(string_type n)const
		{
			return &*std::find_first_of(parent_type::begin(), parent_type::end(), &n, (&n)+1, bycode);
		}
		/** Gets a constant pointer to a symbol for a given name.
		 *
		 * @param n a string name.
		 * @return pointer to a symbol.
		 */
		const_pointer findByName(string_type n)const
		{
			return &*std::find_first_of(parent_type::begin(), parent_type::end(), &n, (&n)+1, byname);
		}
		/** Gets a property name for an index.
		 * 
		 * @param n an index.
		 * @return a property string name.
		 */
		const string_type& propertyName(size_type n)const
		{
			ASSERT(n < propertyNames.size());
			return propertyNames[n];
		}
		/** Gets an index for a specific property name ignoring case.
		 *
		 * @param name a property name.
		 * @return an index to a property.
		 */
		size_type indexForProperty(const string_type& name)const
		{
			unsigned int i=0;
			for(;i<propertyNames.size();++i)	
				if( stricmpn(name.c_str(), propertyNames[i].c_str(), (unsigned int)name.length()) ) return i-3;
			return i;
		}
		/** Gets the number of properties.
		 *
		 * @return the number of properties.
		 */
		size_type propertySize()const
		{
			return propertyNames.size();
		}
		/** Tests if the symbol is unknown (or the last symbol).
		 *
		 * @param n a symbol to test
		 * @return true if the symbol is unknown.
		 */
		bool is_unknown(const_pointer n)const
		{
			return indexOf(n) == (parent_type::size()-1);
		}
		/** Tests if the symbol hash is unknown (or the last symbol).
		 *
		 * @param n a symbol hash to test
		 * @return true if the symbol is unknown.
		 */
		bool is_unknown(hash_type n)const
		{
			return indexOf(n) == (parent_type::size()-1);
		}
		/** Gets an error message.
		 *
		 * @return an error message otherwise NULL.
		 */
		const char* errormsg()const
		{
			return errmsg;
		}

	private:
		const_iterator findi(hash_type n)const
		{
			return std::find(parent_type::begin(), parent_type::end(), n);
		}
		static bool bycode(const symbol_type& sym, const string_type& code)
		{
			return sym.code() == code;
		}
		static bool byname(const symbol_type& sym, const string_type& name)
		{
			return sym.name() == name;
		}

	protected:
		/** Reads an alphabet from the input stream.
		 *
		 * @param in a reference to an input stream.
		 * @param alpha a reference to an alphabet.
		 * @return a reference to an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, alphabet& alpha)
		{
			symbol_type sym;
			std::string line;
			char hash;
			std::getline(in, line);
			if( !stringToValue(line, alpha.propertyNames, ",") )
			{
				alpha.errmsg = ERRORMSG("Error parsing alphabet header" );
				in.setstate( std::ios::failbit );
				return in;
			}
			while(!in.eof())
			{
				std::getline(in, line, ',');
				sym.name(line);
				if( in.eof() )
				{
					alpha.errmsg = ERRORMSG("Error parsing symbol name" );
					in.setstate( std::ios::failbit );
					return in;
				}
				std::getline(in, line, ',');
				sym.code(line);
				if( in.eof() )
				{
					alpha.errmsg = ERRORMSG("Error parsing symbol code" );
					in.setstate( std::ios::failbit );
					return in;
				}
				std::getline(in, line, ',');
				if( in.eof() || !stringToValue(line, hash) || line.find('\n') != std::string::npos )
				{
					alpha.errmsg = ERRORMSG("Error parsing symbol hash" );
					in.setstate( std::ios::failbit );
					return in;
				}
				sym.hash(hash);
				std::getline(in, line);
				if( !stringToValue(line, sym.properties(), ",") )
				{
					alpha.errmsg = ERRORMSG("Error parsing symbol properties" );
					in.setstate( std::ios::failbit );
					return in;
				}
				alpha.push_back(sym);
			}
			std::stable_sort(alpha.begin(), alpha.end());
			return in;
		}
		/** Writes an alphabet to the output stream. Does nothing so far.
		 *
		 * @param out a reference to an output stream.
		 * @param alpha a constant reference to an alphabet.
		 * @return a reference to an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const alphabet& alpha)
		{
			alpha.errmsg = ERRORMSG("Writing alphabet not supported!");
			return out;
		}

	private:
		string_vector propertyNames;
		mutable const char* errmsg;
	};


};
};

#endif

