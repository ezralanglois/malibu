/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * sequence.hpp
 * Copyright (C) 2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_DOMAIN_SEQUENCE_H
#define _EXEGETE_DOMAIN_SEQUENCE_H
#include "symbol.h"
#include "stringutil.h"
#include "errorutil.h"

/** @file sequence.hpp
 * @brief Defines a sequence of symbols
 * 
 * This file contains the sequence class.
 *
 * @author Robert Ezra Langlois (rlangl1@uic.edu)
 * @version 1.0
 */

namespace exegete
{
namespace domain
{
	/** @brief Defines a sequence of symbols
	 * 
	 * This class template defines an sequence of biological symbols.
	 */
	template<class T>
	class sequence : public std::vector< const symbol<T>* >
	{
		typedef std::vector< const symbol<T>* > parent_type;
	public:
		/** Defines a symbol type. **/
		typedef symbol<T> symbol_type;
		/** Defines a feature map. **/
		typedef std::map<std::string, std::string> feature_map;
	public:
		/** Constructs a sequence.
		 */
		sequence()
		{
		}
		/** Destructs a sequence.
		 */
		~sequence()
		{
		}
		
	public:
		/** Get the feature map.
		 * 
		 * @return a feature map.
		 */
		feature_map& featuremap()
		{
			return featmap;
		}
		/** Get the feature map.
		 * 
		 * @return a feature map.
		 */
		const feature_map& featuremap()const
		{
			return featmap;
		}
		/** Clear sequence and feature map.
		 */
		void clear()
		{
			featmap.clear();
			parent_type::clear();
		}
		
	private:
		feature_map featmap;
	};
};
};


#endif



