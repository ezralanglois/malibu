/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * symbol.hpp
 * Copyright (C) 2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_DOMAIN_SYMBOL_H
#define _EXEGETE_DOMAIN_SYMBOL_H
#include <string>
#include <vector>

/** @file symbol.hpp
 * @brief Defines a symbol for an alphabet
 * 
 * This file contains the symbol for an alphabet.
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
namespace domain
{
	/** @brief Defines a symbol for an alphabet
	 * 
	 * This class template defines a biological symbol. This symbol has a 
	 * name, a code and a hash (single letter). It also contains a vector of
	 * of some property.
	 */
	template<class T>
	class symbol : public std::vector< T >
	{
		typedef std::vector<T> parent_type;
	public:
		/** Defines the template parameter as a property type. **/
		typedef T property_type;
		/** Defines a parent size type as a size type. **/
		typedef typename parent_type::size_type size_type;
		/** Defines a parent value type as a value type. **/
		typedef typename parent_type::value_type value_type;
		/** Defines a parent iterator as an iterator. **/
		typedef typename parent_type::iterator iterator;
		/** Defines a parent constant iterator as a constant iterator. **/
		typedef typename parent_type::const_iterator const_iterator;
		/** Defines a parent reference as a reference. **/
		typedef typename parent_type::reference reference;
		/** Defines a parent constant reference as a constant reference. **/
		typedef typename parent_type::const_reference const_reference;
	public:
		/** Defines an integer as a hash type. **/
		typedef int hash_type;
		/** Defines a std::string as a string type. **/
		typedef std::string string_type;

	public:
		/** Constructs a symbol with a null hash code.
		 */
		symbol() : hashInt(0)
		{
		}
		/** Destructs a symbol.
		 */
		~symbol()
		{
		}

	public:
		/** Implicitly converts a symbol to a hash type.
		 *
		 * @return a hash type.
		 */
		operator hash_type()const
		{
			return hashInt;
		}
		/** Tests if this symbol's hash is less than or equal to the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is less than or equal to the argument.
		 */
		bool operator<=(const symbol& sym)const
		{
			return hashInt <= sym.hashInt;
		}
		/** Tests if this symbol's hash is greater than or equal to the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is greater than or equal to the argument.
		 */
		bool operator>=(const symbol& sym)const
		{
			return hashInt >= sym.hashInt;
		}
		/** Tests if this symbol's hash is less than the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is less than the argument.
		 */
		bool operator<(const symbol& sym)const
		{
			return hashInt < sym.hashInt;
		}
		/** Tests if this symbol's hash is greater than the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is greater than the argument.
		 */
		bool operator>(const symbol& sym)const
		{
			return hashInt > sym.hashInt;
		}
		/** Tests if this symbol's hash is equal to the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is equal to the argument.
		 */
		bool operator==(const symbol& sym)const
		{
			return hashInt == sym.hashInt;
		}
		/** Tests if this symbol's hash is not equal to the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is not equal to the argument.
		 */
		bool operator!=(const symbol& sym)const
		{
			return hashInt != sym.hashInt;
		}

	public:
		/** Tests if this symbol's hash is less than or equal to the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is less than or equal to the argument.
		 */
		bool operator<=(const symbol* sym)const
		{
			return hashInt <= sym->hashInt;
		}
		/** Tests if this symbol's hash is greater than or equal to the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is greater than or equal to the argument.
		 */
		bool operator>=(const symbol* sym)const
		{
			return hashInt >= sym->hashInt;
		}
		/** Tests if this symbol's hash is less than the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is less than the argument.
		 */
		bool operator<(const symbol* sym)const
		{
			return hashInt < sym->hashInt;
		}
		/** Tests if this symbol's hash is greater than the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is greater than the argument.
		 */
		bool operator>(const symbol* sym)const
		{
			return hashInt > sym->hashInt;
		}
		/** Tests if this symbol's hash is equal to the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is equal to the argument.
		 */
		bool operator==(const symbol* sym)const
		{
			return hashInt == sym->hashInt;
		}
		/** Tests if this symbol's hash is not equal to the argument.
		 *
		 * @param sym a symbol to compare.
		 * @return true if this symbol's has is not equal to the argument.
		 */
		bool operator!=(const symbol* sym)const
		{
			return hashInt != sym->hashInt;
		}

	public:
		/** Tests if this symbol's hash is less than the argument.
		 *
		 * @param h a hash to compare.
		 * @return true if this symbol's has is less than the argument.
		 */
		bool operator<=(hash_type h)const
		{
			return hashInt <= h;
		}
		/** Tests if this symbol's hash is greater than or equal to the argument.
		 *
		 * @param h a hash to compare.
		 * @return true if this symbol's has is greater than or equal to the argument.
		 */
		bool operator>=(hash_type h)const
		{
			return hashInt >= h;
		}
		/** Tests if this symbol's hash is less than the argument.
		 *
		 * @param h a hash to compare.
		 * @return true if this symbol's has is less than the argument.
		 */
		bool operator<(hash_type h)const
		{
			return hashInt < h;
		}
		/** Tests if this symbol's hash is greater than the argument.
		 *
		 * @param h a hash to compare.
		 * @return true if this symbol's has is greater than the argument.
		 */
		bool operator>(hash_type h)const
		{
			return hashInt > h;
		}
		/** Tests if this symbol's hash is equal to the argument.
		 *
		 * @param h a hash to compare.
		 * @return true if this symbol's has is equal to the argument.
		 */
/*		bool operator==(hash_type h)const
		{
			return hashInt == h;
		}*/
		/** Tests if this symbol's hash is not equal to the argument.
		 *
		 * @param h a hash to compare.
		 * @return true if this symbol's has is not equal to the argument.
		 */
		bool operator!=(hash_type h)const
		{
			return hashInt != h;
		}

	public:
		/** Sets the name of the symbol.
		 *
		 * @param val a string name.
		 */
		void name(const string_type& val)
		{
			nameStr = val;
		}
		/** Gets the name of the symbol.
		 *
		 * @return a string name.
		 */
		const string_type& name()const
		{
			return nameStr;
		}
		/** Sets the code of the symbol.
		 *
		 * @param val a string code.
		 */
		void code(const string_type& val)
		{
			codeStr = val;
		}
		/** Gets the code of the symbol.
		 *
		 * @return a string code.
		 */
		const string_type& code()const
		{
			return codeStr;
		}
		/** Sets the hash of the symbol.
		 *
		 * @param val a string hash.
		 */
		void hash(const hash_type val)
		{
			hashInt = val;
		}
		/** Gets the hash of the symbol.
		 *
		 * @return a string hash.
		 */
		const hash_type hash()const
		{
			return hashInt;
		}
		/** Gets a reference to the parent property vector.
		 *
		 * @return a reference to a property vector.
		 */
		parent_type& properties()
		{
			return *this;
		}
		/** Gets a constant reference to the parent property vector.
		 *
		 * @return a constant reference to a property vector.
		 */
		const parent_type& properties()const
		{
			return *this;
		}

	private:
		string_type nameStr;
		string_type codeStr;
		hash_type hashInt;
	};

};
};

#endif


