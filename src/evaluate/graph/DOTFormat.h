/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * DOTFormat.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_DOTFORMAT_H
#define _EXEGETE_DOTFORMAT_H
#include "GraphFormat.h"

/** @file DOTFormat.h
 * @brief Writes a graph in the DOT format.
 * 
 * This file contains the DOTFormat class.
 *
 * @ingroup ExegeteGraph
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Writes a graph in the DOT format.
	 * 
	 * This class defines the Graphviz DOT format. 
	 */
	class DOTFormat : public GraphFormat
	{
		typedef std::vector<long> id_vector;
		typedef std::vector<id_vector> rank_vector;
		typedef std::vector<std::string> string_vector;
		typedef std::map<std::string, unsigned int> count_map;
		typedef rank_vector::const_iterator const_rank_iterator;
		typedef id_vector::const_iterator const_id_iterator;
	public:
		/** Constructs a DOT format.
		 * 
		 * @param f a graph formater.
		 */
		DOTFormat(GeneralGraphFormat& f) : GraphFormat(f), depthInt(0), useCount(false)
		{
		}
		/** Destructs a DOT format.
		 */
		virtual ~DOTFormat()
		{
		}
		
	public:
		/** Gets the name of the format.
		 * 
		 * @return name of the format.
		 */
		std::string name()const
		{
			return "DOT";
		}
	
	protected:
		/** Writes the header of the graph file in the DOT format.
		 * 
		 * @param out an output stream.
		 * @param graph a source graph.
		 */
		void write_header(std::ostream& out, const Graph& graph)const
		{
			out << "digraph \"adt\" {\n";
			out << "nodesep = \"0.05\"\n";
			out << "margin = \"0.05\"\n";
			out << "ranksep=0.1\n";
			ranks.clear();
			setupGroup(graph);
			depthInt = countmap.size();

			if(graph.exampleCount() > countmap.size() )
			{
				useCount=true;
				out << "node_toc [ label=";
				out << "< <TABLE BORDER=\"0\">\n";
				out << "<TR><TD align=\"left\"><FONT COLOR=\"black\">Total</FONT></TD>\n";
				out << "<TD><FONT COLOR=\"black\"> </FONT></TD></TR>\n";
				for(count_map::const_iterator beg = countmap.begin(), end=countmap.end();beg != end;++beg)
				{
					if( htmluse(beg->first) )
					{
						out << "<TR><TD align=\"left\"><FONT COLOR=\"" << htmlcolor(beg->first) << "\">";
						out << beg->first << "</FONT></TD>\n";
						out << "<TD><FONT COLOR=\"" << htmlcolor(beg->first) << "\">";
						out << beg->second << "</FONT></TD></TR>\n";
					}
				}
				out << "</TABLE> >";
				out << ",fontsize=14,shape=plaintext,margin=0,0";
				out << "];\n";
			}
		}
		/** Writes the footer of the graph file in the DOT format.
		 * 
		 * @param out an output stream.
		 * @param graph a source graph.
		 */
		void write_footer(std::ostream& out, const Graph& graph)const
		{
			for(const_rank_iterator rbeg=ranks.begin(), rend=ranks.end();rbeg != rend;++rbeg)
			{
				if( rbeg->size() > 1 )
				{
					out << "{rank=same;";
					if( (rbeg+1) == rend ) out << " node_toc";
					for(const_id_iterator ibeg=rbeg->begin(), iend=rbeg->end();ibeg != iend;++ibeg)
						out << " node_" << *ibeg;
					out << "}\n";
				}
			}
			out << "}\n";
		}
		/** Writes a node for a graph in the DOT format.
		 * 
		 * @param out an output stream.
		 * @param node a graph node.
		 */
		void write_node(std::ostream& out, const GraphNode& node)const
		{
			if( ((unsigned int)node.rank()) >= ranks.size() ) ranks.resize(node.rank()+1);
			ranks[node.rank()].push_back(node.id());
			setupCount(countmap.begin(), countmap.end());
			out << "node_" << node.id() << " [ label=";
			if( node.property() == Graph::HTML )
			{
				string_vector names, ids;
				std::string str = node.label();
				stringToValue(str, names, "\n");
				unsigned int i=0;
				unsigned int total=0;
				{
					count_map::iterator it;
					std::string grp;
					for(i=0;i<names.size();++i)
					{
						grp = group(names[i]);
						if( !grp.empty() )
						{
							it = countmap.insert(countmap.begin(), std::make_pair(grp,0u));
							it->second++;
						}
					}
					names.resize(countmap.size()+int(useCount));
					ids.resize(countmap.size()+int(useCount));
					if(useCount) 
					{
						ids[0] = "Total";
					}
					
					for(it=countmap.begin(), i=int(useCount);it != countmap.end();++it,++i)
					{
						if(useCount) 
						{
							valueToString(it->second, names[i]);
							total+=it->second;
						}
						else names[i] = it->first;
						ids[i] = it->first;
					}
				}
				if(useCount) 
				{
					valueToString(total, names[0]);
				}
				out << "< <TABLE BORDER=\"0\">\n";
				for(i=0;i<names.size();++i)
				{
					if( htmluse(ids[i]) )
					{
						out << "<TR><TD><FONT COLOR=\"" << htmlcolor(ids[i]) << "\">";
						out << names[i] << "</FONT></TD></TR>\n";
					}
				}
				for(;i<depthInt;++i) out << "<TR><TD><FONT COLOR=\"white\">.</FONT></TD></TR>\n";
				out << "</TABLE> >";
				out << ",fontsize=14,shape=plaintext,margin=0,0";
			}
			else
			{
				out << "\"" << GraphFormat::flt2str(node.label()) << "\",";
				out << fontsize() << ",";
				if( node.conserved() == 0.0f )
				{
					out << ncolor(node.property()+1) << ",";
				}
				else
				{
					out << ccolor(node.conserved()) << ",";
				}
				out << nshape(node.property()+1);
			}
			out << "];\n";
		}
		/** Writes an edge for a graph in the DOT format.
		 * 
		 * @param out an output stream.
		 * @param edge a graph edge.
		 */
		void write_edge(std::ostream& out, const GraphEdge& edge)const
		{
			out << "node_" << edge.fromID() << " -> " << "node_" << edge.toID();
			if( !edge.label().empty() )
			{
				out << " [ label=\"" << GraphFormat::flt2str(edge.label()) << "\",";
				out << fontsize() << ",";
				out << "color=black";
				out << "]";
			}
			out << ";\n";
		}
		
	private:
		std::string fontsize()const
		{
			std::string line;
			valueToString(GraphFormat::general().fontSize(), line);
			line = "fontsize="+line;
			return line;
		}
		std::string nshape(unsigned int n)const
		{
			ASSERT(n < GraphFormat::general().nshapes().size());
			std::string line = "shape=" + GraphFormat::general().nshapes()[n];
			return line;
		}
		std::string ncolor(unsigned int n)const
		{
			ASSERT(n < GraphFormat::general().ncolors().size());
			std::string line = "color=" + GraphFormat::general().ncolors()[n];
			return line;
		}
		std::string ccolor(float c)const
		{
			std::string line = "color=" + GraphFormat::general().conserved(c);
			return line;
		}
		std::string htmlcolor(const std::string& cl)const
		{
			return GraphFormat::general().htmlcolor(cl);
		}
		bool htmluse(const std::string& cl)const
		{
			return GraphFormat::general().htmluse(cl);
		}
		std::string group(const std::string& id)const
		{
			return GraphFormat::general().group(id);
		}
		void setupGroup(const Graph& graph)const
		{
			return GraphFormat::general().setupGroup(graph, countmap);
		}
		static void setupCount(count_map::iterator beg, count_map::iterator end)
		{
			for(;beg != end;++beg) beg->second = 0;
		}
		
	private:
		mutable count_map countmap;
		mutable rank_vector ranks;
		mutable unsigned int depthInt;
		mutable bool useCount;
	};
};


#endif


