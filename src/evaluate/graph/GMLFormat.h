/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * GMLFormat.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_GMLFORMAT_H
#define _EXEGETE_GMLFORMAT_H
#include "GraphFormat.h"

/** @file GMLFormat.h
 * @brief --Implement--Writes a graph in the GML format.
 * 
 * This file contains the GMLFormat class.
 *
 * @ingroup ExegeteGraph
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief --Implement--Writes a graph in the GML format.
	 * 
	 * This class writes a graph in the GML format.
	 * 
	 * @todo Implement this class.
	 */
	class GMLFormat : public GraphFormat
	{
	public:
		/** Constructs a GML format.
		 * 
		 * @param f a graph formater.
		 */
		GMLFormat(GeneralGraphFormat& f) : GraphFormat(f)
		{
		}
		/** Destructs a GML format.
		 */
		virtual ~GMLFormat()
		{
		}
		
	public:
		/** Gets the name of the format.
		 * 
		 * @return GML
		 */
		std::string name()const
		{
			return "GML";
		}
		
	protected:
		/** Writes the header of the graph file in the GML format.
		 * 
		 * @param out an output stream.
		 * @param graph a source graph.
		 */
		void write_header(std::ostream& out, const Graph& graph)const
		{
			out << "Creator \"malibu\"\n";
			out << "Version 1.0\n";
			out << "graph\n[ hierarchic 1\n";
			out << "directed 1\n";
		}
		/** Writes the footer of the graph file in the GML format.
		 * 
		 * @param out an output stream.
		 * @param graph a source graph.
		 */
		void write_footer(std::ostream& out, const Graph& graph)const
		{
			out << "]\n";
		}
		/** Writes a node for a graph in the GML format.
		 * 
		 * @param out an output stream.
		 * @param node a graph node.
		 */
		void write_node(std::ostream& out, const GraphNode& node)const
		{
			out << "node\n[ id " << node.id() << "\n";
			out << "LabelGraphics\n[";
			out << "text \"" << node.label() << "\" ]\n";
			out << "]\n";
		}
		/** Writes an edge for a graph. in the GML format
		 * 
		 * @param out an output stream.
		 * @param edge a graph edge.
		 */
		void write_edge(std::ostream& out, const GraphEdge& edge)const
		{
			out << "edge\n[ source " << edge.fromID() << "\n";
			out << "target " << edge.toID() << "\n";
			out << "graphics\n[ Line\n]\n";
			out << "]\n";			
		}
	};
};

#endif


