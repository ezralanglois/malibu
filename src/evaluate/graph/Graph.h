/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Graph.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_GRAPH_H
#define _EXEGETE_GRAPH_H
#include "GraphNode.h"
#include <vector>
#include <map>

/** @file Graph.h
 * @brief Defines a graph structure.
 * 
 * This file contains the Graph class.
 *
 * @ingroup ExegeteGraph
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @defgroup ExegeteGraph Graph System
 *  This group holds all the files related to graphs.
 */

namespace exegete
{
	/** @brief Defines a graph structure.
	 * 
	 * This class defines a graph structure.
	 */
	class Graph
	{
		typedef std::map<long, GraphNode> node_map;
		typedef std::vector<GraphEdge> edge_vector;
		typedef std::map<std::string, unsigned int> count_map;
		typedef std::vector<count_map> count_map_vector;
	public:;
		/** Defines an count iterator **/
		typedef count_map::iterator count_iterator;
		/** Defines an edge iterator **/
		typedef edge_vector::const_iterator const_edge_iterator;
		/** Defines a node iterator **/
		typedef node_map::const_iterator const_node_iterator;
		/** Defines a node iterator **/
		typedef node_map::iterator node_iterator;
		/** Defines node types **/
		enum{ LEAF=-1, REAL=0, NOMINAL=1, SUBSET=2, HTML=3 };
		/** Defines edge types **/
		enum{ EDGE_LEAF=0, EDGE_REAL=1, EDGE_NOMINAL=2, EDGE_SUBSET=3 };
		
	public:
		/** Constructs a graph.
		 */
		Graph() : exampleCountInt(0)
		{
		}
		/** Destructs a graph.
		 */
		~Graph()
		{
		}
		
	public:
		/** Get a node for a unique identifier.
		 * 
		 * @param val a unique identifier.
		 * @return a reference to a graph node.
		 */
		GraphNode& get(long val)
		{
			ASSERTMSG(nodes.find(val) != nodes.end(), val);
			return nodes[val];
		}
		/** Get a node for a unique identifier.
		 * 
		 * @param val a unique identifier.
		 * @return a reference to a graph node.
		 */
		GraphNode& operator[](long val)
		{
			ASSERTMSG(nodes.find(val) != nodes.end(), val);
			return nodes[val];
		}
		/** Get a number of nodes.
		 * 
		 * @return number of nodes.
		 */
		unsigned int nodeCount()const
		{
			return nodes.size();
		}
		/** Test if the node is a leaf.
		 * 
		 * @param node a graph node.
		 * @return true if the node is a leaf.
		 */
		bool isleaf(const GraphNode& node)const
		{
			for(const_edge_iterator beg = edges.begin(), end=edges.end();beg != end;++beg)
				if( beg->fromID() == node.id() ) return false;
			return true;
		}
		
	public:
		/** Get the highest rank.
		 * 
		 * @return highest rank.
		 */
		long highestRank()const
		{
			long rank=0;
			for(const_node_iterator beg = nodes.begin(), end=nodes.end();beg != end;++beg)
				rank = std::max(beg->second.rank(), rank);
			return rank;
		}
		/** Add a node to the graph.
		 * 
		 * @param id a unique identifier of the node.
		 * @param label a label for the node.
		 * @param r the rank of the node.
		 * @param p a property index.
		 */
		void addnode(long id, const std::string& label, long r, unsigned int p=0)
		{
			nodes.insert(std::make_pair(id, GraphNode(label, id, r, p)));
		}
		/** Add an edge to the graph.
		 * 
		 * @param from a unique identifier to a node starting an edge.
		 * @param to a unique identifier to a node ending an edge.
		 * @param label a label for an edge.
		 * @param p a property index.
		 */
		void addedge(long from, long to, const std::string& label="", unsigned int p=0)
		{
			edges.push_back(GraphEdge(from, to, label, p));
		}
		/** Gets an iterator to the start of an edge collection.
		 * 
		 * @return iterator to the start of an edge collection.
		 */
		const_edge_iterator edge_begin()const
		{
			return edges.begin();
		}
		/** Gets an iterator to the end of an edge collection.
		 * 
		 * @return iterator to the end of an edge collection.
		 */
		const_edge_iterator edge_end()const
		{
			return edges.end();
		}
		/** Gets an iterator to the start of a node collection.
		 * 
		 * @return iterator to the start of a node collection.
		 */
		const_node_iterator node_begin()const
		{
			return nodes.begin();
		}
		/** Gets an iterator to the end of a node collection.
		 * 
		 * @return iterator to the end of a node collection.
		 */
		const_node_iterator node_end()const
		{
			return nodes.end();
		}
		/** Gets an iterator to the start of a node collection.
		 * 
		 * @return iterator to the start of a node collection.
		 */
		node_iterator node_begin()
		{
			return nodes.begin();
		}
		/** Gets an iterator to the end of a node collection.
		 * 
		 * @return iterator to the end of a node collection.
		 */
		node_iterator node_end()
		{
			return nodes.end();
		}
		/** Get a group map at a specific index.
		 * 
		 * @param n an index.
		 * @return a map counting the number of a specific id.
		 */
		const count_map& group(unsigned int n)const
		{
			ASSERT(n < groupVec.size());
			return groupVec[n];
		}
		/** Get a group at a specific index.
		 * 
		 * @param n an index.
		 * @return a map counting the number of a specific id.
		 */
		count_map& group(unsigned int n)
		{
			ASSERT(n < groupVec.size());
			return groupVec[n];
		}
		/** Get number of groups.
		 * 
		 * @return number of groups.
		 */
		unsigned int groupCount()const
		{
			return (unsigned int)groupVec.size();
		}
		/** Resize the number of groups.
		 * 
		 * @param n number of groups.
		 */
		void resize_groups(unsigned int n)
		{
			groupVec.resize(n);
		}
		/** Gets the number of examples.
		 * 
		 * @return the number of examples.
		 */
		unsigned int exampleCount()const
		{
			return exampleCountInt;
		}
		/** Sets the number of examples
		 * 
		 * @param n the number of examples.
		 */
		void exampleCount(unsigned int n)
		{
			exampleCountInt = n;
		}
		
	private:
		node_map nodes;
		edge_vector edges;
	private:
		count_map_vector groupVec;
		unsigned int exampleCountInt;
	};
};

#endif


