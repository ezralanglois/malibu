/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * GraphFormat.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_GRAPHFORMAT_H
#define _EXEGETE_GRAPHFORMAT_H
#include "Graph.h"
#include <iomanip>
#include "typeutil.h"

/** @file GraphFormat.h
 * @brief Defines utility classes for a graph format.
 * 
 * This file contains the GraphFormat class, the GeneralGraphFormat class.
 *
 * @ingroup ExegeteGraph
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a general graph format.
	 * 
	 * This class defines a general graph format.
	 */
	class GeneralGraphFormat
	{
		typedef ArgumentList arglist_type;
		typedef std::vector<std::string> string_vector;
		typedef std::map<std::string, unsigned int> count_map;
		typedef std::vector< std::pair<float,std::string> > con_vector;
		typedef std::vector<std::pair<std::string,std::string> > stringpair_vector;
		typedef std::vector< string_vector > string_vector2d;
	public:
		/** Constructs a general graph format.
		 */
		GeneralGraphFormat() : posInt(0), fltPrecInt(3), fontSizeInt(14), 
							   shapeVec(4, "box"), colorVec(4, "black"), 
							   conVec(5), classColorVec(2)
		{
			shapeVec[0] = "ellipse";
			conVec[0] = std::make_pair(0.1f, "black");
			conVec[1] = std::make_pair(0.3f, "blue");
			conVec[2] = std::make_pair(0.5f, "green");
			conVec[3] = std::make_pair(0.7f, "yellow");
			conVec[4] = std::make_pair(0.9f, "orange");
			conVec[4] = std::make_pair(1.0f, "red");
			
			classColorVec[0] = std::make_pair(std::string("0"), std::string("black"));
			classColorVec[1] = std::make_pair(std::string("1"), std::string("red"));
		}
		
	public:
		/** Initialize an argument map with arguments that control the general graph format.
		 * 
		 * @param map an argument map.
		 */
		template<class U>
		void argumentmap(U& map)
		{
			arginit(map, fltPrecInt,  			"precision","precision of floating-point number, number of decimal places", ArgumentMap::ADDITIONAL);
			arginit(map, fontSizeInt, 			"fontsize", "font size of all elements", ArgumentMap::ADDITIONAL);
			arginit(map, shapeArg,    			"nshape", 	"shape for each node type", ArgumentMap::ADDITIONAL);
			arginit(shapeArg, shapeVec[0], 		"leaf", 	"shape of leaf node");
			arginit(shapeArg, shapeVec[1], 		"real", 	"shape of threshold decision node");
			arginit(shapeArg, shapeVec[2], 		"nominal", 	"shape of nominal decision node");
			arginit(shapeArg, shapeVec[3], 		"subset", 	"shape of subset decision node");
			arginit(map, colorArg, 				"ncolor", 	"color for each node type", ArgumentMap::ADDITIONAL);
			arginit(colorArg, colorVec[0], 		"leaf", 	"color of leaf node");
			arginit(colorArg, colorVec[1], 		"real", 	"color of threshold decision node");
			arginit(colorArg, colorVec[2], 		"nominal", 	"color of nominal decision node");
			arginit(colorArg, colorVec[3], 		"subset", 	"color of subset decision node");
			arginit(map, conVec, 				"conserved","a list of colors and conservation scores, format value:color,value2:color2", ArgumentMap::ADDITIONAL);
			arginit(map, idVec, 				"useonlyid","use only ids in list", ArgumentMap::ADDITIONAL);
			arginit(map, posInt, 				"idpos", 	"position of id", ArgumentMap::ADDITIONAL);
			arginit(map, classColorVec,	 		"groupcolor", "color of class, format class:color,class2:color2", ArgumentMap::ADDITIONAL);
		}
		
	public:
		/** Gets the precision of a floating-point number.
		 * 
		 * @return floating-point precision.
		 */
		unsigned int floatPrecision()const
		{
			return fltPrecInt;
		}
		/** Gets the size of the font.
		 * 
		 * @return font size.
		 */
		unsigned int fontSize()const
		{
			return fontSizeInt;
		}
		/** Get shape vector.
		 * 
		 * @return shape vector.
		 */
		const string_vector& nshapes()const
		{
			return shapeVec;
		}
		/** Get shape vector.
		 * 
		 * @return shape vector.
		 */
		const string_vector& ncolors()const
		{
			return colorVec;
		}
		/** Get conserved color.
		 * 
		 * @param val a conserved value.
		 * @return color string.
		 */
		const std::string& conserved(float val)const
		{
			if(val>=1.0f) val = 0.999f;
			con_vector::const_iterator it;
			it = std::lower_bound(conVec.begin(), conVec.end(), val, pair_first_less<float,std::string>());
			ASSERT(!colorVec.empty());
			if( it == conVec.end() ) return colorVec.front();
			return it->second;
		}
		/** Sort the conserved vector.
		 */
		void sortConserved()
		{
			std::stable_sort(conVec.begin(), conVec.end());
		}
		/** Gets the html color.
		 * 
		 * @param id example id
		 * @return color of id.
		 */
		std::string htmlcolor(const std::string& id)const
		{
			if( !classColorVec.empty() )
			{
				std::string cl = group(id);
				stringpair_vector::const_iterator beg = classColorVec.begin();
				stringpair_vector::const_iterator end = classColorVec.end();
				for(;beg != end;++beg) if( beg->first == cl ) break;
				if( beg != end ) return beg->second;
			}
			return "black";
		}
		/** Test whether to add an id.
		 * 
		 * @param id example id
		 * @return color of id.
		 */
		bool htmluse(const std::string& id)const
		{
			if( idVec.empty() ) return true;
			return std::find(idVec.begin(), idVec.end(), id) != idVec.end();
		}
		/** Get name for an id.
		 * 
		 * @param id a identifier
		 * @return a group.
		 */
		std::string group(const std::string& id)const
		{
			string_vector ar;
			stringToValue(id, ar, ":");
			if( posInt >= ar.size() ) return id;
			return ar[posInt];
		}
		/** Setup group count map.
		 * 
		 * @param graph a graph
		 * @param m a count map.
		 */
		void setupGroup(const Graph& graph, count_map& m)const
		{
			if( graph.groupCount() == 0 ) return;
			unsigned int pos = (graph.groupCount() > posInt) ? posInt : graph.groupCount()-1;
			m = graph.group(pos);
		}
		
	private:
		int countgroupBool;
		unsigned int posInt;
		unsigned int fltPrecInt;
		unsigned int fontSizeInt;
		string_vector shapeVec;
		string_vector colorVec;
		string_vector idVec;
		con_vector conVec;
		stringpair_vector classColorVec;
	private:
		arglist_type shapeArg;
		arglist_type colorArg;
	};
	/** @brief Defines an abstract graph format.
	 * 
	 * This class defines an abstract graph format.
	 */
	class GraphFormat
	{
	public:
		/** Constructs a graph format.
		 * 
		 * @param f a general graph format.
		 */
		GraphFormat(GeneralGraphFormat& f) : genformat(&f)
		{
		}
		/** Destructs a graph format.
		 */
		virtual ~GraphFormat()
		{
		}
		
	public:
		/** Gets the name of the format.
		 * 
		 * @return name of format.
		 */
		virtual std::string name()const=0;
		/** Write a graph to an output stream.
		 * 
		 * @param out an output stream.
		 * @param graph a graph to write.
		 * @return an error message or NULL.
		 */
		const char* write(std::ostream& out, const Graph& graph)const
		{
			write_header(out, graph);

			for(Graph::const_node_iterator beg=graph.node_begin(), end=graph.node_end();beg != end;++beg)
			{
				write_node(out, beg->second);
			}
			for(Graph::const_edge_iterator beg=graph.edge_begin(), end=graph.edge_end();beg != end;++beg)
			{
				write_edge(out, *beg);
			}
			
			write_footer(out, graph);
			return 0;
		}
		
	protected:
		/** Writes the header of the graph file.
		 * 
		 * @param out an output stream.
		 * @param graph a source graph.
		 */
		virtual void write_header(std::ostream& out, const Graph& graph)const
		{
		}
		/** Writes the footer of the graph file.
		 * 
		 * @param out an output stream.
		 * @param graph a source graph.
		 */
		virtual void write_footer(std::ostream& out, const Graph& graph)const
		{
		}
		/** Writes a node for a graph.
		 * 
		 * @param out an output stream.
		 * @param node a graph node.
		 */
		virtual void write_node(std::ostream& out, const GraphNode& node)const
		{
		}
		/** Writes an edge for a graph.
		 * 
		 * @param out an output stream.
		 * @param edge a graph edge.
		 */
		virtual void write_edge(std::ostream& out, const GraphEdge& edge)const
		{
		}
		/** Convert a float to a string.
		 * 
		 * @param f a float value.
		 * @param s a string reference.
		 */
		void flt2str(double f, std::string& str)const
		{
			std::ostringstream val;
			val << std::setprecision(genformat->floatPrecision()) << f;
			str = val.str();
		}
		/** Convert a float in string format to some precision. If not a float, then
		 * return the original string.
		 * 
		 * @param orig a string
		 * @return a formatted string.
		 */
		std::string flt2str(const std::string& orig)const
		{
			if( TypeUtil<float>::valid(orig.c_str()) )
			{
				float val;
				stringToValue(orig, val);
				std::string temp;
				flt2str(val, temp);
				return temp;
			}
			return orig;
		}
		
	protected:
		/** Get a general graph format.
		 * 
		 * @return a general graph format.
		 */
		const GeneralGraphFormat& general()const
		{
			return *genformat;
		}
		
	private:
		GeneralGraphFormat* genformat;
	};
};

#endif


