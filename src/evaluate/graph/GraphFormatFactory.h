/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * GraphFormatFactory.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_GRAPHFORMATFACTORY_H
#define _EXEGETE_GRAPHFORMATFACTORY_H
#include "GraphFormat.h"
#include "GMLFormat.h"
#include "DOTFormat.h"
#include <vector>

/** @file GraphFormatFactory.h
 * @brief Defines a factory for graph formats
 * 
 * This file contains the GraphFormatFactory class.
 *
 * @ingroup ExegeteGraph
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#define _GRAPHFORMATFACTORY_VERSION 101000

namespace exegete
{
	/** @brief Defines a factory for graph formats
	 * 
	 * This class defines a graph format factory. It writes a graph is a format
	 * set by a parameter.
	 */
	class GraphFormatFactory
	{
		typedef std::vector<GraphFormat*> format_vector;
		typedef format_vector::const_iterator const_iterator;
		typedef format_vector::iterator iterator;
	public:
		/** Constructs a graph format.
		 */
		GraphFormatFactory() : typeInt(0)
		{
			formats.push_back(new DOTFormat(gformat));
		}
		/** Destructs a graph format.
		 */
		~GraphFormatFactory()
		{
			for(iterator beg=formats.begin(), end=formats.end();beg != end;++beg) delete *beg;
		}
		
	public:
		/** Initialize a map with parameters.
		 * 
		 * @param map a parameter object.
		 */
		template<class U>
		void init(U& map)
		{
			arginit(map, typeInt, 	"graph", 		options("print graph>"), ArgumentMap::ADDITIONAL);
			arginit(map, graphfile, "graphfile", 	"write graph to specified file", ArgumentMap::ADDITIONAL);
			gformat.argumentmap(map);
		}
		
	public:
		/** Write a graph to a file. The file and graph format are specified 
		 *  by a arguments in an argument map.
		 * 
		 * @param graph a graph.
		 */
		const char* write(const Graph& graph)
		{
			gformat.sortConserved();
			const char* msg;
			std::ofstream fout;
			if( (msg=openfile(fout, graphfile)) != 0 ) return msg;
			if( (msg=write(fout, graph)) != 0 ) return msg;
			fout.close();
			return 0;
		}
		/** Write a graph to an output stream. The graph format is specified 
		 *  by an argument map.
		 * 
		 * @param out an output stream.
		 * @param graph a graph.
		 */
		const char* write(std::ostream& out, const Graph& graph)const
		{
			ASSERT((typeInt) < formats.size());
			ASSERT(formats[typeInt] != 0);
			return formats[typeInt]->write(out, graph);
		}
		/** Get a filename for a graph format.
		 * 
		 * @return name of output graph file.
		 */
		const std::string& file()const
		{
			return graphfile;
		}
		/** Gets the name of the argument group.
		 * 
		 * @return Graphing
		 */
		static std::string name()
		{
			return "Graphing";
		}
		/** Get the version of the argument group.
		 * 
		 * @return current version number.
		 */
		static int version()
		{
			return _GRAPHFORMATFACTORY_VERSION;
		}
		
	private:
		std::string options(const char* prefix)
		{
			unsigned int len = (unsigned int)formats.size();
			std::string ds=prefix;
			std::string tmp;
			for(unsigned int i=0;i<len;++i)
			{
				ds+=";";
				valueToString(i, tmp);
				ds+=formats[i]->name() + ":";
				ds+=tmp;
			}
			return ds;
		}
		
	private:
		unsigned int typeInt;
		std::string graphfile;
		format_vector formats;
		GeneralGraphFormat gformat;
	};
	
};

#endif


