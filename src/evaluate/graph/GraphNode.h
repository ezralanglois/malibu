/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * GraphNode.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_GRAPHNODE_H
#define _EXEGETE_GRAPHNODE_H
#include <string>

/** @file GraphObject.h
 * @brief Defines child graph objects
 * 
 * This file contains the GraphNode class that represents an element in a graph. This
 * includes a GraphObject and a GraphEdge.
 *
 * @ingroup ExegeteGraph
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a general graph object.
	 * 
	 * This class defines an object in a graph.
	 */
	class GraphObject
	{
	public:
		/** Constructs a graph object.
		 * 
		 * @param l a string node label.
		 * @param p a property index.
		 */
		GraphObject(const std::string& l, unsigned int p) : 
			propertyTypeInt(p), labelStr(l)
		{
		}
		
	public:
		/** Append a string to the label of the graph object.
		 * 
		 * @param str a string to append.
		 * @return a reference to this class object.
		 */
		GraphObject& operator+=(const std::string& str)
		{
			labelStr += str;
			return *this;
		}
		
	public:
		/** Gets a string label.
		 * 
		 * @return a string label.
		 */
		const std::string& label()const
		{
			return labelStr;
		}
		/** Sets a string label.
		 * 
		 * @param i a string label.
		 */
		void label(const std::string& i)
		{
			labelStr = i;
		}
		/** Gets the propety index.
		 * 
		 * @return a property index.
		 */
		unsigned int property()const
		{
			return propertyTypeInt;
		}
		/** Sets the propety index.
		 * 
		 * @param i a property index.
		 */
		void property(unsigned int i)
		{
			propertyTypeInt = i;
		}
		
	private:
		unsigned int propertyTypeInt;
		std::string labelStr;
	};
	/** @brief Defines a node in a graph
	 * 
	 * This class defines a node in a graph.
	 */
	class GraphNode : public GraphObject
	{
	public:
		/** Constructs a graph node.
		 */
		GraphNode() : GraphObject("",0), idInt(0), 
					  rankInt(0), orderInt(0), conservedFlt(0.0f)
		{
		}
		/** Constructs a graph node.
		 * 
		 * @param l a string node label.
		 * @param i a unique identifier.
		 * @param r a rank index.
		 * @param p a property index.
		 */
		GraphNode(const std::string& l, long i, long r, unsigned int p=0) : GraphObject(l, p), 
				idInt(i), rankInt(r), orderInt(0), conservedFlt(0.0f)
		{
		}
		/** Destructs a graph node.
		 */
		~GraphNode()
		{
		}
		
	public:
		/** Gets a unique id for a node in a graph.
		 * 
		 * @return a unique id.
		 */
		long id()const
		{
			return idInt;
		}
		/** Sets a unique id for a node in a graph.
		 * 
		 * @param i a unique id.
		 */
		void id(long i)
		{
			idInt = i;
		}
		/** Gets a heirarchical rank for a node in a graph.
		 * 
		 * @return a heirarchical rank.
		 */
		long rank()const
		{
			return rankInt;
		}
		/** Sets a heirarchical rank for a node in a graph.
		 * 
		 * @param i a heirarchical rank.
		 */
		void rank(long i)
		{
			rankInt = i;
		}
		/** Gets a growth order for a node in a graph.
		 * 
		 * @return a heirarchical rank.
		 */
		long order()const
		{
			return orderInt;
		}
		/** Sets a growth order for a node in a graph.
		 * 
		 * @param i a heirarchical rank.
		 */
		void order(long i)
		{
			orderInt = i;
		}
		/** Gets the level of conservation.
		 * 
		 * @return level of conservation.
		 */
		float conserved()const
		{
			return conservedFlt;
		}
		/** Sets the level of conservation.
		 * 
		 * @param i level of conservation.
		 */
		void conserved(float i)
		{
			conservedFlt = i;
		}
		
	private:
		long idInt;
		long rankInt;
		long orderInt;
		float conservedFlt;
	};
	
	/** @brief Defines an edge connection two nodes.
	 * 
	 * This class defines an edge connecting two nodes in a graph.
	 */
	class GraphEdge : public GraphObject
	{
	public:
		/** Constructs a graph node.
		 * 
		 * @param f a unique node identifier the edge leaves.
		 * @param t a unique node identifier the edge enters.
		 * @param l a label on the edge.
		 * @param p a property index.
		 */
		GraphEdge(long f, long t, std::string l="", unsigned int p=0) : GraphObject(l, p), fromInt(f), toInt(t)
		{
		}
		/** Destructs a graph node.
		 */
		~GraphEdge()
		{
		}
		
	public:
		/** Gets a unique id for a node in a graph where an edge begins.
		 * 
		 * @return a unique id.
		 */
		long fromID()const
		{
			return fromInt;
		}
		/** Sets a unique id for a node in a graph where an edge begins.
		 * 
		 * @param i a unique id.
		 */
		void fromID(long i)
		{
			fromInt = i;
		}
		/** Gets a unique id for a node in a graph where an edge ends.
		 * 
		 * @return a unique id.
		 */
		long toID()const
		{
			return toInt;
		}
		/** Sets a unique id for a node in a graph where an edge ends.
		 * 
		 * @param i a unique id.
		 */
		void toID(long i)
		{
			toInt = i;
		}
		
	private:
		long fromInt;
		long toInt;
	};
};


#endif


