/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * threshold_metrics.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_CONTINGENCY_METRICS_HPP
#define _EXEGETE_CONTINGENCY_METRICS_HPP
#include "mathutil.h"
#include "AttributeTypeUtil.h"
#include "contingency_table.hpp"


/** @file contingency_metrics.hpp
 * @brief Calculates metrics from a contingency table.
 * 
 * This file contains a set of functions to calculate metrics from
 * a contingency table.
 *
 * 
 * @ingroup ExegeteMeasure
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{
	/** Compute the Gini-index.
	 * 
	 * @param array of probability values.
	 * @param tot total weight.
	 * @param length of probability values.
	 * @return the gini index.
	 */
	double gini(const double* array, double tot, unsigned int len)
	{
		double sum = 0.0, val;
		for(unsigned int i=0;i<len;++i)
		{
			val = array[i]/tot;
			sum += val*val;
		}
		return 1.0 - sum;
	}
	/** Estimate the entropy for a single value.
	 * 
	 * @param val a value to estimate
	 * @return entropy value.
	 */
	inline double entropy_ln(double val)
	{
		return val < ::math::eps ? 0 : val * std::log(val);
	}	
	/** Calculates the accuracy for the given confusion matrix.
	 *
	 * \f$H(X)= -\sum_{j=1}^{m}{ p_j \log_2 p_j} \f$
	 *
	 * @param array of probability values.
	 * @param length of probability values.
	 * @return the entropy.
	 */
	inline double entropy(const double* array, unsigned int len)
	{
		double ent = 0.0, sum = 0.0, val;
		for(unsigned int i=0;i<len;++i)
		{
			val = array[i];
			ent -= entropy_ln(val);
			sum += val;
		}
		return double_eq(sum, 0.0) ? 0.0 :
			   (ent + entropy_ln(sum)) / (sum * ::math::log2);
	}
	/** Estimate the conditional entropy of columns given rows.
	 * 
	 * @param ar a contingency array.
	 * @return conditional entropy of columns given rows.
	 */
	inline double entropy(const contingency_array<double>& ar)
	{
		return entropy(ar, ar.length());
	}
	/** Estimate the conditional entropy of columns given rows.
	 * 
	 * @todo add equation
	 * @todo add more metrics (WEKA ContingencyTables.java)
	 * 
	 * @param table a contingency table.
	 * @param rows number of rows.
	 * @param cols number of cols.
	 * @return conditional entropy of columns given rows.
	 */
	inline double entropy_column_on_row(const double*const* table, unsigned int rows, unsigned int cols)
	{
		double ent = 0.0, sum = 0.0, rowsum, val;
		for(unsigned int r=0;r<rows;++r)
		{
			rowsum = 0.0f;
			for(unsigned int c=0;c<cols;++c)
			{
				val = table[r][c];
				ent = ent + entropy_ln( val );
				rowsum += val;
			}
			ent = ent - entropy_ln( rowsum );
			sum += rowsum;
		}
		return double_eq(sum, 0.0) ? 0.0 :
			   -ent / ( sum * ::math::log2 );
	}
	/** Estimate the conditional entropy of columns given rows.
	 * 
	 * @param table a contingency table.
	 * @return conditional entropy of columns given rows.
	 */
	inline double entropy_column_on_row(const contingency_table<double>& table)
	{
		return entropy_column_on_row(table, table.rows(), table.columns());
	}
	/** Sum example weight by class.
	 * 
	 * @param table a contingency table.
	 * @param rows number of rows.
	 * @param cols number of cols.
	 * @return conditional entropy of columns given rows.
	 */
	template<class I>
	inline double build_contingency(I beg, I end, double* table, unsigned int len, double frac)
	{
		double sum = 0.0, wgt;
		for(;beg != end;++beg)
		{
			wgt = beg->weight(frac);
			sum += wgt;
			if( is_attribute_missing(beg->y()) ) continue;
			table[ beg->y() ] += wgt;
		}
		return sum;
	}
	/** Sum example weight by class.
	 * 
	 * @param table a contingency table.
	 * @param rows number of rows.
	 * @param cols number of cols.
	 * @return conditional entropy of columns given rows.
	 */
	template<class I>
	inline double build_contingency(I beg, I end, contingency_array<double>& ar, double frac)
	{
		return build_contingency(beg, end, ar, ar.length(), frac);
	}
};


#endif



