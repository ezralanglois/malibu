/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * threshold_metrics.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_CONTINGENCY_TABLE_HPP
#define _EXEGETE_CONTINGENCY_TABLE_HPP
#include "memoryutil.h"


/** @file contingency_table.hpp
 * @brief A contingency table
 * 
 * This file contains a contingency table.
 *
 * 
 * @ingroup ExegeteMeasure
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{
	/** @brief A contingency table
	 * 
	 * This class defines a contingency table.
	 */
	template<class T=double>
	class contingency_table
	{
	public:
		/** Defines a size type. **/
		typedef unsigned int size_type;
		/** Defines a value type. **/
		typedef T value_type;
		/** Defines a pointer. **/
		typedef T* pointer;
		/** Defines a constant pointer. **/
		typedef const T* const_pointer;
	private:
		typedef const const_pointer* const_pointer2d;
		
	public:
		/** Constructs a contingency table.
		 */
		contingency_table() : table(0), rowCnt(0), colCnt(0)
		{
		}
		/** Constructs a contingency table.
		 * 
		 * @param r number of rows.
		 * @param c number of columns.
		 */
		contingency_table(size_type r, size_type c) : table(0), rowCnt(r), colCnt(c)
		{
			table=resize_impl(table,r,c);
		}
		/** Destructs a contingency table.
		 */
		~contingency_table()
		{
			table=resize_impl(table,0,0);
		}
		
	public:
		/** Assigns a copy of a contingency table.
		 * 
		 * @param cont a contingency table.
		 * @return a reference to this object.
		 */
		contingency_table& operator=(const contingency_table& cont)
		{
			resize(cont.rows(), cont.columns());
			const_pointer rbeg = *cont.table, rend = rbeg + rowCnt;
			pointer* it = table;
			for(;rbeg != rend;++rbeg,++it)
			{
				std::copy(rbeg, (rbeg+colCnt), *it);
			}
			return *this;
		}
		
	public:
		/** Resizes a contingency table.
		 * 
		 * @param r number of rows.
		 * @param c number of columns.
		 */
		void resize(size_type r, size_type c)
		{
			if( rowCnt != r || colCnt != c )
			{
				table=resize_impl(table,r,c);
				rowCnt=r;
				colCnt=c;
			}
		}
		/** Implicitly convert contingency table to a pointer to a two-dimensional array.
		 * 
		 * @return pointer to two-dimensional array.
		 */
		operator pointer*()
		{
			ASSERT(table!=0);
			return table;
		}
		/** Implicitly convert contingency table to a pointer to a two-dimensional array.
		 * 
		 * @return pointer to two-dimensional array.
		 */
		operator const_pointer2d()const
		{
			ASSERT(table!=0);
			return table;
		}
		/** Get number of rows.
		 * 
		 * @return number of rows.
		 */
		size_type rows()const
		{
			return rowCnt;
		}
		/** Get number of columns.
		 * 
		 * @return number of columns.
		 */
		size_type columns()const
		{
			return colCnt;
		}
		/** Get total number of cells.
		 * 
		 * @return total length.
		 */
		size_type length()const
		{
			return colCnt*rowCnt;
		}
		/** Get an array for a row.
		 * 
		 * @return pointer to an array.
		 */
		pointer row_at(size_type r)
		{
			ASSERT(r < rowCnt);
			return table[r];
		}
		/** Get an array for a row.
		 * 
		 * @return pointer to an array.
		 */
		const_pointer row_at(size_type r)const
		{
			ASSERT(r < rowCnt);
			return table[r];
		}
		/** Fill the matrix with values.
		 * 
		 * @param val values to fill.
		 */
		void fill(value_type val)
		{
			ASSERT(table != 0);
			ASSERT(table[0] != 0);
			std::fill( table[0], table[0]+length(), val );
		}
		
	private:
		inline static pointer* resize_impl(pointer* tab, size_type r, size_type c)
		{
			if( r > 0 && c > 0 )
			{
				tab = ::resize(tab, r);
				tab[0] = 0;
				tab[0] = ::resize(tab[0], r*c);
				for(unsigned int i=1;i<r;++i)
					tab[i] = tab[i-1]+c;
			}
			else if( tab != 0 )
			{
				::erase(tab[0]);
				::erase(tab);
				return 0;
			}
			return tab;
		}
		
	private:
		pointer* table;
		size_type rowCnt;
		size_type colCnt;
	};
	

	/** @brief A contingency array
	 * 
	 * This class defines a contingency array.
	 */
	template<class T=double>
	class contingency_array
	{
	public:
		/** Defines a size type. **/
		typedef unsigned int size_type;
		/** Defines a value type. **/
		typedef T value_type;
		/** Defines a pointer. **/
		typedef T* pointer;
		/** Defines a constant pointer. **/
		typedef const T* const_pointer;
		
	public:
		/** Constructs a contingency array.
		 */
		contingency_array() : table(0), len(0)
		{
		}
		/** Constructs a contingency array.
		 * 
		 * @param l number of elements in array.
		 */
		contingency_array(size_type l) : table(0), len(l)
		{
			table=::resize(table,l);
		}
		/** Destructs a contingency array.
		 */
		~contingency_array()
		{
			::erase(table);
		}
		
	public:
		/** Resizes a contingency array.
		 * 
		 * @param l number of elements in array.
		 */
		void resize(size_type l)
		{
			table=::resize(table,l);
			len=l;
		}
		/** Implicitly convert contingency array to an array.
		 * 
		 * @return pointer to an array.
		 */
		operator pointer()
		{
			ASSERT(table!=0);
			return table;
		}
		/** Implicitly convert contingency array to an array.
		 * 
		 * @return pointer to an array.
		 */
		operator const_pointer()const
		{
			ASSERT(table!=0);
			return table;
		}
		/** Get number of elements.
		 * 
		 * @return number of elements.
		 */
		size_type length()const
		{
			return len;
		}
		/** Fill the matrix with values.
		 * 
		 * @param val values to fill.
		 */
		void fill(value_type val)
		{
			ASSERT(table != 0);
			std::fill( table, table+length(), val );
		}
		/** Get the start of the contingency array.
		 * 
		 * @return iterator to start.
		 */
		pointer begin()
		{
			return table;
		}
		/** Get the end of the contingency array.
		 * 
		 * @return iterator to end.
		 */
		pointer end()
		{
			return table+len;
		}
		/** Get the start of the contingency array.
		 * 
		 * @return iterator to start.
		 */
		const_pointer begin()const
		{
			return table;
		}
		/** Get the end of the contingency array.
		 * 
		 * @return iterator to end.
		 */
		const_pointer end()const
		{
			return table+len;
		}
		
	private:
		pointer table;
		size_type len;
	};
};


#endif



