/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * measure.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MEASURE_HPP
#define _EXEGETE_MEASURE_HPP
#include "measure_factory.hpp"
#include "metrics.hpp"
#include "multiclass_metrics.hpp"

/** @file measure.hpp
 * @brief Interface to various measures
 * 
 * This file contains the measure interface classes. The main interface is measure and it creates
 * measure factories holding specific metrics or plots.
 *
 * @ingroup ExegeteMeasure
 * @see measure_factory
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** Holds classes involved in machine learning.
 */
namespace exegete
{
	/** Holds private classes only used within the current file.
	 */
	namespace detail
	{
		/** @brief Defines functor for a binary metric
		 * 
		 * This class defines a functor for a binary metric calculated from 
		 * a collection of predictions.
		 */
		template<class I, class R, class F>
		struct prediction_base
		{
			/** Defines a return type. **/
			typedef R result_type;
			/** Defines a parameter type. **/
			typedef I param_type;
			/** Defines a functor type. **/
			typedef R (*function_type)(param_type,param_type);
		};
		/** @brief Defines functor for a multi-class metric
		 * 
		 * This class defines a functor for a multi-class metric calculated from 
		 * a collection of predictions and the number of classes.
		 */
		template<class I, class R, class F>
		struct prediction_base<I, R, F*>
		{
			/** Defines a return type. **/
			typedef R result_type;
			/** Defines a parameter type. **/
			typedef I param_type;
			/** Defines a functor type. **/
			typedef R (*function_type)(param_type,param_type,unsigned int);
		};
		/** @brief Defines functor for a confusion metric.
		 * 
		 * This class defines a functor for a binary confusion metric calculated from 
		 * a confusion table.
		 */
		template<class P, class R>
		struct confusion_base
		{
			/** Defines a return type. **/
			typedef R result_type;
			/** Defines a parameter type. **/
			typedef P param_type;
			/** Defines a functor type. **/
			typedef R (*function_type)(P,P,P,P);
		};
		/** @brief Defines functor for a plot
		 * 
		 * This class defines a functor for a binary plot taking a collection of
		 * predictions and returning a collection of coordinates.
		 */
		template<class P, class R>
		struct plot_base
		{
			/** Defines a return type. **/
			typedef R result_type;
			/** Defines a parameter type. **/
			typedef P param_type;
			/** Defines a functor type. **/
			typedef R* (*function_type)(P,P,R*,R*);
		};
		/** @brief Defines functor for an array of binary metrics from a multi-class problem.
		 * 
		 * This class defines a functor for calculating an array of binary metrics from a multi-class
		 * problem. It takes a collection of examples, an output array, a row index and number of classes.
		 */
		template<class P, class R>
		struct array_base
		{
			/** Defines a return type. **/
			typedef R result_type;
			/** Defines a parameter type. **/
			typedef P param_type;
			/** Defines a functor type. **/
			typedef void (*function_type)(P,P,double*,unsigned int,unsigned int);
		};
	};
	
	namespace detail
	{
		/** @brief Adds a set of single threshold metrics.
		 * 
		 * This class adds a set of single threshold metrics to a vector. A single threshold metric
		 * captures both classes in a single metric.
		 * 	- Accuracy
		 * 	- Lift
		 * 	- F-score
		 * 	- Net Precision/Recall
		 * 	- Net Accuracy
		 * 	- Matthew's Correlation
		 * 
		 * @see skew_threshold_measure_base order_measure_base real_measure_base max_threshold_measure_base count_measure_base
		 */
		template<class B, class F>
		struct single_threshold_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(threshold_acc<param_type>, "Accuracy", 					"ACC", "$\\frac{TP+TN}{TP+TN+FP+FN}$", "Single Threshold Binary Metric"));
				metrics.push_back(T(threshold_lft<param_type>, "Lift", 						"LFT", "$\\frac{\\frac{TP}{TP+FN}*(TP+FP+TN+FN)}{TP+FN}$", "Single Threshold Binary Metric"));
				metrics.push_back(T(threshold_fsc<param_type>, "F-score", 					"FSC", "$\\frac{2.0*PRE*RCL}{PRE+RCL}$", "Single Threshold Binary Metric"));
				metrics.push_back(T(threshold_net<param_type>, "Net Precision/Recall", 		"NET", "$\\frac{PRE+NPV}{2}$", "Single Threshold Binary Metric"));
				metrics.push_back(T(threshold_nac<param_type>, "Net Accuracy", 				"NAC", "$\\frac{SEN+PRE}{2}$", "Single Threshold Binary Metric"));
				metrics.push_back(T(threshold_mcc<param_type>, "Matthew's Correlation", 	"MCC", "$\\frac{TP*TN-FP*FN}{\\sqrt{(TN+FN)(TN+FP)(TP+FN)(TP+FP)}}$", "Single Threshold Binary Metric"));
			}
		};
		/** @brief Adds a set of skew threshold metrics.
		 * 
		 * This class adds a set of skew threshold metrics to a vector. A skew threshold metric
		 * captures only one class and has a counterpart to capture another class.
		 * 	- Sensitivity
		 * 	- Specificity
		 * 	- Precision
		 * 	- Negative Predictive Value
		 * 
		 * @see single_threshold_measure_base order_measure_base real_measure_base max_threshold_measure_base count_measure_base
		 */
		template<class B, class F>
		struct skew_threshold_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(threshold_sen<param_type>, "Sensitivity", 				"SEN", "$\\frac{TP}{TP+FN}$", "Binary Skew Threshold Metric"));
				metrics.push_back(T(threshold_spe<param_type>, "Specificity", 				"SPE", "$\\frac{TN}{FP+TN}$", "Binary Skew Threshold Metric"));
				metrics.push_back(T(threshold_pre<param_type>, "Precision", 				"PRE", "$\\frac{TP}{FP+TP}$", "Binary Skew Threshold Metric"));
				metrics.push_back(T(threshold_npv<param_type>, "Negative Predictive Value", "NPV", "$\\frac{TN}{TN+FN}$", "Binary Skew Threshold Metric"));
				//single_threshold_measure_base<B,F>::build(metrics);
			}
		};
		/** @brief Adds a set of order metrics.
		 * 
		 * This class adds a set of order metrics to a vector. An order metric measures
		 * the classifier's ability to rank examples in the correct order.
		 * 	- Area under ROC
		 * 	- Area under Lift
		 * 	- Area under Precision/Recall
		 * 	- Area under NPV/Specificty
		 * 	- Area under Break-even
		 * 
		 * @see single_threshold_measure_base skew_threshold_measure_base real_measure_base max_threshold_measure_base count_measure_base
		 */
		template<class B, class F>
		struct order_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(roca<param_type>, 	"Area under ROC", 				"AUR",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "Binary Order Metric"));
				metrics.push_back(T(lfta<param_type>, 	"Area under Lift",	 			"AUL",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "Binary Order Metric"));
				metrics.push_back(T(prca<param_type>, 	"Area under Precision/Recall",	"AUP",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "Binary Order Metric"));
				metrics.push_back(T(ipra<param_type>, 	"Area under NPV/Specificty",	"AUN",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "Binary Order Metric"));
				metrics.push_back(T(bpra<param_type>, 	"Area under Break-even",	 	"AUB",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "Binary Order Metric"));
			}
		};
		/** @brief Adds a set of regression metrics.
		 * 
		 * This class adds a set of regression metrics to a vector. A regression metric
		 * measures the learners ability to predict a real-value.
		 * 	- Root Mean Square Error
		 * 	- Mean Cross Entropy
		 * 
		 * @see single_threshold_measure_base skew_threshold_measure_base order_measure_base max_threshold_measure_base count_measure_base
		 */
		template<class B, class F>
		struct real_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(rmse<param_type>,	"Root Mean Square Error",	"RME",	"$\\frac{1}{N}\\displaystyle\\sum_{i=0}^N (p_i-c_i)^2$", "Binary Regression Metric"));
				metrics.push_back(T(mcxe<param_type>,	"Mean Cross Entropy",		"CXE",	"$\\frac{1}{N}\\displaystyle\\sum_{i=0}^N (T*log(p_i) + (1-T)*log(1-p_i))$ where $T\\in{0,1}$", "Binary Regression Metric"));
			}
		};
		/** @brief Adds a set of max threshold metrics.
		 * 
		 * This class adds a set of max threshold metrics to a vector. A max threshold metric is similar to a 
		 * single threshold metric except it is the maximum value over all thresholds.
		 * 	- Max Accuracy
		 * 	- Max F-score
		 * 	- Max MCC
		 * 	- Max Lift
		 * 
		 * @see single_threshold_measure_base skew_threshold_measure_base order_measure_base real_measure_base count_measure_base
		 */
		template<class B, class F>
		struct max_threshold_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(max_acc<param_type>,	"Max Accuracy",		"MAXACC",	"$argmax_t ACC(t)$", "Maximum Threshold Binary Metric"));
				metrics.push_back(T(max_fsc<param_type>,	"Max F-score",		"MAXFSC",	"$argmax_t FSC(t)$", "Maximum Threshold Binary Metric"));
				metrics.push_back(T(max_mcc<param_type>,	"Max MCC",			"MAXMCC",	"$argmax_t MCC(t)$", "Maximum Threshold Binary Metric"));
				metrics.push_back(T(max_lft<param_type>,	"Max Lift",			"MAXLFT",	"$argmax_t LFT(t)$", "Maximum Threshold Binary Metric"));
			}
		};
		/** @brief Adds a set of threshold counts.
		 * 
		 * This class adds a set of threshold counts to a vector. A threshold count
		 * tabulates the number of successes or failures for a particular class.
		 * 	- True Positive
		 * 	- True Negative
		 * 	- False Positive
		 * 	- False Negative
		 * 	- Threshold (not calculated just displayed)
		 * 
		 * @see single_threshold_measure_base skew_threshold_measure_base order_measure_base real_measure_base max_threshold_measure_base
		 */
		template<class B, class F>
		struct count_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{				
				metrics.push_back(T(threshold_ctp<param_type>,		"True Positive",	"TP",		"$TP$", "Threshold Binary Count"));
				metrics.push_back(T(threshold_ctn<param_type>,		"True Negative",	"TN",		"$TN$", "Threshold Binary Count"));
				metrics.push_back(T(threshold_cfp<param_type>,		"False Positive",	"FP",		"$FP$", "Threshold Binary Count"));
				metrics.push_back(T(threshold_cfn<param_type>,		"False Negative",	"FN",       "$FN$", "Threshold Binary Count"));
				metrics.push_back(T(threshold_average<param_type>, 	"Threshold", 		"THR",		"$T$",  "Threshold Binary Count"));
			}
		};
	};
	
	
	namespace detail
	{
		/** @brief Adds a set of multi-class single threshold metrics.
		 * 
		 * This class adds a set of multi-class single threshold metrics to a vector. A single 
		 * threshold metric captures all classes.
		 * 	- Accuracy
		 * 	- OVO
		 * 		- Lift
		 * 		- F-score
		 * 		- Matthew's Correlation
		 * 	- OVA
		 * 		- Lift
		 * 		- F-score
		 * 		- Matthew's Correlation
		 * 
		 * @see skew_threshold_measure_base order_measure_base real_measure_base max_threshold_measure_base count_measure_base
		 */
		template<class B, class F>
		struct single_threshold_measure_base<B, F*>
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(mc_acc<param_type>, "Accuracy", 				"ACC", "$\\frac{T}{N}$", "OvO Threshold Metric"));
				
				metrics.push_back(T(ovo_lft<param_type>, "Lift", 					"LFT", "$\\frac{\\frac{TP}{TP+FN}*(TP+FP+TN+FN)}{TP+FN}$", "OvO Threshold Metric"));
				metrics.push_back(T(ovo_fsc<param_type>, "F-score", 				"FSC", "$\\frac{2.0*PRE*RCL}{PRE+RCL}$", "OVO Threshold Metric"));
				metrics.push_back(T(ovo_mcc<param_type>, "Matthew's Correlation", 	"MCC", "$\\frac{TP*TN-FP*FN}{\\sqrt{(TN+FN)(TN+FP)(TP+FN)(TP+FP)}}$", "OvO Threshold Metric"));

				metrics.push_back(T(ova_lft<param_type>, "OVA Lift", 					"OVA_LFT", "$\\frac{\\frac{TP}{TP+FN}*(TP+FP+TN+FN)}{TP+FN}$", "OvA Threshold Metric"));
				metrics.push_back(T(ova_fsc<param_type>, "OVA F-score", 				"OVA_FSC", "$\\frac{2.0*PRE*RCL}{PRE+RCL}$", "OvA Threshold Metric"));
				metrics.push_back(T(ova_mcc<param_type>, "OVA Matthew's Correlation", 	"OVA_MCC", "$\\frac{TP*TN-FP*FN}{\\sqrt{(TN+FN)(TN+FP)(TP+FN)(TP+FP)}}$", "OvA Threshold Metric"));
			}
		};
		/** @brief Adds a set of multi-class skew threshold metrics.
		 * 
		 * This class adds a set of skew threshold metrics to a vector. A skew threshold metric
		 * captures a single class averaged over all classes.
		 * 	- OVO
		 * 		- Sensitivity
		 * 		- Precision
		 * 	- OVA
		 * 		- Sensitivity
		 * 		- Precision
		 * 
		 * @see single_threshold_measure_base order_measure_base real_measure_base max_threshold_measure_base count_measure_base
		 */
		template<class B, class F>
		struct skew_threshold_measure_base<B,F*>
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(ovo_sen<param_type>, "Sensitivity", 			"SEN", "$\\frac{TP}{TP+FN}$", "OvO Threshold Metric"));
				metrics.push_back(T(ovo_pre<param_type>, "Precision", 				"PRE", "$\\frac{TP}{FP+TP}$", "OvO Threshold Metric"));
				
				metrics.push_back(T(ova_sen<param_type>, "OVA Sensitivity", 			"OVA_SEN", "$\\frac{TP}{TP+FN}$", "OvA Threshold Metric"));
				metrics.push_back(T(ova_pre<param_type>, "OVA Precision", 				"OVA_PRE", "$\\frac{TP}{FP+TP}$", "OvA Threshold Metric"));
			}
		};
		/** @brief Adds a set of multi-class order metrics.
		 * 
		 * This class adds a set of multi-class order metrics to a vector. A multi-class order metric
		 * captures the average ordering of examples.
		 * 	- OVO
		 * 		- Area under ROC
		 * 		- Area under Lift
		 * 		- Area under Precision/Recall
		 * 		- Area under NPV/Specificty
		 * 		- Area under Break-even
		 * 	- OVA
		 * 		- Area under ROC
		 * 		- Area under Lift
		 * 		- Area under Precision/Recall
		 * 		- Area under NPV/Specificty
		 * 		- Area under Break-even
		 * 
		 * @see single_threshold_measure_base skew_threshold_measure_base real_measure_base max_threshold_measure_base count_measure_base
		 */
		template<class B, class F>
		struct order_measure_base<B,F*>
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{				
				metrics.push_back(T(ovo_roca<param_type>, 	"Area under ROC",	 			"AUR",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvO Order Metric"));
				metrics.push_back(T(ovo_lfta<param_type>, 	"Area under Lift",	 			"AUL",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvO Order Metric"));
				metrics.push_back(T(ovo_prca<param_type>, 	"Area under Precision/Recall",	"AUP",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvO Order Metric"));
				metrics.push_back(T(ovo_ipra<param_type>, 	"Area under NPV/Specificty",	"AUN",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvO Order Metric"));
				metrics.push_back(T(ovo_bpra<param_type>, 	"Area under Break-even",	 	"AUB",		"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvO Order Metric"));
				
				metrics.push_back(T(ova_roca<param_type>, 	"OVA Area under ROC", 				"OVA_AUR",	"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvA Order Metric"));
				metrics.push_back(T(ova_lfta<param_type>, 	"OVA Area under Lift",	 			"OVA_AUL",	"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvA Order Metric"));
				metrics.push_back(T(ova_prca<param_type>, 	"OVA Area under Precision/Recall",	"OVA_AUP",	"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvA Order Metric"));
				metrics.push_back(T(ova_ipra<param_type>, 	"OVA Area under NPV/Specificty",	"OVA_AUN",	"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvA Order Metric"));
				metrics.push_back(T(ova_bpra<param_type>, 	"OVA Area under Break-even",	 	"OVA_AUB",	"$\\displaystyle\\sum_{i=0}^n \\frac{f(x_{i-1})+f(x_i)}{2} |x_{i-1}-x_i|$", "OvA Order Metric"));
			}
		};
		/** @brief Adds a set of regression metrics.
		 * 
		 * This class adds a set of regression metrics to a vector. A regression metric
		 * measures the closeness of a real-valued output and class.
		 * 	- Root Mean Square Error
		 * 	- Mean Cross Entropy
		 * 
		 * @see single_threshold_measure_base skew_threshold_measure_base order_measure_base max_threshold_measure_base count_measure_base
		 */
		template<class B, class F>
		struct real_measure_base<B,F*>
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(mc_rmse<param_type>,	"Root Mean Square Error",	"RME",	"$\\frac{1}{N}\\displaystyle\\sum_{i=0}^N (p_i-c_i)^2$", "Multi-class Regression Metric"));
				metrics.push_back(T(mc_mcxe<param_type>,	"Mean Cross Entropy",		"CXE",	"$\\frac{1}{N}\\displaystyle\\sum_{i=0}^N (T*log(p_i) + (1-T)*log(1-p_i))$ where $T\\in{0,1}$", "Multi-class Regression Metric"));
				//Structured learning
				metrics.push_back(T(normalized_optimal<param_type>, 	"NormalizedOptimal", 	"NORMOPT",	"$1-\frac{1}{n}Cost_1^n$",  	"Normalized optimal fraction of structured learning output"));
			}
		};
		/** @brief Adds a set of max threshold metrics.
		 * 
		 * @note Here for compatibility, adds nothing
		 * 
		 * @see single_threshold_measure_base skew_threshold_measure_base order_measure_base real_measure_base count_measure_base
		 */
		template<class B, class F>
		struct max_threshold_measure_base<B,F*>
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @note adds nothing
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
			}
		};
		/** @brief Adds a set of threshold counts.
		 * 
		 * @note Here for compatibility, adds nothing
		 * 
		 * @see single_threshold_measure_base skew_threshold_measure_base order_measure_base real_measure_base max_threshold_measure_base
		 */
		template<class B, class F>
		struct count_measure_base<B,F*>
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @note adds nothing
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				metrics.push_back(T(normalized_cost<param_type>, 	"NormalizedCost", 	"NORMCOST",	"$\frac{1}{n}Cost_1^n$",  	"Normalized cost of structured learning output"));
				metrics.push_back(T(unnormalized_cost<param_type>, 	"Cost", 			"COST",		"$Cost_1^n$",  				"Cost of structured learning output"));
				metrics.push_back(T(total_cost<param_type>, 		"TotalCost", 		"TOTCOST",	"$Total$",  				"Total cost of structured learning output"));
			}
		};
	};
	
	namespace detail
	{
		/** @brief Adds a set of row counts.
		 * 
		 * This class adds a function that tabulates predictions to an array.
		 * 
		 * @see plot_measure_base tune_measure_base full_measure_base
		 */
		template<class B>
		struct array_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{		
				metrics.push_back(T(mc_cont_row<param_type>, "Row Count", "RCNT", "Count predictions in row", "Row Count"));
			}
		};
		/** @brief Adds a set of plots.
		 * 
		 * This class adds plots. The plots can only handle the two class case.
		 * 	- Lift
		 * 	- Reciever Operating Characteristic
		 * 	- Cost
		 * 	- Lower Envelope X
		 * 	- Lower Envelope
		 * 	- Reliability
		 * 	- Precision/Recall
		 * 	- NPV/Specificity
		 * 
		 * @see array_measure_base tune_measure_base full_measure_base
		 */
		template<class B>
		struct plot_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{		
				metrics.push_back(T(lft_curve<param_type>,		"Lift",									"LFC",  "Fraction of Highest Scoring",	"Lift"));
				metrics.push_back(T(roc_curve<param_type>,		"Reciever Operating Characteristic",	"ROC",  "False Positive Rate",			"True Positive Rate"));
				metrics.push_back(T(cst_curve<param_type>,		"Cost",									"CST",  "Probability of Positive",		"Normalized Expected Cost"));
				metrics.push_back(T(env_curve_ex<param_type>,	"Lower Envelope X",						"ENVX", "Probability of Positive",		"Normalized Expected Cost"));
				metrics.push_back(T(env_curve<param_type>,		"Lower Envelope",						"ENV",  "Probability of Positive",		"Normalized Expected Cost"));
				metrics.push_back(T(red_curve<param_type>,		"Reliability",							"REL",  "Mean Prediction",				"Fraction of True Positives"));
				metrics.push_back(T(pr_curve<param_type>,		"Precision/Recall",						"PRC",  "Recall",						"Precision"));
				metrics.push_back(T(ipr_curve<param_type>,		"NPV/Specificity",						"NPR",  "Specificity",					"NPV"));
			}
		};
		/** @brief Adds a set tuning metrics.
		 * 
		 * This class only adds metrics that are useful for tuning a classifier.
		 * 	- single_threshold_measure_base
		 * 	- order_measure_base
		 * 	- real_measure_base
		 * 	- max_threshold_measure_base
		 * 
		 * @see array_measure_base plot_measure_base full_measure_base
		 */
		template<class B, class F>
		struct tune_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				single_threshold_measure_base<B,F>::build(metrics);
				order_measure_base<B,F>::build(metrics);
				real_measure_base<B,F>::build(metrics);
				max_threshold_measure_base<B,F>::build(metrics);
			}
		};
		/** @brief Adds all metrics
		 * 
		 * This class adds all metrics.
		 * 	- tune_measure_base
		 * 	- skew_threshold_measure_base
		 * 	- count_measure_base
		 * 
		 * @see array_measure_base plot_measure_base tune_measure_base
		 */
		template<class B, class F>
		struct full_measure_base
		{
			/** Defines a function type. **/
			typedef typename B::function_type function_type;
			/** Defines a parameter type. **/
			typedef typename B::param_type param_type;
			/** Adds a set of functors to a vector describing a function pointer.
			 * 
			 * @param metrics a vector of functors.
			 */
			template<class T> static void build(std::vector<T>& metrics)
			{
				tune_measure_base<B,F>::build(metrics);
				skew_threshold_measure_base<B,F>::build(metrics);
				count_measure_base<B,F>::build(metrics);
			}
		};
	};

	/** @brief Interface for creating a measure factory.
	 * 
	 * This class is a compile-time interface that creates a class factory
	 * that contains a subset of metrics or plots:
	 * 	- array = array_measure_base + array_base
	 * 	- tune = tune_measure_base + prediction_base
	 * 	- full = full_measure_base + prediction_base
	 * 	- full_binary = full_measure_base + prediction_base (float class)
	 * 	- calibration = single_threshold_measure_base + confusion_base
	 * 	- plot = plot_measure_base + plot_base
	 * 
	 * @see array_measure_base plot_measure_base tune_measure_base
	 */
	template<class I, class R, class F>
	struct measure
	{
		/** Defines a factory of array measures with a prediction interface. **/
		typedef measure_factory< 
			detail::array_measure_base< 
				detail::array_base<I,R>
				>
			> array;
		/** Defines a factory of tune measures  with a prediction interface. **/
		typedef measure_factory< 
			detail::tune_measure_base< 
				detail::prediction_base<I,R,F>, F
				>
			> tune;
		/** Defines a factory of every metric with a prediction interface. **/
		typedef measure_factory< 
			detail::full_measure_base< 
				detail::prediction_base<I,R,F>, F
				>
			> full;
		/** Defines a factory of every metric (only binary) with a prediction interface. **/
		typedef measure_factory< 
			detail::full_measure_base< 
				detail::prediction_base<I,R,float>, float
				>
			> full_binary;
		/** Defines a factory of single threshold metrics with a confusion interface. **/
		typedef measure_factory< 
			detail::single_threshold_measure_base< 
				detail::confusion_base<I,R>, float
				>
			> calibration;
		/** Defines a factory of plots with a prediction interface. **/
		typedef measure_factory< 
			detail::plot_measure_base< 
				detail::plot_base<I,R>
				>
			> plot;
	};
};


#endif




