/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * measure_factory.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MEASURE_FACTORY_HPP
#define _EXEGETE_MEASURE_FACTORY_HPP
#include <vector>
#include "metric_plots.hpp"

/** @file measure_factory.hpp
 * @brief Defines a measure factory
 * 
 * This file contains the measure factory class and private classes for metric
 * functors, its specializations and the measure functor class.
 *
 * @ingroup ExegeteMeasure
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Holds private classes and functions
	 * 
	 * This namespace holds private classes and functions. 
	 */
	namespace detail
	{
		/** @brief Defines a metric functor
		 * 
		 * This class defines a metric functor that requires 
		 * specialization.
		 * 
		 * @note for must types this class is undefined
		 */
		template<class T> class metric_functor;

		/** @brief Defines a metric functor for a set of predictions
		 * 
		 * This class defines a metric functor that specializes for a function
		 * taking two prediction iterators and returns a double.
		 * 
		 * Specializes for double (*)(P,P)
		 */
		template<class P>
		class metric_functor< double (*)(P,P) >
		{
			typedef double (*function_type)(P,P);
		public:
			/** Constructs a metric functor from a function pointer.
			 * 
			 * @param f a function pointer.
			 */
			metric_functor(function_type f) : pfun(f)
			{
			}
			/** Calculates a metric from a collection of predictions.
			 * 
			 * @param beg an iterator to start of prediction collection.
			 * @param end an iterator to end of prediction collection.
			 * @param i a dummy index.
			 * @return a metric.
			 */
			double calculate(P beg, P end, unsigned int i)const
			{
				ASSERT(beg != end);
				return pfun(beg, end);
			}
			
		private:
			function_type pfun;
		};
		/** @brief Defines a multi-class metric functor for a set of predictions
		 * 
		 * This class defines a multi-class metric functor that specializes for a function
		 * taking two prediction iterators and an integer and returns a double.
		 * 
		 * Specializes for double (*)(P,P,unsigned int)
		 */
		template<class P>
		class metric_functor< double (*)(P,P,unsigned int) >
		{
			typedef double (*function_type)(P,P,unsigned int);
		public:
			/** Constructs a metric functor from a function pointer.
			 * 
			 * @param f a function pointer.
			 */
			metric_functor(function_type f) : pfun(f)
			{
			}
			/** Calculates a multi-class metric from a collection of predictions.
			 * 
			 * @param beg an iterator to start of prediction collection.
			 * @param end an iterator to end of prediction collection.
			 * @param i number of classes.
			 * @return a metric.
			 */
			double calculate(P beg, P end, unsigned int i)const
			{
				ASSERT(beg != end);
				return pfun(beg, end, i);
			}
			
		private:
			function_type pfun;
		};
		/** @brief Defines a metric functor for a set of predictions at a given threshold
		 * 
		 * This class defines a metric functor that specializes for a function
		 * taking two prediction iterators and a float and returns a double.
		 * 
		 * Specializes for double (*)(P,P,float)
		 */
		template<class P>
		class metric_functor< double (*)(P,P,float) >
		{
			typedef double (*function_type)(P,P,unsigned int);
		public:
			/** Constructs a metric functor from a function pointer.
			 * 
			 * @param f a function pointer.
			 */
			metric_functor(function_type f) : pfun(f)
			{
			}
			/** Calculates a metric from a collection of predictions and a given
			 * threshold.
			 * 
			 * @param beg an iterator to start of prediction collection.
			 * @param end an iterator to end of prediction collection.
			 * @param th a threshold.
			 * @return a metric.
			 */
			double calculate(P beg, P end, float th)const
			{
				ASSERT(beg != end);
				return pfun(beg, end, th);
			}
			
		private:
			function_type pfun;
		};
		/** @brief Defines a metric functor for a set of predictions giving an array for a multi-class column
		 * 
		 * This class defines a metric functor that specializes for a function
		 * taking two prediction iterators, destination array, number of classes
		 * and column index.
		 * 
		 * Specializes for void (*)(P,P,double*,unsigned int,unsigned int)
		 */
		template<class P>
		class metric_functor< void (*)(P,P,double*,unsigned int,unsigned int) >
		{
			typedef void (*function_type)(P,P,double*,unsigned int,unsigned int);
		public:
			/** Constructs a metric functor from a function pointer.
			 * 
			 * @param f a function pointer.
			 */
			metric_functor(function_type f) : pfun(f)
			{
			}
			/** Fills a metric array from a collection of predictions, number of classes
			 * and a row index.
			 * 
			 * @param beg an iterator to start of prediction collection.
			 * @param end an iterator to end of prediction collection.
			 * @param p a destination metric array.
			 * @param i number of classes.
			 * @param j row index.
			 */
			void fill(P beg, P end, double* p, unsigned int i, unsigned int j)const
			{
				ASSERT(beg != end);
				pfun(beg, end, p, i, j);
			}
			
		private:
			function_type pfun;
		};
		/** @brief Defines a metric functor for a set of predictions giving a plot
		 * 
		 * This class defines a metric functor that specializes for a function
		 * taking two prediction iterators, (x,y)-coordinate arrays and returns
		 * the number of coordinates.
		 * 
		 * Specializes for double* (*)(P,P,double*,double*)
		 */
		template<class P>
		class metric_functor< double* (*)(P,P,double*,double*) >
		{
			typedef double* (*function_type)(P,P,double*,double*);
		public:
			/** Constructs a metric functor from a function pointer.
			 * 
			 * @param f a function pointer.
			 */
			metric_functor(function_type f) : pfun(f)
			{
			}
			/** Plots a collection of predictions and a integer flag to two destination coordinate
			 * arrays.
			 * 
			 * @param beg an iterator to start of prediction collection.
			 * @param end an iterator to end of prediction collection.
			 * @param px x-coordinate array.
			 * @param py y-coordinate array.
			 * @param n an flag integer.
			 * @return number of coordinates.
			 */
			unsigned int plot(P beg, P end, double*& px, double*& py, unsigned int n)const
			{
				ASSERT(beg != end);
				return plot_curve(beg, end, px, py, pfun, n);
			}
			
		private:
			function_type pfun;
		};
		/** @brief Defines a metric functor for a confusion matrix
		 * 
		 * This class defines a metric functor that specializes for a function
		 * taking a confusion matrix (set of counts).
		 * 
		 * Specializes for double (*)(unsigned int,unsigned int,unsigned int,unsigned int)
		 */
		template<>
		class metric_functor< double (*)(unsigned int,unsigned int,unsigned int,unsigned int) >
		{
			typedef double (*function_type)(unsigned int,unsigned int,unsigned int,unsigned int);
		public:
			/** Constructs a metric functor from a function pointer.
			 * 
			 * @param f a function pointer.
			 */
			metric_functor(function_type f) : pfun(f)
			{
			}
			/** Calculates a metric from a confusion matrix.
			 * 
			 * @param tp count true positives.
			 * @param tn count true negatives.
			 * @param fp count false positives.
			 * @param fn count false negatives.
			 * @return a metric.
			 */
			double calculate(unsigned int tp, unsigned int tn, unsigned int fp, unsigned int fn)const
			{
				return pfun(tp, tn, fp, fn);
			}
			
		private:
			function_type pfun;
		};
		/** @brief Defines a functor with name, code and description.
		 * 
		 * This class defines a measure factory with the following properties:
		 * 	- function pointer
		 * 	- name
		 * 	- code
		 * 	- equation or x-axis label
		 * 	- group or y-axis label
		 */
		template<class F>
		class measure_functor : public metric_functor<F>
		{
			typedef F pfunctor;
		public:
			/** Constructs a measure functor.
			 * 
			 * @param f a function pointer.
			 * @param n a name.
			 * @param c a code.
			 * @param e an equation x-axis label.
			 * @param y a x-axis label or group.
			 */
			measure_functor(F f, const std::string& n, const std::string& c, 
						   const std::string& e, const std::string& y="") : 
						   metric_functor<F>(f), name_str(n), 
						   code_str(c), equn_str(e), yaxis_str(y)
			{
			}
			
		public:
			/** Gets the name of the functor.
			 * 
			 * @return a name.
			 */
			const std::string& name()const
			{
				return name_str;
			}
			/** Gets the code of the functor.
			 * 
			 * @return a code.
			 */
			const std::string& code()const
			{
				return code_str;
			}
			/** Gets the equation of the functor.
			 * 
			 * @return an equation.
			 */
			const std::string& equation()const
			{
				return equn_str;
			}
			/** Gets the group of the functor.
			 * 
			 * @return a group.
			 */
			const std::string& group()const
			{
				return yaxis_str;
			}
			/** Gets the x-axis label of the functor.
			 * 
			 * @return a x-axis label.
			 */
			const std::string& xaxis()const
			{
				return equn_str;
			}
			/** Gets the y-axis label of the functor.
			 * 
			 * @return a y-axis label.
			 */
			const std::string& yaxis()const
			{
				return yaxis_str;
			}
			
		private:
			std::string name_str;
			std::string code_str;
			std::string equn_str;
			std::string yaxis_str;
		};
	};

	/** @brief Defines a measure factory.
	 * 
	 * This class defines a measure factory holding a collection of metric 
	 * or plot functors. This object is a singleton and can only be constructed
	 * by a call to the static instance method.
	 */
	template<class BP>
	class measure_factory : protected std::vector< detail::measure_functor< typename BP::function_type > >
	{
		typedef std::vector< detail::measure_functor< typename BP::function_type > > measure_vector;
	public:
		/** Defines a metric vector constant iterator as a constant iterator. **/
		typedef typename measure_vector::const_iterator const_iterator;
		/** Defines a metric vector constant reference as a constant reference. **/
		typedef typename measure_vector::const_reference const_reference;
		/** Defines a metric vector size type as a size type. **/
		typedef typename measure_vector::size_type size_type;
		/** Defines a metric vector value type as a value type. **/
		typedef typename measure_vector::value_type value_type;
		/** Defines a metric vector value type as a metric type. **/
		typedef typename measure_vector::value_type metric_type;
	
	protected:
		/** Constructs a measure factory.
		 * 
		 * @note cannot be called directly
		 */
		measure_factory()
		{
			BP::build(*this);
		}

	public:
		/** Instantiates a singleton instace of a measure factory.
		 * 
		 * @return a reference to a measure factory.
		 */
		static const measure_factory<BP>& instance()
		{
			static measure_factory<BP> factory;
			return factory;
		}
		/** Builds a option list for for an argument description.
		 * 
		 * @param pfx a prefix on the option list.
		 * @param idx the start index of the option list.
		 * @return a string option list.
		 */
		std::string options(const char* pfx, int idx=0)const
		{
			unsigned int len = (unsigned int)size();
			std::string ds=pfx, tmp;
			for(unsigned int i=0;i<len;++i,++idx)
			{
				valueToString(idx,tmp);
				ds+=measure_vector::operator[](i).name() + ":";
				ds+=tmp;
				if( i != (len-1) ) ds += ";";
			}
			return ds;
		}
		/** Get the number of measure functors.
		 * 
		 * @return number of measure functors.
		 */
		size_type size()const
		{
			return measure_vector::size();
		}
		/** Gets reference to measure functor.
		 * 
		 * @param n index of measure functor.
		 * @return measure functor.
		 */
		const_reference operator[](size_type n)const
		{
			return measure_vector::operator[](n);
		}
		/** Gets an iterator to a measure functor using a code.
		 * 
		 * @param cde a string code.
		 * @return iterator to measure functor.
		 */
		const_iterator findbycode(const std::string& cde)const
		{
			const_iterator beg=measure_vector::begin(), end=measure_vector::end();
			for(;beg != end;++beg) if( cde == beg->code() ) return beg;
			for(beg=measure_vector::begin();beg != end;++beg) if( cde == beg->name() ) return beg;
			return end;
		}
		/** Gets an iterator to first functor.
		 * 
		 * @return iterator to first functor.
		 */
		const_iterator begin()const
		{
			return measure_vector::begin();
		}
		/** Gets an iterator to after last functor.
		 * 
		 * @return iterator after last functor.
		 */
		const_iterator end()const
		{
			return measure_vector::end();
		}
	};

};


#endif




