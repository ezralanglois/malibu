/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * metric_plots.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_METRIC_PLOTS_H
#define _EXEGETE_METRIC_PLOTS_H
#include <cmath>
#include "float.h"
#include "memoryutil.h"
#include "mathutil.h"

/** @file metric_plots.hpp
 * @brief Contains a set of metric plots.
 * 
 * This file contains a set of global functions that generate graphs.
 *
 * @todo create general plot function, to plot any threshold metrics on either x or y axis
 * 
 * @ingroup ExegeteMeasure
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** Calculates the area under a curve built using the graph function pointer.
	 *
	 * \f$\sum_{i}{x_{i+1}-x_i * \frac{y_{i+1}+y_i}{2}}\f$
	 * 
	 * @todo average over higher and lower trapezoid
	 * 
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @param graph a pointer to a graph function.
	 * @return the trapizodal area under some curve.
	 */
	template<class I, class G>
	double auc(I beg, I end, G graph)
	{
		ASSERT(beg != end);
		double* px = new double[ std::distance(beg, end)+2 ], *px_beg=px;
		double* py = new double[ std::distance(beg, end)+2 ], *py_beg=py;
		double* px_end = graph(beg, end, px_beg, py_beg);
		double area = 0.0, xprev, yprev;
		if( px_beg != px_end )
		{
			xprev = *px_beg; ++px_beg;
			yprev = *py_beg; ++py_beg;
		}
		for(;px_beg != px_end;++px_beg, ++py_beg)
		{
			ASSERT(!std::isnan(*px_beg));
			ASSERT(!std::isnan(xprev));
			ASSERT(!std::isnan(*py_beg));
			ASSERT(!std::isnan(yprev));
			area+=trap_area(*px_beg, xprev, *py_beg, yprev);
			xprev = *px_beg;
			yprev = *py_beg;
		}
		delete[] px;
		delete[] py;
		return area;
	}
	/** Counts the number of positive examples.
	 *
	 * @param curr start iterator.
	 * @param end end iterator.
	 * @return positive count.
	 */
	template<class I>
	unsigned int countpos(I curr, I end)
	{
		unsigned int n=0;
		for(;curr != end;++curr) 
		{
			if( is_attribute_missing(curr->y()) ) continue;
			if( curr->y() > 0 ) n++;
		}
		return n;
	}
	/** Counts the number of negative examples.
	 *
	 * @param curr start iterator.
	 * @param end end iterator.
	 * @return negative count.
	 */
	template<class I>
	unsigned int countneg(I curr, I end)
	{
		unsigned int n=0;
		for(;curr != end;++curr) 
		{
			if( is_attribute_missing(curr->y()) ) continue;
			if( curr->y() < 1 ) n++;
		}
		return n;
	}
	/** Compares if the left prediction is greater than the right.
	 *
	 * @param p1 prediction on left of comparison.
	 * @param p2 prediction on right of comparison.
	 * @return true if the first prediction is greater than the second.
	 */
	template<class T>
	bool compare_pred(const T& p1, const T& p2)
	{
		return p1.p() > p2.p();
	}
	/** Copies a collection of predictions and sorts this collection.
	 *
	 * @param beg an iterator to the start of a collection.
	 * @param end an iterator to the end of a collection.
	 * @param nend a reference to an iterator to the end of new collection.
	 * @return an iterator to the start of a new collection.
	 */
	template<class I, class I2>
	I2 predsort(I beg, I end, I2& nend)
	{
		typedef typename TypeUtil<I2>::value_type value_type;
		I2 nbeg = setsize<value_type>(std::distance(beg,end));//end-beg);
		nend = nbeg + std::distance(beg,end);//(end-beg);
		std::copy(beg, end, nbeg);
		std::stable_sort(nbeg, nend, compare_pred<value_type>);
		return nbeg;
	}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

	/** Plots a lift curve from a set of predictions.
	 *
	 * Lift: \f$\frac{\frac{TP}{TP+FN}*(TP+FP+TN+FN)}{TP+FN}\f$
	 * 
	 * Prediction
	 * 	- Probabilistic confidence: \f$p_i \in [0,1]\f$
	 * 	- Class: \f$c_i \in \{0,1\}\f$
	 * 
	 * X-axis: Fraction of highest scoring \f$0 \leq \alpha \leq 1\f$
	 * Y-axis: Lift for fraction \f$L(\alpha) = \frac{p(\alpha)}{p(\alpha)+n(\alpha)} \times \frac{P+N}{P}\f$
	 * 
	 * @param obeg an iterator pointing to the start of a collection of predictions.
	 * @param oend an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates.
	 * @param py an array of y-coordinates.
	 * @return a pointer to the last x-coordinates.
	 */
	template<class I>
	double* lft_curve(I obeg, I oend, double* px, double* py)
	{
		typedef typename TypeUtil<I>::value_type value_type;
		typedef value_type* pointer;
		double tp=0, fp=0, pfp=0, ptp=0;
		double prev, prob;
		pointer end, beg, nbeg;

		ASSERT(obeg != oend);
		nbeg=beg=predsort(obeg, oend, end);
		for(pointer curr=beg;curr != end;++curr)
		{
			if( is_attribute_missing(curr->y()) ) continue;
			if( curr->y() > 0 ) ptp+=curr->w();
			else pfp+=curr->w();
		}
		if(beg != end) prev=beg->p();
		prob = (ptp+pfp) / (ptp);
		for(;beg != end;++beg)
		{
			if( is_attribute_missing(beg->y()) ) continue;
			if( beg->p() != prev )
			{
				*px = (tp+fp)/(ptp+pfp);
				*py = (tp)/(tp+fp)*prob;//double(ptp-tp)/double(ptp) * prob;
				prev = beg->p();
				++px;
				++py;
			}
			if( beg->y() > 0 ) tp+=beg->w();
			else fp+=beg->w();
		}
		erase(nbeg);
		return px;
	}
	/** Plots a precision/recall curve from a set of predictions.
	 *
	 * X-axis: Recall \f$\frac{TP}{TP+FN}\f$
	 * Y-axis: Precision \f$\frac{TP}{FP+TP}\f$
	 * 
	 * @param obeg an iterator pointing to the start of a collection of predictions.
	 * @param oend an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates.
	 * @param py an array of y-coordinates.
	 * @return a pointer to the last x-coordinates.
	 */
	template<class I>
	double* pr_curve(I obeg, I oend, double* px, double* py)
	{
		typedef typename TypeUtil<I>::value_type value_type;
		typedef value_type* pointer;
		unsigned int tp=0, fp=0, ltp=0, lfp=0, pos, curr;
		double prev, d, tmp;
		pointer end, beg=predsort(obeg, oend, end), nbeg=beg;

		ASSERT(obeg != oend);
		*px = 0.0; ++px;
		*py = 1.0; ++py;
		pos = countpos(beg, end);
		if( beg != end ) prev=beg->p();
		for(;beg != end;++beg)
		{
			if( is_attribute_missing(beg->y()) ) continue;
			if( beg->p() != prev && tp != ltp)
			{
				d = double(fp-lfp) / double(tp-ltp);
				for(curr=ltp+1;curr < tp;++curr)
				{
					tmp = double(curr+lfp+d*(curr-ltp));
					if( tmp != 0.0 )
					{
						*px = double(curr) / double(pos);
						*py = double(curr) / tmp;
						++px; ++py;
					}
				}
				*px = double(tp) / double(pos);
				*py = double(tp) / double(tp+fp);
				++px; ++py;
				ltp=tp;
				lfp=fp;
				prev = beg->p();
			}
			if( beg->y() > 0 ) tp++;
			else fp++;
		}
		*px = 1.0; ++px;
		*py = 0.0; ++py;
		erase(nbeg);
		return px;
	}
	
	/** Plots a inverse precision/recall curve from a set of predictions.
	 *
	 * X-axis: Specificity \f$\frac{TN}{FP+TN}\f$
	 * Y-axis: NPV \f$\frac{TN}{TN+FN}\f$
	 * 
	 * @param obeg an iterator pointing to the start of a collection of predictions.
	 * @param oend an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates.
	 * @param py an array of y-coordinates.
	 * @return a pointer to the last x-coordinates.
	 */
	template<class I>
	double* ipr_curve(I obeg, I oend, double* px, double* py)
	{
		typedef typename TypeUtil<I>::value_type value_type;
		typedef value_type* pointer;
		unsigned int tp=0, fp=0, ltp=0, lfp=0, pos, curr;
		double prev, d, tmp;
		pointer end, beg=predsort(obeg, oend, end), nbeg=beg;

		ASSERT(obeg != oend);
		*px = 0.0; ++px;
		*py = 1.0; ++py;
		pos = countneg(beg, end);
		if( beg != end ) prev=beg->p();
		for(;beg != end;++beg)
		{
			if( is_attribute_missing(beg->y()) ) continue;
			if( beg->p() != prev && tp != ltp)
			{
				d = double(fp-lfp) / double(tp-ltp);
				for(curr=ltp+1;curr < tp;++curr)
				{
					tmp = double(curr+lfp+d*(curr-ltp));
					if( tmp != 0.0 )
					{
						*px = double(curr) / double(pos);
						*py = double(curr) / tmp;
						++px; ++py;
					}
				}
				*px = double(tp) / double(pos);
				*py = double(tp) / double(tp+fp);
				++px; ++py;
				ltp=tp;
				lfp=fp;
				prev = beg->p();
			}
			if( beg->y() > 0 ) fp++;
			else tp++;
		}
		*px = 1.0; ++px;
		*py = 0.0; ++py;
		erase(nbeg);
		return px;
	}
	
	
	/** Plots a reliability diagram from a set of predictions.
	 *
	 * X-axis: 
	 * Y-axis: 
	 * 
	 * @todo Fix this plot
	 *
	 * @param obeg an iterator pointing to the start of a collection of predictions.
	 * @param oend an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates.
	 * @param py an array of y-coordinates.
	 * @return a pointer to the last x-coordinates.
	 */
	template<class I>
	double* red_curve(I obeg, I oend, double* px, double* py)
	{
		typedef typename TypeUtil<I>::value_type value_type;
		typedef value_type* pointer;
		double inc=0.1, val;//, min=DBL_MAX, max=-DBL_MAX;
		double mean=0.0;
		unsigned int tp=0, ptp=0, bs=0;
		pointer end, beg=predsort(obeg, oend, end), nbeg=beg;
		ptp = countpos(beg, end);
		//for(I curr=beg;curr != end;++curr)
		//{
		//	if( curr->y() > 0 ) ptp++;
		//	if( min > curr->p() ) min = curr->p();
		//	if( max < curr->p() ) max = curr->p();
		//}
		double prev;
		ASSERT(obeg != oend);
		--end;--beg;
		if( beg != end ) prev=end->p();//((end->p()-min)/(max-min)) + inc;
		for(;beg != end;--end)
		{
			if( is_attribute_missing(beg->y()) ) continue;
			val = end->p();//(end->p()-min)/(max-min);
			mean+=val;
			bs++;
			if( val > prev )
			{
				*py = double(tp) / double(ptp);
				*px = mean/double(bs);
				prev = val+inc;
				++px;
				++py;
				mean=0.0;
				bs=0;
			}
			if( end->y() > 0 ) tp++;
		}
		//if( *(px-1) < 1.0 || *(py-1) < 1.0 )
		//{
		//	*px = 1.0;
		//	*py = 1.0;
		//	++px;
		//	++py;
		//}
		erase(nbeg);
		return px;
	}

	/** Plots a recieving operator characteristic curve from a set of predictions.
	 *
	 * X-axis: False Positive Rate \f$\frac{FP}{FP+TN}\f$
	 * Y-axis: True Positive Rate \f$\frac{TP}{TP+FN}\f$
	 *
	 * @param obeg an iterator pointing to the start of a collection of predictions.
	 * @param oend an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates.
	 * @param py an array of y-coordinates.
	 * @return a pointer to the last x-coordinates.
	 */
	template<class I>
	double* roc_curve(I obeg, I oend, double* px, double* py)
	{
		typedef typename TypeUtil<I>::value_type value_type;
		typedef value_type* pointer;
		double tp=0, fp=0, pfp=0, ptp=0;
		double prev=-DBL_MAX;
		pointer end, beg, nbeg;
		ASSERT(obeg != oend);
		nbeg=beg=predsort(obeg, oend, end);
		for(pointer curr=beg;curr != end;++curr)
		{
			if( is_attribute_missing(curr->y()) ) continue;
			if( curr->y() > 0 ) ptp+=curr->w();
			else pfp+=curr->w();
		}
		ASSERT(beg != end);
		for(;beg != end;++beg)
		{
			if( is_attribute_missing(beg->y()) ) continue;
			if( beg->p() != prev )
			{
				*px = divide(double(fp), double(pfp));
				*py = divide(double(tp), double(ptp));
				prev = beg->p();
				++px;
				++py;
			}
			if( beg->y() > 0 ) tp+=beg->w();
			else fp+=beg->w();
		}
		if( *(px-1) < 1.0 || *(py-1) < 1.0 )
		{
			*px = 1.0;
			*py = 1.0;
			++px;
			++py;
		}
		erase(nbeg);
		return px;
	}
	
	/** Plots y-coordinates for a cost curve from a set of predictions.
	 *
	 * X-axis: Probability of Positive (Actually holds False positive rate)
	 * Y-axis: Normalized Expected Cost (Actually holds False negative rate)
	 * 
	 * @todo Properly document
	 *
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @param px an array of y1-coordinates False Positive Rate.
	 * @param py an array of y2-coordinates 1 - True Positive Rate.
	 * @return a pointer to the last y1-coordinates.
	 */
	template<class I>
	double* cst_curve(I beg, I end, double* px, double* py)
	{
		double* end2 = roc_curve(beg, end, px, py);
		for(double* pbeg=px;pbeg != end2;++pbeg, ++py) (*py) = (1.0-(*py));
		return end2;
	}
	/** Plots the lower envelope of the cost curve with a few cost lines.
	 *
	 * X-axis: Probability of Positive 
	 * Y-axis: Normalized Expected Cost
	 * 
	 * @todo Properly document
	 *
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates Probability of Postive.
	 * @param py an array of y-coordinates 1 - True Positive Rate and False Positive Rate.
	 * @return a pointer to the last y-coordinates.
	 */
	template<class I>
	double* env_curve_ex(I beg, I end, double* px, double* py)
	{
		unsigned int n = (unsigned int)(*px);
		double* xend1 = cst_curve(beg, end, px, py);
		double* xsav2 = new double[xend1-px];
		double* ysav2 = new double[xend1-px];
		double* xend2 = xsav2+(xend1-px);
		std::copy(px, xend1, xsav2);
		std::copy(py, py+(xend1-px), ysav2);
		double* xbeg2=xsav2, *xcur2=xsav2;
		double* ybeg2=ysav2, *ycur2=ysav2;
		double x, last_x=DBL_MAX;
		double y, last_y, m, b;
		double f = 1.0/double(n);
		double y1, x1;

		x = 0.0;
		if(  n > 0 )
		{
			*px = 0.0;++px;
			*py = 1.0;++py;
		}
		for(unsigned int i=1;i<n;++i)
		{
			y = DBL_MAX;
			for(xcur2=xsav2,ycur2=ysav2;xcur2 != xend2;++xcur2, ++ycur2)
			{
				b = (*xcur2);
				m = (*ycur2) - b;
				last_y = m*x + b;
				if( last_y < y )
				{
					y = last_y;
					x1 = *xcur2;
					y1 = *ycur2;
				}
			}
			*px = x1;++px;
			*py = y1;++py;
			x+=f;
		}
		if(  n > 0 )
		{
			--px;*px = 1.0;++px;
			--py;*py = 0.0;++py;
		}

		for(;xbeg2 != xend2;++xbeg2, ++ybeg2)
		{
			x = *xbeg2;
			if( x != last_x )
			{
				y = DBL_MAX;
				for(xcur2=xsav2,ycur2=ysav2;xcur2 != xend2;++xcur2, ++ycur2)
				{
					b = (*xcur2);
					m = (*ycur2) - b;
					last_y = m*x + b;
					if( last_y < y ) y = last_y;
				}
				*px = x;++px;
				*py = y;++py;
				last_x = x;
			}
		}
		delete[] xsav2;
		delete[] ysav2;
		return px;
	}
	
	/** Plots the lower envelope of the cost curve.
	 *
	 * X-axis: Probability of Positive
	 * Y-axis: Normalized Expected Cost
	 * 
	 * @todo Properly document
	 *
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates Probability of Postive.
	 * @param py an array of y-coordinates 1 - True Positive Rate and False Positive Rate.
	 * @return a pointer to the last y-coordinates.
	 */
	template<class I>
	double* env_curve(I beg, I end, double* px, double* py)
	{
		double* xend1 = cst_curve(beg, end, px, py);
		double* xsav2 = new double[xend1-px];
		double* ysav2 = new double[xend1-px];
		double* xend2 = xsav2+(xend1-px);
		std::copy(px, xend1, xsav2);
		std::copy(py, py+(xend1-px), ysav2);
		double* xbeg2=xsav2, *xcur2=xsav2;
		double* ybeg2=ysav2, *ycur2=ysav2;
		double x, last_x=DBL_MAX;
		double y, last_y, m, b;
		for(;xbeg2 != xend2;++xbeg2, ++ybeg2)
		{
			x = *xbeg2;
			if( x != last_x )
			{
				y = DBL_MAX;
				for(xcur2=xsav2,ycur2=ysav2;xcur2 != xend2;++xcur2, ++ycur2)
				{
					b = (*xcur2);
					m = (*ycur2) - b;
					last_y = m*x+b;
					if( last_y < y ) y = last_y;
				}
				*px = x;++px;
				*py = y;++py;
				last_x = x;
			}
		}
		delete[] xsav2;
		delete[] ysav2;
		return px;
	}

	/** Plots the specified plot and takes care of memory allocation.
	 *
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates.
	 * @param py an array of y-coordinates.
	 * @param plot a functor to a plot to generate.
	 * @return size of coordinate arrays.
	 */
	template<class I, class G>
	inline unsigned int plot_curve(I beg, I end, double*& px, double*& py, G plot)
	{
		ASSERT(beg != end);
		px = resize(px, std::distance( beg, end) + 4);
		py = resize(py, std::distance( beg, end) + 4);
		double* pn = plot(beg, end, px, py);
		return (unsigned int)std::distance(px, pn);
	}

	/** Plots the specified plot and takes care of memory allocation and sets 
	 * the curve count parameter.
	 *
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @param px an array of x-coordinates.
	 * @param py an array of y-coordinates.
	 * @param plot a functor to a plot to generate.
	 * @param n the number of curves to draw.
	 * @return size of coordinate arrays.
	 */
	template<class I, class G>
	inline unsigned int plot_curve(I beg, I end, double*& px, double*& py, G plot, unsigned int n)
	{
		ASSERT(beg != end);
		px = resize(px, std::distance( beg, end ) + 4 + n);
		py = resize(py, std::distance( beg, end ) + 4 + n);
		*px = n;
		double* pn = plot(beg, end, px, py);
		return (unsigned int)std::distance(px, pn);
	}

};


#endif



