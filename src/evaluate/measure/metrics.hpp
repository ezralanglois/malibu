/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * metrics.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_METRICS_HPP
#define _EXEGETE_METRICS_HPP
#include "charutil.h"
#include "threshold_metrics.hpp"
#include "metric_plots.hpp"


/** @file metrics.hpp
 * @brief Contains a set of binary order and regression metrics
 * 
 * This file contains a set of functions to calculate binary 
 * order and regression metrics.
 *
 *
 * Order Metrics built using confidence-rated predictions.
 * <p>
 * For some curve, \f$f(x)\f$
 * 
 * \f$\sum_{i}{x_{i+1}-x_i * \frac{f(x_{i+1})+f(x_i)}{2}}\f$
 * </p>
 *
 * Regression Metrics built using probabilistic predictions.
 * <p>
 * \f$Root\:mean\:square\:error = \frac{1}{N}\displaystyle\sum_{i=0}^N (p_i-c_i)^2\f$ <br>
 * \f$Mean\:cross\:entropy = \frac{1}{N}\displaystyle\sum_{i=0}^N (T*log(p_i) + (1-T)*log(1-p_i))\f$ <br>
 * where \f$T\in{0,1}\f$
 * </p>
 *
 * 
 * @ingroup ExegeteMeasure
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{	
	/** Calculates the area under a ROC curve.
	 *
	 * @see roc_curve
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @return area under ROC.
	 */
	template<class I>
	double roca(I beg, I end)
	{
		return auc(beg, end, roc_curve<I>);
	}
	/** Calculates the area under a Lift curve.
	 *
	 * @see lft_curve
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @return area under Lift.
	 */
	template<class I>
	double lfta(I beg, I end)
	{
		return auc(beg, end, lft_curve<I>);
	}
	/** Calculates the area under a precision/recall curve.
	 *
	 * @see pr_curve
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @return area under precision/recall.
	 */
	template<class I>
	double prca(I beg, I end)
	{
		return auc(beg, end, pr_curve<I>);
	}
	/** Calculates the area under a negative predictive value/specificity curve.
	 *
	 * @see pr_curve
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @return area under negative predictive value/specificity.
	 */
	template<class I>
	double ipra(I beg, I end)
	{
		return auc(beg, end, ipr_curve<I>);
	}
	/** Calculates the average area under precision/recall curves.
	 *
	 * @see ipra
	 * @see prca
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @return verage area under precision/recall curves.
	 */
	template<class I>
	double bpra(I beg, I end)
	{
		return (ipra(beg,end)+prca(beg,end))/2.0;
	}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

	/** Averages the first value in a collection of predictions.
	 *
	 * \f$\frac{1}{N}\displaystyle\sum_{i=0}^N x_i\f$
	 *
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @return the average value.
	 */
	template<class I>
	inline double avg_first(I beg, I end)
	{
		double avg=0.0;
		double tot=double(std::distance(beg,end));
		for(;beg != end;++beg) 
		{
			if( is_attribute_missing(beg->y()) ) continue;
			avg+=beg->y();
		}
		return avg / tot;
	}
	

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/** Calculates the root mean squared error for a set of predictions.
	 *
	 * \f$\frac{1}{N}\displaystyle\sum_{i=0}^N (p_i-c_i)^2\f$
	 *
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @return root mean squared error.
	 */
	template<class I>
	double rmse(I beg, I end)
	{
		double sse = 0.0;
		double tot = double(std::distance(beg, end));
		for(;beg != end;++beg) 
		{
			if( is_attribute_missing(beg->y()) ) continue;
			sse+=square(double(beg->y()-beg->p()));
		}
		return std::sqrt( sse / tot );
	}
	/** Calculates the mean cross-entropy for a set of predictions.
	 *
	 * \f$\frac{1}{N}\displaystyle\sum_{i=0}^N (T*log(p_i) + (1-T)*log(1-p_i))\f$
	 * where \f$T\in{0,1}\f$
	 *
	 * @param beg an iterator pointing to the start of a collection of predictions.
	 * @param end an iterator pointing to the end of a collection of predictions.
	 * @return mean cross-entropy.
	 */
	template<class I>
	double mcxe(I beg, I end)
	{
		double cxe = 0.0;
		double tot = double(std::distance(beg, end)), tmp;
		double log2 = 1.0; // /std::log10(2.0);
		double thd=avg_first(beg,end);
		cxe=0.0f;
		for(;beg != end;++beg)
		{
			if( is_attribute_missing(beg->y()) ) continue;
			tmp = (beg->y() < thd) ? (1.0 - beg->p()) : beg->p();
			if( tmp < 0 )  break;
			else cxe += -std::log(tmp)*log2;//log10
		}
		return (beg==end) ? cxe / tot : 9e22;
	}
};


#endif



