/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * multiclass_metrics.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_MULTICLASS_METRICS_HPP
#define _EXEGETE_MULTICLASS_METRICS_HPP
#include "charutil.h"
#include "threshold_metrics.hpp"
#include "prediction_set.hpp"


/** @file multiclass_metrics.hpp
 * @brief Contains a set of multi-class metrics
 * 
 * This file contains a set of functions to calculate multi-class metrics
 * including one-versus-one (OVO), one-versus-all (OVA) threshold metrics,
 * multi-class regression metrics, multi-class accuracy and row counts.
 *
 *
 * @todo continue to document
 * @todo need to fix doxygen warnings ----- HERE
 *
 * 
 * @ingroup ExegeteMeasure
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** Defines a one-versus-all (OVA) threshold metric calculated over a 
 * collection of predictions.
 * 
 * @note creates function named ova_{suffix} with the following arguments
 * 	- beg: an iterator to start of prediction collection.
 * 	- end: an iterator to end of prediction collection.
 * 	- cl: number of classes.
 * 	- returns: threshold metric.
 * 
 * @param suffix name of threshold metric.
 */
#define OVA(suffix)												        	\
	template<class I> double ova_##suffix(I beg, I end, unsigned int cl)	\
	{																		\
		typedef unsigned int uint;											\
		double tp=0, fp=0, tn=0, fn=0;										\
		double ctp, cfp, ctn, cfn;											\
		for(unsigned int c=0;c<cl;++c)										\
		{																	\
			ctp = cfp = ctn = cfn = 0;										\
			for(I curr=beg;curr != end;++curr)								\
			{																\
				if( is_attribute_missing(curr->y()) ) continue;				\
				if( uint(curr->y()) == c )									\
				{															\
					if( uint(curr->best_index(cl)) == c ) ctp+=curr->w();	\
					else cfn+=curr->w();									\
				}															\
				else														\
				{															\
					if( uint(curr->best_index(cl)) == c ) cfp+=curr->w();	\
					else ctn+=curr->w();									\
				}															\
			}																\
			if( (ctp+cfn) > 0.0f && (cfp+ctn) > 0.0f )						\
			{																\
				tp+=ctp;													\
				fn+=cfn;													\
				fp+=cfp;													\
				tn+=ctn;													\
			}																\
		}																	\
		return suffix(tp, tn, fp, fn);										\
	}

/** Defines a one-versus-one (OVO) threshold metric calculated over a 
 * collection of predictions.
 * 
 * @note creates function named ovo_{suffix} with the following arguments
 * 	- beg: an iterator to start of prediction collection.
 * 	- end: an iterator to end of prediction collection.
 * 	- cl: number of classes.
 * 	- returns: threshold metric.
 * 
 * @param suffix name of threshold metric.
 */
#define OVO(suffix)												         		\
	template<class I> double ovo_##suffix(I beg, I end, unsigned int cl) 		\
	{																	 		\
		typedef unsigned int uint;										 		\
		double tp=0, fp=0, tn=0, fn=0;									 		\
		double ctp, cfp, ctn, cfn;										 		\
		ASSERT(beg != end);												 		\
		for(unsigned int i=0,j;i<cl;++i)								 		\
		{																 		\
			for(j=i+1;j<cl;++j)											 		\
			{															 		\
				ctp = cfp = ctn = cfn = 0;								 		\
				for(I curr=beg;curr != end;++curr)						 		\
				{														 		\
					if( is_attribute_missing(curr->y()) ) continue;		 		\
					if( uint(curr->y()) != i && uint(curr->y()) != j ) continue;\
					if( uint(curr->y()) == i )							 		\
					{														 	\
						if( uint(curr->best_index(cl)) == i ) ctp+=curr->w();  	\
						else cfn+=curr->w();							 		\
					}													 		\
					else 												 		\
					{													 		\
						if( uint(curr->best_index(cl)) == i ) cfp+=curr->w();  	\
						else ctn+=curr->w();							 		\
					}													 		\
				}														 		\
				if( (ctp+cfn) > 0.0f && (cfp+ctn) > 0.0f )				 		\
				{														 		\
					tp+=ctp;											 		\
					fn+=cfn;											 		\
					fp+=cfp;											 		\
					tn+=ctn;											 		\
				}														 		\
			}															 		\
		}																 		\
		return suffix(tp, tn, fp, fn);									 		\
	}

/** Defines a one-versus-one (OVO) order metric calculated over a 
 * collection of predictions.
 * 
 * @note creates function named ovo_{suffix} with the following arguments
 * 	- beg: an iterator to start of prediction collection.
 * 	- end: an iterator to end of prediction collection.
 * 	- cl: number of classes.
 * 	- returns: order metric.
 * 
 * @param suffix name of order metric.
 */
#define AOVO(suffix)														\
	template<class I> double ovo_##suffix(I beg, I end, unsigned int cl)	\
	{																		\
		typedef typename TypeUtil<I>::value_type value_type;				\
		typedef typename value_type::float_type float_type;					\
		typedef typename value_type::class_type class_type;					\
		prediction_set<float_type,class_type> tmp;							\
		double avg = 0.0;													\
		unsigned int k=0;													\
		for(unsigned int i=0,j;i<cl;++i)									\
		{												 					\
			for(j=i+1;j<cl;++j)												\
			{																\
				if(!tmp.copy(beg, end, cl, i, j)) continue;					\
				avg+=suffix(tmp.begin(), tmp.end());						\
				ASSERT(!std::isnan(avg));									\
				k++;														\
			}																\
		}																	\
		if( k == 0 ) return 0;												\
		return avg / double(k);												\
	}					

/** Defines a one-versus-all (OVA) order metric calculated over a 
 * collection of predictions.
 * 
 * @note creates function named ova_{suffix} with the following arguments
 * 	- beg: an iterator to start of prediction collection.
 * 	- end: an iterator to end of prediction collection.
 * 	- cl: number of classes.
 * 	- returns: order metric.
 * 
 * @param suffix name of order metric.
 */
#define AOVA(suffix)														\
	template<class I> double ova_##suffix(I beg, I end, unsigned int cl)	\
	{																		\
		typedef typename TypeUtil<I>::value_type value_type;				\
		typedef typename value_type::float_type float_type;					\
		typedef typename value_type::class_type class_type;					\
		prediction_set<float_type,class_type> tmp;							\
		double avg = 0.0;													\
		for(unsigned int i=0;i<cl;++i)										\
		{																	\
			if( !tmp.copy(beg, end, cl, i) ) continue;						\
			avg+=suffix(tmp.begin(), tmp.end()) / double(cl);				\
		}																	\
		return avg;															\
	}

/** Creates a new function for each of the threshold metrics:
 * 	- Sensitivity
 * 	- Precision
 * 	- Lift
 * 	- F-score
 * 	- Matthew's Correlation Coefficient 
 *
 * @param suffix a function macro.
 */
#define MULTI_CLASS_THRESH(suffix)\
	suffix(sen)\
	suffix(pre)\
	suffix(lft)\
	suffix(fsc)\
	suffix(mcc)

/** Creates a new function for each of the order metrics:
 * 	- ROC
 * 	- Lift
 * 	- Precision/Recall
 * 	- Negative predictive value/Specificity
 * 	- Average Precision/Recall
 *
 * @param suffix a function macro.
 */
#define MULTI_CLASS_RANK(suffix)\
	suffix(roca)\
	suffix(lfta)\
	suffix(prca)\
	suffix(ipra)\
	suffix(bpra)


namespace exegete
{
	/** Counts the number of predictions for each predicted class in the given class row.
	 * 
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @param px output counts.
	 * @param cl number of classes.
	 * @param row specified row.
	 */
	template<class I>
	inline void mc_cont_row(I beg, I end, double* px, unsigned int cl, unsigned int row)
	{
		typedef unsigned int uint;
		std::fill(px, px+cl, 0.0);
		for(;beg != end;++beg)
		{
			if( uint(beg->y()) == row )
				px[uint(beg->best_index(cl))]+=beg->w();
		}
	}
	/** Calculates the multi-class accuracy.
	 * 
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @param cl number of classes.
	 * @return multi-class accuracy.
	 */
	template<class I>
	inline double mc_acc(I beg, I end, unsigned int cl)
	{
		typedef unsigned int uint;
		double tp=0.0f, n=0.0f;
		for(;beg != end;++beg)
		{
			if( uint(beg->y()) == uint(beg->best_index(cl)) ) tp+=beg->w();
			n+=beg->w();
		}
		return divide(tp, n);
	}
	/** Calculates the multi-class root mean square error.
	 * 
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @param cl number of classes.
	 * @return multi-class root mean square error.
	 */
	template<class I>
	double mc_rmse(I beg, I end, unsigned int cl)
	{
		typedef unsigned int uint;
		double sse = 0.0;
		double tot = double(std::distance(beg, end));
		unsigned int bst;
		for(;beg != end;++beg) 
		{
			if( is_attribute_missing(beg->y()) ) continue;
			bst = uint(beg->best_index(cl));
			sse+=square(double(1.0f - beg->p_at(bst)));
		}
		return std::sqrt( divide(sse, tot) );
	}
	/** Calculates the multi-class cross entropy.
	 * 
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @param cl number of classes.
	 * @return multi-class cross entropy.
	 */
	template<class I>
	double mc_mcxe(I beg, I end, unsigned int cl)
	{
		typedef unsigned int uint;
		double cxe=0.0;
		double tot = double(std::distance(beg, end)), tmp;
		double log2=1.0/std::log10(2.0);
		unsigned int bst;
		cxe=0.0f;
		for(;beg != end;++beg)
		{
			if( is_attribute_missing(beg->y()) ) continue;
			bst = uint(beg->best_index(cl));
			tmp = beg->p_at(bst);
			if( tmp < 0 )  break;
			if( bst != uint(beg->y()) ) tmp = 1.0f - tmp;
			cxe += -std::log10(tmp)*log2;//log10
				
		}
		return (beg==end) ? cxe / tot : 9e22;
	}

	/** Defines a set of one-versus-all threshold metrics.
	 */
	MULTI_CLASS_THRESH(OVA)
	/** Defines a set of one-versus-one threshold metrics.
	 */
	MULTI_CLASS_THRESH(OVO)

	/** Defines a set of one-versus-all order metrics.
	 */
	MULTI_CLASS_RANK(AOVA)
	/** Defines a set of one-versus-one order metrics.
	 */
	MULTI_CLASS_RANK(AOVO)
	
///////////////////////////////////////////////////////////////////////////
	
	/** Estimates the normalized cost for a structure learning algorithm.
	 * 
	 * @note This algorithm uses the first and second prediction from multiclass.
	 * 
	 * @param beg start of bags (or structured example)
	 * @param end end of bags (or structured example)
	 * @param cl dummy
	 */
	template<class I>
	double normalized_cost(I beg, I end, unsigned int cl)
	{
		double sum=0.0, tot=0.0;
		for(;beg != end;++beg)
		{
			sum += beg->p_at(0);
			tot += beg->p_at(1);
		}
		return sum/tot;
	}
	/** Estimates the normalized optimal (1-cost) for a structure learning algorithm.
	 * 
	 * @note This algorithm uses the first and second prediction from multiclass.
	 * 
	 * @param beg start of bags (or structured example)
	 * @param end end of bags (or structured example)
	 * @param cl dummy
	 */
	template<class I>
	double normalized_optimal(I beg, I end, unsigned int cl)
	{
		return 1.0 - normalized_cost(beg, end, cl);
	}
	/** Estimates the unnormalized cost for a structure learning algorithm.
	 * 
	 * @note This algorithm uses the first prediction from multiclass.
	 * 
	 * @param beg start of bags (or structured example)
	 * @param end end of bags (or structured example)
	 * @param cl dummy
	 */
	template<class I>
	double unnormalized_cost(I beg, I end, unsigned int cl)
	{
		double sum=0.0;
		for(;beg != end;++beg)
		{
			sum += beg->p_at(0);
		}
		return sum;
	}
	/** Estimates the unnormalized cost for a structure learning algorithm.
	 * 
	 * @note This algorithm uses the first prediction from multiclass.
	 * 
	 * @param beg start of bags (or structured example)
	 * @param end end of bags (or structured example)
	 * @param cl dummy
	 */
	template<class I>
	double total_cost(I beg, I end, unsigned int cl)
	{
		double sum=0.0;
		for(;beg != end;++beg)
		{
			sum += beg->p_at(1);
		}
		return sum;
	}
};


#endif



