/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * measure_binding.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_MEASURE_BINDING_HPP
#define _EXEGETE_MEASURE_BINDING_HPP
#include <boost/python.hpp>
#include <map>

/** @file measure_binding.hpp
 * @brief Binds a measure factory to python
 * 
 * This file contains the measure binding class template.
 *
 * @ingroup ExegeteMeasure
 * @ingroup ExegetePython
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @defgroup ExegeteMeasure Measure System
 * 
 *  This group holds all the measure related classes and function files.
 */

/** @defgroup ExegetePython Python Binding System
 * 
 *  This group holds all the python binding related classes and function files.
 */

namespace exegete
{
	/** @brief Binds a measure factory to python
	 * 
	 * This class defines a binding for a measure factory to python. It adds
	 * the following functions:
	 * 	- avg_metric: calculates an average metric from the given record for the given name.
	 * 	- metric: calculates a metric from the given prediction set for the given name.
	 * 	- metric_names: gets a list of metric names.
	 * 	- metric_groups: gets a tuple list of metric groups and corresponding names
	 * 	- plots: gets a list of plots
	 * 	- plot: plots the first prediction set from a record for a plot name.
	 */
	template<class M, class P>
	class measure_binding : public ::boost::python::def_visitor< measure_binding<M,P> >
	{
		typedef M 												measure_type;
		typedef typename measure_type::array 					metric_array_measure;
		typedef typename measure_type::full 					metric_measure;
		typedef typename measure_type::plot 					plot_measure;
		typedef typename plot_measure::const_iterator 			const_plot_iterator;
		typedef typename metric_array_measure::const_iterator 	const_array_iterator;
		typedef typename metric_measure::const_iterator 		const_metric_iterator;
	private:
		typedef P 											prediction_record_set;
		typedef typename prediction_record_set::value_type	prediction_record;
		typedef typename prediction_record::value_type		prediction_set;
		typedef typename prediction_set::value_type			prediction;
		typedef typename prediction_record::const_iterator 	const_record_iterator;
	public:
		/** Binds a set of functions to the measure factory.
		 * 
		 * @param cl a python class binding.
		 */
        void visit(::boost::python::class_<M>& cl)const
        {
        	cl
				.def("avg_metric", &avg_metric)
				.def("metric", &metric)
				.def("metric_names", &metric_names)
				.def("metric_groups", &metric_groups)
				.def("plots", &plots)
				.def("plot", &plot)
			;
        }
        
	private:
		static ::boost::python::list metric_groups(measure_type& m)
		{
			::boost::python::list list;
			const_metric_iterator beg = metric_measure::instance().begin();
			const_metric_iterator end = metric_measure::instance().end();
			std::map<std::string, std::vector<std::string> > map;
			for(;beg != end;++beg) map[beg->group()].push_back(beg->name());
			for(typename std::map<std::string, std::vector<std::string> >::iterator gbeg=map.begin(), gend=map.end();gbeg != gend;++gbeg)
			{
				::boost::python::list tmp;
				for(typename std::vector<std::string>::iterator nbeg=gbeg->second.begin(), nend=gbeg->second.end();nbeg != nend;++nbeg)
				{
					tmp.append( ::boost::python::str( *nbeg ) );
				}
				list.append( ::boost::python::make_tuple( ::boost::python::str(gbeg->first), tmp) );
			}
			return list;
		}
		static ::boost::python::list plot(measure_type& m, prediction_record& record, ::boost::python::str type, int n)
		{
			static double *px=0, *py=0;
			unsigned int r_size;
			::boost::python::list list, xlist, ylist;
			
			std::string nm = ::boost::python::extract<const char*>(type)();
			const_plot_iterator pplot = plot_measure::instance().findbycode(nm);
			if( record.size() == 0 ) return list;
			if( pplot == plot_measure::instance().end() )
			{
				python_error(PyExc_StandardError, ERRORMSG("Cannot find plot by the name of \"" << nm << "\""));
			}
			//inplace_merge(start, start + 3, end) ;
			r_size = pplot->plot(record[0].begin(), record[0].end(), px, py, n);
			for(unsigned int i=0;i<r_size;++i)
			{
				xlist.append(px[i]);
				ylist.append(py[i]);
			}
			list.append(xlist);
			list.append(ylist);
			return list;
		}
		static ::boost::python::list plots(measure_type& m)
		{
			::boost::python::list list;
			const_plot_iterator beg = plot_measure::instance().begin();
			const_plot_iterator end = plot_measure::instance().end();
			for(;beg != end;++beg)
			{
				list.append( ::boost::python::make_tuple(
						::boost::python::str(beg->name()),
						::boost::python::str(beg->xaxis()),
						::boost::python::str(beg->yaxis()),
						::boost::python::str(beg->code())
				) );
			}
			return list;
		}
		static ::boost::python::list metric_names(measure_type& m)
		{
			::boost::python::list list;
			const_metric_iterator beg = metric_measure::instance().begin();
			const_metric_iterator end = metric_measure::instance().end();
			for(;beg != end;++beg)
			{
				list.append( ::boost::python::str(beg->name()) );
			}
			return list;
		}
		static double avg_metric(measure_type& m, prediction_record& record, ::boost::python::str type)
		{
			std::string nm = ::boost::python::extract<const char*>(type)();
			const_metric_iterator pmetric = metric_measure::instance().findbycode(nm);
			if( pmetric == metric_measure::instance().end() )
			{
				python_error(PyExc_StandardError, ERRORMSG("Cannot find metric by the name of \"" << nm << "\""));
			}
			double sum=0.0;
			for(const_record_iterator beg=record.begin(), end=record.end();beg != end;++beg)
				sum+=pmetric->calculate(beg->begin(), beg->end(), beg->class_count());
			return sum / double(record.size());
		}
		static double metric(measure_type& m, prediction_set& set, ::boost::python::str type)
		{
			std::string nm = ::boost::python::extract<const char*>(type)();
			const_metric_iterator pmetric = metric_measure::instance().findbycode(nm);
			if( pmetric == metric_measure::instance().end() )
			{
				python_error(PyExc_StandardError, ERRORMSG("Cannot find metric by the name of \"" << nm << "\""));
			}
			return pmetric->calculate(set.begin(), set.end(), set.class_count());
		}
		
	private:
		static void python_error(PyObject *type, const char* msg)
		{
            PyErr_SetString(type, msg);
            ::boost::python::throw_error_already_set();
		}
	};

};

#endif


