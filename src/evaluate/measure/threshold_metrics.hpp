/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * threshold_metrics.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_THRESHOLDMETRICS_HPP
#define _EXEGETE_THRESHOLDMETRICS_HPP
#include <cmath>
#include "mathutil.h"
#include "AttributeTypeUtil.h"


/** @file threshold_metrics.hpp
 * @brief Calculates threshold metrics.
 * 
 * This file contains a set of functions to calculate threshold metrics.
 *			
 * 
 * 		
  <table  class="confusion" width="175" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="48" class="confusiontd">&nbsp;</td>
      <td width="67" class="confusiontd">Positive</td>
      <td width="70" class="confusiontd">Negative</td>
    </tr>
    <tr>
      <td class="confusiontd">Postive</td>
      <td class="confusiontd"><div align="center">TP</div></td>
      <td class="confusiontd"><div align="center">FN</div></td>
    </tr>
    <tr>
      <td class="confusiontd">Negative</td>
      <td class="confusiontd"><div align="center">FP</div></td>
      <td class="confusiontd"><div align="center">TN</div></td>
    </tr>
  </table>
 *
 * <ul>
 * <li>True Positive  (TP)</li>
 * <li>False Positive (FP)</li>
 * <li>True Negative  (TN)</li>
 * <li>False Negative (FN)</li>
 * </ul>
 *
 * Threshold Metrics built using the above (single) confusion matrix.
 * <p>
 * \f$Accuracy = \frac{TP+TN}{TP+TN+FP+FN}\f$ <br>
 * \f$Sensitivity = \frac{TP}{TP+FN}\f$ <br>
 * \f$Specificity = \frac{TN}{FP+TN}\f$ <br>
 * \f$False\:Positive Rate = \frac{FP}{FP+TN}\f$ <br>
 * \f$Precision = \frac{TP}{FP+TP}\f$ <br>
 * \f$Negative\:Predictive Value = \frac{TN}{TN+FN}\f$ <br>
 * \f$Lift = \frac{\frac{TP}{TP+FN}*(TP+FP+TN+FN)}{TP+FN}\f$ <br>
 * \f$F-score = \frac{2.0*PRE*RCL}{PRE+RCL}\f$ <br>
 * \f$Net\:Precision = \frac{1}{2}(PRE+NPV)\f$ <br>
 * \f$Net\:Accuracy = \frac{1}{2}(SEN+PRE)\f$ <br>
 * \f$Matthew's\:Correlation\:Coefficient = \frac{TP*TN-FP*FN}{\sqrt{(TN+FN)(TN+FP)(TP+FN)(TP+FP)}}\f$ <br>
 * \f$Base\:Error\:Rate = \frac{1}{2}(\frac{FN}{TP+FN}+\frac{FP}{TN+FP})\f$
 * </p>
 *
 * 
 * @ingroup ExegeteMeasure
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** Defines a function that calculates some threshold metric from a tabulated
 * confusion matrix.
 *
 * @note creates function named threshold_{suffix} with the following arguments
 * 	- beg: an iterator to start of prediction collection.
 * 	- end: an iterator to end of prediction collection.
 * 	- returns: threshold metric.
 * 
 * @param suffix the functor and function name suffix.
 */
#define THRESHOLD1(suffix)												\
	template<class I> double threshold_##suffix(I beg, I end)			\
	{																	\
		double tp=0, fp=0, tn=0, fn=0;									\
		ASSERT(std::distance(beg, end) > 0);		 					\
		for(;beg != end;++beg)											\
		{																\
			if( is_attribute_missing(beg->y()) ) continue;				\
			if( beg->y() > 0 )											\
			{															\
				if( beg->p() > beg->t() ) tp+=beg->w();					\
				else fn+=beg->w();										\
			}															\
			else														\
			{															\
				if( beg->p() > beg->t() ) fp+=beg->w();					\
				else tn+=beg->w();										\
			}															\
		}																\
		return suffix(tp, tn, fp, fn);									\
	}

/** Defines a function that calculates some threshold metric from a tabulated
 * confusion matrix.
 *
 * @note creates function named threshold_{suffix} with the following arguments
 * 	- beg: an iterator to start of prediction collection.
 * 	- end: an iterator to end of prediction collection.
 *	- th: a threshold of prediction.
 * 	- returns: threshold metric.
 *
 * @param suffix the functor and function name suffix.
 */
#define THRESHOLD2(suffix)												\
	template<class I> double threshold_##suffix(I beg, I end, float th)	\
	{																	\
		double tp=0, fp=0, tn=0, fn=0;									\
		for(;beg != end;++beg)											\
		{																\
			if( is_attribute_missing(beg->y()) ) continue;				\
			if( beg->y() > 0 )											\
			{															\
				if( beg->p() > th ) tp+=beg->w();						\
				else fn+=beg->w();										\
			}															\
			else														\
			{															\
				if( beg->p() > th ) fp+=beg->w();						\
				else tn+=beg->w();										\
			}															\
		}																\
		return suffix(tp, tn, fp, fn);									\
	}

/** Defines a template function that calculates some threshold metric
 * from the given confusion matrix.
 *
 * @note creates function named threshold_{suffix} with the following arguments
 *	- tp true positive.
 *	- tn true negative.
 *	- fp false positive.
 *	- fn false negative.
 * 	- returns: threshold metric.
 *
 * @param suffix the functor and function name suffix.
 */
#define THRESHOLD3(suffix) \
	template<class I> double threshold_##suffix(I tp, I tn, I fp, I fn) \
	{ return suffix(tp, tn, fp, fn); }

/** Defines a function that calculates the maximum of some threshold metric
 * over all thresholds.
 *
 * @note creates function named max_{suffix} with the following arguments
 * 	- beg1: an iterator to start of prediction collection.
 * 	- end1: an iterator to end of prediction collection.
 * 	- returns: maximum threshold metric.
 *
 * @param suffix the functor and function name suffix.
 */
#define THRESHOLD4(suffix) \
	template<class I> double max_##suffix(I beg1, I end1)					\
	{																		\
		double maxval=-DBL_MAX, val;										\
		for(I curr=beg1,curr2;curr != end1;++curr)							\
		{																	\
			if( is_attribute_missing(curr->y()) ) continue;					\
			curr2=beg1;														\
			val = threshold_##suffix(curr2, end1, (float)curr->p());		\
			if( val > maxval ) maxval = val;								\
		}																	\
		return maxval;														\
	}

/** Creates a new function for each of the threshold metrics.
 *
 * @param suffix a function macro.
 */
#define THRESHOLD_ALL(suffix)\
	suffix(acc)\
	suffix(sen)\
	suffix(spe)\
	suffix(fpr)\
	suffix(pre)\
	suffix(npv)\
	suffix(lft)\
	suffix(fsc)\
	suffix(net)\
	suffix(nac)\
	suffix(mcc)\
	suffix(ber)\
	suffix(ppv)\
	suffix(rcl)\
	suffix(tpr)\
	suffix(ctp)\
	suffix(cfp)\
	suffix(ctn)\
	suffix(cfn)



namespace exegete
{
	/** Calculates the accuracy for the given confusion matrix.
	 *
	 * \f$\frac{TP+TN}{TP+TN+FP+FN}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the accuracy.
	 */
	inline double acc(double tp, double tn, double fp, double fn)
	{
		return divide(tp+tn, tp+tn+fp+fn);
	}
	/** Calculates the sensitivity for the given confusion matrix.
	 *
	 * \f$\frac{TP}{TP+FN}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the sensitivity.
	 */
	inline double sen(double tp, double tn, double fp, double fn)
	{
		return divide(tp, tp+fn);
	}
	/** Calculates the specificity for the given confusion matrix.
	 * 
	 * \f$\frac{TN}{FP+TN}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the specificity.
	 */
	inline double spe(double tp, double tn, double fp, double fn)
	{
		return divide(tn, fp+tn);
	}
	/** Calculates the false positive rate for the given confusion matrix.
	 *
	 *  \f$\frac{FP}{FP+TN}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the false positive rate.
	 */
	inline double fpr(double tp, double tn, double fp, double fn)
	{
		return divide(fp, fp+tn);
	}
	/** Calculates the precision for the given confusion matrix.
	 *
	 *  \f$\frac{TP}{FP+TP}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the precision.
	 */
	inline double pre(double tp, double tn, double fp, double fn)
	{
		return divide(tp, tp+fp);
	}
	/** Calculates the negative predictive value for the given confusion matrix.
	 *
	 *  \f$\frac{TN}{TN+FN}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the negative predictive value.
	 */
	inline double npv(double tp, double tn, double fp, double fn)
	{
		return divide(tn, tn+fn);
	}
	/** Calculates the lift at 20%? for the given confusion matrix.
	 *
	 *  \f$\frac{SEN*(TP+FP+TN+FN)}{TP+FN}\f$
	 *
	 * @todo check lift description
	 * 
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the lift.
	 */
	inline double lft(double tp, double tn, double fp, double fn)
	{
		return divide(sen(tp, tn, fp, fn) * (tp+fp+fn+tn), tp+fn);
	}
	/** Calculates the F-score for the given confusion matrix.
	 *
	 *  \f$\frac{2.0*PRE*RCL}{PRE+RCL}\f$
	 *
	 * @todo more descriptive, add more general version
	 * 
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the F-score.
	 */
	inline double fsc(double tp, double tn, double fp, double fn)
	{
		double p = pre(tp, tn, fp, fn);
		double r = sen(tp, tn, fp, fn);
		return divide(2.0*p*r, p+r);
	}
	/** Calculates the net precision for the given confusion matrix.
	 *
	 *  \f$\frac{1}{2}(PRE+NPV)\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the net precision.
	 */
	inline double net(double tp, double tn, double fp, double fn)
	{
		return 0.5*pre(tp,tn,fp,fn)+0.5*npv(tp,tn,fp,fn);
	}
	/** Calculates the net accuracy for the given confusion matrix.
	 *
	 *  \f$\frac{1}{2}(SEN+PRE)\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the net accuracy.
	 */
	inline double nac(double tp, double tn, double fp, double fn)
	{
		return 0.5*sen(tp,tn,fp,fn)+0.5*pre(tp,tn,fp,fn);
	}
	/** Calculates the Matthew's correlation coefficient for the given confusion matrix.
	 *
	 *  \f$\frac{TP*TN-FP*FN}{\sqrt{(TN+FN)(TN+FP)(TP+FN)(TP+FP)}}\f$
	 *
	 * @todo use faster method to calculate
	 * 
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the Matthew's correlation coefficient.
	 */
	inline double mcc(double tp, double tn, double fp, double fn)
	{
		double d = std::sqrt((tn+fn)*(tn+fp)*(tp+fn)*(tp+fp));
		double p = divide(tp*tn, d);
		double n = divide(fp*fn, d);
		return (p - n);
	}
	/** Calculates the base error rate for the given confusion matrix.
	 *
	 *  \f$\frac{1}{2}(\frac{FN}{TP+FN}+\frac{FP}{TN+FP})\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the base error rate.
	 */
	inline double ber(double tp, double tn, double fp, double fn)
	{
		return 0.5*divide(fn,tp+fn) + 0.5*divide(fp,tn+fp);
	}
	/** Calculates the positive predictive value for the given confusion matrix.
	 *
	 *  \f$\frac{TP}{FP+TP}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the positive predictive value.
	 */
	inline double ppv(double tp, double tn, double fp, double fn)
	{
		return pre(tp, tn, fp, fn);
	}
	/** Calculates the recall for the given confusion matrix.
	 *
	 * \f$\frac{TP}{TP+FN}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the recall.
	 */
	inline double rcl(double tp, double tn, double fp, double fn)
	{
		return sen(tp, tn, fp, fn);
	}
	/** Calculates the true positive rate for the given confusion matrix.
	 *
	 * \f$\frac{TP}{TP+FN}\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the true positive rate.
	 */
	inline double tpr(double tp, double tn, double fp, double fn)
	{
		return sen(tp, tn, fp, fn);
	}
	/** Calculates the true positives for the given confusion matrix.
	 *
	 * \f$TP\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the true positives.
	 */
	inline double ctp(double tp, double tn, double fp, double fn)
	{
		return tp;
	}
	/** Calculates the false positives for the given confusion matrix.
	 *
	 * \f$FP\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the false positives.
	 */
	inline double cfp(double tp, double tn, double fp, double fn)
	{
		return fp;
	}
	/** Calculates the true negatives for the given confusion matrix.
	 *
	 * \f$TN\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the true negatives.
	 */
	inline double ctn(double tp, double tn, double fp, double fn)
	{
		return tn;
	}
	/** Calculates the false negatives for the given confusion matrix.
	 *
	 * \f$FN\f$
	 *
	 * @param tp true positive count.
	 * @param tn true negative count.
	 * @param fp false positive count.
	 * @param fn false negative count.
	 * @return the false negatives.
	 */
	inline double cfn(double tp, double tn, double fp, double fn)
	{
		return fn;
	}
	
	/** Tabulates a confusion matrix and then calculates a threshold metric 
	 * from this matrix.
	 */
	THRESHOLD_ALL(THRESHOLD1)
	/** Tabulates a confusion matrix and then calculates a threshold metric 
	 * from this matrix and the given threshold.
	 */
	THRESHOLD_ALL(THRESHOLD2)
	/** Converts an inline function to a template function.
	 */
	THRESHOLD_ALL(THRESHOLD3)
	/** Calculates a set of threshold metrics over all thresholds
	 * and returns the maximum.
	 */
	THRESHOLD_ALL(THRESHOLD4)
	
	/** Calculates the average threshold over a set of predictions.
	 * 
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @return average threshold.
	 */
	template<class I> 
	double threshold_average(I beg, I end)
	{
		unsigned int n=0;
		double t=0.0;
		n = (unsigned int)std::distance(beg, end);
		for(;beg != end;++beg) t += beg->t();
		return (n==0)?0.0:t/n;
	}
};

#endif


