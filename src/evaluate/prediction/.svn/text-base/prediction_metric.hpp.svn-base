/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_metric.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_METRIC_HPP
#define _EXEGETE_PREDICTION_METRIC_HPP
#include "prediction_set.hpp"
#include "measure.hpp"
#include <vector>


/** @file prediction_metric.hpp
 * 
 * @brief Contains the prediction metric class
 * 
 * This file contains the prediction metric class template.
 *
 * 
 * @ingroup ExegetePrediction
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{

	/** @brief Estimates metrics from a collection of predictions.
	 * 
	 * This class template serves to estimate metrics from a collection of predictions.
	 */
	template<class P, class Y, class T=typename detail::default_threshold<P>::float_type >
	class prediction_metric : public prediction_set< P, Y, T >
	{
		typedef prediction_set< P, Y, T > prediction_vector;
	public:
		/** Defines a parent value type as a value type. **/
		typedef typename prediction_vector::value_type		value_type;
		/** Defines a parent iterator as an iterator. **/
		typedef typename prediction_vector::iterator		iterator;
		/** Defines a parent constant iterator as a constant iterator. **/
		typedef typename prediction_vector::const_iterator	const_iterator;
		/** Defines a parent size type as a size type. **/
		typedef typename prediction_vector::size_type		size_type;
		/** Defines a parent size type as a size type. **/
		typedef typename prediction_vector::pointer			pointer;
	public:
		/** Defines a value confidence type as a confidence type. **/
		typedef typename value_type::prediction_type		prediction_type;
		/** Defines a value class type as a class type. **/
		typedef typename value_type::class_type				class_type;
		/** Defines a value float type as a float type. **/
		typedef typename value_type::float_type				float_type;
	private:
		typedef measure<const_iterator,double,P> 			measure_type;
	public:
		/** Defines a tune measure. **/
		typedef typename measure_type::tune					tune_measure;
		/** Defines a constant tune iterator. **/
		typedef typename tune_measure::const_iterator 		const_tune_iterator;

	public:
		/** Constructs a prediction metric.
		 * 
		 * @param n number of predictions.
		 * @param b number of bags.
		 * @param m number of runs.
		 * @param c number of classes.
		 */
		prediction_metric(size_type n=0, size_type b=0, size_type m=1, size_type c=2) : 
														prediction_vector(n,b,1,c), val(0.0f), index(0), total(0), ins_level_int(0)
		{
			total=m;
			pcurr = tune_measure::instance().begin()+index;
			val = 0.0f;
		}
		/** Destructs a prediction metric.
		 */
		~prediction_metric()
		{
		}
		
	public:
		/** Appends an argument to a map.
		 * 
		 * @param map some argument collection.
		 * @param use should append argument.
		 * @param pfx prefix for argument name.
		 */
		template<class U>
		void init(U& map, bool use, std::string pfx="")
		{
			if( use )
			{
				arginit(map, index, 		pfx, "metric", tune_measure::instance().options("a metric to compare>"));
				arginit(map, ins_level_int, pfx, "use_instance", "use instance-level evaluation?");
			}
		}
		
	public:
		/** Resize the number of predictions and classes.
		 * 
		 * @param n number of predictions.
		 * @param c number of classes.
		 */
		void resize(size_type n, size_type c)
		{
			prediction_vector::resize(n,c);
			total=1;
			pcurr = tune_measure::instance().begin()+index;
			val = 0.0f;
		}
		/** Resize the number of predictions (instances and bags), runs and classes.
		 * 
		 * @param n number of predictions.
		 * @param b number of bags.
		 * @param m number of runs.
		 * @param c number of classes.
		 */
		void resize(size_type n, size_type b, size_type m, size_type c)
		{
			prediction_vector::resize(n,b,c);
			total=(m==0?1:m);
			pcurr = tune_measure::instance().begin()+index;
			val = 0.0f;
		}
		/** Resize the number of predictions (instances and bags) and classes.
		 * 
		 * @param n number of predictions.
		 * @param b number of bags.
		 * @param c number of classes.
		 */
		void resize(size_type n, size_type b, size_type c)
		{
			prediction_vector::resize(n,b,c);
			total=1;
			pcurr = tune_measure::instance().begin()+index;
			val = 0.0f;
		}
		
	public:
		/** Initalize the prediction metric.
		 */
		void initialize()
		{
			pcurr = tune_measure::instance().begin()+index;
		}
		/** Setup next run.
		 */
		void next_run()
		{
			prediction_vector::next_run();
			if( ins_level_int > 0 && prediction_vector::instance_size() > 0 )
			{
				val+=(float_type)pcurr->calculate(prediction_vector::instance_begin(), prediction_vector::instance_end(), prediction_vector::class_count());
			}
			else
			{
				val+=(float_type)pcurr->calculate(prediction_vector::begin(), prediction_vector::end(), prediction_vector::class_count());
			}
		}
		/** Finalize the prediction metric.
		 */
		void finalize()
		{
			prediction_vector::finalize();
		}
		
	public:
		/** Get average metric.
		 * 
		 * @return average metric.
		 */
		float_type average()const
		{
			return val / float_type(total);
		}
		/** Get name of metric.
		 * 
		 * @return metric name.
		 */
		const std::string& name()const
		{
			return pcurr->name();
		}
		/** Set the type of metric by index.
		 * 
		 * @param i index of metric.
		 */
		void type(size_type i)
		{
			index = i;
			pcurr = tune_measure::instance().begin()+index;
		}
		/** Write average prediction metric to output stream.
		 * 
		 * @param out an output stream.
		 * @param set a prediction metric.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const prediction_metric& set)
		{
			out << set.average();
			return out;
		}
		
	private:
		float_type val;
		size_type index;
		size_type total;
		const_tune_iterator pcurr;
	private:
		int ins_level_int;
	};

};


#endif


