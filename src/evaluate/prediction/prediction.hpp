/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_HPP
#define _EXEGETE_PREDICTION_HPP
#include "typeutil.h"
#include "errorutil.h"
#include "stringutil.h"
#include "ClassUtil.h"

/** @file prediction.hpp
 * 
 * @brief Contains classes defining a single prediction
 * 
 * This file contains classes related to a single prediction.
 *
 * @ingroup ExegetePrediction
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	namespace detail
	{
		/** @brief Base implementation of a prediction.
		 * 
		 * This class defines a base implementation of a prediction. It holds
		 * the following information:
		 * 	- a confidence in prediction
		 * 	- a class label
		 * 	- a weight
		 */
		template<class P, class Y, class F=P>
		class base_prediction_impl
		{
		public:
			/** Flags whether class has a label. **/
			enum{ haslabel=!TypeUtil<Y>::ispod};
			
		public:
			/** Defines first template parameter as a prediction type. **/
			typedef P prediction_type;
			/** Defines second template parameter as a class type. **/
			typedef Y class_type;
			/** Defines third template parameter as a float type. **/
			typedef F float_type;
			/** Defines actual class value as a class int. **/
			typedef typename ClassUtil<Y>::class_type class_int;
			
		public:
			/** Constructs a base prediction implementation with given confidence, class and weight.
			 * 
			 * @param _p a prediction confidence.
			 * @param _y a class value.
			 * @param _w a weight value.
			 */
			base_prediction_impl(prediction_type _p, const class_type& _y, float_type _w=1.0f) : p_val(_p), y_val(_y), w_val(_w)
			{
			}
			
		public:
			/** Sets a class value.
			 *
			 * @param v a class index value.
			 */
			void y(const class_type& v)
			{
				y_val = v;
			}
			/** Gets a class value.
			 *
			 * @return a class value.
			 */
			const class_type& y()const
			{
				return y_val;
			}
			/** Adds a class value.
			 *
			 * @param v a class value.
			 */
			void add_y(const class_type& v)
			{
				y_val += v;
			}
			/** Sets the prediction value.
			 *
			 * @param v a confidence-rated prediction.
			 */
			void p(prediction_type v)
			{
				p_val = v;
			}
			/** Gets the prediction value.
			 *
			 * @return a confidence-rated prediction.
			 */
			prediction_type p()const
			{
				return p_val;
			}
			/** Sets the weight value.
			 *
			 * @param w a weight.
			 */
			void w(float_type w)
			{
				w_val = w;
			}
			/** Gets the weight value.
			 *
			 * @return a weight.
			 */
			float_type w()const
			{
				return w_val;
			}
			/** Sets the prediction, class and weight.
			 * 
			 * @param _p a prediction confidence.
			 * @param _y a class value.
			 * @param _w a weight value.
			 */
			void set(prediction_type _p, class_type _y, float_type _w=1.0f)
			{
				p_val = _p;
				y_val = _y;
				w_val = _w;
			}
			
		protected:
			/** Holds a prediction confidence. **/
			prediction_type p_val;
			/** Holds a class value. **/
			class_type y_val;
			/** Holds a weight value. **/
			float_type w_val;
		};
		/** @brief Base binary prediction
		 * 
		 * This class defines an interface to a base binary prediction. 
		 * 
		 * @see base_prediction_impl
		 */
		template<class P, class Y>
		class base_prediction : public base_prediction_impl<P,Y>
		{
			typedef base_prediction_impl<P,Y> parent_type;
		public:
			/** Defines a prediction type. **/
			typedef typename parent_type::prediction_type prediction_type;
			/** Defines a class type. **/
			typedef typename parent_type::class_type class_type;
			/** Defines a float type. **/
			typedef typename parent_type::float_type float_type;
			/** Defines a class integer. **/
			typedef typename parent_type::class_int class_int;
			/** Defines a read type. **/
			typedef prediction_type read_type[2];
			/** Flags as binary. **/
			enum{ is_binary=true };
			
		public:
			/** Constructs a base prediction.
			 * 
			 * @param _p a prediction confidence.
			 * @param _y a class value.
			 * @param _w a weight value.
			 */
			base_prediction(prediction_type _p, class_type _y, float_type _w=1.0f) : parent_type(_p,_y,_w)
			{
			}
			
		public:
			/** Gets a prediction.
			 * 
			 * @param n dummy.
			 * @return a prediction.
			 */
			float_type p_at(unsigned int n)const
			{
				return parent_type::p_val;
			}
			/** Adds a prediction value.
			 * 
			 * @param v a prediction value.
			 */
			void add_p(float_type v)
			{
				parent_type::p_val+=v;
			}
			/** Sets a prediction value in the read array.
			 * 
			 * @param val a read array.
			 */
			void read(read_type val)const
			{
				val[0] = parent_type::p_val;
			}
			/** Copies a prediction.
			 * 
			 * @param b iterator to start of confidences.
			 * @param e iterator to end of confidences.
			 * @param cl a class value.
			 * @param wg a weight value.
			 */
			void set_copy(float_type* b, float_type* e, class_type cl, float_type wg=1.0f)
			{
				parent_type::w(wg);
				parent_type::y(cl);
				if(b != e) parent_type::p(*b);
			}
			/** Sets the prediction value.
			 * 
			 * @param _p a confidence value.
			 * @param _y a class value.
			 * @param _w a weight value.
			 */
			void set(prediction_type _p, class_type _y, float_type _w=1.0f)
			{
				parent_type::set(_p, _y, _w);
			}
			/** Sets the threshold in a read array.
			 * 
			 * @param val a read array.
			 * @param t a threshold.
			 */
			void set_threshold(read_type val, float_type t)const
			{
				val[1] = t;
			}
			/** Get index of best prediction.
			 * 
			 * @param n number of classes.
			 * @return 0, a dummy value.
			 */
			inline class_int best_index(unsigned int n)const
			{
				return 0;
			}
			/** Copy a collection of predictions.
			 * 
			 * @note does nothing
			 * 
			 * @param beg start iterator to prediction collection.
			 * @param end end iterator to prediction collection.
			 * @param it iterator to destination collection.
			 * @param n number of classes.
			 */
			template<class I1, class I2>
			static void assign(I1 beg, I1 end, I2 it, unsigned int n)
			{
			}
		};
		/** @brief Base multi-class prediction
		 * 
		 * This class defines an interface to a base multi-class prediction. 
		 * 
		 * @see base_prediction_impl
		 */
		template<class P, class Y>
		class base_prediction<P*,Y> : public base_prediction_impl<P*,Y,P>
		{
			typedef base_prediction_impl<P*,Y, P> parent_type;
		public:
			/** Defines a prediction iterator. **/
			typedef P* iterator;
			/** Defines a constant prediction iterator. **/
			typedef const P* const_iterator;
			/** Defines a prediction value type. **/
			typedef P value_type;
			/** Defines a parent prediction type. **/
			typedef typename parent_type::prediction_type prediction_type;
			/** Defines a parent class type. **/
			typedef typename parent_type::class_type class_type;
			/** Defines a parent float type. **/
			typedef typename parent_type::float_type float_type;
			/** Defines a parent class value. **/
			typedef typename parent_type::class_int class_int;
			typedef prediction_type read_type;
			/** Flags prediction as not binary. **/
			enum{ is_binary=false };
			
		public:
			/** Constructs a base prediction.
			 * 
			 * @param _p a prediction confidence.
			 * @param _y a class value.
			 * @param _w a weight value.
			 */
			base_prediction(prediction_type _p, class_type _y, float_type _w=1.0f) : parent_type(_p,_y,_w)
			{
			}
			/** Constructs a copy of a prediction.
			 * 
			 * @param bp a base prediction.
			 */
			base_prediction(const base_prediction& bp) : parent_type(0, bp.y(), bp.w())
			{
			}
			/** Assigns a copy of a base prediction.
			 * 
			 * @param bp a base prediction.
			 * @return reference to this object.
			 */
			base_prediction& operator=(const base_prediction& bp)
			{
				y(bp.y());
				w(bp.w());
				return *this;
			}
			/** Destructs a base prediction.
			 */
			~base_prediction()
			{
				::erase(parent_type::p_val);
			}
			
		public:
			/** Copy a collection of predictions.
			 * 
			 * @param beg start iterator to prediction collection.
			 * @param end end iterator to prediction collection.
			 * @param it iterator to destination collection.
			 * @param n number of classes.
			 */
			template<class I1, class I2>
			static void assign(I1 beg, I1 end, I2 it, unsigned int n)
			{
				for(;beg != end;++beg,++it)
					it->copy(beg->p(), n);
			}
			
		public:
			/** Copies confidence values in a multi-class prediction.
			 * 
			 * @param p a prediction pointer.
			 * @param n number of classes.
			 */
			void copy(prediction_type p, unsigned int n)
			{
				parent_type::p_val = resize(parent_type::p_val, n);
				std::copy(p, p+n, parent_type::p_val);
			}
			/** Gets a prediction at a specific index.
			 * 
			 * @param n an index.
			 * @return a confidence value.
			 */
			float_type p_at(unsigned int n)const
			{
				return parent_type::p_val[n];
			}
			/** Adds a prediction confidence value.
			 * 
			 * @param v a prediction confidence value.
			 * @param cl number of classes.
			 */
			void add_p(float_type v, unsigned int cl)
			{
				parent_type::p_val[cl] += v;
			}
			/** Sets a prediction in the read array.
			 * 
			 * @param val a read array.
			 */
			void read(read_type& val)const
			{
				val = parent_type::p_val;
			}
			/** Copies a prediction.
			 * 
			 * @param b iterator to start of confidences.
			 * @param e iterator to end of confidences.
			 * @param cl a class value.
			 * @param wg a weight value.
			 */
			void set_copy(float_type* b, float_type* e, class_type cl, float_type wg=1.0f)
			{
				parent_type::w(wg);
				parent_type::y(cl);
				parent_type::p_val = resize(parent_type::p_val, std::distance(b,e));
				std::copy(b, e, parent_type::p_val);
			}
			/** Gets the index of the best prediction.
			 * 
			 * @param n number of classes.
			 * @return best prediction class.
			 */
			inline class_int best_index(unsigned int n)const
			{
				unsigned int i,j=0;
				float_type best=0.0f;
				for(i=0;i<n;++i)
				{
					if( parent_type::p_val[i] > best )
					{
						best = parent_type::p_val[i];
						j=i;
					}
				}
				return j;
			}
			/** Sets the threshold in a read array.
			 * 
			 * @note does nothing
			 * 
			 * @param val a read array.
			 * @param t a threshold.
			 */
			void set_threshold(read_type val, float_type t)const
			{
			}
			/** Gets a start iterator to a multi-class prediction collection.
			 * 
			 * @return start iterator.
			 */
			iterator begin()
			{
				return parent_type::p_val;
			}
			/** Gets an end iterator to a multi-class prediction collection.
			 * 
			 * @return end iterator.
			 */
			iterator end()
			{
				return parent_type::p_val;
			}
			/** Gets a start iterator to a multi-class prediction collection.
			 * 
			 * @return start iterator.
			 */
			const_iterator begin()const
			{
				return parent_type::p_val;
			}
			/** Gets an end iterator to a multi-class prediction collection.
			 * 
			 * @return end iterator.
			 */
			const_iterator end()const
			{
				return parent_type::p_val;
			}
		};
	};
	namespace detail
	{
		/** @brief Determines a threshold type
		 * 
		 * This class template selects a threshold value based on the
		 * prediction confidence type. This specialization creates a 
		 * threshold type as a confidence type.
		 */
		template<class F>
		struct default_threshold
		{
			typedef F float_type;
		};
		/** @brief Determines a threshold type
		 * 
		 * This class template selects a threshold value based on the
		 * prediction confidence type. This specialization creates a 
		 * void threshold type.
		 */
		template<class F>
		struct default_threshold<F*>
		{
			typedef void float_type;
		};
	};
	/** @brief Prediction with a threshold
	 * 
	 * This class defines a prediction with a threshold.
	 */
	template<class P, class Y, class T=typename detail::default_threshold<P>::float_type>
	class prediction : public detail::base_prediction<P,Y>
	{
		typedef detail::base_prediction<P,Y> parent_type;
	public:
		/** Defines a prediction type. **/
		typedef typename parent_type::prediction_type prediction_type;
		/** Defines a class type. **/
		typedef typename parent_type::class_type class_type;
		/** Defines a float type. **/
		typedef T float_type;
		/** Defines a read type. **/
		typedef typename parent_type::read_type read_type;
	public:
		/** Constructs a prediction.
		 * 
		 * @param _p a prediction confidence.
		 * @param _c a class value.
		 * @param _t a threshold value.
		 * @param _w a weight value.
		 */
		prediction(prediction_type _p=0, class_type _c=0, float_type _t=0, float_type _w=1.0f) : parent_type(_p, _c, _w), t_val(_t)
		{
		}
		/** Constructs a copy of a prediction.
		 * 
		 * @param p a prediction without a threshold to copy.
		 */
		prediction(const prediction<P,Y,void>& p) : parent_type(p.p(), p.y(), p.w()), t_val(0.0f)
		{
		}
		
	public:
		/** Sets a prediction, class, threshold and weight values for a prediction.
		 * 
		 * @param _p a confidence value.
		 * @param _y a class value.
		 * @param _t a threshold value.
		 * @param _w a weight value.
		 */
		void set(prediction_type _p, class_type _y, float_type _t, float_type _w=1.0f)
		{
			parent_type::set(_p, _y, _w);
			t_val = _t;
		}
		/** Sets a threshold.
		 * 
		 * @param v a threshold.
		 */
		void t(float_type v)
		{
			t_val = v;
		}
		/** Gets a threshold.
		 * 
		 * @return a threshold.
		 */
		float_type t()const
		{
			return t_val;
		}
		/** Saves the prediction information into a read array.
		 * 
		 * @param val a read array.
		 */
		void read(read_type& val)const
		{
			parent_type::read(val);
			parent_type::set_threshold(val, t_val);
		}
		/** Copies a collection of confidence values, a class value and a weight value.
		 * 
		 * @param b start of confidence collection.
		 * @param e end of confidence collection.
		 * @param cl a class value.
		 * @param _w a weight value.
		 */
		void set_copy(float_type* b, float_type* e, class_type cl, float_type _w=1.0f)
		{
			parent_type::set_copy(b, e, cl, _w);
			if( b != e )
			{
				++b;
				if( b != e ) t_val = *b;
			}
		}
		
	private:
		float_type t_val;
	};
	/** @brief Prediction without a threshold
	 * 
	 * This class defines a prediction without a threshold.
	 */
	template<class P, class Y>
	class prediction<P, Y, void> : public detail::base_prediction<P,Y>
	{
		typedef detail::base_prediction<P,Y> parent_type;
	public:
		/** Defines a prediction type. **/
		typedef typename parent_type::prediction_type prediction_type;
		/** Defines a class type. **/
		typedef typename parent_type::class_type class_type;
		/** Defines a float type. **/
		typedef typename parent_type::float_type float_type;
		/** Defines a read type. **/
		typedef typename parent_type::read_type read_type;
	public:
		/** Constructs a prediction.
		 * 
		 * @param _p a prediction confidence.
		 * @param _c a class value.
		 * @param _w a weight value.
		 */
		prediction(prediction_type _p=0, class_type _c=0, float_type _w=1.0f) : parent_type(_p, _c, _w)
		{
		}
		/** Constructs a copy of a prediction.
		 * 
		 * @param p a prediction with/without a threshold to copy.
		 */
		template<class T>
		prediction(const prediction<P,Y,T>& p) : parent_type(p.p(), p.y(), p.w())
		{
		}
		
	public:
		/** Saves the prediction information into a read array.
		 * 
		 * @param val a read array.
		 */
		void read(read_type& val)const
		{
			parent_type::read(val);
			parent_type::set_threshold(val, 0.0f);
		}
		/** Sets a prediction, class and weight values for a prediction.
		 * 
		 * @param _p a confidence value.
		 * @param _y a class value.
		 * @param _w a weight value.
		 */
		void set(prediction_type _p, class_type _y, float_type _w=1.0f)
		{
			parent_type::set(_p, _y, _w);
		}
		/** Sets a prediction, class, threshold and weight values for a prediction.
		 * 
		 * @param _p a confidence value.
		 * @param _y a class value.
		 * @param _t a dummy value.
		 * @param _w a weight value.
		 */
		void set(prediction_type _p, class_type _y, float_type _t, float_type _w=1.0f)
		{
			parent_type::set(_p, _y, _w);
		}
	};

};

/** @brief Type utility interface for a prediction
 * 
 *  This class template provides basic type operations for a prediction.
 */
template<class P, class Y, class T>
struct TypeUtil< ::exegete::prediction<P,Y,T> >
{
	/** Flags prediction as not primative.
	 */
	enum{ ispod=false };
	/** Defines a prediction as a value type.
	 */
	typedef ::exegete::prediction<P,Y,T> value_type;
	/** Tests if a string can be converted to a specific type.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return "prediction"
	*/
	static const char* name() 
	{
		return "prediction";
	}
};
#endif


