/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_record.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_RECORD_HPP
#define _EXEGETE_PREDICTION_RECORD_HPP
#include "prediction_set.hpp"
#include <vector>
#include <ctime>


/** @file prediction_record.hpp
 * 
 * @brief Contains a prediction record
 * 
 * This file contains the prediction record class template.
 *
 * @ingroup ExegetePrediction
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{

	/** @brief Holds a prediction record: description and prediction sets.
	 * 
	 * This class template serves as an interface to a collection of predictions and a description
	 * of the record.
	 */
	template<class P, class Y, class T=typename detail::default_threshold<P>::float_type>
	class prediction_record : public std::vector< prediction_set< P, Y, T > >
	{
		template<class P1, class Y1, class T1> friend class prediction_record;
		typedef std::vector< prediction_set< P, Y, T > >	prediction_vector;
		typedef std::vector< std::string >					string_vector;
	public:
		/** Defines a parent value type as a value type. **/
		typedef typename prediction_vector::const_reference	const_reference;
		/** Defines a parent value type as a value type. **/
		typedef typename prediction_vector::value_type		value_type;
		/** Defines a parent iterator as an iterator. **/
		typedef typename prediction_vector::iterator		iterator;
		/** Defines a parent constant iterator as a constant iterator. **/
		typedef typename prediction_vector::const_iterator	const_iterator;
		/** Defines a parent size type as a size type. **/
		typedef typename prediction_vector::size_type		size_type;
		/** Defines a parent size type as a size type. **/
		typedef typename prediction_vector::pointer			pointer;
	public:
		/** Defines a value confidence type as a confidence type. **/
		typedef typename value_type::prediction_type		prediction_type;
		/** Defines a value class type as a class type. **/
		typedef typename value_type::class_type				class_type;
		/** Defines a value float type as a float type. **/
		typedef typename value_type::float_type				float_type;
	public:
		/** Defines a read type. **/
		typedef typename value_type::read_type 				read_type;
		/** Defines a prediction type. **/
		typedef typename value_type::value_type				pred_type;
		/** Flags a record as binary. **/
		enum{ is_binary=pred_type::is_binary };

	public:
		/** Constructs a prediction record.
		 */
		prediction_record() : errmsg(0), index(0), start_time(0), stop_time(0)
		{
		}
		
	public:
		/** Copy the binary prediction sets into the binary record.
		 * 
		 * @param bin saved binary records.
		 */
		void copy(prediction_record<float_type,Y,float_type>& bin)
		{
			bin.resize(prediction_vector::size());
			typename prediction_record<float_type,Y,float_type>::iterator it = bin.begin();
			for(const_iterator beg = prediction_vector::begin(),end=prediction_vector::end();beg != end;++beg,++it)
			{
				it->copy(*beg);
			}
			bin.header(algorithm(), validation(), dataset(), comment());
			bin.class_names=class_names;
		}
		
	public:
		/** Resize the number of predictions.
		 * 
		 * @param n number of predictions.
		 */
		void resize(size_type n)
		{
			prediction_vector::resize(n);
			index=n;
		}
		/** Resize the number of predictions (instances and bags), runs and classes.
		 * 
		 * @param n number of predictions.
		 * @param b number of bags.
		 * @param m number of runs.
		 * @param c number of classes.
		 */
		void resize(size_type n, size_type b, size_type m, size_type c=2)
		{
			prediction_vector::assign(m, value_type(n,b,1,c));
			index=0;
		}
		/** Clear a prediction record.
		 */
		void clear()
		{
			prediction_vector::clear();
			index=0;
		}
		/** Get maximum number of predictions in each run.
		 * 
		 * @return maximum number of predictions in each run.
		 */
		size_type max_size()const
		{
			size_type m = 0, sum;
			for(const_iterator beg = prediction_vector::begin(),end=prediction_vector::end();beg != end;++beg)
			{
				sum = beg->size()+beg->instance_size();
				if( sum > m ) m = sum;
			}
			return m;
		}
		/**
		 * 
		 */
		void only_positive_bags(size_type b, size_type e)
		{
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				beg->only_positive_bags(b, e);
		}
		/** Set the prediction weights for type:
		 *	#. Use the class cost vector. (type 0)
		 * 	#. Weight each class by relative size (type 1)
		 * 	#. Set all weights to 1.0 (type 2)
		 * 
		 * @param costs a cost vector.
		 * @param type a type weight.
		 * @return an error message or NULL.
		 */
		const char* set_cost(const std::vector<float>& costs, int type)
		{
			const char* msg;
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				if( (msg=beg->set_cost(costs, type)) != 0 ) return msg;
			return 0;
		}
		/** Set the class labels using mapped unique values.
		 * 
		 * @param map a label map.
		 * @param type type of label.
		 * @return an error message or NULL.
		 */
		template<class M>
		const char* set_labels(const M& map, int type)
		{
			const char* msg;
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				if( (msg=beg->set_labels(map, type)) != 0 ) return msg;
			return 0;
		}
		/** Set binary threshold.
		 * 
		 * @param thresh
		 */
		void threshold(float thresh)
		{
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				beg->threshold(thresh);
		}
		/** Get the number of runs.
		 * 
		 * @return number of runs.
		 */
		size_type run_count()const
		{
			return prediction_vector::size();
		}
		
	public:
		/** Initalize the prediction record.
		 */
		void initialize()
		{
			ASSERT(index<prediction_vector::size());
			prediction_vector::operator[](index).initialize();
		}
		/** Add a prediction (confidence, class, threshold, weight) to the current prediction set.
		 * 
		 * @param p a prediction confidence.
		 * @param y a prediction class.
		 * @param t a prediction threshold.
		 * @param w a prediction weight.
		 */
		void next(prediction_type p, const class_type& y, float_type t, float_type w=1.0f)
		{
			ASSERT(index<prediction_vector::size());
			prediction_vector::operator[](index).next(p, y, t, w);
		}
		/** Add a bag prediction (confidence, class, threshold, weight) to the current prediction set.
		 * 
		 * @param p a prediction confidence.
		 * @param y a prediction class.
		 * @param t a prediction threshold.
		 * @param w a prediction weight.
		 */
		void next_bag(prediction_type p, const class_type& y, float_type t, float_type w=1.0f)
		{
			ASSERT(index<prediction_vector::size());
			prediction_vector::operator[](index).next_bag(p, y, t, w);
		}
		/** Move to next run.
		 */
		void next_run()
		{
			ASSERT(index<prediction_vector::size());
			prediction_vector::operator[](index).next_run();
			index++;
		}
		/** Finalize the prediction record.
		 */
		void finalize()
		{
		}
		/** Start the timer.
		 */
		void start_timer()
		{
			start_time = std::time(0);
		}
		/** Stop the timer.
		 */
		void stop_timer()
		{
			stop_time = std::time(0);
		}
		
	public:
		/** Set the algorithm, validation, dataset and comment.
		 * 
		 * @param a algorithm name.
		 * @param v validation name.
		 * @param d dataset name.
		 * @param c comment name.
		 * @return reference to this object.
		 */
		prediction_record& operator()(const std::string& a, const std::string& v, const std::string& d, const std::string& c)
		{
			header(a, v, d, c);
			return *this;
		}
		/** Set the algorithm, validation, dataset and comment.
		 * 
		 * @param a algorithm name.
		 * @param v validation name.
		 * @param d dataset name.
		 * @param c comment name.
		 */
		void header(const std::string& a, const std::string& v, const std::string& d, const std::string& c)
		{
			algorithm_str = a;
			validation_str = v;
			dataset_str = d;
			comment_str = c;
		}
		/** Sets the class name collection.
		 * 
		 * @param beg start of class name collection.
		 * @param end end of class name collection.
		 */
		template<class I>
		void classes(I beg, I end)
		{
			class_names.assign(beg, end);
		}
		/** Set the algorithm name.
		 * 
		 * @param a algorithm name.
		 */
		void algorithm(const std::string& a)
		{
			algorithm_str = a;
		}
		/** Set the comment string.
		 * 
		 * @param a comment string.
		 */
		void comment(const std::string& a)
		{
			comment_str = a;
		}
		/** Set the validation name.
		 * 
		 * @param a validation name.
		 */
		void validation(const std::string& a)
		{
			validation_str = a;
		}
		/** Set the dataset name.
		 * 
		 * @param a dataset name.
		 */
		void dataset(const std::string& a)
		{
			dataset_str = a;
		}
		
	public:
		/** Get the algorithm name.
		 * 
		 * @return a algorithm name.
		 */
		const std::string& algorithm()const
		{
			return algorithm_str;
		}
		/** Get the comment.
		 * 
		 * @return a comment.
		 */
		const std::string& comment()const
		{
			return comment_str;
		}
		/** Get the validation name.
		 * 
		 * @return a validation name.
		 */
		const std::string& validation()const
		{
			return validation_str;
		}
		/** Get the dataset name.
		 * 
		 * @return a dataset name.
		 */
		const std::string& dataset()const
		{
			return dataset_str;
		}
		/** Get an error message.
		 * 
		 * @return an error message.
		 */
		const char* errormsg()const
		{
			return errmsg;
		}
		/** Test if any prediction set is empty.
		 * 
		 * @return true if any prediction set is empty.
		 */
		bool test_empty()const
		{
			for(const_iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				if( beg->test_empty() ) return true;
			return false;
		}
		/** Get the number of classes in the first prediction set.
		 * 
		 * @return number of classes.
		 */
		size_type class_count()const
		{
			if(!prediction_vector::empty()) return prediction_vector::front().class_count();
			return 0;
		}
		/** Get the number of instances in the first prediction set.
		 * 
		 * @return number of instances.
		 */
		size_type instance_count()const
		{
			if(!prediction_vector::empty()) return prediction_vector::front().instance_count();
			return 0;
		}
		/** Get the number of bags in the first prediction set.
		 * 
		 * @return number of bags.
		 */
		size_type bag_count()const
		{
			if(!prediction_vector::empty()) return prediction_vector::front().bag_count();
			return 0;
		}
		
	private:
		/** Write a prediction record: description and collection of prediction sets.
		 * 
		 * @param out an output stream.
		 * @param record a prediction record.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const prediction_record& record)
		{
			if( record.empty() ) return out;
			out << "#ET\t" << record.algorithm_str << " | ";
			out << record.validation_str << " | ";
			out << record.dataset_str << " | ";
			out << (record.stop_time-record.start_time);
			out << "\n";
			out << "#ES\t";
			out << record.front().class_count() << " | ";
			out << record.size() << " | ";
			out << record.max_size() << "\n";
			
			if( !record.class_names.empty() )
			{
				out << "#CL\t" << record.class_names[0];
				for(unsigned int i=1;i<record.class_names.size();++i)
					out << "," << record.class_names[i];
				out << "\n";
			}
			out << "#HH\n";
			std::string cm = "#SP";
			for(const_iterator beg = record.begin(),end=record.end();beg != end;++beg)
			{
				write_predictions(out, *beg, cm);
				if( (beg+1) == end ) out << "#XX\n";
				else out << cm << "\n";
			}
			return out;
		}
		static void write_predictions(std::ostream& out, const_reference record, const std::string& cm)
		{
			typedef typename value_type::const_iterator const_pred_iterator;
			size_type cl = record.class_count(), i;
			read_type arr;
			for(const_pred_iterator beg = record.begin(),end=record.end();beg != end;++beg)
			{
				beg->read(arr);
				out << cm << "\t" << beg->y();
				ASSERT( arr != 0 );
				for(i=0;i<cl;++i) out << " " << arr[i];
				if( beg->w() != 1.0f ) out << " " << beg->w();
				out << "\n";
			}
			if( record.instance_size() > 0 )
			{
				if( record.begin() != record.end() ) out << cm << "\t//\n";
				for(const_pred_iterator beg = record.instance_begin(),end=record.instance_end();beg != end;++beg)
				{
					beg->read(arr);
					out << cm << "\t" << beg->y();
					for(i=0;i<cl;++i) out << " " << arr[i];
					if( beg->w() != 1.0f ) out << " " << beg->w();
					out << "\n";
				}
			}
		}
		
	private:
		/** Read a prediction record: description and collection of prediction sets.
		 * 
		 * @param in an input stream.
		 * @param record a prediction record.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, prediction_record& record)
		{
			std::string line;
			std::vector<std::string> arr;
			size_type num, index=0;
			typename std::string::size_type n;
			std::vector<float_type> vals;
			class_type cl;
			float_type wgt;
			bool done=false;
			while( !in.eof() )
			{
				std::getline(in, line);
				if( line.empty() ) continue;
				if( strcmpn(line.c_str(), "#ES",  3) )
				{
					size_type m, cl;
					if( (n=line.find_first_of('\t')) != std::string::npos ) line = line.substr(n+1);
					stringToValue(line, arr, "|");
					if( arr.size() < 1 ) return record.error(in, ERRORMSG("Header missing class count"));
					stringToValue(arr[0], cl);
					if( arr.size() < 2 ) return record.error(in, ERRORMSG("Header missing record count"));
					stringToValue(arr[1], m);
					if( arr.size() < 3 ) return record.error(in, ERRORMSG("Header missing prediction count"));
					stringToValue(arr[2], num);
					vals.resize(cl);
					arr.clear();
					record.resize(num, 0, m, cl);
				}
				else if( strcmpn(line.c_str(), "#CL",  3) )
				{
					if( (n=line.find_first_of('\t')) != std::string::npos ) line = line.substr(n+1);
					stringToValue(line, record.class_names, ",");
				}
				else if( strcmpn(line.c_str(), "#ET",  3) )
				{
					if( (n=line.find_first_of('\t')) != std::string::npos ) line = line.substr(n+1);
					stringToValue(line, arr, "|");
					if( arr.size() < 1 ) return record.error(in, ERRORMSG("Header missing algorithm name"));
					record.algorithm_str = arr[0];
					if( arr.size() < 2 ) return record.error(in, ERRORMSG("Header missing validation name"));
					record.validation_str = arr[1];
					if( arr.size() < 3 ) return record.error(in, ERRORMSG("Header missing dataset name"));
					record.dataset_str = arr[2];
					arr.clear();
				}
				else if( strcmpn(line.c_str(), "#HH",  3) ) break;
				else if( strcmpn(line.c_str(), "#XX",  3) ) break;
			}
			while( !in.eof() )
			{
				if( index >= record.size() ) return record.error(in, ERRORMSG("Run index exceeds header: " << index << " >= " << record.size() << " -> \"" << line << "\"" ));
				record[index].resize(num, vals.size());
				while( !in.eof() )
				{
					in >> line;
					if( in.peek() == '\t') in.get();
					if( in.peek() == '\n')
					{
						in.get();
						if( strcmpn(line.c_str(), "#XX",  3) ) done=true;
						break;
					}
					if( in.peek() == '/')
					{
						in.get();
						if( in.get() != '/' ) return record.error(in, ERRORMSG("Unable to parse bag separator"));
						record[index].finalize_bag();
						continue;
					}
					in >> cl;
					if( in.fail() ) return record.error(in, ERRORMSG("Unable to parse label and class"));
					for(unsigned int i=0;i<vals.size();++i)
					{
						if( in.peek() != ' ' ) return record.error(in, ERRORMSG("Unable to parse prediction values, missing space at column " << i << " and row " << index << " \"" << in.get() << "\"" ));
						else in.get();
						in >> vals[i];
					}
					if( in.peek() == ' ') 
					{
						in.get();
						in >> wgt;
					}
					else wgt = 1.0f;
					if( in.get() != '\n' ) return record.error(in, ERRORMSG("Unable to parse prediction line"));
					record[index].next(&(*vals.begin()), (&(*vals.begin()))+vals.size(), cl, wgt);
				}
				record[index].next_run();
				ASSERT( record[index].begin() != record[index].end() );
				index++;
				if( done ) break;
			}
			record.index=index;
			return in;
		}
		std::istream& error(std::istream& in, const char* msg)
		{
			errmsg = msg;
			in.setstate( std::ios::failbit );
			return in;
		}
		
	private:
		const char* errmsg;
		size_type index;
		string_vector class_names;
		std::string comment_str;
		std::string algorithm_str;
		std::string validation_str;
		std::string dataset_str;
		time_t start_time;
		time_t stop_time;
	};
};

#endif


