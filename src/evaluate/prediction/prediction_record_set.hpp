/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_record_set.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_RECORD_SET_HPP
#define _EXEGETE_PREDICTION_RECORD_SET_HPP
#include "prediction_record.hpp"
#include <vector>


/** @file prediction_record_set.hpp
 * 
 * @brief Contains the prediction record class
 * 
 * This file contains the prediction record class template.
 *
 * @ingroup ExegetePrediction
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{
	/** @brief Holds a set of prediction records
	 * 
	 * This class template serves as an interface to a collection of prediction records.
	 */
	template<class P, class Y, class T=typename detail::default_threshold<P>::float_type>
	class prediction_record_set : public std::vector< prediction_record< P, Y, T > >
	{
		template<class P1, class Y1, class T1> friend class prediction_record_set;
		typedef std::vector< prediction_record< P, Y, T > >	prediction_vector;
	public:
		/** Defines a parent value type as a value type. **/
		typedef typename prediction_vector::value_type		value_type;
		/** Defines a parent iterator as an iterator. **/
		typedef typename prediction_vector::iterator		iterator;
		/** Defines a parent constant iterator as a constant iterator. **/
		typedef typename prediction_vector::const_iterator	const_iterator;
		/** Defines a parent size type as a size type. **/
		typedef typename prediction_vector::size_type		size_type;
		/** Defines a parent size type as a size type. **/
		typedef typename prediction_vector::pointer			pointer;
		/** Defines a parent size type as a constant reference. **/
		typedef typename prediction_vector::const_reference	const_reference;
		/** Defines a parent size type as a constant reference. **/
		typedef typename prediction_vector::reference		reference;
	public:
		/** Defines a value confidence type as a confidence type. **/
		typedef typename value_type::prediction_type		prediction_type;
		/** Defines a value class type as a class type. **/
		typedef typename value_type::class_type				class_type;
		/** Defines a value float type as a float type. **/
		typedef typename value_type::float_type				float_type;
		typedef prediction_record_set<float_type,Y,float_type> binary_set;
	public:
		/** Defines a prediction type. **/
		typedef typename value_type::pred_type				pred_type;
		/** Flags the record set of binary. **/
		enum{ is_binary=pred_type::is_binary };

	public:
		/** Constructs a prediction record set.
		 */
		prediction_record_set() : errmsg(0), write_idx(0)
		{
		}
		
	public:
		/** Resize the number of predictions (instances and bags) and runs.
		 * 
		 * @param n number of predictions.
		 * @param b number of bags.
		 * @param m number of runs.
		 */
		void resize(size_type n, size_type b, size_type m)
		{
		}
		/** Resize the number of predictions.
		 * 
		 * @param n number of predictions.
		 */
		void resize(size_type n)
		{
			prediction_vector::resize(n);
		}
		/** Get an error message.
		 * 
		 * @return an error message.
		 */
		const char* errormsg()const
		{
			return errmsg;
		}
		/** Set the write index, which record to write to an output stream. If index is zero, then
		 * write all records.
		 * 
		 * @param n index of record.
		 */
		void set_write_index(size_type n)
		{
			write_idx = n;
		}
		/** Test if record set is empty.
		 * 
		 * @return true if record set is empty.
		 */
		bool empty()const
		{
			return prediction_vector::empty();
		}
		/** Get the number of records in set.
		 * 
		 * @return number of records.
		 */
		size_type size()const
		{
			return prediction_vector::size();
		}
		/** Get record at index.
		 * 
		 * @param n an index.
		 * @return a record.
		 */
		reference operator[](size_type n)
		{
			return prediction_vector::operator[](n);
		}
		/** Get record at index.
		 * 
		 * @param n an index.
		 * @return a record.
		 */
		const_reference operator[](size_type n)const
		{
			return prediction_vector::operator[](n);
		}
		/**
		 * 
		 */
		void only_positive_bags(size_type b, size_type e)
		{
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				beg->only_positive_bags(b, e);
		}
		/** Set the prediction weights for type:
		 *	#. Use the class cost vector. (type 0)
		 * 	#. Weight each class by relative size (type 1)
		 * 	#. Set all weights to 1.0 (type 2)
		 * 
		 * @param costs a cost vector.
		 * @param type a type weight.
		 * @return an error message or NULL.
		 */
		const char* set_cost(const std::vector<float>& costs, int type)
		{
			const char* msg;
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				if( (msg=beg->set_cost(costs, type)) != 0 ) return msg;
			return 0;
		}
		/** Set the class labels using mapped unique values.
		 * 
		 * @param map a label map.
		 * @param type type of label.
		 * @return an error message or NULL.
		 */
		template<class M>
		const char* set_labels(const M& map, int type)
		{
			const char* msg;
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				if( (msg=beg->set_labels(map, type)) != 0 ) return msg;
			return 0;
		}
		/** Set binary threshold.
		 * 
		 * @param thresh
		 */
		void threshold(float thresh)
		{
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				beg->threshold(thresh);
		}
		/** Test if any prediction record is empty.
		 * 
		 * @return true if any record is empty.
		 */
		bool test_empty()const
		{
			for(const_iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;++beg)
				if( beg->test_empty() ) return true;
			return false;
		}
		/** Split the records into binary and multi-class.
		 * 
		 * @param bin saved binary records.
		 */
		void split(prediction_record_set<float_type,Y,float_type>& bin)
		{
			for(iterator beg = prediction_vector::begin(), end = prediction_vector::end();beg != end;)
			{
				if( beg->class_count() == 2 ) 
				{
					bin.resize(bin.size()+1);
					beg->copy(bin.back());
					prediction_vector::erase(beg);
					if(prediction_vector::empty()) break;
					beg = prediction_vector::begin();
					end = prediction_vector::end();
				}
				else ++beg;
			}
		}
		
	private:
		/** Write a prediction record set to an output stream.
		 * 
		 * @param out an output stream.
		 * @param record a prediction record set.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const prediction_record_set& record)
		{
			if( record.write_idx == 0 )
			{
				for(const_iterator beg = record.begin(), end= record.end();beg != end;++beg) out << *beg;
			}
			else
			{
				if( (record.write_idx-1) >= record.size() ) 
				{
					record.errmsg = ERRORMSG("No such record at index: " << record.write_idx << " only have " << record.size() << " records");
					out.setstate( std::ios::failbit );
					return out;
				}
				out << record[record.write_idx-1];
			}
			return out;
		}
		/** Read a prediction record set from an input stream.
		 * 
		 * @param in an input stream.
		 * @param record a prediction record set.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, prediction_record_set& record)
		{
			while( !in.eof() )
			{
				record.resize(record.size()+1);
				in >> record.back();
				ASSERTMSG( !record.front().test_empty() || (!in.eof() && in.fail()), record.size() );
				if( !in.eof() && in.fail() )
				{
					record.errmsg=record.back().errormsg();
					return in;
				}
				else if(record.back().empty()) break;
			}
			if( !record.empty() && record.back().empty() ) record.resize(record.size()-1);
			ASSERT( !record.test_empty() );
			if( record.empty() ) 
			{
				in.setstate( std::ios::failbit );
				record.errmsg = ERRORMSG("No predictions read");
				return in;
			}
			return in;
		}
		std::istream& error(std::istream& in, const char* msg)
		{
			errmsg = msg;
			in.setstate( std::ios::failbit );
			return in;
		}
		
	private:
		mutable const char* errmsg;
		size_type write_idx;
	};
};

#endif


