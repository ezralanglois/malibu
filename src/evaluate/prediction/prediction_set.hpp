/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_set.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_SET_HPP
#define _EXEGETE_PREDICTION_SET_HPP
#include "prediction.hpp"
#include <vector>


/** @file prediction_set.hpp
 * @brief Contains a set of predictions
 * 
 * This file contains the prediction set class template.
 *
 * @ingroup ExegetePrediction
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{

	/** @brief Holds a collection of predictions.
	 * 
	 * This class template serves as an interface to a collection of predictions.
	 */
	template<class P, class Y, class T=typename detail::default_threshold<P>::float_type>
	class prediction_set : public std::vector< prediction< P, Y, T > >
	{
		template<class P1, class Y1, class T1> friend class prediction_set;
		typedef std::vector< prediction< P, Y, T > >		prediction_vector;
	public:
		/** Defines a parent value type as a value type. **/
		typedef typename prediction_vector::value_type		value_type;
		/** Defines a parent iterator as an iterator. **/
		typedef typename prediction_vector::iterator		iterator;
		/** Defines a parent constant iterator as a constant iterator. **/
		typedef typename prediction_vector::const_iterator	const_iterator;
		/** Defines a parent size type as a size type. **/
		typedef typename prediction_vector::size_type		size_type;
		/** Defines a parent size type as a size type. **/
		typedef typename prediction_vector::pointer			pointer;
		/** **/
		typedef typename prediction_vector::reference		reference;
		typedef typename prediction_vector::const_reference	const_reference;
	public:
		/** Defines a value confidence type as a confidence type. **/
		typedef typename value_type::prediction_type		prediction_type;
		/** Defines a value class type as a class type. **/
		typedef typename value_type::class_type				class_type;
		/** Defines a value float type as a float type. **/
		typedef typename value_type::float_type				float_type;
	public:
		/** Defines a read type. **/
		typedef typename value_type::read_type 				read_type;
		/** Flags a prediction set as binary. **/
		enum{ is_binary=value_type::is_binary };
	private:
		typedef size_type*									size_pointer;

	public:
		/** Constructs a prediction set.
		 * 
		 * @param n number of predictions.
		 * @param b number of bags.
		 * @param m number of runs.
		 * @param c number of classes.
		 */
		prediction_set(size_type n=0, size_type b=0, size_type m=1, size_type c=2) : 
														prediction_vector(n*m+b*m), bag_end(prediction_vector::begin()), 
		  												ins_beg(prediction_vector::begin()), ins_end(prediction_vector::begin()),  
		  												run_int(m>1?m:0), class_cnt(c==0?2:c), bag_idx(0)
		{
			ASSERT(m!=0);
			if(b>0) bag_idx = ::resize(bag_idx, b);
			resize_itr(b);
		}
		/** Destructs a prediction set.
		 */
		~prediction_set()
		{
			::erase(bag_idx);
		}
		
	public://prediction_vector(set.size()*set.run_int+set.instance_size()*set.run_int)
		/** Constructs a copy of a prediction set.
		 * 
		 * @param set a prediction set to copy.
		 */
		prediction_set(const prediction_set& set) : prediction_vector(set), run_int(set.run_int), 
												    class_cnt(set.class_cnt), bag_idx(0)
		{
			assign_itr(set);
			value_type::assign(set.begin(), set.end(), prediction_vector::begin(), set.class_count());
			value_type::assign(set.instance_begin(), set.instance_end(), instance_begin(), set.class_count());
			
			ASSERT( std::distance(set.begin(), set.end()) == std::distance(prediction_vector::begin(), end()) );
			ASSERTMSG( set.actual_size() == actual_size(), set.actual_size() << " == " << actual_size() );
			ASSERTMSG( set.size() == size(), set.size() << " == " << size() );
		}
		/** Assigns a copy of predictions.
		 * 
		 * @param set a prediction set to copy.
		 * @return a reference to this object.
		 */
		prediction_set& operator=(const prediction_set& set)
		{
			prediction_vector::operator=(set);
			assign_itr(set);
			value_type::assign(set.begin(), set.end(), prediction_vector::begin(), set.class_count());
			value_type::assign(set.instance_begin(), set.instance_end(), instance_begin(), set.class_count());
			ASSERTMSG( set.actual_size() == prediction_vector::size(), set.actual_size() << " == " << prediction_vector::size() );
			return *this;
		}
		
	public:
		/** Copies a multi-class prediction set and transform to binary:
		 * 	- If binary, the second prediction is the threshold.
		 * 	- Else, assign as one-versus-one or one-versus-all
		 * 
		 * @param from a multi-class prediction set.
		 * @param pos the positive index.
		 * @param neg the negative index.
		 * @return true if at least one example is in each class.
		 */
		template<class P1, class T1>
		bool copy(const prediction_set<P1,Y,T1>& from, int pos=-1, int neg=-1)
		{
			if( from.class_count() == 2 )
			{
				resize(from.instance_size()==0?from.size():from.instance_size(), from.instance_size()==0?from.instance_size():from.size(), 1, 2);
				typename prediction_set<P1,Y,T1>::const_iterator beg = from.begin();
				typename prediction_set<P1,Y,T1>::const_iterator end = from.end();
				for(;beg != end;++beg, ++bag_end)
				{
					bag_end->set(beg->p_at(0), beg->y(), beg->p_at(1), beg->w());
				}
				beg = from.instance_begin();
				end = from.instance_end();
				for(;beg != end;++beg, ++ins_end)
				{
					ASSERT(ins_end < prediction_vector::end());
					ins_end->set(beg->p_at(0), beg->y(), beg->p_at(1), beg->w());
				}
				return true;
			}
			else 
			{
				ASSERT( from.instance_size() == 0 );
				return copy(from.begin(), from.end(), from.class_count(), pos, neg);
			}
		}
		/** Copies a multi-class prediction set and transform to binary:
		 * 	- One-versus-one
		 * 	- One-versus-all
		 * 
		 * @param beg an iterator to start of prediction collection.
		 * @param end an iterator to end of prediction collection.
		 * @param cl number of classes.
		 * @param pos a positive index.
		 * @param neg a negative index.
		 * @return true if at least one example is in each class.
		 */
		template<class I>
		bool copy(I beg, I end, unsigned int cl, int pos, int neg=-1)
		{
			typedef unsigned int uint;
			resize(std::distance(beg, end), 0, 1, 2);
			unsigned int n;
			bool is_pos=false;
			bool is_neg=false;
			float_type val;
			
			for(;beg != end;++beg, ++bag_end)
			{
				if( pos == beg->y() || ( pos == -1 && neg != beg->y() ) )
				{
					n = (unsigned int)beg->best_index(cl);
					val = beg->p_at(n);
					if( val < 0 ) val = -val;
					if ( n == uint(beg->y()) || (pos == -1 && int(n) != neg) )
						 bag_end->set( val, 1, 0.0f, beg->w() );
					else bag_end->set(-val, 1, 0.0f, beg->w() );
					if( !is_pos ) is_pos = true;
				}
				else if( neg == beg->y() || ( neg == -1 && pos != beg->y() ) )
				{
					n = (unsigned int)beg->best_index(cl);
					val = beg->p_at(n);
					if( val < 0 ) val = -val;
					if ( n == uint(beg->y()) || (neg == -1 && int(n) != pos) )
						 bag_end->set(-val, 0, 0.0f, beg->w() );
					else bag_end->set( val, 0, 0.0f, beg->w() );
					if( !is_neg ) is_neg = true;
				}
			}
			return is_pos && is_neg;
		}
		
	public:
		/** Resize the number of predictions.
		 * 
		 * @param n number of predictions.
		 */
		void resize(size_type n)
		{
			resize(n,0,2);
		}
		/** Resize the number of predictions and classes.
		 * 
		 * @param n number of predictions.
		 * @param c number of classes.
		 */
		void resize(size_type n, size_type c)
		{
			resize(n,0,c);
		}
		/** Resize the number of predictions (instances and bags), runs and classes.
		 * 
		 * @param n number of predictions.
		 * @param b number of bags.
		 * @param m number of runs.
		 * @param c number of classes.
		 */
		void resize(size_type n, size_type b, size_type m, size_type c)
		{
			if( n == 0 )
			{
				n = b;
				b = 0;
			}
			ASSERT(m!=0);
			resize(n*m, b*m, c);
			if(m>1) run_int=m;
		}
		/** Resize the number of predictions (instances and bags) and runs.
		 * 
		 * @param n number of predictions.
		 * @param b number of bags.
		 * @param c number of classes.
		 */
		void resize(size_type n, size_type b, size_type c)
		{
			prediction_vector::resize(n+b);
			resize_itr(b);
			class_cnt=(c==0?2:c);
			if(b>0) bag_idx = ::resize(bag_idx, b);
		}
		/** Clear the prediction set.
		 */
		void clear()
		{
			prediction_vector::clear();
			resize_itr();
		}
		/** Trim the prediction set to the actual size.
		 */
		void trim()
		{
			trim_itr();
			size_type b = size(), n = instance_size(), nb = std::distance(prediction_vector::begin(), instance_begin());
			prediction_vector::resize(std::distance(prediction_vector::begin(),n>0?instance_end():end()));
			bag_end = prediction_vector::begin()+b;
			ins_beg = ins_end = prediction_vector::begin()+nb;
			ins_end = ins_beg + n;
			if(instance_size()>0) bag_idx = ::resize(bag_idx, size());
		}
		
	public:
		/** Initalize the prediction set.
		 */
		void initialize()
		{
		}
		/** Add a prediction (confidence, class, threshold, weight) to the current position.
		 * 
		 * @param b an iterator to start of confidence collection.
		 * @param e an iterator to end of confidence collection.
		 * @param y a prediction class.
		 * @param w a prediction weight.
		 */
		void next(float_type* b, float_type* e, const class_type& y, float_type w=1.0f)
		{
			ASSERTMSG(ins_end < prediction_vector::end(), std::distance(prediction_vector::begin(), ins_end) << " < " << prediction_vector::size() );
			ins_end->set_copy(b, e, y, w);
			++ins_end;
		}
		/** Add a prediction (confidence, class, threshold, weight) to the current prediction.
		 * 
		 * @param p a prediction confidence.
		 * @param y a prediction class.
		 * @param t a prediction threshold.
		 * @param w a prediction weight.
		 */
		void next(prediction_type p, const class_type& y, float_type t, float_type w=1.0f)
		{
			ASSERTMSG(ins_end < prediction_vector::end(), std::distance(prediction_vector::begin(), ins_end) << " < " << prediction_vector::size() );
			set(ins_end, p, y, t, w);
			++ins_end;
		}
		/** Add a bag prediction (confidence, class, threshold, weight) to the current bag.
		 * 
		 * @param p a prediction confidence.
		 * @param y a prediction class.
		 * @param t a prediction threshold.
		 * @param w a prediction weight.
		 */
		void next_bag(prediction_type p, const class_type& y, float_type t, float_type w=1.0f)
		{
			ASSERT( bag_end < ins_beg );
			ASSERT( bag_idx != 0 );
			ASSERT( std::distance(prediction_vector::begin(),bag_end) < std::distance(prediction_vector::begin(),ins_beg) );
			bag_idx[std::distance(prediction_vector::begin(),bag_end)] = std::distance(ins_beg,ins_end);
			set(bag_end, p, y, t, w);
			++bag_end;
		}
		/** Move to next run.
		 */
		void next_run()
		{
			if( run_int < 2 ) trim();
		}
		/** Finalize the prediction record.
		 */
		void finalize()
		{
			if( run_int > 1 ) trim();
		}
		/** Finalize the addition of bags.
		 */
		void finalize_bag()
		{
			ins_beg = bag_end = ins_end;
		}
		/** Get the number of runs.
		 * 
		 * @return number of runs.
		 */
		size_type run_count()const
		{
			return run_int;
		}
		
	public:
		/** Get iterator to end bag.
		 * 
		 * @return iterator to end bag.
		 */
		iterator end()
		{
			return bag_end;
		}
		/** Get iterator to end bag.
		 * 
		 * @return iterator to end bag.
		 */
		const_iterator end()const
		{
			return bag_end;
		}
		/** Get iterator to start instance.
		 * 
		 * @return iterator to start instance.
		 */
		iterator instance_begin()
		{
			return ins_beg;
		}
		/** Get iterator to end instance.
		 * 
		 * @return iterator to end instance.
		 */
		iterator instance_end()
		{
			return ins_end;
		}
		/** Get iterator to start instance.
		 * 
		 * @return iterator to start instance.
		 */
		const_iterator instance_begin()const
		{
			return ins_beg;
		}
		/** Get iterator to end instance.
		 * 
		 * @return iterator to end instance.
		 */
		const_iterator instance_end()const
		{
			return ins_end;
		}
		/** Get actual number of predictions.
		 * 
		 * @return actual number of predictions.
		 */
		size_type actual_size()const
		{
			return prediction_vector::size();
		}
		/** Get instance at index.
		 * 
		 * @param n index.
		 * @return instance.
		 */
		reference instance_at(size_type n)
		{
			return prediction_vector::operator[](n+size());
		}
		/** Get instance at index.
		 * 
		 * @param n index.
		 * @return instance.
		 */
		const_reference instance_at(size_type n)const
		{
			return prediction_vector::operator[](n+size());
		}
		/** Get number of predictions (bags).
		 * 
		 * @return number of predictions.
		 */
		size_type size()const
		{
			return (size_type)std::distance(prediction_vector::begin(), end());
		}
		/** Get number of predictions (instances).
		 * 
		 * @return number of predictions (instances).
		 */
		size_type instance_size()const
		{
			return (size_type)std::distance(instance_begin(), instance_end());
		}
		/** Get number of classes.
		 * 
		 * @return number of classes.
		 */
		size_type class_count()const
		{
			return class_cnt;
		}
		/** Get start of bag indices.
		 * 
		 * @return pointer to bag indices.
		 */
		const size_pointer bagindex_begin()const
		{
			return bag_idx;
		}
		/** Test if prediction set is empty.
		 * 
		 * @return true prediction set is empty.
		 */
		bool test_empty()const
		{
			return size() == 0;
		}
		/** Get number of instances.
		 * 
		 * @return number of instances.
		 */
		size_type instance_count()const
		{
			if( ins_beg == ins_end )
			{
				if( end() != prediction_vector::begin() )
					return (size_type)std::distance(prediction_vector::begin(), end());
				return size_type(prediction_vector::size()-std::distance(prediction_vector::begin(), instance_begin()));
			}
			return (size_type)std::distance(instance_begin(), instance_end());
		}
		/** Get number of bags.
		 * 
		 * @return number of bags.
		 */
		size_type bag_count()const
		{
			if( instance_begin() == instance_end() )
			{
				return size_type(std::distance(prediction_vector::begin(), instance_begin()));
			}
			return (size_type)std::distance(prediction_vector::begin(), end());
		}
		
	public:
		/**
		 * 
		 */
		void only_positive_bags(size_type b, size_type e)
		{
			std::vector< prediction< P, Y, T > > tmp;
			size_type ncnt=0, bcnt=0;
			std::string bag, ins;
			tmp.resize(prediction_vector::size());
			iterator bit = tmp.begin();
			read_type arr;
			for(const_iterator beg = prediction_vector::begin(), fin = end();beg != fin;++beg)
			{
				if( beg->y() == 1 )
				{
					beg->read(arr);
					bit->set_copy(arr, arr+class_cnt, beg->y(), beg->w());
//std::cerr << "bag: " << bit->y() << std::endl;
					//*bit = *beg;
					bag = beg->y().label();
					for(const_iterator ibeg = instance_begin(), ifin = instance_end();ibeg != ifin;++ibeg)
					{
						ins = ( e > b ) ? ibeg->y().label().substr(b, e-b) : ibeg->y().label();
						if( ins == bag ) ncnt++;
					}
					bit++;
				}
			}
			bcnt = (size_type)std::distance(tmp.begin(), bit);
std::cerr << "b: " << bcnt << " n: " << ncnt << " | " << std::distance(instance_begin(), instance_end()) << " " << size() << std::endl;
			tmp.resize(bcnt+ncnt);
			bit = tmp.begin()+bcnt;
			
			size_type non=0;
			const_iterator end = tmp.begin()+bcnt;
			for(const_iterator ibeg = instance_begin(), ifin = instance_end(), beg, fin;ibeg != ifin;++ibeg)
			{
				ins = ( e > b ) ? ibeg->y().label().substr(b, e-b) : ibeg->y().label();
				for(beg = tmp.begin();beg != end;++beg)
				{
					if( ins == beg->y().label() )
					{
						ibeg->read(arr);
						bit->set_copy(arr, arr+class_cnt, ibeg->y(), ibeg->w());
						++bit;
						break;
					}
				}
				if( beg == end ) non++;
			}
			prediction_vector::resize(tmp.size());
std::cerr << "tot: " << tmp.size() << " -> " << non << std::endl;
			iterator it = prediction_vector::begin();
			for(const_iterator beg = tmp.begin(), fin = tmp.end();beg != fin;++beg, ++it)
			{
				beg->read(arr);
				it->set_copy(arr, arr+class_cnt, beg->y(), beg->w());
			}
			ins_beg = bag_end = prediction_vector::begin()+bcnt;
			ins_end = prediction_vector::end();
//std::cerr << "b: " << bcnt << " " << prediction_vector::begin()->y().label()  << " " << prediction_vector::begin()->p() << " " << prediction_vector::begin()->y() << std::endl;
//std::cerr << "b: " << bcnt << " " << instance_begin()->y().label()  << " " << instance_begin()->p() << " " << instance_begin()->y() << std::endl;
		}
		/** Set the class labels using mapped unique values.
		 * 
		 * @param map a label map.
		 * @param type type of label.
		 * @return an error message or NULL.
		 */
		template<class M>
		const char* set_labels(const M& map, int type)
		{
			typedef typename M::const_iterator const_map_iterator;
			typedef pair_first_less<std::string, float> less_type;
			std::string lbl;
			const_map_iterator it;
			if( instance_begin() == instance_end() )
			{
				if( type == 0 )
				{
					for(iterator beg=prediction_vector::begin(), fin = end();beg != fin;++beg)
					{
						lbl = beg->y().label();
						if( (it=std::lower_bound(map.begin(), map.end(), lbl, less_type())) == map.end() ) return ERRORMSG("Cannot find unqiue label: \"" << lbl << "\"");
						if( it->first != lbl ) return ERRORMSG("Cannot find unqiue label: \"" << lbl << "\"");
						beg->y(it->second);
					}
				}
				else
				{
					for(iterator beg=prediction_vector::begin(), fin = end(), st=beg;beg != fin;++beg)
					{
						if( size_type(std::distance(st, beg)) >= map.size() )  return ERRORMSG("Cannot find unqiue label: \"" << std::distance(st, beg) << "\"");
						beg->y(map[std::distance(st, beg)].second);
					}
				}
			}
			else
			{
				if( type == 0 )
				{
					for(iterator beg=prediction_vector::begin(), fin = end();beg != fin;++beg)
					{
						lbl = beg->y().label();
						if( (it=std::lower_bound(map.begin(), map.end(), lbl, less_type())) == map.end() ) return ERRORMSG("Cannot find unqiue label: \"" << lbl << "\"");
						if( it->first != lbl ) return ERRORMSG("Cannot find unqiue label: \"" << lbl << "\"");
						beg->y(it->second);
					}
				}
				else
				{
					for(iterator beg=prediction_vector::begin(), fin = end(), st=beg;beg != fin;++beg)
					{
						if( size_type(std::distance(st, beg)) >= map.size() )  return ERRORMSG("Cannot find unqiue label: \"" << std::distance(st, beg) << "\"");
						beg->y(map[std::distance(st, beg)].second);
					}
				}
			}
			return 0;
		}
		/** Set the prediction weights for type:
		 *	#. Use the class cost vector. (type 0)
		 * 	#. Weight each class by relative size (type 1)
		 * 	#. Set all weights to 1.0 (type 2)
		 * 
		 * @param costs a cost vector.
		 * @param type a type weight.
		 * @return an error message or NULL.
		 */
		const char* set_cost(const std::vector<float>& costs, int type)
		{
			if( type == 0 )
			{
				if( costs.empty() ) return 0;
				if( costs.size() != class_count() ) return ERRORMSG("Costs vector does not match number of classes.");
				set_cost_impl(costs, prediction_vector::begin(), end());
				set_cost_impl(costs, instance_begin(), instance_end());
			}
			else if( type == 1 )
			{
				set_cost_bal(prediction_vector::begin(), end(), class_count());
				set_cost_bal(instance_begin(), instance_end(), class_count());
			}
			else if( type == 2 )
			{
				for(iterator beg = prediction_vector::begin(), fin = end();beg != fin;++beg) beg->w( 1.0f );
				for(iterator beg = instance_begin(), fin = instance_end();beg != fin;++beg) beg->w( 1.0f );
			}
			return 0;
		}
		/** Set binary threshold.
		 * 
		 * @param thresh
		 */
		void threshold(float thresh)
		{
			for(iterator beg=prediction_vector::begin(), fin = end();beg != fin;++beg)
			{
				beg->p(beg->t());
				beg->t(thresh);
			}
			for(iterator beg=instance_begin(), fin = instance_end();beg != fin;++beg)
			{
				beg->p(beg->t());
				beg->t(thresh);
			}
		}
		
	private:
		void static set_cost_impl(const std::vector<float>& costs, iterator beg, iterator fin)
		{
			for(;beg != fin;++beg)
			{
				if( is_attribute_missing(beg->y()) ) continue;
				beg->w( costs[ (unsigned int)beg->y() ] );
			}
		}
		void static set_cost_bal(iterator beg, iterator fin, unsigned int m)
		{
			std::vector<float> costs(m, 0.0f);
			for(iterator curr=beg;curr != fin;++curr)
			{
				if( is_attribute_missing(curr->y()) ) continue;
				costs[ (unsigned int)curr->y() ]++;
			}
			for(typename std::vector<float>::iterator wbeg=costs.begin(), wend=costs.end();wbeg != wend;++wbeg)
				(*wbeg) = 1.0f / (*wbeg);
			set_cost_impl(costs, beg, fin);
		}
		
	private:
		void trim_itr()
		{
			if( bag_end == prediction_vector::begin() )
			{
				bag_end = ins_end;
				ins_end = ins_beg;
			}
		}
		void resize_itr(size_type b=0, size_type n=0)
		{
			bag_end = prediction_vector::begin();
			ins_beg = ins_end = prediction_vector::begin()+b;
		}
		void assign_itr(const prediction_set& set)
		{
			bag_end = prediction_vector::begin()+std::distance(set.begin(), const_iterator(set.bag_end));
			ins_beg = prediction_vector::begin()+std::distance(set.begin(), const_iterator(set.ins_beg));
			ins_end = prediction_vector::begin()+std::distance(set.begin(), const_iterator(set.ins_end));
			if(set.instance_begin() != set.begin() ) 
			{
				bag_idx = ::resize(bag_idx, std::distance(set.begin(), set.instance_begin()) );
				std::copy(set.bag_idx, set.bag_idx+set.size(), bag_idx);
			}
		}
		
	private:
		inline void set(iterator it, float_type p, const class_type& y, float_type t, float_type w)
		{
			it->set(p, y, t, w);
		}
		inline void set(iterator it, float_type* p, const class_type& y, float_type t, float_type w)
		{
			it->set_copy(p, p+class_cnt, y, w);
		}

	private:
		iterator bag_end;
		iterator ins_beg;
		iterator ins_end;
		size_type run_int;
		size_type class_cnt;
		size_pointer bag_idx;
	};

};


#endif


