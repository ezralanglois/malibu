/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_binding.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_BINDING_HPP
#define _EXEGETE_PREDICTION_BINDING_HPP
#include <boost/python/suite/indexing/indexing_suite.hpp>
#include <boost/python.hpp>
#include "prediction.hpp"

/** @file prediction_binding.hpp
 * 
 * @brief Binds a prediction to python
 * 
 * This file contains the prediction binding class template.
 *
 * @ingroup ExegetePrediction
 * @ingroup ExegetePython
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @defgroup ExegetePrediction Prediction System
 * 
 *  This group holds all the prediction related classes and function files.
 */


namespace exegete
{
	/* @brief Defines a prediction binding to python
	 * 
	 * This class template defines a prediction binding to python.
	 */


	template<class T>
	class prediction_binding;
	
	namespace detail
	{
		/** @brief Defines a prediction binding base
		 * 
		 * This class template binds several methods of a prediction to python including:
		 * 	- y: a property for setting and getting the class
		 * 	- is_binary: a property testing if the prediction is binary
		 */
		template<class T>
		class prediction_binding_base
		{
			typedef T prediction_type;
			typedef typename prediction_type::float_type float_type;
			typedef typename prediction_type::class_int class_type;
			
		public:
			/** Binds a set of functions to a prediction.
			 * 
			 * @param cl a python class binding.
			 */
			static void extension_def(::boost::python::class_<T>& cl)
			{
	        	cl
					.add_property("y", &get_y, &set_y)
					.add_property("is_binary", &is_binary)
	 			;
			}
			
		private:
			static bool is_binary(prediction_type& pred)
			{
				return prediction_type::is_binary;
			}
			static class_type get_y(prediction_type& pred)
			{
				return pred.y();
			}
			static void set_y(prediction_type& pred, class_type val)
			{
				pred.y(val);
			}
		};
		/** @brief Helper class for indexing suite
		 * 
		 * This class template defines a helper class for an indexing suite
		 * of a prediction binding.
		 */
	    template<class T>
	    class prediction_binding_policies : public prediction_binding< T > {};
	};
	/** @brief Defines a python binding to a binary prediction
	 * 
	 * This class template binds several methods of a binary prediction to python including:
	 *	- p: a property for setting and getting the confidence
	 *	- t: a property for setting and getting the threshold
	 *	- w: a property for setting and getting the weight
	 *	- __repr__: gets a string representation
	 *	- __str__: gets a string representation
	 */
	template<class T>
	class prediction_binding : public ::boost::python::def_visitor< prediction_binding<T> >
	{
		typedef T prediction_type;
		typedef typename T::class_type class_type;
		typedef typename T::prediction_type confidence_type;
		typedef typename T::float_type float_type;
	public:
		/** Binds a set of functions to a prediction.
		 * 
		 * @param cl a python class binding.
		 */
        void visit(::boost::python::class_<T>& cl)const
        {
        	detail::prediction_binding_base<T>::extension_def(cl);
        	cl
				.add_property("p", &get_p, &set_p)
				.add_property("t", &get_t, &set_t)
				.add_property("w", &get_w, &set_w)
				.def("__repr__", &as_str)
				.def("__str__", &as_str)
			;
        }
        
	private:
		static void set_w(prediction_type& pred, float_type val)
		{
			pred.w(val);
		}
		static float_type get_w(prediction_type& pred)
		{
			return pred.w();
		}
		static ::boost::python::str as_str(prediction_type& pred)
		{
			std::string lbl;
			std::string pstr;
			valueToString(pred.y(), lbl);
			valueToString(pred.p(), pstr);
			lbl+="\t";
			lbl+=pstr;
			return ::boost::python::str(lbl);
		}
		static confidence_type get_p(prediction_type& pred)
		{
			return pred.p();
		}
		static void set_p(prediction_type& pred, confidence_type val)
		{
			pred.p(val);
		}
		static confidence_type get_t(prediction_type& pred)
		{
			return pred.t();
		}
		static void set_t(prediction_type& pred, confidence_type val)
		{
			pred.t(val);
		}
	};
	/** @brief Defines a python binding to a multi-class prediction
	 * 
	 * This class template binds several methods of a multi-class prediction to python including:
	 *	- w: a property for setting and getting the weight
	 */
	template<class P, class Y>
	class prediction_binding< prediction<P*,Y,void> > :
		  public ::boost::python::indexing_suite< prediction<P*,Y,void>, 
		  detail::prediction_binding_policies<prediction<P*,Y,void> >, 
		  false, true, P, long, P >
	{
		typedef prediction<P*,Y,void> Container;
		typedef long index_type;
		typedef P key_type;
		typedef P float_type;
		typedef P data_type;
		typedef typename ::boost::mpl::if_< ::boost::is_class<P>, P&, P>::type return_type;
		
	public:
		/** Binds a set of functions to a prediction.
		 * 
		 * @param cl a python class binding.
		 */
        template <class Class>
        static void extension_def(Class& cl)
	    {
        	detail::prediction_binding_base<Container>::extension_def(cl);
			cl.add_property("w", &get_w, &set_w);//todo NOT CORRECT!!!!
		}
        
	private:
		static void set_w(Container& pred, float_type val)//todo NOT CORRECT!!!!
		{
			pred.w(val);
		}
		static float_type get_w(Container& pred)//todo NOT CORRECT!!!!
		{
			return pred.w();
		}

	public:
        static return_type get_item(Container& container, index_type i)
        { 
            return container.p_at(i);
        }
        static void set_item(Container& container, index_type i, data_type const& v)
        {
        	container.p()[i] = v;
        }
        static void delete_item(Container& container, index_type i)
        { 
        	delete_not_supported();
        }
        static size_t size(Container& container)
        {
        	size_not_supported();
            return 0;
        }
        static bool contains(Container& container, key_type const& key)
        {
        	contains_not_supported();
        	return false;
        }
        static bool compare_index(Container& /*container*/, index_type a, index_type b)
        {
            return a < b;
        }
        static index_type convert_index(Container& container, PyObject* i_)
        { 
        	::boost::python::extract<long> i(i_);
            if (i.check())
            {
                long index = i();
                if(index < 0) index_out_of_range();
                return index;
            }
            object_index_not_supported();
            return index_type();
        }
		
	private:
		static void contains_not_supported()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Contains not supported");
	        ::boost::python::throw_error_already_set();
	    }
		static void index_out_of_range()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Index out of range");
	        ::boost::python::throw_error_already_set();
	    }
		static void size_not_supported()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Size not supported");
	        ::boost::python::throw_error_already_set();
	    }
		static void set_not_supported()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Set not supported");
	        ::boost::python::throw_error_already_set();
	    }
		static void delete_not_supported()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Delete not supported");
	        ::boost::python::throw_error_already_set();
	    }
		static void object_index_not_supported()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Invalid index type");
	        ::boost::python::throw_error_already_set();
	    }
	};
};

#endif


