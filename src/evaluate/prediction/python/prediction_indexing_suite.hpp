/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_indexing_suite.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_INDEXING_SUITE_HPP
#define _EXEGETE_PREDICTION_INDEXING_SUITE_HPP
#include <boost/python.hpp>
#include <boost/python/suite/indexing/indexing_suite.hpp>

/** @file prediction_indexing_suite.hpp
 * 
 * @brief Holds a prediction indexing suite
 * 
 * This file contains the prediction indexing suite class template.
 *
 * @ingroup ExegetePrediction
 * @ingroup ExegetePython
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	/* @brief Defines a python binding to an indexing suite
	 * 
	 * This class template binds indexing methods a prediction set to a python including:
	 *	- resize: resize all elements in a container
	 *	- clear: clears a container
	 */
	template <class Container, class Ext, bool NoProxy, class DerivedPolicies>
	class prediction_indexing_suite;
	
	namespace detail
	{
		/** @brief Helper class for an indexing suite
		 * 
		 * This class template defines a helper class for an indexing suite.
		 */
	    template <class Container, class Ext, bool NoProxy>
	    class prediction_indexing_policies
	        : public prediction_indexing_suite<Container, Ext,
	            NoProxy, prediction_indexing_policies<Container, Ext, NoProxy> > {};
	};
	/** @brief Defines a python binding to an indexing suite
	 * 
	 * This class template binds indexing methods a prediction set to a python including:
	 *	- resize: resize all elements in a container
	 *	- clear: clears a container
	 */
	template<
		class Container, 
		class Ext,
		bool NoProxy=false, 
		class DerivedPolicies = detail::prediction_indexing_policies<Container, Ext, NoProxy>
	>
	class prediction_indexing_suite :
		public ::boost::python::indexing_suite<Container, DerivedPolicies, NoProxy, true >
	{
		typedef ::boost::python::object object;
	public:
	    /** Defines a container value type to a data type. **/
		typedef typename Container::value_type data_type;
	    /** Defines a container value type to a value type. **/
		typedef typename Container::value_type value_type;
	    /** Defines a container value type to a key type. **/
		typedef typename Container::value_type key_type;
	    /** Defines a container reference to a reference. **/
		typedef typename Container::reference reference;
    	/** Defines a container constant reference to a constant reference. **/
		typedef typename Container::const_reference const_reference;
	    /** Defines a container size type to an index type. **/
        typedef typename Container::size_type index_type;
		        
	public:
		/** Binds a set of functions to some object.
		 * 
		 * @param cl a python class binding.
		 */
        template <class Class>
        static void extension_def(Class& cl)
	    {
        	Ext::extension_def(cl);
        	cl
        		.def("resize", &resizeall )
    			.def("clear", &Container::clear)
        	;
        }
    	/** Get an item from a container at the specific index.
    	 * 
    	 * @param container a container.
    	 * @param i an index.
    	 * @return a reference to data at the specific index.
    	 */
        static reference get_item(Container& container, index_type i)
        { 
            return container[i];
        }
    	/** Set an item in a container at the specific index.
    	 * 
    	 * @param container a container.
    	 * @param i an index.
    	 * @param v a value.
    	 */
        static void set_item(Container& container, index_type i, data_type const& v)
        {
        	container[i] = v;
        }
    	/** Delete an item from a container at the specific index.
    	 * 
    	 * @param container a container.
    	 * @param i an index.
    	 */
        static void delete_item(Container& container, index_type i)
        { 
        	delete_not_supported();
        }
        /** Gets the size of the container.
         * 
    	 * @param container a container.
    	 * @return size of container.
         */
        static index_type size(Container& container)
        {
            return container.size();
        }
        /** Tests if the container contains the given key.
         * 
         * @note Not supported: throws exception.
         * 
    	 * @param container a container.
    	 * @param key a key value.
    	 * @return false.
         */
        static bool contains(Container& container, key_type const& key)
        {
        	contains_not_supported();
            return false;
        }
    	/** Compare two indices.
    	 * 
    	 * @param container a container.
    	 * @param a an index.
    	 * @param b an index.
    	 * @return true if a < b.
    	 */
        static bool compare_index(Container& container, index_type a, index_type b)
        {
            return a < b;
        }
        /** Converts a python index to an internal index.
         * 
    	 * @param container a container.
    	 * @param i_ a python object index.
    	 * @return internal index.
         */
        static index_type convert_index(Container& container, PyObject* i_)
        {
        	::boost::python::extract<long> i(i_);
            if (i.check())
            {
                long index = i();
                if(index < 0) index += DerivedPolicies::size(container);
                if(index >= long(container.size()) || index < 0) index_out_of_range();
                return index;
            }
            object_index_not_supported();
            return index_type();
        }
		
	private:
		static void resizeall(Container& container, index_type n, index_type b, index_type m)
		{
			container.resize(n, b, m);
		}
		static void contains_not_supported()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Contains not supported");
	        ::boost::python::throw_error_already_set();
	    }
		static void index_out_of_range()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Index out of range");
	        ::boost::python::throw_error_already_set();
	    }
		static void delete_not_supported()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Delete not supported");
	        ::boost::python::throw_error_already_set();
	    }
		static void object_index_not_supported()
	    {
	        PyErr_SetString(PyExc_RuntimeError, "Invalid index type");
	        ::boost::python::throw_error_already_set();
	    }
	};
};

#endif


