/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_record_binding.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_RECORD_BINDING_HPP
#define _EXEGETE_PREDICTION_RECORD_BINDING_HPP
#include <boost/python.hpp>
#include "prediction_indexing_suite.hpp"

/** @file prediction_record_binding.hpp
 * 
 * @brief Binds a prediction record to python
 * 
 * This file contains the prediction record binding class template.
 *
 * @ingroup ExegetePrediction
 * @ingroup ExegetePython
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	/* @brief Defines a python binding to a prediction record
	 * 
	 * This class template binds several methods of a prediction record to python including:
	 *	- algorithm: a property for setting and getting the algorithm
	 *	- validation: a property for setting and getting the validation
	 *	- dataset: a property for setting and getting the dataset
	 *	- run_count: a property for getting the run count
	 *	- class_count: a property for getting the class count
	 *	- instance_count: a property for getting the instance count
	 *	- bag_count: a property for getting the bag count
	 *	- is_binary: a property for getting whether the record is binary
	 */
	template <class Container, bool NoProxy, class DerivedPolicies>
	class prediction_record_binding;

	namespace detail
	{
		/** @brief Helper class for indexing suite
		 * 
		 * This class template defines a helper class for an indexing suite
		 * of a prediction record binding.
		 */
	    template <class Container, bool NoProxy>
	    class prediction_record_binding_policies
	        : public prediction_record_binding<Container, 
	            NoProxy, prediction_record_binding_policies<Container, NoProxy> > {};
	};

	/** @brief Defines a python binding to a prediction record
	 * 
	 * This class template binds several methods of a prediction record to python including:
	 *	- algorithm: a property for setting and getting the algorithm
	 *	- validation: a property for setting and getting the validation
	 *	- dataset: a property for setting and getting the dataset
	 *	- run_count: a property for getting the run count
	 *	- class_count: a property for getting the class count
	 *	- instance_count: a property for getting the instance count
	 *	- bag_count: a property for getting the bag count
	 *	- is_binary: a property for getting whether the record is binary
	 */
	template<
		class Container, 
		bool NoProxy=false, 
		class DerivedPolicies = detail::prediction_record_binding_policies<Container, NoProxy>
	>
	class prediction_record_binding :
		public prediction_indexing_suite<Container, DerivedPolicies, NoProxy >
	{
		typedef ::boost::python::object object;	
		typedef typename Container::size_type size_type;
	public:
		/** Binds a set of functions to a prediction record.
		 * 
		 * @param cl a python class binding.
		 */
        template <class Class>
        static void extension_def(Class& cl)
	    {
        	cl
        		.add_property("algorithm",  	&get_algorithm,  &set_algorithm)
        		.add_property("validation", 	&get_validation, &set_validation)
        		.add_property("dataset", 		&get_dataset, 	 &set_dataset)
				.add_property("run_count", 		&get_run_count)
        		.add_property("class_count", 	&get_class_count)
        		.add_property("instance_count", &get_instance_count)
        		.add_property("bag_count", 		&get_bag_count)
    			.add_property("is_binary", 		&is_binary)
        	;
        }
        
	private:
		static bool is_binary(Container& con)
		{
			return Container::is_binary;
		}
		static void set_algorithm(Container& pred, ::boost::python::str val)
		{
			std::string str = ::boost::python::extract<const char*>(val)();
			pred.algorithm(str);
		}
		static ::boost::python::str get_algorithm(Container& pred)
		{
			return ::boost::python::str(pred.algorithm());
		}
		static void set_validation(Container& pred, ::boost::python::str val)
		{
			std::string str = ::boost::python::extract<const char*>(val)();
			pred.validation(str);
		}
		static ::boost::python::str get_validation(Container& pred)
		{
			return ::boost::python::str(pred.validation());
		}
		static void set_dataset(Container& pred, ::boost::python::str val)
		{
			std::string str = ::boost::python::extract<const char*>(val)();
			pred.dataset(str);
		}
		static ::boost::python::str get_dataset(Container& pred)
		{
			return ::boost::python::str(pred.dataset());
		}
		
	private:
		static size_type get_run_count(const Container& con)
		{
			return con.run_count();
		}
		static size_type get_class_count(const Container& con)
		{
			return con.class_count();
		}
		static size_type get_instance_count(const Container& con)
		{
			return con.instance_count();
		}
		static size_type get_bag_count(const Container& con)
		{
			return con.bag_count();
		}
	};
};

#endif


