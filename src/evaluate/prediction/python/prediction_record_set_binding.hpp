/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_record_set_binding.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_RECORD_SET_BINDING_HPP
#define _EXEGETE_PREDICTION_RECORD_SET_BINDING_HPP
#include <boost/python.hpp>
#include "prediction_indexing_suite.hpp"
#include "stringutil.h"
#include "fileioutil.h"

/** @file prediction_record_set_binding.hpp
 * 
 * @brief Binds a prediction record set to python
 * 
 * This file contains the prediction record set binding class template.
 *
 * @ingroup ExegetePrediction
 * @ingroup ExegetePython
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	/* @brief Defines a python binding to a prediction record set
	 * 
	 * This class template binds several methods of a prediction record set to python including:
	 *	- read_file: a method to read a file
	 */
	template <class Container, bool NoProxy, class DerivedPolicies>
	class prediction_record_set_binding;

	namespace detail
	{
		/** @brief Helper class for an indexing suite
		 * 
		 * This class template defines a helper class for an indexing suite
		 * of a prediction record set binding.
		 */
	    template <class Container, bool NoProxy>
	    class prediction_record_set_binding_policies
	        : public prediction_record_set_binding<Container, 
	            NoProxy, prediction_record_set_binding_policies<Container, NoProxy> > {};
	};

	/** @brief Defines a python binding to a prediction record set
	 * 
	 * This class template binds several methods of a prediction record set to python including:
	 *	- read_file: a method to read a file
	 *	- read_files: a method to read a file
	 *	- filter_binary: splits binary and multi-class records
	 *	- str: reads the record set from a string
	 *	- str: writes the record set to a string
	 *	- __str__: a string representation
	 *	- is_binary: test whether the record set is binary
	 */
	template<
		class Container, 
		bool NoProxy=false, 
		class DerivedPolicies = detail::prediction_record_set_binding_policies<Container, NoProxy>
	>
	class prediction_record_set_binding :
		public prediction_indexing_suite<Container, DerivedPolicies, NoProxy >
	{
		typedef ::boost::python::object object;
		typedef typename Container::binary_set binary_set;
		typedef ::boost::python::extract<const char*> extract_str;
	public:
		/** Binds a set of functions to a prediction record set.
		 * 
		 * @param cl a python class binding.
		 */
        template <class Class>
        static void extension_def(Class& cl)
	    {
        	cl
				.def("read_file",  			&read_file)
				.def("read_files",  		&read_file_list)
				.def("filter_binary", 		&split_pred)
    			.def("str",  				&read_str)
        		.def("str",  				&write_str)
        		.def("__str__", 			&write_str)
    			.add_property("is_binary", 	&is_binary)
        	;
        }
        
	private:
		static void split_pred(Container& con, binary_set& bin)
		{
			con.split(bin);
		}
		static void read_file(Container& con, ::boost::python::str val)
		{
			read_file_impl(con, extract_str(val)());
		}
		static void read_file_list(Container& con, ::boost::python::list vals)
		{
			int len = ::boost::python::extract<int>(vals.attr("__len__")());
			for(int i=0;i<len;++i) 
			{
				read_file_impl(con, extract_str(vals[i])());
			}
		}
		static bool is_binary(Container& con)
		{
			return Container::is_binary;
		}
		static ::boost::python::str write_str(Container& con)
		{
			std::string lbl;
			valueToString(con, lbl);
			return ::boost::python::str(lbl);
		}
		static void read_str(Container& con, ::boost::python::str val)
		{
			const char* cval = extract_str(val)();
			std::istringstream convert( cval );
			convert >> con;
			if(!convert.eof() && convert.fail()) python_error(PyExc_StandardError, con.errormsg());
		}
		
	private:
		static void read_file_impl(Container& con, const char* file)
		{
			const char* msg;
			if( (msg=readfile(file, con)) != 0 )
			{
				 python_error(PyExc_StandardError, msg);
			}
		}
		static void python_error(PyObject *type, const char* msg)
		{
            PyErr_SetString(type, msg);
            ::boost::python::throw_error_already_set();
		}
	};
};

#endif



