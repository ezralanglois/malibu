/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_set_binding.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_SET_BINDING_HPP
#define _EXEGETE_PREDICTION_SET_BINDING_HPP
#include <boost/python.hpp>
#include "prediction_indexing_suite.hpp"

/** @file prediction_set_binding.hpp
 * 
 * @brief Binds a prediction set to python
 * 
 * This file contains the prediction set binding class template.
 *
 * @ingroup ExegetePrediction
 * @ingroup ExegetePython
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	template <class Container, bool NoProxy, class DerivedPolicies>
	class prediction_set_binding;

	namespace detail
	{
		/** @brief Helper class for indexing suite
		 * 
		 * This class template defines a helper class for an indexing suite
		 * of a prediction set binding.
		 */
	    template <class Container, bool NoProxy>
	    class prediction_set_binding_policies
	        : public prediction_set_binding<Container, NoProxy, prediction_set_binding_policies<Container, NoProxy> > {};
	    
	    /**@brief Helper class for indexing suite to instances
	     * 
	     * This class template defines policies for an index suite to instances (as opposed to bags) in a prediction set.
	     */
	    template <class Container>
   	    class InstancePolicies
   	    {
   	    public:
   	    	/** Defines a container value type to a data type. **/
   	    	typedef typename Container::value_type data_type;
   	    	/** Defines a container value type to a key type. **/
   	    	typedef typename Container::value_type key_type;
   	    	/** Defines a container size type to an index type. **/
   	        typedef typename Container::size_type index_type;
   	        
   	    public:
   	    	/** Get an item from a container at the specific index.
   	    	 * 
   	    	 * @param container a container.
   	    	 * @param i an index.
   	    	 * @return a reference to data at the specific index.
   	    	 */
   	        static data_type& get_item(Container& container, index_type i)
   	        { 
   	        	ASSERT(i<container.size());
   	            return container.instance_at(i);
   	        }
   	    	/** Compare two indices.
   	    	 * 
   	    	 * @param container a container.
   	    	 * @param a an index.
   	    	 * @param b an index.
   	    	 * @return true if a < b.
   	    	 */
   	        static bool compare_index(Container& container, index_type a, index_type b)
   	        {
   	            return a < b;
   	        }
   	        /** Converts a python index to an internal index.
   	         * 
   	    	 * @param container a container.
   	    	 * @param i_ a python object index.
   	    	 * @return internal index.
   	         */
   	        static index_type convert_index(Container& container, PyObject* i_)
   	        { 
   	        	::boost::python::extract<long> i(i_);
   	            if (i.check())
   	            {
   	                long index = i();
   	                if( index < 0 ) index += container.size();
   	                if( index >= long(container.size()) || index < 0 ) index_out_of_range();
   	                return index;
   	            }
   	            object_index_not_supported();
   	            return index_type();
   	        }
   	        /** Gets the size of the container.
   	         * 
   	    	 * @param container a container.
   	    	 * @return size of container.
   	         */
   	        static index_type size(Container& container)
   	        {
   	        	return container.instance_size();
   	        }
   	        
   	    private:
   			static void index_out_of_range()
   		    {
   		        PyErr_SetString(PyExc_RuntimeError, "Index out of range");
   		        ::boost::python::throw_error_already_set();
   		    }
   			static void object_index_not_supported()
   		    {
   		        PyErr_SetString(PyExc_RuntimeError, "Invalid index type");
   		        ::boost::python::throw_error_already_set();
   		    }
   	    };
	};

	/** @brief Defines a python binding to a prediction set
	 * 
	 * This class template binds several methods of a prediction set to python including:
	 *	- instance: get an instance from the set for specified index
	 *	- class_count: a property for getting the class count
	 *	- instance_count: a property for getting the instance count
	 *	- bag_count: a property for getting the bag count
	 *	- run_count: a property for getting the run count
	 *	- is_binary: a property for getting whether the record is binary
	 */
	template<
		class Container, 
		bool NoProxy=false, 
		class DerivedPolicies = detail::prediction_set_binding_policies<Container, NoProxy>
	>
	class prediction_set_binding :
		public prediction_indexing_suite<Container, DerivedPolicies, NoProxy >
	{
		typedef ::boost::python::object object;	
		typedef typename Container::size_type size_type;
		
		typedef ::boost::python::detail::container_element<Container, size_type, detail::InstancePolicies<Container>  > container_element_t;
		typedef ::boost::python::detail::proxy_helper< Container, detail::InstancePolicies<Container>, container_element_t, size_type> proxy_handler;
	public:
		/** Binds a set of functions to a prediction set.
		 * 
		 * @param cl a python class binding.
		 */
        template <class Class>
        static void extension_def(Class& cl)
	    {
        	proxy_handler::register_container_element();
        	cl
        		.def("instance", instance)
    			.add_property("class_count", 	&get_class_count)
    			.add_property("instance_count", &get_instance_count)
    			.add_property("bag_count", 		&get_bag_count)
    			.add_property("run_count", 		&get_run_count)
    			.add_property("is_binary", 		&is_binary)
        	;
        }
        
	private:
		static object instance( ::boost::python::back_reference<Container&> container, PyObject* i)
		{
			return proxy_handler::base_get_item_(container, i);
		}
		static bool is_binary(Container& con)
		{
			return Container::is_binary;
		}
		static size_type get_class_count(const Container& con)
		{
			return con.class_count();
		}
		static size_type get_instance_count(const Container& con)
		{
			return con.instance_count();
		}
		static size_type get_bag_count(const Container& con)
		{
			return con.bag_count();
		}
		static size_type get_run_count(const Container& con)
		{
			return con.run_count();
		}
	};
};

#endif


