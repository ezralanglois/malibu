/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_template.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_TEMPLATE_HPP
#define _EXEGETE_PREDICTION_TEMPLATE_HPP
#include "measure.hpp"
#include "prediction_record_set.hpp"
#include <iomanip>


/** @file prediction_template.hpp
 * 
 * @brief Calculates metrics and plots from a set of predictions and formats them
 * 
 * This file contains classes related to a prediction template, which calculates metrics 
 * and plots from a set of predictions and formats them.
 *
 * @ingroup ExegetePrediction
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	namespace detail
	{
		/** @brief Handles plotting binary predictions.
		 * 
		 * This class defines a handler for plotting measures of binary predictions.
		 */
		template<class P, class Y, class T>
		class plot_template_handler
		{
			typedef prediction_set< P, Y, T > 				predictions;
			typedef typename predictions::const_iterator 	const_iterator;
			typedef measure<const_iterator,double,P> 		measure_type;
		public:
			/** Defines a measure array as a metric array measure. **/
			typedef typename measure_type::array 			metric_array_measure;
			/** Defines a full measure as a metric measure. **/
			typedef typename measure_type::full 			metric_measure;
			/** Defines a plot measure as a plot measure. **/
			typedef typename measure_type::plot 			plot_measure;
			/** Defines a constant iterator to a plot as a constant plot iterator. **/
			typedef typename plot_measure::const_iterator 	const_plot_iterator;
			
		public:
			/** Constructs a plot template handler.
			 */
			plot_template_handler()
			{
			}
			/** Creates a plot for a set of predictions.
			 * 
			 * @param pplot iterator to specific plot functor.
			 * @param pred a set of predictions.
			 * @param px reference to pointer to x-coordinates.
			 * @param py reference to pointer to y-coordinates.
			 * @param n a flag passed to plot functor.
			 * @param isbag
			 * @return number of coordinates.
			 */
			unsigned int plot(const_plot_iterator pplot, const predictions& pred, double*& px, double*& py, unsigned int n, bool isbag)
			{
				if( isbag ) 
				{
					ASSERT( pred.instance_begin() != pred.instance_end() );
					return pplot->plot(pred.instance_begin(), pred.instance_end(), px, py, n);
				}
				else
				{
					ASSERT( pred.begin() != pred.end() );
					return pplot->plot(pred.begin(), pred.end(), px, py, n);
				}
			}
			/** Initializes a map with arguments.
			 * 
			 * @note does nothing
			 * 
			 * @param map some map.
			 * @param use dummy.
			 * @param pfx some argument name prefix.
			 */
			template<class U>
			void init(U& map, bool use=true, std::string pfx="")
			{
			}
		};
		/** @brief Handles plotting multi-class predictions.
		 * 
		 * This class defines a handler to plot multi-class predictions on a binary plot.
		 */
		template<class P, class Y, class T>
		class plot_template_handler<P*, Y, T>
		{
			typedef prediction_set< P*, Y, T > 			 	predictions;
			typedef prediction_set< P, Y, T > 			 	binary_predictions;
			typedef typename predictions::const_iterator 	const_iterator;
		public:
			/** Defines a measure array as a metric array measure. **/
			typedef typename measure<typename predictions::const_iterator, double, P*>::array 		metric_array_measure;
			/** Defines a full measure as a metric measure. **/
			typedef typename measure<typename predictions::const_iterator, double, P*>::full 		metric_measure;
			/** Defines a plot measure as a plot measure. **/
			typedef typename measure<typename binary_predictions::const_iterator, double, P>::plot 	plot_measure;
			/** Defines a constant iterator to a plot as a constant plot iterator. **/
			typedef typename plot_measure::const_iterator const_plot_iterator;
			
		public:
			/** Constructs a plot template handler.
			 */
			plot_template_handler() : pos_class(0), neg_class(-1) 
			{
			}
			/** Creates a plot for a set of predictions.
			 * 
			 * @param pplot iterator to specific plot functor.
			 * @param mcpred a set of multi-class predictions.
			 * @param px reference to pointer to x-coordinates.
			 * @param py reference to pointer to y-coordinates.
			 * @param n a flag passed to plot functor.
			 * @param isbag
			 * @return number of coordinates.
			 */
			unsigned int plot(const_plot_iterator pplot, const predictions& mcpred, double*& px, double*& py, unsigned int n, bool isbag)
			{
				binary_predictions pred;
				pred.copy(mcpred, pos_class, neg_class);
				ASSERT( pred.begin() != pred.end() );
				return pplot->plot(pred.begin(), pred.end(), px, py, n);
			}
			/** Initializes a map with arguments.
			 * 
			 * @param map some map.
			 * @param use should add arguments.
			 * @param pfx some argument name prefix.
			 */
			template<class U>
			void init(U& map, bool use=true, std::string pfx="")
			{
				if( use )
				{
					arginit(map, pos_class, pfx, "pos", "positive class (-1 for all classes, except negative)");
					arginit(map, neg_class, pfx, "neg", "negative class (-1 for all classes, except positive)");
				}
			}
			
		private:
			int pos_class;
			int neg_class;
		};
		/** @brief Handles calculating and writing metrics and plots in some format.
		 * 
		 * This class defines a handler calculate and write both metrics and plots in 
		 * some format.
		 */
		template<class P, class Y, class T>
		class prediction_template_handler : public plot_template_handler<P,Y,T>
		{
			typedef prediction_record_set< P, Y, T > 		record_set;
			typedef prediction_set< P, Y, T > 				prediction_type;
			typedef plot_template_handler<P,Y,T> 			plot_handler;
		public:
			/** Defines a plot handler array measure as a metric array measure. **/
			typedef typename plot_handler::metric_array_measure 	metric_array_measure;
			/** Defines a plot handler metric measure as a metric measure. **/
			typedef typename plot_handler::metric_measure 			metric_measure;
			/** Defines a plot handler plot measure as a plot measure. **/
			typedef typename plot_handler::plot_measure 			plot_measure;
		private:
			typedef typename metric_array_measure::const_iterator 	const_array_iterator;
			typedef typename metric_measure::const_iterator 		const_metric_iterator;
			typedef typename plot_measure::const_iterator 			const_plot_iterator;
			
		public:
			/** Constructs a prediction template handler referencing a given set of prediction
			 * records.
			 * 
			 * @param ref a prediction record set.
			 */
			prediction_template_handler(record_set& ref) : pset(&ref), eidx(0), ridx(0), avgBool(false), bagBool(false), px(0), py(0), r_index(0), r_size(0), c_index(0), c_size(0)
			{
			}
			/** Destructs a prediction template handler.
			 */
			~prediction_template_handler()
			{
				erase(px);
				erase(py);
			}
			
		public:
			/** Repeatedly writes indices, commas or flags specific conditions.
			 * 
			 * @param out an output stream.
			 * @param name a tag name.
			 * @param val extra parameters.
			 */
			void repeatwrite(std::ostream& out, const std::string& name, const char* val)
			{
				if( name == ("label") )
				{
					if( val == 0 ) return;
					if( *val == 'i' ) out << eidx+1;
					else if( *val == ',' && (eidx+1) < pset->size() ) out << ",";
				}
				else if( name == ("average")  ) avgBool=(true);
				else if( name == ("instance") ) bagBool=(true);
			}
			/** Tests if conditional tags should be written.
			 * 
			 * @param name a tag name.
			 * @param val extra parameters.
			 * @return true if tags should be written.
			 */
			bool repeatcheck(const std::string& name, const char* val)
			{
				if( name == ("instance") )		return (*pset)[eidx][ridx].instance_size() > 0;
				else if( name == ("average")  ) return (*pset)[eidx].size() > 1;
				else if( name == ("run") )		return (*pset)[eidx].size() == 1;
				return true;
			}
			/** Tests if repeatable tags should be repeated.
			 * 
			 * @param name a tag name.
			 * @param val extra parameters.
			 * @return true if tags should be repeated.
			 */
			bool repeatnext(const std::string& name, const char* val)
			{
				if( name == ("record") )		
				{
					eidx++;
					if( eidx < pset->size() ) return true;
					eidx=0;
				}
				else if( name == ("run") )
				{
					ridx++;
					if( ridx < ((*pset)[eidx].size()) ) return true;
					ridx=0;
				}
				else if( name == ("average")  ) avgBool=(false);
				else if( name == ("instance") ) bagBool=(false);
				return false;
			}

		public:
			/** Writes a label value to the output stream.
			 * 
			 * @param out an output stream.
			 * @param name a tag name.
			 * @param val extra parameters.
			 */
			void labelwrite(std::ostream& out, const std::string& name, const char* val)
			{
				if( val == 0 || *val == 'm' || *val == 'p' || *val == 0 ) 
				{
					if( name == ("dataset") )			out << (*pset)[eidx].dataset();
					else if( name == ("algorithm") )	out << (*pset)[eidx].algorithm();
					else if( name == ("validation") )	out << (*pset)[eidx].validation();
				}
				else if( *val == 'n' )
				{
					if( name == ("dataset") )			out << "Dataset";
					else if( name == ("algorithm") )	out << "Algorithm";
					else if( name == ("validation") )	out << "Validation";
				}
				else if( *val == 'c' )
				{
					if( name == ("dataset") )			out << "DAT";
					else if( name == ("algorithm") )	out << "ALG";
					else if( name == ("validation") )	out << "VLD";
				}
			}
			
		public:
			/** Writes (and calculates) a metric value to the output stream.
			 * 
			 * @param out an output stream.
			 * @param name a tag name.
			 * @param val extra parameters.
			 */
			void metricwrite(std::ostream& out, const std::string& name, const char* val)
			{
				const_metric_iterator pmetric = metric_measure::instance().findbycode(name);
				if( val == 0 || *val == 'm' || *val == 0 ) out << calculate(pmetric);
				else if( *val == 'p' ) out << std::setprecision(3) << calculate(pmetric)*100;
				else if( *val == 'n' ) out << pmetric->name();
				else if( *val == 'c' ) out << pmetric->code();
			}
			
		private:
			double calculate(const_metric_iterator metric)const
			{
				if( avgBool )
				{
					typedef typename record_set::value_type record;
					typedef typename record::const_iterator const_iterator;
					double avg=0.0f;
					unsigned int n=(unsigned int)(*pset)[eidx].size();
					for(const_iterator beg=(*pset)[eidx].begin(), end=(*pset)[eidx].end();beg != end;++beg)
						avg+=calculate(metric, *beg);
					return avg/double(n);
				}
				else return calculate(metric, *((*pset)[eidx].begin()+ridx));
			}
			double calculate(const_metric_iterator metric, const prediction_type& pred)const
			{
				if( bagBool ) 
				{
					ASSERTMSG(pred.instance_begin() != pred.instance_end(), eidx << " " << ridx);
					return metric->calculate(pred.instance_begin(), pred.instance_end(), pred.class_count());
				}
				ASSERTMSG(pred.begin() != pred.end(), eidx << " " << ridx);
				return metric->calculate(pred.begin(), pred.end(), pred.class_count());
			}
			
		public:
			/** Writes (and calculates) plot coordinates to the output stream.
			 * 
			 * @param out an output stream.
			 * @param name a tag name.
			 * @param val extra parameters.
			 */
			void plotwrite(std::ostream& out, const std::string& name, const char* val)
			{
				int n = 0;
				const_plot_iterator pplot = plot_measure::instance().findbycode(name);
				if( val != 0 && TypeUtil<int>::valid(val) ) stringToValue(val, n);
				if( r_index == 0 && isheader(val) ) //|| n != 0 && n != size)
				{
					r_size = plot_handler::plot(pplot, (*pset)[eidx][ridx], px, py, n, bagBool);
					ASSERT(((unsigned int)n)<r_size);
					if( n > 0 ) r_size = n;
				}
				if( islabel(val) )
				{
					std::string tmp;
					if( bagBool ) tmp=" instance-level";
						 if( *val == 'T' ) out << pplot->name()+tmp; //plot_handler::title(
					else if( *val == 'X' ) out << pplot->xaxis();
					else if( *val == 'Y' ) out << pplot->yaxis();
					return;
				}
				if( isheader(val) ) return;
				if ( n == 0 )
				{
					ASSERT(val != 0 && (val[0] == 'x' || val[0] == 'y') );
					if( val[0] == 'x' ) out << px[r_index];
					else out << py[r_index];
				}
			}
			/** Tests if should continue writing plot coordinates.
			 * 
			 * @param name a tag name.
			 * @param val extra parameters.
			 * @return true if more coordinates should be written.
			 */
			bool plotnext(const std::string& name, const char* val)
			{
				if( isheader(val) )
				{
					++r_index;
					if( r_index < r_size ) return true;
					r_index = 0;
				}
				return false;
			}

		private:
			bool isheader(const char* val)
			{
				return val == 0 || ( *val != 'x' && *val != 'y' && !islabel(val));
			}
			bool islabel(const char* val)
			{
				return val != 0 && (*val == 'T' || *val == 'X' || *val == 'Y');
			}
			
		public:
			/** Writes (and calculates) an array of metrics to the output stream.
			 * 
			 * @param out an output stream.
			 * @param name a tag name.
			 * @param val extra parameters.
			 */
			void metric_array_write(std::ostream& out, const std::string& name, const char* val)
			{
				int n = 0;
				const_array_iterator parr = metric_array_measure::instance().findbycode(name);
				if( parr == metric_array_measure::instance().end() ) return;
				if( val != 0 && TypeUtil<int>::valid(val) ) stringToValue(val, n);
				if( r_index == 0 && is_array_header(val) ) //|| n != 0 && n != size)
				{
					c_size = r_size = (*pset)[eidx][ridx].class_count();
					px = ::resize(px, r_size);
					parr->fill((*pset)[eidx][ridx].begin(), (*pset)[eidx][ridx].end(), px, r_size, c_index);
				}
				if( is_array_label(val) )
				{
						 if( *val == 'T' ) out << parr->name();
					else if( *val == 'C' ) out << r_index; //class name
					else if( *val == 'R' ) out << c_index; //class name
					return;
				}
				if( is_array_header(val) ) return;
				if( n == 0 )
				{
					out << px[r_index];
				}
			}
			/** Tests if should continue writing array elements.
			 * 
			 * @param name a tag name.
			 * @param val extra parameters.
			 * @return true if more elements should be written.
			 */
			bool metric_array_next(const std::string& name, const char* val)
			{
				if( name[0] != '_' && is_array_header(val) )
				{
					++r_index;
					if( r_index < r_size ) return true;
					r_index = 0;
				}
				if( name[0] == '_' && is_array_header(val) )
				{
					++c_index;
					if( c_index < c_size ) return true;
					c_index=0;
				}
				return false;
			}
			
		private:
			bool is_array_header(const char* val)
			{
				return val == 0 || ( *val != 'i' && !islabel(val));
			}
			bool is_array_label(const char* val)
			{
				return val != 0 && (*val == 'T' || *val == 'C' );
			}
			
		private:
			record_set* pset;
			unsigned int eidx;
			unsigned int ridx;
			bool avgBool;
			bool bagBool;

		private:
			double *px, *py;
			unsigned int r_index, r_size;
			unsigned int c_index, c_size;
		};
	};
	/** @brief Calculates and formats a set of metrics and plots from predictions
	 * 
	 * This class defines a prediction template, which calculates and formats a 
	 * set of metrics and plots from predictions.
	 */
	template<class P, class Y, class T>
	class prediction_template : public detail::prediction_template_handler<P,Y,T>
	{
		typedef prediction_record_set< P, Y, T > 				record_set;
		typedef detail::prediction_template_handler<P, Y, T> 	template_handler;
		typedef TemplateSymbolFunctor< template_handler > 		symbol_type;
		typedef Template< symbol_type > 						template_type;
	public:
		/** Defines a metric measure. **/
		typedef typename template_handler::metric_measure 		metric_measure;
		/** Defines a plot measure. **/
		typedef typename template_handler::plot_measure 		plot_measure;
		/** Defines a metric array measure. **/
		typedef typename template_handler::metric_array_measure metric_array_measure;
		

	public:
		/** Constructs a prediction template for a set of prediction records.
		 * 
		 * @param set a set of prediction records.
		 */
		prediction_template(record_set& set) : template_handler(set), format(*this)
		{
			format.add(symbol_repeat("template",	"repeat for each template") );
			format.add(symbol_repeat("record",		"repeat for each record") );
			format.add(symbol_repeat("run",			"repeat for each run") );
			format.add(symbol_repeat("average",		"use average") );
			format.add(symbol_repeat("instance",	"use instance-level") );
			format.add(symbol_repeat("label",		"use program defined label") );
			
			format.add(symbol_label("dataset",		"the name of the dataset") );
			format.add(symbol_label("validation",	"the name of the validation algorithm") );
			format.add(symbol_label("algorithm",	"the name of the machine learning algorithm") );
			
			const metric_measure& m = metric_measure::instance();
			for(typename metric_measure::const_iterator beg=m.begin(),end=m.end();beg != end;++beg)
				format.add( symbol_metric(beg->code(), beg->name()) );
			
			const plot_measure& g = plot_measure::instance();
			for(typename plot_measure::const_iterator beg=g.begin(),end=g.end();beg != end;++beg)
				format.add( symbol_plot(beg->code(), beg->name()) );

			const metric_array_measure& a = metric_array_measure::instance();
			for(typename metric_array_measure::const_iterator beg=a.begin(),end=a.end();beg != end;++beg)
			{
				format.add( symbol_metric_array(beg->code(), beg->name()) );
				format.add( symbol_metric_array("_"+beg->code()+"_", beg->name()) );
			}
		}
		
	public:
		/** Gets an error message.
		 *
		 * @return an error message.
		 */
		const char* errormsg()const
		{
			return format.errormsg();
		}
		/** Builds a template from a string.
		 *
		 * @param layout a string containing the template layout.
		 * @return an error message or NULL.
		 */
		const char* build(const std::string& layout)
		{
			return format.build(layout);
		}

	private:
		symbol_type* symbol_repeat(const char* nm, const char* ds)
		{
			return new symbol_type(nm, ds, &template_handler::repeatwrite, &template_handler::repeatnext, &template_handler::repeatcheck);
		}
		symbol_type* symbol_label(const char* nm, const char* ds)
		{
			return new symbol_type(nm, ds, &template_handler::labelwrite, 0, 0);
		}
		symbol_type* symbol_metric(const std::string& nm, const std::string& ds)
		{
			return new symbol_type(nm, ds, &template_handler::metricwrite, 0, 0);
		}
		symbol_type* symbol_plot(const std::string& nm, const std::string& ds)
		{
			return new symbol_type(nm, ds, &template_handler::plotwrite, &template_handler::plotnext, 0);
		}
		symbol_type* symbol_metric_array(const std::string& nm, const std::string& ds)
		{
			return new symbol_type(nm, ds, &template_handler::metric_array_write, &template_handler::metric_array_next, 0);
		}
		
	private:
		friend std::ostream& operator<<(std::ostream& out, const prediction_template& templ)
		{
			out << templ.format;
			return out;
		}
		friend std::istream& operator>>(std::istream& in, prediction_template& templ)
		{
			in >> templ.format;
			return in;
		}
		
	private:
		template_type format;
	};
};

#endif


