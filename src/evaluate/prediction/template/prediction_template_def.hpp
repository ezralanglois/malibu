/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_template_def.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_TEMPLATE_DEF_HPP
#define _EXEGETE_PREDICTION_TEMPLATE_DEF_HPP
#include "prediction_template_format.hpp"
#include "fileioutil.h"
#include <map>

/** @file prediction_template_def.hpp
 * 
 * @brief Provides a set of standard formats for prediction metrics and plots
 * 
 * This file contains the prediction template definitions class template, which provides a set 
 * of standard formats for prediction metrics and plots
 *
 * @see prediction_template_format.hpp
 * @ingroup ExegetePrediction
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	/** @brief Provides a set of standard formats for prediction metrics and plots
	 * 
	 * This class provides a set of standard formats for prediction metrics and plots.
	 * 
	 * Binary Metrics
	 * 	- std
	 * 		- Classification metrics
	 * 		- ACC,SEN,SPE,AUR
	 * 	- med
	 * 		- Medical metrics
	 * 		- ACC,SEN,SPE,PRE,NPV
	 * 	- inf
	 * 		- Information retrieval metrics
	 * 		- PRE,SEN,FSC
	 * 	- thr
	 * 		- Threshold metrics
	 * 		- ACC,SEN,SPE,PRE,NPV,FSC,MCC,LFT
	 * 	- ord
	 * 		- Ranking metrics
	 * 		- AUR,AUL,AUP,AUN,AUB
	 * 	- prb
	 * 		- Probability metrics
	 * 		- RME,CXE
	 * 	- con
	 * 		- Confusion matrix
	 * 		- TP,TN,FP,FN,THR
	 * 	- long
	 * 		- All metrics
	 * 		- ACC,SEN,SPE,PRE,NPV,FSC,MCC,LFT,AUR,AUL,AUP,AUN,AUB,RME,CXE,TP,TN,FP,FN,THR
	 * 
	 * Multi-class Metrics
	 * 	- std
	 * 		- Multi-class metrics
	 * 		- ACC,SEN,PRE,AUR
	 * 	- ovo_thr
	 * 		- One-versus-one threshold metrics
	 * 		- SEN,PRE,LFT,FSC,MCC
	 * 	- ova_thr
	 * 		- One-versus-all threshold metrics
	 * 		- OVA_SEN,OVA_PRE,OVA_LFT,OVA_FSC,OVA_MCC
	 * 	- ovo_ord
	 * 		- One-versus-one ranking metrics
	 * 		- AUR,AUL,AUP,AUN,AUB
	 * 	- ova_ord
	 * 		- One-versus-all ranking metrics
	 * 		- OVA_AUR,OVA_AUL,OVA_AUP,OVA_AUN,OVA_AUB
	 * 	- prob
	 * 		- Probability metrics
	 * 		- RME,CXE
	 * 	- long
	 * 		- All metrics
	 * 		- ACC,SEN,PRE,LFT,FSC,MCC,AUR,AUL,AUP,AUN,AUB,RME,CXE,OVA_SEN,OVA_PRE,OVA_LFT,OVA_FSC,OVA_MCC,OVA_AUR,OVA_AUL,OVA_AUP,OVA_AUN,OVA_AUB
	 * 	- rcnt
	 * 		- Count predicted to each class
	 * 		- RCNT
	 * 
	 * Plots
	 * 	- Receiver operating curve
	 * 	- Lift curve
	 * 	- Cost curve
	 * 	- Lower envelope curve
	 * 	- Extended lower envelope curve
	 * 	- Reliability curve
	 * 	- Precision/Recall curve
	 * 	- Negative predictive/Specificity curve
	 * 
	 * 
	 */
	class prediction_template_def
	{
		typedef std::pair< std::string, std::string > 	template_pair;
		typedef std::map< std::string, template_pair >	template_map;
		typedef template_map::value_type				template_value;
		typedef template_map::const_iterator			const_iterator;
	public:
		/** Constructs a prediction template definition.
		 * 
		 * @param binary if true create binary metrics.
		 */
		prediction_template_def(bool binary=true)
		{
			if( binary )
			{
				insert_metric("std", "ACC,SEN,SPE,AUR", 										"Classification metrics");
				insert_metric("med", "ACC,SEN,SPE,PRE,NPV", 									"Medical metrics");
				insert_metric("inf", "PRE,SEN,FSC", 											"Information retrieval metrics");
				insert_metric("thr", "ACC,SEN,SPE,PRE,NPV,FSC,MCC,LFT", 						"Threshold metrics");
				insert_metric("ord", "AUR,AUL,AUP,AUN,AUB", 									"Ranking metrics");
				insert_metric("prb", "RME,CXE", 												"Probability metrics");
				insert_metric("con", "TP,TN,FP,FN,THR", 										"Confusion matrix");
				insert_metric("long", "ACC,SEN,SPE,PRE,NPV,FSC,MCC,LFT,AUR,AUL,AUP,AUN,AUB,RME,CXE,TP,TN,FP,FN,THR", 	"All metrics");
			}
			else
			{
				insert_metric("std", 	 "ACC,SEN,PRE,AUR", 										"Multi-class metrics");
				insert_metric("ovo_thr", "SEN,PRE,LFT,FSC,MCC", 									"One-versus-one threshold metrics");
				insert_metric("ova_thr", "OVA_SEN,OVA_PRE,OVA_LFT,OVA_FSC,OVA_MCC", 				"One-versus-all threshold metrics");
				insert_metric("ovo_ord", "AUR,AUL,AUP,AUN,AUB", 									"One-versus-one ranking metrics");
				insert_metric("ova_ord", "OVA_AUR,OVA_AUL,OVA_AUP,OVA_AUN,OVA_AUB", 				"One-versus-all ranking metrics");
				insert_metric("prob", 	 "RME,CXE", 												"Probability metrics");
				insert_metric("long", 	 "ACC,SEN,PRE,LFT,FSC,MCC,AUR,AUL,AUP,AUN,AUB,RME,CXE,OVA_SEN,OVA_PRE,OVA_LFT,OVA_FSC,OVA_MCC,OVA_AUR,OVA_AUL,OVA_AUP,OVA_AUN,OVA_AUB", 	"All metrics");
				insert_metric("struct", 	 "NORMCOST,COST,TOTCOST", 								"Structured cost");
			}
			//
			//
			insert_plot("roc", "ROC", "Receiver operating curve");
			insert_plot("lfc", "LFC", "Lift curve");
			insert_plot("cst", "CST", "Cost curve");
			insert_plot("env", "ENV", "Lower envelope curve");
			insert_plot("enx", "ENVX", "Extended lower envelope curve");
			insert_plot("rel", "REL", "Reliability curve");
			insert_plot("prc", "PRC", "Precidion/Recall curve");
			insert_plot("npr", "NPR", "Negative predictive/Specificity curve");			
			
			insert_array("rcnt", "RCNT", "Count predicted to each class");
		}
		
	private:
		void insert_array(const std::string& nm, std::string str, const std::string& des)
		{
			typedef detail::array_format_def def;
			insert(nm, 	 			def::standard(str.c_str(), true), des);
			//
			insert(nm+".i", 	 	def::standard(str.c_str(), false), des);
		}
		
	private:
		void insert_plot(const std::string& nm, std::string str, const std::string& des)
		{
			typedef detail::plot_format_def def;
			insert(nm, 	 	def::gnuplot(str.c_str(), true), des);
			insert(nm+".d", def::gnuplot(str.c_str(), true, "dataset", "algorithm"), des+" comparing algorithms");
			insert(nm+".a", def::gnuplot(str.c_str(), true, "algorithm", "dataset"), des+" comparing datasets");
			insert(nm+".b", def::gnuplot(str.c_str(), true, ""), des+" comparing both datasets and algorithms");

			insert(nm+".i",   def::gnuplot(str.c_str(), false), des+" on instance-level");
			insert(nm+".i.d", def::gnuplot(str.c_str(), false, "dataset", "algorithm"), des+" comparing algorithms on instance-level");
			insert(nm+".i.a", def::gnuplot(str.c_str(), false, "algorithm", "dataset"), des+" comparing datasets on instance-level");
			insert(nm+".i.b", def::gnuplot(str.c_str(), false, ""), des+" comparing both datasets and algorithms on instance-level");
		}
		void insert_metric(const std::string& nm, std::string str, const std::string& ds)
		{
			typedef detail::metric_format_def def;
			str = "algorithm,dataset,validation,"+str;
			insert(nm, def::standard(str.c_str()), ds);
			insert(nm+".htm", def::html(str.c_str()), ds+" in html");
			insert(nm+".tex", def::latex(str.c_str()), ds+" in latex");
		}
		
	public:
		/** Gets the name of the default template (file).
		 * 
		 * @return std
		 */
		std::string default_file()const
		{
			return "std";
		}
		/** Build a template from an id:
		 * 	- if id corresponds to a built-in template, return this template
		 * 	- otherwise search for a file with the corresponding name and read in the template
		 * 
		 * @param id a template id
		 * @return a corresponding template.
		 */
		std::string build(const std::string& id)const
		{
			const_iterator it = templ_map.find(id);
			if( it == templ_map.end() ) 
			{
				std::string buffer;
				const char* msg = readfile2buffer(id, buffer);
				if( msg != 0 ) std::cerr << "Error reading file: " << id << std::endl;
				return buffer;
			}
			return it->second.second;
		}
		/** Writes the built in templates to an output stream.
		 * 
		 * @param out an output stream.
		 * @param def a prediction template def.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const prediction_template_def& def)
		{
			for(const_iterator beg = def.templ_map.begin(), end = def.templ_map.end();beg != end;++beg)
				out << beg->first << ": " << beg->second.first << "\n";
			return out;
		}
		
	private:
		void insert(const std::string& n, const std::string& s, const std::string& d)
		{
			templ_map.insert(std::make_pair(n, std::make_pair(d,s)));
		}
		
	private:
		template_map templ_map;
	};
};

#endif


