/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * prediction_template_format.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PREDICTION_TEMPLATE_FORMAT_HPP
#define _EXEGETE_PREDICTION_TEMPLATE_FORMAT_HPP
#include "TemplateDefNode.h"
#include <string>

/** @file prediction_template_format.hpp
 * 
 * @brief Contains formats for metrics and plots.
 * 
 * This file contains formats for prediction metrics and plots.
 * 	- metric
 * 		- standard
 * 		- html
 * 		- latex
 * 	- plot
 * 		- gnuplot
 * 	- array
 * 		- standard
 *
 * @ingroup ExegetePrediction
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	namespace detail 
	{
		/** @brief Formats for metrics 
		 * 
		 * This class contains methods to format a set of metrics.
		 */
		struct metric_format_def
		{
			/** Creates a standard format for a list of metrics.
			 * 
			 * @param metrics a comma separated list of metrics.
			 * @return a metric template.
			 */
			static std::string standard(const char* metrics)
			{
				typedef TemplateDefNode::pointer pointer;
				TemplateDefNode templ;
				pointer rcd, tmp, def;
				def=templ.set("template", "TYPE\t", "\n");
	
				def->sadd(metrics, "", "\t", 'c');
				rcd=def->add("record", "\n", "");
				tmp=rcd->add("run", "RUN\t", "");
				tmp->sadd(metrics, "", "\t", 0);
				tmp=rcd->add("average", "\nAVG\t", "");
				tmp->sadd(metrics, "", "\t", 0);
	
				rcd=def->add("instance", "\n\nTYPE\t", "\n");
				rcd->sadd(metrics, "", "\t", 'c');
				rcd=rcd->add("record", "\n", "");
				tmp=rcd->add("run", "IRUN\t", "");
				tmp->sadd(metrics, "", "\t", 0);
				tmp=rcd->add("average", "\nIAVG\t", "");
				tmp->sadd(metrics, "", "\t", 0);
				return templ.str();
			}
			/** Creates a latex format for a list of metrics.
			 * 
			 * @param metrics a comma separated list of metrics.
			 * @return a metric template.
			 */
			static std::string latex(const char* metrics)
			{
				typedef TemplateDefNode::pointer pointer;
				TemplateDefNode templ;
				pointer rcd, tmp, def;
				def=templ.set("template", "\\begin{tabular}{ll|cccc}\\hline\\hline\n", "\\hline\n\\end{tabular}\n");
				def->sadd(metrics, "", " & ", 'n');
				def->add("none", "\\\\\\hline\n", "");
				rcd=def->add("record", "", "\\\\\n");
				tmp=rcd->add("run", "", "");
				tmp->sadd(metrics, "", " & ", 'p');
				tmp=rcd->add("average", "", "");
				tmp->sadd(metrics, "", " & ", 'p');
	
				rcd=def->add("instance", "\\hline\n\\end{tabular}\n\\begin{tabular}{ll|cccc}\\hline\\hline\n", "\\hline\n\\end{tabular}\n");
				rcd->sadd(metrics, "", " & ", 'n');
				rcd=rcd->add("record", "", "\\\\\n");
				tmp=rcd->add("run", "", "");
				tmp->sadd(metrics, "", " & ", 'p');
				tmp=rcd->add("average", "", "");
				tmp->sadd(metrics, "", " & ", 'p');
				return templ.str();
			}
			/** Creates a HTML format for a list of metrics.
			 * 
			 * @param metrics a comma separated list of metrics.
			 * @return a metric template.
			 */
			static std::string html(const char* metrics)
			{
				typedef TemplateDefNode::pointer pointer;
				TemplateDefNode templ;
				pointer rcd, tmp, def;
				def=templ.set("template", "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody>\n<tr>\n", "</tbody></table>\n");
	
				def->sadd(metrics, "\t<td>", "</td>\n", 'c');
				rcd=def->add("record", "<tr>\n", "</tr>\n");
				tmp=rcd->add("run", "", "");
				tmp->sadd(metrics, "\t<td>", "</td>\n", 0);
				tmp=rcd->add("average", "<td>AVG</td>\n", "");
				tmp->sadd(metrics, "\t<td>", "</td>\n", 0);
	
				rcd=def->add("instance", "</tbody></table>\n<br/>\nInstance-level Comparison\n<br/>\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tbody>\n<tr>\n", "");//\t<td>TYPE</td>\n
				rcd->sadd(metrics, "\t<td>", "</td>\n", 'c');
				rcd=rcd->add("record", "<tr>\n", "</tr>\n");
				tmp=rcd->add("run", "", "");
				tmp->sadd(metrics, "\t<td>", "</td>\n", 0);
				tmp=rcd->add("average", "<td>AVG</td>\n", "");
				tmp->sadd(metrics, "\t<td>", "</td>\n", 0);
				return templ.str();
			}
		};
		/** @brief Formats for plots 
		 * 
		 * This class contains methods to format a plot.
		 */
		struct plot_format_def
		{
			/** Creates a GNUPlot format for a set of plot coordinates.
			 * 
			 * @param plot a plot.
			 * @param isbag if false use instances in MIL data.
			 * @param key a legend of the plot.
			 * @param tit a title of the plot.
			 * @return a plot template.
			 */
			static std::string gnuplot(const char* plot, bool isbag, const char* key=0, const char* tit=0)
			{
				typedef TemplateDefNode::pointer pointer;
				TemplateDefNode templ;
				pointer rcd, tmp, pdef;
				std::string str, pstr=plot, title, empty;
				if( key == 0 ) str="unset key\n";
				else str="set key 0.8,0.8\n";
				title = str+"set terminal postscript enhanced color \"Times-Roman\" 24\nset output \"plot.eps\"\n";
				
				pdef=templ.set("template", (isbag?title:empty), "");
				if( !isbag ) pdef=pdef->add("instance", title, "");
				
				pdef->add(plot, "set title \"", "", 'T');
				if(key==0)		pdef->add("algorithm", " Comparison of ", " over <!-- dataset //!-->\"\n", 0); 
				else if(tit!=0) pdef->add(tit, " comparison of ", "\"\n", 0);
				else pdef->add("", "", "\"\n", 0);
				pdef->add(plot, "set xlabel \"", "\"\n", 'X');
				pdef->add(plot, "set ylabel \"", "\"\nplot ", 'Y');
	
				rcd=pdef->add("record", "", "");
				rcd->add("label", "\"-\" using 1:2 with lines lw 3 lt ", "", 'i');
				if( key != 0 )
				{
					if( *key == '\0' ) rcd->add(key, " title \"<!-- algorithm //!--> - <!-- dataset //!-->", "\"", 0);
					else rcd->add(key, " title \"", "\"", 0);
				}
				rcd->add("label", "", "", ',');
				if(pstr == "ENVX")
				{
					rcd=pdef->add("record", ",", "");
					rcd->add("label", "\"-\" using 1:2 with lines lw 3 lt ", "", 'i');
					rcd->add("", " title \"", "\"", 0);
					//rcd->add("label", "", "", ',');
					
					rcd=pdef->add("record", "\n", "e\n");
					tmp=rcd->add(plot, "", "\n");
					tmp->add(plot, "", "\n", '5');
					tmp->add(plot, "0.0\t", "", 'x');
					tmp->add(plot, "\n1.0\t", "", 'y');
					
					rcd=pdef->add("record", "\n", "e\n");
					tmp=rcd->add(plot, "", "\n");
					tmp->add(plot, "", "", 'x');
					tmp->add(plot, "\t", "", 'y');
				}
				else if( pstr == "CST" )
				{
					rcd=pdef->add("record", "\n", "e\n");
					tmp=rcd->add(plot, "", "\n");
					tmp->add(plot, "0.0\t", "", 'x');
					tmp->add(plot, "\n1.0\t", "", 'y');
				}
				else
				{
					rcd=pdef->add("record", "\n", "e\n");
					tmp=rcd->add(plot, "", "\n");
					tmp->add(plot, "", "", 'x');
					tmp->add(plot, "\t", "", 'y');
				}
				return templ.str();
			}
		};
		/** @brief Formats for an array of class counts
		 * 
		 * This class contains methods to format an array of class counts.
		 */
		struct array_format_def
		{
			/** Creates a standard format for a set of metrics.
			 * 
			 * @param metric a specific metric for row.
			 * @param isbag if false use instances in MIL data.
			 * @param tit a title of the class array.
			 * @return a count template.
			 */
			static std::string standard(const char* metric, bool isbag, const char* tit=0)
			{
				typedef TemplateDefNode::pointer pointer;
				TemplateDefNode templ;
				pointer rcd, tmp, pdef;
				
				pdef=templ.set("template", "", "");
				if( !isbag ) pdef=pdef->add("instance", "", "");
				pdef->add(metric, "", "\n", 'T');
	
				rcd=pdef->add("record", "\n", "\n");
				rcd->add(metric, "", "\n", 'T');
				tmp=rcd->add("_"+std::string(metric)+"_", "", "\n");
				tmp=tmp->add(metric, "", "");
				tmp->add(metric, " ", "", 'i');
				return templ.str();
			}
		};
	};
};

#endif


