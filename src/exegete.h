/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * exegete.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_EXEGETE_H
#define _EXEGETE_EXEGETE_H
#include "exegete_vs.h"
#include "ArgumentMap.h"
#include "version_utility.hpp"
#include "mpi/mpi_def.hpp"

/** @mainpage malibu: A Machine Learning Workbench
 * 
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * 
 * @section Intro Introduction
 * malibu is collection of a machine learning algorithms and utilities. It has the
 * following features:
 * 
 *	- Learning Algorithms
 * 		- Binary Classification
 * 		- Importance-weighted Binary Classification
 * 		- Cost-sensitive Binary Classification
 * 		- Multi-class Classification
 * 		- Multi-class Probablistic Classification
 * 		- Probablistic regression
 * 		- Quantile regression
 * 		- Multiple-instance learning
 *	- Unified user-interface
 * 		- command-line driven
 * 		- documented configuration file
 * 		- customizable configuration output
 * 		- modular configuration files
 *	- Validation for every learning algorithm
 * 		- Supports many automated methods, e.g. Cross-validation
 * 		- Repeated experiments
 * 		- Model files for each training iteration
 * 		- Graphing for tree-based algorithms
 *	- Parameter selection for every learning algorithm
 * 		- Use at least 10 different measures to select best parameter set
 * 		- Support for all validation algorithms listed above
 * 		- Write each parameter combination and corresponding measure
 *	- Dataset parsing
 * 		- Choose any column as class attribute
 * 		- Choose any number of prefixing labels
 * 		- Attribute header (optional)
 * 		- Both sparse and standard attributes
 * 		- Labels or partial labels to determine bags for MIL
 *	- Advanced features
 * 		- Distributed computing using MPI
 * 		- Python bindings to bench utility
 * 		- Restart: if algorithm dies prematurely, can be restarted
 * 
 * @section Learn Learning with malibu
 * This section gives a brief overview for each learning algorithm available in 
 * the malibu workbench. Note that each classifier can be compiled to a single 
 * executable while the extensions must be used in conjunction with at least
 * a classifier. The extensions may also be layered over other classifiers as long
 * as a classifier forms the base of the resulting algorithm.
 * 
 * @subsection Classifier Base Classifiers
 * The following classifiers are available in malibu:
 * 	- C4.5 decision tree
 * 	- IND decision trees
 * 	- Willow decision tree
 * 	- Willow KMR decision stump
 * 	- Willow alternating decision tree
 * 	- Cover tree k-nearest neighbor
 * 	- LibSVM support vector machines
 * 
 * The last two classifiers, Cover tree and LibSVM, are distance-based classifiers; the
 * dataset should be transformed such that all nominal attributes are converted to 
 * binary ones, every value is normalized between 0 and 1 and their are no missing values.
 * 
 * @subsection Wrapper Learning Extensions
 * The following extensions are available in malibu:
 * 	- Calibration: make a classifier a probabilistic classifier using Isotonic and Sigmoid
 * 	- Cost sensitive: make any weighted learner cost-sensitive
 * 	- Importance weighted: make any weighted learner importance-weighted
 * 	- AdaBoost: make a weak classifier a strong classifier
 * 	- AdaBoost.C2M1: make a weak classifier a strong MIL classifier
 * 	- AdaBoost.MIL: make a weak classifier a strong MIL classifier
 * 	- Bagging: make a weak classifier a strong classifier
 * 	- Costing: make an unweighted classifier a weighted classifier
 * 	- Probing: make a weighte classifier a probabilistic regressor
 * 	- Quanting: make a weighte classifier a quantile regressor
 * 	- Output codes: make a binary classifier a multi-class classifier using one-versus-one and one-versus-all 
 *
 * Note that any classifier in malibu may be used as a multiple-instance learning classifier (MIL).
 * 
 * @section Util Utilities in malibu
 * The following utilities provide a broad range of services to facilitate the use of a 
 * malibu learning algorithm. Currently, only the bench utility provides python bindings
 * and a set of scripts demonstrating their use. 
 *
 * @subsection bench bench
 * This utility benchmarks an output file from a malibu learning algorithm. The benchmark
 * includes calculating a large set of metrics and a number of plots. The metrics can be
 * written in HTML, latex and console formats whereas the plots currently support the 
 * GNUPlot format. When multiple output files are passed to bench, each output file makes up
 * a row in a metric table or a line in the plot.
 * 
 * The python binding version of bench accompanies a set of python scripts illustraing its use. Moreover,
 * these scripts provides an alternative benchmark program that allows the user to view all metrics and plots
 * organized in a single file in one of the following formats:
 * 	- benchRST.py: restructured text
 * 	- benchHTM.py: HTML
 * 	- benchTEX.py: LaTeX
 * 	- benchPDF.py: PDF
 * 
 * The first three formats are archived in a TAR GZIP file while the PDF script produces a single PDF file.
 * 
 * @subsection deform deform
 * This utility transforms the attributes in a dataset, genearally for distance-based
 * learning algorithms such as support vector machines and k-nearest neighbor. It supports
 * the following operators:
 * 	- normalization
 * 	- nominal to binary
 * 	- missing value fill in
 * 
 * @subsection partition partition
 * This utility leverages the automated validation algorithms available in every malibu
 * learning algorithm to partition a dataset into training and testset pairs (or just 
 * testsets). It supports:
 * 	- Cross-validation
 * 	- Holdout
 * 	- Inverted Holdout
 * 	- Progressive Validation
 * 	- Inverted Progressive Validation
 * 	- Sampling with replacement
 * 	- Sampling without replacement
 * 
 * @subsection postfile postfile
 * This utility posts a malibu output file to a database.
 * 
 * @todo Consistent API coding conventions (follow boost.org)
 * @todo Documentation reviewer to help make code more understandable.
 * @todo Large-scale benchmark
 * @todo Python distutils install http://docs.python.org/distutils/index.html
 */

/** @file exegete.h
 * @brief Contains common functions for malibu programs.
 * 
 * These common functions do the following:
 * 	- Parse a set of implicit configuration files.
 * 	- Append a header to an argument map.
 * 	- Write an argument to an output stream.
 * 	- Write an error message to an error stream and argument map to output stream.
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** 
 * @brief Contains all non-utility classes and functions.
 * 
 * Defines a namespace for every class and function except utility functions
 * and templates.
 */
namespace exegete
{
};

/** Implicitly parses configuration files:
 *	# some-learner.cfg
 *	# cfg/some-learner.cfg
 *	# .exegete.cfg
 *	# cfg/.exegete.cfg
 *	# $HOME/.exegete.cfg
 * 
 * @param map an argument map.
 * @param name name of learning binary.
 * @return error message or NULL.
 */
inline const char* parse_implicit(::exegete::ArgumentMap& map, const char* name)
{
	const char* msg;
	std::vector<std::string> vec(5);
	vec[0] = base_name(name); vec[0] += ".cfg";
	vec[1] = vec[0]; vec[1] = join_file("cfg", vec[1].c_str());
	vec[2] = ".exegete.cfg";
	vec[3] = vec[2]; vec[3] = join_file("cfg", vec[3].c_str());
	vec[4] = vec[2]; vec[4] = join_file(home_path(), vec[4].c_str());
	for(unsigned int i=0;i<vec.size();++i)
		if( testfile(vec[i]) == 0 && (msg=map.parse(vec[i])) != 0 ) return msg;
	return 0;
}
/** Creates a header and adds it to an argument map. The header includes the following
 *  information:
 *	- Whether the algorithm was compiled in debug mode
 *	- Version of malibu
 *	- Authors
 *	- Websites
 *	- Additional usage information
 *
 * @param map an argument map.
 * @param name the program name.
 */
inline void header(::exegete::ArgumentMap& map, const std::string& name)
{
#ifndef NDEBUG
	map("Debug: "+name);
#else
	map(name);
#endif
	map("malibu Version "+version_string(_MALIBU_VERSION));
	map("UIC Bioinformatics");
	map("Chicago, Illinois");
	map("Robert Ezra Langlois and Hui Lu");
	map("Website1: http://proteomics.bioengr.uic.edu/malibu");
	map("Website2: http://code.google.com/p/exegete/");
	map("Use the -maplevel argument to view more options.");
	map("Levels: None,Standard,Additional,Advanced,Developer.");
}

/** Writes out an argument map and does special error handling.
 * 
 * @param map an argument map.
 * @param out an output stream.
 */
inline int writemap(const ::exegete::ArgumentMap& map, std::ostream& out)
{
	if( map.iswritable() )
	{
		const char* msg;
		if( (msg=map.write(out)) != 0 )
		{
			std::cerr << msg << std::endl << std::endl;
			MPI_EXIT ;
			return ErrorUtil::errcode();
		}
	}
	MPI_EXIT ;
	return 0;
}

/** Handles errors by:
 * 	- Writing an error message to stderr
 * 	- Writing an MPI errors
 * 	- Writes an argument map to the stdout
 * 	- Returns a meaningful error code
 *
 * @param map an argument map.
 * @param msg an error message.
 * @param msg2 another message.
 * @return one or greater error condition to OS.
 */
inline int error(const ::exegete::ArgumentMap& map, const char* msg, const char* msg2=0)
{
	MPI_ERROR(std::cerr);
	if( msg2 != 0 ) std::cerr << msg << " " << msg2 << std::endl << std::endl;
	else std::cerr << msg << std::endl << std::endl;
	writemap(map, std::cout);
	return ErrorUtil::errcode();
}
/** Handles errors by:
 * 	- Writing an error message to stderr
 * 	- Writing an MPI errors
 * 	- Writes an argument map to an output stream
 * 	- Returns a meaningful error code
 *
 * @param map an argument map.
 * @param msg an error message.
 * @param out an output stream.
 * @return one or greater error condition to OS.
 */
inline int error(const ::exegete::ArgumentMap& map, const char* msg, std::ostream& out)
{
	MPI_ERROR(std::cerr);
	std::cerr << msg << std::endl << std::endl;
	writemap(map, out);
	return ErrorUtil::errcode();
}



#endif


