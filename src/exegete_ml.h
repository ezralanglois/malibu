/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * exegete_ml.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_EXEGETE_ML_H
#define _EXEGETE_EXEGETE_ML_H
#include "debugutil.h"

/** @file exegete_ml.h
 * @brief Contains a set of macros and typedefs used to build a single learner.
 * 
 * This complext set of macros and typedefs are used to build a layered
 * learning algorithm using a minimal set of defined macros. For example,
 * wwillowboost is built using the following macros:
 * -D_WEIGHTED -D_WILLOWTREE -D_ADABOOST
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#ifdef _SPARSE
typedef std::pair<float,int> malibu_attribute_type;
#else
typedef float malibu_attribute_type;
#endif

/** Asserts at compile-time that the weight type is void. **/
#define TEST_UNWEIGHTED BOOST_STATIC_ASSERT( IsVoid<weight_type>::value);
/** Asserts at compile-time that the class type is integer. **/
#define TEST_REGRESS BOOST_STATIC_ASSERT( IS_CLASS_INT::value );
/** Creates a variable with prefix Classifier and some suffix. **/
#define CLASSIFIER1(X) Classifier##X
/** Creates a variable with prefix Classifier and some suffix. **/
#define CLASSIFIER(X) CLASSIFIER1(X)
/** Creates a variable with prefix Holder and some suffix. **/
#define HOLDER1(X) Holder##X
/** Creates a variable with prefix Holder and some suffix. **/
#define HOLDER(X) HOLDER1(X)
/** Creates a place holder for a given type X at holder index I. **/
#define DEFHOLDER(X,I) typedef TypeHolder1< ::exegete::X > HOLDER(I)
/** Creates a place holder for given types X and Y at holder index I. **/
#define DEFHOLDER2(X,Y,I) typedef TypeHolder2< ::exegete::X, Y > HOLDER(I)
/** Creates a place holder for given types X and Y at holder index I. **/
#define DEFHOLDER3(X,Y,I) typedef TypeHolder3< ::exegete::X, Y > HOLDER(I)
/** Creates a classifier by wrapping Holder X around classifier Y yielding classifier X. **/
#define ADAPTER(X,Y) typedef TypeAdapter< HOLDER(X), CLASSIFIER(Y) >::Result CLASSIFIER(X)
/** Defines a place holder for a class template with a single argument. **/
template<template<class> class T> struct TypeHolder1;
/** Defines a place holder for a class template with two arguments. **/
template<template<class,class> class T, class Y> struct TypeHolder2;
/** Defines a place holder for a class template with two arguments. **/
template<template<class,template<class> class> class T, template<class>class Y> struct TypeHolder3;
/** @brief Defines an adapter wrapping one class around another one. **/
template<class T, class U> struct TypeAdapter;
/** @brief Wraps one class around another.
 * 
 * Defines a template specialization for TypeHolder1, a single argument
 *  template class placeholder.
 */
template<template<class> class T, class U> 
struct TypeAdapter<TypeHolder1<T>,U>
{
	/** Defines T wrapped around U as Result. **/
	typedef T<U> Result;
};
/**  @brief Wraps one class around two classes.
 * 
 * Defines a template specialization for TypeHolder2, two argument
 *  template class placeholder.
 */
template<template<class,class> class T, class U1, class U2> 
struct TypeAdapter<TypeHolder2<T,U1>,U2>
{
	/** Defines T wrapped around U1 and U2 as Result. **/
	typedef T<U2,U1> Result;
};
/**  @brief Wraps one class around two classes.
 * 
 * Defines a template specialization for TypeHolder2, two argument
 *  template class placeholder.
 */
template<template<class,template<class>class> class T, class U1, template<class>class U2> 
struct TypeAdapter<TypeHolder3<T,U2>,U1>
{
	/** Defines T wrapped around U1 and U2 as Result. **/
	typedef T<U1,U2> Result;
};
#ifdef _WEIGHTED
/** Defines a double as a weighted type (if macro _WEIGHTED is defined). **/
typedef double impl_weight_type;
#else
/** Defines void as a weighted type (if macro _WEIGHTED is not defined). **/
typedef void impl_weight_type;
#endif

#ifdef _MULTICLASS
/** Defines a double as a weighted type (if macro _WEIGHTED is defined). **/
typedef impl_weight_type* weight_type;
#else
/** Defines void as a weighted type (if macro _WEIGHTED is not defined). **/
typedef impl_weight_type weight_type;
#endif

#ifdef _REGRESS
/** Defines a double as a class attribute type (if macro _REGRESS is defined). **/
typedef double class_attr_type;
#else
/** Defines an integer as a class attribute type (if macro _REGRESS is not defined). **/
typedef int class_attr_type;
#endif

/** Defines a test whether class attribute type is an integer. **/
typedef IsSameType<class_attr_type,int> IS_CLASS_INT;

//_SPARSE

/** Defines the compile-time increment _LEVEL as zero. **/
#define _LEVEL 0
#if defined(_KNN)
#include "CoverTree.h"
/** Defines the CoverTree k-Nearest Neighbor classifier as Classifier0 (when _KNN is defined). **/
typedef ::exegete::CoverTree<class_attr_type, weight_type> Classifier0;
#elif defined(_LIBSVM)
#include "LibSVM.h"
/** Defines the LibSVM support vector machines classifier as Classifier0 (when _LIBSVM is defined). **/
typedef ::exegete::LibSVM<class_attr_type> Classifier0;
#elif defined(_C45)
#include "C45.h"
TEST_REGRESS
TEST_UNWEIGHTED
/** Defines a C4.5 tree classifier as Classifier0 (when _C45 is defined). **/
typedef ::exegete::C45 Classifier0;
#elif defined(_IND)
#include "INDTree.h"
TEST_REGRESS
TEST_UNWEIGHTED
/** Defines an IND tree classifier as Classifier0 (when _IND is defined). **/
typedef ::exegete::INDTree Classifier0;
#elif defined(_MILSPHERE)
#include "mil_optimal_sphere.hpp"
TEST_REGRESS
typedef ::exegete::mil_optimal_sphere<float,int,weight_type> Classifier0;
#elif defined(_WILLOWTREE)
#include "WillowTree.h"
TEST_REGRESS
/** Defines a Willow tree classifier as Classifier0 (when _WILLOWTREE is defined). **/
typedef ::exegete::WillowTree<float,int,weight_type> Classifier0;
#elif defined(_ASPENTREE)
#include "aspen_tree.hpp"
TEST_REGRESS
/** Defines a Aspen tree classifier as Classifier0 (when _ASPENTREE is defined). **/
typedef ::exegete::aspen_tree<std::pair<float,int>, int, weight_type> Classifier0;
#elif defined(_WILLOWKMRSTUMP)
#include "WillowKMRStump.h"
TEST_REGRESS
/** Defines a Willow KMR stump classifier as Classifier0 (when _WILLOWKMRSTUMP is defined). **/
typedef ::exegete::WillowKMRStump<float,int,weight_type> Classifier0;
#elif defined(_PERCEPTRON)
#include "perceptron.hpp"
//malibu_attribute_type
typedef ::exegete::perceptron< std::pair<float,int>,int,weight_type > Classifier0;
#elif defined(_WILLOWADTREE)
#include "WillowADTree.h"
TEST_REGRESS
TEST_UNWEIGHTED
/** Defines a Willow ADTree classifier as Classifier0 (when _WILLOWADTREE is defined). **/
typedef ::exegete::WillowADTree<float,int> Classifier0;
#else
/** Defines void as a classifier causing a compile error (only if none of the above macros are defined). **/
typedef void Classifier0;
#endif



#if defined(_ADABOOSTC2MIL)
#include "adaboost_c2mil.hpp"
/** Defines a AdaBoostC2MIL wrapped around a learner (when _ADABOOSTC2MIL is defined). **/
DEFHOLDER(adaboost_c2mil, _ADABOOSTC2MIL );
#if _LEVEL < _ADABOOSTC2MIL
#undef _LEVEL
/** Defines _LEVEL as _ADABOOSTC2M1. **/
#define _LEVEL _ADABOOSTC2MIL
#endif
#endif

#if defined(_ADABOOSTC2M1)
#include "AdaBoost.C2M1.h"
/** Defines a AdaBoostC2M1 wrapped around a learner (when _ADABOOSTC2M1 is defined). **/
DEFHOLDER(AdaBoostC2M1, _ADABOOSTC2M1 );
#if _LEVEL < _ADABOOSTC2M1
#undef _LEVEL
/** Defines _LEVEL as _ADABOOSTC2M1. **/
#define _LEVEL _ADABOOSTC2M1
#endif
#endif

#if defined(_LOGODDBOOST)
#include "any_boost.hpp"
#include "mil_log_odds_cost_function.hpp"
/** Defines a AdaBoost wrapped around a learner (when _ADABOOST is defined). **/
DEFHOLDER3(any_boost, ::exegete::mil_log_odds_cost_function, _LOGODDBOOST );
#if _LEVEL < _LOGODDBOOST
#undef _LEVEL
/** Defines _LEVEL as _ADABOOST. **/
#define _LEVEL _LOGODDBOOST
#endif
#endif

#if defined(_TAGGING_SEARN)
#include "searn.hpp"
#include "tagging_policy.hpp"
/** Defines a tagging searn wrapped around a learner (when _TAGGING_SEARN is defined). **/
DEFHOLDER3(searn, ::exegete::tagging_policy, _TAGGING_SEARN );
#if _LEVEL < _TAGGING_SEARN
#undef _LEVEL
/** Defines _LEVEL as _TAGGING_SEARN. **/
#define _LEVEL _TAGGING_SEARN
#endif
#endif

#if defined(_NORBOOST)
#include "any_boost.hpp"
#include "mil_nor_cost_function.hpp"
/** Defines a AdaBoost wrapped around a learner (when _ADABOOST is defined). **/
DEFHOLDER3(any_boost, ::exegete::mil_nor_cost_function, _NORBOOST );
#if _LEVEL < _NORBOOST
#undef _LEVEL
/** Defines _LEVEL as _ADABOOST. **/
#define _LEVEL _NORBOOST
#endif
#endif

#if defined(_ADABOOST)
#include "AdaBoost.h"
/** Defines a AdaBoost wrapped around a learner (when _ADABOOST is defined). **/
DEFHOLDER(AdaBoost, _ADABOOST );
#if _LEVEL < _ADABOOST
#undef _LEVEL
/** Defines _LEVEL as _ADABOOST. **/
#define _LEVEL _ADABOOST
#endif
#endif

/*#if defined(_ADABOOSTMIL)
#include "AdaBoostMIL.h"
 Defines a AdaBoostMIL wrapped around a learner (when _ADABOOSTMIL is defined). 
DEFHOLDER(AdaBoostMIL, _ADABOOSTMIL );
#if _LEVEL < _ADABOOSTMIL
#undef _LEVEL
 Defines _LEVEL as _ADABOOSTMIL. 
#define _LEVEL _ADABOOSTMIL
#endif
#endif*/

#if defined(_BAGGING)
#include "Bagging.h"
/** Defines a Bagging wrapped around a learner (when _BAGGING is defined). **/
DEFHOLDER(Bagging, _BAGGING );
#if _LEVEL < _BAGGING
#undef _LEVEL
/** Defines _LEVEL as _BAGGING. **/
#define _LEVEL _BAGGING
#endif
#endif

#if defined(_COSTING)
#include "Costing.h"
/** Defines a Costing wrapped around a learner (when _COSTING is defined). **/
DEFHOLDER(Costing, _COSTING );
#if _LEVEL < _COSTING
#undef _LEVEL
/** Defines _LEVEL as _COSTING. **/
#define _LEVEL _COSTING
#endif
#endif

#if defined(_QUANTING)
#include "Quanting.h"
/** Defines a Quanting wrapped around a learner (when _QUANTING is defined). **/
DEFHOLDER(Quanting, _QUANTING );
#if _LEVEL < _QUANTING
#undef _LEVEL
/** Defines _LEVEL as _QUANTING. **/
#define _LEVEL _QUANTING
#endif
#endif

#if defined(_MSEPROBING)
#include "Probing.h"
/** Defines a Probing with MSELoss wrapped around a learner (when _MSEPROBING is defined). **/
DEFHOLDER2(Probing, ::exegete::MSELoss, _MSEPROBING );
#if _LEVEL < _MSEPROBING
#undef _LEVEL
/** Defines _LEVEL as _MSEPROBING. **/
#define _LEVEL _MSEPROBING
#endif
#endif

#if defined(_CXEPROBING)
#include "Probing.h"
/** Defines a Probing with CXELoss wrapped around a learner (when _CXEPROBING is defined). **/
DEFHOLDER2(Probing, ::exegete::CXELoss, _CXEPROBING );
#if _LEVEL < _CXEPROBING
#undef _LEVEL
/** Defines _LEVEL as _CXEPROBING. **/
#define _LEVEL _CXEPROBING
#endif
#endif

#if defined(_IMPORTANCEWEIGHTED)
#include "ImportanceWeighted.h"
/** Defines a ImportanceWeighted wrapped around a learner (when _IMPORTANCEWEIGHTED is defined). **/
DEFHOLDER(ImportanceWeighted, _IMPORTANCEWEIGHTED );
#if _LEVEL < _IMPORTANCEWEIGHTED
#undef _LEVEL
/** Defines _LEVEL as _IMPORTANCEWEIGHTED. **/
#define _LEVEL _IMPORTANCEWEIGHTED
#endif
#endif

#if defined(_COSTSENSITIVE)
#include "CostSensitive.h"
/** Defines a CostSensitive wrapped around a learner (when _COSTSENSITIVE is defined). **/
DEFHOLDER(CostSensitive, _COSTSENSITIVE );
#if _LEVEL < _COSTSENSITIVE
#undef _LEVEL
/** Defines _LEVEL as _COSTSENSITIVE. **/
#define _LEVEL _COSTSENSITIVE
#endif
#endif

#if defined(_CALIBRATION)
#include "Calibration.h"
/** Defines a Calibration wrapped around a learner (when _CALIBRATION is defined). **/
DEFHOLDER2(Calibration, void, _CALIBRATION );
#if _LEVEL < _CALIBRATION
#undef _LEVEL
/** Defines _LEVEL as _CALIBRATION. **/
#define _LEVEL _CALIBRATION
#endif
#endif

#if defined(_WCALIBRATION)
#include "Calibration.h"
/** Defines a weighted Calibration wrapped around a learner (when _WCALIBRATION is defined). **/
DEFHOLDER2(Calibration, double, _WCALIBRATION );
#if _LEVEL < _WCALIBRATION
#undef _LEVEL
/** Defines _LEVEL as _WCALIBRATION. **/
#define _LEVEL _WCALIBRATION
#endif
#endif

#if defined(_OUTPUTCODES)
#include "output_codes.hpp"
/** Defines output codes wrapped around a learner (when _OUTPUTCODES is defined). **/
DEFHOLDER(output_codes, _OUTPUTCODES );
#if _LEVEL < _OUTPUTCODES
#undef _LEVEL
/** Defines _LEVEL as _OUTPUTCODES. **/
#define _LEVEL _OUTPUTCODES
#endif
#endif

#if defined(_FILTERTREE)
#include "filter_tree.hpp"
/** Defines output codes wrapped around a learner (when _FILTERTREE is defined). **/
DEFHOLDER(filter_tree, _FILTERTREE );
#if _LEVEL < _FILTERTREE
#undef _LEVEL
/** Defines _LEVEL as _FILTERTREE. **/
#define _LEVEL _FILTERTREE
#endif
#endif



#if _LEVEL > 0
/** Places a learning wrapper around a base classifier. **/
ADAPTER(1,0);
#endif
#if _LEVEL > 1
/** Places a learning wrapper around another wrapper. **/
ADAPTER(2,1);
#endif
#if _LEVEL > 2
/** Places a learning wrapper around another wrapper. **/
ADAPTER(3,2);
#endif
#if _LEVEL > 3
/** Places a learning wrapper around another wrapper. **/
ADAPTER(4,3);
#endif
#if _LEVEL > 4
/** Places a learning wrapper around another wrapper. **/
ADAPTER(5,4);
#endif

/** Defines a fully wrapped learner. **/
typedef CLASSIFIER(_LEVEL) Learner;
#endif


