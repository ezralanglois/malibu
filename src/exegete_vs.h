/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */


/*
 * exegete_vs.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

/** @file exegete_vs.h
 * @brief Contains a set of macros to compile algorithms in Visual Studio.
 * 
 * This header file contains definitions to assist compiling algorithms in
 * Visual Studio where the user uncomments only the definitions for the
 * algorithm of interest.
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#ifdef _MSC_VER
//#define _SPARSE
//#define _SEQ

//#define _STATDOMAIN
//#define _TRANSFORM
//#define _PARTITION
//#define _ORGCOUNT
//#define _EVALF 0
//#define _ENCODE
//#define _BLASTEVAL
//#define _ANNOTATIONTABLE
//#define _EXTRACTSEQ
//#define _FILTERSEQ
//#define _STAT_DOMAIN
//#define _STAT_PFAMDOMAIN
//#define _EXTRACT_DOMAIN

//#define _SINGLE_LEARNER
//#define _TRAIN_LEARNER
//#define _TEST_LEARNER
//#define _NFOLD_LEARNER


#if defined(_SINGLE_LEARNER) || defined(_TRAIN_LEARNER) || defined(_TEST_LEARNER) || defined(_NFOLD_LEARNER )
//#define _WEIGHTED
//#define _KNN
//#define _LIBSVM
//#define _C45
//#define _IND
//#define _WILLOWTREE
//#define _WILLOWKMRSTUMP
//#define _WILLOWADTREE
//#define _WEIGHTEDTREE
//#define _ADABOOST 1
//#define _BAGGING 1
//#define _COSTING 1
//#define _COSTSENSITIVE 2
//#define _IMPORTANCEWEIGHTED 2
//#define _ADABOOSTC2M1 1
#endif

#endif


//
//#ifdef _MSC_VER
////#define _SINGLECLASSIFIER
////#define _EVALUATE
////#define _BLAST_EVAL
////#define _XENCODE
////#define _ENCODE
////#define _ANNOTATE
////#define _DATASET
////#define _INTERACTION
////#define _PATHCLASS
////#define _EVALUATION
////#define _TRANSFORM
////#define _SPARSE
////#define _SEQUENCE
//#endif



