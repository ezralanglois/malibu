/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Calibration.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_CALIBRATION_H
#define _EXEGETE_CALIBRATION_H
#include "ArgumentMap.h"
#include "ExampleSet.h"
#include "ModelValidation.h"
#include "ModelSelection.h"
#include "prediction_set.hpp"
#include "calibration/isotonic_calibration.hpp"
#include "calibration/sigmoid_calibration.hpp"
/** Defines the version of calibration algorithm. 
 * @todo add to group
 *
 */
#define _CALIBRATION_VERSION 1.0

/** @file Calibration.h
 * This file contains the Calibration class template.
 *
 * @ingroup ExegeteLearning
 * @ingroup ExegeteAdapter
 * @ingroup ExegeteCalibration
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteLearning Learners
 * 
 *  This group holds all the learning files.
 */

/** @defgroup ExegeteAdapter Learning Adapters
 * 
 *  This group holds all the learning adapter files.
 */

/** @defgroup ExegeteCalibration Learning Calibration
 * 
 *  This group holds all the learning calibration files.
 */

namespace exegete
{
	namespace detail
	{
		/** @brief Compile-time selection whether to copy dataset
		 * 
		 * This class selects a dataset and copies a weighted dataset if necessary.
		 */
		template<class T, class W>
		class weighted_leaner_adapter
		{
			typedef typename T::dataset_type parentset_type;
			typedef typename parentset_type::class_type class_type;
			typedef typename parentset_type::attribute_type attribute_type;
		public:
			/** Defines a dataset type. **/
			typedef ExampleSet<attribute_type, class_type, W> 	dataset_type;
			/** Defines a weight type. **/
			typedef typename T::weight_type	 					weight_type;
			/** Defines a prediction  vector. **/
			typedef typename T::prediction_vector		 		prediction_vector;
			/** Trains a learning algorithm on a dataset.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a dataset.
			 */
			inline static const char* train(T& learner, dataset_type& d)
			{
				parentset_type dataset(d); dataset.assign(d);
				return learner.train(dataset);
			}
			/** Trains a learning algorithm on a dataset with a prediction vector.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a dataset.
			 * @param pred a prediction vector.
			 */
			inline static const char* train(T& learner, dataset_type& d, prediction_vector pred)
			{
				parentset_type dataset(d); dataset.assign(d);
				return learner.train(dataset, pred);
			}
		};
		/** @brief Compile-time selection whether to copy dataset
		 * 
		 * This class specialization uses the current dataset.
		 */
		template<class T>
		class weighted_leaner_adapter<T,void>
		{
		public:
			/** Defines a dataset type. **/
			typedef typename T::dataset_type 		dataset_type;
			/** Defines a weight type. **/
			typedef typename T::weight_type	 		weight_type;
			/** Defines a prediction  vector. **/
			typedef typename T::prediction_vector	prediction_vector;
			/** Trains a learning algorithm on a dataset.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a dataset.
			 */
			inline static const char* train(T& learner, dataset_type& dataset)
			{
				return learner.train(dataset);
			}
			/** Trains a learning algorithm on a dataset with a prediction vector.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a dataset.
			 * @param pred a prediction vector.
			 */
			inline static const char* train(T& learner, dataset_type& dataset, prediction_vector pred)
			{
				return learner.train(dataset, pred);
			}
		};
	};

	/** @brief Calibrates confidence-rated predictions as probabilistic.
	 * 
	 * This class template defines an Calibration probabilistic transform.
	 *
	 * @todo consider weighted error
	 * @todo consider MIL
	 * @todo cost sensitive threshold calibration
	 * @todo experimental, needs more testing
	 */
	template<class T, class W>
	class Calibration : public T
	{
		typedef T learner_type;
		typedef detail::weighted_leaner_adapter<T,W> adapter;
	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines a dataset type. **/
		typedef typename adapter::dataset_type								dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer				const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type						descriptor_type;
		/** Defines a float type. **/
		typedef typename learner_type::float_type							float_type;
		/** Defines a tunable argument iterator. **/
		typedef typename learner_type::argument_iterator					argument_iterator;
		/** Defines a weight type. **/
		typedef typename adapter::weight_type								weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef typename learner_type::prediction_vector		 			prediction_vector;
		/** Defines argument level. **/
		enum{ARGUMENT=T::ARGUMENT+1};
	private:
		typedef typename dataset_type::class_type 							class_type;
		typedef TuneParameterTree											argument_type;
		typedef typename argument_type::parameter_type						range_type;
		typedef typename dataset_type::iterator								example_iterator;
		typedef prediction_set<float_type, class_type>						prediction_measure;
		typedef ModelValidation<learner_type,prediction_measure,dataset_type>	validation_type;
		//typedef ConfusionMetric<float_type> threshold_type;
		typedef std::pair<float_type, float_type>							prediction_pair;
		typedef std::vector< prediction_pair >								prediction_map;
		typedef prediction< float_type, class_type> 						pred_value;
		typedef std::vector< pred_value > 									prediction_array;
		
	public:
		/** Constructs a calibrated classifier.
		 */
		Calibration() : A(0.0f), B(0.0f), typeInt(0), currthresh(0.5f)
		{
			learner_type::add_test( learner_type::BINARY );
			learner_type::add_test( learner_type::NOMIL );
		}
		/** Destructs a calibrated classifier.
		 */
		~Calibration()
		{
		}

	public:
		/** Appends arguments to a map.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			arginit(map, NAME_VERSION(Calibration));
			arginit(map, typeInt, "calibration", "type of calibration>None:0;Platt:1;Isotonic:2");
			validation.init(map, validint(t,3), "cal");
			//thresholdMetric.init(map, isvalid(t), "cal");
		}
		
	private:
		static bool isvalid(int t)
		{
			return t == 0 || t == ARGUMENT;
		}
		static int validint(int t, int s)
		{
			if( s == 0 ) return ( (t == 0 || t == ARGUMENT)?0:-1);
			return ( (t == 0 || t == ARGUMENT)?1:-1) * s;
		}

	public:
		/** Makes a deep copy of the calibration classifier.
		 *
		 * @param cal a source calibration model.
		 */
		Calibration(const Calibration& cal) : learner_type(cal), validation(cal.validation), predmap(cal.predmap), A(cal.A), B(cal.B), typeInt(cal.typeInt), currthresh(cal.currthresh)
		{
		}
		/** Makes a deep copy of the calibration model.
		 *
		 * @param cal a source calibration model.
		 * @return a reference to this object.
		 */
		Calibration& operator=(const Calibration& cal)
		{
			learner_type::operator=(cal);
			argument_copy(cal);
			predmap = cal.predmap;
			A = cal.A;
			B = cal.B;
			currthresh = cal.currthresh;
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return currthresh;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			if( !predmap.empty() ) return isotonic_predict(learner_type::predict(pred), predmap);
			if( A == 0.0f ) return learner_type::predict(pred);
			return sigmoid_predict(learner_type::predict(pred), A, B);
		}
		/** Builds a model for the calibrated classifier.
		 *
		 * @param learnset the training set.
		 * @param pred an array of predictions.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, prediction_vector& pred)
		{
			const char* msg;
			if( (msg=adapter::train(*this, learnset, pred)) != 0 ) return msg;
			if( skipPostProcess() ) return 0;
			
			prediction_array predictions(pred.size());
			for(unsigned int i=0;i<pred.size();++i)
			{
				predictions[i].p(pred[i].p());
				predictions[i].y(pred[i].y());
				predictions[i].w(learnset[i].weight(1.0f));
			}
			calibrate(predictions);
			return 0;
		}
		/** Builds a model for the calibrated classifier.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			const char* msg;
			if( (msg=adapter::train(*this, learnset)) != 0 ) return msg;
			if( skipPostProcess() ) return 0;
			
			prediction_measure pred;
			learner_type learner;
			learner.argument_copy(*this);
			if( (msg=validation.validate(learner, pred, learnset)) != 0 ) return msg;
			prediction_array predictions(pred.size());
			std::copy(pred.begin(), pred.end(), predictions.begin());
			calibrate(predictions);
			return 0;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Calibration
		 */
		static std::string name()
		{
			return "Calibration";
		}
		/** Gets the version of this wrapper.
		 * 
		 * @return vesion.
		 */
		static int version()
		{
			return _CALIBRATION_VERSION;
		}
		/** Copies the arguments of the calibrated classifier.
		 *
		 * @param cal a source calibrated model.
		 */
		void argument_copy(const Calibration& cal)
		{
			typeInt = cal.typeInt;
			validation = cal.validation;
		}
		/** Makes a shallow copy of an calibrated classifier.
		 *
		 * @param cal a calibrated classifier.
		 */
		void shallow_copy(Calibration& cal)
		{
			learner_type::shallow_copy(cal);
			argument_copy(cal);
			predmap = cal.predmap;
			A = cal.A;
			B = cal.B;
			currthresh = cal.currthresh;
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("Calibration ")+learner_type::class_name();
		}
		/** Get the expected name of the program.
		 * 
		 * @return cal
		 */
		static std::string progname()
		{
			return "cal";
		}
		/** Accept a vistor class (part of the vistor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			learner_type::accept(visitor);
		}
		/** Test if classifier is calibrated.
		 * 
		 * @return true if calibration model not empty.
		 */
		bool is_calibrated()const
		{
			return A != 0.0f || !predmap.empty();
		}
		/** Set a restart flag.
		 * 
		 * @param r a restart flag.
		 * @return error message or NULL.
		 */
		const char* set_restart(int r)
		{
			const char* msg;
			validation.restart(r);
			if( (msg=validation.check_restart("cal")) != 0 ) return msg;
			return T::set_restart(r);
		}
		
	private:
		void calibrate(prediction_array& predictions)
		{
			if( typeInt == 1 ) sigmoid_calibration(predictions.begin(), predictions.end(), 0, A, B);
			else isotonic_calibration(predictions.begin(), predictions.end(), 0, predmap);
			//if( !thresholdMetric ) 
			{
				currthresh = 0.5f;
				return;
			}
			//if( typeInt > 2 ) isotonic_prediction(predictions.begin(), predictions.end(), predmap);
			//else if( A != 0.0f ) sigmoid_prediction(predictions.begin(), predictions.end(), A, B);
			//currthresh = threshold_calibration(predictions.begin(), predictions.end(), thresholdMetric);
		}
		bool skipPostProcess()
		{
			if( learner_type::isSelectingParameters() ) return true;
			if( validation.type() == validation_type::NO ) return true;
			if( typeInt == 0 ) return true;
			return false;
		}
		static learner_type& parent(learner_type& p){return p;}
		static const learner_type& parent(const learner_type& p){return p;}
		
	protected:
		/** Reads a calibrated model from the input stream.
		 *
		 * @param in an input stream.
		 * @param pc a calibrated model
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, Calibration& pc)
		{
			unsigned int n;
			float thresh;
			if( in.peek() == '$')
			{
				in.get(); in.get();
				in >> pc.A;
				in.get();
				in >> pc.B;
				in.get();
				in >> thresh;
				pc.currthresh = thresh;
				if( in.peek() != '\n' ) 
				{
					pc.errormsg(in, "Calibration model missing end of line - 1 - " << int(in.peek()));
					return in;
				}
				else in.get();
				if( pc.A == 0.0f )
				{
					in >> n;
					pc.predmap.resize(n);
					for(unsigned int i=0;i<n;++i)
						in >> pc.predmap[i].first >> pc.predmap[i].second;
					if( in.peek() != '\n' ) 
					{
						pc.errormsg(in, "Calibration model missing end of line - 2 - " << int(in.peek()));
						return in;
					}
					else in.get();
				}
			}
			in >> parent(pc);
			return in;
		}
		/** Writes a calibrated model to the output stream.
		 *
		 * @param out an output stream.
		 * @param pc a calibrated model
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const Calibration& pc)
		{
			if( pc.is_calibrated() )
			{
				out << "$\t" << pc.A << "\t" << pc.B << "\t" << pc.currthresh << "\n";
				if( pc.A == 0.0f )
				{
					out << pc.predmap.size();
					for(unsigned int i=0;i<pc.predmap.size();++i)
						out << " " << pc.predmap[i].first << " " << pc.predmap[i].second;
					out << "\n";
				}
			}
			out << parent(pc);
			return out;
		}

	private:
		validation_type validation;
		//threshold_type thresholdMetric;
		prediction_map predmap;
		float_type A, B;
		int typeInt;
		float currthresh;
	};
	
	/** @brief Selects the parent classifier as a base learner
	 * 
	 * This class selects the parent classifier as a base learner.
	 */
	template<class T, class U>
	struct base_learner< Calibration<T, U> >
	{
		/** Defines parent as base learner. **/
		typedef typename base_learner<T>::result result;
	};
};


/** @brief Type utility helper specialized for Calibration
 * 
 * This class defines a type utility helper specialized for Calibration.
 */
template<class T, class U>
struct TypeTrait < ::exegete::Calibration<T, U> >
{
	/** Defines a value type. **/
	typedef ::exegete::Calibration<T,U> value_type;
};



#endif


