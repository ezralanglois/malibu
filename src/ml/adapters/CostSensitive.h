/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * CostSensitive.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_COSTSENSITIVE_H
#define _EXEGETE_COSTSENSITIVE_H

/** Defines the version of cost-sensitive algorithm. 
 * @todo add to group
 *
 */
#define _COSTSENSITIVE_VERSION 1.0

/** @file CostSensitive.h
 * @brief Wraps a cost-sensitive algorithm
 * 
 * This file contains the CostSensitive class template.
 *
 * @ingroup ExegeteLearning
 * @ingroup ExegeteAdapter
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Wraps a cost-sensitive algorithm
	 * 
	 * This class template defines a cost-sensitive classifier.
	 */
	template<class T>
	class CostSensitive : public T
	{
		typedef T learner_type;
	public:
		/** Defines a parent type. **/
		typedef typename T::parent_type parent_type;
		/** Defines a dataset type. **/
		typedef typename learner_type::dataset_type				dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a double as a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef typename learner_type::argument_iterator		argument_iterator;
		/** Defines a prediction vector. **/
		typedef typename learner_type::prediction_vector		prediction_vector;
		/** Defines argument level. **/
		enum{ARGUMENT=T::ARGUMENT+1};
	private:
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename dataset_type::iterator					example_iterator;

	public:
		/** Constructs a default cost-sensitive classifier.
		 */
		CostSensitive() : normBool(false), poswFlt(1.0f),
		 				  poswSel(poswFlt, "Positive Weight", range_type(0.5f, 1.0f, 0.1f, '+'))
		{
			poswSel.add( &learner_type::argument() );
		}
		/** Destructs an cost-sensitive classifier.
		 */
		~CostSensitive()
		{
		}
		
	public:
		/** Appends arguments to a map.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(CostSensitive));
				if( t>=0 )
				arginit(map, poswSel,  "wpos", "weight for positive examples");
				else
				arginit(map, poswFlt,  "wpos", "weight for positive examples");
				//arginit(map, normBool, "norm", "normalize weights?");
			}
		}

	public:
		/** Makes a deep copy of the cost-sensitive classifier.
		 *
		 * @param cs a source cost-sensitive model.
		 */
		CostSensitive(const CostSensitive& cs) : learner_type(cs), normBool(cs.normBool), poswFlt(cs.poswFlt), poswSel(poswFlt,cs.poswSel)
		{
			poswSel.add( &learner_type::argument() );
		}
		/** Makes a deep copy of the cost-sensitive classifier.
		 *
		 * @param cs a source cost-sensitive model.
		 * @return a reference to this object.
		 */
		CostSensitive& operator=(const CostSensitive& cs)
		{
			learner_type::operator=(cs);
			argument_copy(cs);
			return *this;
		}
		/** Makes a shallow copy of the cost-sensitive classifier.
		 *
		 * @param cs a source cost-sensitive model.
		 * @return a reference to this object.
		 */
		void shallow_copy(CostSensitive& cs)
		{
			T::shallow_copy(cs);
			argument_copy(cs);
		}
		/** Copies the arguments of the cost-sensitive classifier.
		 *
		 * @param cs a source cost-sensitive model.
		 */
		void argument_copy(const CostSensitive& cs)
		{
			poswFlt = cs.poswFlt;
			poswSel = cs.poswSel;
			normBool = cs.normBool;
		}
		
	public:
		/** Builds a model for the cost-sensitive classifier.
		 *
		 * @param learnset the training set.
		 * @param pred an array of predictions.
		 */
		const char* train(dataset_type& learnset, prediction_vector& pred)
		{
			float_type tot = updtwgts(learnset.begin(), learnset.end(), poswFlt);
			if( normBool ) normwgts( learnset.begin(), learnset.end(), tot );
			return learner_type::train(learnset, pred);
		}
		/** Builds a model for the cost-sensitive classifier.
		 *
		 * @param learnset the training set.
		 */
		const char* train(dataset_type& learnset)
		{
			float_type tot = updtwgts(learnset.begin(), learnset.end(), poswFlt);
			if( normBool ) normwgts( learnset.begin(), learnset.end(), tot );
			return learner_type::train(learnset);
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("CostSensitive ")+learner_type::class_name();
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return CostSensitive.
		 */
		static std::string name()
		{
			return "CostSensitive";
		}
		/** Get the version of this wrapper.
		 * 
		 * @return version.
		 */
		static int version()
		{
			return _COSTSENSITIVE_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return cs
		 */
		static std::string prefix()
		{
			return "cs";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return poswSel;
		}
		/** Get the expected name of the program.
		 * 
		 * @return cs
		 */
		static std::string progname()
		{
			return "cs";
		}
		/** Accept a vistor class (part of the vistor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			learner_type::accept(visitor);
		}

	private:
		static float_type updtwgts(example_iterator beg, example_iterator end, float_type w)
		{
			float_type tot=0.0f;
			float_type negw = 1.0;
			float_type posw = w;
			if( posw > 1.0 )
			{
				negw = 1/posw;
				posw = 1.0f;
			}
			for(;beg != end;++beg)
			{
				beg->w( beg->y() > 0 ? posw : negw );
				tot += beg->w();
			}
			return tot;
		}
		static void normwgts(example_iterator beg, example_iterator end, float_type w)
		{
			ASSERT(w != 0.0f);
			w = 1.0f/w;
			for(;beg != end;++beg) beg->w_update(w);
		}

	private:
		bool normBool;
		float_type poswFlt;
		argument_type poswSel;
	};
};


/** @brief Type utility helper specialized for CostSensitive
 * 
 * This class defines a type utility helper specialized for CostSensitive.
 */
template<class T>
struct TypeTrait < ::exegete::CostSensitive<T> >
{
	/** Defines a value type. **/
	typedef ::exegete::CostSensitive<T> value_type;
};


#endif


