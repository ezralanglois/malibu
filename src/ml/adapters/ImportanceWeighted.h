/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ImportanceWeighted.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_IMPORTANCEWEIGHTED_H
#define _EXEGETE_IMPORTANCEWEIGHTED_H

/** Defines the version of importanced-weighted algorithm. 
 * @todo add to group
 *
 */
#define _IMPORTANCEWEIGHTED_VERSION 1.0

/** @file ImportanceWeighted.h
 * 
 * @brief Wraps an importance-weighted algorithm
 * 
 * This file contains the ImportanceWeighted class template.
 *
 * @ingroup ExegeteLearning
 * @ingroup ExegeteAdapter
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Wraps an importance-weighted algorithm
	 * 
	 * This class template defines an importance-weighted classifier.
	 */
	template<class T>
	class ImportanceWeighted : public T
	{
		typedef T learner_type;
	public:
		/** Defines a parent type. **/
		typedef typename T::parent_type parent_type;
		/** Defines a dataset as a learnset type. **/
		typedef typename learner_type::dataset_type				dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a double as a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef typename learner_type::argument_iterator		argument_iterator;
		/** Defines a prediction vector. **/
		typedef typename learner_type::prediction_vector		prediction_vector;
		/** Defines argument level. **/
		enum{ARGUMENT=T::ARGUMENT+1};
	private:
		typedef typename dataset_type::iterator					example_iterator;
		typedef typename dataset_type::class_type				class_type;
		typedef typename dataset_type::attribute_type			attribute_type;

	public:
		/** Constructs a default importance-weighted classifier.
		 */
		ImportanceWeighted() : normBool(false)
		{
		}
		/** Destructs an importance-weighted classifier.
		 */
		~ImportanceWeighted()
		{
		}
		
	public:
		/** Appends arguments to a map.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				//arginit(map, NAME_VERSION(ImportanceWeighted));
				//arginit(map, normBool, "norm", "normalize weights?");
			}
		}
		
	public:
		/** Makes a deep copy of the importance-weighted classifier.
		 *
		 * @param iw a source importance-weighted model.
		 */
		ImportanceWeighted(const ImportanceWeighted& iw) : learner_type(iw), normBool(iw.normBool)
		{
		}
		/** Makes a deep copy of the importance-weighted classifier.
		 *
		 * @param iw a source importance-weighted model.
		 * @return a reference to this object.
		 */
		ImportanceWeighted& operator=(const ImportanceWeighted& iw)
		{
			learner_type::operator=(iw);
			argument_copy(iw);
			return *this;
		}
		/** Makes a shallow copy of an importance-weighted classifier.
		 * 
		 * @param iw a source importance-weighted model.
		 */
		void shallow_copy(ImportanceWeighted& iw)
		{
			T::shallow_copy(iw);
			argument_copy(iw);
		}
		/** Copy arguments in an importance-weighted wrapper.
		 * 
		 * @param iw a source importance-weighted model.
		 */
		void argument_copy(const ImportanceWeighted& iw)
		{
			normBool = iw.normBool;
		}
		/** Setups a concrete example set.
		 *
		 * @param dataset a concrete dataset modify.
		 */
		void setup(ConcreteExampleSet<attribute_type,class_type>& dataset)
		{
			dataset.setupWeights();
			T::setup(dataset);
		}
		
	public:
		/** Builds a model for the importance-weighted classifier.
		 *
		 * @param learnset the training set.
		 * @param pred a destination array of predictions.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, prediction_vector& pred)
		{		
			float_type tot = updtwgts(learnset.begin(), learnset.end());
			if( normBool ) normwgts( learnset.begin(), learnset.end(), tot );
			return learner_type::train(learnset, pred);
		}
		/** Builds a model for the importance-weighted classifier.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{			
			float_type tot = updtwgts(learnset.begin(), learnset.end());
			if( normBool ) normwgts( learnset.begin(), learnset.end(), tot );
			return learner_type::train(learnset);
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("ImportanceWeighted ")+learner_type::class_name();
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return ImportanceWeighted
		 */
		static std::string name()
		{
			return "ImportanceWeighted";
		}
		/** Get the version of this wrapper.
		 * 
		 * @return version.
		 */
		static int version()
		{
			return _IMPORTANCEWEIGHTED_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return iw
		 */
		static std::string prefix()
		{
			return "iw";
		}
		/** Get the expected name of the program.
		 * 
		 * @return iw
		 */
		static std::string progname()
		{
			return "iw";
		}
		/** Accept a vistor class (part of the vistor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			learner_type::accept(visitor);
		}

	private:
		template<class A, class Y>
		static float_type updtwgts(Example<A,Y,void>* beg, Example<A,Y,void>* end)
		{
			return 0.0f;
		}
		template<class A, class Y>
		static void normwgts(Example<A,Y,void>* beg, Example<A,Y,void>* end, float_type w)
		{
		}
		template<class A, class Y, class W>
		static float_type updtwgts(Example<A,Y,W>* beg, Example<A,Y,W>* end)
		{
			float_type tot=0.0f;
			for(;beg != end;++beg)
			{
				beg->w( beg->attr_weight() );
				tot += beg->w();
			}
			return tot;
		}
		template<class A, class Y, class W>
		static void normwgts(Example<A,Y,W>* beg, Example<A,Y,W>* end, float_type w)
		{
			ASSERT(w != 0.0f);
			w = 1.0f/w;
			for(;beg != end;++beg) beg->w_update(w);
		}

	private:
		bool normBool;
	};
};

/** @brief Type utility helper specialized for ImportanceWeighted
 * 
 * This class defines a type utility helper specialized for ImportanceWeighted.
 */
template<class T>
struct TypeTrait < ::exegete::ImportanceWeighted<T> >
{
	/** Defines a value type. **/
	typedef ::exegete::ImportanceWeighted<T> value_type;
};


#endif


