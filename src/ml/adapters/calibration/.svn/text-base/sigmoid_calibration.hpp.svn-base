/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * sigmoid_calibration.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_SIGMOID_CALIBRATION_HPP
#define _EXEGETE_SIGMOID_CALIBRATION_HPP
#include <cmath>

/** @file sigmoid_calibration.hpp
 * 
 * @brief Contains functions for sigmoid calibration
 * 
 * This file contains a set of global functions that 
 * define the sigmoid calibration algorithm.
 *
 * @ingroup ExegeteCalibration
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail
	{
		/** Calculates the logarithm of a value.
		 * 
		 * @param val a value.
		 * @return logarithm of a value.
		 */
		template<class F>
		F sflog(F val)
		{
			if ( val == 0 ) return -200.0f;
			return std::log(val);
		}
	};
	/** Fit parameters for sigmoid calibration to a collection of predictions.
	 * 
	 * @param beg start of prediction collection.
	 * @param end end of prediction collection.
	 * @param attr number of attributes.
	 * @param sA destination parameter.
	 * @param sB destination parameter.
	 */
	template<class I, class F>
	void sigmoid_calibration(I beg, I end, unsigned int attr, F& sA, F& sB)
	{
		double prior1 = 0.0f, prior0 = 0.0f;
		I it;
		for(it=beg;it != end;++it)
		{
			if( it->y() > 0 ) prior1+=beg->w();//++
			else prior0+=beg->w();
		}
	
		unsigned int iter_max = 100, i;
		double stp_min=1e-10;
		double sigma=1e-3;
		double eps=1e-5;
		///
		double A, B;
		double hi_target=(prior1+1.0)/(prior1+2.0);
		double lo_target=1.0/(prior0+2.0);
		double fApB,p,q,h11,h22,h21,g1,g2,det,dA,dB,gd,stepsize;
		double newA,newB,newf,d1,d2,fval=0.0,t,a;
	
		A = 0.0;
		B = detail::sflog( (prior0+1.0)/(prior1+1.0) );
	
		for(it=beg;it != end;++it)
		{
			t = (it->y() > 0) ? hi_target : lo_target;
			fApB = it->p_at(attr)*A+B;
			if( fApB >=0 ) fval += t*fApB + detail::sflog(1+exp(-fApB));
			else fval += (t-1)*fApB + detail::sflog(1+std::exp(fApB));
		}
		for(i=0;i<iter_max;++i)
		{
			h11=h22=sigma;
			h21=g1=g2=0.0;
			
			for(it=beg;it != end;++it)
			{
				t = (it->y() > 0) ? hi_target : lo_target;
				a = it->p_at(attr);
				fApB = a*A+B;
				if(fApB >= 0)
				{
					p=std::exp(-fApB)/(1.0+std::exp(-fApB));
					q=1.0/(1.0+std::exp(-fApB));
				}
				else
				{
					p=1.0/(1.0+std::exp(fApB));
					q=std::exp(fApB)/(1.0+std::exp(fApB));
				}
				d2=p*q;
				h11+=a*a*d2;
				h22+=d2;
				h21+=a*d2;
				d1=t-p;
				g1+=a*d1;
				g2+=d1;
			}
			if(std::fabs(g1)<eps && std::fabs(g2)<eps) break;
			det=h11*h22-h21*h21;
			dA=-( h22 * g1 - h21 * g2) / det;
			dB=-(-h21 * g1 + h11 * g2) / det;
			gd=g1*dA+g2*dB;
			
			stepsize = 1;
			while(stepsize >= stp_min)
			{
				newA = A + stepsize * dA;
				newB = B + stepsize * dB;
				newf = 0.0;
				for(it=beg;it != end;++it)
				{
					t = (it->y() > 0) ? hi_target : lo_target;
					fApB = it->p_at(attr)*newA+newB;
					if (fApB >= 0) newf += t*fApB + detail::sflog(1+std::exp(-fApB));
					else newf += (t-1)*fApB + detail::sflog(1+std::exp(fApB));
				}
				if (newf<fval+0.0001*stepsize*gd)
				{
					A=newA;B=newB;fval=newf;
					break;
				}
				else stepsize = stepsize / 2.0;
			}
			WARN(stepsize < stp_min, "Line search fails");
			if( stepsize < stp_min ) break;
			//ASSERTMSG( stepsize >= stp_min );
		}
		ASSERT(i < iter_max);
		sA = A;
		sB = B;
	}
	/** Estimate probabilities using the sigmoid transform with given parameters.
	 * 
	 * @param fApB prediction value.
	 * @param A sigmoid parameter.
	 * @param B sigmoid parameter.
	 * @return probabilistic transform.
	 */
	template<class F>
	F sigmoid_predict(F fApB, F A, F B)
	{
		fApB = fApB*A+B;
		if (fApB >= 0) return std::exp(-fApB)/(1.0+std::exp(-fApB));
		else return 1.0/(1+std::exp(fApB));
	}
};



#endif


