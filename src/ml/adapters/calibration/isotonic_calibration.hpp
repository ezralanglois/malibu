/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * isotonic_calibration.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ISOTONIC_CALIBRATION_HPP
#define _EXEGETE_ISOTONIC_CALIBRATION_HPP
#include <vector>
#include <algorithm>
#include <cmath>

/** @file isotonic_calibration.hpp
 * 
 * @brief Contains functions for isotonic calibration
 * 
 * This file contains a set of global functions that define isotonic calibration 
 * algorithms.
 *
 * @ingroup ExegeteCalibration
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail
	{
		/** @brief Compares a prediction to a map value
		 * 
		 * This class compares a prediction to a mpa value.
		 */
		template<class M, class F>
		struct compare_mapped_prediction
		{
			/** Defines the first template argument as a mapped type. **/
			typedef M mapped_type;
			/** Defines the second template argument as a float type. **/
			typedef F float_type;
	
			/** Compares a mapped prediction to a value.
			 *
			 * @param lhs a float value.
			 * @param rhs a mapped prediction value.
			 * @return true if the value is less than the mapped prediction.
			 */
			bool operator()(const float_type lhs, const mapped_type& rhs)
			{
				return lhs < rhs.first;
			}
			/** Compares a mapped prediction to a value.
			 *
			 * @param lhs a mapped prediction value.
			 * @param rhs a float value.
			 * @return true if mapped prediction is less than the value.
			 */
			bool operator()(const mapped_type& lhs, const float_type rhs)
			{
				return lhs.first < rhs;
			}
			/** Compares a mapped prediction to a value.
			 *
			 * @param lhs a mapped prediction value.
			 * @param rhs a mapped prediction value.
			 * @return true if mapped prediction is less than a mapped prediction.
			 */
			bool operator()(const mapped_type& lhs, const mapped_type& rhs)
			{
				return lhs.first < rhs.first;
			}
		};
		/** @brief Compares two prediction confidences at an index
		 * 
		 * This class compares two prediction confidences at an index.
		 */
		template<class T>
		struct compare_prediction
		{
			/** Defines a value type. **/
			typedef typename T::value_type value_type;
			/** Defines a constant reference. **/
			typedef const value_type& const_reference;
			/** Constructs a comparison prediction.
			 * 
			 * @param index index to compare.
			 */
			compare_prediction(unsigned int i) : index(i)
			{
			}
			/** Compares two prediction confidence values.
			 *
			 * @param val1 the left hand object.
			 * @param val2 the right hand object.
			 * @return true if val1 is less than val2.
			 */
			bool operator()(const_reference val1, const_reference val2)
			{
				return val1.p_at(index) < val2.p_at(index);
			}
			
		private:
			unsigned int index;
		};
		/** @brief Isotonic calibration elements
		 * 
		 * This class defiens an isotonic element.
		 */
		template<class T>
		struct isotonic
		{
			/** Class value. **/
			T y;
			/** Weight value. **/
			T w;
			/** Confidence value. **/
			T c;
		};
	};
	/** Fit parameters for an isotonic calibration mapping.
	 * 
	 * @param beg start of prediction collection.
	 * @param end end of prediction collection.
	 * @param attr number of attributes.
	 * @param map a calibration mapping.
	 */
	template<class I, class F>
	void isotonic_calibration(I beg, I end, unsigned int attr, std::vector< std::pair<F,F> >& map)
	{
		typedef std::vector< std::pair<F,F> > calibration_vector;
		typedef std::vector< detail::isotonic<F> > isotonic_vector;
		typedef typename calibration_vector::iterator cal_iterator;
		typedef typename isotonic_vector::iterator iterator;
		std::stable_sort(beg, end, detail::compare_prediction<I>(attr));
		isotonic_vector mod(std::distance(beg,end)), tmp;
		iterator mbeg = mod.begin(), mend;
		iterator tbeg, tend;
		bool has_violators;

		mbeg->y = beg->y();
		mbeg->w = beg->w();
		for(++beg;beg != end;++beg)
		{
			if( beg->p_at(attr) > (beg-1)->p_at(attr) )
			{
				mbeg->c = 0.5 * (beg->p_at(attr) + (beg-1)->p_at(attr));
				++mbeg;
			}
			mbeg->y += beg->y();
			mbeg->w += beg->w();
		}
		mend = mbeg + 1;
		mbeg = mod.begin();
		
		do {
			has_violators = false;
			tmp.resize(std::distance(mbeg,mend));
			tbeg = tmp.begin();
			tbeg->y = mbeg->y;
			tbeg->w = mbeg->w;
			for(++mbeg;mbeg != mend;++mbeg)
			{
				if( (mbeg->y/mbeg->w) > (tbeg->y/tbeg->w) )
				{
					tbeg->c = (mbeg-1)->c;
					++tbeg;
					tbeg->y = mbeg->y;
					tbeg->w = mbeg->w;
				}
				else
				{
					tbeg->w += mbeg->w;
					tbeg->y += mbeg->y;
					has_violators = true;
				}
			}
			mod = tmp;
			mbeg = mod.begin();
			mend = mbeg + std::distance(tmp.begin(), tbeg) + 1;
		}while(has_violators);
		
		map.resize(mod.size());
		cal_iterator it = map.begin();
		for(mbeg=mod.begin(),mend=mod.end();mbeg != mend;++mbeg,++it)
		{
			it->first = mbeg->c;
			it->second = mbeg->y / mbeg->w;
		}
	}
	/** Estimate probabilities using the isotonic mapping.
	 * 
	 * @param val prediction value
	 * @param map a calibration mapping.
	 * @return probabilistic transform.
	 */
	template<class F>
	F isotonic_predict(F val, const std::vector< std::pair<F,F> >& map)
	{
		typedef typename std::vector< std::pair<F,F> >::const_iterator const_iterator;
		if( map.empty() ) return val;
		if( val < map.front().first ) return map.front().second;
		if( val >= map.back().first ) return map.back().second;
		const_iterator it = std::lower_bound(map.begin(), map.end(), val, detail::compare_mapped_prediction< std::pair<F,F> ,F>());
		if( it->first == val ) return (it+1)->second;
		return it->second;
	}
};


#endif


