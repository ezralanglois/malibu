/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * threshold_calibration.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_THRESHOLD_CALIBRATION_HPP
#define _EXEGETE_THRESHOLD_CALIBRATION_HPP
#include <algorithm>
#include <cmath>

/** @file threshold_calibration.hpp
 * 
 * @brief Contains functions for threshold calibration
 * 
 * This file contains a set of functions to calibrate a threshold.
 *
 * @ingroup ExegeteCalibration
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail
	{
		/** @brief ompares two binary predictions.
		 * 
		 * This class compares two binary predictions.
		 */
		template<class T>
		struct compare_binary_prediction
		{
			/** Defines a value type. **/
			typedef typename T::value_type value_type;
			/** Defines a constant reference. **/
			typedef const value_type& const_reference;
			/** Compares two binary predictions.
			 *
			 * @param val1 the left hand prediction.
			 * @param val2 the right hand prediction.
			 * @return true if val1 is less than val2.
			 */
			bool operator()(const_reference val1, const_reference val2)
			{
				return val1.p() < val2.p();
			}
		};
	};
	/** Calculates the threshold of a set of predictions and a metric function.
	 *
	 * @param beg an iterator to start of prediction collection.
	 * @param end an iterator to end of prediction collection.
	 * @param pF a functor that measures some metric.
	 * @return a threshold.
	 */
	template<class I, class M>
	double threshold_calibration(I beg, I end, M pF)
	{
		unsigned int tp, tn, fp, fn;
		double th, bth;
		double acc, bst=0;
		std::stable_sort(beg, end, detail::compare_binary_prediction<I>() );
		I i=beg; ++i;
		for(I l=beg;i != end;++i)
		{
			th = (i->p() + l->p())/2.0f;
			acc = pF(beg, end, th);
			if( acc > bst )
			{
				bst=acc;
				bth=th;
			}
			l = i;
		}
		return bth;
	}
};

#endif

