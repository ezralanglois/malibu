/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * C45.c
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

/** @file C45.c
 * @brief Contains the implementation of the C45 tree interface functions.
 * 
 * This source file contains implementation of the C45 tree interface functions.
 * 
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#include "buildex.i"



#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define Calloc(type,n) (type *)calloc((n), sizeof(type))

extern ClassNo		MaxClass;
extern float *ClassSum;
extern Attribute	MaxAtt;
extern DiscrValue	*MaxAttVal;
extern ItemNo		MaxItem;
extern Boolean	Changed;
extern Boolean	AllKnown;
extern Set	*PossibleValues;

/** @brief Defines a structure to save a tree node 
 * 
 * This class defines a structure to save a tree node.
 */
struct TreeSave
{
	Tree node;
	ItemCount *ClassDist;
	short Forks;
	Tree	*Branch;
	short NodeType;
	Set         *Subset;
	struct TreeSave** next;
};
struct TreeSave* find_save(Tree node, struct TreeSave* save)
{
	ItemNo v;
	struct TreeSave* tmp = 0;
	if( node->Branch != save->Branch )
	{
		ForEach(v, 1, save->Forks)
		{
			tmp = find_save(node, save->next[v]);
			if( tmp != 0 ) return tmp;
		}
	}
	else tmp = save;
	return tmp;
}

void c45_erase_subset(Set *Subset, DiscrValue Forks, short NodeType)
{
	ItemNo v;
	//if( NodeType == BrSubset || NodeType == 0 ) 
	{
		if( Subset != 0 )
		{
			ForEach(v, 1, Forks)
			{
				free(Subset[v]);
			}
			free(Subset);
		}
	}
}
void erase_saved(struct TreeSave* save, struct TreeSave* stop, int flag)
{
	ItemNo v;
	if( save == stop ) return;
	if( save->NodeType || save->Forks )
	{
		ForEach(v, 1, save->Forks)
		{
			erase_saved(save->next[v], stop, 1);
		}
	}
	if( flag )
	{
		c45_erase_subset(save->Subset, save->Forks, save->NodeType);
		free(save->Branch);
		free(save->node);
		free(save->ClassDist);
		free(save->next);
		free(save);
	}
}
void c45_erase_pruned_node(Tree node, struct TreeSave* save, int d)
{
	ItemNo v;
	struct TreeSave* tmp = 0;
	if( node == 0 ) return;
	if( node->NodeType || node->Forks )
	{
		if( save->Branch != node->Branch )
		{
			tmp = save;
			save = find_save(node, save);
			erase_saved(tmp, save, 0);
		}
		ForEach(v, 1, node->Forks)
		{
			c45_erase_pruned_node(node->Branch[v], save->next[v], d+1);
		}
		if( save->Forks > node->Forks )
		{
			ForEach(v, node->Forks+1, save->Forks)
			{
				erase_saved(save->next[v], 0, 1);
			}
		}
		if( tmp != 0 )
		{
			c45_erase_subset(tmp->Subset, tmp->Forks, tmp->NodeType);
			free(tmp->Branch);
			if(tmp->node != node) free(tmp->node);
			free(tmp->ClassDist);
			free(tmp->next);
			free(tmp);
		}
	}
	else erase_saved(save, 0, 0);
	if( node->Subset != save->Subset ) c45_erase_subset(save->Subset, save->Forks, save->NodeType);
	c45_erase_subset(node->Subset, save->Forks, node->NodeType);
	if(save->node != node) free(save->node);
	free(save->next);
	free(save);
	free(node->Branch);
	free(node->ClassDist);
	free(node);
}

// - Free Subset use global? status (SpecialStatus)
void c45_erase_node(Tree node)
{
	ItemNo v;
	if( node == 0 ) return;
	if( node->NodeType || node->Forks )
	{
		ForEach(v, 1, node->Forks) c45_erase_node(node->Branch[v]);
		if( node->Forks ) free(node->Branch);
		if( node->NodeType == BrSubset ) //|| node->NodeType == 0 ) 
		{
			if( node->Subset != 0 )
			{
				ForEach(v, 1, node->Forks)
				{
					free(node->Subset[v]);
				}
				free(node->Subset);
			}
		}
	}
	free(node->ClassDist);
	free(node);
}


////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////


struct TreeModel
{
	Tree model;
	unsigned int *perm;
	DiscrValue *maxatt;
	DiscrValue count;
	ClassNo maxclass;
	struct TreeSave* save;
};
void c45_init(struct TreeModel* model)
{
	model->model = 0;
	model->perm = 0;
	model->maxatt = 0;
	model->count = 0;
	model->save = 0;
	model->maxclass = 0;
}
void c45_erase_model(struct TreeModel* model)
{
	if( model->save != 0 ) c45_erase_pruned_node(model->model, model->save, 0);
	else if( model->model != 0 ) c45_erase_node(model->model);
	model->save = 0;
	if( model->perm != 0 ) free(model->perm);
	if( model->maxatt != 0 ) free(model->maxatt);
	c45_init(model);
}

Tree CopyTree(Tree);
void c45_copy_tree(const struct TreeModel* from, struct TreeModel* to)
{
	int i, n;
	c45_erase_model(to);
	to->maxclass = from->maxclass;
	to->count = from->count;
	if( from->model != 0 ) to->model = CopyTree(from->model);
	if( from->count > 0 )
	{
		to->maxatt = Malloc(DiscrValue, to->count);
		for(i=0, n=to->count;i<n;++i) to->maxatt[i] = from->maxatt[i];
		if( from->perm != 0 ) 
		{	
			to->perm = Malloc(unsigned int, to->count);
			for(i=0, n=to->count;i<n;++i) to->perm[i] = from->perm[i];
		}
	}
}

struct TreeSave* copy_tree(Tree model)
{
	ItemNo v;
	if( model != 0 )
	{
		struct TreeSave* root = (struct TreeSave*)malloc(sizeof(struct TreeSave));
		root->node = model;
		root->ClassDist = model->ClassDist;
		root->Forks = model->Forks;
		root->Branch = model->Branch;
		root->NodeType = model->NodeType;
		root->Subset = model->Subset;
		root->next = (struct TreeSave**)malloc(sizeof(struct TreeSave*)*(model->Forks+1));
		root->next[0] = 0;
		ForEach(v, 1, model->Forks)//5
		{
			root->next[v] = copy_tree(model->Branch[v]);
		}
		return root;
	}
	return 0;
}

Tree FormTree(ItemNo Fp, ItemNo Lp);
float EstimateErrors(Tree T, ItemNo Fp, ItemNo Lp, short Sh, Boolean UpdateTree);
void CheckPossibleValues(Tree T);
void SoftenThresh(Tree  T);
void InitialiseWeights();
void c45_train(struct TreeModel* model)
{
	extern Boolean PROBTHRESH;
	extern Boolean PRUNE;
	Attribute old = MaxAtt;
	MaxAtt = model->count-1;
	MaxClass = model->maxclass;
	MaxAttVal = model->maxatt;
	if( model->save != 0 ) c45_erase_pruned_node(model->model, model->save, 0);
	else if( model->model != 0 ) c45_erase_node(model->model);
	model->save = 0;
	model->model = FormTree(0, MaxItem);
	if( PRUNE == 1 )
	{
		Attribute a;
		ItemNo i;
		model->save = copy_tree(model->model);
		InitialiseWeights();
		AllKnown = true;
		Changed = false;
		EstimateErrors(model->model, 0, MaxItem, 0, true);
		if( SUBSET )
		{
			if( !PossibleValues ) PossibleValues = (Set*)calloc(MaxAtt+1, sizeof(Set));
			ForEach(a, 0, MaxAtt)
			{
				if( MaxAttVal[a] )
				{
					if( PossibleValues[a] != 0 ) free(PossibleValues[a]);
					PossibleValues[a] = (Set)malloc((MaxAttVal[a]>>3) + 1);
					ClearBits((MaxAttVal[a]>>3) + 1, PossibleValues[a]);
					ForEach(i, 1, MaxAttVal[a]) SetBit(i, PossibleValues[a]);
				}
			}
			CheckPossibleValues(model->model);
		}
	}
	if( PROBTHRESH == 1 ) SoftenThresh(model->model);
	MaxAtt = old;
}


Description TempItem = 0;
//void Classify(Description CaseDesc, Tree T, float Weight);
void c45_copy(const AttValue* from, AttValue* to, const struct TreeModel* model);

double c45_predict(const struct TreeModel* model, const AttValue* CaseDesc)
{
	Attribute old = MaxAtt;
	int i;
	MaxAttVal = model->maxatt;
	if( MaxClass != model->maxclass )
	{
		MaxClass = model->maxclass;
		free(ClassSum);
		ClassSum = Malloc(float, MaxClass+1);
	}
	for(i=0;i<=MaxClass;++i) ClassSum[i] = 0;
	if( MaxAtt != (model->count-1) || TempItem == 0 )
	{
		MaxAtt = model->count-1;
		TempItem = (Description)realloc(TempItem, (MaxAtt+2)*sizeof(AttValue)); 
	}
	c45_copy(CaseDesc, TempItem, model);
	Classify(TempItem, model->model, 1.0);
	MaxAtt = old;
	return ClassSum[1];
}

