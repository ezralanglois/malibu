/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * C45.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_C45_H
#define _EXEGETE_C45_H
#include "Learner.h"
#include "C45Util.h"
/** Defines the version of the C4.5 algorithm. 
 * @todo add to group
 */
#define _C45_VERSION 101000

/** @file C45.h
 * @brief Third-party C4.5 classifier interface
 * 
 * This file contains the third-party C45 class interface.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @defgroup ExegeteClassifiers Classifiers
 * 
 *  This group holds all the classifier files.
 */

namespace exegete
{
	/** @brief Third-party C4.5 classifier interface
	 * 
	 * This class defines an interface to the third-party C4.5 algorithm.
	 *
	 * @todo add shallow copy
	 * @todo add parameter copy
	 * 
	 * @todo IMPORTANT -> possible error random forests, writing model, convert trained tree indices to perm before using!!!
	 */
	class C45 : public Learner // 
	{
	public:
		/** Interface flags:
		 *	- Do not set attribute size to 8
		 * 	- Not a lazy learner
		 * 	- Do not use a training set
		 * 	- Start argument at 1
		 */
		enum{ ATTR8=0, LAZY=0, USE_TRAINSET=0, ARGUMENT=1, GRAPHABLE=1 };

	public:
		/** Defines a parent type. **/
		typedef void parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<AttValue, int, void>						dataset_type;
		/** Defines a testset type. **/
		typedef ExampleSet<AttValue, int, std::string*>				testset_type;
		/** Defines a constant attribute pointer. **/
		typedef dataset_type::const_attribute_pointer				const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef AttValue											descriptor_type;
		/** Defines a float type. **/
		typedef double												float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator							argument_iterator;
		/** Defines a weight type. **/
		typedef void												weight_type;
		/** Defines a prediction type. **/
		typedef float_type 											prediction_type;
	private:
		typedef dataset_type::size_type			size_type;
		typedef TuneParameterTree				argument_type;
		typedef argument_type::parameter_type	range_type;

	public:
		/** Constructs a default C4.5 tree.
		 */
		C45() : Learner(SEMI|BINARY), subsetInt(0), 
		 		cfSel(CF, "CF", range_type(0.25f, 0.35f, 0.1f, '+')),
		 		subsetSel(subsetInt, "RandSubset", range_type(0,0,1,'+'), &cfSel)
		{
			c45_init(&model);
		}
		/** Destructs a C4.5 classifier and deallocates a C4.5 model.
		 */
		~C45()
		{
			c45_erase_model(&model);
		}		
		
	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(C45) );
				arginit(map, TRIALS,			"trials",		"enable windowing with number of trials", ArgumentMap::ADVANCED);
				arginit(map, PROBTHRESH,		"probthresh",	"should use probability thresholds?");
				arginit(map, WINDOW,			"window",		"initial window of size", ArgumentMap::ADVANCED);
				arginit(map, INCREMENT,			"increment",	"maximum window increment", ArgumentMap::ADVANCED);
				arginit(map, GAINRATIO,			"gainratio",	"should use gain ratio?");
				arginit(map, SUBSET,			"subset",		"should use tests on discreet attribute groups?");
				arginit(map, MINOBJS,			"minobjs",		"require at least n cases to branch", ArgumentMap::ADVANCED);
				arginit(map, PRUNE,				"prune",		"should prune the tree?");
				if(t>=0)
				{
					arginit(map, cfSel,			"cf",			"pruning confidence level");
					arginit(map, subsetSel,		"randsubset",	"use a random attrute subset of size n");
				}
				else
				{
					arginit(map, CF,			"cf",			"pruning confidence level");
					arginit(map, subsetInt,		"randsubset",	"use a random attrute subset of size n");
				}
			}
		}
		/** Constructs a copy of the C4.5 tree.
		 *  
		 * @todo Need to fix, copy ->save, fix memory errors
		 * 
		 * @param tree a C4.5 tree.
		 */
		C45(const C45& tree) : Learner(tree), subsetInt(tree.subsetInt), cfSel(CF, tree.cfSel), subsetSel(subsetInt, tree.subsetSel, &cfSel)
		{
			c45_init(&model);
			if( tree.model.model != 0 ) c45_copy_tree(&tree.model, &model);
		}
		/** Assigns a copys of a C4.5 tree.
		 * 
		 * @param tree a C4.5 tree.
		 * @return a reference to this object.
		 */
		C45& operator=(const C45& tree)
		{
			Learner::operator=(tree);
			argument_copy(tree);
			c45_copy_tree(&tree.model, &model);
			return *this;
		}
		
	public:
		/** Copies arguments of a C4.5 tree.
		 * 
		 * @param ref a C4.5 model.
		 */
		void argument_copy(const C45& ref)
		{
			subsetInt = ref.subsetInt;
			cfSel = ref.cfSel;
			subsetSel = ref.subsetSel;
			Learner::argument_copy(ref);
		}
		/** Makes a shallow copy of a C4.5 tree.
		 *
		 * @param ref a reference to a C4.5 tree.
		 */
		void shallow_copy(C45& ref)
		{
			Learner::operator=(ref);
			argument_copy(ref);
			model.model = ref.model.model;
			model.perm = ref.model.perm;
			model.maxatt = ref.model.maxatt;
			model.count = ref.model.count;
			model.model = ref.model.model;
			model.maxclass = ref.model.maxclass;
			model.save = ref.model.save;
			ref.model.model = 0;
			ref.model.perm = 0;
			ref.model.maxatt = 0;
			ref.model.save = 0;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.5f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			if( empty() ) return threshold();
			return c45_predict(&model, pred);
		}
		/** Builds a model for the C4.5 tree.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			c45_setup(&model, learnset, subsetInt);
			c45_train(&model);
			return 0;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return C4.5
		 */
		static std::string name()
		{
			return "C4.5";
		}
		/** Get the version of the algorithm.
		 * 
		 * @return algorithm version.
		 */
		static int version()
		{
			return _C45_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return c45
		 */
		static std::string prefix()
		{
			return "c45";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return cfSel;
		}
		/** Tests if model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return model.model == 0;
		}
		/** Clear the model.
		 */
		void clear()
		{
			c45_erase_model(&model);
		}
		/** Clears all dynamically allocated global memory in C4.5.
		 */
		static void cleanall()
		{
			c45_erase();
		}
		/** Get the expected name of the program.
		 * 
		 * @return c45
		 */
		static std::string progname()
		{
			return "c45";
		}
		/** Get the class name of the learner.
		 * 
		 * @return C45
		 */
		static std::string class_name()
		{
			return "C45";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
		}
		/** Graph a C4.5 model.
		 * 
		 * @param graph a graph object.
		 */
		void graph(Graph& graph)const
		{
			c45_graph(graph, &model);
		}
		/** Collect rules in a C4.5 model.
		 * 
		 * @param vec a 2D vector.
		 */
		void graph(std::vector< std::vector<double> >& vec)const
		{
			c45_graph(vec, &model);
		}

	public:
		/** Reads an c4.5 tree from the input stream.
		 *
		 * @param in an input stream.
		 * @param tree a C4.5 tree.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, C45& tree)
		{
			tree.errormsg(in, c45_read_model(in, &tree.model));
			return in;
		}
		/** Writes an C4.5 tree to the output stream.
		 *
		 * @param out an output stream.
		 * @param tree a C4.5 tree.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const C45& tree)
		{
			tree.errormsg(out, c45_write_model(out, &tree.model));
			return out;
		}

	private:
		struct TreeModel model;

	private:
		unsigned int subsetInt;
		argument_type cfSel;
		argument_type subsetSel;
	};

};

/** @brief Type utility interface to a C4.5 tree
 * 
 * This class defines a type utility interface to a C4.5 tree.
 */
template<>
struct TypeUtil< ::exegete::C45 >
{
	/** Flags class as not primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::C45 value_type;
	/** Tests if a string can be converted to a C4.5 type.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return C45
	 */
	static const char* name() 
	{
		return "C45";
	}
};

#endif

