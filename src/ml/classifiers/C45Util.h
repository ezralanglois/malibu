/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * C45Util.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_C45UTIL_H
#define _EXEGETE_C45UTIL_H
#include "AttributeTypeUtil.h"
#include "typeutil.h"
#include "ExampleSet.h"
#include "Graph.h"



/** @file C45Util.h
 * @brief Utility classes for the C4.5 tree
 * 
 * This file contains utility classes for the C4.5 tree.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#ifndef _NO_C45_STRUCT
extern "C"
{
	/** @brief Defines a C4.5 attribute union
	 * 
	 * This union defines a C4.5 attribute value.
	 */
	typedef  union  _attribute_value
	 {
	    short	_discr_val;
	    float	_cont_val;
	 } AttValue;
	 
	 /** Defines a pointer to a tree record as a Tree. **/
	typedef  struct _tree_record *Tree;
	/** @brief A wrapper of a C4.5 tree model
	 * 
	 * This c structure is an adaptation for random forests.
	 */
	struct TreeModel
	{
		Tree model;
		unsigned int *perm;
		short *maxatt;
		short count;
		short maxclass;
		struct TreeSave* save;
	};
	/** Trains a modified C4.5 model.
	 *
	 * @param model a modified C4.5 model.
	 */
	void c45_train(TreeModel* model);
	/** Frees memory in modified C4.5 model.
	 *
	 * @param model a modified C4.5 model.
	 */
	void c45_erase_model(TreeModel* model);
	/** Copies a modified C4.5 tree model.
	 *
	 * @param from a source tree.
	 * @param to a destination tree.
	 */
	void c45_copy_tree(const TreeModel* from, TreeModel* to);
	/** Makes a prediction based on a C4.5 tree model.
	 *
	 * @param model a modified C4.5 model.
	 * @param CaseDesc an attribute vector.
	 * @return a prediction.
	 */
	double c45_predict(const TreeModel* model, const AttValue* CaseDesc);
	/** Initalizes tree model.
	 *
	 * @param model a pointer to a modified C4.5 model.
	 */
	void c45_init(TreeModel* model);
	/** Erases memory allocated for thirdparty C4.5 code.
	 */
	void c45_erase();

	/** Sets trials for windowing. **/
	extern short TRIALS;
	/** Use soft thresholds. **/
	extern char	PROBTHRESH;
	/** Initial window size. **/
	extern int	WINDOW;
	/** Max window increment each iteration. **/
	extern int	INCREMENT;
	/** Sets the gain ratio impurity for splitting attributes. **/
	extern char	GAINRATIO;
	/** Subset tests are allowed. **/
	extern char	SUBSET;
	/** Sets the minimum items each side of a split. */
	extern int	MINOBJS;
	/** Sets the C4.5 to prune after growing. **/
	extern char	PRUNE;
	/** Sets the confidence limit for tree pruning. **/
	extern float	CF;
}
#endif

/** @brief Type utility interface for a C4.5 attribute value
 * 
 * This class template provides basic type operations for 
 * a C4.5 attribute value.
 */
template<>
struct TypeUtil< AttValue >
{
	/** Flags class as primative.
	 */
	enum{ ispod=true };
	/** Defines a value type. **/
	typedef float value_type;
	/** Tests if a string can be converted to class type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	*/
	static bool valid(const char* str)
	{
		return TypeUtil<value_type>::valid(str);
	}
	/** Gets the name of the type.
	 *
	 * @return AttValue
	*/
	static const char* name() 
	{
		return "AttValue";
	}
	/** Gets maximum value of the AttValue.
	 *
	 * @return the maximum value.
	 */
	static value_type max()
	{
		return TypeUtil<value_type>::max();
	}
	/** Gets minimum value of the AttValue.
	 *
	 * @return the minimum value.
	 */
	static value_type min()
	{
		return TypeUtil<value_type>::min();
	}
};
namespace exegete
{

	/** @brief Specializes AttributeUtil for an AttValue
	 * 
	 * This class represents an attribute type corresponding to a column in
	 * the dataset. It is specialized to AttValue
	 * 
	 * @ingroup ExegeteDataset
	 */
	template<>
	class AttributeUtil< AttValue >
	{
	public:
		/** Flags attribute as sparse. **/
		enum{IS_SPARSE=false};
	public:
		/** Defines a TypeUtil as limit type. **/
		typedef TypeUtil<AttValue>		limit_type;
		/** Defines a float as a value type. **/
		typedef float					value_type;
		/** Defines a AttValue as an attribute type. **/
		typedef AttValue				attribute_type;
		/** Defines pointer to attribute type as pointer. **/
		typedef attribute_type*			pointer;
		/** Defines a constant pointer to attribute type as a constant pointer. **/
		typedef const attribute_type*	const_pointer;
		/** Defines an int as an index type. **/
		typedef int						index_type;
		/** Defines a size_t as a size type. **/
		typedef size_t					size_type;
		/** Defines a pointer difference as a difference type. */
		typedef ptrdiff_t				difference_type;

	public:
		/** Converts an attribute to a value.
		 *
		 * @param attr an attribute type.
		 * @return AttValue _cont_val.
		 */
		static const value_type& valueOf(const attribute_type& attr)
		{
			return attr._cont_val;
		}
		/** Converts an attribute to a value.
		 *
		 * @param attr an attribute type.
		 * @return AttValue _cont_val.
		 */
		static value_type& valueOf(attribute_type& attr)
		{
			return attr._cont_val;
		}
		/** Converts an attribute to an index.
		 *
		 * @param attr an attribute type.
		 * @return 0
		 */
		static index_type indexOf(const attribute_type& attr)
		{
			return 0;
		}
		/** Converts an attribute to an index.
		 *
		 * @param attr an attribute type.
		 * @return 0
		 */
		static index_type& indexOf(attribute_type& attr)
		{
			static index_type tmpindex=0;
			return tmpindex;
		}
		/** Flags the specified attribute as the last.
		 * 
		 * @note Does nothing.
		 *
		 * @param px a pointer to attributes.
		 * @param n index of attribute.
		 */
		static void marklast(pointer px, size_type n)
		{
		}
		/** Tests if attribute is last sparse attribute.
		 *
		 * @param attr a reference to an attribute type.
		 * @return true if attribute index is last.
		 */
		static bool islast(const attribute_type& attr)
		{
			return false;
		}
		/** Gets the internal value of a missing attribute.
		 * 
		 * @return maximum value of AttValue.
		 */
		static value_type missing()
		{
			return limit_type::max();
		}
	};

	/** Sets up the internal data for C4.5.
	 *
	 * @param model a modified C4.5 model.
	 * @param dataset an ExampleSet
	 * @param subset a subset (for Random Forests).
	 */
	void c45_setup(TreeModel* model, ExampleSet<AttValue, int, void>& dataset, unsigned int subset);
	/** Writes a modified C4.5 tree to the output stream.
	 *
	 * @param out an output stream.
	 * @param model a modified C4.5 model.
	 * @return an error message or NULL.
	 */
	const char* c45_write_model(std::ostream& out, const TreeModel* model);
	/** Reads a modified C4.5 tree from the input stream.
	 *
	 * @param in an input stream.
	 * @param model a modified C4.5 model.
	 * @return an error message or NULL.
	 */
	const char* c45_read_model(std::istream& in, TreeModel* model);
	/** Creates a graph of a C4.5 Tree.
	 * 
	 * @param graph a graph object.
	 */
	void c45_graph(Graph& graph, const TreeModel* model);
	/** Creates a graph of a C4.5 Tree.
	 * 
	 * @param graph a graph object.
	 */
	void c45_graph(std::vector< std::vector<double> >& graph, const TreeModel* model);
};



#endif


