/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * C45_dataset.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

/** @file C45_dataset.cpp
 * @brief Contains the implementation of the C4.5 dataset interface functions.
 * 
 * This source file contains implementation of the C4.5 dataset interface functions.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#define _NO_C45_STRUCT
#include "mathutil.h"
#include "assert.h"
#include <stdlib.h>
#include <memory.h>

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define Calloc(type,n) (type *)calloc((n), sizeof(type))


extern "C"
{
	#include "buildex.i"

	
	struct TreeModel
	{
		Tree model;
		unsigned int *perm;
		DiscrValue *maxatt;
		DiscrValue count;
		ClassNo maxclass;
		struct TreeSave* save;
	};


	/**************************************************/
	
	float EX_MISSING = 0;
	short	VERBOSITY = 0;
    short	TRIALS    = 10;
	String		FileName;
	//Args
	Boolean	GAINRATIO  = 1;
	Boolean	SUBSET     = 0;
	Boolean	UNSEENS    = 0;
	Boolean PROBTHRESH = 0;
	Boolean	PRUNE	   = 0;
	ItemNo	MINOBJS   = 2;
	ItemNo	WINDOW    = 0;
	ItemNo	INCREMENT = 0;
	float	CF = 0.25;
	Boolean		AllKnown = 1;

	/**************************************************/

	extern float *ClassSum;
	//double		*ClassSum = 0;
	ClassNo		MaxClass=0;
	DiscrValue	*MaxAttVal = 0;
	char		*SpecialStatus = 0;
	Attribute	MaxAtt=-2;
	Attribute	MaxDiscrVal = 2;
	ItemNo		MaxItem;
	Description	*Item;
	//Unused
	String		*AttName = 0;
	String		**AttValName = 0;
	String		*ClassName = 0;


	/*******************************************************************/
	extern Boolean	Changed;
	extern Set	*PossibleValues;
	extern Boolean *Tested, MultiVal;
	extern float *Gain, *Info, *Bar, *UnknownRate;
	extern float *SplitGain, *SplitInfo;
	extern ItemCount *Slice1, *Slice2;
	extern Set **Subset;
	extern Attribute *Subsets;
	extern ItemCount *Weight, **Freq, *ValFreq, *ClassFreq;
	/*******************************************************************/

	void c45_free(void** ptr)
	{
		if( *ptr != 0 )
		{
			free(*ptr);
			*ptr = 0;
		}
	}

	void c45_erase_dataset()
	{
		c45_free((void**)&ClassSum);
		c45_free((void**)&SpecialStatus);

		MaxClass = 0;
		MaxAtt = -1;
		MaxDiscrVal = 2;
		if( Item != 0 )
		{
			ItemNo i;
			ForEach(i, 0, MaxItem) free( Item[i] );
			free( Item );
			Item = 0;
		}
		MaxItem = 0;
	}

	void c45_erase_comput()
	{
		ItemNo v,a;
		c45_free((void**)&Tested);
		c45_free((void**)&Gain);
		c45_free((void**)&Info);
		c45_free((void**)&Bar);
		c45_free((void**)&Subsets);
		c45_free((void**)&SplitGain);
		c45_free((void**)&SplitInfo);
		c45_free((void**)&Weight);
		c45_free((void**)&ValFreq);
		c45_free((void**)&ClassFreq);
		c45_free((void**)&Slice1);
		c45_free((void**)&Slice2);
		c45_free((void**)&UnknownRate);
		if( Freq != 0 )
		{    
			ForEach(v, 0, MaxDiscrVal) free( Freq[v] );
			free( Freq );
			Freq = 0;
		}
		if( Subset != 0 )
		{
			ForEach(a, 0, MaxAtt)
			{
				if( Subset[a] != 0 )
				{
					ForEach(v, 0, MaxAttVal[a]) free(Subset[a][v]);
					free(Subset[a]);
				}
			}
			free(Subset);
			Subset = 0;
		}
		if( PossibleValues )
		{
			ForEach(a, 0, MaxAtt)
			{
				free(PossibleValues[a]);
			}
		}
		c45_free((void**)&PossibleValues);
	}
	void c45_erase()
	{
		c45_erase_comput();
		c45_erase_dataset();
	}

	void InitialiseWeights();
	void InitialiseTreeData();
	void c45_setup_comput(bool chang)
	{
		if( chang )
		{
			c45_erase_comput();
			InitialiseTreeData();
		}
		else
		{
			ItemNo v, i;
			short a;
			ForEach(v, 0, MaxAtt) 
			{
				Tested[v] = 0;
				Gain[v] = 0;
				Info[v] = 0;
				Bar[v] = 0;
				UnknownRate[v] = 0;
			}
			ForEach(v, 0, MaxClass) ClassFreq[v] = 0;
			ForEach(v, 0, MaxClass+1) Slice1[v] = 0;
			ForEach(v, 0, MaxClass+1) Slice2[v] = 0;

			ForEach(v, 0, MaxDiscrVal) 
			{
				ForEach(i, 0, MaxClass) Freq[v][i] = 0;
				ValFreq[v] = 0;
			}
			MultiVal = 1;
			if ( !SUBSET )
			{
				for ( a = 0 ; MultiVal && a <= MaxAtt ; a++ )
				{
					if ( SpecialStatus[a] != IGNORE )
					{
						MultiVal = (MaxAttVal[a] >= 0.3 * (MaxItem + 1))?1:0;
					}
				}
			}
			if( SplitGain != 0 )
			{
				SplitGain = (float*)realloc(SplitGain, (MaxItem+1)*sizeof(float));
				SplitInfo = (float*)realloc(SplitInfo, (MaxItem+1)*sizeof(float));
				Weight	  = (ItemCount*)realloc(Weight,(MaxItem+1)*sizeof(ItemCount));
				ForEach(i, 0, MaxItem) 
				{
					SplitGain[i] = 0;
					SplitInfo[i] = 0;
					Weight[i] = 0;
				}
			}
		}
		InitialiseWeights();
	}
	void c45_copy(const AttValue* from, AttValue* to, const TreeModel* model)
	{
		const unsigned int* perm = model->perm;
		MaxAttVal = model->maxatt;
		assert(to != 0);
		//SpecialStatus
		for(int i=0,n=model->count,j;i<n;++i)
		{
			j = (perm != 0) ? perm[i] : i;
			if( MaxAttVal[i] || SpecialStatus[i] == DISCRETE )
			{
				if( CVal(from, j) == EX_MISSING ) DVal(to, i) = 0;
				else DVal(to, i) = DiscrValue(CVal(from, j))+1;
			}
			else
			{
				assert(i<model->count);
				if( CVal(from, j) == EX_MISSING ) CVal(to, i) = Unknown;
				else CVal(to, i) = CVal(from, j);
			}
		}
	}
}



#include "C45Util.h"

namespace exegete
{

	void c45_setup(TreeModel* model, ExampleSet<AttValue, int, void>& dataset, unsigned int subset)
	{
		EX_MISSING = ExampleSet<AttValue, int, void>::value_type::missing();
		bool flag;
		ASSERT(dataset.classCount() > 1);
		if( MaxClass != (dataset.classCount()-1) )
		{
			MaxClass = (ClassNo)(dataset.classCount()-1);
			if( ClassSum != 0 ) free(ClassSum);
			ClassSum = Malloc(float, MaxClass+1);
		}
		if( MaxAtt != ((DiscrValue)dataset.attributeCount()-1) )
		{
			flag=true;
			model->count = (DiscrValue)dataset.attributeCount();
			MaxAtt = (DiscrValue)dataset.attributeCount()-1;
			SpecialStatus = Calloc( char, MaxAtt+1 );
			model->maxatt = MaxAttVal = Calloc( DiscrValue, MaxAtt+1 );
		}
		else flag=false;

		model->maxclass = MaxClass;
		if( model->maxatt == 0 )
		{
			model->count = (DiscrValue)dataset.attributeCount();
			model->maxatt = MaxAttVal = Calloc( DiscrValue, MaxAtt+1 );
		}
		if( model->perm != 0 ) 
		{
			free(model->perm);
			model->perm = 0;
		}

		
		if( subset > 0 && subset < dataset.attributeCount() )
		{
			DiscrValue *randAttr = Malloc(DiscrValue, MaxAtt+2);
			for(unsigned int i=0;i<=dataset.attributeCount();++i) randAttr[i]=1;
			unsigned int unusedsubset = (MaxAtt+1)-subset;
			for(unsigned int i=0, m, j;i<unusedsubset;++i)
			{
				m = random_int(MaxAtt-i);
				if( randAttr[m] == 0 )
				{
					for(j=0;j<dataset.attributeCount();j++)
					{
						if( randAttr[j] != 0 )
						{
							if( m == 0 )
							{
								randAttr[j] = 0;
								break;
							}
							else --m;
						}
					}
				}
				else randAttr[m] = 0;
			}
			model->perm  = Malloc(unsigned int, subset+1);
			for(unsigned int i=0,j=0;i<dataset.attributeCount();++i)
			{
				if( randAttr[i] > 0 ) 
				{
					model->perm[j] = i;
					++j;
				}
			}
			model->count=subset;
			free(randAttr);
		}

		unsigned int* perm = model->perm;
		for(unsigned int i=0,j,n=model->count;i<n;++i)
		{
			j = (perm != 0) ? perm[i] : i;
			ASSERT(j<dataset.attributeCount());
			MaxAttVal[i] = (DiscrValue)dataset.attributeAt(j).size();
			if( dataset.attributeAt(j).isnominal() ) if( MaxAttVal[i] > MaxDiscrVal  ) MaxDiscrVal = MaxAttVal[i];
		}
		ASSERTMSG(dataset.size()< (1<<(sizeof(ItemNo)*8-1)), dataset.size() << " < " << (1<<(sizeof(ItemNo)*8-1)));
		if( (MaxItem+1) != dataset.size() )
		{
			if( Item != 0 )
			{
				for(int i=0;i<=MaxItem;++i) free(Item[i]);
				free(Item);
			}
			Item = Calloc(Description, dataset.size());
			MaxItem = (ItemNo)dataset.size()-1;
		}
		DiscrValue old = MaxAtt;
		MaxAtt = model->count-1;
		for(unsigned int i=0;i<dataset.size();++i)
		{
			if( Item[i] ) std::free(Item[i]);
			Item[i] = Malloc(AttValue, MaxAtt+2);
			c45_copy(dataset[i].x(), Item[i], model);
			ASSERTMSG(dataset[i].y() <= MaxClass, dataset[i].y() << " <= " << MaxClass);
			Class(Item[i]) = dataset[i].y();
		}
		MaxAtt=old;
		c45_setup_comput(flag);
	}

	void c45_graph(std::vector< std::vector<double> >& vec, const Tree tree, ClassNo cl, unsigned int rank, float w)
	{
		int type=-1;
		if( tree->NodeType )
		{
			unsigned int n = vec.size()-1;
			unsigned int m = vec.back().size()+1;
			vec.back().push_back( -1 * (tree->Tested+1) );
			for(int i=1;i<=tree->Forks;++i) 
			{
				if( i > 1 )
				{
					vec.push_back(std::vector<double>());
					vec.back().resize(m);
					std::copy(vec[n].begin(), vec[n].begin()+m, vec.back().begin());
					vec.back().back() = -1 * vec.back().back();
				}
				c45_graph(vec, tree->Branch[i], cl, i, w);
			}
		}
		else
		{
			if( tree->ClassDist && tree->Items > 0 )
			{
				vec.back().push_back( float(tree->ClassDist[1]) / float(tree->Items) );
			}
			else vec.back().push_back( 0.5 );
		}
	}
	void c45_graph(Graph& graph, const Tree tree, ClassNo cl, unsigned int rank, float w)
	{
		std::string str1, str2;
		int type=-1;
		if( tree->NodeType )
		{
			switch( tree->NodeType )
			{
				case BrDiscr:
					str1="Unknown";
					type=1;
					break;
				case ThreshContin:
					valueToString(tree->Cut, str2);
					type=0;
					break;
				case BrSubset:
					str1="Subset";
					type=2;
			};
			valueToString(tree->Tested, str1);// attribute name
			graph.addnode(long(tree), str1, rank, type);
			ASSERT(type>=0);
			for(int i=1;i<=tree->Forks;++i) 
			{
				const Tree b = tree->Branch[i];
				if(i == 1)
				{
					graph.addedge(long(tree), long(b), str2, type+1);
				}
				else
				{
					graph.addedge(long(tree), long(b), "", -(type+1));
				}
				c45_graph(graph, b, cl, rank+1, (w * tree->Branch[i]->Items) / tree->Items);
			}
		}
		else
		{
			if( tree->ClassDist && tree->Items > 0 )
			{
				//float val = w * float(tree->ClassDist[1]) / tree->Items;
				float val = float(tree->ClassDist[1]) / tree->Items;
				valueToString(val, str1);
				// attribute name
			}
			else str1="0.5";
			graph.addnode(long(tree), str1, rank, type);
		}
	}
	const char* c45_write_tree(std::ostream& out, const Tree tree, ClassNo cl)
	{
		int Bytes;
		out << tree->NodeType << "," << tree->Leaf << "," << tree->Items << "," << tree->Errors;
		for(int i=0, n=(cl+1);i<n;++i) out << "," << tree->ClassDist[i];
		if( tree->NodeType )
		{
			out << "," << tree->Tested << "," << tree->Forks;
			switch( tree->NodeType )
			{
				case BrDiscr:
					break;
				case ThreshContin:
					out << "," << tree->Cut << "," << tree->Lower << "," << tree->Upper;
					break;
				case BrSubset:
					Bytes = (MaxAttVal[tree->Tested]>>3) + 1;
					for(int i=1;i<=tree->Forks;++i) for(int j=0;j<Bytes;++j) out << "," << tree->Subset[i][j];
			};
		}
		out << "\n";
		if( tree->NodeType ) for(int i=1;i<=tree->Forks;++i) c45_write_tree(out, tree->Branch[i], cl);

		return 0;
	}
	const char* c45_write_model(std::ostream& out, const TreeModel* model)
	{
		out << model->maxclass << "," << model->count;
		for(int i=0;i<model->count;++i) out << "," << model->maxatt[i];
		if( model->perm != 0 ) for(int i=0;i<model->count;++i) out << "," << model->perm[i];
		out << "\n";
		c45_write_tree(out, model->model, model->maxclass);
		return 0;
	}
	void c45_graph(Graph& graph, const TreeModel* model)
	{
		c45_graph(graph, model->model, model->maxclass, 0, 1.0);
	}
	void c45_graph(std::vector< std::vector<double> >& vec, const TreeModel* model)
	{
		vec.resize(1);
		c45_graph(vec, model->model, model->maxclass, 0, 1.0);
	}

	const char* c45_read_tree(std::istream& in, Tree tree, ClassNo cl)
	{
		const char* msg;
		int Bytes;
		if( in.eof() ) return ERRORMSG("Unexpected end of file");
		std::string line;
		std::getline(in, line);
		std::vector<float> arr;
		stringToValue(line, arr, ",");
		if( (int)(arr.size()) < (4+cl) ) return ERRORMSG("Invalid line1");
		tree->NodeType = (short)arr[0];
		tree->Leaf = (ClassNo)arr[1];
		tree->Items = arr[2];
		tree->Errors = arr[3];
		if( cl == 0 ) return ERRORMSG("cl == 0");
		tree->ClassDist = Malloc(ItemCount, cl+1);
		for(int i=0, n=cl+1;i<n;++i) tree->ClassDist[i] = arr[4+i];
		tree->Forks=0;
		if( tree->NodeType )
		{
			if( int(arr.size()) < (4+cl+2+1) ) return ERRORMSG("Invalid line2");
			tree->Tested = (short)arr[4+cl+1]; 
			tree->Forks = (short)arr[4+cl+2];
			switch( tree->NodeType )
			{
				case BrDiscr:
					break;
				case ThreshContin:
					if( int(arr.size()) < (cl+9) ) return ERRORMSG("Invalid line3");
					tree->Cut = arr[7+cl+0];
					tree->Lower = arr[7+cl+1];
					tree->Upper = arr[7+cl+2];
					break;
				case BrSubset:
					Bytes = (MaxAttVal[tree->Tested]>>3) + 1;
					if( int(arr.size()) < (7+cl+(tree->Forks*Bytes)) ) return ERRORMSG("Invalid line2");
					tree->Subset = Calloc(Set, tree->Forks + 1);
					tree->Subset[0]=0;
					for(int i=1,k=0;i<=tree->Forks;++i,++k) 
					{
						tree->Subset[i] = Malloc(char, Bytes);
						for(int j=0;j<Bytes;++j) tree->Subset[i][j] = (char)arr[7+cl+k];
					}
			};
			tree->Branch = Malloc(TreeRec*, tree->Forks+1);
			for(int i=1;i<=tree->Forks;++i) 
			{
				tree->Branch[i] = Malloc(TreeRec, 1);
				if( (msg=c45_read_tree(in, tree->Branch[i], cl)) != 0 ) return msg;
			}
		}
		return 0;
	}
	const char* c45_read_model(std::istream& in, TreeModel* model)
	{
		ASSERT(model != 0);
		EX_MISSING = ExampleSet<AttValue, int, void>::value_type::missing();
		if( in.eof() ) return ERRORMSG("Unexpected end of file");
		if(SpecialStatus != 0) free(SpecialStatus);
		std::string line;
		std::getline(in, line);
		while( !in.eof() && line == "" ) std::getline(in, line);
		if( in.eof() && line == "" ) return ERRORMSG("Unexpected end of file");
		std::vector<int> arr;
		stringToValue(line, arr, ",");
		if( arr.size() <= 0 ) return ERRORMSG("Error parsing line in C4.5 model: \"" << line << "\"");
		model->maxclass = arr[0];
		if( arr.size() <= 1 ) return ERRORMSG("Error parsing line in C4.5 model: \"" << line << "\"");
		model->count = arr[1];
		SpecialStatus = Calloc( char, model->count );
		model->maxatt = Malloc(DiscrValue, model->count);
		if( model->count != (arr.size()-2) && (model->count*2) != (arr.size()-2) ) return ERRORMSG("Invalid header 2");
		int i=2,j=0;
		for(;j<model->count;++i,++j) model->maxatt[j] = arr[i];
		if( i < int(arr.size()) ) model->perm = Malloc(unsigned int, model->count);
		else model->perm = 0;
		for(j=0;i<int(arr.size());++i,j++) model->perm[j] = arr[i];
		model->model = (Tree) malloc(sizeof(TreeRec));
		return c45_read_tree(in, model->model, model->maxclass);
	}
}


