/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * CoverTree.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_COVERTREE_H
#define _EXEGETE_COVERTREE_H
#include "Learner.h"
#include "ArgumentMap.h"
#include "CoverTreeUtil.h"
/** Defines the version of the cover tree algorithm. 
 * @todo add to group
 */
#define _COVERTREE_VERSION 101000

/** @file CoverTree.h
 * @brief Third-party CoverTree kNN classifier interface
 * 
 * This file contains the third-party CoverTree kNN classifier interface.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Third-party CoverTree kNN classifier interface
	 * 
	 * This class interfaces malibu with the third-party CoverTree
	 * k-nearest-neighbor algorithm.
	 *
	 * @todo shallow copy model
	 * @todo force single learner and test learner to have trainfile
	 */
	template<class Y, class W>
	class CoverTree : public Learner
	{
	public:
		/** Interface flags:
		 *	- Set attribute size to 8
		 * 	- A lazy learner
		 * 	- Use a training set
		 * 	- Start argument at 1
		 */
		enum{ ATTR8=1, LAZY=1, USE_TRAINSET=1, TUNE=1, ARGUMENT=1 };

	public:
		/** Defines a parent type. **/
		typedef void parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<knn_value_type, Y, W>					dataset_type;
		/** Defines testset type. **/
		typedef ExampleSet<knn_value_type, Y, std::string*>			testset_type;
		/** Defines a learnset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer		const_attribute_pointer;
		/** Defines a knn attribute value as a descriptor type. **/
		typedef knn_value_type										descriptor_type;
		/** Defines a float type. **/
		typedef float												float_type;
		/** Defines a tunable argument iterator. **/
		typedef typename Learner::argument_iterator					argument_iterator;
		/** Defines a weight type. **/
		typedef W													weight_type;
		/** Defines a prediction type. **/
		typedef float_type 											prediction_type;

	private:
		typedef TuneParameterTree							argument_type;
		typedef typename dataset_type::attribute_pointer	attribute_pointer;
		typedef typename argument_type::parameter_type		range_type;
		typedef typename dataset_type::value_type			example_type;
		typedef typename dataset_type::iterator				example_pointer;
		typedef v_array< example_type >						pred_value;
		typedef v_array< pred_value >						pred_type;
		typedef node< example_type >						model_type;

	public:
		/** Constructs a default cover tree.
		 */
		CoverTree() : Learner(SEMI|MISSING|NOMINAL|BINARY|NORMALIZED), kInt(10), 
		 			  kSel(kInt, "K", range_type(1,10,1,'+'))
		{
			init(nns, model);
		}
		/** Destructs a cover tree.
		 */
		~CoverTree()
		{
			erase(model);
			::erase(nns.elements);
		}

	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(CoverTree) );
				if(t>=0)
				arginit(map, kSel, "knn", "number of nearest neighbors");
				else
				arginit(map, kInt, "knn", "number of nearest neighbors");
			}			
		}
		
	public:
		/** Constructs a deep copy of the CoverTree model.
		 *
		 * @param tree a source CoverTree model.
		 */
		CoverTree(const CoverTree& tree) : Learner(tree), kInt(tree.kInt), kSel(kInt, tree.kSel)
		{
			copy_knn_model(model, tree.model);
		}
		/** Assigns a deep copy of the CoverTree model.
		 *
		 * @param tree a source CoverTree model.
		 * @return a reference to this object.
		 */
		CoverTree& operator=(const CoverTree& tree)
		{
			Learner::operator=(tree);
			argument_copy(tree);
			copy_knn_model(model, tree.model);
			return *this;
		}
		/** Makes a shallow copy of the cover tree.
		 *
		 * @param ref a reference to a cover tree.
		 */
		void shallow_copy(CoverTree& ref)
		{
			//Learner::operator=(ref);
			argument_copy(ref);
			model = ref.model;
			ref.model.p = 0;
			ref.model.max_dist = 0;
			ref.model.parent_dist = 0;
			ref.model.children = 0;
			ref.model.num_children = 0;
			ref.model.scale = 0;
		}
		/** Copies arguments in the cover tree.
		 * 
		 * @param ref a cover tree.
		 */
		void argument_copy(const CoverTree& ref)
		{
			kInt = ref.kInt;
			kSel = ref.kSel;
			Learner::argument_copy(ref);
		}
		/** Setups a concrete example set.
		 *
		 * @param dataset a concrete dataset.
		 * @param flag an unused parameter.
		 */
		template<class Y1>
		void setup(ConcreteExampleSet<knn_value_type,Y1>& dataset, bool flag=false)
		{
#ifndef _SPARSE
			dataset.attribute( 8 - dataset.attributeCount()%8 );
#endif
		}

	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.5f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			if( empty() ) return threshold();
			knn_predict(pred, model, nns, kInt);
			float_type avg = average(nns);
			for(int i=0;i<nns.index;++i) std::free(nns[i].elements);
			return avg;
		}
		/** Builds a model for the cover tree.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			erase(model);
			SET_POINT_LEN((int)learnset.attributeCount() + (8 - learnset.attributeCount()%8));
			pred_value set_of_points;
			assign(set_of_points, learnset.begin(), learnset.end());
			model = batch_create(set_of_points);
			reallocate(nns, kInt);
			return 0;
		}
		/** Get the class name of the learner.
		 * 
		 * @return CoverTree
		 */
		static std::string class_name()
		{
			return "CoverTree";
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return kNN
		 */
		static std::string name()
		{
			return "kNN";
		}
		/** Get the version of the classifier.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _COVERTREE_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return knn
		 */
		static std::string prefix()
		{
			return "knn";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return kSel;
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return model.num_children == 0;
		}
		/** Clear a cover tree model.
		 */
		void clear()
		{
			erase(model);
			::erase(nns.elements);
			nns.elements = 0;
			nns.index = 0;
			nns.length = 0;
		}
		/** Get the expected name of the program.
		 * 
		 * @return wknn or knn
		 */
		static std::string progname()
		{
#ifdef _WEIGHTED
			return "wknn";
#else
			return "knn";
#endif
		}
		/** Accept a vistor class (part of vistor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
		}
		
	protected:
		/** Calculates the average class value over nearest neighbors.
		 *
		 * @param nns nearest neighbor array.
		 * @return average class value.
		 */
		static float_type average(v_array< v_array< Example<knn_value_type,Y, void> > >& nns)
		{
			float_type tot=0.0f;
			for(unsigned int j=1;j<(unsigned int)nns[0].index;++j) tot+= nns[0][j].y();
			return tot / float_type( nns[0].index - 1 );
		}
		/** Calculates the weighted average class value over nearest neighbors.
		 *
		 * @param nns nearest neighbor array.
		 * @return weighted average class value.
		 */
		template<class W1>
		static float_type average(v_array< v_array< Example<knn_value_type,Y, W1> > >& nns)
		{
			float_type tot=0.0f;
			for(unsigned int j=1;j<(unsigned int)nns[0].index;++j) tot+= nns[0][j].y()*nns[0][j].w();
			return tot;
		}
		/** Finds the k-nearest neighbors for attribute vector and model.
		 *
		 * @param pred source attribute vector.
		 * @param model source model.
		 * @param nns destination neighbors.
		 * @param k number of nearest neighbors.
		 */
		static void knn_predict(const_attribute_pointer pred, const model_type& model, pred_type& nns, unsigned int k)
		{
			example_type ex = const_cast<attribute_pointer>(pred);
			model_type query = new_leaf(ex);
			nns.index=0;
			for(int i=0;i<nns.length;++i) nns[i].index = 0;
			k_nearest_neighbor(model, query, nns, k);
		}
		/** Resizes the temporary prediction vector.
		 *
		 * @param nns temporary prediction vector.
		 * @param len length of temporary prediction vector.
		 */
		static void reallocate(pred_type& nns, int len)
		{
			if( (len+1) > nns.length )
			{
				nns.length = len+1;
				nns.elements = ::resize(nns.elements, nns.length);
			}
		}
		/** Assigns a dataset to an internal dataset type.
		 *
		 * @param val internal dataset.
		 * @param beg an iterator to start of dataset.
		 * @param end an iterator to end of dataset.
		 */
		static void assign(pred_value& val, example_pointer beg, example_pointer end)
		{
			val.index = (int)std::distance(beg,end);
			val.length = (int)std::distance(beg,end);
			val.elements = beg;
		}
		/** Deallocate memory in the cover tree model.
		 *
		 * @param node a cover tree model node.
		 */
		static void erase(model_type& node)
		{
			for(unsigned int i=0;i<node.num_children;++i) erase(node.children[i]);
			if(node.num_children > 0)
			{
				free(node.children);
				node.children=0;
				node.num_children=0;
			}
		}
		/** Initializes the model values.
		 *
		 * @param p a temporary prediction vector.
		 * @param m a cover tree model.
		 */
		static void init(pred_type& p, model_type& m)
		{
			p.length = 0;
			p.index = 0;
			p.elements=0;
			m.num_children=0;
			m.children=0;
			m.p.x(0);
		}
		/** Makes a deep copy of one CoverTree model into another.
		 *
		 * @param model destination model.
		 * @param fromModel source model.
		 */
		static void copy_knn_model(model_type& model, const model_type& fromModel)
		{
			model.p = fromModel.p;
			model.max_dist = fromModel.max_dist;
			model.parent_dist = fromModel.parent_dist;
			model.num_children = fromModel.num_children;
			model.scale = fromModel.scale;
			model.children = 0;
			model.children = (model_type *)realloc(model.children, fromModel.num_children);
			for(int i=0;i<fromModel.num_children;++i) copy_knn_model(model.children[i], fromModel.children[i]);
		}

	public:
		/** Reads a cover tree from the input stream.
		 *
		 * @param in an input stream.
		 * @param tree a cover tree.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, CoverTree& tree)
		{
			in >> tree.kInt;
			return in;
		}
		/** Writes a cover tree to the output stream.
		 *
		 * @param out an output stream.
		 * @param tree a cover tree.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const CoverTree& tree)
		{
			out << tree.kInt;
			return out;
		}

	private:
		unsigned int kInt;
		argument_type kSel;

	private:
		mutable pred_type nns;
		model_type model;
	};
};

/** @brief Type utility interface to a CoverTree
 * 
 * This class defines a type utility interface to a CoverTree.
 */
template<class Y, class W>
struct TypeUtil< ::exegete::CoverTree<Y,W> >
{
	/** Flags a cover tree as non primative.
	 */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::CoverTree<Y,W> value_type;
	/** Tests if a string can be converted to a cover tree.
	 *
	 * @param str a string to test
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return CoverTree
	*/
	static const char* name() 
	{
		return "CoverTree";
	}
};


#endif


