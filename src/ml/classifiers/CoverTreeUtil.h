/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * CoverTreeUtil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_COVERTREEUTIL_H
#define _EXEGETE_COVERTREEUTIL_H
#include "AttributeTypeUtil.h"
#include "typeutil.h"


#ifdef _SPARSE
#include "sparse_vector.h"
/** Defines a single node as a knn value type. **/
typedef single_node knn_value_type;
/** Defines a macro for setting a point length.
 * 
 * @note does nothing
 * 
 * @param x a point length.
 */
#define SET_POINT_LEN(x) ((void)0)
#else
#include "knnvector.h"
/** Defines a float as a knn value type. **/
typedef float knn_value_type;
/** Defines a point length. **/
extern int point_len;
/** Defines a macro for setting a point length.
 * 
 * @param x a point length.
 */
#define SET_POINT_LEN(x) point_len = x
#endif
#include "cover_tree.h"



/** @file CoverTreeUtil.h
 * @brief Utility classes for the cover tree
 * 
 * This file contains utility classes for the cover tree.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

#ifdef _SPARSE
	/** @brief Specializes AttributeUtil for a single node
	 * 
	 * This class represents an attribute type corresponding to a column in
	 * the dataset. It specializes the class template to a single node.
	 * 
	 * @ingroup ExegeteDataset
	 */
	template<>
	class AttributeUtil< single_node >
	{
	public:
		/** Flags a single node as sparse. **/
		enum{IS_SPARSE=true};
	public:
		/** Defines a limit type. **/
		typedef TypeUtil<double>		limit_type;
		/** Defines a double as a value type. **/
		typedef double					value_type;
		/** Defines a single_node an attribute type. **/
		typedef single_node				attribute_type;
		/** Define pointer to attribute type as pointer. **/
		typedef attribute_type*			pointer;
		/** Defines a constant pointer to attribute type as a constant pointer. **/
		typedef const attribute_type*	const_pointer;
		/** Defines an int as an index type. **/
		typedef int						index_type;
		/** Defines a size_t as a size type. **/
		typedef size_t					size_type;
		/** Defines a pointer difference as a difference type. */
		typedef ptrdiff_t				difference_type;

	public:
		/** Converts an attribute to a value.
		 *
		 * @param attr an attribute type.
		 * @return a value type.
		 */
		static const value_type& valueOf(const attribute_type& attr)
		{
			return attr.value;
		}
		/** Converts an attribute to a value.
		 *
		 * @param attr an attribute type.
		 * @return an attribute value.
		 */
		static value_type& valueOf(attribute_type& attr)
		{
			return attr.value;
		}
		/** Converts an attribute to an index.
		 *
		 * @param attr an attribute type.
		 * @return index type.
		 */
		static index_type indexOf(const attribute_type& attr)
		{
			return attr.index;
		}
		/** Converts an attribute to an index.
		 *
		 * @param attr an attribute type.
		 * @return single_node index.
		 */
		static index_type& indexOf(attribute_type& attr)
		{
			return attr.index;
		}
		/** Flags the specified attribute as the last.
		 * 	- Increment the index reference.
		 *  - Assign attribute index to -1.
		 *
		 * @param px a pointer to attributes.
		 * @param n index of attribute.
		 */
		static void marklast(pointer px, size_type& n)
		{
			++n;
			px[n].index = -1;
		}
		/** Tests if attribute is last sparse attribute.
		 *
		 * @param attr a reference to an attribute type.
		 * @return true if attribute index is last.
		 */
		static bool islast(const attribute_type& attr)
		{
			return attr.index == -1;
		}
		/** Gets the internal value of a missing attribute.
		 * 
		 * @return maximum value of double.
		 */
		static value_type missing()
		{
			return limit_type::max();
		}
	};
#endif

};
#ifdef _SPARSE

/** @brief Type utility interface to a CoverTree single node.
 * 
 * This class defines a type utility interface to a CoverTree single node.
 */
template< >
struct TypeUtil< single_node >
{
	/** Flags single node as primative. */
	enum{ ispod=true };
	/** Defines a value type. **/
	typedef single_node value_type;
	/** Tests if a string can be converted to a single node.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return single node
	*/
	static const char* name() 
	{
		return "single_node";
	}
};
#endif


/** @brief Type utility interface to a CoverTree v_array.
 * 
 * This class defines a type utility interface to a CoverTree v_array.
 */
template<class T>
struct TypeUtil< v_array< T > >
{
	/** Flags v_array as primative. */
	enum{ ispod=true };
	/** Defines a value type. **/
	typedef T value_type;
	/** Tests if a string can be converted to a v_array.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return v_array
	*/
	static const char* name() 
	{
		return "v_array";
	}
};


#endif


