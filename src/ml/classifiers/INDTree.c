/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * INDTree.c
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

/** @file INDTree.c
 * @brief Contains the implementation of the IND tree interface functions.
 * 
 * This source file contains implementation of the IND tree interface functions.
 *
 * @todo add io functions
 * 
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#include "exegete_vs.h"
char *progname="ind";
int	depth = 0;
int Multiple_trees = 0;

#if defined(_WIN32) && !defined(__MINGW32__)
double lgamma(double val)
{
}
long random()
{
	long genrand_int31();
	return genrand_int31();
}
void srandom(long s)
{
	void init_genrand(long);
	init_genrand(s);
}
double drand48()
{
	double genrand_real1();
	return genrand_real1();
}
void srand48(long seed)
{
	init_genrand(seed);
}
//char* strdup(char* str)
//{
//	char* tmp = (char*)malloc(strlen(str));
//	strcpy(tmp,str);
//	return tmp;
//}
#endif
#ifdef _IND


#define TRUE	1
#define FALSE	0
#define	bool	unsigned char
#define c45_default  0.25
#define  uerror(s1,s2)   error_LF(s1,s2,0,"")
#define  error(s1,s2)   error_LF(s1,s2,__LINE__,__FILE__)

#ifndef	FLOATMAX
#define FLOATMAX  1.7e38
#endif

#include "SYM.h"
#include "sets.h"

#include "DEFN.h"
#include "TREE.h"
#include <stdio.h>
#include <assert.h>

float prune_factor = 1.0;
long CC_seed = 0;
bool CC_test_flg = FALSE;
int	CV_fold = 0;
float CC_test_prop = 0.0f;
//prune
char pruner = '0';
char converter = '0';
bool Dflg = FALSE;
bool noprun = FALSE;
float prune_factor1 = 0.999;
float prune_factor2 = 0.01;
int max_opts;
bool CC_CV_flg = FALSE;
char *testfile = 0;//probably not be supported
//predict
bool mcflg = FALSE;   /* use most common when making decision */
int	ntrees = 1;
//double *weights=0; 	/* (unnormalised) weight for tree */

void error_LF(char *s1, char *s2, int line, char *file);
void prior_opts(int c, char *optarg);
void tree_opts(int c, char *optarg);
void make_opts(int c, char *optarg);
void dgraph_opts(int c, char *optarg);
void choose_opts(int c, char *optarg);
double *make_dvec(int dim);
void safree(void * tt);

void parse_predict_options(int c, char* optarg)
{
	switch (c)
	{
	case 'o':
		mcflg++;
		break;
	case 'Z':
	case 'U':
		tree_opts(c,optarg);
		break;
	};
}

void parse_prune_options(int c, char* optarg)
{
	extern int depth_bound;
	long seed=0;
	switch (c)
	{
	case 's':
		if ( sscanf(optarg," %ld", &seed) <= 0 ) uerror("incorrect option argument (can't read long)", "");
		srandom(seed);
	case 'A':
	case 'P':
		prior_opts(c,optarg);
	//case 'V':
	//	pruner = c;
	//	testfile = optarg;
	//	break;
	case 'l':
		noprun = TRUE;
		break;
	case 'n':
	case 'b':
	case 'r':
		converter = c;
		break;
	case 'B':
		converter = 'B';
		break;
	case 'D':
		Dflg++;
		break;
	case 'd':
		if ( sscanf(optarg," %d", &depth_bound) <= 0 ) uerror("incorrect option argument (can't read integer)","");
		break;
	case 'c':
		if ( sscanf(optarg," %f", &prune_factor) <= 0 ) uerror("incorrect option argument (can't read float)","");
	case 'f':
	case 'e':
	case 'M':
		pruner = c;
		break;
	case 'p':
		if ( converter=='b'  ) 
		{
			if ( sscanf(optarg," %f", &prune_factor1) <= 0 ) uerror("incorrect option argument (can't read float)","");
		} 
		else 
		{
			if ( sscanf(optarg," %f", &prune_factor) <= 0 ) uerror("incorrect option argument (can't read float)","");
			if ( pruner = 'f' ) prune_factor /= c45_default;
		}
		break;
	case 'q':
		if ( sscanf(optarg," %f", &prune_factor2) <= 0 ) uerror("incorrect option argument (can't read float)","");
		break;
	case 'o':
	    if ( sscanf(optarg," %d", &max_opts) <= 0 ) uerror("incorrect option argument (can't read integer)","");
		break;
	//case 'x':
	//    if ( sscanf(optarg," %d", &Multiple_trees) <= 0 ) uerror("incorrect option argument (can't read integer)","");
	//	if (Multiple_trees < 2) error ("%s: incorrect argument for flag 'x'\n", progname);
	//	break;
	};
}

void parse_train_options(int c, char* optarg)
{
	switch (c)
	{
	case 'U':
		tree_opts(c,optarg);
		break;
	case 's':
//	case 'o':
	case 'O':
	case 'B':
	case 'K':
	case 'J':
	case 'L':
		make_opts(c,optarg);
		break;
#ifdef GRAPH
	case 'I':
	case 'w':
	case 'x':
	case 'b':
	case 'm':
	case 'D':
		dgraph_opts(c,optarg);
		break;
#endif
    case 'X':
	case 'Y':
	case 'G':
	case 'i':
	case 'n':
	case 't':
	case 'M':
	case 'F':
	case 'S':
		choose_opts(c,optarg);
		break;
	case 'A':
	case 'P':
	case 'N':
	case 'd':
		prior_opts(c,optarg);
		break;
	case 'p':
        if ( sscanf(optarg," %f", &prune_factor) <= 0 ) uerror("incorrect option argument (can't read float)","");
		break;
	case 'c':
		CC_test_flg++;
		if ( sscanf(optarg,"%f,%ld", &CC_test_prop, &CC_seed) <= 0 ) uerror("incorrect option argument (can't read float and seed)","");
		break;
	case 'C':
		CC_CV_flg++;
		if ( sscanf(optarg,"%d,%ld", &CV_fold, &CC_seed) <= 0 ) uerror("incorrect option argument (can't read float and seed)","");
		break;
	default:
		assert(FALSE);
	};
}


void argument_getopts(int argc, char* argv[], int t)
{
	extern	int	optind;
	extern	char	*optarg;
	int c;

	optind=0;
	if( t == 0 )
	{
		while ((c = getopt(argc, argv, "OevNS:s:aC:c:d:i:P:n:op:tU:A:B:Mp:r:FfJ:L:X:Y:xIwbmK:G:D")) != EOF) parse_train_options(c, optarg);
	}
	else if( t == 1 )
	{
		while ((c = getopt(argc, argv, "lvSNDnbBd:efV:p:c:A:E:P:lq:o:Mx:rs:")) != EOF) parse_prune_options(c, optarg);
	}
	else if( t == 2 )
	{
		while ((c = getopt(argc, argv, "QwcDdqPpsSU:m:bW:toeZlXvgGfx:")) != EOF) parse_predict_options(c, optarg);
	}
}
int commandToArray(char* cmd, char* argv[])
{
	int i=0;
	if( cmd == 0 ) return 0;
	for(;*cmd != 0;++i)
	{
		while( *cmd != 0 && isspace(*cmd) ) ++cmd;
		argv[i] = cmd;
		while( *cmd != 0 && !isspace(*cmd) ) ++cmd;
		if( *cmd != 0 )
		{
			*cmd = 0;
			++cmd;
		}
	}
	return i;
}
void argument_opts(const char* trn, const char* prn, const char* prd)
{
	char* argv[2048];
	int argc=1;
	char* buffer;
	argv[0] = "main";
	buffer = strdup(trn);
	argc = commandToArray(buffer, argv+1)+1;
	argument_getopts(argc, argv, 0);
	free(buffer);buffer = strdup(prn);
	argc = commandToArray(buffer, argv+1)+1;
	argument_getopts(argc, argv, 1);
	free(buffer);buffer = strdup(prd);
	argc = commandToArray(buffer, argv+1)+1;
	argument_getopts(argc, argv, 2);
	free(buffer);
}

void init_choose_opts()
{
	/** Sets the nominal subsetting flags. **/
	extern unsigned char allsub;
	//1. Subsetting one
	//2. Subsetting full
	//3. Subsetting rest
	/** Maximum number of cuts saved per attribute. **/
	extern int max_cuts;//MULTI_CUTS
	/** Tree node purity flag. **/
	extern char purflg;
	//1. i: Information gain
	//2. t: Twoing
	//3. r: Information gain ratio
	//4. g: Gini criterion
	/** Use transduction criterion and Bayesian rule. **/
	extern bool t_flg;
	/** Probability prepruning threshold. **/
	extern double pre_prob_prune;
	/** Subsample examples if more than subsample size. **/
	extern int subsample_size;
	/** Use random assignment of purity test and gain. **/
	extern bool Fflg;//seed random number generator

	allsub = 00;
	max_cuts = 1;
	purflg = 'i';
	t_flg = TRUE;
	pre_prob_prune = -FLOATMAX*0.9;
	subsample_size = 1000;
	Fflg = FALSE;
}

void init_dgraph_opts()
{
	/** Should build decision graph requires stochastic growth. **/
	extern  bool    decision_graph_flag;
	/** Should build graph with stochastic growth otherwise an option tree. **/
	extern int Stochastic_Growth;
	/** Grow graph with inference. **/
	extern int Inference_flag;
	/** Grow graph with wandering. **/
	extern int Wander_flag;
	/** Search for a smooth graph. **/
	extern int Search_for_smooth;
	/** Store only modifications to graph. **/
	extern int Store_modifications_only;
	/** Output multiple trees. **/
	//extern int Multiple_trees;

	Stochastic_Growth = 0;
	Inference_flag = 0;
	Wander_flag = 0;
	Search_for_smooth = 0;
	Store_modifications_only = 0;
	decision_graph_flag = FALSE;
	Multiple_trees = 0;
}

void init_tree_opts()
{
	/** Use an alternative method to handle zero nodes. **/
	extern bool	Zflg;
	/** Method to handeling unknown attributes: 
		1. Send down proportionally
		2. Send down all paths
		3. Send down most common path
		4. Send down with probability
		5. Send down evenly
		6. Ignore examples with missing values
	 */
	extern char	Uflg;


	Zflg = FALSE;
	Uflg = 1;
}
	
void init_make_opts()
{	
	/** Lookahead smoothes rather than maximises. **/
	extern bool max_not_ave;
	/** Override flag **/
	extern bool	oflg;// Do not include this option.
	/** Stop growing if set at node is less than min set size. **/
	extern int min_set_size;
	/** Stop growing if sets at split is less than min set split. **/
	extern float min_set_split;
	/** Make an option tree. **/
	extern bool jtree_flag;
	/** Stop growing if best within factor of the total subtrees. **/
	extern double grow_add_fact;
	/** Only grow subtrees within leaf probability of best. **/
	extern double grow_leaf_fact;
	/** Number of subtrees to grow after lookahead. **/
	extern int grow_breadth;
	/** Only grow subtrees within look ahead probability of best. **/
	extern double grow_fact;
	/** Only keep those subtrees within a factor of the best. **/
	extern double post_fact;
	/** Keep around a number of subtrees at each node. **/
	extern int post_breadth;
	/** Place those tree within lookahead probability on heap and grow if time/space permits. **/
	extern double incg_fact;
	/** On choice of tests include within factor of best. **/
	extern double choice_fact;
	/** On choice of tests include the best few. **/
	extern double choice_breadth;
	/** Lookahead include tests within factor of best. **/
	extern double look_fact;
	/** Lookahead include the best few tests. **/
	extern double look_breadth;
	/** Lookahead go to this depth max. **/
	extern double look_depth;
	/** Do incremental growing from heap. **/
	extern bool incg_flag;

	max_not_ave = TRUE;      
	oflg = FALSE;
	min_set_size = 1;	
	min_set_split = 0.5;
	jtree_flag = FALSE;
	grow_add_fact = 0.75;
	grow_leaf_fact = 0.00001;
	grow_breadth = 1;      
	grow_fact = 0.005; 
	post_fact = 0.005;
	post_breadth = 1;
	incg_fact = 0.9;
	incg_flag = TRUE;
	choice_fact = 0.00001;
	choice_breadth = 1;
	look_breadth = 1;
	look_depth = 1;
	look_fact= 0.00001;
}


void init_prior_opts()
{	
	/** Depth bound for pruning. **/
	extern int depth_bound;
	/** Normalize prior. **/
	extern bool nmlz;
	/** Test if prior flag already set. **/
	extern bool prior_flag_set;
	/** Flags specifiying the type of prior. **/
	//extern prior_flags	 prior_flag;
	/** First parameter for beta priors. **/
	extern float palphaval;
	/** Prior weight for a leaf. **/
	extern float l_weight;
	/** Prior weight for a node. **/
	extern float n_weight;
	/** Wallace-Oliver prior probability of joining nodes. **/
	extern double p_join;

	depth_bound = 200;
	nmlz = FALSE;
	prior_flag_set = FALSE;
	n_weight = 0.0;
	l_weight = 0.0;
	p_join = 0.0;
	palphaval = 1.0;
}

void init_opts()
{
	init_prior_opts();
	init_tree_opts();
	init_make_opts();
	init_dgraph_opts();
	init_choose_opts();

	depth = 0;
	prune_factor = 1.0;
	CC_seed = 0;
	CC_test_flg = FALSE;
	CV_fold = 0;
	CC_test_prop = 0.0f;
	//prune
	pruner = '0';
	converter = '0';
	Dflg = FALSE;
	noprun = FALSE;
	prune_factor1 = 0.999;
	prune_factor2 = 0.01;
	max_opts = 0;
	CC_CV_flg = FALSE;
	testfile = 0;
	//predict
	//wflg = FALSE;
	mcflg = FALSE;
	//tflg = FALSE;
	ntrees = 1;
	//if(weights != 0 ) safree(weights);weights=0;
	//Gflg = FALSE;
}
/*****************************************************************************************/
/*****************************************************************************************/
/*****************************************************************************************/
/*****************************************************************************************/
	
struct TreeModel
{
	t_head thead;
	ot_ptr treePtr;
};
typedef struct TreeModel mode_type;
typedef mode_type* model_pointer;

egset *egs = 0;
void install_prior();
void train_model(model_pointer model)
{
	static int init_gain_again = 0;
	void free_gainstore();
	void free_table();
	void init_gainstore();
	float CC_CV_grow();
	void init_table();

	init_table();
	tree = model->treePtr = new_node((bt_ptr)0);
	tree->xtra.gr.egs = egs;
	add_counts(tree);
	install_prior((t_prior *)0,tree->eg_count);
	if( init_gain_again == 1 ) init_gainstore();
	else init_gain_again = 1;
	if ( CC_test_flg )
	{
		CC_test_grow(tree, egs, CC_test_prop, prune_factor, CC_seed);
	}
	else if( CC_CV_flg )
	{
		model->thead.sprob = CC_CV_grow(tree, egs, CV_fold, prune_factor, CC_seed, 0);
	}
	else maketree(tree, egs);
	load_prior(&model->thead.prior);
	free_gainstore();
	free_table();
}
//egtesttype   testing;
void ind_train(void** model)
{
	void erase_model();
	model_pointer tmp = *(model_pointer*)model;
	if( tmp != 0 ) erase_model(tmp);
	tmp = salloc( sizeof( mode_type ) );
	train_model(tmp);
	(*(model_pointer*)model) = tmp;
}
void prune_model(model_pointer model)
{
	void c45_prune();
	void pess_prune();
	void tree_reprob();
	/** Depth bound for pruning. **/
	extern int depth_bound;
	double   tree_smpl();
	egtesttype  testing;
	tree = model->treePtr;
	install_prior(&model->thead.prior, tree->eg_count);
	if ( depth_bound >= 0 ) depth_prune(tree, depth_bound);
	if ( thprobs(&model->thead) )
	{
		if ( Dflg ) 
		{
			if ( thbayes(&model->thead) ) 
			{
	#ifdef GRAPH
			  if ( thgraph(&model->thead) ) graph_smooth(tree,Dflg);
			  else
	#endif
				tree_smooth(tree,Dflg);
				unset_flag(model->thead.hflags,bayes);
			} 
			else dec_prune(tree);	
		}
		return;
	}
	if ( (thoptions(&model->thead) || thgraph(&model->thead)) && strchr("fMecV", pruner) ) uerror("is option tree or graph, can't prune!","");

	//void CC_test_prune();
	/*
	 *	first do the pruning
	 */
	switch(pruner)
	{
	case 'f':
		c45_prune(tree, prune_factor*c45_default);
		break;
	case 'M':
		mincost_prune(tree);
		break;
	case 'e':
		pess_prune(tree, prune_factor);
		break;
	case 'c':
		CC_costprune(tree, prune_factor);
		break;
	default:
		break;
	}
	/*
	 * 	now convert to probs
	 */
	switch(converter)
	{
	case 'n':
		tree_reprob(tree);
		set_flag(model->thead.hflags,probs);
		break;
	case 'r':
	case 'b':
		set_flag(model->thead.hflags,bayes);
#ifdef GRAPH
		if (decision_graph_flag && is_dgraph(tree)) 
		{
				tree->testing = init_test();
	
				model->thead.sprob =  model->thead.prior.prior_nml - message_length(tree, 1, 0);
				graph_avet(tree,prune_factor2, max_opts, noprun);
		} else {
#endif
			testing = init_test();
			model->thead.eprob = log_beta(tree, tree->eg_count); 
			model->thead.sprob =  model->thead.prior.prior_nml + tree_avet(tree,prune_factor2, max_opts, noprun,testing);
			sfree(testing.unordp);
			if ( converter=='r' ) model->thead.sprob += log(tree_smpl(tree));
#ifdef GRAPH
		}
#endif
		set_flag(model->thead.hflags,probs);
		break;
	case 'B':
		testing = init_test();
		model->thead.eprob = log_beta(tree, tree->eg_count); 
		model->thead.sprob =  model->thead.prior.prior_nml + tree_maxt(tree,prune_factor1,prune_factor2,testing);
		set_flag(model->thead.hflags,probs);
		sfree(testing.unordp);
		break;
	default:
		break;
	}
}

void ind_prune(void** model)
{
	prune_model( *(model_pointer*)model );
}

void erase_model(model_pointer tmp)
{
	void free_tree();
	void free_stored_trees();
	if( tmp != 0 && tmp->treePtr != 0 )
	{
		if(tmp->treePtr != 0) tmp->treePtr->xtra.gr.egs=0;
#ifdef GRAPH
		free_stored_trees();
#endif
		if(tmp->treePtr != 0) free_tree(tmp->treePtr);
	}
	if( tmp != 0 ) sfree(tmp);
}

void ind_erase(void** model)
{
	if( (*(model_pointer*)model) != 0 ) 
		erase_model( *(model_pointer*)model );
	*(model_pointer*)model = 0;
}
	
egtype tmp_eg;
int oldegsize=0;
float *d = 0, **dd = 0;
void ind_copy(const float* from, egtype* eg);
float ind_predict(void** vmodel, const float* attr)
{
	model_pointer model = *(model_pointer*)vmodel;
	int j, index;
	tree = model->treePtr;
	if( d == 0 ) d = mems_alloc(float, ndecs);
	if( dd == 0 ) dd = mems_alloc(float*, ntrees);
	if( oldegsize != egsize )
	{
		oldegsize = egsize;
		tmp_eg.unordp = (unordtype *)salloc (egsize);
	}
	ind_copy(attr, &tmp_eg);
	for (j = 0; j < ndecs; j++) d[j] = 0.0;
	if(thbayes(&model->thead)) dd[0] = tree_class_ave(tree, tmp_eg);
	else dd[0] = tree_class(tree, tmp_eg);
	for (j = 0; j < ndecs; j++) d[j] += dd[0][j];// * weights[0];
	index = mcflg ? most_common(d) : best_dec(d);
	sfree(dd[0]);
	return ((index==0)?-1:1)*d[index];
}


void ind_copy_model(void** from, void** to)
{
	model_pointer model = *(model_pointer*)from;
	if( to != 0 ) ind_erase(to);
	(*(model_pointer*)to)->treePtr = copy_otree(model->treePtr);
	(*(model_pointer*)to)->thead = model->thead;
}

#include "TABLE.h"
extern ot_rec dummyt;
extern egtesttype testing;
void init_table()
{
	extern ftbl tbl;
	tbl.maskarray = 0;
	tbl.unknown.d = 0;
	tbl.known = 0;
	dummyt.parents = 0;
	dummyt.eg_count = 0;
	testing.unordp=0;
}
void free_table()
{
	extern ftbl tbl;
	if( tbl.maskarray != 0 ) 
	{
		sfree( tbl.maskarray-2 );
		tbl.maskarray = 0;
	}
	if( tbl.unknown.d != 0 ) 
	{
		sfree( tbl.unknown.d );
		tbl.unknown.d = 0;
	}
	if( tbl.known )
	{
		if( tbl.known[0].d ) 
		{
			sfree( tbl.known[0].d );
			tbl.known[0].d = 0;
		}
		sfree(tbl.known);
		tbl.known = 0;
	}
	sfree(dummyt.parents);
	dummyt.parents = 0;
	sfree(dummyt.eg_count);
	dummyt.eg_count = 0;
	sfree(testing.unordp);
	testing.unordp=0;
}


#endif


#include <stdio.h>
int parse_symbols(FILE *fp)//dummy function
{
	return 0;
}



