/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * INDTree.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_IND_H
#define _EXEGETE_IND_H
#include "Learner.h"
#include "INDTreeUtil.h"
#include "Option.h"
/** Defines the version of the IND tree algorithm. 
 * @todo add to group
 */
#define _IND_VERSION 101000

/** @file INDTree.h
 * @brief Third-party IND tre classifier interface
 * 
 * This file contains the INDTree class template.
 *
 * The third-party software as released on the following agreement:
 *	http://opensource.arc.nasa.gov/software/ind/nosa/
 *
 * This software has been modified to with this adapter (minorly) and
 * to compile under windows (less minorly). Compile does not mean work.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

	/** @brief Third-party IND tree classifier interface
	 * 
	 * The class defines a classifier that builds an IND model.
	 *
	 * @todo add shallow copy
	 * @todo add parameter copy
	 * @todo add ind style dataset type to malibu
	 *
	 * @todo add ind set handling
	 * @todo free all ind static memory
	 * @todo track IND changes
	 * @todo make random forests
	 * @todo make lgamma work under windows
	 *
	 * @todo legal: 1. change log 2. under this lisence
	 *
	 */
	class INDTree : public Learner
	{
	public:
		/** Interface flags:
		 *	- Do not set attribute size to 8
		 * 	- Not a lazy learner
		 * 	- Do not use a training set
		 * 	- Start argument at 1
		 */
		enum{ ATTR8=0, LAZY=0, USE_TRAINSET=0, ARGUMENT=1 };

	public:
		/** Defines a parent type. **/
		typedef void parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<float, int, void>						dataset_type;
		/** Defines a testset type. **/
		typedef ExampleSet<float, int, std::string*>				testset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef dataset_type::const_attribute_pointer				const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef float												descriptor_type;
		/** Defines a float type. **/
		typedef double												float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator							argument_iterator;
		/** Defines a weight type. **/
		typedef void												weight_type;
		/** Defines a prediction type. **/
		typedef float_type 											prediction_type;
	private:
		typedef dataset_type::size_type		size_type;
		typedef TuneParameterTree				argument_type;
		typedef argument_type::parameter_type	range_type;
		typedef OptionSet						option_set;
		typedef void*							model_type;

	public:
		/** Constructs a default IND tree.
		 */
		INDTree() : Learner(SEMI|BINARY), model(0), treeInt(1),
		   			treeSel(treeInt, "Style", range_type(1, 2, 13, '+'))//MISSING|NOMINAL|
		{
		}
		/** Destructs an IND classifier and deallocates a IND model.
		 */
		~INDTree()
		{
			if( model != 0 ) ind_erase(&model);
		}
		
	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(INDTree));
#ifdef GRAPH
				if(t>=0)
				arginit(map, treeSel,								"style",			"IND tree style>manual:0;cart:1;cart0:2;id3:3;c4:4;mml:5;smml:6;bayes:7;c4laplace:8;c4bayes:9;mmllaplace:10;look:11;opt:12;opt1:13;dgraph:14;dgraphwan:15;treewan:16;*:17");
				else
				arginit(map, treeInt,								"style",			"IND tree style>manual:0;cart:1;cart0:2;id3:3;c4:4;mml:5;smml:6;bayes:7;c4laplace:8;c4bayes:9;mmllaplace:10;look:11;opt:12;opt1:13;dgraph:14;dgraphwan:15;treewan:16");
#else
				if(t>=0)
				arginit(map, treeSel,								"style",			"IND tree style>manual:0;cart:1;cart0:2;id3:3;c4:4;mml:5;smml:6;bayes:7;c4laplace:8;c4bayes:9;mmllaplace:10;look:11;opt:12;opt1:13;*:14");
				else
				arginit(map, treeInt,								"style",			"IND tree style>manual:0;cart:1;cart0:2;id3:3;c4:4;mml:5;smml:6;bayes:7;c4laplace:8;c4bayes:9;mmllaplace:10;look:11;opt:12;opt1:13");
#endif
				init(map);
			}
		}

	private:
		template<class U>
		void init(U& map)
		{
			arginit(map, "-Growing", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('p', "1.0"),					"pfactor",			"prune factor", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('c', "0.0,0"),				"ptest",			"prune using a test file: test set proportion and random seed", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('C', "0,0"),					"pcross",			"prune using cross-validation: number of folds and random seed", ArgumentMap::ADVANCED);
			arginit(map, "-Option Tree", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('s', "1,0.5"),				"minset",			"stop growing if size drops below $1 or some percentage $2", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"looksmooth",		"lookahead smoothes rather than maximises>Yes:0;No:O", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('B', "1,1,0.00001,1,0.00001"),"lookahead",		"allow up to $1 options within a factor of $2 best; beam-search until depth $3 and only explore upto $4 tests within probability $5", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('K', "1,0.005"),				"postgrow",			"keep upto $1 subtrees at each node; only within $2 factor of best", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('J', "1,0.005,0.75,0.00001"),	"postlook",			"grow $1 trees after lookahead; only grow those with lookahead probability within $2 of best and within factor $4 of leaf; stop growing if best within $3", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('L', "0.9"),					"postadd",			"if time permits add subtrees within this factor", ArgumentMap::ADVANCED);
			arginit(map, "-Prior", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('A', "1.0"),					"aprior",			"alpha (prior weight for symmetric Dirichlet) e.g. type,alpha (string,float): nonsym, classes, null, <nothing>", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('P', "1.0"),					"wprior",			"oliver-wallace prior: null,mml,nochoices,choices,weight where mml,%f; weight,%f,%f", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"priornorm",		"normalize prior>Yes:N;No:0", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('d', "200"),					"maxdepth",			"maximum depth of tree", ArgumentMap::ADVANCED);
			arginit(map, "-Choose", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('S', "one"),					"subsetting",		"nominal subsetting type>one:one;full:full;rest:rest", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('Y', "0"),					"multicut",			"number of splits over the same attribute", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('i', "i"),					"purity",			"node purity>Information Gain:i;Twoing:t;Gain Ratio:r;Gini:g", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"bayessplit",		"use bayesian splitting>Yes:t;No:0", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('n', "1e100"),				"preprune",			"pre-probability pruning", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('G', "1000"),					"samplesize",		"subsample size", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"randsplit",		"choose a random split type>Yes:F;No:0", ArgumentMap::ADVANCED);
#ifdef GRAPH
			arginit(map, "-Decision Graph", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"inference",		"grow graph with inference>Yes:I;No:0", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"wander",			"grow graph with wandering>Yes:w;No:0", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"multiple",			"grow graph with multiple trees>Yes:x;No:0", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"smoothsearch",		"grow graph while searching for smooth>Yes:b;No:0", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('2', "0"),					"modifications",	"store modifications>Yes:m;No:0", ArgumentMap::ADVANCED);
			arginit(map, trn_opts('1', "None"),					"dgraph",			"type of grow graph>None:0;Stochastic:D;Decision:DD", ArgumentMap::ADVANCED);
#endif
			arginit(map, "IND Prune", ArgumentMap::ADVANCED);
			arginit(map, prn_opts('2', "0"),					"noprune",			"do not perform pruning>Yes:l;No:0", ArgumentMap::ADVANCED);
			arginit(map, prn_opts('p', "0.999"),				"pfactor1",			"prune factor 1", ArgumentMap::ADVANCED);
			arginit(map, prn_opts('q', "0.01"),					"pfactor2",			"prune factor 2", ArgumentMap::ADVANCED);
			arginit(map, prn_opts('o', "0"),					"maxopts",			"maximum number of options to consider", ArgumentMap::ADVANCED);
			arginit(map, prn_opts('2', "0"),					"smooth",			"smooth probabilities after pruning>Yes:D;No:0", ArgumentMap::ADVANCED);
			arginit(map, prn_opts('0', "0"),					"converter",		"converts the leaf probabilities>Laplacian:n;Bayesian:b;Log:r;MML:B;None:0", ArgumentMap::ADVANCED);
			arginit(map, prn_opts('0', "0"),					"pruner",			"type of pruning method>C4.5:f;Cost Complexity:c;Pessimistic:e;Minimum Cost:M;None:0", ArgumentMap::ADVANCED);

			arginit(map, "IND Predict", ArgumentMap::ADVANCED);
			arginit(map, prd_opts('2', "0"),					"mostcommon",		"use mostcommon instead of best in prediction>Yes:o;No:0", ArgumentMap::ADVANCED);
			arginit(map, prd_opts('2', "0"),					"zeronode",			"use alternative zero nodes>Yes:Z;No:0", ArgumentMap::ADVANCED);
			arginit(map, prd_opts('U', "Proportionally"),		"unknown",			"method to handle unknown values>Proportionally:1;All:2;Common:3;Probability:4;Evenly:5;Ignore:6", ArgumentMap::ADVANCED);
		}

	public:
		/** Constructs a deep copy of the IND tree model.
		 *
		 * @param tree a source IND tree model.
		 */
		INDTree(const INDTree& tree) : Learner(tree), model(0), treeInt(tree.treeInt), trn_opts(tree.trn_opts), prn_opts(tree.prn_opts), prd_opts(tree.prd_opts), 
			treeSel(treeInt, tree.treeSel)
		{
			if( tree.model != 0 ) ind_copy_model(&tree.model, &model);
		}
		/** Assigns a deep copy of the IND tree model.
		 *
		 * @param tree a source IND tree model.
		 * @return a reference to this object.
		 */
		INDTree& operator=(const INDTree& tree)
		{
			if( tree.model != 0 ) ind_copy_model(&tree.model, &model);
			argument_copy(tree);
			return *this;
		}
		/** Makes a shallow copy of an IND tree.
		 *
		 * @param ref a reference to an IND tree.
		 */
		void shallow_copy(INDTree& ref)
		{
			model = ref.model;
			argument_copy(ref);
			ref.model=0;
		}
		/** Copies arguments in an IND tree.
		 * 
		 * @param ref an IND tree.
		 */
		void argument_copy(const INDTree& ref)
		{
			treeInt = ref.treeInt;
			trn_opts = ref.trn_opts;
			prn_opts = ref.prn_opts;
			prd_opts = ref.prd_opts;
			treeSel = ref.treeSel;
			Learner::argument_copy(ref);
		}
		/** Initialize third-party code with dataset attributes.
		 * 
		 * @param dataset a description of the expected dataset.
		 */
		template<class Y1>
		static void setup_dataset(ExampleSet<descriptor_type,Y1,std::string*>& dataset)
		{
			testset_type testset(dataset);
			ind_setup_header(testset);
		}

	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.5f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			if( empty() ) return threshold();
			return ind_predict(&model, pred);
		}
		/** Builds a model for the IND tree.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			setoptions();
			ind_setup(learnset);
			ind_train(&model);
			ind_prune(&model);
			return 0;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return INDTree
		 */
		static std::string name()
		{
			return "INDTree";
		}
		/** Get the version of the IND tree classifier.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _IND_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return ind
		 */
		static std::string prefix()
		{
			return "ind";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return treeSel;
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return model == 0;
		}
		/** Clear an IND tree model.
		 */
		void clear()
		{
			if( model != 0 ) ind_erase(&model);
		}
		/** Clears all dynamically allocated global memory in the parent learner.
		 */
		static void cleanall()
		{
		}
		/** Gets the class name of the learner.
		 * 
		 * @return INDTree
		 */
		static std::string class_name()
		{
			return "INDTree";
		}
		/** Gets the expected name of the program.
		 * 
		 * @return ind
		 */
		static std::string progname()
		{
			return "ind";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
		}

	private:
		void setoptions()
		{
			init_opts();
			const char* styles[][4] = 
			{//	 name,			train,											prune,  predict (not necessary)
				{"cart",		"-it -Pnull -p1 -Sfull -s5 -C10 -n0",			"-n",	"-slG"},
				{"cart0",		"-it -Pnull -p0 -Sfull -s1 -C10 -n0",			"-n",	"-slG"},
				{"id3",			"-ii -Pnull",									"-n",	"-sl"},
				{"c4",			"-ir -Pnull -s1,2 -Sfull",						"-fn",	"-sl"},
				{"mml",			"-tPmml -A1 -n0.0001",							"-b",	"-slvg"},
				{"smml",		"-tPmml -Pchoices -A1 -n0.0001",				"-B",	"-slvg"},
				{"bayes",		"-tAnonsym,1",									"-b",	"-slvg"},
				{"c4laplace",	"-ir -Pnull -s1,2 -Sfull",						"-n",	"-slvg"},
				{"c4bayes",		"-ir -Pnull -s1,2 -Sfull",						"-b",	"-slvg"},
				{"mmllaplace",	"-tPmml -A1 -n0.0001",							"-n",	"-slvg"},
				{"look",		"-t -B2,4",										"",		""},
				{"tlook",		"-t -B3,1,0.00001,4 -J1",						"",		""},
				{"opt",			"-t -B2,4 -J3,0.005,0.75,0.1 -K3,.05 -L0.9",	"",		""},
				{"opt1",		"-t -B1,4 -J3,0.005,0.75,0.1 -K3,.05 -L0.9",	"",		""},
				{"dgraph",		"-tA1 -DD -tPmml",								"-b",	"-slvg"},
				{"lgraph",		"-tA1 -DD -tPmml -B2,4",						"-b",	"-slvg"},
				{"dgraphwan",	"-tA1 -DD -tPmml -w",							"-b",	"-slvg"},
				{"treewan",		"-tA1 -D -tPmml -w",							"-b",	"-slvg"},
				{0, 0, 0, 0 }
			};
			if( treeInt == 0 )
			{
				setup_options(prd_opts, parse_predict_options);
				setup_options(trn_opts, parse_train_options);
				setup_options(prn_opts, parse_prune_options);
			}
			else 
			{
				argument_opts(styles[treeInt-1][1], styles[treeInt-1][2], styles[treeInt-1][3]);
			}
		}
		static void setup_options(option_set& opts, void (*func)(int c, char* optarg), bool dflag=false)
		{
			Option temp;
			for(OptionSet::iterator beg=opts.begin(), end=opts.end();beg != end;++beg)
			{
				if( beg->code() == '0' )
				{
					if( beg->length() > 0 )
					{
						temp.code((*beg)[0]);
						std::string::size_type n = beg->find(',');
						if( n != std::string::npos ) temp = beg->substr(n+1);
						func(temp.code(), const_cast<char*>(temp.c_str()));
					}
				}
				else if( beg->code() == '1' )
				{
					for(unsigned int j=0;j<beg->length();++j)
					{
						temp.code((*beg)[j]);
						func(temp.code(), const_cast<char*>(temp.c_str()));
					}
				}
				else if( beg->code() == '2' )
				{
					temp.code((*beg)[0]);
					if( temp.code() != '0' ) 
						func(temp.code(), const_cast<char*>(temp.c_str()));
				}
				else func(beg->code(), const_cast<char*>(beg->c_str()));
			}
		}

	public:
		/** Reads an IND tree from the input stream.
		 *
		 * @param in an input stream.
		 * @param tree an IND tree.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, INDTree& tree)
		{
			tree.errormsg(in, ind_read_model(in, &tree.model));
			return in;
		}
		/** Writes an IND tree to the output stream.
		 *
		 * @param out an output stream.
		 * @param tree an IND tree.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const INDTree& tree)
		{
			tree.errormsg(out, ind_write_model(out, &tree.model));
			return out;
		}

	private:
		mutable model_type model;

	private:
		int treeInt;
		option_set trn_opts;
		option_set prn_opts;
		option_set prd_opts;
		argument_type treeSel;
	};
};
/** @brief Type utility interface to a IND tree
 * 
 * This class defines a type utility interface to an IND tree.
 */
template<>
struct TypeUtil< ::exegete::INDTree >
{
	/** Flags IND tree as non-primative.*/
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::INDTree value_type;
	/** Tests if a string can be converted to a an IND tree.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return INDTree
	 */
	static const char* name() 
	{
		return "INDTree";
	}
};


#endif

