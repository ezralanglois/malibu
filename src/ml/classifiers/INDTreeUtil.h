/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * INDTreeUtil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_INDTREEUTIL_H
#define _EXEGETE_INDTREEUTIL_H
#include "AttributeTypeUtil.h"
#include "typeutil.h"
#include "ExampleSet.h"


/** @file INDTreeUtil.h
 * @brief Utility classes for the IND tree
 * 
 * This file contains utility classes for the IND tree.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#ifndef _NO_IND_STRUCT
extern "C"
{
	/** Copies an IND tree model.
	 *
	 * @param from a source model.
	 * @param to a destination model.
	 */
	void ind_copy_model(void** from, void** to);
	/** Use the IND Tree to predict the class of an example.
	 *
	 * @param vmodel the IND tree model.
	 * @param attr an example attribute vector.
	 * @return a prediction confidence.
	 */
	float ind_predict(void** vmodel, const float* attr);
	/** Prune an IND tree.
	 *
	 * @param model a tree model to prune.
	 */
	void ind_prune(void** model);
	/** Train an IND tree with a given set of examples.
	 *
	 * @param model a tree model to train.
	 */
	void ind_train(void** model);
	/** Erase an IND tree model.
	 *
	 * @param model a tree model to train.
	 */
	void ind_erase(void** model);
	/** Initialize the IND tree options.
	 */
	void init_opts();
	/** Setups options for prediction.
	 *
	 * @param c an option type.
	 * @param optarg a string value.
	 */
	void parse_predict_options(int c, char* optarg);
	/** Setups options for pruning.
	 *
	 * @param c an option type.
	 * @param optarg a string value.
	 */
	void parse_prune_options(int c, char* optarg);
	/** Setups options for training.
	 *
	 * @param c an option type.
	 * @param optarg a string value.
	 */
	void parse_train_options(int c, char* optarg);
	/** Set the arguments with a single command string.
	 *
	 * @param trn training command line.
	 * @param prn pruning command line.
	 * @param prd prediction command line.
	 */
	void argument_opts(const char* trn, const char* prn, const char* prd);
}
#endif

namespace exegete
{
	/** Setup the IND header.
	 *
	 * @param header an example set to copy.
	 */
	void ind_setup_header(AttributeHeader<float, int>& header);
	/** Setup the IND dataset.
	 *
	 * @param dataset an example set to copy.
	 */
	void ind_setup(ExampleSet<float, int, void>& dataset);
	/** Write the IND model.
	 *
	 * @param out an output stream.
	 * @param model an IND tree/graph model.
	 * @return an error message or NULL.
	 */
	const char* ind_write_model(std::ostream& out, void** model);
	/** Read an IND model.
	 *
	 * @param in an input stream.
	 * @param model an IND tree/graph model.
	 * @return an error message or NULL.
	 */
	const char* ind_read_model(std::istream& in, void** model);

};



#endif


