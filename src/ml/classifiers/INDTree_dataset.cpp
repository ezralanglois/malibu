/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * INDTree_dataset.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

/** @file INDTree_dataset.cpp
 * @brief Contains the implementation of the IND tree dataset interface functions.
 * 
 * This source file contains implementation of the IND tree dataset interface functions.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#include "exegete_vs.h"
#ifdef _IND
extern "C" 
{
#include "BITSET.h"
//#include "BITARRAY.h"
typedef bitset  *bitarray;
extern  int    card_i;
}
#define bitarray_set(set,i)	((set)[i/BITSETMAX] & (1<<(i%BITSETMAX)))
#define empty_bitarray(set1)        for (card_i=set1[-1]; card_i>=0; \
					   set1[card_i--] = 0 )
#define set_bitarray(set,i)	(set)[i/BITSETMAX] |= 1<<(i%BITSETMAX)

#define MAXATTRS  815 //254
#undef set_union
#define	unordvals(A)	st[(int)A].u.unord->vals /* = num values of unord attr*/
#define	tempty(T)		flag_set((T)->tflags,emptt)
#define int_flags(set)         ((unsigned int)set.i)
#define set_test(tester)   (type(tester->attr) == SETTYPE)
#define discrete_test(tester)   (type(tester->attr) == DISCRETE)
#define attr_test(tester)   flag_set(tester->tsflags,attr)
#define subset_test(tester)   flag_set(tester->tsflags,subset)
#define subsets_test(tester)   (flag_set(tester->tsflags,subset) || \
					flag_set(tester->tsflags,bigsubset) )
#define bigsubset_test(tester)  flag_set(tester->tsflags,bigsubset)
#define remdr_test(tester)   flag_set(tester->tsflags,remdr)
#define cut_test(tester)      flag_set(tester->tsflags,cut)
#define bigsubset_test(tester)  flag_set(tester->tsflags,bigsubset)
#define decattr         ld.h_decattr
#define foraattrs(i)	for (decattr?(i=0):(i=1); i<=nattrs; ((++i)==decattr)?(++i):i )
#define nattrs          (ncattrs + ndattrs + nsattrs)
#define ncattrs         ld.h_ncattrs
#define nsattrs         ld.h_nsattrs
#define ndattrs         ld.h_ndattrs
#define negs            ld.h_negs
#define	DONTKNOW 254		/* unknown for unordtype */
#define FDKNOW	 999999.0	/* unknown for float */
#define SDKNOW	 SINGLETON(SYMSETMAX)	
#define TRUE	1
#define FALSE	0
#define SYMSETMAX  BITSETMAX-1 
#define CTS	'C'
#define NORM	'N'
#define EXP	'E'
#define STP	'J'
#define POISS	'P'
#define DISCRETE 'U'
#define DECISION 'D'
#define SETTYPE  'S'
#define SB_OFF    00
#define	ord_val_s(E, A)	        ((E).ordp[(int)A])
#define	set_val_s(E, A)	        ((E).setp[(int)A])
#define	unord_val_s(E, A)       ((E).unordp[(int)A])
#define sindex(A)				st[(int)A].sym_index 
#define	ord_val(E, A)	        ord_val_s(E, sindex(A))
#define	set_val(E, A)	        set_val_s(E, sindex(A))
#define	unord_val(E, A)         unord_val_s(E, sindex(A))
#define	class_val(E)	        (*(E).unordp)
#define	sym_fill(A)	flag_set(st[(int)A].sflags,fill)
//#define type(A)		st[(int)A].sym_type
#define set_type(A)	(st[(int)A].sym_type == SETTYPE)
#define set_copy(newset,set)	newset = set
#define	unkns(A)	flag_set(st[(int)A].sflags,unkn)
#define mem_alloc(type) ((type *) salloc(sizeof(type)))
#define mems_alloc(type,size) ( (type *) salloc(sizeof(type)*(size)) )
#define num_type(A)	(st[(int)A].sym_type == CTS ||\
			st[(int)A].sym_type == NORM ||\
			st[(int)A].sym_type == EXP ||\
			st[(int)A].sym_type == STP ||\
			st[(int)A].sym_type == POISS)
#define	thgraph(T) flag_set((T)->hflags,graph)
#define	ttest(T)		flag_set((T)->tflags,test)
#define	toptions(T)		flag_set((T)->tflags,optiont)
#define	outcomes(tester)	( tester->no )
#define foroutcomes(i,tester)	for (i=outcomes(tester)-1; i>=0; i--)
#define  foroptions(op,i,t)   for ( i=0, op = toptions(t)? \
				(t->option.s.o[i=t->option.s.c-1]) :\
				(t->option.o) ;\
			        i>=0; op = (--i>=0)?t->option.s.o[i]:\
				 (bt_ptr)0 )

extern "C" 
{
#ifdef  DEBUG_alloc
#define salloc(s)	salloc_LF((int)s,__LINE__,__FILE__)
void *salloc_LF(int size,int line,char *file);
#else
#define salloc(s)	salloc_LF((int)s)
void *salloc_LF(int size);
#endif
	bitarray new_bitarray(int);

	typedef struct  Header Header; 
	typedef	struct	Symbol Symbol;
	typedef struct egset egset;
	typedef struct Sampler Sampler;
	typedef union egtype egtype;
	extern Symbol st[];
	extern Header ld;

	struct Sampler
	{
		unsigned char	s_type;		/*  codes, see sample.c */
			int     s_sample_size;  /*  sample size  */
			int     s_partitions;   /*  partitions for cross valid.  */
			int     s_partition;    /*  size of sample  */
			long    s_seed;         /*  seed for random sampling */
	};

	struct Header
	{
		int	h_magicno;	/*  used in encoded_file(), read_enc_eg(),... */
		int	h_nsattrs;	/*  no. of set attrs  */
		int	h_ncattrs;	/*  no. of continuous attrs  */
		int	h_ndattrs;	/*  no. of discrete attrs  */
		int     h_decattr;      /*  index for decision attribute  */
		int	h_negs;		/*  no. of examples in file  */
		int	h_egsize;	/*  size in chars of eg stuff */
					/*	set to 0 if variable  */
		Sampler h_sampler;      /*  details of sampling information */
	};

	union sym_flags 
	{
		struct {
			unsigned  unkn : 1;	/*  true if attr. has missing vals */
			unsigned  fill : 1;	/*  true if attr. details incomplete */
		} b;
		unsigned char i;
	};
	struct unord_sym {
		int vals;	/* Number of possible values */
		char **ptr;	/* Pointer to the value strings */
		char *hasht;    /* hash table for looking up vals */
		unsigned subsetting : 2; /* filled with SB_* values */
	};
	struct ord_sym {
		float min;	/* Min possible value for cts attr */
		float max;	/* Max possible value ... */
	};

	typedef	struct contexts Context;
	typedef	union	sym_flags   sym_flags;
	typedef	struct	unord_sym	unord_sym;
	typedef	struct	ord_sym		ord_sym;
	typedef	struct	Symbol		Symbol;
	struct Symbol
	{
		char sym_type;	/* DISCRETE, CTS, DECISION, SETTYPE */
		sym_flags sflags;	
		int  sym_index;
		union 
		{
			unord_sym  *unord;
			ord_sym  *ord;
		} u;
		char	*_name;			/*  name of attribute  */
		Context *_onlyif;		/*  context  */
	};

	typedef	unsigned char unordtype;
	union  egtype
	{  
			float   *ordp;  
			unordtype *unordp;
			bitset   *setp;  
	};

	void free_allset(egset* egs);
	void no_sample(Sampler *sampler);
	egset *make_set(egtype *dblock);
	static float MISSING;
	void ind_copy(const float* from, egtype* eg)
	{
		int i;
		foraattrs(i)
		{
			if( num_type(i) )
			{
				if( from[i-1] == MISSING )
				{
					ord_val((*eg),i) = FDKNOW;
					unkns(i) = TRUE;
				}
				else
				{
					ord_val((*eg),i) = from[i-1];
				}
			}
			else if ( set_type(i) )
			{
				if( from[i-1] == MISSING )
				{
					set_copy(set_val((*eg),i), SDKNOW);
					unkns(i) = TRUE;
				}
				else
				{
					set_copy(set_val((*eg),i),(bitset)from[i-1]);
				}
			}
			else //discreet
			{
				if( from[i-1] == MISSING )
				{
					unord_val((*eg),i) = DONTKNOW;
					unkns(i) = TRUE;
				}
				else
				{
					unord_val((*eg),i) = (unordtype)from[i-1];
				}
			}
		}
	}

	extern int st_ndecs;
	extern egset* egs;
	extern  int     nvalsattr;
	extern float	*prop;
	void safree(void * tt);
}
#include "ExampleSet.h"
#include <stdio.h>
namespace exegete
{
	void ind_setup_header(AttributeHeader<float, int>& header)
	{
		typedef AttributeHeader<float, int> attribute_header;
		typedef attribute_header::header_type header_type;
		typedef attribute_header::type_util type_util;
		MISSING = type_util::missing();
		unsigned int i=0;
		st_ndecs = header.classCount();
		for(i=1;i<=header.attributeCount();++i)
		{
			if( st[i].sym_type == DISCRETE ) safree(st[i].u.unord);
			else safree(st[i].u.ord);
		}
		ld.h_ndattrs = header.countType(header_type::NOMINAL);
		ld.h_ncattrs = header.attributeCount() - ld.h_ndattrs;
		ld.h_nsattrs = 0;
		ld.h_decattr = 0;
		no_sample(&ld.h_sampler);
		unsigned int coffset = (unsigned int)(ld.h_ndattrs*sizeof(unordtype))/sizeof(float)+1;
		unsigned int soffset = (unsigned int)(((ld.h_ncattrs+coffset) * sizeof(float)) / sizeof(bitset));
		unsigned int doffset = (unsigned int)((ld.h_nsattrs+soffset) * sizeof(bitset));
		int dcnt=0, ccnt=0;
		ld.h_egsize = doffset;
		nvalsattr = 2;
		for(i=0;i<header.attributeCount();i++)
		{			
			if( header.attributeAt(i).isnominal() )
			{
				st[i+1].sym_type = DISCRETE;
				st[i+1].u.unord = mem_alloc(unord_sym);
				st[i+1].u.unord->vals = (int)header.attributeAt(i).size();
				st[i+1].u.unord->subsetting = SB_OFF;
				st[i+1].u.unord->ptr = 0;
				st[i+1].u.unord->hasht = 0;
				st[i+1].sym_index = (dcnt+1);
				if( nvalsattr < st[i+1].u.unord->vals ) nvalsattr = st[i+1].u.unord->vals;
				dcnt++;
			}
			else 
			{
				st[i+1].sym_type = CTS;
				st[i+1].u.ord = mem_alloc(ord_sym);
				st[i+1].u.ord->min = header.attributeAt(i).min();
				st[i+1].u.ord->max = header.attributeAt(i).max();
				st[i+1].sym_index = ccnt;
				ccnt++;
			}
			sym_fill(i+1) = FALSE;
			st[i+1]._name = const_cast<char*>( header.attributeAt(i).name().c_str() );
			st[i+1]._onlyif = 0;
			unkns(i+1) = FALSE;
		}
		st[0].sym_type = DECISION;//Set more there?
		st[0].sym_index = 0;
		for(i=0;i<=header.attributeCount();++i)
		{
			if( num_type(i) )	   sindex(i) += coffset;
			else if( set_type(i) ) sindex(i) += soffset;
		}
	}
	void ind_setup(ExampleSet<float, int, void>& dataset)
	{
		unsigned int i=0;
		ind_setup_header(dataset);
		ld.h_negs = dataset.size();
		unsigned int egsize = ld.h_egsize;
		negs = (int) dataset.size();
		egtype* made = mems_alloc(egtype, negs);
		unordtype* temp = (unordtype *)salloc(negs * ld.h_egsize);
		ASSERT((unsigned int)nattrs == dataset.attributeCount());
		for(i=0;i<(unsigned int)negs;++i)
		{
			made[i].unordp = temp;
			ind_copy(dataset[i].x(), &made[i]);
			class_val(made[i]) = (unordtype)dataset[i].y();
			temp += egsize;
		}
		egs = make_set(made);
	}
}

extern "C" 
{
union  thead_flags 
{
    struct {
#ifdef LITTLE_ENDIAN
	unsigned pad : 3;
	unsigned graph : 1;		/*  not a tree but a graph */
	unsigned random : 1;		/*  generation randomized */
	unsigned optiont : 1;		/*  has options */
	unsigned bayes : 1;		/*  wants or has bayes pruning */
	unsigned probs : 1;		/*  has probs set, not counts */
#else
	unsigned probs : 1;		/*  has probs set, not counts */
	unsigned bayes : 1;		/*  wants or has bayes pruning */
	unsigned optiont : 1;		/*  has options */
	unsigned random : 1;		/*  generation randomized */
	unsigned graph : 1;		/*  not a tree but a graph */
	unsigned pad : 3;
#endif
    } b;
    unsigned char i;
};
typedef  union  thead_flags thead_flags;

typedef union  prior_flags prior_flags;
union  prior_flags
{
    struct {
#ifdef LITTLE_ENDIAN
	unsigned pad : 1;
        unsigned search_p_join : 1;    /*  search for p_join for dgraphs  */
        unsigned alpha_classes : 1;     /*  dont divide alpha by the number of classes  */
        unsigned alpha_nonsym : 1;     /*  non-symmetric palpha  */
        unsigned node_prop : 1;        /*  multiply palphaval by node freq.  */
        unsigned attr_choice : 1;      /*  test prob / #choices */
        unsigned wallace_weight : 1;   /*  if not, then bushy prior */
        unsigned local_normal : 1;     /*  normalize weights at each node  */
#else
        unsigned local_normal : 1;     /*  normalize weights at each node  */
        unsigned wallace_weight : 1;   /*  if not, then bushy prior */
        unsigned attr_choice : 1;      /*  test prob / #choices */
        unsigned node_prop : 1;        /*  multiply palphaval by node freq.  */
        unsigned alpha_nonsym : 1;     /*  non-symmetric palpha  */
        unsigned alpha_classes : 1;     /*  dont divide alpha by the number of classes  */
        unsigned search_p_join : 1;    /*  search for p_join for dgraphs  */
	unsigned pad : 1;
#endif
    } b;
    unsigned char i;
};

union test_flags 
{
	struct {
#ifdef LITTLE_ENDIAN
 		unsigned pad : 3;
 		unsigned bigsubset : 1;
 		unsigned remdr : 1;
 		unsigned attr : 1;
 		unsigned cut : 1;
 		unsigned subset : 1;
#else
		unsigned  subset : 1;
		unsigned  cut : 1;
		unsigned  attr : 1;
		unsigned  remdr : 1;
		unsigned  bigsubset : 1;
		unsigned pad : 3;
#endif
	} b;
	unsigned char i;
};
typedef union test_flags test_flags;

struct t_prior
{
    float   alphaval;
	prior_flags	prior_flag;
    float   node_weight, leaf_weight, p_join;
	int	depth_bound;
	double  prior_nml;
};
typedef	struct	t_prior	t_prior; /* type for tree prior info. */
typedef  union  tree_flags tree_flags;
union  tree_flags 
{
    struct  {
#ifdef LITTLE_ENDIAN
        unsigned pad : 4;
	unsigned overgrown : 1;         /*  ??? ask jono  */
	unsigned lprobset : 1;          /*  ??? ask jono  */
	unsigned transit3 : 1;          /*  ??? ask jono  */
	unsigned tree_section : 1;      /*  ??? ask jono  */
	unsigned transit2 : 1;          /*  ??? ask jono  */
	unsigned lock : 1;		/*  ??? ask jono  */
	unsigned transit : 1;		/*  ??? ask jono  */
	unsigned optiont : 1;		/*  node has optiont */
	unsigned visited : 1;		/*  node has been visited */
	unsigned emptt : 1;		/*  node has been forced empty */
	unsigned leaf : 1;		/*  node is a leaf */
	unsigned test : 1;		/*  node is a test */
#else
	unsigned test : 1;		/*  node is a test */
	unsigned leaf : 1;		/*  node is a leaf */
	unsigned emptt : 1;		/*  node has been forced empty */
	unsigned visited : 1;		/*  node has been visited */
	unsigned optiont : 1;		/*  node has optiont */
	unsigned transit : 1;		/*  ??? ask jono  */
	unsigned lock : 1;		/*  ??? ask jono  */
	unsigned transit2 : 1;          /*  ??? ask jono  */
	unsigned tree_section : 1;      /*  ??? ask jono  */
	unsigned transit3 : 1;          /*  ??? ask jono  */
	unsigned lprobset : 1;          /*  ??? ask jono  */
	unsigned overgrown : 1;         /*  ??? ask jono  */
        unsigned pad : 4;
#endif
    } b;
    unsigned short i;
};
typedef	struct	bt_rec	bt_rec;	/* type for branch record */
typedef		bt_rec	*bt_ptr;
typedef struct CC_rec CC_rec;
typedef struct egset egset;
typedef union egtype egtesttype;
typedef	struct test_rec	test_rec;
struct test_rec	/* type for test details */
{
	unsigned int	attr;	/* attribute tested */
	unsigned char	no;	/* no. of test values  */
	test_flags	tsflags; /* indicates type of test */
	union	{
		bitset		valset;
		bitarray	valbigset;
		float		cut;
		int		oval;
	}	tval;
	union  	{
		float		prop_true;   /* prop. of successes
					     * for this test, if binary */
		float		*prop_vec;   /* for non-binary tests */
	}	tprop;
};

union xtras {
      struct    	/* for storing details of tree growing */
      {
        float  gain;            /*  quality of split or log. prob of node */
	egset	*egs;		/*  for this node  */
      } gr;
      struct            /* type for storing details of tree pruning */
      {
        float  lprob;       /* probab. of leaf node */
        float  sprob;       /* log prob of subnode */
      } pn;
      CC_rec	*cc;
      float     *errs;
      float	*re_count;
} xtra;		/* spare space for use by grow, prune, etc */
typedef union  xtras   xtras;  /* extra junk at end of record */


struct ot_rec	/* type for tree */
{
	tree_flags	tflags;	/* set of binary flags as listed above */
	float	tot_count; 	/* total count of egs (to save recalculating) */
	float	lprob;	   	/* basic probab. this node will be a leaf */
	float   bestprob;       /* best message length for this node */
	union  bts {
		bt_ptr	o;	/* one test */
		struct  btss {
			bt_ptr	*o;	/* several optional tests */
			short	c;	/*  numb. of options  */
		} s;
	} option;
	float	*eg_count;	/* vector of counts (one for each class) */
	bt_ptr	*parents;	/* reverse link to parents  */
	int	num_parents, line_no;
#ifdef GRAPH
	int	parent_c;
#endif
	egtesttype  testing;
	xtras xtra;		/* spare space for use by grow, prune, etc */
};
typedef		ot_rec	*ot_ptr;

struct bt_rec	/* type for tree */
{
	tree_flags	tflags;	/* set of binary flags as listed above */
	float	nprob;	   	/* basic node prior prob for this subtree,
				   i.e.  any log. probs. other than those
					 already in node_weight()  */
	union {
	  float	nprop; 	     /* basic proportion for option (see "reclass.c") */
	  float	nodew; 	     /* basic node weight for log. prob. */
	} np;
	ot_ptr	parent;		/* reverse link to parent  */
	test_rec	*test;	/* details of node branches */
	ot_ptr	*branches;	/* subtrees */
	float   gain;		/* gain (in choose) or log prob. for node  */
};
struct t_head	/*  header details about tree  */
{
    int magicno;
    int treesize;            /*    number of nodes  */
    int leafsize;            /*    number of leaves  */
	thead_flags	hflags;      /*    see "tree header flags"     */
	double	sprob;               /*    post.log.prob for tree */
	double	eprob;               /*    post.log.prob for egs */
	double	genprob;             /*    prob. of generat. this random tree
				           (for importance sampling)    */
        t_prior	prior;
};
typedef	struct	t_head	t_head;	/* type for tree header */


struct TreeModel
{
	t_head thead;
	ot_ptr treePtr;
};
typedef struct TreeModel model_type;
typedef model_type* model_pointer;
extern bool decision_graph_flag;
int is_dgraph(ot_ptr t);
int tree_size(ot_ptr t);
int leaf_size(ot_ptr t);
int previous_transit(ot_ptr t);
void alloc_jline_no_tree();
void unset_transit_flag(ot_ptr t);
void unlock_transit_flag();
void assign_jline_no_node(ot_ptr t);
}
namespace exegete
{
	const char* write_model(std::ostream& out, ot_ptr t);
	const char* read_model(std::istream& in, ot_ptr* t);
	
	const char* ind_write_model(std::ostream& out, void** model)
	{
		model_pointer tptr = *(model_pointer*)model;
		tptr->thead.treesize = tree_size(tptr->treePtr);
		tptr->thead.leafsize = leaf_size(tptr->treePtr);
		//----------------------------------------------------------
		out << "SIZE: " << tptr->thead.treesize << "," << tptr->thead.leafsize << ","
			<< int_flags(tptr->thead.hflags) << "," << tptr->thead.sprob << ","
			<< tptr->thead.eprob << "," << tptr->thead.genprob << "\n";
		out << "PRIOR: " << tptr->thead.prior.alphaval  << ","
			<< tptr->thead.prior.node_weight << "," 
			<< tptr->thead.prior.leaf_weight << "," 
			<< tptr->thead.prior.p_join << "," 
			<< int_flags(tptr->thead.prior.prior_flag) << ","
			<< tptr->thead.prior.depth_bound << ","
			<< tptr->thead.prior.prior_nml << "\n";
		return write_model(out, tptr->treePtr);
	}
	const char* ind_read_model(std::istream& in, void** model)
	{
		model_pointer tptr;
		unsigned int  oct;
		std::string tmp;
		const char* msg;
		tptr = (model_pointer)salloc(sizeof(model_type));
		if( (in >> tmp).fail() || tmp != "SIZE:" ) return ERRORMSG("Cannot read in size code for IND tree");
		if( in.peek() == ' ' ) in.get();
		if( (in >> tptr->thead.treesize).fail() ) return ERRORMSG("Cannot read tree size for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.leafsize).fail() ) return ERRORMSG("Cannot read leaf size for IND tree");
		if( in.get() != ',' || (in >> oct).fail() ) return ERRORMSG("Cannot read in tree flags for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.sprob).fail() ) return ERRORMSG("Cannot read in sprob for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.eprob).fail() ) return ERRORMSG("Cannot read in eprob for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.genprob).fail() ) return ERRORMSG("Cannot read in genprob for IND tree");
		set_flags(tptr->thead.hflags,oct);
		if( in.peek() == '\n' ) in.get();
		if( (in >> tmp).fail() || tmp != "PRIOR:" ) return ERRORMSG("Cannot read in prior code for IND tree");
		if( in.peek() == ' ' ) in.get();
		if( (in >> tptr->thead.prior.alphaval).fail() ) return ERRORMSG("Cannot read alphaval for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.prior.node_weight).fail() ) return ERRORMSG("Cannot read node weight for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.prior.leaf_weight).fail() ) return ERRORMSG("Cannot read leaf weight for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.prior.p_join).fail() ) return ERRORMSG("Cannot read p_join for IND tree");
		if( in.get() != ',' || (in >> oct).fail() ) return ERRORMSG("Cannot read prior flags for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.prior.depth_bound).fail() ) return ERRORMSG("Cannot read depth_bound for IND tree");
		if( in.get() != ',' || (in >> tptr->thead.prior.prior_nml).fail() ) return ERRORMSG("Cannot read prior_nml for IND tree");
		set_flags(tptr->thead.prior.prior_flag,oct);

		tptr->treePtr = (ot_ptr)salloc(sizeof(ot_rec));
		msg = read_model(in, &tptr->treePtr);
		tptr->treePtr->parents = (bt_ptr *)salloc(sizeof(bt_ptr));
		tptr->treePtr->parents[0] = 0;
		tptr->treePtr->num_parents = 1;
		(*(model_pointer*)model) = tptr;
		return msg;
	}
	
	
	const char* write_model(std::ostream& out, ot_ptr t)
	{
		register test_rec *tsp;
        register int i, j, s;
		bt_ptr option;
		const char* msg;

		out << int_flags(t->tflags) << "," << st_ndecs << "," << t->eg_count[0];
		for(i=1;i<st_ndecs;++i) out << "+" << t->eg_count[i];
		out << "=" << t->tot_count << "(" << t->lprob << "," << t->bestprob << ")";
		if( ttest(t) )
		{
			if( toptions(t) ) out << "," << t->option.s.c;
			foroptions(option,j,t)
			{
				out << "," << int_flags(option->tflags);
				tsp=option->test;
				out << "," << int_flags(tsp->tsflags) << "," << tsp->attr;
				if( cut_test(tsp) ) out << "," << tsp->tval.cut;
				else if( subset_test(tsp) ) out << "," << tsp->tval.valset;
				else if ( bigsubset_test(tsp) )
				{
					out << " ";
					s = set_card(tsp->tval.valbigset[-2]) + tsp->tval.valbigset[-1]*BITSETMAX;
					for(i=0;i<s;++i)
					{
						if ( bitarray_set(tsp->tval.valbigset,i) ) out << '1';
						else out << '0';
						if( i%32 == 31 ) out << ';';
						else if( i%8 == 7 ) out << ',';
					}
				}
				out << "(" << option->nprob << " " << option->np.nprop << " " << option->gain << ")\n";
				foroutcomes(i,option->test) if( (msg=write_model(out, option->branches[i])) != 0 ) return msg;
			}
		}
		else out << "\n";
		return 0;
	}

	const char* read_model(std::istream& in, ot_ptr* pt)
	{
        register bt_ptr option;
        register ot_ptr t = *pt, t2;
        int i,j, oct=0, s;
		test_rec *tsp;
		const char* msg;
		char ch;
		if( (in >> oct).fail() ) return ERRORMSG("Cannot read tree flags for IND tree");
		set_flags(t->tflags, oct);
		if( tempty(t) )
		{
			free(t);
			*pt = 0;
			return 0;
		}
		if( in.get() != ',' || (in >> st_ndecs).fail() ) return ERRORMSG("Cannot read number of classes for IND tree: " << int(ch) << " - " << ch);
		t->eg_count = (float*)salloc(st_ndecs * sizeof(float));
		if( in.get() != ',' || (in >> t->eg_count[0]).fail() ) return ERRORMSG("Cannot read number of classes for IND tree");
		for(i=1;i<st_ndecs;++i) if( in.get() != '+' || (in >> t->eg_count[i]).fail() ) return ERRORMSG("Cannot read number of examples for each class for IND tree");
		if( in.get() != '=' || (in >> t->tot_count).fail() ) return ERRORMSG("Cannot read total number for IND tree");
		if( in.get() != '(' || (in >> t->lprob).fail() ) return ERRORMSG("Cannot read lprob for IND tree");
		if( in.get() != ',' || (in >> t->bestprob).fail() || in.get() != ')' ) return ERRORMSG("Cannot read bestprob for IND tree");

		if( ttest(t) )
		{
			if( toptions(t) )
			{
				if( in.get() != ',' || (in >> t->option.s.c).fail() ) return ERRORMSG("Cannot read number of options for IND tree");
				t->option.s.o = (bt_ptr *) salloc(t->option.s.c*sizeof(bt_ptr));
				for(i=0;i<t->option.s.c;i++) t->option.s.o[i] = (bt_ptr)salloc(sizeof(bt_rec));
			}
			else option = t->option.o = (bt_ptr)salloc(sizeof(bt_rec));
			foroptions(option,j,t)
			{
				option->parent = t;
				oct=0;
				if( in.get() != ',' || (in >> oct).fail() ) return ERRORMSG("Cannot read tree flags for IND tree");
				set_flags(option->tflags, oct);
				option->test = tsp = (test_rec*)salloc(sizeof(test_rec));
				{
					if( in.get() != ',' || (in >> oct).fail() ) return ERRORMSG("Cannot read test flags for IND tree");
					set_flags(tsp->tsflags, oct);
					if( in.get() != ',' || (in >> tsp->attr).fail() ) return ERRORMSG("Cannot read attr for IND tree");
					if( tsp->attr > nattrs ) return ERRORMSG("Attribute index exceeds number of attributes: " << tsp->attr << " >= " << nattrs);
					tsp->no = 2;
					if( cut_test(tsp) ) 
					{ 
						if( st[tsp->attr].sym_type != CTS ) return ERRORMSG("Incorrectly read in attribute is not continuous");
						if( in.get() != ',' || (in >> tsp->tval.cut).fail() ) return ERRORMSG("Cannot read cut for IND tree");
					}
					else if( subset_test(tsp) ) 
					{
						if( st[tsp->attr].sym_type != DISCRETE ) return ERRORMSG("Incorrectly read in attribute is not nominal: " << tsp->attr << " -> " << st[tsp->attr].sym_type );
						if( in.get() != ',' || (in >> tsp->tval.valset).fail() ) return ERRORMSG("Cannot read valset for IND tree");
					}
					else if( bigsubset_test(tsp) ) 
					{
						if( in.get() == ',' ) return ERRORMSG("Error reading in big subset");
						//unordvals(tsp->attr)
						if( st[tsp->attr].sym_type != DISCRETE ) return ERRORMSG("Incorrectly read in attribute is not nominal");
						tsp->tval.valbigset = new_bitarray(unordvals(tsp->attr));
						s = set_card(tsp->tval.valbigset[-2]) + tsp->tval.valbigset[-1]*BITSETMAX;
						empty_bitarray(tsp->tval.valbigset);
						for(i=0;i<s;++i)
						{
							if( (oct=in.get()) == '1' ) set_bitarray(tsp->tval.valbigset, i);
							else if(oct < 0) break;
							else if( oct == ',' || oct == ';' ) --i;
							else if( oct != '0' ) return ERRORMSG("Cannot read valbigset - 1 for IND tree: " << i << " " << oct);
						}
						if( i < s ) return ERRORMSG("Cannot read valbigset - 2 for IND tree");
					}
					else
					{
						tsp->tval.valset = 0;
						if( st[tsp->attr].sym_type != DISCRETE ) return ERRORMSG("Incorrectly read in attribute is not nominal");
						ASSERT(tsp->attr < MAXATTRS);
						tsp->no = unordvals(tsp->attr);
					}
				}
				if( (ch=in.get()) != '(' || (in >> option->nprob).fail() ) return ERRORMSG("Cannot read option nprob for IND tree: \"" << ch << "\"");
				if( (in >> option->np.nprop).fail() ) return ERRORMSG("Cannot read option np.nprop for IND tree");
				if( (in >> option->gain).fail() || in.get() != ')' ) return ERRORMSG("Cannot read option gain for IND tree");
				option->branches = (ot_ptr*)salloc(outcomes(option->test)*sizeof(ot_ptr));
				foroutcomes(i,option->test)
				{
					option->branches[i] = (ot_ptr)salloc(sizeof(ot_rec));
					if( (msg=read_model(in, &option->branches[i])) != 0 ) return msg;
					if( tempty(option->branches[i]) )
					{
						free(option->branches[i]);
						option->branches[i] = 0;
					}
					else 
					{
						t2 = option->branches[i];
						t2->parents = (bt_ptr*)salloc(sizeof(bt_ptr));
						t2->parents[0] = option;
						t2->num_parents = 1;
					}
				}
			}
		}
		else t->option.o = (bt_ptr)0;
		return 0;
	}

	
};


#endif


