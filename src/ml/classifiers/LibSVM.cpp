/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * LibSVM.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

/** @file LibSVM.cpp
 * @brief Contains the implementation of the LibSVM interface functions.
 * 
 * This source file contains implementation of the LibSVM interface functions:
 * 	- wsvm.cpp
 * 	- svm.cpp
 * 	- native interface
 * 
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#ifdef _WEIGHTED
#include "wsvm.cpp"
#else
#include "svm.cpp"
#endif


#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#include <iostream>
#include <iomanip>
#include <string>
#include "errorutil.h"



const char* check_model(const svm_model *model)
{
	const svm_parameter& param = model->param;
	if( model->SV == 0 ) return "Empty support vector data";
	if( model->sv_coef == 0 ) return "Empty support vector coefficients";
	if( model->rho == 0 ) return "Empty decision function constants";

	if(param.svm_type == ONE_CLASS ||
	   param.svm_type == EPSILON_SVR ||
	   param.svm_type == NU_SVR)
	{
		return 0;
	}
	if( model->label == 0 ) return "Empty class labels";
	if( model->nSV == 0 ) return "Empty number of support vectors for each class";
	return 0;
}

double libsvm_predict(const svm_model *model, const struct svm_node *x)
{
	const svm_parameter& param = model->param;
	double dec;
	svm_predict_values(model, x, &dec);
	
	if(param.svm_type == ONE_CLASS ||
	   param.svm_type == EPSILON_SVR ||
	   param.svm_type == NU_SVR)
	{
		return dec;
	}
	
	int neg = model->label[ dec > 0 ? 0 : 1 ];
	if( neg > 0 ) 
	{
		if( dec > 0 ) return dec;
		return -dec;
	}
	else
	{
		if( dec < 0 ) return dec;
		else return -dec;
	}
}


const char* read_libsvm_model(std::istream& in, svm_model*& model)
{
	model = Malloc(svm_model, 1);
	svm_parameter& param = model->param;
	std::string cmd;
	model->rho = 0;
	model->probA = 0;
	model->probB = 0;
	model->label = 0;
	model->nSV = 0;

	while(1)
	{
		in >> cmd;
		if(cmd == "svm_type")
		{
			in >> cmd;
			int i;
			for(i=0;svm_type_table[i];i++)
			{
				if(cmd == svm_type_table[i])
				{
					param.svm_type=i;
					break;
				}
			}
			if(svm_type_table[i] == 0)
			{
				free(model->rho);
				free(model->label);
				free(model->nSV);
				free(model);
				model=0;
				return ERRORMSG("unknown svm function: " << cmd);
			}
		}
		else if(cmd == "kernel_type")
		{
			in >> cmd;
			int i;
			for(i=0;kernel_type_table[i];i++)
			{
				if(cmd == kernel_type_table[i])
				{
					param.kernel_type=i;
					break;
				}
			}
			if(kernel_type_table[i] == 0)
			{
				free(model->rho);
				free(model->label);
				free(model->nSV);
				free(model);
				model = 0;
				return ERRORMSG("unknown kernel function: " << cmd);
			}
		}
		else if(cmd == "degree")	in >> param.degree;
		else if(cmd == "gamma")		in >> param.gamma;
		else if(cmd == "coef0")		in >> param.coef0;
		else if(cmd == "nr_class")	in >> model->nr_class;
		else if(cmd == "total_sv")	in >> model->l;
		else if(cmd == "rho")
		{
			int n = model->nr_class * (model->nr_class-1)/2;
			model->rho = Malloc(double,n);
			for(int i=0;i<n;i++) in >> model->rho[i];
		}
		else if(cmd == "label")
		{
			int n = model->nr_class;
			model->label = Malloc(int,n);
			for(int i=0;i<n;i++) in >> model->label[i];
		}
		else if(cmd == "probA")
		{
			int n = model->nr_class * (model->nr_class-1)/2;
			model->probA = Malloc(double,n);
			for(int i=0;i<n;i++) in >> model->probA[i];
		}
		else if(cmd == "probB")
		{
			int n = model->nr_class * (model->nr_class-1)/2;
			model->probB = Malloc(double,n);
			for(int i=0;i<n;i++) in >> model->probB[i];
		}
		else if(cmd == "nr_sv")
		{
			int n = model->nr_class;
			model->nSV = Malloc(int,n);
			for(int i=0;i<n;i++) in >> model->nSV[i];
		}
		else if(cmd == "SV")
		{
			while(!in.eof()) if(in.get()=='\n') break;	
			break;
		}
		else
		{
			free(model->rho);
			free(model->label);
			free(model->nSV);
			free(model);
			model = 0;
			return ERRORMSG("unknown text in model file: " << cmd);
		}
	}
	int elements = 0;
	long pos = in.tellg();
	while(1)
	{
		int c = in.get();
		switch(c)
		{
			case '\n':
				// count the '-1' element
			case ':':
				++elements;
				break;
			case EOF:
				goto out;
			default:
				;
		}
	}
out:
	in.clear();
	in.seekg(pos);
	int m = model->nr_class - 1;
	int l = model->l;
	model->sv_coef = Malloc(double *,m);
	int i;
	for(i=0;i<m;i++) model->sv_coef[i] = Malloc(double,l);
	model->SV = Malloc(svm_node*,l);
	svm_node *x_space=0;
	if(l>0) x_space = Malloc(svm_node,elements);

	int j=0;
	char c;
	for(i=0;i<l;i++)
	{
		model->SV[i] = &x_space[j];
		for(int k=0;k<m;k++) in >> model->sv_coef[k][i];
		while(1)
		{
			in >> x_space[j].index >> c >> x_space[j].value;
			while( isspace(in.peek()) && in.peek() != '\n' ) in.get(c);
			++j;
			if( in.peek() == '\n' )
			{
				in.get(c);
				break;
			}
		}	
//out2:
		x_space[j++].index = -1;
	}
	model->free_sv = 1;	// XXX
	return 0;
}


void write_libsvm_model(std::ostream& out, const svm_model* model)
{
	const svm_parameter& param = model->param;
	out << "svm_type " << svm_type_table[param.svm_type] << "\n";
	out << "kernel_type " << kernel_type_table[param.kernel_type] << "\n";

	if(param.kernel_type == POLY) 
		out << "degree " << param.degree << "\n";
	if(param.kernel_type == POLY || param.kernel_type == RBF || param.kernel_type == SIGMOID) 
		out << "gamma " << param.gamma << "\n";
	if(param.kernel_type == POLY || param.kernel_type == SIGMOID) 
		out << "coef0 " << param.coef0 << "\n";
	
	int nr_class = model->nr_class;
	int l = model->l;
	out << "nr_class " << nr_class << "\n";
	out << "total_sv " << l << "\n";

	out << "rho";
	for(int i=0;i<nr_class*(nr_class-1)/2;i++) out << " " << model->rho[i];
	out << "\n";

	if(model->label)
	{
		out << "label";
		for(int i=0;i<nr_class;i++) out << " " << model->label[i];
		out << "\n";
	}
	if(model->probA) // regression has probA only
	{
		out << "probA";
		for(int i=0;i<nr_class*(nr_class-1)/2;i++) out << " " << model->probA[i];
		out << "\n";
	}
	if(model->probB)
	{
		out << "probB";
		for(int i=0;i<nr_class*(nr_class-1)/2;i++) out << " " << model->probB[i];
		out << "\n";
	}
	if(model->nSV)
	{
		out << "nr_sv";
		for(int i=0;i<nr_class;i++) out << " " << model->nSV[i];
		out << "\n";
	}
	out << "SV\n";
	const double * const *sv_coef = model->sv_coef;
	const svm_node * const *SV = model->SV;

	for(int i=0;i<l;i++)
	{
		for(int j=0;j<nr_class-1;j++) 
			out << std::setprecision(16) << sv_coef[j][i] << " ";

		const svm_node *p = SV[i];
		while(p->index != -1)
		{
			out << p->index << ":" << std::setprecision(8) << p->value << " ";
			p++;
		}
		out << "\n";
	}
}

svm_model* copy_libsvm_model(const svm_model* from)
{
	if(from == 0 ) return 0;
	svm_model *model = Malloc(svm_model,1);
	int nr_class = from->nr_class;
	int l = from->l;
	int rhon = nr_class*(nr_class-1)/2;
	int i,j;

	model->param = from->param;
	model->nr_class = nr_class;
	model->l = l;

	if( from->label )
	{
		model->label = Malloc(int,nr_class);
		for(i=0;i<nr_class;++i) model->label[i] = from->label[i];
	}
	else model->label = 0;

	if( from->rho )
	{
		model->rho = Malloc(double,rhon);
		for(i=0;i<rhon;++i) model->rho[i] = from->rho[i];
	}
	else model->rho = 0;

	if( from->probA )
	{
		model->probA = Malloc(double,rhon);
		for(i=0;i<rhon;++i) model->probA[i] = from->probA[i];
	}
	else model->probA = 0;

	if( from->probB )
	{
		model->probB = Malloc(double,rhon);
		for(i=0;i<rhon;++i) model->probB[i] = from->probB[i];
	}
	else model->probB = 0;
	
	if( from->nSV )
	{
		model->nSV = Malloc(int,nr_class);
		for(i=0;i<nr_class;++i) model->nSV[i] = from->nSV[i];
	}
	else model->nSV = 0;

	if( from->sv_coef )
	{
		model->sv_coef = Malloc(double*,nr_class-1);
		for(i=0;i<nr_class-1;++i) 
		{
			model->sv_coef[i] = Malloc(double, l);
			for(j=0;j<l;++j) model->sv_coef[i][j] = from->sv_coef[i][j];
		}
	}
	else model->sv_coef = 0;

	if( from->SV )
	{
		model->SV = Malloc(svm_node*,l);
		for(i=0;i<l;++i) model->SV[i] = from->SV[i];
	}
	else model->SV = 0;
	model->free_sv = 0;


	return model;
}

void output_libsvm_param(std::ostream& out, const svm_parameter& param)
{
	out << "LIBSVM on " << kernel_type_table[param.kernel_type] << " (";
	if( param.kernel_type==POLY )
		out << param.degree << ", " << param.gamma << ", " << param.coef0 << ", ";
	else if( param.kernel_type==SIGMOID ) 
		out << param.gamma << ", " << param.coef0 << ", ";
	else if( param.kernel_type == RBF )
		out << param.gamma << ", ";
	out << param.C << ")";
}



