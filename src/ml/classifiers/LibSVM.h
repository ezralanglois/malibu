/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * LibSVM.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_LIBSVM_H
#define _EXEGETE_LIBSVM_H
#include "Learner.h"
#include "LibSVMUtil.h"
/** Defines a macro to a nice malloc function.
 * 
 * @param type memory type.
 * @param n number of blocks.
 */
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

#ifdef LIBSVM_VERSION
/** Defines the version of the LIBSVM algorithm. 
 * @todo add to group
 */
#define _LIBSVM_VERSION 208060
#else
/** Defines the version of the LIBSVM algorithm. 
 * @todo add to group
 */
#define _LIBSVM_VERSION 201000
#endif

/** @file LibSVM.h
 * @brief Third-party LibSVM classifier interface
 * 
 * This file contains the LibSVM class template.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Third-party LibSVM classifier interface
	 * 
	 * This class defines a classifier that builds an SVM model.
	 *
	 * @todo add shallow copy
	 * @todo add parameter copy
	 * @todo check ARGUMENT=1 do I need it?
	 */
	template<class Y=int>
	class LibSVM : public Learner
	{
		typedef struct svm_problem	 svm_problem;
		typedef struct svm_model	 svm_model;
		typedef struct svm_node		 svm_node;

	public:
#ifdef _WEIGHTED
		/** Defines a double as a weight type. **/
		typedef double weight_type;
#else
		/** Defines a void as a weight type. **/
		typedef void weight_type;
#endif
	public:
		/** Interface flags:
		 *	- Do not set attribute size to 8
		 * 	- Not a lazy learner
		 * 	- Use a training set
		 * 	- Start argument at 1
		 */
		enum{ USE_TRAINSET=1, TUNE=1, ARGUMENT=1 };

	public:
		/** Defines a parent type. **/
		typedef void parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<svm_node, Y, weight_type>				dataset_type;
		/** Defines a testset type. **/
		typedef ExampleSet<svm_node, Y, std::string*>				testset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer		const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef svm_node											descriptor_type;
		/** Defines a float type. **/
		typedef double												float_type;
		/** Defines a tunable argument iterator. **/
		typedef typename Learner::argument_iterator					argument_iterator;
		/** Defines a prediction type. **/
		typedef float_type 											prediction_type;
	private:
		typedef typename dataset_type::size_type 					size_type;

	public:
		/** Constructs a default LibSVM.
		 */
		LibSVM() : Learner(SEMI|MISSING|NOMINAL|BINARY|NORMALIZED), model(0)
		{
		}
		/** Destructs a LibSVM classifier and deallocates a LibSVM model.
		 */
		~LibSVM()
		{
			if(model != 0) svm_destroy_model(model);
			model = 0;
		}
		
	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			arginit(map, NAME_VERSION(LibSVM) );
			param.initparam(map, t);
		}

	public:
		/** Constructs a deep copy of the LibSVM model.
		 *
		 * @param svm a source LibSVM model.
		 */
		LibSVM(const LibSVM& svm) : Learner(svm), param(svm.param)
		{
			model = copy_libsvm_model(svm.model);
		}
		/** Assigns a deep copy of the LibSVM model.
		 *
		 * @param svm a source LibSVM model.
		 * @return a reference to this object.
		 */
		LibSVM& operator=(const LibSVM& svm)
		{
			Learner::operator=(svm);
			argument_copy(svm);
			model = copy_libsvm_model(svm.model);
			return *this;
		}
		/** Makes a shallow copy of a LibSVM.
		 *
		 * @param ref a reference to a LibSVM.
		 */
		void shallow_copy(LibSVM& ref)
		{
			Learner::operator=(ref);
			argument_copy(ref);
			model = ref.model;
			ref.model = 0;
		}
		/** Copies arguments in a LibSVM classifier.
		 * 
		 * @param svm a LibSVM classifier.
		 */
		void argument_copy(const LibSVM& svm)
		{
			param = svm.param;
			Learner::argument_copy(svm);
		}

	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			if( empty() ) return threshold();
			return libsvm_predict(model, pred);
		}
		/** Builds a model for the SVM.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			param.setup(learnset.attributeCount());
			svm_problem prob;
			copy(prob, learnset);
			if( model != 0 ) svm_destroy_model(model);
			ASSERTMSG(svm_check_parameter(&prob, &param.parameter()) == 0, svm_check_parameter(&prob, &param.parameter()));
			model = svm_train(&prob, &param.parameter());
			ASSERTMSG(check_model(model) == 0, check_model(model));
			erase(prob);
			return 0;
		}
		/** Get the class name of the learner.
		 * 
		 * @return LibSVM
		 */
		static std::string class_name()
		{
			return "LibSVM";
		}
		/** Get the version of the LibSVM classifier.
		 * 
		 * @return version.
		 */
		static int version()
		{
			return _LIBSVM_VERSION;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return LibSVM
		 */
		static std::string name()
		{
			return "LibSVM";
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return svm
		 */
		static std::string prefix()
		{
			return "svm";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return param.tuneparameter();
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return model == 0;
		}
		/** Clear an svm model.
		 */
		void clear()
		{
			if(model != 0) svm_destroy_model(model);
			model = 0;
		}
		/** Get the expected name of the program.
		 * 
		 * @return wlibsvm or libsvm
		 */
		static std::string progname()
		{
#ifdef _WEIGHTED
			return "wlibsvm";
#else
			return "libsvm";
#endif
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
		}

	public:
		/** Reads an svm from the input stream.
		 *
		 * @param in an input stream.
		 * @param svm an svm model.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, LibSVM& svm)
		{
			svm.errormsg(in, read_libsvm_model(in, svm.model));
			return in;
		}
		/** Writes an svm to the output stream.
		 *
		 * @param out an output stream.
		 * @param svm an svm model.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const LibSVM& svm)
		{
			write_libsvm_model(out, svm.model);
			return out;
		}

	private:
		static void copy(svm_problem& prob, dataset_type& learnset)
		{
			prob.l = learnset.size();
			prob.x = setsize(prob.x, learnset.size());
			prob.y = setsize(prob.y, learnset.size());
#ifdef _WEIGHTED
			prob.W = setsize(prob.W, learnset.size());
#endif
			for(size_type i=0;i<learnset.size();++i)
			{ 
				prob.x[i] = learnset[i].x();
				prob.y[i] = learnset[i].y();
#ifdef _WEIGHTED
				prob.W[i] = learnset[i].w();
#endif
			}
		}
		static void erase(svm_problem& prob)
		{
			::erase(prob.x);
			::erase(prob.y);
#ifdef _WEIGHTED
			::erase(prob.W);
#endif
		}

	private:
		svm_model* model;
		LibSVMParam param;
	};
};

/** @brief Type utility interface to a LibSVM
 * 
 * This class defines a type utility interface to a LibSVM.
 */
template< class Y >
struct TypeUtil< ::exegete::LibSVM<Y> >
{
	/** Flags a LibSVM classifier as non-primative.
	 */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::LibSVM<Y> value_type;
	/** Tests if a string can be converted to a LibSVM classifier.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return LibSVM
	*/
	static const char* name() 
	{
		return "LibSVM";
	}
};


#endif

