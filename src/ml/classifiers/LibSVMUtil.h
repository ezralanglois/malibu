/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * LibSVMUtil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_LIBSVMUTIL_H
#define _EXEGETE_LIBSVMUTIL_H
#ifdef _WEIGHTED
#include "wsvm.h"
#else
#include "svm.h"
#endif

#include "AttributeTypeUtil.h"
#include "typeutil.h"
#include "ArgumentMap.h"
#include "TuneParameterTree.h"


/** @file LibSVMUtil.h
 * @brief Utility classes for the LibSVM classifier
 * 
 * This file contains utility classes for the LibSVM classifier.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Specializes AttributeUtil for an svm node
	 * 
	 * This class represents an attribute type corresponding to a column in
	 * the dataset. It specializes the class template to a svm node.
	 * 
	 * @ingroup ExegeteDataset
	 */
	template<>
	class AttributeUtil< svm_node >
	{
	public:
		/** Flags attribute as sparse. **/
		enum{IS_SPARSE=true};

	public:
		/** Defines a TypeUtil as limit type. **/
		typedef TypeUtil<double>		limit_type;
		/** Defines a double as a value type. **/
		typedef double					value_type;
		/** Defines a svm_node as an attribute type. **/
		typedef svm_node				attribute_type;
		/** Defines pointer to attribute type as pointer. **/
		typedef attribute_type*			pointer;
		/** Defines a constant pointer to attribute type as a constant pointer. **/
		typedef const attribute_type*	const_pointer;
		/** Defines an int as an index type. **/
		typedef int						index_type;
		/** Defines a size_t as a size type. **/
		typedef size_t					size_type;
		/** Defines a pointer difference as a difference type. */
		typedef ptrdiff_t				difference_type;

	public:
		/** Converts an attribute to a value.
		 *
		 * @param attr an attribute type.
		 * @return svm_node value.
		 */
		static const value_type& valueOf(const attribute_type& attr)
		{
			return attr.value;
		}
		/** Converts an attribute to a value.
		 *
		 * @param attr an attribute type.
		 * @return svm_node value.
		 */
		static value_type& valueOf(attribute_type& attr)
		{
			return attr.value;
		}
		/** Converts an attribute to an index.
		 *
		 * @param attr an attribute type.
		 * @return svm_node index.
		 */
		static index_type indexOf(const attribute_type& attr)
		{
			return attr.index;
		}
		/** Converts an attribute to an index.
		 *
		 * @param attr an attribute type.
		 * @return svm_node index.
		 */
		static index_type& indexOf(attribute_type& attr)
		{
			return attr.index;
		}
		/** Flags the specified attribute as the last.
		 *	- Increment the index reference.
		 *	- Assign attribute index to -1.
		 *
		 * @param px a pointer to attributes.
		 * @param n index of attribute.
		 */
		static void marklast(pointer px, size_type& n)
		{
			++n;
			px[n].index = -1;
		}
		/** Tests if attribute is last sparse attribute.
		 *
		 * @param attr a reference to an attribute type.
		 * @return true if attribute index is last.
		 */
		static bool islast(const attribute_type& attr)
		{
			return attr.index == -1;
		}
		/** Gets the internal value of a missing attribute.
		 *
		 * @todo Fix svm, any zero must be increasted to DBL_MIN
		 * 
		 * @return 0.
		 */
		static value_type missing()
		{
			return limit_type::max();
		}
	};

	/** @brief LibSVM parameters
	 * 
	 * Defines the full set of svm parameters and makes them tunable.
	 */
	class LibSVMParam
	{
		typedef TuneParameterTree				argument_type;
		typedef argument_type::parameter_type	range_type;
		typedef struct svm_parameter 			svm_parameter;
	public:
		/** Constructs a default LibSVM parameters.
		 */
		LibSVMParam() : kernel_type(0), poswgt(0.0), negwgt(0.0),
						kernel(kernel_type, "Kernel", range_type(1,2,1,'+')),
						lineC(param.C, "Cost", range_type(1,2,1,'+'), &kernel, 0, 1),
						linepC(poswgt, "PosCost", range_type(0,1,1,'+'), &lineC),
						linenC(negwgt, "NegCost", range_type(0,1,1,'+'), &linepC),
				
						gausC(param.C, "Cost", range_type(1,128,2,'*'), &kernel, 1, 2),
						gauspC(poswgt, "PosCost", range_type(0,1,1,'+'), &gausC),
						gausnC(negwgt, "NegCost", range_type(0,1,1,'+'), &gauspC),
						gausg(param.gamma, "Gamma", range_type(0.00001f,32,2,'*'), &gausnC),
				
						sigmC(param.C, "Cost", range_type(1,2,1,'+'), &kernel, 2, 3),
						sigmpC(poswgt, "PosCost", range_type(0,1,1,'+'), &sigmC),
						sigmnC(negwgt, "NegCost", range_type(0,1,1,'+'), &sigmpC),
						sigmg(param.gamma, "Gamma", range_type(1,2,1,'+'), &sigmnC),
						sigmc(param.coef0, "Coef0", range_type(0,1,1,'+'), &sigmg),
				
						polyC(param.C, "Cost", range_type(1,2,1,'+'), &kernel, 3, 4),
						polypC(poswgt, "PosCost", range_type(0,1,1,'+'), &polyC),
						polynC(negwgt, "NegCost", range_type(0,1,1,'+'), &polypC),
						polyg(param.gamma, "Gamma", range_type(1,2,1,'+'), &polynC),
						polyc(param.coef0, "Coef0", range_type(0,1,1,'+'), &polyg),
						polyd(param.degree, "Degree", range_type(2,8,1,'+'), &polyc)
		{
			init(param);
		}
		/** Destructs a libsvm parameter and deallocates the parameter memory.
		 */
		~LibSVMParam()
		{
			svm_destroy_param(&param);
		}
		
	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void initparam(U& map, int t)
		{
			if( t == 0 || t == 1 )
			{
				arginit(map, param.svm_type,		"svm_type",			"set type of SVM>C-SVC:0;nu-SVC:1;one-class SVM:2;epsilon-SVR:3;nu-SVR:4", ArgumentMap::ADDITIONAL);//"set type of kernel function>Linear[u'*v]:0;Polynomial[(gamma*u'*v + coef0)^degree]:1;Radial Basis[exp(-gamma*|u-v|^2)]:2;Sigmoid[tanh(gamma*u'*v + coef0)]:3"
				
				if(t>=0)
				{
				arginit(map, kernel,				"kernel",			"set type of kernel function>Linear:0;Gaussian:1;Sigmoid:2;Polynomial:3;*:4");
				arginit(map, "Polynomial Kernel");
				arginit(map, polyd,					"degree",			"set degree in polynomial kernel");
				arginit(map, polyg,					"gamma_poly",		"set gamma in polynomial kernel", ArgumentMap::ADDITIONAL);
				arginit(map, polyc,					"coef0_poly",		"set coef0 in polynomial kernel", ArgumentMap::ADDITIONAL);
				arginit(map, polyC,					"cost_poly",		"set the cost parameter for polynomial kernel");
				arginit(map, polypC,				"pwgt_poly",		"set positive weight for polynomial kernel", ArgumentMap::ADDITIONAL);
				arginit(map, polynC,				"nwgt_poly",		"set negative weight for polynomial kernel", ArgumentMap::ADDITIONAL);
				arginit(map, "Sigmoid Kernel");
				arginit(map, sigmc,					"coef0_sigm",		"set coef0 in sigmoid kernel");
				arginit(map, sigmg,					"gamma_sigm",		"set gamma in sigmoid kernel");
				arginit(map, sigmC,					"cost_sigm",		"set the cost parameter for sigmoid kernel");
				arginit(map, sigmpC,				"pwgt_sigm",		"set positive weight for sigmoid kernel", ArgumentMap::ADDITIONAL);
				arginit(map, sigmnC,				"nwgt_sigm",		"set negative weight for sigmoid kernel", ArgumentMap::ADDITIONAL);
				arginit(map, "Gaussian Kernel");
				arginit(map, gausg,					"gamma_gaus",		"set gamma in gaussian kernel");
				arginit(map, gausC,					"cost_gaus",		"set the cost parameter for gaussian kernel");
				arginit(map, gauspC,				"pwgt_gaus",		"set positive weight for gaussian kernel", ArgumentMap::ADDITIONAL);
				arginit(map, gausnC,				"nwgt_gaus",		"set negative weight for gaussian kernel", ArgumentMap::ADDITIONAL);
				arginit(map, "Linear Kernel");
				arginit(map, lineC,					"cost_line",		"set the cost parameter for linear kernel");
				arginit(map, linepC,				"pwgt_line",		"set positive weight for linear kernel", ArgumentMap::ADDITIONAL);
				arginit(map, linenC,				"nwgt_line",		"set negative weight for linear kernel", ArgumentMap::ADDITIONAL);
				}
				else
				{
				arginit(map, kernel_type,			"kernel",		"set type of kernel function>Linear:0;Gaussian:1;Sigmoid:2;Polynomial:3;*:4");
				arginit(map, param.degree,			"degree",		"set degree in kernel function");
				arginit(map, param.gamma,			"gamma",		"set gamma in kernel function");
				arginit(map, param.coef0,			"coef0",		"set coef0 in kernel function");
				arginit(map, param.C,				"cost",			"set the cost parameter for kernel function");
				arginit(map, poswgt,				"pwgt",			"set positive weight for kernel function");
				arginit(map, negwgt,				"nwgt",			"set negative weight for kernel function");
				}
				
				arginit(map, "SVM", ArgumentMap::ADVANCED);
				arginit(map, param.nu,				"nu",				"set the parameter nu of nu-SVC, one-class SVM, and nu-SVR", ArgumentMap::ADVANCED);
				arginit(map, param.p,				"eps",				"set the epsilon in loss function of epsilon-SVR", ArgumentMap::ADVANCED);
				arginit(map, param.cache_size,		"cachesize",		"set cache memory size in MB", ArgumentMap::ADVANCED);
				arginit(map, param.eps,				"term",				"set tolerance of termination criterion", ArgumentMap::ADVANCED);
				arginit(map, param.shrinking,		"shrinking",		"whether to use the shrinking heuristics?", ArgumentMap::ADVANCED);
			}
		}

	public:
		/** Constructs a copy of a LibSVM parameter.
		 *
		 * @param lparam a LibSVMParam to copy.
		 */
		LibSVMParam(const LibSVMParam& lparam) : 
			param(lparam.param), kernel_type(lparam.kernel_type), 
			poswgt(lparam.poswgt), negwgt(lparam.negwgt),
			kernel(kernel_type, lparam.kernel),
			lineC(param.C,  lparam.lineC, &kernel),
			linepC(poswgt,  lparam.linepC, &lineC),
			linenC(negwgt,  lparam.linenC, &lineC),
			gausC(param.C,  lparam.gausC, &kernel),
			gauspC(poswgt,  lparam.gauspC, &gausC),
			gausnC(negwgt,  lparam.gausnC, &gausC),
			gausg(param.gamma,  lparam.gausg, &gausnC),
			sigmC(param.C,  lparam.sigmC, &kernel),
			sigmpC(poswgt,  lparam.sigmpC, &sigmC),
			sigmnC(negwgt,  lparam.sigmnC, &sigmC),
			sigmg(param.gamma,  lparam.sigmg, &sigmnC),
			sigmc(param.coef0,  lparam.sigmc, &sigmg),
			polyC(param.C,  lparam.polyC, &kernel),
			polypC(poswgt,  lparam.polypC, &polyC),
			polynC(negwgt,  lparam.polynC, &polyC),
			polyg(param.gamma,  lparam.polyg, &polynC),
			polyc(param.coef0,  lparam.polyc, &polyg),
			polyd(param.degree,  lparam.polyd, &polyc)
		{
		}

	public:
		/** Setups up the proper parameters, call before training svm.
		 */
		void setup(unsigned int attrn)
		{
			if( kernel_type == 0 )		param.kernel_type = LINEAR;
			else if( kernel_type == 1 )	param.kernel_type = RBF;
			else if( kernel_type == 2 )	param.kernel_type = SIGMOID;
			else if( kernel_type == 3 )	param.kernel_type = POLY;
			if( param.gamma == 0.0f ) param.gamma = 1.0f / attrn;
			if( negwgt != 0.0 || poswgt != 0.0 )
			{
				unsigned int i=0;
				param.nr_weight = 0;
				if( negwgt != 0.0 ) param.nr_weight++;
				if( poswgt != 0.0 ) param.nr_weight++;
				if( param.nr_weight > 0 )
				{
					param.weight = resize(param.weight, param.nr_weight);
					param.weight_label = resize(param.weight_label, param.nr_weight);
				}
				else
				{
					erase(param.weight);param.weight=0;
					erase(param.weight_label);param.weight_label=0;
				}
				if( negwgt != 0.0 )
				{
					param.weight_label[i] = 0;
					param.weight[i] = negwgt;++i;
				}
				if( poswgt != 0.0 )
				{
					param.weight_label[i] = 0;
					param.weight[i] = poswgt;
				}
			}
		}
		/** Gets an svm parameter.
		 *
		 * @return an svm parameter.
		 */
		svm_parameter& parameter()
		{
			return param;
		}
		/** Gets the first tunable parameter in a tune parameter tree.
		 *
		 * @return a root tune parameter.
		 */
		argument_type& tuneparameter()
		{
			return kernel;
		}

	private:
		static void init(svm_parameter& param)
		{
			param.svm_type = C_SVC;
			param.kernel_type = 0;
			param.degree = 2;
			param.gamma = 0.000001;	// 1/k
			param.coef0 = 0;
			param.nu = 0.5;
			param.cache_size = 100;
			param.C = 1;
			param.eps = 1e-3;
			param.p = 0.1;
			param.shrinking = 1;
			param.probability = 0;
			param.weight=0;
			param.weight_label=0;
			param.nr_weight=0;
		}

	private:
		svm_parameter param;
		int kernel_type;
		double poswgt;
		double negwgt;
	private:
		argument_type kernel;
		argument_type lineC;
		argument_type linepC;
		argument_type linenC;
	private:
		argument_type gausC;
		argument_type gauspC;
		argument_type gausnC;
		argument_type gausg;
	private:
		argument_type sigmC;
		argument_type sigmpC;
		argument_type sigmnC;
		argument_type sigmg;
		argument_type sigmc;
	private:
		argument_type polyC;
		argument_type polypC;
		argument_type polynC;
		argument_type polyg;
		argument_type polyc;
		argument_type polyd;
	};

};

/** @brief Type utility interface for a LibSVM svm node
 * 
 * This class template provides basic type operations for 
 * a LibSVM svm node.
 */
template<>
struct TypeUtil< svm_node >
{
	/** lags class as primative.
	*/
	enum{ ispod=true };
	/** Defines a value type. **/
	typedef svm_node value_type;
	/** Tests if a string can be converted to an svm node.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return svm node
	*/
	static const char* name() 
	{
		return "svm_node";
	}
};


#include <iostream>
/** Defines an svm model. **/
typedef struct svm_model	 svm_model;
/** Copies a libsvm model.
 *
 * @param from a pointer to a libsvm model.
 * @return a pointer to a libsvm.
 */
svm_model* copy_libsvm_model(const svm_model* from);
/** Writes a libsvm model to the output stream.
 *
 * @param out an output stream.
 * @param model a pointer to a model.
 */
void write_libsvm_model(std::ostream& out, const svm_model* model);
/** Reads a libsvm model from an input stream.
 *
 * @param in an input stream.
 * @param model a libsvm model.
 * @return an error message or NULL.
 */
const char* read_libsvm_model(std::istream& in, svm_model*& model);
/** Makes a binary (signed) prediction using an SVM model.
 *
 * @param model an svm model.
 * @param x an attribute vector.
 * @return a signed prediction.
 */
double libsvm_predict(const svm_model *model, const struct svm_node *x);
/** Writes svm parameters to the output stream.
 *
 * @param out an output stream.
 * @param param svm parameters.
 */
void output_libsvm_param(std::ostream& out, const svm_parameter& param);
/** Tests if the SVM model is valid.
 *
 * @param model a model to test.
 * @return an error message or NULL.
 */
const char* check_model(const svm_model *model);

#endif


