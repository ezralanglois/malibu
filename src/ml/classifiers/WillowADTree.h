/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowADTree.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWADTREE_H
#define _EXEGETE_WILLOWADTREE_H
#include "WillowSplit.h"
#include "WillowSlice.h"
#include "WillowImpurity.h"
#include "Learner.h"
#include <cmath>
/** Defines the version of the ADTree algorithm. 
 * @todo add to group
 */
#define _WILLOWADTREE_VERSION 101000

/** @file WillowADTree.h
 * @brief Native implementation of the ADTree
 * 
 * This file contains the native implementation of the ADTree.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Native implementation of the ADTree
	 * 
	 * This class template defines an ADTree classifier.
	 *
	 * @todo fast adaboost tuning
	 * @todo use attr_weight (look like importance weighted algorithm), back it up too.
	 */
	template<class A, class C>
	class WillowADTree : public Learner
	{
	public:
		/** Interface flags:
		 *	- Do not set attribute size to 8
		 * 	- Not a lazy learner
		 * 	- Do not use a training set
		 * 	- Tunable
		 * 	- Graphable
		 * 	- Fast tunable
		 * 	- Start argument at 1
		 */
		enum{ ATTR8=0, LAZY=0, USE_TRAINSET=0, TUNE=1, ARGUMENT=1, GRAPHABLE=1, FAST_TUNE=1 };

	public:
		/** Defines a parent type. **/
		typedef void parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<A, C, double>							dataset_type;
		/** Defines a testset type. **/
		typedef ExampleSet<A, C, std::string*>						testset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer		const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type				descriptor_type;
		/** Defines a float type. **/
		typedef double												float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator							argument_iterator;
		/** Defines a weight type. **/
		typedef void												weight_type;
		/** Defines a prediction type. **/
		typedef float_type 											prediction_type;
	private:
		typedef Example<A,C,double*>								example_type;
		typedef WillowSplit<dataset_type, float_type, KMRLoss>		splitter_type;
		typedef typename splitter_type::willow_node					willow_node;
		typedef TuneParameterTree									argument_type;
		typedef typename argument_type::parameter_type				range_type;
		typedef example_type*										example_iterator;
		typedef WillowSlice< example_type >							slice_type;
		typedef std::vector<slice_type>								slice_vector;
		typedef typename slice_vector::iterator						slice_iterator;
		typedef typename willow_node::size_type						size_type;
		typedef std::vector<unsigned int>							int_vector;
		typedef typename willow_node::float_type 					node_float;


	public:
		/** Constructs a default Willow ADTree.
		 */
		WillowADTree() : Learner(SEMI|BINARY), subsettingInt(0), iterationsInt(20),
						 iterationsSel(iterationsInt, "Iterations", true, range_type(5, 20, 1, '+'))
		{
		}
		/** Destructs a ADTree classifier.
		 */
		~WillowADTree()
		{
		}
		
	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(WillowADTree) );
				if(t>=0)
				arginit(map, iterationsSel, "iterations", "number of iterations or nodes");
				else
				arginit(map, iterationsInt, "iterations", "number of iterations or nodes");
				arginit(map, subsettingInt, "subsetting", "group nominal attributes for binary splits?");
			}
		}
		
	public:
		/** Constructs a deep copy of the willow ADTree model.
		 *
		 * @param tree a source willow ADTree model.
		 */
		WillowADTree(const WillowADTree& tree) : Learner(tree), model(tree.model), subsettingInt(tree.subsettingInt), iterationsInt(tree.iterationsInt), 
												 iterationsSel(iterationsInt, tree.iterationsSel)
		{
		}
		/** Assigns a deep copy of the willow ADTree model.
		 *
		 * @param tree a source willow ADTree model.
		 * @return a reference to this object.
		 */
		WillowADTree& operator=(const WillowADTree& tree)
		{
			Learner::operator=(tree);
			model = tree.model;
			argument_copy(tree);
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			if( empty() ) return threshold();
			return model.sumpredict(pred, example_type::missing(), iterationsInt);
		}
		/** Builds a model for the willow ADTree.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			model.clear();
			slice_iterator sav;
			example_iterator fbeg, fend;
			size_type last=1, nom = size_type(learnset.maximumNominal()>2?learnset.maximumNominal():2);
			float_type err = initweights(learnset.begin(), learnset.end(), 1.0f);
			float_type wsum = float_type(learnset.size()), minerr;
			
			err = KMRLoss::confidence(wsum, err, float_type(1.0));
			wsum += updtweights(learnset.begin(), learnset.end(), err);
			model.threshold((node_float)err);
			slice_vector slices(iterationsInt*nom+1);
			slices[0].resize(model, learnset.begin(), learnset.end());
			splitter_type splitter(1.0f/learnset.size(),wsum);
			model.resize_leaf(iterationsInt);
			for(unsigned int i=0,j,n,k;i<iterationsInt;++i)
			{
				splitter.reset(); minerr = splitter.minErr();
				sav = slices.begin();
				ASSERT(last < slices.size());
				for(slice_iterator curr=sav, end=slices.begin()+last;curr != end;++curr)
				{
					splitter.split(learnset, curr->begin(), curr->end(), subsettingInt==1);
					if( splitter.minErr() < minerr )
					{
						sav = curr;
						minerr = splitter.minErr();
					}
				}
				//ASSERTMSG(minerr != splitter_type::maxErr(), i << " - " << learnset.size() );
				if( minerr == splitter_type::maxErr() )
				{
					std::cerr << "WARNING: ADTree stopped early: " << i << " < " << iterationsInt << std::endl;
					break;
				}
				sav->node().add();
				willow_node& lastnode = sav->node().last();
				lastnode.order(i);
				splitter.shallow_update(lastnode, iterationsInt-i-1);
				n=lastnode.size();
				ASSERT((last+n-1) < slices.size());
				for(j=0;j<n;++j) slices[last+j].resize(lastnode[j], sav->size());
				j = lastnode.index();
				for(fbeg=sav->begin(), fend=sav->end();fbeg != fend;++fbeg)
				{
					if( fbeg->x()[j] != example_type::missing() )
					{
						k = lastnode.belongsTo(fbeg->x());
						slices[last+k].add(*fbeg);
					}
					else 
					{
						for(k=0;k<n;++k) slices[last+k].add(*fbeg);
					}
				}
				for(j=0;j<n;++j)
				{
					slices[last+j].trim(); 
					wsum += updtweights(slices[last+j].begin(), slices[last+j].end(), slices[last+j].node().threshold());
				}
				last+=n;
				splitter.total(wsum);
			}
			return 0;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return ADTree
		 */
		static std::string name()
		{
			return "ADTree";
		}
		/** Get the version of the ADTree classifier.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _WILLOWADTREE_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return adtree
		 */
		static std::string prefix()
		{
			return "adtree";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationsSel;
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return model.empty();
		}
		/** Clear an willow model.
		 */
		void clear()
		{
			model.clear();
		}
		/** Copies arguments in a willow ADTree.
		 * 
		 * @param tree a source ADTree.
		 */
		void argument_copy(const WillowADTree& tree)
		{
			subsettingInt = tree.subsettingInt;
			iterationsInt = tree.iterationsInt;
			iterationsSel = tree.iterationsSel;
			Learner::argument_copy(tree);
		}
		/** Makes a shallow copy of a Willow ADTree.
		 *
		 * @param ref a reference to a Willow ADTree.
		 */
		void shallow_copy(WillowADTree& ref)
		{
			//Learner::operator=(ref);
			model.shallow_copy(ref.model);
			argument_copy(ref);
		}
		/** Get the expected name of the program.
		 * 
		 * @return adtree
		 */
		static std::string progname()
		{
			return "adtree";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			model.accept(visitor);
		}
		/** Get number of iterations.
		 * 
		 * @return number of iterations.
		 */
		unsigned int iterations()const
		{
			return iterationsInt;
		}
		/** Get the class name of the learner.
		 * 
		 * @return WillowADTree
		 */
		static std::string class_name()
		{
			return "WillowADTree";
		}

	public:
		/** Reads an willow ADTree from the input stream.
		 *
		 * @param in an input stream.
		 * @param tree a willow tree.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, WillowADTree& tree)
		{
			tree.model.clear();
			in >> tree.iterationsInt;
			if( in.get() != '\t' )
			{
				tree.errormsg(in, "Missing tab character in AdaBoost model");
				return in;
			}
			tree.errormsg(in, tree.model.read(in));
			return in;
		}
		/** Writes an willow ADTree to the output stream.
		 *
		 * @param out an output stream.
		 * @param tree an willow tree.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const WillowADTree& tree)
		{
			out << tree.iterationsInt << "\t";
			tree.errormsg(out, tree.model.write(out));
			return out;
		}

	private:
		template<class E>
		static float_type sumweights(E beg, E end)
		{
			float_type wsum=0.0f;
			for(;beg != end;++beg) wsum += beg->w();
			return wsum;
		}
		static float_type initweights(typename dataset_type::iterator beg, typename dataset_type::iterator end, float_type w)
		{
			float_type psum=0.0f;
			for(;beg != end;++beg)
			{
				beg->w(w);
				if(beg->y() > 0) psum+=w;
			}
			return psum;
		}
		template<class E>
		static float_type updtweights(E beg, E end, float_type p)
		{
			float_type pos = std::exp(-p);
			float_type neg = std::exp(p);
			float_type bsum=0.0f, esum=0.0f;
//E st=beg;
			for(;beg != end;++beg)
			{
//std::cerr << std::distance(st,beg) << std::endl;
				bsum += beg->w();
				beg->w_update(beg->y()>0?pos:neg);
				ASSERT(beg->w() >= 0);
				esum += beg->w();
			}
			return (esum-bsum);
		}

	private:
		willow_node model;
		int subsettingInt;
		unsigned int iterationsInt;
		argument_type iterationsSel;
	};

};

/** @brief Type utility interface to a Willow ADTree
 * 
 * This class defines a type utility interface to a Willow ADTree.
 */
template<class A, class C>
struct TypeUtil< ::exegete::WillowADTree<A,C> >
{
	/** Flags class as non-primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::WillowADTree<A,C> value_type;
	/** Tests if a string can be converted to a Willow ADTree.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return WillowADTree
	*/
	static const char* name() 
	{
		return "WillowADTree";
	}
};


#endif


