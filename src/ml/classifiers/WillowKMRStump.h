/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowKMRStump.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWKMRSTUMP_H
#define _EXEGETE_WILLOWKMRSTUMP_H
#include "WillowSplit.h"
#include "WillowImpurity.h"
#include "Learner.h"
/** Defines the version of the Willow KMR stump algorithm. 
 * @todo add to group
 */
#define _WILLOWKMRSTUMP_VERSION 101000

/** @file WillowKMRStump.h
 * @brief Native implementation of the Willow KMR stump
 * 
 * This file contains the WillowKMRStump class template.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Native implementation of the Willow KMR stump
	 * 
	 * This class template defines a native implementation of the KMR stump.
	 *
	 * @todo add twoing, gain, gain ratio, twoing ordinal
	 */
	template<class A, class C, class W>
	class WillowKMRStump : public Learner
	{
	public:
		/** Interface flags:
		 *	- Do not set attribute size to 8
		 * 	- Not a lazy learner
		 * 	- Do not use a training set
		 * 	- Graphable
		 * 	- Start argument at 1
		 */
		enum{ ATTR8=0, LAZY=0, USE_TRAINSET=0, ARGUMENT=1, GRAPHABLE=1 };

	public:
		/** Defines a parent type. **/
		typedef void parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<A, C, W>									dataset_type;
		/** Defines a testset type. **/
		typedef ExampleSet<A, C, std::string*>						testset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer		const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type				descriptor_type;
		/** Defines a float type. **/
		typedef double												float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator							argument_iterator;
		/** Defines a weight type. **/
		typedef W													weight_type;
		/** Defines a prediction type. **/
		typedef float_type 											prediction_type;
	private:
		typedef TuneParameterTree									argument_type;
		typedef typename argument_type::parameter_type				range_type;
		typedef typename dataset_type::value_type					example_type;
		typedef WillowSplit<dataset_type, float_type, KERLoss>		splitter_type;
		typedef typename splitter_type::willow_node					willow_node;


	public:
		/** Constructs a default willow tree.
		 */
		WillowKMRStump() : Learner(SEMI|BINARY), subsetInt(1),
		  				   subsetSel(subsetInt, "Subset", range_type(0, 1, 1, '+'))
		{
		}
		/** Destructs a willow classifier.
		 */
		~WillowKMRStump()
		{
		}

	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map,  NAME_VERSION(WillowKMRStump));
				arginit(map, subsetInt, "subsetting", "groups nominal attributes for binary splits?");
			}
		}
		
	public:
		/** Constructs a deep copy of the willow tree model.
		 *
		 * @param tree a source willow tree model.
		 */
		WillowKMRStump(const WillowKMRStump& tree) : Learner(tree), subsetInt(tree.subsetInt),
													 subsetSel(subsetInt, tree.subsetSel), model(tree.model)
		{
		}
		/** Assigns a deep copy of the willow tree model.
		 *
		 * @param tree a source willow tree model.
		 * @return a reference to this object.
		 */
		WillowKMRStump& operator=(const WillowKMRStump& tree)
		{
			Learner::operator=(tree);
			model = tree.model;
			argument_copy(tree);
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			if( empty() ) return threshold();
			return model.predict(pred, example_type::missing());
		}
		/** Builds a model for the willow tree.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			splitter_type splitter(1.0f/learnset.size(),1.0);
			splitter.split(model, learnset, subsetInt==1);
			return 0;
		}
		/** Get the class name of the learner.
		 * 
		 * @return WillowKMRStump
		 */
		static std::string class_name()
		{
			return "WillowKMRStump";
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return WillowKMRStump
		 */
		static std::string name()
		{
			return "WillowKMRStump";
		}
		/** Gets the version of the willow tree classifier.
		 * 
		 * @return version.
		 */
		static int version()
		{
			return _WILLOWKMRSTUMP_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return kmr
		 */
		static std::string prefix()
		{
			return "kmr";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return subsetSel;
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return model.empty();
		}
		/** Clear an KMR stump model.
		 */
		void clear()
		{
			model.clear();
		}
		/** Copies arguments in a willow KMR stump.
		 * 
		 * @param tree a source willow KMR stump.
		 */
		void argument_copy(const WillowKMRStump& tree)
		{
			subsetInt = tree.subsetInt;
			subsetSel = tree.subsetSel;
			Learner::argument_copy(tree);
		}
		/** Makes a shallow copy of a Willow KMR Stump.
		 *
		 * @param ref a reference to a Willow KMR Stump.
		 */
		void shallow_copy(WillowKMRStump& ref)
		{
			//Learner::operator=(ref);
			model.shallow_copy(ref.model);
			argument_copy(ref);
		}
		/** Get the expected name of the program.
		 * 
		 * @return kmrstump
		 */
		static std::string progname()
		{
			return "kmrstump";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			model.accept(visitor);
		}

	public:
		/** Reads an willow tree from the input stream.
		 *
		 * @param in an input stream.
		 * @param tree a willow tree.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, WillowKMRStump& tree)
		{
			tree.errormsg(in, tree.model.read(in));
			return in;
		}
		/** Writes an willow tree to the output stream.
		 *
		 * @param out an output stream.
		 * @param tree a willow tree.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const WillowKMRStump& tree)
		{
			tree.errormsg(out, tree.model.write(out));
			return out;
		}

	private:
		int subsetInt;
		argument_type subsetSel;

	private:
		willow_node model;
	};

};

/** @brief Type utility interface to a Willow KMR stump
 * 
 * This class defines a type utility interface to a Willow KMR stump.
 */
template<class A, class C, class W>
struct TypeUtil< ::exegete::WillowKMRStump<A,C,W> >
{
	/** Flags class as non-primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::WillowKMRStump<A,C,W> value_type;
	/** This function tests if a string can be converted to a willow KMR stump.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return KMRStump
	 */
	static const char* name() 
	{
		return "KMRStump";
	}
};


#endif


