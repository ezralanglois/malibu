/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowTree.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWTREE_H
#define _EXEGETE_WILLOWTREE_H
#include "WillowSprout.h"
#include "WillowImpurity.h"
#include "Learner.h"
/** Defines the version of the Willow tree algorithm. 
 * @todo add to group
 */
#define _WILLOWTREE_VERSION 101000

/** @file WillowTree.h
 * @brief Native implementation of the Willow tree
 * 
 * This file contains the WillowTree class template.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Native implementation of the Willow tree
	 * 
 	 * This class template defines the Willow tree classifier.
	 *
	 * @todo add twoing, gain, gain ratio, twoing ordinal
	 */
	template<class A, class C, class W>
	class WillowTree : public Learner
	{
	public:
		/** Interface flags:
		 * 	- Tunable
		 * 	- Graphable
		 */
		enum{ TUNE=1, ARGUMENT=1, GRAPHABLE=1 };//, TUNE=0

	public:
		/** Defines a parent type. **/
		typedef void parent_type;
		/** Defines an example set as a learnset type. **/
		typedef ExampleSet<A, C, W>							dataset_type;
		/** Defines an example set as a learnset type. **/
		typedef ExampleSet<A, C, std::string*>				testset_type;
		/** Defines a learnset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a svm attribute value as a descriptor type. **/
		typedef typename dataset_type::attribute_type		descriptor_type;
		/** Defines the second template parameter as a float type. **/
		typedef double										float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator					argument_iterator;
		/** Defines a weight type. **/
		typedef W											weight_type;
		/** Defines a prediction type. **/
		typedef float_type 									prediction_type;
	private:
		typedef WillowSprout<dataset_type, float_type>		growth_type;
		typedef typename growth_type::argument_type			willow_args;
		typedef typename growth_type::willow_node			willow_node;
		typedef std::vector<growth_type*>					growth_vector;
		typedef TuneParameterTree							argument_type;
		typedef typename dataset_type::value_type			example_type;
		typedef typename argument_type::parameter_type		range_type;


	public:		
		/** Constructs a default willow tree.
		 */
		WillowTree() : Learner(SEMI|BINARY), impurityInt(3),
		  				impuritySel(impurityInt,	"Impurity", range_type(3, 4, 1, '+')),
		  				randsetSel(args.randsetInt,	"Subset", range_type(0, 0, 1, '+'), &impuritySel),
		  				depthSel(args.maxdepthInt,	"Depth", range_type(0, 0, 1, '+'), &randsetSel),
		  				confSel(args.confidenceFlt,	"Confidence", range_type(0.9f, 1, 0.1f, '+'), &depthSel)
		{
			init(growths);
		}
		/** Destructs a willow classifier.
		 */
		~WillowTree()
		{
			for(typename growth_vector::iterator beg=growths.begin(), end=growths.end();beg != end;++beg) delete *beg;
		}

	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{			
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(WillowTree) );
				arginit(map, args.minobjsInt,			"minobjs",		"minimum objects to split node", ArgumentMap::ADVANCED);
				arginit(map, args.probabilityInt,		"probability",  "estimate probabilities at leaf nodes?");
				//arginit(map, args.minweightFlt,		"minweight",	"minimum weight to split node");
				arginit(map, args.reuseInt,				"reuse",		"resuse the attributes durning growth?", ArgumentMap::ADVANCED);
				arginit(map, args.subsetInt,			"subsetting",	"groups nominal attributes for binary splits?");
				if(t>=0)
				{
				arginit(map, depthSel,					"depth",		"max depth to stop splitting");
				arginit(map, randsetSel,				"randsubset",	"use a random subset of attributes");
				arginit(map, impuritySel,				"impurity",		list("impurity criterion to choose the best split>", growths));
				arginit(map, confSel,					"confidence",	"a confidence value to exceed to split child node");
				}
				else
				{
				arginit(map, args.maxdepthInt,			"depth",		"max depth to stop splitting");
				arginit(map, args.randsetInt,			"randsubset",	"use a random subset of attributes");
				arginit(map, args.confidenceFlt,		"confidence",	"a confidence value to exceed to split child node");
				arginit(map, impurityInt,				"impurity",		list("impurity criterion to choose the best split>", growths));
				}
			}
		}
		
	public:		
		/** Constructs a deep copy of the willow tree model.
		 *
		 * @param tree a source willow tree model.
		 */
		WillowTree(const WillowTree& tree) : Learner(tree), args(tree.args), impurityInt(tree.impurityInt), 
											 impuritySel(impurityInt, tree.impuritySel),
											 randsetSel(impurityInt, tree.impuritySel, &impuritySel),
											 depthSel(impurityInt, tree.impuritySel, &randsetSel),
											 confSel(impurityInt, tree.impuritySel, &depthSel), 
											 model(tree.model)
		{
		}
		/** Assigns a deep copy of the willow tree model.
		 *
		 * @param tree a source willow tree model.
		 * @return a reference to this object.
		 */
		WillowTree& operator=(const WillowTree& tree)
		{
			Learner::operator=(tree);
			model = tree.model;
			argument_copy(tree);
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return args.probabilityInt?0.5f:0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			ASSERT(!empty());
			if( empty() ) return threshold();
			return model.predict(pred, example_type::missing());
		}
		/** Builds a model for the willow tree.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			ASSERT(!learnset.empty());
			ASSERT(impurityInt < int(growths.size()));
			growths[impurityInt]->sprout(model, learnset, args);
			ASSERT(!model.empty());
			return 0;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Willow
		 */
		static std::string name()
		{
			return "Willow";
		}
		/** Gets the version of the willow tree classifier.
		 * 
		 * @return version.
		 */
		static int version()
		{
			return _WILLOWTREE_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return wlw
		 */
		static std::string prefix()
		{
			return "wlw";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return impuritySel;
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return model.empty();
		}
		/** Clear an willow model.
		 */
		void clear()
		{
			model.clear();
		}
		/** Copies arguments in a willow tree.
		 * 
		 * @param tree a source willow tree.
		 */
		void argument_copy(const WillowTree& tree)
		{
			args = tree.args;
			impurityInt = tree.impurityInt;
			impuritySel = tree.impuritySel;
			randsetSel = tree.randsetSel;
			depthSel = tree.depthSel;
			confSel = tree.confSel;
			Learner::argument_copy(tree);
		}
		/** Makes a shallow copy of a Willow tree.
		 *
		 * @param ref a reference to a Willow tree.
		 */
		void shallow_copy(WillowTree& ref)
		{
			//Learner::operator=(ref);
			model.shallow_copy(ref.model);
			argument_copy(ref);
		}
		/** Get the class name of the learner.
		 * 
		 * @return WillowTree
		 */
		static std::string class_name()
		{
			return "WillowTree";
		}
		/** Get the expected name of the program.
		 * 
		 * @return wwillow or willow
		 */
		static std::string progname()
		{
#ifdef _WEIGHTED
			return "wwillow";
#else
			return "willow";
#endif
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			model.accept(visitor);
		}

	public:
		/** Reads an willow tree from the input stream.
		 *
		 * @param in an input stream.
		 * @param tree a willow tree.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, WillowTree& tree)
		{
			tree.model.clear();
			tree.errormsg(in, tree.model.read(in));
			return in;
		}
		/** Writes an willow tree to the output stream.
		 *
		 * @param out an output stream.
		 * @param tree a willow tree.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const WillowTree& tree)
		{
			tree.errormsg(out, tree.model.write(out));
			return out;
		}

	private:
		static void init(growth_vector& vect)
		{
			vect.push_back(new WillowSproutBFS<dataset_type, float_type, MSCLoss>());
			vect.push_back(new WillowSproutBFS<dataset_type, float_type, GINLoss>());
			vect.push_back(new WillowSproutBFS<dataset_type, float_type, GYNLoss>());
			vect.push_back(new WillowSproutBFS<dataset_type, float_type, ENTLoss>());
			vect.push_back(new WillowSproutBFS<dataset_type, float_type, KERLoss>());//Replace with KMR
			vect.push_back(new WillowSproutBFS<dataset_type, float_type, LSQLoss>());
			vect.push_back(new WillowSproutBFS<dataset_type, float_type, KMBLoss>());
			//
		}
		static std::string list(const char* str, const std::vector<growth_type*>& vect)
		{
			std::string temp=str;
			std::string index;
			for(unsigned int i=0;i<vect.size();++i)
			{
				temp+=vect[i]->name();
				temp+=":";
				valueToString(i, index);
				temp+=index;
				temp+=";";
			}
			temp +="*:0";
			return temp;
		}

	private:
		willow_args args;
		int impurityInt;
		argument_type impuritySel;
		argument_type randsetSel;
		argument_type depthSel;
		argument_type confSel;

	private:
		willow_node model;
		growth_vector growths;
	};

};

/** @brief Type utility interface to a Willow tree
 * 
 * This class defines a type utility interface to a Willow tree.
 */
template<class A, class C, class W>
struct TypeUtil< ::exegete::WillowTree<A,C,W> >
{
	/** Flags class as non-primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::WillowTree<A,C,W> value_type;
	/** Tests if a string can be converted to a willow tree.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return WillowTree
	 */
	static const char* name() 
	{
		return "WillowTree";
	}
};


#endif


