/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * aspen_impurity.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ASPEN_IMPURITY_HPP
#define _EXEGETE_ASPEN_IMPURITY_HPP
#include "typeutil.h"

/** @file aspen_impurity.hpp
 * 
 * @brief Split Impurity
 * 
 * This file contains tree split impurities.
 *
 * @ingroup ExegeteAspen
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Entropy measure
	 * 
	 * This class estimates the entropy measure.
	 */
	struct aspen_entropy
	{
		/** Estimate the gain of a split.
		 * 
		 * @param p a weight.
		 * @param sum total weight.
		 */
		template<class real>
		real gain(real p, real sum)
		{
			return -entropy(p/sum);
		}
		/** Estimate the full gain of a split.
		 * 
		 * @param positive weight.
		 * @param negative weight.
		 * @param total positive.
		 * @param total negative.
		 * @param eps epsilon factor.
		 * @return gain value.
		 */
		template<class real>
		real gain(real pl, real nl, real psum, real nsum, real eps)//impurity_type::gain(wpl, wnl, psum, nsum);
		{
			real pr = std::max(eps, psum - pl);
			real nr = std::max(eps, nsum - nl);
			real lft = pl + nl;
			real rgt = pr + nr;
			real tot = psum + nsum;
			return -(lgain(pl, lft, tot) + lgain(pr, rgt, tot));
		}
		/** Estimate the gain of a partial split.
		 * 
		 * @param pl a probability.
		 * @param lft weight of left.
		 * @param tot total weight.
		 * @return gain value.
		 */
		template<class real>
		real lgain(real pl, real lft, real tot)
		{
			return lft/tot * entropy(pl/lft);
		}
		/** Estimate the entorpy of a value.
		 * 
		 * @param v a probability value.
		 * @return entropy value.
		 */
		template<class real>
		real entropy(real v)
		{
			ASSERT(v<1.0);
		    return -v*std::log(v)-(1.0f-v)*std::log(1.0f-v);
		}
	};

};

#endif



