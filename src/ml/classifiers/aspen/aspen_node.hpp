/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * aspen_node.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ASPEN_NODE_HPP
#define _EXEGETE_ASPEN_NODE_HPP
#include "typeutil.h"

/** @file aspen_node.hpp
 * 
 * @brief An aspen tree node
 * 
 * This file contains an Aspen tree node.
 *
 * @ingroup ExegeteAspen
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief An Aspen tree node
	 * 
	 * This class serves as an Aspen tree node.
	 */
	template<class X, class F>
	class aspen_node
	{
		union partition
		{
			partition() : s(0) {}
			F f;
			unsigned int n;
			char* s;
		};
		
	protected:
		/** Defines an attribute type. **/
		typedef X 						attribute_type;
		/** Defines a constant attribute pointer. **/
		typedef const X* 				const_attribute_pointer;
		/** Defines a float type **/
		typedef union partition 		partition_type;
		/** Defines a size type **/
		typedef unsigned int 			size_type;
		/** Defines a size utility. **/
		typedef TypeUtil<size_type> 	size_util;
		
	public:
		/** Defines a node type. **/
		typedef unsigned int 			node_type;
		/** Defines a float type. **/
		typedef F 						float_type;
		/** Defines a node value type. **/
		typedef aspen_node<X, F> 		value_type;
		/** Defines a node pointer. **/
		typedef aspen_node<X, F>* 		pointer;
		/** Defines a node reference. **/
		typedef aspen_node<X, F>& 		reference;
		/** Defines a constant node reference. **/
		typedef const aspen_node<X, F>& const_reference;
		/** Node enumerated types. **/
		enum{ Leaf=-1, Real=0, Nominal=1, Subset=2 };
		
	public:
		/** Constructs an Aspen tree node.
		 */
		aspen_node() : indexInt(size_util::max()), orderInt(0), typeCh(Leaf), childernPtr(0)
		{
		}
		/** Destructs an Aspen tree node.
		 */
		~aspen_node()
		{
			clear_memory();
		}
		
	public:
		/** Constructs a deep copy of the node.
		 *
		 * @param n a node to copy.
		 */		
		aspen_node(const aspen_node& n) : partType(n.partType), indexInt(n.indexInt), 
										  orderInt(n.orderInt), typeCh(n.typeCh), childernPtr(0)
		{
			copy(n);
		}
		/** Assigns a deep copy of the Aspen node.
		 *
		 * @param n a node to copy.
		 * @return a reference to this object.
		 */
		aspen_node& operator=(const aspen_node& n)
		{
			if( is_subset_partition() ) ::erase(partType.s);
			partType = n.partType;
			indexInt = n.indexInt;
			typeCh = n.typeCh;
			orderInt = n.orderInt;
			copy(n);
			return *this;
		}
		/** Makes a shallow copy of the Aspen node.
		 *
		 * @param n a node to copy.
		 */
		void shallow_copy(aspen_node& n)
		{
			clear_memory();
			partType = n.partType;
			indexInt = n.indexInt;
			typeCh = n.typeCh;
			orderInt = n.orderInt;
			childernPtr = n.childernPtr;
			if( is_subset_partition() ) n.partType.s = 0;
			n.childernPtr=0;
		}
		
	public:
		/** Gets a child in the array.
		 *
		 * @param n a child index.
		 * @return a reference to a child node.
		 */
		reference operator[](size_type n)
		{
			ASSERT(childernPtr!=0);
			ASSERTMSG(n < size(), n << " < " << size() << " - " << int(typeCh));
			return childernPtr[n];
		}
		/** Gets a child in the array.
		 *
		 * @param n a child index.
		 * @return a reference to a child node.
		 */
		const_reference operator[](size_type n)const
		{
			ASSERT(childernPtr!=0);
			ASSERTMSG(n < size(), n << " < " << size() << " - " << int(typeCh));
			return childernPtr[n];
		}
		
	public:
		/** Predicts the binary class of the attribute vector.
		 *
		 * @param p an attribute vector.
		 * @param m an internal missing flag.
		 * @return a confidence/probability for a binary class.
		 */
		float_type predict(const_attribute_pointer p, X m)const
		{
			if( int(typeCh) == Leaf ) return threshold();
			if( ismissing(p,m) )
			{
				size_type n=size();
				float_type sum=0.0f;
				for(size_type i=0;i<n;++i) sum+=childernPtr[i].predict(p,m);
				return sum / n;
			}
			ASSERT(childernPtr != 0);
			switch(int(typeCh))
			{
			case Real:
				return childernPtr[ p[indexInt] < partType.f?0:1 ].predict(p,m);
			case Nominal:
				return childernPtr[ size_type(p[indexInt]) ].predict(p,m);
			default://Subset
				ASSERTMSG(partType.s != 0, typeCh);
				return childernPtr[ getvalue(partType.s, size_type(p[indexInt]), typeCh) ].predict(p,m);
			};
		}
		/** Tests if attribute is missing (new nominal).
		 *
		 * @param p an attribute vector.
		 * @param missing key for missing value.
		 * @return true if the value is not found.
		 */
		bool ismissing(const_attribute_pointer p, X missing)const
		{
			if( int(typeCh) != Leaf && p[indexInt] == missing ) return true;
			switch(int(typeCh))
			{
			case Leaf:
				return false;
			case Real:
				return false;
			case Nominal:
				return size_type(p[indexInt]) >= partType.n;
			default://Subset
				return size_type(p[indexInt]) >= typeCh;
			};
		}
		/** Accept a vistor class (part of vistor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
//			if( visitor.visit(*this) )
//			{
//				for(unsigned int i=0, n=size();i<n;++i) childernPtr[i].accept(visitor);
//			}
		}
		/** Clears this node.
		 */
		void clear()
		{
			clear_memory();
			indexInt = TypeUtil<size_type>::max();
		}
		
	public:
		/** Tests if node is empty.
		 *
		 * @return true if node is empty.
		 */
		bool empty()const
		{
			return indexInt == TypeUtil<size_type>::max();
		}
		/** Gets the number of childern.
		 *
		 * @return number of childern.
		 */
		size_type size()const
		{
			switch(int(typeCh))
			{
			case Real:
				return 2;
			case Leaf:
				return empty() ? 0 : indexInt;
			case Nominal:
				return partType.n;
			default:
				return 2;
			};
		}
		/** Sets an attribute value threshold split.
		 *
		 * @param i an value threshold split.
		 */
		void threshold(float_type i)
		{
			partType.f = i;
		}
		/** Gets an attribute value threshold split.
		 *
		 * @return an attribute value threshold split.
		 */
		float_type threshold()const
		{
			return partType.f;
		}
		/** Sets the type of this node.
		 *
		 * @param ch a node type.
		 */
		void type(node_type ch)
		{
			typeCh = ch;
		}
		/** Gets the type of this node.
		 *
		 * @return a node type.
		 */
		node_type type()const
		{
			return typeCh;
		}
		/** Sets an attribute split index.
		 *
		 * @param i an attribute index.
		 */
		void index(size_type i)
		{
			indexInt = i;
		}
		/** Gets an attribute split index.
		 *
		 * @return an attribute index.
		 */
		size_type index()const
		{
			return indexInt;
		}
		/** Sets an order index.
		 *
		 * @param i an order index.
		 */
		void order(size_type i)
		{
			orderInt = i;
		}
		/** Gets an order index.
		 *
		 * @return an order index.
		 */
		size_type order()const
		{
			return orderInt;
		}
		
	protected:
		/** Clears only the memory.
		 */
		void clear_memory()
		{
			if( is_subset_partition() ) ::erase(partType.s);
			::erase(childernPtr);
		}
		/** Tests if partition uses subsetting.
		 * 
		 * @return true if partition uses subsetting.
		 */
		bool is_subset_partition()const
		{
			return ( int(typeCh) != Leaf && int(typeCh) > Nominal );
		}
		/** Makes a deep copy of this node.
		 *
		 * @param n a node to copy.
		 */
		void copy(const aspen_node& n)
		{
			if( n.childernPtr == 0 ) return;
			size_type len = size();
			::erase(childernPtr);
			childernPtr = ::setsize(childernPtr, len);
			for(size_type i=0;i<len;++i) childernPtr[i] = n.childernPtr[i];
			if( n.is_subset_partition() )
			{
				size_type m = typeCh/sizeof(char);
				partType.s = setsize<char>(m);
				std::copy(n.partType.s, n.partType.s+m, partType.s);
			}
		}
		
	public:
		/** Gets the subset value flag.
		 *
		 * @param s big subset flags.
		 * @param n index of subset bit.
		 * @param len length of subset.
		 * @return subset flag at index.
		 */
		static size_type getvalue(char* s, size_type n, size_type len)
		{
			size_type m = n/sizeof(char);
			n = n - m*sizeof(char);
			ASSERT(s != 0);
			ASSERTMSG(m<(len/sizeof(char)), m << " < " << len);
			return ((s[m]>>n)&1);
		}
		/** Gets the subset value flag.
		 *
		 * @param s big subset flags.
		 * @param n index of subset bit.
		 * @param val subset flag for index.
		 */
		static void setvalue(char* s, size_type n, char val)
		{
			size_type m = n/sizeof(char);
			n = n - m*sizeof(char);
			if( val ) s[m] |= (1<<n);
			else s[m] &= ~(1<<n);
		}
		/** Get subsetting char array.
		 * 
		 * @param n number of nominal values.
		 * @return character array.
		 */
		static char* subsetting(size_type n)
		{
			size_type len = n/sizeof(char);
			char* subset = setsize<char>(len);
			for(size_type i=0;i<len;++i) subset[i] = 0;
			return subset;
		}
		
	public:
		/** Sets value for a branch.
		 *
		 * @param t a threshold for ordered splits.
		 * @param i an attribute index.
		 * @param n number of nominal attributes.
		 * @param s a subsetting string.
		 */
		void branch(float_type t, size_type i, size_type n, char* s)
		{
			index(i);
			resize(n, s);
			if( n == Real ) threshold(t);
		}
		/** Resize the number of childern.
		 *
		 * @param n number of childern.
		 * @param s a subsetting string.
		 */
		void resize(size_type n, char* s)
		{
			if( is_subset_partition() ) ::erase(partType.s);
			if( n == Real ) 
			{
				typeCh = Real;
				n = 2;
			}
			else if( s != 0 )
			{
				ASSERTMSG(n>Nominal, n << " > " << Nominal);
				typeCh = n;
				partType.s = s;
				n = 2;
			}
			else 
			{
				typeCh = Nominal;
				partType.n = n;
			}
			erase(childernPtr);
			childernPtr = setsize(childernPtr, n);
		}
		/** Sets a value confidence.
		 *
		 * @param i a value confidence.
		 */
		void leaf(float_type i)
		{
			if( is_subset_partition() ) ::erase(partType.s);
			partType.f = i;
			typeCh = Leaf;
			indexInt = 0;
		}
		
	public:
		/** Reads an Aspen node from the input stream.
		 *
		 * @param in an input stream.
		 * @return an error message or NULL
		 */
		const char* read(std::istream& in)
		{
			if( in.eof() ) return ERRORMSG("Unexpected end of file while reading Aspen tree");
			char ch=in.get();
			if( in.get() != ' ' ) return ERRORMSG("Missing space after node type");
			switch(ch)
			{
			case 'L':
				typeCh=Leaf;
				in >> indexInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after real node index");
				in >> partType.f;
				if( in.get() != '\n' ) return ERRORMSG("Missing newline after real node threshold");
				break;
			case 'R':
				typeCh=Real;
				in >> orderInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after order index");
				in >> indexInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after real node index");
				in >> partType.f;
				if( in.get() != '\n' ) return ERRORMSG("Missing newline after real node threshold");
				break;
			case 'N':
				typeCh=Nominal;
				in >> orderInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after order index");
				in >> indexInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after nominal node index");
				in >> partType.n;
				if( in.get() != '\n' ) return ERRORMSG("Missing newline after nominal node count");
				break;
			case 'S':
				in >> orderInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after order index");
				in >> indexInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after nominal subset node index");
				in >> typeCh;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after nominal subset node subset size");
				partType.s = subsetting(typeCh);
				for(size_type i=0;i<typeCh;++i) setvalue(partType.s, i, in.get());
				break;
			default:
				return ERRORMSG("Unknown node type: \"" << ch << "\"");
			};
			size_type n = size();
			erase(childernPtr);
			childernPtr = setsize(childernPtr, n);
			const char* msg;
			for(size_type i=0;i<n;++i) if( (msg=childernPtr[i].read(in)) != 0 ) return msg;
			return 0;
		}
		/** Writes an Aspen node to the output stream.
		 *
		 * @param out an output stream.
		 * @return an error message or NULL.
		 */
		const char* write(std::ostream& out)const
		{
			switch(int(typeCh))
			{
			case Leaf:
				out << 'L' << " " << indexInt << " " << partType.f << "\n";
				break;
			case Real:
				out << 'R' << " " << orderInt << " " << indexInt << " " << partType.f << "\n";
				break;
			case Nominal:
				out << 'N' << " " << orderInt << " " << indexInt << " " << partType.n << "\n";
				break;
			default:// Subset
				out << 'S' << " " << orderInt << " " << indexInt << " " << typeCh << " ";
				ASSERT(partType.s != 0);
				for(size_type i=0;i<typeCh;++i) out << getvalue(partType.s, i, typeCh);
				out << "\n";
				break;
			};
			const char* msg;
			ASSERTMSG(childernPtr!=0||size()==0, "typeCh="<<typeCh << " - " << size());
			for(unsigned int i=0, n=size();i<n;++i) if( (msg=childernPtr[i].write(out)) != 0 ) return msg;
			return 0;
		}
		
	private:
		partition_type partType;
		size_type indexInt;
		size_type orderInt;
		node_type typeCh;

	private:
		pointer childernPtr;
	};
};


#endif


