/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * aspen_partition.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ASPEN_PARTITION_HPP
#define _EXEGETE_ASPEN_PARTITION_HPP
#include "type_traits.hpp"
#include "aspen_node.hpp"

/** @file aspen_partition.hpp
 * 
 * @brief Holds the best partition
 * 
 * This file contains a class to hold the best partition.
 *
 * @ingroup ExegeteAspen
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Holds the best partition
	 * 
	 * This class holds the best partition.
	 */
	template<class dataset>
	class aspen_partition
	{
		typedef typename dataset::size_type 				size_type;
		typedef typename dataset::attribute_type 			attribute_type;
		typedef typename dataset::weight_type 				w_type;
		typedef select_not_void<w_type,double>				select_weight;
		typedef typename select_weight::result				weight_type;
	public:
		/** Defines a feature type. **/
		typedef typename dataset::feature_type 				feature_type;
		/** Defines an index type. **/
		typedef typename dataset::size_type 				index_type;
		/** Defines a node type. **/
		typedef aspen_node<feature_type, weight_type>		node_type;
		/** Defines a float type. **/
		typedef typename node_type::float_type				float_type;
		/** Defines a positive vector. **/
		typedef std::vector< std::pair<float_type, size_type> > positive_vector;
		/** Defines a negative vector. **/
		typedef std::vector< float_type > 					negative_vector;
	private:
		typedef type_limits<index_type> 					index_util;
		
	public:
		/** Constructs an Aspen partition.
		 */
		aspen_partition() : subsetPtr(0), attrIdx(index_util::max()), lenInt(0)
		{
		}
		/** Destructs an Aspen partition.
		 */
		~aspen_partition()
		{
		}
		
	public:
		/** Create a leaf node.
		 * 
		 * @param node a tree node.
		 */
		void create_leaf(node_type& node, float_type conf)
		{
			node.leaf(conf);
		}
		/** Create a branch node.
		 * 
		 * @param node a branch node.
		 */
		void create_branch(node_type& node)
		{
			node.branch(threshFlt, attrIdx, lenInt, subsetPtr);
		}
		
	public:
		/** Get partition a value belongs to.
		 * 
		 * @param v an attribute value.
		 * @return partition index.
		 */
		size_type belong_to(float_type v)const
		{
			if( lenInt == node_type::Real )
			{
				if( threshFlt < 0 ) return v > threshFlt ? 0 : 1;
				return v > threshFlt ? 1 : 0;
			}
			else
			{
				return node_type::getvalue(subsetPtr, size_type(v), lenInt);
			}
		}
		/** Update the best split.
		 * 
		 * @param a an attribute index.
		 * @param v an attribute value.
		 * @param p a positive weight.
		 * @param n a negative weight.
		 * @param g a gain value.
		 */
		void update(index_type a, feature_type v, float_type p, float_type n, float_type g)
		{
			if( lenInt > 1 ) ::erase(subsetPtr);
			lenInt = node_type::Real;
			attrIdx = a;
			threshFlt = v;
			gainFlt = g;
			posFlt = p;
			negFlt = n;
		}
		/** Update the best split.
		 * 
		 * @param a an attribute index.
		 * @param v an attribute value.
		 * @param p a positive weight.
		 * @param n a negative weight.
		 * @param g a gain value.
		 * @param posvec subsetting indices.
		 */
		void update(index_type a, feature_type v, float_type p, float_type n, float_type g, positive_vector& posvec)
		{
			if( lenInt > 1 ) ::erase(subsetPtr);
			lenInt = posvec.size();
			attrIdx = a;
			subsetPtr = node_type::subsetting(lenInt);
			gainFlt = g;
			posFlt = p;
			negFlt = n;
			threshFlt = 1.0f;
			for(size_type i=0;i<lenInt;++i) node_type::setvalue(subsetPtr, posvec[i].second, i>=v);
		}
		/** Set the sum weights of the current partition.
		 * 
		 * @param p partition.
		 * @param i current.
		 */
		void set_weights(const aspen_partition& p, size_type i, float_type g=0.0f)
		{
			psumFlt = p.psum(i);
			nsumFlt = p.nsum(i);
			gainFlt = g;
		}
		/** Set the positive and negative sums.
		 * 
		 * @param p a positive sum.
		 * @param n a negative sum.
		 */
		void set(float_type p, float_type n, float_type g)
		{
			psumFlt = p;
			nsumFlt = n;
			gainFlt = g;
		}
		/** Get the best split index.
		 * 
		 * @return best split index.
		 */
		index_type best_index()const
		{
			return attrIdx;
		}
		/** Get the positive sum.
		 * 
		 * @return positive sum.
		 */
		float_type psum()const
		{
			return psumFlt;
		}
		/** Get the negative sum.
		 * 
		 * @return negative sum.
		 */
		float_type nsum()const
		{
			return nsumFlt;
		}
		/** Get the positive sum.
		 * 
		 * @return positive sum.
		 */
		float_type psum(size_type i)const
		{
			if( i == 0 ) return posFlt;
			return psumFlt - posFlt;
		}
		/** Get the negative sum.
		 * 
		 * @return negative sum.
		 */
		float_type nsum(size_type i)const
		{
			if( i == 0 ) return negFlt;
			return nsumFlt - negFlt;
		}
		/** Test if weights in split fall below some threshold.
		 * 
		 * @param minw minimum weight threshold.
		 * @return true if a child has minimum weight.
		 */
		bool minweight(float_type minw)const
		{
			return ( posFlt < minw && negFlt < minw ) || 
				   ( (psumFlt-posFlt) < minw && (nsumFlt-negFlt) < minw );
		}
		/** Get the negative sum.
		 * 
		 * @return negative sum.
		 */
		float_type threshold()const
		{
			return threshFlt;
		}
		/** Get the maximum gain.
		 * 
		 * @return maximum gain.
		 */
		float_type gain()const
		{
			return gainFlt;
		}
		
	public:
		/** Set the partition.
		 * 
		 * @param b partition start.
		 * @param e partition end.
		 */
		void set_partition(size_type b)
		{
			partBeg = b;
		}
		/** Get partition start.
		 * 
		 * @return partition start.
		 */
		size_type partition_begin()const
		{
			return partBeg;
		}
		
	private:
		float_type threshFlt;
		char* subsetPtr;
		index_type attrIdx;
		float_type gainFlt;
		size_type lenInt;
		
	private:
		float_type posFlt;
		float_type negFlt;
		float_type psumFlt;
		float_type nsumFlt;
		
	private:
		size_type partBeg;
	};
};

#endif

