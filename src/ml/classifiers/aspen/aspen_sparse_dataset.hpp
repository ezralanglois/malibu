/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * aspen_sparse_dataset.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ASPEN_SPARSE_DATASET_HPP
#define _EXEGETE_ASPEN_SPARSE_DATASET_HPP

/** @file aspen_sparse_dataset.hpp
 * 
 * @brief Dataset for sparse aspen
 * 
 * This file contains a class to hold examples for a sparse aspen
 *
 * @ingroup ExegeteAspen
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Dataset for sparse aspen
	 * 
	 * This class defines a dataset for a sparse aspen tree.
	 */
	template< class D, class W, class X=std::pair< typename D::feature_type, typename D::size_type > >
	class aspen_sparse_dataset
	{
		typedef D 												dataset_type;
	public:
		/** Defines an attribute and example values **/
		typedef X												value_type;
		/** Defines an attribute utility. **/
		typedef AttributeTypeUtil<X>							type_util;
		/** Defines a sparse pointer. **/
		typedef value_type*										sparse_pointer;
		/** Defines an attribute pointer. **/
		typedef sparse_pointer*									attribute_pointer;
		/** Defines a size type. **/
		typedef size_t											size_type;
		/** Defines a size pointer. **/
		typedef size_type*										size_pointer;
		/** Defines a reference. **/
		typedef value_type&										reference;
		/** Defines a reference. **/
		typedef const value_type&								const_reference;
		
	private:
		typedef typename dataset_type::class_type 				class_type;
		typedef typename dataset_type::attribute_type 			attribute_type;
		typedef typename dataset_type::const_header_iterator 	const_header_iterator;
		typedef W												weight_type;
		typedef weight_type* 									weight_pointer;
		typedef class_type* 									class_pointer;
		
	public:
		/** Constructs a sparse aspen dataset.
		 */
		aspen_sparse_dataset() : y_ptr(0), w_ptr(0), a_ptr(0), s_ptr(0), e_cnt(0), a_cnt(0), a_tot(0)
		{
		}
		/** Destructs a sparse aspen dataset.
		 */
		~aspen_sparse_dataset()
		{
			if( a_ptr != 0 ) ::erase(*a_ptr);
			::erase(a_ptr);
			::erase(s_ptr);
			::erase(y_ptr);
			::erase(w_ptr);
		}
		
	public:
		/** Update a sparse Aspen dataset.
		 * 
		 * @param dataset an example set.
		 */
		void update(dataset_type& dataset)
		{
		}
		/** Create a sparse Aspen dataset.
		 * 
		 * @param dataset an example set.
		 */
		template<class Y1, class W1>
		void assign(ExampleSet<attribute_type,Y1,W1>& dataset)
		{
			y_ptr = ::resize(y_ptr, dataset.size());
			w_ptr = ::resize(w_ptr, dataset.size());
			resize_impl(dataset);
			std::fill(s_ptr, s_ptr+dataset.attributeCount(), 0);
			fill_dataset(dataset);
			
			sort_dataset(dataset.header_begin(), dataset.header_end(), a_ptr);
			
			e_cnt = dataset.size();
		}
		
	public:
		/** Get start of list of examples for an attribute.
		 * 
		 * @param n attribute index.
		 */
		sparse_pointer begin(size_type n)
		{
			return a_ptr[n];
		}
		/** Get end of list of examples for an attribute.
		 * 
		 * @param n attribute index.
		 */
		sparse_pointer end(size_type n)
		{
			return a_ptr[n]+s_ptr[n];
		}
		/** Get a weight for example i.
		 * 
		 * @param i example index.
		 * @return weight at index.
		 */
		weight_type& w(size_type i)
		{
			return w_ptr[i];
		}
		/** Get a class for example i.
		 * 
		 * @param i example index.
		 * @return class at index.
		 */
		class_type y(size_type i)const
		{
			return y_ptr[i];
		}
		/** Get the number of examples.
		 * 
		 * @return example count.
		 */
		size_type example_count()const
		{
			return e_cnt;
		}
		
	private:
		static bool compare_sparse(const_reference lhs, const_reference rhs)
		{
			return type_util::valueOf(lhs) < type_util::valueOf(rhs);
		}
		void sort_dataset(const_header_iterator beg, const_header_iterator end, attribute_pointer it)
		{
			for(;beg != end;++beg, ++it)
			{
				if( beg->isunsorted() ) continue;
				std::stable_sort(*it, *(it+1), compare_sparse);
			}
		}
		template<class Y1, class W1>
		void fill_dataset(const ExampleSet<attribute_type,Y1,W1>& dataset)
		{
			typedef typename ExampleSet<attribute_type,Y1,W1>::const_iterator 			const_iterator;
			typedef typename ExampleSet<attribute_type,Y1,W1>::const_attribute_pointer 	const_attribute_pointer;
			typedef typename ExampleSet<attribute_type,Y1,W1>::type_util 				dataset_util;
			typedef typename dataset_util::index_type 									index_type;
			const_attribute_pointer pattr;
			size_type m=0;
			index_type n;
			for(const_iterator beg = dataset.begin(), end = dataset.end();beg != end;++beg, ++m)
			{
				pattr=beg->x();
				y_ptr[m] = beg->y();
				n = dataset_util::indexOf(*pattr);
				while( n != -1 )
				{
					n--;
					type_util::indexOf(a_ptr[n][s_ptr[n]]) = m;
					type_util::valueOf(a_ptr[n][s_ptr[n]]) = dataset_util::valueOf(*pattr);
					s_ptr[n]++;
					++pattr;
					n = dataset_util::indexOf(*pattr);
				}
			}
		}
		template<class Y1, class W1>
		size_type get_size(const ExampleSet<attribute_type,Y1,W1>& dataset)
		{
			typedef typename ExampleSet<attribute_type,Y1,W1>::const_attribute_pointer 	const_attribute_pointer;
			typedef typename ExampleSet<attribute_type,Y1,W1>::const_iterator 			const_iterator;
			typedef typename ExampleSet<attribute_type,Y1,W1>::type_util 				dataset_util;
			typedef typename dataset_util::index_type 									index_type;
			size_type m = dataset.attributeCount();
			s_ptr = ::resize(s_ptr, m);
			std::fill(s_ptr, s_ptr+m, 0);
			size_type tot=0;
			index_type n;
			const_attribute_pointer pattr;
			for(const_iterator beg = dataset.begin(), end = dataset.end();beg != end;++beg)
			{
				pattr=beg->x();
				ASSERTMSG(pattr != 0, dataset.size());
				n = dataset_util::indexOf(*pattr);
				while( n != -1 )
				{
					n--;
					s_ptr[n]++;
					++pattr;
					++tot;
					n = dataset_util::indexOf(*pattr);
				}
			}
			return tot;
		}
		template<class Y1, class W1>
		void resize_impl(const ExampleSet<attribute_type,Y1,W1>& dataset)
		{
			size_type tot = get_size(dataset);
			size_type n = dataset.attributeCount();
			sparse_pointer val = 0;
			if( a_ptr != 0 && a_tot != tot )
			{
				val = *a_ptr;
				//if( tot < a_tot ) erase_all(val+tot, val+a_tot);
			}
			a_ptr = ::resize(a_ptr, n+1);
			*a_ptr = val;
			if(a_tot != tot) *a_ptr = ::resize(*a_ptr, tot);
			for(size_type i=1;i<n+1;++i) a_ptr[i] = a_ptr[i-1]+s_ptr[i-1];
			val = *a_ptr;
			//if( tot > a_tot ) init_all(val+a_tot, val+tot);
			a_tot = tot;
			a_cnt = n;
		}
		/*static void init_all(sparse_pointer beg, sparse_pointer end)
		{
			for(;beg != end;++beg) *beg = 0;
		}
		static void erase_all(sparse_pointer beg, sparse_pointer end)
		{
			for(;beg != end;++beg) ::erase(*beg);
		}*/
		
	private:
		class_pointer y_ptr;
		weight_pointer w_ptr;
		attribute_pointer a_ptr;
		size_pointer s_ptr;
		size_type e_cnt;
		size_type a_cnt;
		size_type a_tot;
	};
};

#endif

