/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * aspen_split.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ASPEN_SPLIT_HPP
#define _EXEGETE_ASPEN_SPLIT_HPP
#include "aspen_sparse_dataset.hpp"
#include "aspen_partition.hpp"

/** @file aspen_split.hpp
 * 
 * @brief Determines the best split
 * 
 * This file contains a class to determine the best split.
 *
 * @ingroup ExegeteAspen
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Determines the best split
	 * 
	 * This class determines the best split.
	 */
	template<class dataset, class impurity>
	class aspen_split : public impurity
	{
		typedef impurity										impurity_type;
		typedef dataset 				 						dataset_type;
		typedef typename dataset_type::type_util				dataset_util;
		typedef typename dataset_util::attribute_type			attribute_type;
		//typedef typename dataset_util::value_type				feature_type;
		typedef typename dataset_type::class_type				class_type;
	public:
		/** Defines a partition type. **/
		typedef aspen_partition<dataset> 						partition_type;
		/** Defines a feature type. **/
		typedef typename partition_type::feature_type			feature_type;
	private:
		typedef typename partition_type::float_type 			weight_type;
		typedef aspen_sparse_dataset<dataset, weight_type> 		sparse_dataset_type;
		typedef typename sparse_dataset_type::type_util 		type_util; 
		typedef typename type_util::value_type					value_type;
		//typedef typename type_util::index_type					index_type;
		typedef typename sparse_dataset_type::sparse_pointer 	sparse_pointer;
		typedef TypeUtil<value_type>							value_util;
	public:
		/** Defines a size type. **/
		typedef typename dataset_type::size_type 		size_type;
		/** Defines a float type. **/
		typedef typename partition_type::float_type 	float_type;
		/** Defines a attribute index type. **/
		typedef typename partition_type::index_type 	index_type;
		
	public:
		/** Constructs an Apsen split.
		 */
		aspen_split() : o_vec(0), f_vec(0), f_cnt(0), valid(0)
		{
		}
		/** Destructs an Apsen split.
		 */
		~aspen_split()
		{
			::erase(o_vec);
			::erase(valid);
			::erase(f_vec);
		}
		
	public:
		/** Setup an Aspen split.
		 * 
		 * @param set a dataset.
		 */
		template<class Y1, class W1>
		void setup_dataset(ExampleSet<attribute_type,Y1,W1>& set)
		{
			sprdataset.assign(set);
			valid = ::resize(valid, set.size());
			std::fill(valid, valid+set.size(), 0);
		}
		/** Update an Aspen split.
		 * 
		 * @param set a dataset.
		 */
		void update_dataset(dataset_type& set)
		{
			sprdataset.update(set);
		}
		/** Finalize the dataset.
		 * 
		 * @param set a dataset.
		 */
		void finalize(dataset_type& set)
		{
			typedef typename dataset_type::const_iterator const_iterator;
			size_type j;
			for(const_iterator beg = set.begin(), end = set.end();beg != end;++beg)
			{
				j = size_type( dataset_util::valueOf(beg->x()[-1]) );
				valid[j]=0;
			}
		}
		/** Get the confidence in prediction.
		 * 
		 * @param prt a partition.
		 * @return confidence.
		 * @param eps a smooth factor.
		 */
		float_type confidence(partition_type& prt, float_type eps)
		{
			float_type p = std::max(eps,  prt.psum());
			float_type n = std::max(eps,  prt.nsum());
			return p / ( p + n );
		}
		/** Initialize the partition and valid array.
		 * 
		 * @param prt a partition.
		 * @param set a dataset.
		 */
		void init(partition_type& prt, dataset_type& set)
		{
			typedef typename dataset_type::const_iterator const_iterator;
			float_type wpl=0, wnl=0, w, inv = 1.0 / set.size();
			class_type y;
			size_type j;
			
			for(const_iterator beg = set.begin(), end = set.end();beg != end;++beg)
			{
				y = beg->y();
				w = beg->weight(inv);
				j = size_type( dataset_util::valueOf(beg->x()[-1]) );
				if( y > 0 ) wpl += w;
				else wnl += w;
				valid[j]=1;
				sprdataset.w(j) = w;
			}
			w=wpl+wnl;
			prt.set(wpl, wnl, impurity_type::gain(wpl, w));
		}
		/** Partition examples based on best split.
		 * 
		 * @param old_prt parent partition.
		 * @param new_prt current partition.
		 * @param i partition index.
		 */
		void update_split(partition_type& old_prt, partition_type& new_prt, size_type i)
		{
			sparse_pointer beg = sprdataset.begin(old_prt.best_index()), cur=beg;
			sparse_pointer end = sprdataset.end(old_prt.best_index());
			if( i == 0 )
			{
				sparse_pointer fin=end, mid;
				while(cur < fin) // binary search 
				{
					//mid = (cur+fin) / 2;
					mid = beg + ( (std::distance(beg, cur)+std::distance(beg, fin)) / 2 );
					if( old_prt.belong_to(type_util::valueOf(*mid)) == 1 )  fin = mid;
					else cur = mid + 1;
				}
				old_prt.set_partition(std::distance(beg,cur));
				for(;cur != fin;++cur) valid[ type_util::indexOf(*cur) ] -= 1;
			}
			else if( i == 1 )
			{
				cur=beg+old_prt.partition_begin();
				for(;cur != end;++cur) valid[ type_util::indexOf(*cur) ] += 2;
				for(size_type i=0, n=sprdataset.example_count();i<n;++i) valid[i] -= 1;
			}
			else
			{
				cur=beg+old_prt.partition_begin();
				for(;cur != end;++cur) valid[ type_util::indexOf(*cur) ] -= 1;
				for(size_type i=0, n=sprdataset.example_count();i<n;++i) valid[i] += 1;
			}
		}
		/** Converts an attribute vector.
		 * 
		 * @param ptr a source attribute vector.
		 * @param a_cnt number of attributes.
		 * @return a destination attribute vector.
		 */
		feature_type* convert_attribute(const attribute_type* ptr, size_type a_cnt)const
		{
			if( a_cnt != f_cnt )
			{
				f_vec = ::resize(f_vec, a_cnt);
				std::fill(f_vec, f_vec+a_cnt, 0);
				o_vec = ::resize(o_vec, a_cnt);
				dataset_util::indexOf(*o_vec)=-1;
				f_cnt = a_cnt;
			}
			typename dataset_util::index_type idx;
			attribute_type* o_curr = o_vec;
			while( (idx=dataset_util::indexOf(*ptr)) != -1 ) f_vec[ idx ] = 0;
			o_curr = o_vec;
			while( (idx=dataset_util::indexOf(*ptr)) != -1 )
			{
				*o_curr = *ptr;
				f_vec[ idx ] = dataset_util::valueOf(*ptr);
				++ptr;
				++o_curr;
			}
			*o_curr = *ptr;
			return f_vec;
		}
	private:
		mutable attribute_type* o_vec;
		mutable feature_type* f_vec;
		mutable size_type f_cnt;
		
	public:
		/** Finds the best partition in a set of examples on an unordered attribute.
		 * 
		 * @param prt a partition.
		 * @param set a dataset.
		 * @param attr the attribute to split on.
		 * @param eps a smooth factor.
		 */
		void split_unordered(partition_type& prt, dataset_type& set, int attr, float_type eps)
		{
			typedef typename partition_type::positive_vector positive_vector;
			typedef typename partition_type::negative_vector negative_vector;
			positive_vector posvec(set.attributeAt(attr).size());
			negative_vector negvec(set.attributeAt(attr).size());
			value_type thr = value_util::max();
			sparse_pointer beg = sprdataset.begin(attr), cur;
			sparse_pointer end = sprdataset.end(attr);
			size_type prev_value, curr_value;
			float_type max_wpl, max_wnl;
			float_type wpl=0, wnl=0;
			value_type val;
			float_type gain;
			index_type cur_idx, prv_idx;
			float_type max_gain = prt.gain();
			float_type psum = prt.psum();
			float_type nsum = prt.nsum();
			float_type pzsum=0, nzsum=0;

			for(;beg != end;++beg)
			{
				if( is_attribute_missing(*beg) ) break;
				cur_idx = type_util::indexOf(*beg);
				if( valid[cur_idx] > 0 )
				{
					prv_idx = cur_idx;
					break;
				}
			}
			if( beg == end ) return;
			for(size_type i=0;i<posvec.size();++i)
			{
				posvec[i].first = 0.0f;
				posvec[i].second = i;
			}
			for(cur=beg;cur != end;++cur)
			{
				val = type_util::valueOf(*cur);
				if( val == type_util::missing() ) continue;
				cur_idx = type_util::indexOf(*cur);
				if( valid[cur_idx] <= 0 ) continue;
				if( sprdataset.y(cur_idx) > 0 )
				{
					posvec[size_type(val)].first += sprdataset.w(cur_idx);
					pzsum += sprdataset.w(cur_idx);
				}
				else
				{
					negvec[size_type(val)] += sprdataset.w(cur_idx);
					nzsum += sprdataset.w(cur_idx);
				}
			}
			for(;cur != end;++cur)
			{
				cur_idx = type_util::indexOf(*cur);
				if( valid[cur_idx] <= 0 ) continue;
				if( sprdataset.y(cur_idx) > 0 )
					psum -= sprdataset.w(cur_idx);
				else
					nsum -= sprdataset.w(cur_idx);
			}
			pzsum = std::max(eps, psum - pzsum);
			nzsum = std::max(eps, nsum - nzsum);
			posvec[0].first = pzsum;
			negvec[0] = nzsum;
			std::stable_sort(posvec.begin(), posvec.end());
			prv_idx = 0;
			prev_value = posvec[0].second;
			for(size_type i=1;i<posvec.size();++i)
			{
				curr_value = posvec[i].second;
				if( sprdataset.y(prv_idx) > 0 )
					wpl += posvec[prv_idx].first;
				else
					wnl += negvec[prev_value];
				if( prev_value != curr_value )
				{
					gain = impurity_type::gain(wpl, wnl, psum, nsum, eps);
					if( gain > max_gain )
					{
						thr = (prev_value+curr_value) * 0.5;
						max_gain = gain;
						max_wpl = wpl;
						max_wnl = wnl;
					}
				}
				prev_value = curr_value;
				prv_idx = i;
			}
			if( thr != value_util::max() )
			{
				prt.update(attr, thr, max_wpl, max_wnl, max_gain, posvec);
			}
		}
		/** Finds the best partition in a set of examples on an binary attribute.
		 * 
		 * @param prt a partition.
		 * @param set a dataset.
		 * @param attr the attribute to split on.
		 * @param eps a smooth factor.
		 */
		void split_binary(partition_type& prt, dataset_type& set, int attr, float_type eps)
		{
			float_type gain;
			float_type max_gain = prt.gain();
			float_type psum = prt.psum();
			float_type nsum = prt.nsum();
			float_type wpl=0, wnl=0;
			sparse_pointer cur = sprdataset.begin(attr), beg=cur;
			sparse_pointer end = sprdataset.end(attr);
			index_type cur_idx;
			
			for(;cur != end;++cur)
			{
				if( is_attribute_missing(*cur) ) continue;
				cur_idx = type_util::indexOf(*cur);
				if( valid[cur_idx] <= 0 ) continue;
				if( sprdataset.y(cur_idx) > 0 )
					wpl += sprdataset.w(cur_idx);
				else
					wnl += sprdataset.w(cur_idx);
			}
			for(;beg != end;++beg)
			{
				cur_idx = type_util::indexOf(*beg);
				if( valid[cur_idx] <= 0 ) continue;
				if( sprdataset.y(cur_idx) > 0 )
					psum -= sprdataset.w(cur_idx);
				else
					nsum -= sprdataset.w(cur_idx);
			}
			wpl = std::max(eps, psum - wpl);
			wnl = std::max(eps, psum - wnl);
			gain = impurity_type::gain(wpl, wnl, psum, nsum, eps);
std::cerr << "gain: " << gain << " > " << max_gain << std::endl;
			if( gain > max_gain )
			{
				prt.update(attr, 0.5, wpl, wnl, gain);
			}
		}
		/** Finds the best partition in a set of examples on an ordered attribute.
		 * 
		 * @param prt a partition.
		 * @param set a dataset.
		 * @param attr the attribute to split on.
		 * @param eps a smooth factor.
		 */
		void split_ordered(partition_type& prt, dataset_type& set, int attr, float_type eps)
		{
			float_type gain;
			float_type max_gain = prt.gain();
			float_type psum = prt.psum();
			float_type nsum = prt.nsum();
			float_type pzsum=eps, nzsum=eps;
			float_type wpl=eps, wnl=eps;
			float_type max_wpl, max_wnl;
			value_type prev_value, curr_value, thr = value_util::max();
			sparse_pointer cur = sprdataset.begin(attr), beg=cur;
			sparse_pointer end = sprdataset.end(attr);
			index_type cur_idx, prv_idx;
			for(;cur != end;++cur)
			{
				if( is_attribute_missing(*cur) ) return;
				cur_idx = type_util::indexOf(*cur);
				if( valid[cur_idx] > 0 )
				{
					prv_idx = cur_idx;
					break;
				}
			}
			if( cur == end ) return;
			prev_value = type_util::valueOf(*cur);
			for(;beg != end;++beg)
			{
				if( is_attribute_missing(*beg) ) break;
				cur_idx = type_util::indexOf(*beg);
				if( valid[cur_idx] <= 0 ) continue;
				if( sprdataset.y(cur_idx) > 0 )
					pzsum += sprdataset.w(cur_idx);
				else
					nzsum += sprdataset.w(cur_idx);
			}
			for(;beg != end;++beg)
			{
				cur_idx = type_util::indexOf(*beg);
				if( valid[cur_idx] <= 0 ) continue;
				if( sprdataset.y(cur_idx) > 0 )
					psum -= sprdataset.w(cur_idx);
				else
					nsum -= sprdataset.w(cur_idx);
			}
			pzsum = std::max(eps, psum - pzsum);
			nzsum = std::max(eps, nsum - nzsum);
			if( prev_value > 0 )
			{
				wpl += pzsum;
				wnl += nzsum;
				gain = impurity_type::gain(wpl, wnl, psum, nsum, eps);
				if( gain > max_gain )
				{
					thr = prev_value * 0.5;
					max_gain = gain;
					max_wpl = wpl;
					max_wnl = wnl;
				}
			}
			for(cur++;cur != end;++cur)
			{
				curr_value = type_util::valueOf(*cur);
				if( curr_value == type_util::missing() ) break;
				cur_idx = type_util::indexOf(*cur);
				if( valid[cur_idx] <= 0 ) continue;
				if( sprdataset.y(prv_idx) > 0 )
					wpl += sprdataset.w(prv_idx);
				else
					wnl += sprdataset.w(prv_idx);
				if( prev_value < 0 && 0 < curr_value )
				{
					gain = impurity_type::gain(wpl, wnl, psum, nsum, eps);
					if( gain > max_gain )
					{
						thr = prev_value * 0.5;
						max_gain = gain;
						max_wpl = wpl;
						max_wnl = wnl;
					}
					wpl += pzsum;
					wnl += nzsum;
					gain = impurity_type::gain(wpl, wnl, psum, nsum, eps);
					if( gain > max_gain )
					{
						thr = prev_value * 0.5;
						max_gain = gain;
						max_wpl = wpl;
						max_wnl = wnl;
					}
				}
				if( prev_value != curr_value )
				{
					gain = impurity_type::gain(wpl, wnl, psum, nsum, eps);
					if( gain > max_gain )
					{
						thr = (prev_value+curr_value) * 0.5;
						max_gain = gain;
						max_wpl = wpl;
						max_wnl = wnl;
					}
				}
				prev_value = curr_value;
				prv_idx = cur_idx;
			}
			if( thr != value_util::max() )
			{
				prt.update(attr, thr, max_wpl, max_wnl, max_gain);
			}
		}
		
	private:
		sparse_dataset_type sprdataset;
		int* valid;
	};
	// sparse/normal split
	// multi-class/binary split
};


#endif


