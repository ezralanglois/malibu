/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * aspen_sprout.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ASPEN_SPROUT_HPP
#define _EXEGETE_ASPEN_SPROUT_HPP
#include "aspen_split.hpp"
#include "ShuffleAlgorithms.h"

/** @file aspen_sprout.hpp
 * 
 * @brief Grows an aspen tree
 * 
 * This file contains a class to grow an aspen tree.
 *
 * @ingroup ExegeteAspen
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Grows an Aspen tree
	 * 
	 * This class grows an Aspen tree.
	 */
	template<class dataset, class impurity>
	class aspen_sprout : public aspen_split<dataset,impurity>
	{
		typedef dataset 								dataset_type;
		typedef typename dataset_type::attribute_type 	attribute_type;
		typedef typename dataset_type::class_type 		class_type;
		typedef aspen_split<dataset,impurity> 			split_type;
		typedef typename split_type::partition_type 	partition_type;
		typedef std::vector< partition_type > 			partition_vector;
		typedef typename partition_type::index_type 	index_type;
		typedef TypeUtil<index_type>					index_util;
		typedef TuneParameterTree 						argument_type;
		typedef TuneParameterTree& 						argument_iterator;
		typedef typename argument_type::parameter_type	range_type;
		typedef typename split_type::feature_type		feature_type;
	public:
		/** Defines a size type. */
		typedef typename dataset_type::size_type 		size_type;
		/** Defines a node type. **/
		typedef typename partition_type::node_type 		node_type;
		/** Defines a float type. **/
		typedef typename split_type::float_type 		float_type;
		
	public:
		/** Constructs an aspen sprout algorithm.
		 */
		aspen_sprout() : subsetInt(1), randsetInt(0), maxdepthInt(0), 
						 probabilityInt(1), confidenceFlt(0.9f), minweightFlt(1e-6),
						 attr_subset(0), attr_valid(0), attr_cnt(0), max_attr_cnt(0),
						 depthSel(maxdepthInt,	"Depth", range_type(0, 1, 1, '+')),
						 randsetSel(randsetInt,	"RandSet", range_type(0, 1, 1, '+'), &depthSel)
		{
		}
		/** Destructs an aspen sprout algorithm.
		 */
		~aspen_sprout()
		{
			::erase(attr_subset);
			::erase(attr_valid);
		}
		
	public:
		/** Construct a deep copy of a sprout.
		 * 
		 * @param s an Aspen sprout.
		 */
		aspen_sprout(const aspen_sprout& s) : subsetInt(s.subsetInt), randsetInt(s.randsetInt), maxdepthInt(s.maxdepthInt), 
											 probabilityInt(s.probabilityInt), confidenceFlt(s.confidenceFlt), minweightFlt(s.minweightFlt),
											 attr_subset(0), attr_valid(0), attr_cnt(0), max_attr_cnt(0),
											 depthSel(maxdepthInt,	s.depthSel),
											 randsetSel(randsetInt,	s.randsetSel)
		{
			
		}
		/** Assign a deep copy of a sprout.
		 * 
		 * @param s an Aspen sprout.
		 * @return this object.
		 */
		aspen_sprout& operator=(const aspen_sprout& s)
		{
			argument_copy(s);
			return *this;
		}
		/** Copy arguments.
		 * 
		 * @param s an Aspen sprout.
		 */
		void argument_copy(const aspen_sprout& s)
		{
			subsetInt = s.subsetInt;
			randsetInt = s.randsetInt;
			maxdepthInt = s.maxdepthInt;
			probabilityInt = s.probabilityInt;
			confidenceFlt = s.confidenceFlt;
			minweightFlt = s.minweightFlt;
			depthSel = s.depthSel;
			randsetSel = s.randsetSel;
		}
	
	public:
		/** Adds arguments to an argument map.
		 * 
		 * @param map an argument map.
		 */
		template<class U>
		void setup_arguments(U& map)
		{
			arginit(map, depthSel,		"depth",		"max depth to stop splitting");
			arginit(map, randsetSel,	"randsubset",	"use a random subset of attributes");
			arginit(map, minweightFlt,	"minweight",	"minimum weight at node");
		}
		/** Setup the dataset.
		 * 
		 * @param dataset a dataset.
		 */
		template<class Y1, class W1>
		void setup_dataset(ExampleSet<attribute_type, Y1, W1>& dataset)
		{
			split_type::setup_dataset(dataset);
		}
		/** Grows an Aspen tree.
		 * 
		 * @param node an Aspen tree model.
		 * @param set a dataset.
		 */
		void sprout(node_type& node, dataset_type& set)
		{
std::cerr << "sprout: 0" << std::endl;
			split_type::update_dataset(set);
			node.clear();
std::cerr << "sprout: 1" << std::endl;
			if( partitions.size() < set.attributeCount() ) partitions.resize(set.attributeCount());
			if( maxdepthInt == 0 ) maxdepthInt = set.attributeCount();
std::cerr << "sprout: 2" << std::endl;
			if( size_type(max_attr_cnt) != set.attributeCount() ) setup_attrs(set.attributeCount());
			if( randsetInt > 0 )
			{
				shuffle(attr_subset, attr_subset+max_attr_cnt);
				if( attr_cnt != randsetInt ) attr_cnt = randsetInt;
			}
			else if( attr_cnt != max_attr_cnt ) attr_cnt = max_attr_cnt;
std::cerr << "sprout: 3" << std::endl;
			split_type::init(partitions[0], set);
std::cerr << "sprout: 4" << std::endl;
			sprout_node(node, set, 0);
std::cerr << "sprout: 5" << std::endl;
			split_type::finalize(set);
std::cerr << "sprout: 6" << std::endl;
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return depthSel;
		}
		/** Converts an attribute vector.
		 * 
		 * @param ptr a source attribute vector.
		 * @param a_cnt number of attributes.
		 * @return a destination attribute vector.
		 */
		feature_type* convert_attribute(const attribute_type* ptr, size_type a_cnt)const
		{
			return split_type::convert_attribute(ptr, a_cnt);
		}
		
	private:
		void sprout_node(node_type& node, dataset_type& set, int d)
		{
std::cerr << "sprout_node: 0" << std::endl;
			index_type splt = split_node(node, partitions[d], set);
			if( splt == index_util::max() || partitions[d].minweight(minweightFlt) )
			{	// Create Leaf
std::cerr << "sprout_node: 1" << std::endl;
				partitions[d].create_leaf(node, split_type::confidence(partitions[d], minweightFlt));
			}
			else if( (d+1) >= maxdepthInt )
			{	// Add Leaves
std::cerr << "sprout_node: 2" << std::endl;
				partitions[d].create_branch(node);
				d++;
				for(size_type i=0, n=node.size();i<n;++i)
				{
					partitions[d].set_weights( partitions[d-1], i);
					partitions[d].create_leaf(node, split_type::confidence(partitions[d], minweightFlt));
				}
			}
			else
			{	// Continue growing
std::cerr << "sprout_node: 3" << std::endl;
				partitions[d].create_branch(node);
				d++;
				attr_valid[splt]=0;
				for(size_type i=0, n=node.size();i<n;++i)
				{
					set_weights(partitions[d], partitions[d-1], i);
					split_type::update_split(partitions[d-1], partitions[d], i);
					sprout_node(node[i], set, d);
				}
				attr_valid[splt]=1;
			}
		}
		void set_weights(partition_type& child, partition_type& parent, size_type i)
		{
			float_type p = parent.psum(i);
			float_type s = p + parent.nsum(i);
			child.set_weights(parent, i, split_type::gain(p, s));
		}
		index_type split_node(node_type& node, partition_type prt, dataset_type& set)
		{
			for(unsigned int a=0, idx;a<attr_cnt;++a)
			{
				idx = attr_subset[a];
				if( attr_valid[idx] == 0 ) continue;
				if( set.attributeAt(idx).isbinary() )
					 split_type::split_binary(prt, set, idx, minweightFlt);
				else if( set.attributeAt(idx).isnominal() )
					 split_type::split_unordered(prt, set, idx, minweightFlt);
				else split_type::split_ordered(prt, set, idx, minweightFlt);
			}
			return prt.best_index();
		}
		void setup_attrs(size_type n)
		{
			max_attr_cnt = attr_cnt = n;
			attr_valid = ::resize(attr_valid, n);
			attr_subset = ::resize(attr_subset, n);
			std::fill(attr_valid, attr_valid+n, 1);
			for(size_type i=0;i<n;++i) attr_subset[i] = int(i);
		}
	
	private:
		int subsetInt;//unused
		unsigned int randsetInt;
		int maxdepthInt;
		int probabilityInt;//unused
		float confidenceFlt;//unused
		float minweightFlt;
		
	private:
		partition_vector partitions;
		int* attr_subset;
		int* attr_valid;
		unsigned int attr_cnt;
		unsigned int max_attr_cnt;
		
	private:
		argument_type depthSel;
		argument_type randsetSel;
	};
};

#endif


