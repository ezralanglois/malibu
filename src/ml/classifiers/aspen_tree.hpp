/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * aspen_tree.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ASPEN_TREE_HPP
#define _EXEGETE_ASPEN_TREE_HPP
#include "Learner.h"
#include "aspen_sprout.hpp"
#include "aspen_impurity.hpp"

/** Defines the version of the Aspen tree algorithm. 
 */
#define _ASPEN_TREE_VERSION 101000

/** @file aspen_tree.hpp
 * 
 * @brief Aspen tree classifier
 * 
 * This file contains a decision tree classifier.
 *
 * @ingroup ExegeteAspen
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Aspen tree classifier
	 * 
	 * This class defines an Aspen tree classifier.
	 */
	template<class X, class Y, class W>
	class aspen_tree : public Learner
	{
		typedef detail::weight_adapter<W> 				wadapter;
		typedef detail::prediction_adapter<double, W> 	padapter;
		typedef typename wadapter::weight_type 			w_type;
	public:
		/** Interface flags:
		 * 	- Tunable
		 * 	- Graphable
		 */
		enum{ TUNE=1, ARGUMENT=1, GRAPHABLE=1 };

	public:
		/** Defines a parent type. **/
		typedef void 											parent_type;
		/** Defines an example set as a learnset type. **/
		typedef ExampleSet<X, Y, w_type>						dataset_type;
		/** Defines an example set as a learnset type. **/
		typedef ExampleSet<X, Y, std::string*>					testset_type;
		/** Defines a learnset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a svm attribute value as a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines the second template parameter as a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef w_type											weight_type;
		/** Defines a prediction type. **/
		typedef typename padapter::result						prediction_type;
		
	private:
		typedef TuneParameterTree 								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename dataset_type::size_type 				size_type;
		typedef aspen_sprout< dataset_type, aspen_entropy > 	sprout_type;
		typedef typename sprout_type::node_type					node_type;
		enum{ binary_flag = padapter::binary };
		
	public:
		/** Constructs an Aspen tree.
		 */
		aspen_tree() : Learner(SEMI|binary_flag), class_cnt(0), attr_cnt(0)
		{
		}
		/** Destructs an Aspen classifier.
		 */
		~aspen_tree()
		{
		}
		
	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{			
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(aspen_tree) );
				sprout.setup_arguments(map);
			}
		}
		
	public:		
		/** Constructs a deep copy of the Aspen tree model.
		 *
		 * @param tree a source Aspen tree model.
		 */
		aspen_tree(const aspen_tree& tree) : Learner(tree), sprout(tree.sprout), root(tree.root), class_cnt(tree.class_cnt), attr_cnt(tree.attr_cnt)
		{
		}
		/** Assigns a deep copy of the Aspen tree model.
		 *
		 * @param tree a source Aspen tree model.
		 * @return a reference to this object.
		 */
		aspen_tree& operator=(const aspen_tree& tree)
		{
			Learner::operator=(tree);
			argument_copy(tree);
			root = tree.root;
			class_cnt = tree.class_cnt;
			attr_cnt = tree.attr_cnt;
			return *this;
		}
	public:
		/** Setups a concrete example set.
		 *
		 * @param dataset a concrete dataset.
		 */
		template<class Y1>
		void setup(ConcreteExampleSet<X, Y1>& dataset, bool testset=false)
		{
			if(!testset) sprout.setup_dataset(dataset);
		}
		/** Get the numer of classes.
		 * 
		 * @return class count
		 */
		unsigned int classCount()const
		{
			return class_cnt;
		}
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.5f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @param parray an array of real-valued predictions.
		 */
		void predict(const_attribute_pointer pred, prediction_type parray, prediction_type pend=0)const
		{
			ASSERT(false);
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			typedef typename dataset_type::value_type example_type;
			ASSERT(!empty());
			if( empty() ) return threshold();
			return root.predict(sprout.convert_attribute(pred, attr_cnt), example_type::missing());
		}
		/** Builds a model for the Aspen tree.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
std::cerr << "aspen-tree-train: 0" << std::endl;
			attr_cnt = learnset.attributeCount();
			class_cnt = learnset.classCount();
			sprout.sprout(root, learnset);
			return 0;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Aspen
		 */
		static std::string name()
		{
			return "Aspen";
		}
		/** Gets the version of the Aspen tree classifier.
		 * 
		 * @return version.
		 */
		static int version()
		{
			return _ASPEN_TREE_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return aspen
		 */
		static std::string prefix()
		{
			return "aspen";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return sprout.argument();
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return root.empty();
		}
		/** Clear an Aspen tree model.
		 */
		void clear()
		{
			root.clear();
		}
		/** Copies arguments in an Aspen tree.
		 * 
		 * @param p a source Aspen tree.
		 */
		void argument_copy(const aspen_tree& p)
		{
			Learner::argument_copy(p);
			sprout.argument_copy(p.sprout);
		}
		/** Makes a shallow copy of an Aspen tree.
		 *
		 * @param ref a reference to an Aspen tree.
		 */
		void shallow_copy(aspen_tree& ref)
		{
			root.shallow_copy(ref.root);
			argument_copy(ref);
			class_cnt = ref.class_cnt;
			attr_cnt = ref.attr_cnt;
		}
		/** Get the class name of the learner.
		 * 
		 * @return aspen_tree
		 */
		static std::string class_name()
		{
			return "aspen_tree";
		}
		/** Get the expected name of the program.
		 * 
		 * @return class name
		 */
		static std::string progname()
		{
#ifdef _MULTICLASS
	#ifdef _WEIGHTED
			return "wmaspen";
	#else
			return "maspen";
	#endif
#else
	#ifdef _WEIGHTED
			return "waspen";
	#else
			return "aspen";
	#endif
#endif
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			root.accept(visitor);
		}

	public:
		/** Reads an Aspen tree from the input stream.
		 *
		 * @param in an input stream.
		 * @param tree an Aspen tree.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, aspen_tree& tree)
		{
			in >> tree.class_cnt;
			if( in.get() != '\t' ) tree.fail(in, ERRORMSG("Missing tab in Aspen model"));
			in >> tree.attr_cnt;
			if( in.get() != '\n' ) tree.fail(in, ERRORMSG("Missing newline in Aspen model"));
			tree.root.clear();
			tree.errormsg(in, tree.root.read(in));
			return in;
		}
		/** Writes an Aspen tree to the output stream.
		 *
		 * @param out an output stream.
		 * @param tree an Aspen tree.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const aspen_tree& tree)
		{
			out << tree.class_cnt << "\t" << tree.attr_cnt << "\n";
			tree.errormsg(out, tree.root.write(out));
			return out;
		}
		
	private:
		sprout_type sprout;
		node_type root;
		size_type class_cnt;
		size_type attr_cnt;
	};
};

/** @brief Type utility interface to an Aspen tree
 * 
 * This class defines a type utility interface to an Aspen tree.
 */
template<class A, class C, class W>
struct TypeUtil< ::exegete::aspen_tree<A,C,W> >
{
	/** Flags class as non-primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::aspen_tree<A,C,W> value_type;
	/** Tests if a string can be converted to an Aspen tree.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return aspen_tree
	 */
	static const char* name() 
	{
		return "aspen_tree";
	}
};


#endif



