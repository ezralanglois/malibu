/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mil_optimal_sphere.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_MIL_OPTIMAL_SPHERE_HPP
#define _EXEGETE_MIL_OPTIMAL_SPHERE_HPP
#include "Learner.h"
//#include "matrix.hpp"
/** Defines the version of the optimal sphere MIL algorithm. 
 * @todo add to group
 */
#define _MIL_OPTIMAL_SPHERE_VERSION 101000

/** @file mil_optimal_sphere.hpp
 * @brief Native implementation of the optimal sphere multiple-instance learning classifier
 * 
 * This file contains the native implementation of the optimal sphere multiple-instance 
 * learning classifier.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Native implementation of the optimal sphere multiple-instance learning classifier
	 * 
	 * This class template defines the Native implementation of the optimal sphere 
	 * multiple-instance learning classifier.
	 * 
	 * @todo add more distance metrics: pearson (add as negative)
	 * @todo add hyper rectangle
	 */
	template<class A, class C, class W>
	class mil_optimal_sphere : public Learner
	{
	public:
		/** Interface flags:
		 * 	- Tunable
		 */
		enum{ EVAL=1, FORCE_MIL=1, TUNE=1, ARGUMENT=1 };

	public:
		/** Defines a parent type. **/
		typedef void 												parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<A, C, W>									dataset_type;
		/** Defines a testset type. **/
		typedef ExampleSet<A, C, std::string*>						testset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer		const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type				descriptor_type;
		/** Defines a float type. **/
		typedef double												float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator							argument_iterator;
		/** Defines a weight type. **/
		typedef W													weight_type;
		/** Defines a prediction type. **/
		typedef float_type 											prediction_type;
		/** Defines a tunable argument type. **/
		typedef typename Learner::argument_type						argument_type;
		
	private:
		typedef typename argument_type::parameter_type				range_type;
		typedef typename dataset_type::bag_iterator 				bag_iterator;
		typedef typename dataset_type::iterator 					example_iterator;
		typedef typename dataset_type::size_type 					size_type;
		typedef typename dataset_type::bag_reference 				bag_reference;
		typedef typename dataset_type::reference 					example_reference;

	public:
		/** Constructs a default multiple-instance optimal sphere.
		 */
		mil_optimal_sphere() : Learner(SEMI|MISSING|NOMINAL|BINARY|NORMALIZED|MIL), distance(0), queue(0), ndist(0), radius(0.0), center(0), attrn(0),
						       distInt(0), 
						       distSel(distInt, "Distance", range_type(0,1,1,'+'))
		{
		}
		/** Destructs a multiple-instance optimal sphere.
		 */
		~mil_optimal_sphere()
		{
			deallocate(queue, ndist);
			::erase(center);
			::erase(distance);
		}
		
	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(mil_optimal_sphere) );
				if(t>=0)
				arginit(map, distSel, "distance", "type of distance metric>Chebyshev:0;Manhattan:1;Euclidean:2;*:*");
				else
				arginit(map, distInt, "distance", "type of distance metric>Chebyshev:0;Manhattan:1;Euclidean:2;*:*");
			}
		}
		
	public:
		/** Constructs a deep copy of the multiple-instance optimal sphere.
		 *
		 * @param sphere a source multiple-instance optimal sphere.
		 */
		mil_optimal_sphere(const mil_optimal_sphere& sphere) : Learner(sphere), distance(0), queue(0), ndist(0), radius(sphere.radius), center(0), attrn(sphere.attrn),
															   distInt(sphere.distInt), distSel(distInt, sphere.distSel)
		{
			center = ::resize(center, attrn);
			std::copy(sphere.center, sphere.center+attrn, center);
		}
		/** Assigns a deep copy of the multiple-instance optimal sphere.
		 *
		 * @param sphere a source multiple-instance optimal sphere.
		 * @return a reference to this object.
		 */
		mil_optimal_sphere& operator=(const mil_optimal_sphere& sphere)
		{
			Learner::operator=(sphere);
			attrn = sphere.attrn;
			radius = sphere.radius;	
			center = ::resize(center, attrn);
			std::copy(sphere.center, sphere.center+attrn, center);
			argument_copy(sphere);
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			float_type dist = distance_type(center, center+attrn, pred);
			return (radius - dist);
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param fbeg start of attribute vectors.
		 * @param fend end of attribute vectors.
		 * @return a real-valued prediction.
		 */
		template<class I>
		float_type predict(I fbeg, I fend)const
		{
			float_type dist, mindist=TypeUtil<float_type>::max(), maxdist=0;
			for(;fbeg != fend;++fbeg)
			{
				dist = distance_type(center, center+attrn, fbeg->x());
				if( dist < mindist ) mindist = dist;
				if( dist > maxdist ) maxdist = dist;
			}
			if( mindist <= radius ) return radius-mindist;
			return radius-maxdist;
		}
		/** Builds a model for the multiple-instance optimal sphere.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			reallocate_center(learnset.attributeCount());
			std::fill(center, center+attrn, 0);
			distance=initalize_distance(learnset, distance, queue, ndist);
			optimal_radius(learnset, distance);
			return 0;
		}
		
	private:
		void optimal_radius(dataset_type& learnset, float_type*** bdist)
		{
			size_type nbag = learnset.bagCount();
			float_type** idist;
			float_type *sdist, *edist, *adist = ::setsize<float_type>(nbag);
			example_iterator ebeg, eend;
			float_type radi, tot, bst=0;
			radius=0;
			for(bag_iterator beg = learnset.bag_begin(), end=learnset.bag_end(), cur=beg;cur != end;++cur, ++bdist)
			{
				if( cur->y() == 0) continue;
				idist = *bdist;
				for(ebeg = cur->begin(), eend = cur->end();ebeg != eend;++ebeg, ++idist)
				{
					sdist = *idist;
					edist = sdist+nbag;
					std::copy(sdist, edist, adist);
					sdist = adist;
					edist = sdist+nbag;
					std::stable_sort(sdist, edist);
					for(++sdist;sdist != edist;++sdist)
					{
						radi = *sdist-(*sdist-*(sdist-1))/2.0;
						tot = evaluate(beg, end, *idist, radi);
std::cerr << "optimal_radius: " << tot << " -> " << radi << " - " << **idist << ", " << *sdist << std::endl;
						if( tot > bst || (tot==bst && radi > radius) )
						{
							std::copy(ebeg->x(), ebeg->x()+attrn, center);
							radius = radi;
							bst = tot;
						}
					}
				}
			}
			::erase(adist);
		}
		float_type evaluate(bag_iterator beg, bag_iterator end, float_type* pdist, float_type radi)
		{
			float_type tot=0.0f;
			float_type inv = 1.0f / std::distance(beg, end);
			for(;beg != end;++beg, ++pdist)
			{
				if( beg->y() > 0 )
				{
					if( *pdist <= radi ) tot+=beg->weight(inv);
				}
				else
				{
					if( *pdist > radi ) tot+=beg->weight(inv);
				}
			}
			return tot;
		}
		
	private:
		float_type*** initalize_distance(dataset_type& learnset, float_type*** dist, float_type*** q, size_type old)
		{
			size_type nbag = learnset.bagCount(), nins;
			if( nbag > old )
			{
				q = queue = resize(q, nbag);
				std::fill(q+old, q+nbag, (float_type**)(0));
				dist = resize<float_type**>(dist, nbag);
				ndist = nbag;
			}
			float_type*** pdist=dist;
			for(bag_iterator beg = learnset.bag_begin(), end=learnset.bag_end();beg != end;++beg, ++pdist)
			{
				if( beg->y() > 0)
				{
					nins = beg->size();
					*q = *pdist = reallocate(*q, nins, nbag);
					initalize_for_positive(beg, learnset, *pdist);
					++q;
				}
				else *pdist = 0;
			}
			return dist;
		}
		void initalize_for_positive(bag_iterator xbag, dataset_type& learnset, float_type** pdist)
		{
			float_type* dist;
			bag_iterator bbeg = learnset.bag_begin(), bcurr, bend = learnset.bag_end();
			for(example_iterator beg = xbag->begin(), end=xbag->end();beg != end;++beg, ++pdist)
			{
				for(bcurr=bbeg, dist=*pdist;bcurr != bend;++bcurr, ++dist)
				{
					if( bcurr == xbag ) *dist = 0;
					else *dist = minimum_bag_distance(*beg, *bcurr, learnset.attributeCount());
				}
			}
		}
		float_type minimum_bag_distance(const_attribute_pointer center, bag_reference bag, size_type attrn)
		{
			float_type dist=0.0f, mindist=TypeUtil<float_type>::max();
			for(example_iterator beg = bag.begin(), end=bag.end();beg != end;++beg)
			{
				dist = distance_type(center, center+attrn, *beg);
				if( dist < mindist ) mindist = dist;
			}
			return mindist;
		}
		float_type distance_type(const_attribute_pointer bcent, const_attribute_pointer ecent, const_attribute_pointer curr)const
		{
				 if( distInt == 0 ) return distance_inf_norm(bcent, ecent, curr);
			else if( distInt == 1 ) return distance_manhattan(bcent, ecent, curr);
			else if( distInt == 2 ) return distance_squared(bcent, ecent, curr);
			else return distance_power(bcent, ecent, curr, distInt);
		}
		static float_type distance_power(const_attribute_pointer bcent, const_attribute_pointer ecent, const_attribute_pointer curr, unsigned int n)
		{
			float_type dist=0.0f, val;
			for(;bcent != ecent;++bcent, ++curr) 
			{
				val=std::abs(*bcent - *curr);
				for(unsigned int i=0;i<n;++i) val*= val;
				dist += val;
			}
			return dist;
		}
		static float_type distance_manhattan(const_attribute_pointer bcent, const_attribute_pointer ecent, const_attribute_pointer curr)
		{
			float_type dist=0.0f;
			for(;bcent != ecent;++bcent, ++curr) dist += std::abs(*bcent - *curr);
			return dist;
		}
		static float_type distance_squared(const_attribute_pointer bcent, const_attribute_pointer ecent, const_attribute_pointer curr)
		{
			float_type dist=0.0f;
			for(;bcent != ecent;++bcent, ++curr) dist += (*bcent - *curr) * (*bcent - *curr);
			return dist;
		}
		static float_type distance_inf_norm(const_attribute_pointer bcent, const_attribute_pointer ecent, const_attribute_pointer curr)
		{
			float_type dist=0.0f, val;
			for(;bcent != ecent;++bcent, ++curr) 
			{
				val = std::abs(*bcent - *curr);
				if( val > dist ) dist=val;
			}
			return dist;
		}
		
	public:
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return distSel;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return ADTree
		 */
		static std::string name()
		{
			return "MIL Optimal Sphere";
		}
		/** Gets the version of the multiple-instance optimal sphere classifier.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _MIL_OPTIMAL_SPHERE_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return adsphere
		 */
		static std::string prefix()
		{
			return "milsphere";
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return attrn == 0;
		}
		/** Clear an willow model.
		 */
		void clear()
		{
			deallocate(queue, ndist);
			::erase(center);
			::erase(distance);
			queue = 0;
			attrn = 0;
			ndist = 0;
			center = 0;
			distance = 0;
			radius = 0;
		}
		/** Copies arguments in a multiple-instance optimal sphere.
		 * 
		 * @param sphere a source multiple-instance optimal sphere.
		 */
		void argument_copy(const mil_optimal_sphere& sphere)
		{
			distInt = sphere.distInt;
			distSel = sphere.distSel;
			Learner::argument_copy(sphere);
		}
		/** Makes a shallow copy of a multiple-instance optimal sphere.
		 *
		 * @param ref a reference to a multiple-instance optimal sphere.
		 */
		void shallow_copy(mil_optimal_sphere& ref)
		{
			radius = ref.radius;
			attrn = ref.attrn;
			center = ref.center;
			ref.center = 0;
			ref.attrn = 0;
			argument_copy(ref);
		}
		/** Get the expected name of the program.
		 * 
		 * @return milsphere
		 */
		static std::string progname()
		{
			return "milsphere";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
		}
		/** Get the class name of the learner.
		 * 
		 * @return mil_optimal_sphere
		 */
		static std::string class_name()
		{
			return "mil_optimal_sphere";
		}

	public:
		/** Reads a multiple-instance optimal sphere from the input stream.
		 *
		 * @param in an input stream.
		 * @param sphere a multiple-instance optimal sphere.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, mil_optimal_sphere& sphere)
		{
			size_type n;
			in >> sphere.distInt;
			if( in.get() != '\t' ) return sphere.fail(in, "Missing tab after distance");
			in >> sphere.radius;
			if( in.get() != '\t' ) return sphere.fail(in, "Missing tab after radius");
			in >> n;
			sphere.reallocate_center(n);
			for(size_type i=0;i<n;++i)
			{
				if( in.get() != '\t' ) return sphere.fail(in, "Missing tab before center");
				in >> sphere.center[i];
			}
			if( in.get() != '\n' ) return sphere.fail(in, "Missing newline terminating model");
			return in;
		}
		/** Writes a multiple-instance optimal sphere to the output stream.
		 *
		 * @param out an output stream.
		 * @param sphere a multiple-instance optimal sphere.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const mil_optimal_sphere& sphere)
		{
			out << sphere.distInt << "\t" << sphere.radius << "\t" << sphere.attrn;
			for(size_type i=0;i<sphere.attrn;++i) out << "\t" << sphere.center[i];
			out << "\n";
			return out;
		}
		
	private:
		static void deallocate(float_type*** ptr, size_type n)
		{
			if(ptr != 0)
			{
				for(size_type i=0;i<n;++i)
				{
					if(ptr[i]!=0) 
					{
						::erase(ptr[i][0]);
						ptr[i][0]=0;
					}
					::erase(ptr[i]);
					ptr[i]=0;
				}
				::erase(ptr);
			}
		}
		static float_type** reallocate(float_type** ptr, size_type r, size_type c)
		{
			ASSERT(r>0);
			bool init=(ptr==0);
			float_type* old=0;
			if(!init) old = *ptr;
			ptr = ::resize(ptr, r);
			*ptr = old;
			*ptr = ::resize(*ptr, r*c);
			for(unsigned int i=1;i<r;++i) ptr[i] = ptr[i-1]+c;
			return ptr;
		}
		void reallocate_center(size_type n)
		{
			if( attrn != n )
			{
				attrn = n;
				center = ::resize(center, attrn);
			}
		}

	private:
		// temporary saved for speed
		float_type*** distance;
		float_type*** queue;
		size_type ndist;
		
	private:
		//model
		float_type radius;
		descriptor_type* center;//can be sparse
		size_type attrn;
		
	private:
		//arguments
		int distInt;
		argument_type distSel;
	};

};

/** @brief Type utility interface to a multiple-instance optimal sphere
 * 
 * This class defines a type utility interface to a multiple-instance optimal sphere.
 */
template<class A, class C, class W>
struct TypeUtil< ::exegete::mil_optimal_sphere<A,C,W> >
{
	/** Flags class as non-primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::mil_optimal_sphere<A,C,W> value_type;
	/** Tests if a string can be converted to a multiple-instance optimal sphere.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return mil_optimal_sphere
	*/
	static const char* name() 
	{
		return "mil_optimal_sphere";
	}
};


#endif


