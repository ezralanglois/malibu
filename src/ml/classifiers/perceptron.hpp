/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * perceptron.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PERCEPTRON_HPP
#define _EXEGETE_PERCEPTRON_HPP
#include "Learner.h"
#include "ShuffleAlgorithms.h"
#include <iomanip>

/** Defines the version of the perceptron algorithm. 
 * @todo add to group
 */
#define _PERCEPTRON_VERSION 101000

/** @file perceptron.hpp
 * @brief Native implementation of the perceptron
 * 
 * This file contains the perceptron algorithm class template.
 *
 * @ingroup ExegeteClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail
	{
		template<class W, class E>
		class perceptron_model;
	};
	/** @brief Native implementation of the perceptron algorithm
	 * 
 	 * This class template defines the perceptron classifier.
 	 * 
 	 * @todo add shuffle
	 */
	template<class A, class C, class W>
	class perceptron : public Learner
	{
		typedef detail::weight_adapter<W> 				wadapter;
		typedef detail::prediction_adapter<double, W> 	padapter;
		typedef typename wadapter::weight_type 			w_type;
	public:
		/** Interface flags:
		 * 	- Tunable
		 */
		enum{ TUNE=1, ARGUMENT=1 };//, TUNE=0

	public:
		/** Defines a parent type. **/
		typedef void 											parent_type;
		/** Defines an example set as a learnset type. **/
		typedef ExampleSet<A, C, w_type>						dataset_type;
		/** Defines an example set as a learnset type. **/
		typedef ExampleSet<A, C, std::string*>					testset_type;
		/** Defines a learnset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a svm attribute value as a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines the second template parameter as a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef w_type 											weight_type;
		/** Defines a prediction type. **/
		typedef typename padapter::result	 					prediction_type;
		
	private:
		typedef TuneParameterTree 						argument_type;
		typedef typename argument_type::parameter_type	range_type;
		typedef typename dataset_type::size_type 		size_type;
		typedef typename dataset_type::feature_type* 	feature_pointer;
		typedef typename dataset_type::value_type 		example_type;
		typedef typename dataset_type::iterator 		iterator;
		typedef typename dataset_type::class_type 		class_type;
		typedef detail::perceptron_model<prediction_type, example_type> model_type;
		enum{ binary_flag = padapter::binary };

	public:		
		/** Constructs a default perceptron.
		 */
		perceptron() : Learner(SEMI|MISSING|NOMINAL|binary_flag|NORMALIZED), decreaseBool(0), shuffleBool(1), repeatInt(0), rateFlt(0.01),
						rateSel(rateFlt, "Rate", range_type(0.001, 0.01, 10, '*'))
		{
		}
		/** Destructs a willow classifier.
		 */
		~perceptron()
		{
		}

	public:
		/** Initalizes the learner.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{			
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(perceptron) );
				arginit(map, shuffleBool, 	"reshuffle", 	"should reshuffle every round?");
				arginit(map, repeatInt,		"repeat", 		"how many times to repeat learning");
				arginit(map, decreaseBool,  "decrease", 	"decrease learning rate each iteration by 1/rounds?");
				if(t>=0)
				arginit(map, rateSel,   	"rate",   		"learning rate for the perceptron");
				else
				arginit(map, rateFlt,   	"rate",   		"learning rate for the perceptron");
				// @todo add shuffle bool
			}
		}
		
	public:		
		/** Constructs a deep copy of the perceptron model.
		 *
		 * @param p a source perceptron model.
		 */
		perceptron(const perceptron& p) : Learner(p), model(p.model), decreaseBool(p.decreaseBool), shuffleBool(p.shuffleBool), repeatInt(p.repeatInt), rateFlt(p.rateFlt),
										  rateSel(rateFlt, p.rateSel)
		{
		}
		/** Assigns a deep copy of the perceptron model.
		 *
		 * @param p a source perceptron model.
		 * @return a reference to this object.
		 */
		perceptron& operator=(const perceptron& p)
		{
			Learner::operator=(p);
			model = p.model;
			argument_copy(p);
			return *this;
		}
		
	public:
		/** Get the numer of classes.
		 * 
		 * @return class count
		 */
		unsigned int classCount()const
		{
			return model.classCount();
		}
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @param parray an array of real-valued predictions.
		 */
		void predict(const_attribute_pointer pred, prediction_type parray, prediction_type pend=0)const
		{
			model.predict(pred, parray, pend);
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			return model.predict(pred);
		}
		/** Builds a model for the perceptron.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			if( repeatInt==0 ) repeatInt = 1;
			iterator beg = learnset.begin(), curr;
			iterator end = learnset.end();
			class_type p;
			model.initialize(learnset.attributeCount()+1, learnset.classCount());
			float_type r = rateFlt, inc = 1.0/repeatInt;
			for(size_type i=0;i<repeatInt;++i)
			{
				if(shuffleBool) shuffle(beg, end);
				for(curr=beg;curr != end;++curr)
				{
					p = model.predict_best_label(*curr);
					if( curr->y() != p )
						model.update(*curr, p, r*curr->weightforclass(p));
				}
				if(decreaseBool) r *= inc;
			}
			model.finalize();
			return 0;
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Willow
		 */
		static std::string name()
		{
			return "Perceptron";
		}
		/** Gets the version of the perceptron classifier.
		 * 
		 * @return version.
		 */
		static int version()
		{
			return _PERCEPTRON_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return tron
		 */
		static std::string prefix()
		{
			return "tron";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		parameter_type& argument()
		{
			return rateSel;
		}
		/** Tests is model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return model.empty();
		}
		/** Clear an willow model.
		 */
		void clear()
		{
			model.clear();
		}
		/** Copies arguments in a perceptron.
		 * 
		 * @param p a source perceptron.
		 */
		void argument_copy(const perceptron& p)
		{
			Learner::argument_copy(p);
			decreaseBool = p.decreaseBool;
			shuffleBool = p.shuffleBool;
			repeatInt = p.repeatInt;
			rateFlt = p.rateFlt;
			rateSel = p.rateSel;
		}
		/** Makes a shallow copy of a perceptron.
		 *
		 * @param ref a reference to a perceptron.
		 */
		void shallow_copy(perceptron& ref)
		{
			model.shallow_copy(ref.model);
			argument_copy(ref);
		}
		/** Get the class name of the learner.
		 * 
		 * @return perceptron
		 */
		static std::string class_name()
		{
			return "perceptron";
		}
		/** Get the expected name of the program.
		 * 
		 * @return wwillow or willow
		 */
		static std::string progname()
		{
#ifdef _MULTICLASS
	#ifdef _WEIGHTED
			return "wmtron";
	#else
			return "mtron";
	#endif
#else
	#ifdef _WEIGHTED
			return "wtron";
	#else
			return "tron";
	#endif
#endif
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
		}

	public:
		/** Reads an perceptron from the input stream.
		 *
		 * @param in an input stream.
		 * @param p a perceptron.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, perceptron& p)
		{
			in >> p.model;
			if(in.fail()) p.errormsg(p.model.errormsg());
			return in;
		}
		/** Writes an perceptron to the output stream.
		 *
		 * @param out an output stream.
		 * @param p a perceptron.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const perceptron& p)
		{
			out << p.model;
			if(out.fail()) p.errormsg(p.model.errormsg());
			return out;
		}
		
	private:
		model_type model;
		
	private:
		int decreaseBool;
		int shuffleBool;
		size_type repeatInt;
		float_type rateFlt;
		argument_type rateSel;
	};
	
	namespace detail
	{
		/** @brief Handles non-sparse vector operations.
		 * 
		 * This class defines non-sparse vector operations.
		 */
		template<class X, bool S=AttributeUtil<X>::IS_SPARSE>
		struct perceptron_util
		{
			/** Adds values in the attribute vector to the weight vector
			 * multiplied by some contant.
			 * 
			 * @param beg start of weight vector.
			 * @param end end of weight vector.
			 * @param a an attribute vector.
			 * @param r a multiplicative constant.
			 */
			template<class W>
			static void sum(W* beg, W* end, const X* a, W r)
			{
				for(;beg != end;++beg,++a) (*beg) += ( (*a) * r );
			}
			/** Adds values to a weight vector.
			 * 
			 * @param beg start of weight vector.
			 * @param end end of weight vector.
			 * @param r a multiplicative constant.
			 */
			template<class W>
			static void sum(W* beg, W* end, W r)
			{
				for(;beg != end;++beg) (*beg) += r;
			}
			/** Multiplies a weight vector by an attribute vector.
			 * 
			 * @param beg start of weight vector.
			 * @param end end of weight vector.
			 * @param a an attribute vector.
			 * @return dot product of vectors.
			 */
			template<class W>
			static W dot_product(const W* beg, const W* end, const X* a)
			{
				W sum = 0.0;
				for(;beg != end;++beg,++a) sum += ( (*beg)*(*a) );
				return sum;
			}
			/** Fills a weight vector with a value.
			 * 
			 * @param beg start of weight vector.
			 * @param end end of weight vector.
			 */
			template<class W>
			static W fill(const W* beg, const W* end, W w)
			{
				for(;beg != end;++beg) *beg = w;
			}
			/** Normalize a weight vector with a value.
			 * 
			 * @param wbeg start of weight vector.
			 * @param wend end of weight vector.
			 * @param cbeg start of count vector.
			 * @param w a count.
			 */
			template<class W>
			static void normalize(W* wbeg, W* wend, const W* cbeg, W w)
			{
				for(;wbeg != wend;++wbeg, ++cbeg) *wbeg = *wbeg - *cbeg / w;
			}
		};
		/** @brief Handles sparse vector operations.
		 * 
		 * This class defines sparse vector operations.
		 */
		template<class X>
		struct perceptron_util<X,true>
		{
			/** Define an attribute utility. **/
			typedef AttributeUtil<X> util_type;
			/** Adds values in the attribute vector to the weight vector
			 * multiplied by some contant.
			 * 
			 * @param beg start of weight vector.
			 * @param end end of weight vector.
			 * @param a an attribute vector.
			 * @param r a multiplicative constant.
			 */
			template<class W>
			static void sum(W* beg, W* end, const X* a, W r)
			{
				for(;util_type::indexOf(*a) != -1;++a)
				{
					ASSERTMSG((beg+util_type::indexOf(*a)) < end, util_type::indexOf(*a));
					beg[util_type::indexOf(*a)] += ( util_type::valueOf(*a) * r );
				}
			}
			/** Adds values to the weight vector.
			 * 
			 * @param beg start of weight vector.
			 * @param end end of weight vector.
			 * @param r a multiplicative constant.
			 */
			template<class W>
			static void sum(W* beg, W* end, W r)
			{
				for(;beg != end;++beg) (*beg) += r;
			}
			/** Multiplies a weight vector by an attribute vector.
			 * 
			 * @param beg start of weight vector.
			 * @param end end of weight vector.
			 * @param a an attribute vector.
			 * @return dot product of vectors.
			 */
			template<class W>
			static W dot_product(const W* beg, const W* end, const X* a)
			{
				W sum = 0.0;
				for(;util_type::indexOf(*a) != -1;++a)
				{
					ASSERTMSG((beg+util_type::indexOf(*a)) < end, util_type::indexOf(*a) << " < " << std::distance(beg,end));
					sum += ( beg[util_type::indexOf(*a)]*util_type::valueOf(*a) );
				}
				return sum;
			}
			/** Fills a weight vector with a value.
			 * 
			 * @param beg start of weight vector.
			 * @param end end of weight vector.
			 */
			template<class W>
			static W fill(const W* beg, const W* end, W w)
			{
				for(;beg != end;++beg) *beg = w;
			}
			/** Normalize a weight vector with a value.
			 * 
			 * @param wbeg start of weight vector.
			 * @param wend end of weight vector.
			 * @param cbeg start of count vector.
			 * @param w a count.
			 */
			template<class W>
			static void normalize(W* wbeg, W* wend, const W* cbeg, W w)
			{
				for(;wbeg != wend;++wbeg, ++cbeg) *wbeg = *wbeg - *cbeg / w;
			}
		};
		/** @brief Maintains a binary perceptron model
		 * 
		 * This class defines a binary perceptron model.
		 */
		template<class W, class E>
		class perceptron_model
		{
			typedef E example_type;
			typedef W weight_type;
			typedef W* weight_pointer;
			typedef const W* const_weight_pointer;
			typedef unsigned int size_type;
			typedef typename example_type::const_pointer const_attribute_pointer;
			typedef typename example_type::class_type class_type;
			typedef typename example_type::attribute_type attribute_type;
			typedef perceptron_util<attribute_type> util_type;
			
		public:
			/** Constructs a perceptron model.
			 */
			perceptron_model() : wgtPtr(0), lenInt(0), errmsg(0)
			{
			}
			/** Destructs a perceptron model.
			 */
			~perceptron_model()
			{
				::erase(wgtPtr);
			}
			
		public:
			/** Constructs a copy of a perceptron model.
			 * 
			 * @param m model to copy.
			 */
			perceptron_model(const perceptron_model& m) : wgtPtr(0), lenInt(m.lenInt)
			{
				wgtPtr = copy_model(wgtPtr, m.wgtPtr, m.lenInt);
			}
			/** Assigns a copy of a perceptron model.
			 * 
			 * @param m model to copy.
			 * @return this object.
			 */
			perceptron_model& operator=(const perceptron_model& m)
			{
				wgtPtr = copy_model(wgtPtr, m.wgtPtr, m.lenInt);
				lenInt = m.lenInt;
				return *this;
			}
			/** Makes a shallow copy of a perceptron model.
			 * 
			 * @param m a model to copy.
			 */
			void shallow_copy(perceptron_model& m)
			{
				wgtPtr = m.wgtPtr;
				lenInt = m.lenInt;
				m.wgtPtr = 0;
				m.lenInt = 0;
			}
			
		private:
			static weight_pointer copy_model(weight_pointer to, const_weight_pointer from, size_type n)
			{
				to = ::resize(to, n);
				std::copy(from, from+n, to);
				return to;
			}
			
		public:
			/** Predicts a class for the specified attribute vector.
			 *
			 * @param pred an attribute vector.
			 * @return class index.
			 */
			class_type predict_best_label(const_attribute_pointer pred)const
			{
				if( predict(pred) > 0 ) return 1;
				return 0;
			}
			/** Predicts a class for the specified attribute vector.
			 *
			 * @param pred an attribute vector.
			 * @param parray an array of real-valued predictions.
			 */
			void predict(const_attribute_pointer pred, weight_type* parray, weight_type* pend)const
			{
				weight_type p = predict(pred);
				if( p > 0.0 ) parray[1] = p;
				else parray[0] = -p;
			}
			/** Predicts a class for the specified attribute vector.
			 *
			 * @param pred an attribute vector.
			 * @return a real-valued prediction.
			 */
			weight_type predict(const_attribute_pointer pred)const
			{
				return util_type::dot_product(wgtPtr, wgtPtr+lenInt, pred);
			}
			/** Updates a model for the perceptron.
			 *
			 * @param x an attribute pointer.
			 * @param y a class index.
			 * @param p predicted class.
			 * @param r multiplicative constant.
			 */
			void update(const example_type& ex, class_type p, weight_type r)
			{
				util_type::sum(wgtPtr, wgtPtr+lenInt, ex.x(), r);
			}
			/** Initalize the model.
			 * 
			 * @param acnt attribute count.
			 * @param ycnt class count.
			 */
			void initialize(size_type acnt, size_type ycnt)
			{
				lenInt = acnt;
				wgtPtr = ::resize(wgtPtr, acnt);
				std::fill(wgtPtr, wgtPtr+lenInt, 0.0f);
			}
			/** Finalize the model.
			 */
			void finalize()
			{
			}
			/** Test if the model is empty.
			 * 
			 * @return true if model is empty.
			 */
			bool empty()const
			{
				return lenInt == 0;
			}
			/** Clear the model.
			 */
			void clear()
			{
				::erase(wgtPtr);
				lenInt = 0;
			}
			/** Get the numer of classes.
			 * 
			 * @return 2
			 */
			unsigned int classCount()const
			{
				return 2;
			}
			
		public:
			/** Reads an perceptron model from the input stream.
			 *
			 * @param in an input stream.
			 * @param p a perceptron model.
			 * @return an input stream.
			 */
			friend std::istream& operator>>(std::istream& in, perceptron_model& p)
			{
				in >> p.lenInt;
				if( in.get() != '\n' ) return p.fail(in, "Missing newline character in perceptron model");
				p.wgtPtr = ::resize(p.wgtPtr, p.lenInt);
				for(weight_pointer beg = p.wgtPtr, end = p.wgtPtr+p.lenInt;beg != end;++beg)
				{
					if( in.get() != '\t' ) return p.fail(in, "Missing tab character in perceptron model");
					in >> *beg;
				}
				return in;
			}
			/** Writes an perceptron model to the output stream.
			 *
			 * @param out an output stream.
			 * @param p a perceptron model.
			 * @return an output stream.
			 */
			friend std::ostream& operator<<(std::ostream& out, const perceptron_model& p)
			{
				out << p.lenInt << "\n";
				for(const_weight_pointer beg = p.wgtPtr, end = p.wgtPtr+p.lenInt;beg != end;++beg)
					out << "\t" << std::setprecision(16) << *beg;
				return out;
			}
			
		private:
			/** Sets an error message and flags the stream as failed.
			 *
			 * @param s a stream .
			 * @param m an error message or NULL.
			 */
			void errormsg(std::ios& s, const char* m)const
			{
				if( m != 0 )
				{
					errmsg = m;
					s.setstate( std::ios::failbit );
				}
			}
			/** Sets an error message and flags the stream as failed.
			 *
			 * @param i a stream.
			 * @param m an error message or NULL.
			 * @return a stream.
			 */
			std::istream& fail(std::istream& i, const char* m)const
			{
				errormsg(i, m);
				return i;
			}
			/** Sets an error message and flags the stream as failed.
			 *
			 * @param o a stream.
			 * @param m an error message or NULL.
			 * @return a stream.
			 */
			std::ostream& fail(std::ostream& o, const char* m)const
			{
				errormsg(o, m);
				return o;
			}
			
		public:
			/** Get an error message if reading fails.
			 * 
			 * @return error message or NULL.
			 */
			const char* errormsg()const
			{
				return errmsg;
			}
			
		private:
			weight_pointer wgtPtr;
			size_type lenInt;
			mutable const char* errmsg;
		};
		/** @brief Maintains a multi-class perceptron model
		 * 
		 * This class defines a multi-class perceptron model.
		 */
		template<class W, class E>
		class perceptron_model<W*, E>
		{
			typedef E example_type;
			typedef W weight_type;
			typedef W* weight_pointer;
			typedef const W* const_weight_pointer;
			typedef weight_pointer* weight_pointer2d;
			typedef const const_weight_pointer* const_weight_pointer2d;
			typedef unsigned int size_type;
			typedef typename example_type::const_pointer const_attribute_pointer;
			typedef typename example_type::class_type class_type;
			typedef typename example_type::attribute_type attribute_type;
			typedef perceptron_util<attribute_type> util_type;
			
		public:
			/** Constructs a perceptron model.
			 */
			perceptron_model() : wgtPtr(0), rowInt(0), colInt(0), errmsg(0)
			{
			}
			/** Destructs a perceptron model.
			 */
			~perceptron_model()
			{
				if( wgtPtr != 0 ) ::erase(*wgtPtr);
				::erase(wgtPtr);
			}
			
		public:
			/** Constructs a copy of a perceptron model.
			 * 
			 * @param m model to copy.
			 */
			perceptron_model(const perceptron_model& m) : wgtPtr(0), rowInt(m.rowInt), colInt(m.colInt)
			{
				wgtPtr = copy_model(wgtPtr, m.wgtPtr, m.rowInt, m.colInt);
			}
			/** Assigns a copy of a perceptron model.
			 * 
			 * @param m model to copy.
			 * @return this object.
			 */
			perceptron_model& operator=(const perceptron_model& m)
			{
				wgtPtr = copy_model(wgtPtr, m.wgtPtr, m.rowInt, m.colInt);
				rowInt = m.rowInt;
				colInt = m.colInt;
				return *this;
			}
			/** Makes a shallow copy of a perceptron model.
			 * 
			 * @param m a model to copy.
			 */
			void shallow_copy(perceptron_model& m)
			{
				wgtPtr = m.wgtPtr;
				rowInt = m.rowInt;
				colInt = m.colInt;
				m.wgtPtr = 0;
				m.rowInt = 0;
				m.colInt = 0;
			}
			
		private:
			static weight_pointer2d copy_model(weight_pointer2d to, const_weight_pointer2d from, size_type r, size_type c)
			{
				to = reallocate(to, r, c);
				std::copy(*from, (*from)+(r*c), *to);
				return to;
			}
			static weight_pointer2d reallocate(weight_pointer2d to, size_type r, size_type c)
			{
				weight_pointer val = 0;
				if( to != 0 ) val = *to;
				to = ::resize(to, r);
				*to = val;
				*to = ::resize(*to, r*c);
				for(size_type i=1;i<r;++i) to[i] = to[i-1]+c;
				return to;
			}
			
		public:
			/** Predicts a class for the specified attribute vector.
			 *
			 * @param pred an attribute vector.
			 * @return class index.
			 */
			class_type predict_best_label(const_attribute_pointer pred)const
			{
				weight_type max_py=0, py;
				size_type max_y = 0;
				for(size_type y=0;y<rowInt;++y)
				{
					py = util_type::dot_product(wgtPtr[y], wgtPtr[y]+colInt, pred);
					if( y > 0 && py > max_py )
					{
						max_py = py;
						max_y = y;
					}
				}
				return max_y;
			}
			/** Predicts a class for the specified attribute vector.
			 *
			 * @param pred an attribute vector.
			 * @param parray an array of real-valued predictions.
			 */
			void predict(const_attribute_pointer pred, weight_type* parray, weight_type* pend)const
			{
				ASSERT(rowInt > 0);
				ASSERT(parray != 0);
				ASSERT(wgtPtr != 0);
				for(size_type y=0;y<rowInt;++y)
				{
					ASSERTMSG(pend == 0 || (parray+y) < pend, "y: " << y << " < " << std::distance(parray, pend));
					parray[y] = util_type::dot_product(wgtPtr[y], wgtPtr[y]+colInt, pred);
				}
			}
			/** Predicts a class for the specified attribute vector.
			 *
			 * @param pred an attribute vector.
			 * @return a real-valued prediction.
			 */
			weight_type predict(const_attribute_pointer pred)const
			{
				return 0;
			}
			/** Updates a model for the perceptron.
			 *
			 * @param ex an example (attribute vector, class value)
			 * @param p predicted class.
			 * @param r multiplicative constant.
			 */
			void update(const example_type& ex, class_type p, weight_type r)
			{
				weight_pointer pwgt = wgtPtr[ex.y()];
				util_type::sum(pwgt, pwgt+colInt, ex.x(), r);
				pwgt = wgtPtr[p];
				util_type::sum(pwgt, pwgt+colInt, ex.x(), -r);
			}
			/** Initalize the model.
			 * 
			 * @param acnt attribute count (number of columns).
			 * @param ycnt class count (number of rows).
			 */
			void initialize(size_type acnt, size_type ycnt)
			{
				wgtPtr = reallocate(wgtPtr, ycnt, acnt);
				rowInt = ycnt;
				colInt = acnt;
				std::fill(wgtPtr[0], wgtPtr[0]+rowInt*colInt, 0.0f);
			}
			/** Finalize the model.
			 */
			void finalize()
			{
			}
			/** Test if the model is empty.
			 * 
			 * @return true if model is empty.
			 */
			bool empty()const
			{
				return rowInt == 0;
			}
			/** Clear the model.
			 */
			void clear()
			{
				if( wgtPtr != 0 ) ::erase(*wgtPtr);
				::erase(wgtPtr);
				rowInt = 0;
			}
			/** Get the numer of classes.
			 * 
			 * @return class count (number of rows)
			 */
			unsigned int classCount()const
			{
				return colInt;
			}
			
		public:
			/** Reads an perceptron model from the input stream.
			 *
			 * @param in an input stream.
			 * @param p a perceptron model.
			 * @return an input stream.
			 */
			friend std::istream& operator>>(std::istream& in, perceptron_model& p)
			{
				in >> p.rowInt;
				if( in.get() != '\t' ) return p.fail(in, "Missing tab character in perceptron model");
				in >> p.colInt;
				if( in.get() != '\n' ) return p.fail(in, "Missing newline character in perceptron model");
				p.wgtPtr = reallocate(p.wgtPtr, p.rowInt, p.colInt);
				for(weight_pointer beg = *p.wgtPtr, end = (*p.wgtPtr)+(p.rowInt*p.colInt);beg != end;++beg)
				{
					if( in.get() != '\t' ) return p.fail(in, "Missing tab character in perceptron model");
					in >> *beg;
				}
				return in;
			}
			/** Writes an perceptron model to the output stream.
			 *
			 * @param out an output stream.
			 * @param p a perceptron model.
			 * @return an output stream.
			 */
			friend std::ostream& operator<<(std::ostream& out, const perceptron_model& p)
			{
				out << p.rowInt << "\t" << p.colInt << "\n";
				for(const_weight_pointer beg = *p.wgtPtr, end = (*p.wgtPtr)+(p.rowInt*p.colInt);beg != end;++beg)
					out << "\t" << std::setprecision(16) << *beg;
				return out;
			}
			
		private:
			/** Sets an error message and flags the stream as failed.
			 *
			 * @param s a stream .
			 * @param m an error message or NULL.
			 */
			void errormsg(std::ios& s, const char* m)const
			{
				if( m != 0 )
				{
					errmsg = m;
					s.setstate( std::ios::failbit );
				}
			}
			/** Sets an error message and flags the stream as failed.
			 *
			 * @param i a stream.
			 * @param m an error message or NULL.
			 * @return a stream.
			 */
			std::istream& fail(std::istream& i, const char* m)const
			{
				errormsg(i, m);
				return i;
			}
			/** Sets an error message and flags the stream as failed.
			 *
			 * @param o a stream.
			 * @param m an error message or NULL.
			 * @return a stream.
			 */
			std::ostream& fail(std::ostream& o, const char* m)const
			{
				errormsg(o, m);
				return o;
			}
			
		public:
			/** Get an error message if reading fails.
			 * 
			 * @return error message or NULL.
			 */
			const char* errormsg()const
			{
				return errmsg;
			}
			
		private:
			weight_pointer2d wgtPtr;
			size_type rowInt;
			size_type colInt;
			mutable const char* errmsg;
		};
	};
	
};


/** @brief Type utility interface to a perceptron
 * 
 * This class defines a type utility interface to a perceptron.
 */
template<class A, class C, class W>
struct TypeUtil< ::exegete::perceptron<A,C,W> >
{
	/** Flags class as non-primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::perceptron<A,C,W> value_type;
	/** Tests if a string can be converted to a perceptron.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return perceptron
	 */
	static const char* name() 
	{
		return "perceptron";
	}
};


#endif


