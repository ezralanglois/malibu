/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowImpurity.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWIMPURITY_H
#define _EXEGETE_WILLOWIMPURITY_H
#include "debugutil.h"
#include <cmath>

/** @file WillowImpurity.h
 * 
 * @brief Contains a collection splitting loss functions
 * 
 * This file contains a set of classes defineing willow impurity functions.
 *
 * @todo add gain, gain ratio
 * 
 * 
 * @ingroup ExegeteWillow
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @defgroup ExegeteWillow Willow
 * 
 *  This group holds all the willow files.
 */

namespace exegete
{
	/** @brief Estimates the 1/-1 confidence for a node
	 * 
	 * This class template defines two static methods to calculate the 
	 * probability and confidence of a 1/-1 loss.
	 */
	template<int I>
	struct Loss
	{
		/** Sets the loss for the negative class. **/
		enum{neg=I};
		/** Calculates the confidence of a prediction.
		 *
		 * @param w the total weight for split.
		 * @param wy the weight subtracted for each negative and added for each positive.
		 * @param eps a numerical correction.
		 * @return a confidence value.
		 */
		template<class F>
		inline static F confidence(F w, F wy, F eps)
		{
			return w > 0.0f ? (wy+eps)/(w+eps) : 0.0f;
		}
		/** Calculates the probability of a prediction.
		 *
		 * @param w the total weight for split.
		 * @param wy the weight subtracted for each negative and added for each positive.
		 * @param eps a numerical correction.
		 * @return a probability value.
		 */
		template<class F>
		inline static F probability(F w, F wy, F eps)
		{
			return w > 0.0f ? 0.5f * ( 1 + (wy / w) ) : 0.5f;
		}
	};
	/** @brief Estimates the 1/0 confidence for a node
	 * 
	 * This class template specialization defines two static methods to calculate
	 * the probability and confidence of a 0/1 loss.
	 */
	template<>
	struct Loss<0>
	{
		/** Sets the loss for the negative class. **/
		enum{neg=0};
		/** Calculates the confidence of a prediction.
		 *
		 * @param w the total weight for split.
		 * @param wy the weight added for each positive.
		 * @param eps a numerical correction.
		 * @return a confidence value.
		 */
		template<class F>
		inline static F confidence(F w, F wy, F eps)
		{
			ASSERTMSG(wy >= 0.0f, wy);
			return 0.5f*std::log( (wy+eps) / ((w-wy)+eps) );
		}
		/** Calculates the probability of a prediction.
		 *
		 * @param w the total weight for split.
		 * @param wy the weight added for each positive.
		 * @param eps a numerical correction.
		 * @return a probability value.
		 */
		template<class F>
		inline static F probability(F w, F wy, F eps)
		{
			return w > 0.0f ? wy/w : 0.0f;
		}
	};
	/** @brief Esimates the misclassification loss
	 * 
	 * This class defines the misclassification loss.
	 */
	struct MSCLoss : public Loss<-1>
	{
		/** Calculates the tail end of the loss function.
		 *
		 * @param w total weight over certain examples.
		 * @param wsum the total weight over all examples.
		 * @return 0.0
		 */
		template<class F>
		inline static F tail(F w, F wsum)
		{
			return 0.0f;
		}
		/** Calculates the misclassification impurity.
		 *
		 * @param w the total weight for split.
		 * @param wy the class weight for split.
		 * @param eps a dummy variable.
		 * @param wsum the total weight.
		 * @return impurity for half a split.
		 */
		template<class F>
		inline static F impurity(F w, F wy, F eps, F wsum)
		{
			eps = 0.5f * (w + wy);
			return std::min(eps, w - eps);
		}
		/** Calculates the misclassification impurity for both sides of a split.
		 *
		 * @param wl the total weight for the left split.
		 * @param wyl the class weight for the left split.
		 * @param wr the total weight for the right split.
		 * @param wyr the class weight for the right split.
		 * @param wsum the total weight.
		 * @param eps a dummy variable.
		 * @return impurity for the split.
		 */
		template<class F>
		inline static F impurity(F wl, F wyl, F wr, F wyr, F wsum, F eps)
		{
			return impurity(wl, wyl, eps, wsum) + impurity(wr, wyr, eps, wsum);
		}
		/** Gets the name of the impurity.
		 *
		 * @return Misclassificaton
		 */
		static const char* name(){return "Misclassificaton";}
		/** Gets the code of the impurity.
		 *
		 * @return MSC
		 */
		static const char* code(){return "MSC";}
	};
	/** @brief Estimates the gini index loss
	 * 
	 * This class defines the gini index loss.
	 */
	struct GINLoss : public Loss<-1>
	{
		/** Calculates the tail end of the loss function.
		 *
		 * @param w total weight over certain examples.
		 * @param wsum the total weight over all examples.
		 * @return 0.0
		 */
		template<class F>
		inline static F tail(F w, F wsum)
		{
			return 0.0f;
		}
		/** Calculates the gini impurity.
		 *
		 * @param w the total weight for split.
		 * @param wy the class weight for split.
		 * @param eps a dummy variable.
		 * @param wsum the total weight.
		 * @return impurity for half a split.
		 */
		template<class F>
		inline static F impurity(F w, F wy, F eps, F wsum)
		{
			eps = 0.5f * (w + wy);
			return 2.0f * eps * (1.0f - probability(w, wy, eps));
		}
		/** Calculates the gini impurity for both sides of a split.
		 *
		 * @param wl the total weight for the left split.
		 * @param wyl the class weight for the left split.
		 * @param wr the total weight for the right split.
		 * @param wyr the class weight for the right split.
		 * @param wsum the total weight.
		 * @param eps a dummy variable.
		 * @return impurity for the split.
		 */
		template<class F>
		inline static F impurity(F wl, F wyl, F wr, F wyr, F wsum, F eps)
		{
			//return (2.0*(impurity(wl, wyl) + impurity(wr, wyr))) + (wsum - (wl + wr));
			//return (2.0*(impurity(wl, wyl, eps) + impurity(wr, wyr, eps))) + ( wsum - (wl+wr) );
			return impurity(wl, wyl, eps, wsum) + impurity(wr, wyr, eps, wsum);
		}
		/** Gets the name of the impurity.
		 *
		 * @return GiniIndex
		 */
		static const char* name(){return "GiniIndex";}
		/** Gets the code of the impurity.
		 *
		 * @return GIN
		 */
		static const char* code(){return "GIN";}
	};
	/** @brief Esimates the gini index loss
	 * 
	 * This class defines the gini index loss.
	 */
	struct GYNLoss : public Loss<-1>
	{
		/** Calculates the tail end of the loss function.
		 *
		 * @param w total weight over certain examples.
		 * @param wsum the total weight over all examples.
		 * @return 0.0
		 */
		template<class F>
		inline static F tail(F w, F wsum)
		{
			return 0.0f;
		}
		/** Calculates the gini impurity.
		 *
		 * @param w the total weight for split.
		 * @param wy the class weight for split.
		 * @param eps a dummy variable.
		 * @param wsum the total weight.
		 * @return impurity for half a split.
		 */
		template<class F>
		inline static F impurity(F w, F wy, F eps, F wsum)
		{
			eps = 0.5f * (w + wy);
			return 2.0f * eps * (1.0f - confidence(w, wy, eps));
		}
		/** Calculates the gini impurity for both sides of a split.
		 *
		 * @param wl the total weight for the left split.
		 * @param wyl the class weight for the left split.
		 * @param wr the total weight for the right split.
		 * @param wyr the class weight for the right split.
		 * @param wsum the total weight.
		 * @param eps a dummy variable.
		 * @return impurity for the split.
		 */
		template<class F>
		inline static F impurity(F wl, F wyl, F wr, F wyr, F wsum, F eps)
		{
			return impurity(wl, wyl, eps, wsum) + impurity(wr, wyr, eps, wsum);
		}
		/** Gets the name of the impurity.
		 *
		 * @return GyniIndex
		 */
		static const char* name(){return "GyniIndex";}
		/** Gets the code of the impurity.
		 *
		 * @return GYN
		 */
		static const char* code(){return "GYN";}
	};


	/** @brief Esimates the entroy loss
	 * 
	 * This class defines the entropy loss.
	 */
	struct ENTLoss : public Loss<-1>
	{
		/** Calculates the tail end of the loss function.
		 *
		 * @param w total weight over certain examples.
		 * @param wsum the total weight over all examples.
		 * @return 0.0
		 */
		template<class F>
		inline static F tail(F w, F wsum)
		{
			return 0.0f;
		}
		/** Calculates the entropy impurity.
		 *
		 * @param w the total weight for split.
		 * @param wy the class weight for split.
		 * @param eps a dummy variable.
		 * @param wsum the total weight.
		 * @return impurity for half a split.
		 */
		template<class F>
		inline static F impurity(F w, F wy, F eps, F wsum)
		{
			eps = probability(w, wy, eps);
			wsum = 0.5f * (w + wy);
			if( eps > TypeUtil<F>::fltmin() )
			{
				if( eps < (1.0 - TypeUtil<F>::fltmin()) ) return -1.0f * wsum * std::log(eps) - (w-wsum) * std::log(1.0-eps);
				else return -1.0f * wsum * std::log(eps);
			}
			else if( eps < 1.0 - TypeUtil<F>::fltmin() ) return -1.0f * (w-wsum) * std::log(1.0-eps);
			else return 0.0f;
		}
		/** Calculates the entropy impurity for both sides of a split.
		 *
		 * @param wl the total weight for the left split.
		 * @param wyl the class weight for the left split.
		 * @param wr the total weight for the right split.
		 * @param wyr the class weight for the right split.
		 * @param wsum the total weight.
		 * @param eps a dummy variable.
		 * @return impurity for the split.
		 */
		template<class F>
		inline static F impurity(F wl, F wyl, F wr, F wyr, F wsum, F eps)
		{
			return impurity(wl, wyl, eps, wsum) + impurity(wr, wyr, eps, wsum);
		}
		/** Gets the name of the impurity.
		 *
		 * @return Entropy
		 */
		static const char* name(){return "Entropy";}
		/** Gets the code of the impurity.
		 *
		 * @return ENT
		 */
		static const char* code(){return "ENT";}
	};

	/** @brief Esimates the least-squared loss
	 * 
	 * This class defines the least-squared loss.
	 */
	struct LSQLoss : public Loss<-1>
	{
		/** Calculates the tail end of the loss function.
		 *
		 * @param w total weight over certain examples.
		 * @param wsum the total weight over all examples.
		 * @return 0.0
		 */
		template<class F>
		inline static F tail(F w, F wsum)
		{
			return 0.0f;
		}
		/** Calculates the least-squared impurity.
		 *
		 * @param w the total weight for split.
		 * @param wy the class weight for split.
		 * @param eps a dummy variable.
		 * @param wsum the total sum.
		 * @return impurity for half a split.
		 */
		template<class F>
		inline static F impurity(F w, F wy, F eps, F wsum)
		{
			eps = Loss<-1>::confidence(w, wy, eps);
			return w + eps * eps * w - 2.0f * eps * wy;
		}
		/** Calculates the least-squared impurity for both sides of a split.
		 *
		 * @param wl the total weight for the left split.
		 * @param wyl the class weight for the left split.
		 * @param wr the total weight for the right split.
		 * @param wyr the class weight for the right split.
		 * @param wsum the total weight.
		 * @param eps a dummy variable.
		 * @return impurity for the split.
		 */
		template<class F>
		inline static F impurity(F wl, F wyl, F wr, F wyr, F wsum, F eps)
		{
			return impurity(wl, wyl, eps, wsum) + impurity(wr, wyr, eps, wsum); // + (wsum - (wl+wr));
		}
		/** Gets the name of the impurity.
		 *
		 * @return Least squared
		 */
		static const char* name(){return "Least-squared";}
		/** Gets the code of the impurity.
		 *
		 * @return LSQ
		 */
		static const char* code(){return "LSQ";}
	};
	/** @brief Esimates the top-down loss
	 * 
	 * This class defines the top-down loss.
	 */
	struct KERLoss : public Loss<0>
	{
		/** Calculates the tail end of the loss function.
		 *
		 * @param w total weight over certain examples.
		 * @param wsum the total weight over all examples.
		 * @return 0.0
		 */
		template<class F>
		inline static F tail(F w, F wsum)
		{
			return 0.0f;//( wsum - w );
		}
		/** Calculates the top-down impurity.
		 *
		 * @param w the total weight for split.
		 * @param wy the class weight for split.
		 * @param eps a dummy variable.
		 * @param wsum the total weight.
		 * @return impurity for half a split.
		 */
		template<class F>
		inline static F impurity(F w, F wy, F eps, F wsum)
		{
			return std::sqrt( wy * (w-wy) );
			//return std::sqrt( (eps+wy) * ((w-wy)+eps) );
		}
		/** Calculates the top-down impurity for both sides of a split.
		 *
		 * @param wl the total weight for the left split.
		 * @param wyl the class weight for the left split.
		 * @param wr the total weight for the right split.
		 * @param wyr the class weight for the right split.
		 * @param wsum the total weight.
		 * @param eps a dummy variable.
		 * @return impurity for the split.
		 */
		template<class F>
		inline static F impurity(F wl, F wyl, F wr, F wyr, F wsum, F eps)
		{
			return (2.0*(impurity(wl, wyl, eps, wsum) + impurity(wr, wyr, eps, wsum)));// + tail(wl+wr, wsum);
		}
		/** Gets the name of the impurity.
		 *
		 * @return Kearns&Mansour
		 */
		static const char* name(){return "Kearns&Mansour";}
		/** Gets the code of the impurity.
		 *
		 * @return KER
		 */
		static const char* code(){return "KER";}
	};
	/** @brief Esimates the top-down loss
	 * 
	 * This class defines the top-down loss.
	 */
	struct KMRLoss : public Loss<0>
	{
		/** Calculates the tail end of the loss function.
		 *
		 * @param w total weight over certain examples.
		 * @param wsum the total weight over all examples.
		 * @return tail end of impurity.
		 */
		template<class F>
		inline static F tail(F w, F wsum)
		{
			return ( wsum - w );
		}
		/** Calculates the top-down impurity.
		 *
		 * @param w the total weight for split.
		 * @param wy the class weight for split.
		 * @param eps a dummy variable.
		 * @param wsum the total weight.
		 * @return impurity for half a split.
		 */
		template<class F>
		inline static F impurity(F w, F wy, F eps, F wsum)
		{
			//return std::sqrt( wy * (w-wy) );
			return std::sqrt( (eps+wy) * ((w-wy)+eps) );
		}
		/** Calculates the top-down impurity for both sides of a split.
		 *
		 * @param wl the total weight for the left split.
		 * @param wyl the class weight for the left split.
		 * @param wr the total weight for the right split.
		 * @param wyr the class weight for the right split.
		 * @param wsum the total weight.
		 * @param eps a dummy variable.
		 * @return impurity for the split.
		 */
		template<class F>
		inline static F impurity(F wl, F wyl, F wr, F wyr, F wsum, F eps)
		{
			return (2.0*(impurity(wl, wyl, eps, wsum) + impurity(wr, wyr, eps, wsum))) + tail(wl+wr, wsum);
		}
		/** Calculates the z-pure estimation for early stopping in the ADTree.
		 *
		 * @param w the split weight.
		 * @param wy the class weight.
		 * @param tot the total weight.
		 * @return the z-pure value.
		 */
		template<class F>
		inline static F zpure(F w, F wy, F tot)
		{
			return (2.0*(std::sqrt( 1.0+wy )+std::sqrt( 1.0+(w-wy) ))) + (tot - w);
			//return (2.0*(std::sqrt( wy )+std::sqrt( w-wy ))) + (tot - w);
		}
		/** Gets the name of the impurity.
		 *
		 * @return Kearns&Mansour
		 */
		static const char* name(){return "Kearns&Mansour";}
		/** Gets the code of the impurity.
		 *
		 * @return KMR
		 */
		static const char* code(){return "KMR";}
	};

	/** @brief Esimates the top-down balanced loss
	 * 
	 * This class defines the top-down balanced loss.
	 */
	struct KMBLoss : public Loss<0>
	{
		/** Calculates the tail end of the loss function.
		 *
		 * @param w total weight over certain examples.
		 * @param wsum the total weight over all examples.
		 * @return tail end of the impurity.
		 */
		template<class F>// w0 * [ (W+0A*W-0A) + (W+0B*W-0B) ] - // w1 * [ (W+1A*W-1A) + (W+1B*W-1B) ]
		inline static F tail(F w, F wsum)
		{
			return ( wsum - w );
		}
		/** Calculates the top-down impurity.
		 *
		 * TPR = tp / pos    TNR = tn / neg
		 * FPR = fp / neg    FNR = fn / pos
		 *
		 * @param w the total weight for split.
		 * @param wy the class weight for split.
		 * @param pos positive weight.
		 * @param neg negative weight.
		 * @return impurity for half a split.
		 */
		template<class F>
		inline static F impurity(F w, F wy, F pos, F neg)
		{
			return std::sqrt( (wy/pos) * ((w-wy)/neg) );
		}
		/** Calculates the top-down impurity for both sides of a split.
		 *
		 * @param wl the total weight for the left split.
		 * @param wyl the class weight for the left split.
		 * @param wr the total weight for the right split.
		 * @param wyr the class weight for the right split.
		 * @param wsum the total weight.
		 * @param eps a dummy variable.
		 * @return impurity for the split.
		 */
		template<class F>
		inline static F impurity(F wl, F wyl, F wr, F wyr, F wsum, F eps)
		{
			return (2.0*(impurity(wl, wyl, wl+wr, (wl+wr)-(wyl+wyr)) + impurity(wr, wyr, wl+wr, (wl+wr)-(wyl+wyr)))) + tail(wl+wr, wsum);
		}
		/** Gets the name of the impurity.
		 *
		 * @return Kearns&Mansour-Bal
		 */
		static const char* name(){return "Kearns&Mansour-Bal";}
		/** Gets the code of the impurity.
		 *
		 * @return KMB
		 */
		static const char* code(){return "KMB";}
	};


};


#endif


