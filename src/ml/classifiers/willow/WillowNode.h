/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowNode.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWNODE_H
#define _EXEGETE_WILLOWNODE_H
#include <iostream>
#include <iomanip>
#include "typeutil.h"


/** @file WillowNode.h
 * @brief Contains the willow node class
 * 
 * This file contains the willow node class template.
 *
 * @ingroup ExegeteWillow
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a node in a willow tree
	 * 
	 * This class template defines the basic structure of a willow tree node.
	 */
	template<class A>
	class WillowNode
	{
		union PartitionType
		{
			PartitionType() : s(0) {}
			float f;
			unsigned int n;
			char* s;
		};
	public:
		/** Defines an attribute type. **/
		typedef A attribute_type;
		/** Defines a constant attribute pointer. **/
		typedef const A* const_attribute_pointer;
		/** Defines a float type **/
		typedef union PartitionType partition_type;
		/** Defines a size type **/
		typedef unsigned int size_type;
		/** Defines a node type. **/
		typedef unsigned int node_type;
	public:
		/** Defines a float type. **/
		typedef float float_type;
		/** Defines a node value type. **/
		typedef WillowNode<A> value_type;
		/** Defines a node pointer. **/
		typedef WillowNode<A>* pointer;
		/** Defines a node reference. **/
		typedef WillowNode<A>& reference;
		/** Defines a constant node reference. **/
		typedef const WillowNode<A>& const_reference;
		/** Node types. **/
		enum{ Leaf=-1, Real=0, Nominal=1, Subset=2 };

	public:
		/** Constructs a willow node.
		 */
		WillowNode() : indexInt(TypeUtil<size_type>::max()), orderInt(0), typeCh(Leaf), childernPtr(0)
		{
		}
		/** Destructs a willow node.
		 */
		~WillowNode()
		{
			if( int(typeCh) != Leaf && int(typeCh) > Nominal ) erase(partType.s);
			erase(childernPtr);
		}

	public:
		/** Constructs a deep copy of the willow node.
		 *
		 * @param n a node to copy.
		 */		
		WillowNode(const WillowNode& n) : partType(n.partType), indexInt(n.indexInt), 
		orderInt(n.orderInt), typeCh(n.typeCh), childernPtr(0)
		{
			copy(n);
		}
		/** Assigns a deep copy of the willow node.
		 *
		 * @param n a node to copy.
		 * @return a reference to this object.
		 */
		WillowNode& operator=(const WillowNode& n)
		{
			partType = n.partType;
			indexInt = n.indexInt;
			typeCh = n.typeCh;
			orderInt = n.orderInt;
			copy(n);
			return *this;
		}
		/** Makes a shallow copy of the willow node.
		 *
		 * @param n a node to copy.
		 */
		void shallow_copy(WillowNode& n)
		{
			if( int(typeCh) != Leaf && int(typeCh) > Nominal ) erase(partType.s);
			erase(childernPtr);
			partType = n.partType;
			indexInt = n.indexInt;
			typeCh = n.typeCh;
			orderInt = n.orderInt;
			childernPtr = n.childernPtr;
			if( int(typeCh) != Leaf && int(typeCh) > Nominal ) n.partType.s = 0;
			n.childernPtr=0;
		}
		/** Clears this node and the child array.
		 * 
		 * @param childern a child array to clear.
		 */
		void clear(pointer childern)
		{
			if( int(typeCh) != Leaf && int(typeCh) > Nominal ) erase(partType.s);
			indexInt = TypeUtil<size_type>::max();
			for(size_type i=0,n=size();i<n;++i) childernPtr[i].clear(0);
			if( int(typeCh) == Leaf )
			{
				erase(childern);
				childernPtr = 0;
			}
			else erase(childernPtr);
		}
		/** Clears this node.
		 */
		void clear()
		{
			if( int(typeCh) != Leaf && int(typeCh) > Nominal ) erase(partType.s);
			indexInt = TypeUtil<size_type>::max();
			erase(childernPtr);
			childernPtr = 0;
			partType.s = 0;
		}
		/** Tests if node is empty.
		 *
		 * @return true if node is empty.
		 */
		bool empty()const
		{
			return indexInt == TypeUtil<size_type>::max();
		}
		/** Accept a vistor class (part of vistor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			if( visitor.visit(*this) )
			{
				for(unsigned int i=0, n=size();i<n;++i) childernPtr[i].accept(visitor);
			}
		}

	public:
		/** Gets a child in the array.
		 *
		 * @param n a child index.
		 * @return a reference to a child node.
		 */
		reference operator[](size_type n)
		{
			ASSERT(childernPtr!=0);
			ASSERTMSG(n < size(), n << " < " << size() << " - " << int(typeCh));
			return childernPtr[n];
		}
		/** Gets a child in the array.
		 *
		 * @param n a child index.
		 * @return a reference to a child node.
		 */
		const_reference operator[](size_type n)const
		{
			ASSERT(childernPtr!=0);
			ASSERTMSG(n < size(), n << " < " << size() << " - " << int(typeCh));
			return childernPtr[n];
		}
		/** Sets value for a branch.
		 *
		 * @param t a threshold for ordered splits.
		 * @param i an attribute index.
		 * @param n number of nominal attributes.
		 * @param s a subsetting string.
		 */
		void branch(float_type t, size_type i, size_type n, char* s)
		{
			index(i);
			resize(n, s);
			if( n == Real ) threshold(t);
		}
		/** Resize the number of childern.
		 *
		 * @param n number of childern.
		 * @param s a subsetting string.
		 */
		void resize(size_type n, char* s)
		{
			if( int(typeCh) != Leaf && int(typeCh) > Nominal ) erase(partType.s);
			if( n == Real ) 
			{
				typeCh = Real;
				n = 2;
			}
			else if( s != 0 )
			{
				ASSERTMSG(n>Nominal, n << " > " << Nominal);
				typeCh = n;
				partType.s = s;
				n = 2;
			}
			else 
			{
				typeCh = Nominal;
				partType.n = n;
			}
			erase(childernPtr);
			childernPtr = setsize(childernPtr, n);
		}
		/** Gets the partition an example belongs to.
		 *
		 * @param p a constant attribute pointer.
		 * @return index of partition.
		 */
		size_type belongsTo(const_attribute_pointer p)const
		{
			switch(int(typeCh))
			{
			case Real:
				return p[indexInt] < partType.f?0:1;
			case Leaf:
				return -1;
			case Nominal:
				return size_type(p[indexInt]);
			default:
				ASSERT(partType.s != 0);
				return getvalue(partType.s, size_type(p[indexInt]), typeCh);
			};
		}
		/** Gets the partition the example belongs to.
		 *
		 * @param val a constant attribute pointer.
		 * @return index of partition.
		 */
		size_type belongsTo(A val)const
		{
			switch(int(typeCh))
			{
			case Real:
				return val < partType.f?0:1;
			case Leaf:
				return -1;
			case Nominal:
				return size_type(val);
			default:
				ASSERT(partType.s != 0);
				return getvalue(partType.s, size_type(val), typeCh);
			};
		}
		/** Predicts the binary class of the attribute vector for the ADTree.
		 *
		 * @param p an attribute vector.
		 * @param m an internal missing flag.
		 * @param k early prediction termination.
		 * @return a confidence/probability for a binary class.
		 */
		float_type sumpredict(const_attribute_pointer p, A m, unsigned int k)const
		{
			float_type sum;
			if( orderInt != 0 && orderInt >= k ) return 0.0f;
			if( ismissing(p,m) )
			{
				sum = 0.0f;
				ASSERT(childernPtr != 0);
				for(size_type i=0,n=size();i<n;++i) sum+=childernPtr[i].sumpredict(p,m,k);
				return sum;
			}
			switch(int(typeCh))
			{
			case Real:
				ASSERT(childernPtr != 0);
				return childernPtr[ p[indexInt] < partType.f?0:1 ].sumpredict(p,m,k);
			case Leaf:
				sum = threshold();
				ASSERT(childernPtr != 0);
				for(size_type i=0,n=size();i<n;++i) sum+=childernPtr[i].sumpredict(p,m,k);
				return sum;
			case Nominal:
				ASSERT(childernPtr != 0);
				return childernPtr[ size_type(p[indexInt]) ].sumpredict(p,m,k);
			default:
				ASSERT(childernPtr != 0);
				ASSERT(partType.s != 0);
				return childernPtr[ getvalue(partType.s, size_type(p[indexInt]), typeCh) ].sumpredict(p,m,k);
			};
		}
		/** Predicts the binary class of the attribute vector.
		 *
		 * @param p an attribute vector.
		 * @param m an internal missing flag.
		 * @return a confidence/probability for a binary class.
		 */
		float_type predict(const_attribute_pointer p, A m)const
		{
			if( int(typeCh) == Leaf ) return threshold();
			if( ismissing(p,m) )
			{
				size_type n=size();
				float_type sum=0.0f;
				for(size_type i=0;i<n;++i) sum+=childernPtr[i].predict(p,m);
				return sum / n;
			}
			ASSERT(childernPtr != 0);
			switch(int(typeCh))
			{
			case Real:
				return childernPtr[ p[indexInt] < partType.f?0:1 ].predict(p,m);
			case Nominal:
				return childernPtr[ size_type(p[indexInt]) ].predict(p,m);
			default://Subset
				ASSERTMSG(partType.s != 0, typeCh);
				return childernPtr[ getvalue(partType.s, size_type(p[indexInt]), typeCh) ].predict(p,m);
			};
		}
		/** Tests if attribute is missing (new nominal).
		 *
		 * @param p an attribute vector.
		 * @param missing key for missing value.
		 * @return true if the value is not found.
		 */
		bool ismissing(const_attribute_pointer p, A missing)const
		{
			if( int(typeCh) != Leaf && p[indexInt] == missing ) return true;
			switch(int(typeCh))
			{
			case Leaf:
				return false;
			case Real:
				return false;
			case Nominal:
				return size_type(p[indexInt]) >= partType.n;
			default://Subset
				return size_type(p[indexInt]) >= typeCh;
			};
		}
		/** Store predictions in a collection.
		 * 
		 * @param p a pointer to an attribute vector.
		 * @param vec a collection of predictions.
		 * @param m key for missing value.
		 * @param c current index.
		 * @return current index.
		 */
		template<class T>
		unsigned int predictarray(const_attribute_pointer p, T& vec, A m, unsigned int c=0)const
		{
			typedef typename T::value_type value_type;
			size_type i, n=size();
			if( ismissing(p,m) )
			{
				for(size_type i=0,n=size();i<n;++i) 
					c = childernPtr[i].predictarray(p,vec,m, c);
			}
			switch(int(typeCh))
			{
			case Real:
				c = childernPtr[ p[indexInt] < partType.f?0:1 ].predictarray(p,vec,m,c);
				break;
			case Leaf:
				for(i=0;i<n;++i) c=childernPtr[i].predictarray(p,vec,m,c);
				if( n == 0 ) 
				{
					ASSERT(c<vec.size());
					vec[c] =  value_type(this);
					++c;
				}
				break;
			case Nominal:
				c = childernPtr[ size_type(p[indexInt]) ].predictarray(p,vec,m,c);
				break;
			default:
				ASSERT(partType.s != 0);
				c=childernPtr[ getvalue(partType.s, size_type(p[indexInt]), typeCh) ].predictarray(p,vec,m,c);
			};
			return c;
		}
		/** Store predictions in a collection.
		 * 
		 * @param p a pointer to an attribute vector.
		 * @param vec a collection of predictions.
		 * @param m key for missing value.
		 * @param c current index.
		 * @return current index.
		 */
		template<class T>
		unsigned int predictarray2(const_attribute_pointer p, T& vec, A m, unsigned int c=0)const
		{
			typedef typename T::value_type value_type;
			size_type i, n=size();
			if( c >= vec.size() ) vec.resize(c+1);
			if( ismissing(p,m) )
			{
				for(size_type i=0,n=size();i<n;++i) 
				{
					vec[c].first = this;
					vec[c].second = i;
					c = childernPtr[i].predictarray2(p,vec,m, c);
				}
			}
			switch(int(typeCh))
			{
			case Real:
				vec[c].first = this;
				vec[c].second = (p[indexInt] < partType.f?0:1);
				c = childernPtr[ p[indexInt] < partType.f?0:1 ].predictarray2(p,vec,m,c);
				break;
			case Leaf:
				for(i=0;i<n;++i) c=childernPtr[i].predictarray2(p,vec,m,c);
				if( n == 0 )
				{
					++c;
					if( c >= vec.size() ) vec.resize(c+1, vec.back());
				}
				break;
			case Nominal:
				vec[c].first = this;
				vec[c].second = size_type(p[indexInt]);
				c = childernPtr[ size_type(p[indexInt]) ].predictarray2(p,vec,m,c);
				break;
			default:
				vec[c].first = this;
				vec[c].second = getvalue(partType.s, size_type(p[indexInt]), typeCh);
				ASSERT(partType.s != 0);
				c=childernPtr[ getvalue(partType.s, size_type(p[indexInt]), typeCh) ].predictarray2(p,vec,m,c);
			};
			return c;
		}

	public:
		/** Gets last child added.
		 *
		 * @return last child.
		 */
		reference last()
		{
			ASSERT(indexInt > 0);
			return childernPtr[indexInt-1];
		}
		/** Increments number of childern.
		 */
		void add()
		{
			ASSERT(int(typeCh) == Leaf);
			++indexInt;
		}
		/** Resizes the child vector.
		 *
		 * @param n number of childern.
		 */
		void resize_leaf(size_type n)
		{
			erase(childernPtr);
			childernPtr = setsize(childernPtr, n);
			indexInt = 0;
		}
		/** Sets an attribute split index.
		 *
		 * @param i an attribute index.
		 */
		void index(size_type i)
		{
			indexInt = i;
		}
		/** Gets an attribute split index.
		 *
		 * @return an attribute index.
		 */
		size_type index()const
		{
			return indexInt;
		}
		/** Sets an order index.
		 *
		 * @param i an order index.
		 */
		void order(size_type i)
		{
			orderInt = i;
		}
		/** Gets an order index.
		 *
		 * @return an order index.
		 */
		size_type order()const
		{
			return orderInt;
		}
		/** Sets a value confidence.
		 *
		 * @param i a value confidence.
		 * @param n number of childern.
		 */
		void leaf(float_type i, size_type n)
		{
			resize_leaf(n);
			leaf(i);
		}
		/** Sets a value confidence.
		 *
		 * @param i a value confidence.
		 */
		void leaf(float_type i)
		{
			if( int(typeCh) != Leaf && int(typeCh) > Nominal ) erase(partType.s);
			partType.f = i;
			typeCh = Leaf;
			indexInt = 0;
		}
		/** Sets an attribute value threshold split.
		 *
		 * @param i an value threshold split.
		 */
		void threshold(float_type i)
		{
			partType.f = i;
		}
		/** Gets an attribute value threshold split.
		 *
		 * @return an attribute value threshold split.
		 */
		float_type threshold()const
		{
			return partType.f;
		}
		/** Sets the type of this node.
		 *
		 * @param ch a node type.
		 */
		void type(node_type ch)
		{
			typeCh = ch;
		}
		/** Gets the type of this node.
		 *
		 * @return a node type.
		 */
		node_type type()const
		{
			return typeCh;
		}
		/** Gets the number of childern.
		 *
		 * @return number of childern.
		 */
		size_type size()const
		{
			switch(int(typeCh))
			{
			case Real:
				return 2;
			case Leaf:
				return empty() ? 0 : indexInt;
			case Nominal:
				return partType.n;
			default:
				return 2;
			};
		}
		
	protected:
		/** Makes a deep copy of this node.
		 *
		 * @param n a node to copy.
		 */
		void copy(const WillowNode& n)
		{
			if( n.childernPtr == 0 ) return;
			size_type len = size();
			erase(childernPtr);
			childernPtr = setsize(childernPtr, len);
			for(size_type i=0;i<len;++i) childernPtr[i] = n.childernPtr[i];
		}

	public:
		/** Gets the subset value flag.
		 *
		 * @param s big subset flags.
		 * @param n index of subset bit.
		 * @param len length of subset.
		 * @return subset flag at index.
		 */
		static size_type getvalue(char* s, size_type n, size_type len)
		{
			size_type m = n/sizeof(char);
			n = n - m*sizeof(char);
			ASSERT(s != 0);
			ASSERTMSG(m<(len/sizeof(char)), m << " < " << len);
			return ((s[m]>>n)&1);
		}
		/** Gets the subset value flag.
		 *
		 * @param s big subset flags.
		 * @param n index of subset bit.
		 * @param val subset flag for index.
		 */
		static void setvalue(char* s, size_type n, char val)
		{
			size_type m = n/sizeof(char);
			n = n - m*sizeof(char);
			if( val ) s[m] |= (1<<n);
			else s[m] &= ~(1<<n);
		}
		/** Reads a willow node from the input stream.
		 *
		 * @param in an input stream.
		 * @return an error message or NULL
		 */
		const char* read(std::istream& in)
		{
			if( in.eof() ) return ERRORMSG("Unexpected end of file while reading willow tree");
			char ch=in.get();
			if( in.get() != ' ' ) return ERRORMSG("Missing space after node type");
			switch(ch)
			{
			case 'L':
				typeCh=Leaf;
				in >> indexInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after real node index");
				in >> partType.f;
				if( in.get() != '\n' ) return ERRORMSG("Missing newline after real node threshold");
				break;
			case 'R':
				typeCh=Real;
				in >> orderInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after order index");
				in >> indexInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after real node index");
				in >> partType.f;
				if( in.get() != '\n' ) return ERRORMSG("Missing newline after real node threshold");
				break;
			case 'N':
				typeCh=Nominal;
				in >> orderInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after order index");
				in >> indexInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after nominal node index");
				in >> partType.n;
				if( in.get() != '\n' ) return ERRORMSG("Missing newline after nominal node count");
				break;
			case 'S':
				in >> orderInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after order index");
				in >> indexInt;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after nominal subset node index");
				in >> typeCh;
				if( in.get() != ' ' ) return ERRORMSG("Missing space after nominal subset node subset size");
				partType.s = setsize<char>(typeCh/sizeof(char));
				for(size_type i=0;i<typeCh;++i) setvalue(partType.s, i, in.get());
				break;
			default:
				return ERRORMSG("Unknown node type: \"" << ch << "\"");
			};
			size_type n = size();
			erase(childernPtr);
			childernPtr = setsize(childernPtr, n);
			const char* msg;
			for(size_type i=0;i<n;++i) if( (msg=childernPtr[i].read(in)) != 0 ) return msg;
			return 0;
		}
		/** Writes a willow node to the output stream.
		 *
		 * @param out an output stream.
		 * @return an error message or NULL.
		 */
		const char* write(std::ostream& out)const
		{
			switch(int(typeCh))
			{
			case Leaf:
				out << 'L' << " " << indexInt << " " << partType.f << "\n";
				break;
			case Real:
				out << 'R' << " " << orderInt << " " << indexInt << " " << partType.f << "\n";
				break;
			case Nominal:
				out << 'N' << " " << orderInt << " " << indexInt << " " << partType.n << "\n";
				break;
			default:// Subset
				out << 'S' << " " << orderInt << " " << indexInt << " " << typeCh << " ";
				ASSERT(partType.s != 0);
				for(size_type i=0;i<typeCh;++i) out << getvalue(partType.s, i, typeCh);
				out << "\n";
				break;
			};
			const char* msg;
			ASSERTMSG(childernPtr!=0||size()==0, "typeCh="<<typeCh << " - " << size());
			for(unsigned int i=0, n=size();i<n;++i) if( (msg=childernPtr[i].write(out)) != 0 ) return msg;
			return 0;
		}

	private:
		partition_type partType;
		size_type indexInt;
		size_type orderInt;
		node_type typeCh;

	private:
		pointer childernPtr;
	};


};



#endif


