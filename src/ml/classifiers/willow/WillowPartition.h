/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowPartition.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWPARTITION_H
#define _EXEGETE_WILLOWPARTITION_H
#include "WillowNode.h"
#include "Example.h"
#include <vector>


/** @file WillowPartition.h
 * 
 * @brief Contains the willow partition class.
 * 
 * This file contains the willow partition class.
 *
 * @ingroup ExegeteWillow
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an example partition
	 * 
	 * This class template defines a willow parition, it splits both the 
	 * example and attribute spaces.
	 */
	template<class E, class F>
	class WillowPartition : public std::vector<E>
	{
		typedef std::vector<E> parent_type;
	public:
		/** Defines an example type. **/
		typedef E example_type;
		/** Defines a willow node. **/
		typedef WillowNode< typename E::attribute_type > willow_node;
		/** Defines a pointer to a willow node. **/
		typedef willow_node* node_pointer;
		/** Defines a reference to a willow node. **/
		typedef willow_node& node_reference;
		/** Defines a size type **/
		typedef unsigned int size_type;
		/** Defines a type vector. **/
		typedef std::vector< std::pair<size_type, size_type> > type_vector;
		/** Defines a size utility. **/
		typedef TypeUtil<size_type> size_util;
		/** Defines an example pointer. **/
		typedef E* example_pointer;
		/** Defines a vector iterator. **/
		typedef typename std::vector<E>::iterator iterator;
		/** Defines a float type. **/
		typedef F float_type;

	public:
		/** Constructs a willow partition.
		 *
		 * @param n a node reference.
		 * @param b a start iterator to examples.
		 * @param e a start iterator to examples.
		 * @param a an attribute vector.
		 */
		WillowPartition(node_reference n, example_pointer b, example_pointer e, const type_vector& a) : parent_type(b,e), pnode(&n), cdepth(0), conf(0.0f)
		{
			add(a);
			current = parent_type::begin();
			ASSERTMSG(!parent_type::empty(), std::distance(b,e));
		}
		/** Constructs a willow partition.
		 *
		 * @param n a node reference.
		 * @param len number of examples.
		 * @param c a confidence value.
		 * @param d a depth.
		 */
		WillowPartition(node_reference n, size_type len, float_type c, size_type d) : parent_type(len), pnode(&n), cdepth(d), conf(c)
		{
			current = parent_type::begin();
			ASSERT(!parent_type::empty());
		}
		/** Copies a willow partition.
		 * 
		 * @param p a willow partition to copy.
		 */
		WillowPartition(const WillowPartition& p) : parent_type(p), attrs(p.attrs), pnode(p.pnode), cdepth(p.cdepth), conf(p.conf)
		{
			current = parent_type::begin();
			ASSERT(!parent_type::empty());
		}

	private:
		WillowPartition& operator=(const WillowPartition& p){return *this;}

	public:
		/** Gets the current depth of the splitter.
		 *
		 * @return depth of splitter.
		 */
		size_type depth()const
		{
			return cdepth;
		}
		/** Gets a pointer to start of example partition.
		 *
		 * @return pointer to start of partition.
		 */
		example_pointer begin()
		{
			return &(*parent_type::begin());
		}
		/** Gets a pointer to end of example partition.
		 *
		 * @return pointer to end of partition.
		 */
		example_pointer end()
		{
			return (&parent_type::back())+1;
		}
		/** Gets a vector of attributes.
		 *
		 * @return a vector of attributes.
		 */
		const type_vector& attributes()const
		{
			return attrs;
		}
		/** Gets the current willow node.
		 *
		 * @return a willow node.
		 */
		node_reference node()
		{
			ASSERT(pnode != 0);
			return *pnode;
		}
		/** Gets the confidence in prediction.
		 *
		 * @return confidence in prediction.
		 */
		float_type confidence()const
		{
			return conf;
		}
		/** Adds an attribute vector.
		 *
		 * @param a a type attribute vector.
		 * @param n index of attributes not to add.
		 */
		void add(const type_vector& a, size_type n=-1)
		{
			size_type i=0, j=0;
			attrs.resize(a.size());
			for(;i<a.size();++i)
			{
				if( a[i].second != size_util::max() && a[i].first != n )
				{
					attrs[j] = a[i];
					++j;
				}
			}
			attrs.resize(j);
		}
		/** Add an example to the collection.
		 *
		 * @param ex an example to add.
		 */
		void add(example_type& ex)
		{
			ASSERTMSG(current >= parent_type::begin() && current < parent_type::end(), std::distance(parent_type::begin(), current) << "  " << parent_type::size() );
			*current = ex;
			++current;
		}
		/** Trim allocated examples.
		 */
		void trim()
		{
			parent_type::resize(std::distance(parent_type::begin(), current));
		}
		
	private:
		type_vector attrs;
		node_pointer pnode;
		size_type cdepth;
		float_type conf;
		iterator current;
	};

};


#endif


