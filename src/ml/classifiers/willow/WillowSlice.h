/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowSlice.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWSLICE_H
#define _EXEGETE_WILLOWSLICE_H
#include "WillowNode.h"
#include "Example.h"
#include <vector>


/** @file WillowSlice.h
 * @brief Contains the willow slice class
 * 
 * This file contains the WillowSlice class template.
 *
 * @ingroup ExegeteWillow
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an example partition
	 * 
	 * This class template defines a Willow example slice, it splits both the 
	 * example and attribute spaces.
	 *
	 * @todo add to WillowPartition
	 */
	template<class E>
	class WillowSlice
	{
	public:
		/** Defines an example type. **/
		typedef E example_type;
		/** Defines an example pointer. **/
		typedef E* example_pointer;
		/** Defines a willow node. **/
		typedef WillowNode< typename E::attribute_type > willow_node;
		/** Defines a pointer to a willow node. **/
		typedef willow_node* node_pointer;
		/** Defines a reference to a willow node. **/
		typedef willow_node& node_reference;

	public:
		/** Constructs a willow slice.
		 */
		WillowSlice() : pnode(0), pbeg(0), pend(0), del(false)
		{
		}
		/** Destructs a willow slice.
		 */
		~WillowSlice()
		{
			if(del) erase(pbeg);
		}

	public:
		/** Resizes the example collection.
		 *
		 * @param n a reference to a node.
		 * @param b start of example collection.
		 * @param e end of example collection.
		 */
		void resize(node_reference n, example_pointer b, example_pointer e)
		{
			del = false;
			pnode = &n;
			pbeg = b;
			pend = e;
		}
		/** Resizes the example collection.
		 *
		 * @param n a reference to a node.
		 * @param b start of example collection.
		 * @param e end of example collection.
		 */
		template<class E1>
		void resize(node_reference n, E1 b, E1 e)
		{
			del = true;
			pnode = &n;
			pbeg = pend = setsize<example_type>(e-b);
			for(;b != e;++b,++pend) *pend = *b;
		}
		/** Resizes the example collection.
		 *
		 * @param n a reference to a node.
		 * @param len number of examples.
		 */
		void resize(node_reference n, unsigned int len)
		{
			del = true;
			pnode = &n;
			pbeg = pend = setsize<example_type>(len);
		}
		/** Gets a pointer to start of example partition.
		 *
		 * @return pointer to start of partition.
		 */
		example_pointer begin()
		{
			return pbeg;
		}
		/** Gets a pointer to end of example partition.
		 *
		 * @return pointer to end of partition.
		 */
		example_pointer end()
		{
			return pend;
		}
		/** Gets the current willow node.
		 *
		 * @return a willow node.
		 */
		node_reference node()
		{
			ASSERT(pnode != 0);
			return *pnode;
		}
		/** Add an example to the collection.
		 *
		 * @param ex an example to add.
		 */
		template<class E1>
		void add(E1& ex)
		{
			ASSERT(del);
			*pend = ex;
			ASSERT(pend->w() == ex.w());
			++pend;
		}
		/** Add an example to the collection.
		 *
		 * @param ex an example to add.
		 */
		void add(example_type& ex)
		{
			ASSERT(del);
			*pend = ex;
			ASSERT(pend->w() == ex.w());
			++pend;
		}
		/** Trim allocated examples.
		 */
		void trim()
		{
			unsigned int n = (unsigned int)std::distance(pbeg, pend);
			if(del) 
			{
				pbeg = ::resize(pbeg, n);
				pend = pbeg + n;
			}
		}
		/** Gets size of example array.
		 *
		 * @return size of example array.
		 */
		unsigned int size()const
		{
			return pend-pbeg;
		}

	private:
		node_pointer pnode;
		example_pointer pbeg;
		example_pointer pend;
		bool del;
	};

};


#endif


