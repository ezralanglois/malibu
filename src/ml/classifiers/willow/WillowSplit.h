/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowSplit.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWSPLITNODE_H
#define _EXEGETE_WILLOWSPLITNODE_H
#include "WillowNode.h"
#include "Example.h"


/** @file WillowSplit.h
 * @brief Contains the willow split class
 * 
 * This file contains the WillowSplit class template.
 *
 * @ingroup ExegeteWillow
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a willow split
	 * 
	 * This class template defines a Willow split.
	 */
	template<class L, class F, class I>
	class WillowSplit
	{
	public:
		/** Defines a willow node. **/
		typedef WillowNode< typename L::attribute_type > willow_node;
		/** Defines a learning set. **/
		typedef L learnset_type;
	public:
		/** Defines an example type. **/
		typedef typename L::value_type			example_type;
		/** Defines an example pointer. **/
		typedef typename L::iterator			example_pointer;
		/** Define an attribute value type. **/
		typedef typename L::feature_type		feature_type;
		/** Define an attribute type. **/
		typedef typename L::attribute_type		attribute_type;
		/** Defines a float type. **/
		typedef F								float_type;
	public:
		/** Defines a size type. **/
		typedef unsigned int					size_type;
		/** Defines a type vector. **/
		typedef std::vector< std::pair<size_type, size_type> > type_vector;
		/** Defines a float pointer. **/
		typedef F*								float_pointer;
		/** Defines an impurity type. **/
		typedef I								impurity_type;
		/** Defines a float type utility. **/
		typedef TypeUtil<float_type>			float_util;
	private:
		typedef typename willow_node::float_type node_float;
		
	public:
		/** Constructs default a willow split.
		 * 
		 * @param e epsilon error.
		 * @param t total weight.
		 */
		WillowSplit(float_type e, float_type t) : subsetting(0), 
													minerr(float_util::max()), poscost(1.0), negcost(1.0), 
													eps(e), tot(t), conf(0), yconf(0), temp(0), ytemp(0)
		{
		}
		/** Destructs a willow split.
		 */
		~WillowSplit()
		{
			::erase(temp);
			::erase(ytemp);
			::erase(conf);
			::erase(yconf);
			::erase(subsetting);
		}

	public:
		/** Finds the best split over a collection of examples on a single attribute.
		 *
		 * @param node a destination node.
		 * @param dataset a dataset.
		 * @param subset should use subsetting.
		 */
		void split(willow_node& node, learnset_type& dataset, bool subset)
		{
			minerr = float_util::max();
			split(dataset, dataset.begin(), dataset.end(), subset);
			shallow_update(node);
		}
		/** Finds the best split over a subset of examples on a single attribute.
		 *
		 * @param dataset a dataset.
		 * @param ebeg start example collection.
		 * @param eend end example collection.
		 * @param subset should use subsetting.
		 */
		template<class E>
		void split(learnset_type& dataset, E ebeg, E eend, bool subset)
		{
			for(typename learnset_type::header_iterator beg=dataset.header_begin(), curr=beg, end=dataset.header_end();curr != end;++curr)
			{
				if( curr->size() == 1 ) continue;
				if( curr->isnominal() )
				{
					 if( subset ) split_subset(ebeg, eend, size_type(std::distance(beg,curr)), curr->size());
					 else split_unordered(ebeg, eend, size_type(std::distance(beg,curr)), curr->size());
				}
				else split_ordered(ebeg, eend, size_type(std::distance(beg,curr)));
			}
		}
		/** Finds the best split over a collection of examples on a single attribute.
		 *
		 * @param b iterator to start of examples.
		 * @param e iterator to end of examples.
		 * @param attrs a vector of attributes.
		 * @param subset should use subsetting.
		 */
		void split(example_pointer b, example_pointer e, const type_vector& attrs, bool subset)
		{
			ASSERT(b < e);
			minerr = float_util::max();
			for(typename type_vector::const_iterator curr=attrs.begin(), end=attrs.end();curr != end;++curr)
			{
				//ASSERT(curr->second != 1);
				if( curr->second == 1 ) continue;
				if( curr->second != 0 )
				{
					if( subset ) split_subset(b, e, size_type(curr->first), curr->second);
					else		 split_unordered(b, e, size_type(curr->first), curr->second);
				}
				else split_ordered(b, e, size_type(curr->first));
			}
		}

	public:
		/** Split a collection of examples based on an ordered attribute producing a binary split.
		 *
		 * @param b an iterator to start of examples.
		 * @param e an iterator to end of examples.
		 * @param a an attribute index.
		 */
		template<class E>
		void split_ordered(E b, E e, size_type a)
		{
			E beg = b;
			float_type thresh = float_util::max();
			float_type wsum = 0.0f, wysum = 0.0f;
			float_type wl=0.0f, wyl=0.0f, wr=0.0f, wyr=0.0f;
			float_type lft=0.0f, rgt=0.0f, yrgt=0.0f, ylft=0.0f, err=0.0f;
			feature_type prev=feature_type();

			std::stable_sort( b, e, CompareExample<example_pointer>(a) );
			if( b != e ) prev = b->x()[a];
			for(;b != e;++b)
			{
				if( b->x()[a] == example_type::missing() )break;
				wr = b->weight(eps);
				wsum  += wr;
				wysum += wr * b->y();
			}
			e = b; b = beg; //if( b != e ) ++b;
			while( b != e )
			{
				//prev = (b-1)->x()[a];
				wr = wsum - wl;
				wyr = wysum - wyl;
				err=impurity_type::impurity(wl, wyl, wr, wyr, tot, eps);
				ASSERT( b->x()[a] != example_type::missing() );
				if( err < minerr )
				{//savings@sprintemi.com
					minerr = err;
					thresh = (prev+b->x()[a])*0.5;
					lft = wl; ylft = wyl;
					rgt = wr; yrgt = wyr;
				}
				do{
					wr = b->weight(eps);
					wl += wr;
					wyl += wr * b->y();
					++b;
				}while( b != e && b->x()[a] == prev );
				if( b != e ) prev = b->x()[a];//new
			}
			if( thresh != float_util::max() )
			{
				nominalInt = 0;
				indexInt = a;
				threshldFlt = thresh;
				yconf = ::resize(yconf, 2);
				conf = ::resize(conf, 2);
				yconf[0] = ylft; conf[0] = lft;
				yconf[1] = yrgt; conf[1] = rgt;
			}
		}
		/** Split a collection of examples based on an unordered attribute producing multiple splits.
		 *
		 * @param b start of example collection.
		 * @param e end of example collection.
		 * @param a an attribute index.
		 * @param nom number of nominal attributes.
		 */
		template<class E>
		void split_unordered(E b, E e, size_type a, size_type nom)
		{
			float_type err = 0.0f, terr = 0.0f, dum=0.0f;
			unorder_table(b, e, a, nom);
			for(size_type i=0;i<nom;++i) 
			{
				terr+=temp[i];
				err += impurity_type::impurity(temp[i], ytemp[i], eps, dum);
			}
			err += impurity_type::tail(terr, tot);
			if( err < minerr )
			{
				yconf = ::resize(yconf, nom);
				conf = ::resize(conf, nom);
				for(unsigned int i=0;i<nom;++i)
				{
					yconf[i] = ytemp[i];
					conf[i] = temp[i];
				}
				indexInt = a;
				nominalInt = nom;
				minerr = err;
				threshldFlt=0.0f;
			}
		}
		/** Split a collection of examples based on an unordered attribute producing a binary split.
		 *
		 * @param b start of example collection.
		 * @param e end of example collection.
		 * @param a an attribute index.
		 * @param nom number of nominal attributes.
		 */
		template<class E>
		void split_subset(E b, E e, size_type a, size_type nom)
		{
			typedef std::vector< std::pair<float_type, unsigned int> > pair_vector;
			float_type wsum = 0.0f, wysum = 0.0f, err=0.0f;
			float_type wl=0.0f, wyl=0.0f, wr=0.0f, wyr=0.0f;
			float_type lft=0.0f, rgt=0.0f, yrgt=0.0f, ylft=0.0f;
			size_type index=nom+1;
			pair_vector subsets(nom);
			unorder_table(b, e, a, nom);
			for(unsigned int i=0;i<nom;++i)
			{
				wsum+=temp[i];
				wysum+=ytemp[i];
				subsets[i] = std::make_pair(ytemp[i], i);
			}
			std::stable_sort(subsets.begin(), subsets.end());
			for(size_type i=0;i<nom;++i)
			{
				wr = wsum - wl;
				wyr = wysum - wyl;
				err=impurity_type::impurity(wl, wyl, wr, wyr, tot, eps);
				if(err < minerr)
				{
					index = i;
					minerr = err;
					lft = wl; ylft = wyl;
					rgt = wr; yrgt = wyr;
				}
				wl+=temp[subsets[i].second];
				wyl+=subsets[i].first;
			}
			if( index < nom )
			{
				size_type len = nom/sizeof(char);
				subsetting = ::resize(subsetting, len);
				for(size_type i=0;i<len;++i) subsetting[i] = 0;
				for(unsigned int i=0;i<nom;++i) willow_node::setvalue(subsetting, subsets[i].second, i>=index);
				indexInt = a;
				nominalInt = nom;
				minerr = err;
				threshldFlt=1.0f;
				yconf = ::resize(yconf, 2);
				conf = ::resize(conf, 2);
				yconf[0] = ylft; conf[0] = lft;
				yconf[1] = yrgt; conf[1] = rgt;
			}
		}

	private:
		template<class E>
		void unorder_table(E beg, E end, size_type a, long nom)
		{
			float_type wr;
			feature_type val;
			ytemp = ::resize(ytemp, nom);
			temp = ::resize(temp, nom);
			std::fill(ytemp, ytemp+nom, 0.0f);
			std::fill(temp, temp+nom, 0.0f);
			for(;beg != end;++beg)
			{
				val = beg->x()[a];
				if( val == example_type::missing() ) continue;
				wr = beg->weight(eps);
				ASSERT(val < nom);
				temp[ size_type(val) ] += wr;
				ytemp[ size_type(val) ] += wr * beg->y();
			}
		}

	public:
		/** Shallow copy and update a node with leaves.
		 *
		 * @param node a willow node.
		 * @param m number of node childern.
		 */
		void shallow_update(willow_node& node, size_type m)
		{
			shallow_copy(node);
			for(size_type i=0,n=node.size();i<n;++i) 
			{
				node[i].leaf( (node_float)confidence(i,false), m );
			}
		}
		/** Shallow copy and update a node with leaves.
		 *
		 * @param node a willow node.
		 */
		void shallow_update(willow_node& node)
		{
			shallow_copy(node);
			for(size_type i=0,n=node.size();i<n;++i) node[i].leaf( (node_float)confidence(i,false) );
		}
		/** Shallow copy a willow node.
		 *
		 * @param node a willow node.
		 */
		void shallow_copy(willow_node& node)
		{
			if( threshldFlt > 0 )
			{
				node.branch((node_float)threshldFlt, indexInt, nominalInt, subsetting);
				if(nominalInt > 0) subsetting = 0;
			}
			else node.branch((node_float)threshldFlt, indexInt, nominalInt, 0);
		}
		/** Test if node is unbiased toward a class.
		 *
		 * @param t threshold of prediction.
		 * @return true if all leaves point to difference classes.
		 */
		bool isunbiased(float_type t)
		{
			size_type n = nominalInt, m = 0;
			if( n == 0 || subsetting != 0 ) n = 2;
			for(size_type i=0;i<n;++i)
				if( confidence(i, t==0.5f) > t ) m++;
			return 0 < m && m < n;
		}
		/** Shallow copy a willow node.
		 *
		 * @param node a willow node.
		 * @param prob a confidence value.
		 * @param t threshold of prediction.
		 * @param isroot is node root
		 * @return true if branch is added.
		 */
		bool shallow_copy(willow_node& node, float_type prob, float_type t, bool isroot)
		{
			if( minerr != float_util::max() && (isroot || isunbiased(t) ) ) 
			{
				shallow_copy(node);
				return true;
			}
			else 
			{
				node.leaf( (node_float)prob );
				return false;
			}
		}
		/** Calculates the confidence value based on fraction in partition and
		 * partition conditioned on class.
		 *
		 * @param i a partition index.
		 * @param prob should use probability.
		 * @return confidence value.
		 */
		float_type confidence(size_type i, bool prob)const
		{
			ASSERTMSG( (nominalInt > 0 && threshldFlt == 0.0f ? i < nominalInt : i < 2 ), i << ", " << nominalInt << ", " << threshldFlt );
			return prob ? impurity_type::probability(conf[i],yconf[i],eps) : impurity_type::confidence(conf[i],yconf[i],eps);
		}
		/** Calculates the probability based on fraction in partition and
		 * partition conditioned on class.
		 *
		 * @param i a partition index.
		 * @return confidence value.
		 */
		float_type probability(size_type i)const
		{
			ASSERTMSG( ((nominalInt > 0 && threshldFlt == 0.0f) ? i < nominalInt : i < 2 ), i );
			return impurity_type::probability(conf[i],yconf[i],eps);
		}
		/** Resets the minimum error.
		 */
		void reset()
		{
			minerr = float_util::max();
		}
		/** Gets the minimum error.
		 *
		 * @return minimum error.
		 */
		float_type minErr()const
		{
			return minerr;
		}
		/** Gets the max error for the split.
		 *
		 * @return max error.
		 */
		static float_type maxErr()
		{
			return float_util::max();
		}
		/** Sets the total weight for the dataset.
		 *
		 * @param t the total weight.
		 */
		void total(float_type t)
		{
			tot = t;
		}
		/** Gets the total weight for the dataset.
		 *
		 * @return the total weight.
		 */
		float_type total()const
		{
			return tot;
		}
		/** Sets the positive cost probability.
		 *
		 * @param pcst the positive cost.
		 */
		void positiveCost(float_type pcst)
		{
			poscost = pcst;
			negcost = 1 - pcst;
		}

	private:
		char* subsetting;
		float_type minerr;
		float_type poscost;
		float_type negcost;
		float_type eps;
		float_type tot;
		float_pointer conf;
		float_pointer yconf;
		float_pointer temp;
		float_pointer ytemp;

	private:
		float_type threshldFlt;
		size_type indexInt;
		size_type nominalInt;
	};

};


#endif


