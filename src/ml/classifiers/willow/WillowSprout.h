/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowSprout.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WILLOWSPROUT_H
#define _EXEGETE_WILLOWSPROUT_H
#include "WillowSplit.h"
#include "WillowPartition.h"
#include "ArgumentMap.h"
#include <list>


/** @file WillowSprout.h
 * 
 * @brief Holds a set of classes for sprouting a willow tree
 * 
 * This file contains classes to sprout a willow tree.
 * 	- WillowSproutArgs
 * 	- WillowSprout
 * 	- WillowSproutBFS
 *
 * @ingroup ExegeteWillow
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Holds the arguments for growing a tree.
	 * 
	 * This class holds the arguments for growing a tree.
	 */
	struct WillowSproutArgs
	{
		/** Constructs a default willow arguments.
		 */
		WillowSproutArgs() : reuseInt(0), subsetInt(1), randsetInt(0), 
		maxdepthInt(0), minobjsInt(2), probabilityInt(1), 
		confidenceFlt(0.8f)//, minweightFlt(0.0f)
		{
		}
		/** Flags the reuse of attributes. **/
		int reuseInt;
		/** Use binary split by grouping nominal attributes. **/
		int subsetInt;
		/** Size of random subset of the attributes. **/
		unsigned int randsetInt;
		/** Holds the maximum depth. **/
		int maxdepthInt;
		/** Holds the minimum number of objects. **/
		int minobjsInt;
		/** Flags the probability. **/
		int probabilityInt;
		/** Holds the confidence. **/
		float confidenceFlt;
		/* Holds minimum weight for a node. **/
		//float minweightFlt;
	};

	/** @brief Abstract interface to willow growing algorithm
	 * 
	 * This class template defines a Willow growth algorithm interface.
	 */
	template<class L, class F>
	class WillowSprout
	{
	public:
		/** Defines a float type. **/
		typedef F float_type;
		/** Defines a learnset type. **/
		typedef L learnset_type;
		/** Defines a willow tree model. **/
		typedef WillowNode<typename L::attribute_type> willow_node;
		/** Defines growing arguments. **/
		typedef WillowSproutArgs argument_type;
		
	public:
		/** Destructs a willow sprout.
		 */
		virtual ~WillowSprout()
		{
		}

	public:
		/** Sprouts a willow tree.
		 *
		 * @param node a root node to the tree model.
		 * @param set a learning set.
		 * @param args a set of arguments.
		 */
		virtual void sprout(willow_node& node, learnset_type& set, const argument_type& args)=0;		
		/** Gets the name of the growing algorithm.
		 *
		 * @return growing algorithm name.
		 */
		virtual std::string name()const=0;
	};

	
	/** @brief Grows a willow tree using BFS
	 * 
	 * This class grows a willow tree using BFS.
	 *
	 * @todo add min weight.
	 */
	template<class L, class F, class I>
	class WillowSproutBFS : public WillowSprout<L, F>
	{
		typedef WillowSprout<L, F> parent_type;
		typedef typename parent_type::float_type float_type;
		typedef typename parent_type::learnset_type learnset_type;
		typedef typename parent_type::willow_node willow_node;
		typedef typename parent_type::argument_type argument_type;
		typedef typename learnset_type::iterator example_iterator;
		typedef typename learnset_type::value_type example_type;
		typedef WillowSplit<learnset_type, float_type, I> split_type;
		typedef WillowPartition<example_type, float_type> partition_type;
		typedef typename partition_type::type_vector type_vector;
		typedef typename partition_type::size_type size_type;
		typedef typename partition_type::size_util size_util;
		typedef std::list<partition_type> partition_queue;
		typedef typename partition_queue::iterator partition_iterator;
		typedef std::vector<typename partition_queue::iterator> part_vector;
		typedef typename willow_node::float_type node_float;
	public:
		/** Destructs a willow sprout BFS.
		 */
		virtual ~WillowSproutBFS()
		{
		}
		
	public:
		/** Sprouts a willow tree.
		 *
		 * @param node a root node to the tree model.
		 * @param set a learning set.
		 * @param args a set of arguments.
		 */
		void sprout(willow_node& node, learnset_type& set, const argument_type& args)
		{
			example_iterator beg, end;
			size_type i,n,depth,attrn,j;
			size_type maxdepth = args.maxdepthInt;
			size_type minobjs = args.minobjsInt;
			bool useprob = args.probabilityInt == 1;
			bool subset = args.subsetInt == 1;
			//bool reuse = args.reuseInt == 1;
			float_type maxconf = args.confidenceFlt;
			float_type eps = 1.0f/set.size(), conf;
			float_type tot = setup_examples(set.begin(), set.end(), eps);
			split_type splitter(eps, tot);
			type_vector attrs(set.attributeCount());
			part_vector parts;
			partition_queue queue;
			setup_attributes(set, attrs, args.randsetInt);

			node.clear();
			ASSERT(set.begin() != set.end());
			queue.push_back(partition_type(node, set.begin(), set.end(), attrs));
			while( !queue.empty() )
			{
				partition_type& part = queue.front();
				willow_node& curnode=part.node();
				splitter.split(part.begin(), part.end(), part.attributes(), subset);
				if(splitter.shallow_copy(curnode, part.confidence(), (useprob?0.5:0.0), (&curnode) == (&node) ))
				{
					depth = part.depth()+1;
					attrn = (size_type)part.attributes().size();
					if( (maxdepth == 0 || depth < maxdepth) && attrn > 1 )
					{
						n=curnode.size();j=0;
						parts.resize(n);
						for(i=0;i<n;++i)
						{
							conf = splitter.confidence(i, useprob);
							if( !isWithin(splitter.probability(i), maxconf) )
							{
								curnode[i].leaf((node_float)conf);
								parts[i] = queue.end(); ++j;
							}
							else parts[i] = queue.insert(queue.end(), partition_type(curnode[i], (size_type)part.size(), conf, depth));
						}
						if( j < n )
						{
							j = curnode.index();
							ASSERT(j<set.attributeCount());
							for(beg=part.begin(), end=part.end();beg != end;++beg)
							{
								if( beg->x()[j] != example_type::missing() )
								{
									i = curnode.belongsTo(beg->x());
									ASSERT(i < parts.size());
									if( parts[i] != queue.end() ) parts[i]->add(*beg);
								}
								else 
								{
									for(i=0;i<n;++i) if( parts[i] != queue.end() ) parts[i]->add(*beg);
								}
							}
							for(i=0;i<n;++i)
							{
								if( parts[i] == queue.end() ) continue;
								parts[i]->trim();
								if( parts[i]->size() < minobjs || parts[i]->size() > (part.size()-minobjs) )
								{
									curnode[i].leaf((node_float)splitter.confidence(i, useprob));
									queue.erase(parts[i]);
								}
								else parts[i]->add(part.attributes(), j);
							}
						}
					}
					else
					{
						for(i=0,n=curnode.size();i<n;++i) curnode[i].leaf((node_float)splitter.confidence(i, useprob));
					}
				}
				else{ASSERT( (&curnode) != (&node) );}
				queue.pop_front();
			}
			ASSERT( node[0].index() != ((unsigned int)-1) );
		}
		/** Gets the name of the growing algorithm.
		 *
		 * @return algorithm name.
		 */
		std::string name()const
		{
			return I::name();
		}

	private:
		static bool isWithin(float_type val, float_type conf)
		{
			if( conf >= 1.0 ) return true;
			if( val < 0.5 ) val = 1.0 - val;
			return 0.5001 < val && val < conf;
		}
		static float_type setup_examples(example_iterator beg, example_iterator end, float_type eps)
		{
			float_type tot=0.0f;
			for(;beg != end;++beg) 
			{
				tot+=beg->weight(eps);
				beg->y( beg->y()>0?1:I::neg );
			}
			return tot;
		}
		static void setup_attributes(learnset_type& set, type_vector& attrs, size_type randset)
		{
			for(size_type i=0;i<set.attributeCount();++i)
			{
				attrs[i].first = i;
				if( set.attributeAt(i).isnominal() ) attrs[i].second = size_type(set.attributeAt(i).size());
				else attrs[i].second = 0;
			}
			if( randset > 0 && randset < set.attributeCount() )
			{
				size_type tot = (size_type)set.attributeCount();
				size_type n = tot-randset;
				for(size_type i=0, m, j;i<n;++i)
				{
					m = random_int(tot-i-1);
					if( attrs[m].second == size_util::max() )
					{
						for(j=0;j<tot;j++)
						{
							if( attrs[j].second != size_util::max() )
							{
								if( m == 0 )
								{
									attrs[j].second = size_util::max();
									break;
								}
								else --m;
							}
						}
					}
					else attrs[m].second = size_util::max();
				}
			}
		}
	};


};

#endif


