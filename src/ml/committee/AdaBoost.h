/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AdaBoost.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ADABOOST_H
#define _EXEGETE_ADABOOST_H
#include "Committee.h"
#include "WeakLearner.h"
#include "ExampleSetAlgorithms.h"
#include "mathutil.h"

/** @file AdaBoost.h
 * @brief AdaBoost committee wrapper
 * 
 * This file contains the AdaBoost class template.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#define _ADABOOST_VERSION 101000

namespace exegete
{
	/** @brief AdaBoost committee wrapper
	 * 
	 * This class template defines an AdaBoost committee classifier.
	 * 
	 * @todo write confidence to model file
	 */
	template<class T>
	class AdaBoost : public Committee< WeakLearner<T> >
	{
		typedef T learner_type;
		typedef typename T::weight_type learner_weight;
		typedef IsVoid<learner_weight> is_void;
		typedef typename SelectType<is_void::value,double,learner_weight>::Result w_type;
		typedef Committee< WeakLearner<T> > committee_type;
		typedef typename committee_type::learner_type wlearner_type;
		typedef typename committee_type::size_type size_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator iterator;
		typedef typename learner_type::dataset_type subset_type;
		typedef typename subset_type::attribute_type attribute_type;
		typedef typename subset_type::class_type class_type;
	public:
		/** Flags AdaBoost as tunable. **/
		enum{ TUNE=1, ARGUMENT=T::ARGUMENT+1, FAST_TUNE=1 };

	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines dataset type. **/
		typedef ExampleSet<attribute_type, class_type, w_type>	dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef void											weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef int prediction_vector;
		
	private:
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename dataset_type::bag_iterator				bag_iterator;
		typedef typename dataset_type::iterator					example_iterator;
		typedef typename dataset_type::const_iterator			const_example_iterator;
		typedef typename dataset_type::const_bag_iterator		const_bag_iterator;

	public:
		/** Constructs a default AdaBoost classifier.
		 */		
		AdaBoost() : iterationInt(0), confratedCh('n'), milBool(parent_type::EVAL), alphaFlt(0.0f),
					 iterationSel(iterationInt, "Iteration", true, range_type(1024, 1025, 1, '+'))
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Destructs an AdaBoost classifier.
		 */
		~AdaBoost()
		{
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t=0)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(AdaBoost));
				if(t>=0)
				arginit(map, iterationSel,  "boostn",  "number of boosting iterations");
				else
				arginit(map, iterationInt,  "boostn",  "number of boosting iterations");
				arginit(map, confratedCh, 	"boostc",  "force confidence-rated boosting>Auto:a;Yes:y;No:n");
				if(is_void::value) 
				arginit(map, alphaFlt,	  	"boostr",  "sample without replacement proportion");
				if(!milBool)
				arginit(map, milBool,	  	"boostmil", "boost bag weights for MIL?");
			}
		}

	public:
		/** Constructs a deep copy of the AdaBoost classifier.
		 *
		 * @param boost a source AdaBoost model.
		 */
		AdaBoost(const AdaBoost& boost) : committee_type(boost), iterationInt(boost.iterationInt), confratedCh(boost.confratedCh), milBool(boost.milBool), alphaFlt(boost.alphaFlt),
										  iterationSel(iterationInt, boost.iterationSel)
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Assigns a deep copy of the AdaBoost model.
		 *
		 * @param boost a source AdaBoost model.
		 * @return a reference to this object.
		 */
		AdaBoost& operator=(const AdaBoost& boost)
		{
			committee_type::operator=(boost);
			argument_copy(boost);
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param fbeg start of attribute vectors.
		 * @param fend end of attribute vectors.
		 * @return a real-valued prediction.
		 */
		template<class I>
		float_type predict(I fbeg, I fend)const
		{
			ASSERT(!committee_type::empty());
			if( committee_type::empty() ) return 0.0f;
			float_type sum=0.0f;
			const_iterator beg=committee_type::begin(), end=beg+std::min(iterationInt, committee_type::size());
			if( confidence_rated() )//committee_type
			{
				if( committee_type::threshold() > 0.0f )
				{
					for(;beg != end;++beg) sum+=conf(predict_bag(*beg, fbeg, fend));
				}
				else
				{
					for(;beg != end;++beg) sum+=predict_bag(*beg, fbeg, fend);
				}
			}
			else
			{
				for(;beg != end;++beg) sum+=sign(predict_bag(*beg, fbeg, fend), committee_type::threshold())*beg->weight();
			}
			return sum;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			ASSERT(!committee_type::empty());
			if( committee_type::empty() ) return 0.0f;
			float_type sum=0.0f;
			const_iterator beg=committee_type::begin(), end=beg+std::min(iterationInt, committee_type::size());

			if( confidence_rated() )//committee_type
			{
				if( committee_type::threshold() > 0.0f )
				{
					for(;beg != end;++beg) sum+=conf(beg->predict(pred));
				}
				else
				{
					for(;beg != end;++beg) sum+=beg->predict(pred);
				}
			}
			else
			{
				for(;beg != end;++beg) sum+=sign(beg->predict(pred), committee_type::threshold())*beg->weight();
			}
			return sum;
		}
		/** Builds a model for the AdaBoost classifier.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			if( milBool && learnset.bagCount() == 0 ) return ERRORMSG("boostmil set true and dataset does not have bags");
			if( iterationInt == 0 ) iterationInt = learnset.size()*2;
			committee_type::resize(iterationInt);
			if( milBool )
			{
				initwgts(learnset.bag_begin(), learnset.bag_end(), float_type(learnset.bagCount()));
				return train(learnset, learnset.bag_begin(), learnset.bag_end(), is_void());
			}
			else
			{
				initwgts(learnset.begin(), learnset.end(), float_type(learnset.size()));
				return train(learnset, learnset.begin(), learnset.end(), is_void());
			}
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return AdaBoost
		 */
		static std::string name()
		{
			return "AdaBoost";
		}
		/** Get the version of the AdaBoost classifier.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _ADABOOST_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return boost
		 */
		static std::string prefix()
		{
			return "boost";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationSel;
		}
		/** Copies arguments in an AdaBoost model.
		 * 
		 * @param boost a source boost model.
		 */
		void argument_copy(const AdaBoost& boost)
		{
			iterationInt = boost.iterationInt;
			confratedCh = boost.confratedCh;
			milBool = boost.milBool;
			alphaFlt = boost.alphaFlt;
			iterationSel = boost.iterationSel;
		}
		/** Makes a shallow copy of an AdaBoost classifier.
		 *
		 * @param ref a reference to an AdaBoost classifier.
		 */
		void shallow_copy(AdaBoost& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("AdaBoost ")+T::class_name();
		}
		/** Gets the expected name of the program.
		 * 
		 * @return boost
		 */
		static std::string progname()
		{
			return "boost";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Gets the number of iterations.
		 * 
		 * @return number of iterations.
		 */
		size_type iteration()const
		{
			return iterationInt;
		}
		/** Test if AdaBoost is confidence-rated. 
		 * 
		 * @return true if AdaBoost is confidence-rated.
		 */
		bool confidence_rated()const
		{
			return ( confratedCh == 'y' || (confratedCh == 'a' && committee_type::threshold() == 0.0f) );
		}
		/** Debug the learning algorithm.
		 * 
		 * @param out an output stream.
		 */
		void debug(std::ostream& out)const
		{
			out << "Debug adaboost: " << iterationInt << " " << confratedCh << " " << committee_type::threshold() << std::endl;
		}

	protected:
		/** Builds a model for the AdaBoost classifier using sampling.
		 *
		 * @param learnset the training set.
		 * @param fbeg start of example collection.
		 * @param fend end of example collection.
		 * @param v a dummy flag.
		 * @return an error message or NULL.
		 */
		template<class I>
		const char* train(dataset_type& learnset, I fbeg, I fend, IsVoid<void> v)
		{
			subset_type subset(learnset);
			float_type alpha=1.0f;
			iterator beg = committee_type::begin();
			const char* msg;
			for(iterator end=committee_type::end();beg != end;++beg)
			{
				if((msg=sample(learnset, subset, beg)) != 0) return msg;
				if( subset.empty() ) break;
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				if( !confidence_rated() )
				{
					alpha = errorwgt(fbeg, fend, committee_type::threshold());
					if( testerror(beg, alpha) ) 
					{
						beg->shallow_copy(*this, 1.0f);
						if( beg == committee_type::begin() ) ++beg;
						break;
					}
					alpha = invconf(alpha);
				}
				updtwgts(fbeg, fend, alpha, committee_type::threshold());
				beg->shallow_copy(*this, alpha);
			}
			committee_type::setcount(size_type(std::distance(committee_type::begin(), beg)));
			return 0;
		}
		/** Builds a model for the AdaBoost classifier using weights.
		 *
		 * @param learnset the training set.
		 * @param fbeg start of example collection.
		 * @param fend end of example collection.
		 * @param v a dummy flag.
		 * @return an error message or NULL.
		 */
		template<class I>
		const char* train(dataset_type& learnset, I fbeg, I fend, IsVoid<w_type> v)
		{
			float_type alpha=1.0f;
			iterator beg = committee_type::begin();
			committee_type::normalization(learnset.size());
			const char* msg;
			for(iterator end=committee_type::end();beg != end;++beg)
			{
				if( (msg=committee_type::train(learnset)) != 0 ) return msg;
				if( !confidence_rated() )
				{
					alpha = errorwgt(fbeg, fend, committee_type::threshold());
					if( testerror(beg, alpha) ) 
					{
						beg->shallow_copy(*this, 1.0f);
						if( beg == committee_type::begin() ) ++beg;
						break;
					}
					alpha = invconf(alpha);
				}
				updtwgts(fbeg, fend, alpha, committee_type::threshold());
				beg->shallow_copy(*this, alpha);
			}
			committee_type::setcount(size_type(std::distance(committee_type::begin(), beg)));
			return 0;
		}

	protected:
		/** Tests if the AdaBoost has converged or diverged.
		 *
		 * @param beg current example iterator.
		 * @param alpha the error rate.
		 * @return true if AdaBoost should stop early.
		 */
		bool testerror(iterator beg, float_type alpha)
		{
			if( std::abs(alpha-0.0f) < 1e-5 ) //alpha == 0.0f )
			{
				std::cerr << "AdaBoost converged early: " << alpha << " - " << std::distance(committee_type::begin(),beg) << " < " << committee_type::size() << std::endl;
				//committee_type::setcount(std::distance(committee_type::begin(),beg));
				return true;
			}
			if( std::abs(alpha-0.5f) < 1e-5 )
			{
				std::cerr << "AdaBoost diverged early: " << alpha << " - " << std::distance(committee_type::begin(),beg) << " < " << committee_type::size() << std::endl;
				//committee_type::setcount(std::distance(committee_type::begin(),beg));
				return true;
			}
			return false;
		}
		/** Gets the sign of prediction.
		 *
		 * @param y a class value.
		 * @param t a threshold.
		 * @return sign of the class.
		 */
		inline static int sign(float_type y, float_type t)
		{
			return y>t?1:-1;
		}
		/** Gets the confidence in a prediction.
		 *
		 * @param val an error value.
		 * @return a log of a probability.
		 */
		inline static float_type invconf(float_type val)
		{
			static float_type EPS = 0.00001f;
			val = std::min(std::max(val,EPS), float_type(1)-EPS);
			return 0.5f * std::log( (1.0f - val) / val );
		}
		/** Gets the confidence in a prediction.
		 *
		 * @param val a probability value.
		 * @return a log of a probability.
		 */
		inline static float_type conf(float_type val)
		{
			static float_type EPS = 0.00001f;
			val = std::min(std::max(val,EPS), float_type(1)-EPS);
			return 0.5f * std::log( val / (1.0f - val) );
		}
		/** Initializes weights for a collection of examples to a uniform distribution.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param n number of examples.
		 */
		template<class I>
		static void initwgts(I beg, I end, float_type n)
		{
			ASSERT(n != 0.0f);
			n = 1.0f/n;
			for(;beg != end;++beg) 
			{
				beg->w(n);
				fillwgts(beg);
			}
		}
		/** Normalizes weights for a collection of examples.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param w total weight.
		 */
		template<class I>
		static void normwgts(I beg, I end, float_type w)
		{
			ASSERT(w != 0.0f);
			w = 1.0f/w;
			for(;beg != end;++beg) 
			{
				beg->w_update(w);
				fillwgts(beg);
			}
		}
		/** Updates the weight distribution using predictions.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param alpha a prediction weight.
		 * @param th a threshold.
		 */
		template<class I>
		void updtwgts(I beg, I end, float_type alpha, float_type th)
		{
			I start=beg;
			float_type wsum = 0.0f, pred;
			for(;beg != end;++beg)
			{
				pred = predict(beg);
				if( confidence_rated() ) 
				{
					if( committee_type::threshold() > 0.0f )
						pred = conf(pred);
				}
				else pred = sign(pred, th)*alpha;
				ASSERT(!std::isnan(pred));
				pred = std::exp( -1 * pred * sign(beg->y(), 0) ) * beg->w();
				ASSERT(!std::isnan(pred));
				beg->w(pred);
				wsum += pred;
			}
			normwgts(start, end, wsum);
		}
		/** Calculates weighted error over a set of examples.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param t a threshold.
		 * @return total weighted error.
		 */
		template<class I>
		float_type errorwgt(I beg, I end, float_type t)
		{
			float_type err=0.0f;
			for(;beg != end;++beg)
			{
				if( sign(predict(beg), t) != sign(beg->y(), 0) )
					err+=beg->w();
			}
			return err;
		}
		
	private:
		float_type predict(const_example_iterator it)const
		{
			return committee_type::predict(*it);
		}
		float_type predict(const_bag_iterator it)const
		{
			return predict_bag(*this, it->begin(), it->end(), Int2Type<learner_type::EVAL>());
		}
		float_type predict(const wlearner_type& learner, const_example_iterator it)const
		{
			return learner.predict(*it);
		}
		float_type predict(const wlearner_type& learner, const_bag_iterator it)const
		{
			return predict_bag(learner, it->begin(), it->end(), Int2Type<learner_type::EVAL>());
		}
		template<class I>
		float_type predict_bag(const wlearner_type& learner, I beg, I end)const
		{
			return predict_bag(learner, beg, end, Int2Type<learner_type::EVAL>());
		}
		template<class I>
		float_type predict_bag(const wlearner_type& learner, I beg, I end, Int2Type<0>)const
		{
			float_type t = learner.threshold();
			float_type psum=0.0f, nsum=0.0f, pred;
			size_type ptot=0, ntot=0;
			for(;beg != end;++beg)
			{
				if( (pred=learner.predict(*beg)) > t )
				{
					psum += pred;
					ptot++;
				}
				else
				{
					nsum += pred;
					ntot++;
				}
			}
			return (ptot>0) ? psum/float_type(ptot) : nsum/float_type(ntot);
		}
		template<class I>
		float_type predict_bag(const wlearner_type& learner, I beg, I end, Int2Type<1>)const
		{
			return learner.predict(beg, end);
		}
		const char* sample(dataset_type& learnset, subset_type& subset, iterator beg)
		{
			size_type tot=0;
			do{
				if( milBool )
					bag_wsample(learnset, subset, alphaFlt*learnset.size());
				else
					instance_wsample(learnset, subset, alphaFlt*learnset.size());
				if( tot > 1000 )
				{
					std::cerr << "WARNING: Too few examples for sampling: " << std::distance(committee_type::begin(), beg) << " " << learnset.begin()->w() << " * " << alphaFlt * learnset.size() << " " << learnset.countClass(0) << " " << learnset.countClass(1) << std::endl;
					if(beg != committee_type::begin()) return ERRORMSG("AdaBoost: Too few examples for sampling at iteration " << std::distance(committee_type::begin(), beg));
					subset.clear();
					break;
					//std::exit(1);
				}
				tot++;
			} while( subset.hasZeroClass() );
			return 0;
		}
		static void fillwgts(bag_iterator beg)
		{
			initwgts(beg->begin(), beg->end(), beg->size()/beg->w());
		}
		static void fillwgts(const_example_iterator)
		{
		}
		
	public:
		/** Reads an AdaBoost model from the input stream.
		 *
		 * @param in an input stream.
		 * @param boost a committee.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, AdaBoost& boost)
		{
			char ch;
			in >> boost.confratedCh;
			if( (ch=in.get()) != '\t' ) return boost.fail(in, ERRORMSG("Missing tab character in AdaBoost model: \"" << ch << "\" - " << int(ch)));
			in >> boost.milBool;
			if( in.get() != '\t' ) return boost.fail(in, ERRORMSG("Missing tab character in AdaBoost model"));
			in >> boost.committee();
			boost.iterationInt = boost.size();
			return in;
		}
		/** Writes an AdaBoost model to the output stream.
		 *
		 * @param out an output stream.
		 * @param boost a committee.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const AdaBoost& boost)
		{
			out << boost.confratedCh << "\t" << boost.milBool << "\t";
			out << boost.committee();
			return out;
		}

	private:
		size_type iterationInt;
		char confratedCh;
		bool milBool;
		float alphaFlt;
		argument_type iterationSel;
	};
};


#endif


