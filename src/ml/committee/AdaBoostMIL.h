/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AdaBoostMIL.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ADABOOSTMIL_H
#define _EXEGETE_ADABOOSTMIL_H
#include "mathutil.h"
#include "Committee.h"
#include "WeakLearner.h"
#include "ExampleSetAlgorithms.h"

/** @file AdaBoostMIL.h
 * @brief AdaBoost to MIL committee wrapper
 * 
 * This file contains the AdaBoost to MIL class template.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#define _ADABOOSTMIL_VERSION 101000

namespace exegete
{
	/** @brief AdaBoost to MIL committee wrapper
	 * 
	 * This class template defines an AdaBoost to MIL committee classifier.
	 */
	template<class T>
	class AdaBoostMIL : public Committee< WeakLearner<T> >
	{
		typedef T learner_type;
		typedef typename T::weight_type learner_weight;
		typedef IsVoid<learner_weight> is_void;
		typedef typename SelectType<is_void::value,double,learner_weight>::Result w_type;
		typedef Committee< WeakLearner<T> > committee_type;
		typedef typename committee_type::learner_type wlearner_type;
		typedef typename committee_type::size_type size_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator iterator;
		typedef typename learner_type::dataset_type subset_type;
		typedef typename subset_type::attribute_type attribute_type;
		typedef typename subset_type::class_type class_type;
	public:
		/** Flags AdaBoost as tunable. **/
		enum{ TUNE=1, FORCE_MIL=1, ARGUMENT=T::ARGUMENT+1, FAST_TUNE=1, EVAL=1 };

	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<attribute_type, class_type, w_type>	dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a constant bag iterator **/
		typedef typename dataset_type::const_bag_iterator const_bag_iterator;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef void											weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef int prediction_vector;
	private:
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename dataset_type::iterator					example_iterator;
		typedef typename dataset_type::const_iterator			const_example_iterator;
		typedef typename dataset_type::bag_iterator				bag_iterator;

	public:
		/** Constructs a default AdaBoostMIL classifier.
		 */
		AdaBoostMIL() : iterationInt(0), confratedBool(false), alphaFlt(0.0f),
						iterationSel(iterationInt, "Iteration", true, range_type(1024, 1025, 1, '+'))
		{
			if( !learner_type::argument().empty() )
			{
				iterationSel.add( &learner_type::argument() );
			}
			learner_type::add_test( learner_type::MIL );
		}
		/** Destructs an AdaBoostMIL classifier.
		 */
		~AdaBoostMIL()
		{
		}

	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(AdaBoostMIL));
				if(t>=0)
				arginit(map, iterationSel,  "boostn",  "number of boosting iterations");
				else
				arginit(map, iterationInt,  "boostn",	 "number of boosting iterations");
				arginit(map, confratedBool, "boostc",	 "force confidence-rated boosting?");
				if(is_void::value) 
				arginit(map, alphaFlt,	  	"boostr",  "sample without replacement proportion");
			}
		}

	public:
		/** Constructs a deep copy of the AdaBoostMIL classifier.
		 *
		 * @param boost a source AdaBoostMIL model.
		 */
		AdaBoostMIL(const AdaBoostMIL& boost) : committee_type(boost), iterationInt(boost.iterationInt), confratedBool(boost.confratedBool), alphaFlt(boost.alphaFlt), 
										  		iterationSel(iterationInt, boost.iterationSel)
		{
			if( !learner_type::argument().empty() )
			{
				iterationSel.add( &learner_type::argument() );
			}
		}
		/** Assigns a deep copy of the AdaBoostMIL model.
		 *
		 * @param boost a source AdaBoostMIL model.
		 * @return a reference to this object.
		 */
		AdaBoostMIL& operator=(const AdaBoostMIL& boost)
		{
			committee_type::operator=(boost);
			argument_copy(boost);
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param fbeg start of attribute vectors.
		 * @param fend end of attribute vectors.
		 * @return a real-valued prediction.
		 */
		template<class I>
		float_type predict(I fbeg, I fend)const
		{
			if( committee_type::empty() ) return threshold(true);
			ASSERT(!committee_type::empty());
			if( committee_type::empty() ) return 0.0f;
			float_type sum=0.0f;
			const_iterator beg=committee_type::begin(), end=beg+std::min(iterationInt, committee_type::size());
			if( committee_type::threshold() > 0.0f )
			{
				if( confratedBool )
				{
					for(;beg != end;++beg) 
						sum+=conf(predict_bag(*beg, fbeg, fend, committee_type::threshold()));
				}
				else
				{
					for(;beg != end;++beg) 
						sum+=sign(predict_bag(*beg, fbeg, fend, committee_type::threshold()), committee_type::threshold())*beg->weight();
				}
			}
			else
			{
				for(;beg != end;++beg) 
					sum+=predict_bag(*beg, fbeg, fend, committee_type::threshold());
			}
			return sum;
		}
		
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			if( committee_type::empty() ) return committee_type::threshold();
			ASSERT(!committee_type::empty());
			if( committee_type::empty() ) return 0.0f;
			float_type sum=0.0f;
			const_iterator beg=committee_type::begin(), end=beg+std::min(iterationInt, committee_type::size());
			if( committee_type::threshold() > 0.0f )
			{
				if( confratedBool )
				{
					for(;beg != end;++beg) sum+=conf(beg->predict(pred));
				}
				else
				{
					for(;beg != end;++beg) sum+=sign(beg->predict(pred), committee_type::threshold())*beg->weight();
				}
			}
			else
			{
				for(;beg != end;++beg) sum+=beg->predict(pred);
			}
			return sum;
		}
		/** Builds a model for the AdaBoostMIL classifier.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			if( iterationInt == 0 ) iterationInt = learnset.size()*2;
			committee_type::resize(iterationInt);
			initwgts(learnset.bag_begin(), learnset.bag_end(), float_type(learnset.bagCount()));
			return train(learnset, learnset.bag_begin(), learnset.bag_end(), is_void());
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return AdaBoost.MIL
		 */
		static std::string name()
		{
			return "AdaBoost.MIL";
		}
		/** Get the version of the wrapper.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _ADABOOSTMIL_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return boostmil
		 */
		static std::string prefix()
		{
			return "boostmil";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationSel;
		}
		/** Copies arguments in an AdaBoostMIL model.
		 * 
		 * @param boost a source AdaBoostMIL model.
		 */
		void argument_copy(const AdaBoostMIL& boost)
		{
			iterationInt = boost.iterationInt;
			confratedBool = boost.confratedBool;
			alphaFlt = boost.alphaFlt;
			iterationSel = boost.iterationSel;
		}
		/** Makes a shallow copy of an AdaBoostMIL model.
		 *
		 * @param ref a reference to an AdaBoostMIL model.
		 */
		void shallow_copy(AdaBoostMIL& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
		}
		/** Get the expected name of the program.
		 * 
		 * @return boost
		 */
		static std::string progname()
		{
			return "boost";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("AdaBoostMIL ")+T::class_name();
		}

	protected:
		/** Builds a model for the AdaBoostMIL classifier using sampling.
		 *
		 * @param learnset the training set.
		 * @param bbeg start of bag collection.
		 * @param bend end of bag collection.
		 * @param v a dummy flag.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, bag_iterator bbeg, bag_iterator bend, IsVoid<void> v)
		{
			subset_type subset(learnset);
			float_type alpha=1.0f;
			iterator beg = committee_type::begin();
			unsigned int tot=0;
			const char* msg;
			for(iterator end=committee_type::end();beg != end;++beg)
			{
				tot=0;
				do{
					bag_wsample(learnset, subset, alphaFlt*learnset.size());
					if( tot > 1000 ) return ERRORMSG("AdaBoost: Too few examples for sampling at iteration " << std::distance(committee_type::begin(), beg));
					tot++;
				}while( subset.hasZeroClass() );
				ASSERTMSG(subset.size() > 0, learnset.size());
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				if( committee_type::threshold() != 0.0f && !confratedBool )
				{
					alpha = errorwgt(bbeg, bend, committee_type::threshold());
					if( testerror(beg, alpha) ) 
					{
						beg->shallow_copy(*this, 1.0f);
						if( beg == committee_type::begin() ) ++beg;
						break;
					}
					alpha = invconf(alpha);
				}
				updtwgts(bbeg, bend, alpha, committee_type::threshold());
				beg->shallow_copy(*this, alpha);
			}
			committee_type::setcount(size_type(std::distance(committee_type::begin(), beg)));
			return 0;
		}
		/** Builds a model for the AdaBoostMIL classifier using weights.
		 *
		 * @param learnset the training set.
		 * @param bbeg start of bag collection.
		 * @param bend end of bag collection.
		 * @param v a dummy flag.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, bag_iterator bbeg, bag_iterator bend, IsVoid<w_type> v)
		{
			const char* msg;
			float_type alpha=1.0f;
			iterator beg = committee_type::begin();
			for(iterator end=committee_type::end();beg != end;++beg)
			{
				if( (msg=committee_type::train(learnset)) != 0 ) return msg;
				if( committee_type::threshold() != 0.0f && !confratedBool )
				{
					alpha = errorwgt(bbeg, bend, committee_type::threshold());
					if( testerror(beg, alpha) ) 
					{
						beg->shallow_copy(*this, 1.0f);
						if( beg == committee_type::begin() ) ++beg;
						break;
					}
					alpha = invconf(alpha);
				}
				updtwgts(bbeg, bend, alpha, committee_type::threshold());
				beg->shallow_copy(*this, alpha);
			}
			committee_type::setcount(size_type(std::distance(committee_type::begin(), beg)));
			return 0;
		}

	protected:
		/** Tests if the AdaBoostMIL has converged or diverged.
		 *
		 * @param beg a start example iterator.
		 * @param alpha the error rate.
		 * @return true if AdaBoost should stop early.
		 */
		bool testerror(iterator beg, float_type alpha)
		{
			if( std::abs(alpha-0.0f) < 1e-5 ) //alpha == 0.0f )
			{
				std::cerr << "AdaBoost.MIL converged early" << std::endl;
				//committee_type::setcount(std::distance(committee_type::begin(),beg));
				return true;
			}
			if( std::abs(alpha-0.5f) < 1e-5 )
			{
				std::cerr << "AdaBoost.MIL diverged early" << std::endl;
				//committee_type::setcount(std::distance(committee_type::begin(),beg));
				return true;
			}
			return false;
		}
		/** Gets the sign of prediction.
		 *
		 * @param y a class value.
		 * @param t a threshold.
		 * @return sign of the class.
		 */
		inline static int sign(float_type y, float_type t)
		{
			return y>t?1:-1;
		}
		/** Gets the confidence in a prediction.
		 *
		 * @param val an error value.
		 * @return a log of a probability.
		 */
		inline static float_type invconf(float_type val)
		{
			static float_type EPS = 0.00001f;
			val = std::min(std::max(val,EPS), float_type(1)-EPS);
			return 0.5f * std::log( (1.0f - val) / val );
		}
		/** Gets the confidence in a prediction.
		 *
		 * @param val an error value.
		 * @return a log of a probability.
		 */
		inline static float_type conf(float_type val)
		{
			static float_type EPS = 0.00001f;
			val = std::min(std::max(val,EPS), float_type(1)-EPS);
			return 0.5f * std::log( val / (1.0f - val) );
		}
		/** Initializes weights for a collection of examples to a uniform distribution.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param n number of examples.
		 */
		static void initwgts(bag_iterator beg, bag_iterator end, float_type n)
		{
			ASSERT(n != 0.0f);
			n = 1.0f/n;
			float_type wsum;
			for(;beg != end;++beg) 
			{
				beg->w(n);
				wsum=beg->w()/beg->size();
				setwgts(beg->begin(), beg->end(), wsum);
			}
		}
		/** Initializes weights for a collection of examples to a uniform distribution.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param w weight.
		 */
		static void setwgts(example_iterator beg, example_iterator end, float_type w)
		{
			for(;beg != end;++beg) beg->w(w);
		}
		/** Normalizes weights for a collection of examples.
		 *
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param w total weight.
		 */
		static void normwgts(bag_iterator beg, bag_iterator end, float_type w)
		{
			ASSERT(w != 0.0f);
			w = 1.0f/w;
			float_type wsum;
			for(;beg != end;++beg) 
			{
				beg->w_update(w);
				wsum=beg->w()/beg->size();
				setwgts(beg->begin(), beg->end(), wsum);
			}
		}
		/** Predict the label on a bag.
		 *
		 * @param learner a classifier.
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param t a threshold.
		 * @return a bag-level prediction.
		 */
		template<class I>
		static float_type predict_bag(const wlearner_type& learner, I beg, I end, float_type t)
		{
			return predict_bag(learner, beg, end, t, Int2Type<learner_type::FORCE_MIL>());
		}
		/** Predict the label on a bag.
		 *
		 * @param learner a classifier.
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param t a threshold.
		 * @param dummy compile-time method selection.
		 * @return a bag-level prediction.
		 */
		template<class I>
		inline static float_type predict_bag(const wlearner_type& learner, I beg, I end, float_type t, Int2Type<0> dummy)
		{
			float_type psum=0.0f, nsum=0.0f, pred;
			size_type ptot=0, ntot=0;
			for(;beg != end;++beg)
			{
				if( (pred=learner.predict(*beg)) > t )
				{
					psum += pred;
					ptot++;
				}
				else
				{
					nsum += pred;
					ntot++;
				}
			}
			return (ptot>0) ? psum/float_type(ptot) : nsum/float_type(ntot);
		}
		/** Predict the label on a bag.
		 *
		 * @param learner a classifier.
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param t a threshold.
		 * @param dummy compile-time method selection.
		 * @return a bag-level prediction.
		 */
		template<class I>
		inline static float_type predict_bag(const wlearner_type& learner, I beg, I end, float_type t, Int2Type<1> dummy)
		{
			return learner.predict(beg, end);
		}
		/** Calculates weighted error over a set of bags.
		 *
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param t a threshold.
		 * @return the weighted error.
		 */
		float_type errorwgt(bag_iterator beg, bag_iterator end, float_type t)
		{
			float_type err=0.0f;
			for(;beg != end;++beg)
			{
				if( (predict_bag(*this, beg->begin(), beg->end(), t) > t) != (beg->y() > 0) )
					err += beg->w();
			}
			return err;
		}
		/** Updates the weight distribution using predictions.
		 *
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param alpha a prediction weight.
		 * @param th a threshold.
		 */
		void updtwgts(bag_iterator beg, bag_iterator end, float_type alpha, float_type th)
		{
			bag_iterator start=beg;
			float_type wsum = 0.0f, pred;
			for(;beg != end;++beg)
			{
				pred = predict_bag(*this, beg->begin(), beg->end(), th);//committee_type::predict(*beg);
				if( th != 0.0f )
				{
					if( confratedBool ) pred = conf(pred);
					else pred = sign(pred, th)*alpha;
				}
				ASSERT(!std::isnan(pred));
				pred = std::exp( -1 * pred * sign(beg->y(), 0) ) * beg->w();
				ASSERT(!std::isnan(pred));
				beg->w(pred);
				wsum += pred;
			}
			normwgts(start, end, wsum);
		}
		
	private:
		size_type iterationInt;
		bool confratedBool;
		float alphaFlt;
		argument_type iterationSel;
	};
};


#endif


