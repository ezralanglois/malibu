/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Bagging.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_BAGGING_H
#define _EXEGETE_BAGGING_H
#include "Committee.h"
#include "prediction.hpp"
#include "ExampleSetAlgorithms.h"
/** Defines the version of the Bagging algorithm. 
 * @todo add to group
 */
#define _BAGGING_VERSION 101000

/** @file Bagging.h
 * @brief Bagging committee wrapper.
 * 
 * This file contains the Bagging committee wrapper.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Bagging committee wrapper.
	 * 
	 * This class template defines a Bagging committee wrapper.
	 */
	template<class T>
	class Bagging : public Committee< T >
	{
		typedef T learner_type;
		typedef Committee< T > committee_type;
		typedef typename committee_type::size_type size_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator iterator;
		typedef typename learner_type::dataset_type subset_type;
		typedef typename subset_type::attribute_type attribute_type;
		typedef typename subset_type::class_type class_type;
	public:
		/** Committee Flags:
		 * 	- Bagging as tunable.
		 * 	- Increment argument.
		 */
		enum{ TUNE=1, ARGUMENT=T::ARGUMENT+1 };

	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines an example set as a learnset type. **/
		typedef subset_type										dataset_type;
		/** Defines a learnset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a svm attribute value as a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines the second template parameter as a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef typename learner_type::weight_type				weight_type;
	private:
		typedef prediction< typename T::prediction_type, class_type> pred_value;
	public:
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef std::vector< pred_value > prediction_vector;
	private:
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename dataset_type::iterator					example_iterator;
		typedef std::vector<size_type>							size_vector;
		typedef typename size_vector::iterator					size_iterator;

	public:
		/** Constructs a default Bagging classifier.
		 */
		Bagging() : iterationInt(0), alphaFlt(0.0f),
					iterationSel(iterationInt, "Iteration", range_type(100, 1000, 1000, '%'))
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Destructs an Bagging classifier.
		 */
		~Bagging()
		{
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(Bagging));
				if(t>=0)
				arginit(map, iterationSel,	"bagn", "number of bagging iterations");
				else
				arginit(map, iterationInt,	"bagn",		"number of bagging iterations");
				arginit(map, alphaFlt,		"bagr",		"sample without replacement proportion");
			}
		}
		
	public:
		/** Get parameter values for the learning algorithm.
		 * 
		 * @param vec a vector of floats.
		 */
		void getparam(std::vector<float>& vec)const
		{
			init(vec);
			learner_type::getparam(vec);
		}
		/** Set parameter values for the learning algorithm.
		 * 
		 * @param it a constant vector of floats.
		 */
		void setparam(std::vector<float>::const_iterator& it)
		{
			init(it);
			learner_type::setparam(it);
		}

	public:
		/** Constructs a deep copy of the Bagging model.
		 *
		 * @param bag a source Bagging model.
		 */
		Bagging(const Bagging& bag) : committee_type(bag), iterationInt(bag.iterationInt), alphaFlt(bag.alphaFlt),
									  iterationSel(iterationInt, bag.iterationSel)
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Assigns a deep copy of the Bagging model.
		 *
		 * @param bag a source Bagging model.
		 * @return a reference to this object.
		 */
		Bagging& operator=(const Bagging& bag)
		{
			committee_type::operator=(bag);
			argument_copy(bag);
			return *this;
		}
		
	public:
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			float_type sum = 0.0;
			if( committee_type::empty() ) return committee_type::threshold();
			for(const_iterator it=committee_type::begin(), end=committee_type::end();it != end;++it) sum+=it->predict(pred);
			return sum / float_type(committee_type::size());
		}
		/** Builds a model for the Bagging classifier.
		 *
		 * @todo fix weight problem.
		 * 
		 * @param learnset the training set.
		 * @param pred an array of predictions.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, prediction_vector& pred)
		{
			size_type tot;
			committee_type::resize(iterationInt);
			subset_type trainset(learnset);
			size_vector testset;
			size_iterator pbeg, pend;
			ASSERT(!learnset.empty());
			const char* msg;
			for(iterator beg = committee_type::begin(), end=committee_type::end();beg != end;++beg)
			{
				tot=0;
				do{
					instance_sample(learnset, trainset, testset, alphaFlt);
					if( tot > 1000 ) return ERRORMSG("Bagging: Too few examples for sampling at iteration " << std::distance(committee_type::begin(), beg));
					tot++;
				}while( trainset.hasZeroClass() );
				ASSERT(!trainset.empty());
				if( (msg=committee_type::train(trainset)) != 0 ) return msg;
				for(pbeg = testset.begin(), pend=testset.end();pbeg != pend;++pbeg)
				{
					tot = *pbeg;
					pred[tot].add_p(committee_type::predict(learnset[tot]));
					pred[tot].add_y(1);
				}
				beg->shallow_copy(*this);
			}
			for(tot=0;tot<learnset.size();++tot)
			{
				 pred[tot].p( pred[tot].p() / float_type(pred[tot].y()) );
				 pred[tot].y(learnset[tot].y());
//----------------------------------------------------------- 
				// pred[tot].w( learnset[tot].w() );
//----------------------------------------------------------- 
			}
			return 0;
		}
		/** Builds a model for the Bagging classifier.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			unsigned int tot;
			committee_type::resize(iterationInt);
			subset_type trainset(learnset);
			const char* msg;
			ASSERT(!learnset.empty());
			for(iterator beg = committee_type::begin(), end=committee_type::end();beg != end;++beg)
			{
				tot=0;
				do{
					instance_sample(learnset, trainset, alphaFlt);
					if( tot > 1000 ) return ERRORMSG("Bagging: Too few examples for sampling at iteration " << std::distance(committee_type::begin(), beg));
					tot++;
				}while( trainset.hasZeroClass() );
				ASSERTMSG(!trainset.empty(), "zero: " << trainset.hasZeroClass() << " " << trainset.countClass(0) << ", " << trainset.countClass(1) << " - " << trainset.classCount() );
				if( (msg=committee_type::train(trainset)) != 0 ) return msg;
				beg->shallow_copy(*this);
			}
			return 0;
		}
		/** Gets the name of the self-validation type.
		 *
		 * @return Out-of-bag
		 */
		static std::string validationType()
		{
			return "Out-of-bag";
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("Bagging ")+T::class_name();
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Bagging
		 */
		static std::string name()
		{
			return "Bagging";
		}
		/** Gets the version of the wrapper.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _BAGGING_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return bag
		 */
		static std::string prefix()
		{
			return "bag";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationSel;
		}
		/** Copies arguments in the wrapper.
		 * 
		 * @param bag a Bagging model.
		 */
		void argument_copy(const Bagging& bag)
		{
			iterationInt = bag.iterationInt;
			alphaFlt = bag.alphaFlt;
			iterationSel = bag.iterationSel;
		}
		/** Makes a shallow copy of an Bagging model.
		 *
		 * @param ref a Bagging model.
		 */
		void shallow_copy(Bagging& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
		}
		/** Get the expected name of the program.
		 * 
		 * @return bag
		 */
		static std::string progname()
		{
			return "bag";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}

	private:
		size_type iterationInt;
		float alphaFlt;
		argument_type iterationSel;
	};
};

/** @brief Type utility helper specialized for Bagging
 * 
 * This class defines a type utility helper specialized for Bagging.
 */
template<class T>
struct TypeTrait < ::exegete::Bagging<T> >
{
	/** Defines a value type. **/
	typedef ::exegete::Bagging<T> value_type;
};

#endif


