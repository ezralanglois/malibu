/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Committee.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_COMMITTEE_H
#define _EXEGETE_COMMITTEE_H
#include "Learner.h"

/** @file Committee.h
 * 
 * @brief Base committee of learners
 * 
 * This file contains a base committee of learners
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Base committee of learners
	 * 
	 * This class defines a base committee of learners.
	 */
	template<class T>
	class Committee : public T
	{
	public:
		/** Defines a learner type. **/
		typedef T learner_type;
		/** Defines a reference to a learner type. **/
		typedef T& reference;
		/** Defines a constant reference to a learner type. **/
		typedef const T& const_reference;
		/** Defines a pointer to a learner. **/
		typedef T* pointer;
		/** Defines an iterator to a learner. **/
		typedef T* iterator;
		/** Defines a constant pointer to a learner. **/
		typedef const T* const_pointer;
		/** Defines a constant iterator to a learner. **/
		typedef const T* const_iterator;
		/** Defines a size type. **/
		typedef size_t size_type;
		/** Defines a float type. */
		typedef typename T::float_type float_type;
		/** Defines a flag for the algorithm. **/
		enum{ GRAPHABLE=T::GRAPHABLE };

	public:
		/** Constructs a committee of learning algorithms.
		 */
		Committee() : learners(0), count(0)
		{
		}
		/** Destructs a committee.
		 */
		~Committee()
		{
			delete[] learners;
		}

	public:
		/** Constructs a copy of a committee of learning algorithms.
		 *
		 * @param com a source committee.
		 */
		Committee(const Committee& com) : learner_type(com), learners(0), count(com.count)
		{
			if(count > 0)
			{
				learners = ::setsize<learner_type>(count);
				std::copy(com.learners, com.learners+com.count, learners);
			}
		}
		/** Assigns a committee of learning algorithms.
		 *
		 * @param com a committee.
		 * @return a reference to this object.
		 */
		Committee& operator=(const Committee& com)
		{
			learner_type::operator=(com);
			::erase(learners);
			count = com.count;
			if(count>0)
			{
				learners = ::setsize<learner_type>(count);
				std::copy(com.learners, com.learners+com.count, learners);
			}
			return *this;
		}

	public:
		/** Gets a learner at specified index.
		 * 
		 * @param n an index.
		 * @return learner at index.
		 */
		learner_type& learnerAt(size_type n)
		{
			ASSERT(learners!=0);
			ASSERT(n<count);
			return learners[n];
		}
		/** Gets a learner at a specified index.
		 * 
		 * @param n an index.
		 * @return learner at index.
		 */
		const learner_type& learnerAt(size_type n)const
		{
			ASSERT(learners!=0);
			ASSERT(n<count);
			return learners[n];
		}
		/** Tests if the model is empty.
		 *
		 * @return true if model is empty.
		 */
		bool empty()const
		{
			return count == 0;
		}
		/** Clear the committee.
		 */
		void clear()
		{
			learner_type::clear();
			::erase(learners);
			count = 0;
		}
		/** Set the size of the committee.
		 *
		 * @param n the new size.
		 */
		void resize(size_type n)
		{
			::erase(learners);
			count = n;
			learners = ::setsize<learner_type>(count);
		}
		/** Gets an iterator to start of a learner collection.
		 *
		 * @return start of a learner collection.
		 */
		iterator begin()
		{
			return learners;
		}
		/** Gets an iterator to start of a learner collection.
		 *
		 * @return start of a learner collection.
		 */
		const_iterator begin()const
		{
			return learners;
		}
		/** Gets an iterator to end of a learner collection.
		 *
		 * @return end of a learner collection.
		 */
		iterator end()
		{
			return learners+count;
		}
		/** Gets an iterator to end of a learner collection.
		 *
		 * @return end of a learner collection.
		 */
		const_iterator end()const
		{
			return learners+count;
		}
		/** Sets the number of learners.
		 *
		 * @param n number of learners.
		 */
		void setcount(size_type n)
		{
			ASSERTMSG(n<=count, n << " <= " << count);
			count = n;
		}
		/** Makes a shallow copy of a committee.
		 *
		 * @param ref a reference to a committee.
		 */
		void shallow_copy(Committee& ref)
		{
			learners = ref.learners; ref.learners = 0;
			count = ref.count; ref.count = 0;
			T::shallow_copy(ref);
		}
		/** Gets the numbers of learners.
		 *
		 * @return number of learners.
		 */
		size_type size()const
		{
			return count;
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			if( size() == 0 ) learner_type::accept(visitor);
			for(const_iterator b=begin(), e=end();b != e;++b) b->accept(visitor);
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("Committee ")+T::class_name();
		}
		
#ifdef _MPI
		/** Add a shallow copy of the parent to the committee at the index.
		 * 
		 * @param n index in committeee.
		 */
		void mpi_add_child(size_type n)
		{
			learnerAt(n).shallow_copy(*this);
		}
		/** Add a shallow copy of the parent to the committee at the index.
		 * 
		 * @param n index in committeee.
		 * @param s weight on learner.
		 */
		void mpi_add_child(size_type n, float_type s)
		{
			learnerAt(n).shallow_copy(*this, s);
		}
#endif
		
	protected:
		/** Get reference to this committee object.
		 * 
		 * @return reference to this object.
		 */
		reference parent()
		{
			return *this;
		}
		/** Get reference to this committee object.
		 * 
		 * @return reference to this object.
		 */
		Committee<T>& committee()
		{
			return *this;
		}
		/** Get reference to this committee object.
		 * 
		 * @return reference to this object.
		 */
		const Committee<T>& committee()const
		{
			return *this;
		}

	public:
		/** Reads a committee from the input stream.
		 *
		 * @param in an input stream.
		 * @param com a committee.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, Committee& com)
		{
			in >> com.count;
			if( com.count == 0 )
			{
				com.errormsg(in, "Read in zero committee members");
				return in;
			}
			if( in.get() != '\n' )
			{
				com.errormsg(in, "Missing newline character in committee model");
				return in;
			}
			com.resize(com.count);
			for(iterator it=com.begin(), end=com.end();it != end;++it) 
			{
				if( (in >> *it).fail() ) return in;
				if( !in.eof() && in.peek() == '\n' ) in.get();
			}
			return in;
		}
		/** Writes a committee to the output stream.
		 *
		 * @param out an output stream.
		 * @param com a committee.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const Committee& com)
		{
			out << com.count << "\n";
			for(const_iterator it=com.begin(), end=com.end();it != end;++it) 
			{
				out << *it << "\n";
				if( out.fail() ) 
				{
					com.errormsg(out, "Failed writting Committee");
					return out;
				}
			}
			return out;
		}

	private:
		pointer learners;
		size_type count;
	};

};


#endif



