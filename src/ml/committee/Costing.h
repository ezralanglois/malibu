/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Costing.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_COSTING_H
#define _EXEGETE_COSTING_H
#include "Committee.h"
#include "prediction.hpp"
#include "ExampleSetAlgorithms.h"
/** Defines the version of the Costing algorithm. 
 * @todo add to group
 */
#define _COSTING_VERSION 101000

/** @file Costing.h
 * @brief Costing committee wrapper
 * 
 * This file contains the Costing class template.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Costing committee wrapper
	 * 
	 * This class template defines an Costing committee classifier.
	 *
	 * @todo consider weighted error
	 */
	template<class T>
	class Costing : public Committee< T >
	{
		typedef T learner_type;
		typedef Committee< T > committee_type;
		typedef typename committee_type::size_type size_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator iterator;
		typedef typename learner_type::dataset_type subset_type;
		typedef typename subset_type::attribute_type attribute_type;
		typedef typename subset_type::class_type class_type;
	public:
		/** Flags Costing as tunable. **/
		enum{ TUNE=1, ARGUMENT=T::ARGUMENT+1 };

	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<attribute_type, class_type, double>	dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef double											weight_type;
	private:
		typedef prediction< typename T::prediction_type, class_type> pred_value;
	public:
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef std::vector< pred_value > prediction_vector;
	private:
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename dataset_type::iterator					example_iterator;
		typedef std::vector<size_type>							size_vector;
		typedef typename size_vector::iterator					size_iterator;

	public:
		/** Constructs a default Costing classifier.
		 */
		Costing() : iterationInt(0), alphaFlt(0.0f), wnorm(1.0f),
					iterationSel(iterationInt, "Iteration", range_type(10, 11, 1, '%'))
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Destructs an Costing classifier.
		 */
		~Costing()
		{
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(Costing));
				if(t>=0)
				arginit(map, iterationSel,	"costn", "number of costing iterations");
				else
				arginit(map, iterationInt,	"costn", "number of costing iterations");
				arginit(map, alphaFlt,	    "costr", "sample without replacement proportion");
			}
		}

	public:
		/** Constructs a deep copy of the costing model.
		 *
		 * @param cost a source Costing model.
		 */
		Costing(const Costing& cost) : committee_type(cost), iterationInt(cost.iterationInt), alphaFlt(cost.alphaFlt),wnorm(cost.wnorm),
										  iterationSel(iterationInt, cost.iterationSel)
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Assigns a deep copy of the costing model.
		 *
		 * @param cost a source Costing model.
		 * @return a reference to this object.
		 */
		Costing& operator=(const Costing& cost)
		{
			committee_type::operator=(cost);
			argument_copy(cost);
			return *this;
		}
		
	public:
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			float_type sum = 0.0;
			if( committee_type::empty() ) return committee_type::threshold();
			for(const_iterator it=committee_type::begin(), end=committee_type::end();it != end;++it) sum+=it->predict(pred);
			return sum / float_type(committee_type::size());
		}
		/** Builds a model for the Costing classifier.
		 *
		 * @param learnset the training set.
		 * @param pred an array of predictions.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, prediction_vector& pred)
		{
			const char* msg;
			size_type tot;
			committee_type::resize(iterationInt);
			subset_type trainset(learnset);
			size_vector testset;
			size_iterator pbeg, pend;
			for(iterator beg = committee_type::begin(), end=committee_type::end();beg != end;++beg)
			{
				tot=0;
				do{
					/*if( learnset.bagCount() > 0 ) bag_wsample(learnset, trainset, testset, alphaFlt*wnorm);
					else */
					instance_wsample(learnset, trainset, testset, alphaFlt*wnorm);
					if( tot > 1000 ) return ERRORMSG("Costing: Too few examples for sampling at iteration " << std::distance(committee_type::begin(), beg) << " first weight:" << learnset.begin()->w() );
					tot++;
				}while( trainset.hasZeroClass() );
				if( (msg=committee_type::train(trainset)) != 0 ) return msg;
				for(pbeg = testset.begin(), pend=testset.end();pbeg != pend;++pbeg)
				{
					tot = *pbeg;
					pred[tot].add_p(committee_type::predict(learnset[tot]));
					pred[tot].add_y(1);
				}
				beg->shallow_copy(*this);
			}
			for(tot=0;tot<learnset.size();++tot)
			{
				 pred[tot].p( pred[tot].p() / float_type(pred[tot].y()) );
				 pred[tot].y(learnset[tot].y());
				 pred[tot].w( learnset[tot].w() );
			}
			return 0;
		}
		/** Builds a model for the Costing classifier.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			const char* msg;
			unsigned int tot;
			committee_type::resize(iterationInt);
			subset_type trainset(learnset);
			for(iterator beg = committee_type::begin(), end=committee_type::end();beg != end;++beg)
			{
				tot=0;
				do{
					/*if( learnset.bagCount() > 0 ) bag_wsample(learnset, trainset, alphaFlt*wnorm);
					else*/ 
					instance_wsample(learnset, trainset, alphaFlt*wnorm);
					if( tot > 1000 ) return ERRORMSG("Costing: Too few examples for sampling at iteration " << std::distance(committee_type::begin(), beg) << " first weight:" << learnset.begin()->w() << " 0:" << trainset.countClass(0) << ", " << learnset.countClass(0) << " 1:" << trainset.countClass(1) << ", " << learnset.countClass(1));
					tot++;
				}while( trainset.hasZeroClass() );
				if( (msg=committee_type::train(trainset)) != 0 ) return msg;
				beg->shallow_copy(*this);
			}
			return 0;
		}
		/** Gets the name of the self-validation type.
		 *
		 * @return Out-of-bag
		 */
		static std::string validationType()
		{
			return "Out-of-bag";
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("Costing ")+T::class_name();
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return a name of the learning algorithm.
		 */
		static std::string name()
		{
			return "Costing";
		}
		/** Gets the version of the wrapper.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _COSTING_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return cost
		 */
		static std::string prefix()
		{
			return "cost";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationSel;
		}
		/** Copies arguments in a costing model.
		 * 
		 * @param cost a costing model.
		 */
		void argument_copy(const Costing& cost)
		{
			iterationInt = cost.iterationInt;
			alphaFlt = cost.alphaFlt;
			iterationSel = cost.iterationSel;
			wnorm = cost.wnorm;
		}
		/** Makes a shallow copy of an Costing classifier.
		 *
		 * @param ref a reference to a Costing model.
		 */
		void shallow_copy(Costing& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
		}
		/** Get the expected name of the program.
		 * 
		 * @return cost
		 */
		static std::string progname()
		{
			return "cost";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Normalizes the weighted sample without replacement.
		 * 
		 * @param w a new weight factor.
		 */
		void normalization(float w)
		{
			wnorm = w;
		}

	private:
		size_type iterationInt;
		float alphaFlt;
		float wnorm;
		argument_type iterationSel;
	};
};


/** @brief Type utility interface to a costing algorithm
 * 
 * This class defines a type utility interface to a costing algorithm.
 */
template<class T>
struct TypeUtil< ::exegete::Costing<T> >
{
	/** Flags class as non-primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::Costing<T> value_type;
	/** Tests if a string can be converted to a costing algorithm.
	 *
	 * @param str a string to test.
	 * @return true
	*/
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return Costing
	*/
	static const char* name() 
	{
		return "Costing";
	}
};

#endif


