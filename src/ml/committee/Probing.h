/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Probing.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_PROBING_H
#define _EXEGETE_PROBING_H
#include "WeakLearner.h"
#include "Committee.h"
#include "ExampleSetAlgorithms.h"
#include <cmath>
#include <list>
#include <iomanip>
/** Defines the version of the Probing algorithm. 
 * @todo add to group
 */
#define _PROBING_VERSION 101000

/** @file Probing.h
 * @brief Probing committee wrapper.
 * 
 * This file contains classes for the probing committee wrapper.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Probing committee wrapper.
	 * 
	 * This class template defines the Probing machine learning reduction that transforms
	 * an important weighted classifier into a classifier that gives accurate class membership
	 * probability estimates.
	 * 
	 * @todo Add AdaBoost fast tuning
	 */
	template<class T, class L>
	class Probing : public Committee< WeakLearner<T> >//std::vector< std::pair<T, typename T::float_type> >
	{
		typedef L loss_type;
 		typedef T learner_type;
		typedef typename T::weight_type learner_weight;
		typedef Committee< WeakLearner<T> > committee_type;
		typedef typename committee_type::learner_type wlearner_type;
		typedef typename committee_type::size_type size_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator iterator;
		typedef typename learner_type::dataset_type subset_type;
		typedef typename subset_type::attribute_type attribute_type;
		typedef typename subset_type::class_type class_type;
		typedef typename subset_type::const_iterator const_example_iterator;
		typedef typename subset_type::iterator example_iterator;
		typedef std::list< wlearner_type > committee_list;
		typedef typename committee_list::iterator list_iterator;
		typedef typename committee_list::const_iterator	const_list_iterator;
		typedef std::vector<unsigned int> count_vector;
		typedef typename count_vector::iterator count_iterator;
	public:
		/** Flags Probing as tunable. **/
		enum{ TUNE=1, ARGUMENT=T::ARGUMENT+1 };

	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines an example set as a learnset type. **/
		typedef ExampleSet<attribute_type, class_type, learner_weight>	dataset_type;
		/** Defines a learnset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a svm attribute value as a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines the second template parameter as a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef void											weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef int prediction_vector;
	private:
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;

	public:
		/** Constructs a Probing object.
		 */
		Probing() : currthresh(0.5f), iterationInt(10),
					iterationSel(iterationInt, "Iteration", false, range_type(10, 11, 1, '+'))
		{
			iterationSel.add( &learner_type::argument() );
			learner_type::add_test( learner_type::BINARY );
			learner_type::add_test( learner_type::NOMIL );
		}
		/** Destructs a Probing object.
		 */
		~Probing()
		{
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(Probing));
				if(t>=0)
				arginit(map, iterationSel, "probingn", "number of probing iterations");
				else
				arginit(map, iterationInt, "probingn", "number of probing iterations");
			}
		}

	public:
		/** Constructs a copy of a Probing object.
		 *
		 * @param probing a Probing object.
		 */
		Probing(const Probing& probing) : committee_type(probing), currthresh(probing.currthresh), iterationInt(probing.iterationInt), lastprob(probing.lastprob),
			iterationSel(iterationInt, probing.iterationSel)
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Assigns a copy of a Probing object.
		 *
		 * @param probing a Probing object.
		 * @return a reference to this object.
		 */
		Probing& operator=(const Probing& probing)
		{
			committee_type::operator=(probing);
			argument_copy(probing);
			lastprob = probing.lastprob;
			currthresh = probing.currthresh;
			return *this;
		}

	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return currthresh;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const;
		/** Trains the probing algorithms.
		 *
		 * @todo add estimate of out-of-bag-error
		 * 
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset);

	public:
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Copies of arguments in a probing model.
		 * 
		 * @param probing a probing committee.
		 */
		void argument_copy(const Probing& probing)
		{
			iterationInt = probing.iterationInt;
			iterationSel = probing.iterationSel;
		}
		/** Makes a shallow copy of the source Probing object.
		 *
		 * @param probing the source object.
		 */
		void shallow_copy(Probing& probing)
		{
			committee_type::shallow_copy(probing);
			argument_copy(probing);
			lastprob=probing.lastprob;
			currthresh=probing.currthresh;
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("Probing ")+T::class_name();
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Probing
		 */
		static std::string name()
		{
			return "Probing";
		}
		/** Get the version of the Probing algorithm.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _PROBING_VERSION;
		}
		/** Gets the expected name of the program.
		 * 
		 * @return probing
		 */
		static std::string progname()
		{
			return "probing";
		}
		/** Gets the number of iterations.
		 * 
		 * @return number of iterations.
		 */
		size_type iteration()const
		{
			return iterationInt;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return probe
		 */
		static std::string prefix()
		{
			return "probe";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationSel;
		}
		/** Reads a probing committee from the input stream.
		 *
		 * @param in an input stream.
		 * @param probing a committee.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, Probing& probing)
		{
			in >> probing.currthresh;
			if( in.get() != '\t' )
			{
				probing.errormsg(in, "Missing newline character in Probing model");
				return in;
			}
			in >> probing.lastprob;
			if( in.get() != '\t' )
			{
				probing.errormsg(in, "Missing newline character in Probing model");
				return in;
			}
			in >> probing.committee();
			probing.iterationInt = probing.size();
			return in;
		}
		/** Writes an probing committee to the output stream.
		 *
		 * @param out an output stream.
		 * @param probing a reference to a committee.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const Probing& probing)
		{
			out << probing.currthresh << "\t";
			out << probing.lastprob << "\t";
			out << probing.committee();
			return out;
		}

	protected:
		/** Predicts the index for the attribute vector.
		 * 
		 * @param pred an attribute vector.
		 * @return predicted index.
		 */
		unsigned int predictn(const_attribute_pointer pred)const;

	private:
		void reweight_positive(example_iterator beg, example_iterator end, float_type p);
		void count(const_example_iterator beg, const_example_iterator end, count_iterator cnt, count_vector& wgt);
		list_iterator minmax(list_iterator beg, list_iterator end, float_type& p, count_iterator wbeg);
		void initprob(iterator beg, iterator end);

	private:
		float currthresh;
		float_type lastprob;
		unsigned int iterationInt;
		argument_type iterationSel;
	};

	template<class T, class L>
	const char* Probing<T,L>::train(dataset_type& learnset)
	{
		float_type p = loss_type::probability(loss_type::lo(), loss_type::hi());
		committee_list committee;
		list_iterator lsav=committee.begin(), lcurr;
		example_iterator fbeg = learnset.begin();
		example_iterator fend = learnset.end();
		count_vector counts(learnset.size(), 0);
		count_vector weight(iterationInt+2, 0);
		const char* msg;
		
		//reweight_positive(fbeg, fend, p / (1 - p));
		reweight_positive(fbeg, fend, p);
		committee_type::train(learnset);
		for(unsigned int i=1;i<iterationInt;++i)
		{
			count(fbeg, fend, counts.begin(), weight);
			lcurr = committee.insert(lsav, wlearner_type());
			lcurr->shallow_copy(*this, p);
			ASSERT((committee.size()+1) <= weight.size());
			lsav = minmax(committee.begin(), committee.end(), p, weight.begin());
			//reweight_positive(fbeg, fend, p / (1 - p));
			reweight_positive(fbeg, fend, p);
			if( (msg=committee_type::train(learnset)) != 0 ) return msg;
		}
		lcurr = committee.insert(lsav, wlearner_type());
		lcurr->shallow_copy(*this, p);
		ASSERT( committee.size() == iterationInt );
		committee_type::resize(iterationInt);
		iterator nbeg=committee_type::begin();
		for(lcurr=committee.begin(), lsav=committee.end();lcurr != lsav;++lcurr,++nbeg) 
		{
			nbeg->shallow_copy(*lcurr, lcurr->weight());
		}
		initprob(committee_type::begin(), committee_type::end());
		//currthresh
		//learner_type::threshold(threshold_calibration(predictions.begin(), predictions.end(), thresholdMetric));
		return 0;
	}

	template<class T, class L>
	typename Probing<T,L>::float_type Probing<T,L>::predict(const_attribute_pointer pred)const
	{
		if( committee_type::empty() ) return threshold();
		unsigned int n=predictn(pred);
		return( n < committee_type::size() ) ? committee_type::learnerAt(n).weight() : lastprob;
	}
	
	template<class T, class L>
	unsigned int Probing<T,L>::predictn(const_attribute_pointer pred)const
	{
		unsigned int n=0;
		for(const_iterator curr=committee_type::begin(), end=committee_type::end();curr != end;++curr)
			if( curr->predict(pred) > committee_type::threshold() ) n++;
		return n;
	}

	template<class T, class L>
	void Probing<T,L>::reweight_positive(example_iterator beg, example_iterator end, float_type p)
	{
		//float_type n = (p>1) ? 1.0f/p : 1.0f;
		//p = ((p>1.0) ? 1.0 : p);
		float_type n = 1.0;
		/*if( p < 0.5 )
		{
			n = p/(1-p);
			p = 1.0;
		}
		else*/ p = (1-p)/p;
		for(;beg != end;++beg) 
		{
			if( beg->y() > 0 ) beg->w(p);
			else beg->w(n);
		}
	}

	template<class T, class L>
	void Probing<T,L>::count(const_example_iterator beg, const_example_iterator end, count_iterator cnt, count_vector& wgt)
	{
		std::fill(wgt.begin(), wgt.end(), 0);
		for(;beg != end;++beg, ++cnt)
		{
			if( committee_type::predict(*beg) > committee_type::threshold() ) ++(*cnt);
			ASSERT((*cnt) < wgt.size());
			wgt[*cnt]++;
		}
	}

	template<class T, class L>
	typename Probing<T,L>::list_iterator Probing<T,L>::minmax(list_iterator beg, list_iterator end, float_type& p, count_iterator wbeg)
	{
		list_iterator lsav=beg;
		float_type a = loss_type::lo(), b, a1, b1;
		float_type loss, least = -TypeUtil<float_type>::max();
		for(;beg != end;++beg,++wbeg)
		{
			b = beg->weight();
			loss = loss_type::loss(a, b, *wbeg);
			if( loss > least )
			{
				least = loss;
				a1 = a;
				b1 = b;
				lsav = beg;
			}
			a = b;
		}
		b = loss_type::hi();
		loss = loss_type::loss(a, b, *wbeg);
		if( loss > least )
		{
			a1 = a;
			b1 = b;
			lsav = beg;
		}
		p = loss_type::probability(a1, b1);
		return lsav;
	}
	template<class T, class L>
	void Probing<T,L>::initprob(iterator beg, iterator end)
	{
		float_type a=loss_type::lo(), b;
		for(;beg != end;++beg)
		{
			b = beg->weight();
			beg->weight(loss_type::probability(a,b));
			a = b;
		}
		lastprob = loss_type::probability(a,float_type(loss_type::hi()));
	}

	/** @brief Mean squared error loss
	 * 
	 * This class defines the mean squared loss function for the Probing algorithm.
	 */
	struct MSELoss
	{
		/** Estimates the probability from a probability range.
		 *
		 * @param a start of probability range.
		 * @param b end of probability range.
		 * @return estimated probability.
		 */
		template<class F>
		static F probability(F a, F b)
		{
			return (a+b)/2.0f;
		}
		/** Estimates the loss over a probability range.
		 *
		 * @param a start of probability range.
		 * @param b end of probability range.
		 * @param n number of examples falling in this range.
		 * @return estimated probability.
		 */
		template<class F>
		static F loss(F a, F b, unsigned int n)
		{
			return F(n)*(b-a);
		}
		/** Gets the maximum value in the probability range.
		 *
		 * @return max probability value.
		 */
		static float hi()
		{
			return 1.0f;
		}
		/** Gets the minimum value in the probability range.
		 *
		 * @return min probability value.
		 */
		static float lo()
		{
			return 0.0f;
		}
	};
	/** @brief Mean cross-entropy loss
	 * 
	 * This class defines the mean cross-entropy loss function for the Probing algorithm.
	 */
	struct CXELoss
	{
		/** Estimates the probability from a probability range.
		 *
		 * @param a start of probability range.
		 * @param b end of probability range.
		 * @return estimated probability.
		 */
		template<class F>
		static F probability(F a, F b)
		{
			if( std::abs(a-b) < 1e-8 ) return a;
			return 1.0f / ( 1.0f + exp( (se(b)-se(a))/(b-a) ) );
		}
		/** Estimates the loss over a probability range.
		 *
		 * @param a start of probability range.
		 * @param b end of probability range.
		 * @param n number of examples falling in this range.
		 * @return estimated probability.
		 */
		template<class F>
		static F loss(F a, F b, unsigned int n)
		{
			F p = probability(a,b);
			return F(n)*( ( 1-a )*log2( (1-a) / (1-p) ) + a * log2( a/p ) );
		}
		/** Gets the maximum value in the probability range.
		 *
		 * @return maximum probability value.
		 */
		static float hi()
		{
			return 0.99999f;
		}
		/** Gets the minimum value in the probability range.
		 *
		 * @return minimum probability value.
		 */
		static float lo()
		{
			return 0.00001f;
		}

	private:
		static double se(double val)//shannonEntropy
		{
			return -( val * log2(val) + (1.0f-val) * log2(1.0f-val) );
		}
		static double log2(double val)
		{
			static double log2v = 1.0;///std::log(2.0);
			return std::log(val)*log2v;//log10
		}
	};

};


#endif











