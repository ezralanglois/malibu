/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Quanting.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_QUANTING_H
#define _EXEGETE_QUANTING_H
#include "WeakLearner.h"
#include "Committee.h"
#include <cmath>

/** @file Quanting.h
 * @brief Quanting quantile regression wrapper
 * 
 * This file contains the Quanting class template.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#define _QUANTING_VERSION 101000

namespace exegete
{
	/** @brief Quanting quantile regression wrapper
	 * 
	 * This class template defines the Quanting machine learning reduction that transforms
	 * an important weighted classifier into a classifier that gives accurate quantile 
	 * regression.
	 */
	template<class T>
	class Quanting : public Committee< WeakLearner<T> >
	{
 		typedef T learner_type;
		typedef typename T::weight_type learner_weight;
		typedef Committee< WeakLearner<T> > committee_type;
		typedef typename committee_type::learner_type wlearner_type;
		typedef typename committee_type::size_type size_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator iterator;
		typedef typename learner_type::dataset_type subset_type;
		typedef typename subset_type::attribute_type attribute_type;
		typedef typename subset_type::class_type class_type;
		typedef typename subset_type::const_iterator const_subset_iterator;
		typedef typename subset_type::iterator subset_iterator;
	public:
		/** Flags Quanting as tunable. **/
		enum{ TUNE=T::TUNE, ARGUMENT=T::ARGUMENT+1 };

	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<attribute_type, double, void>			dataset_type;
		/** Defines a testset type. **/
		typedef ExampleSet<attribute_type, double, std::string*>	testset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer		const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type				descriptor_type;
		/** Defines a float type. **/
		typedef double												float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator							argument_iterator;
		/** Defines a weight type. **/
		typedef void												weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef int prediction_vector;
	private:
		typedef TuneParameterTree									argument_type;
		typedef typename argument_type::parameter_type				range_type;
		typedef typename dataset_type::const_iterator 				const_example_iterator;
		typedef typename dataset_type::iterator 					example_iterator;

	public:
		/** Constructs a Quanting object.
		 */
		Quanting() : iterationInt(10), quantileFlt(0.75f), sortBool(1)
		{
			learner_type::add_test( learner_type::BINARY );
			learner_type::add_test( learner_type::NOMIL );
			learner_type::remove_test( learner_type::BINARY );
		}
		/** Destructs a Quanting object.
		 */
		~Quanting()
		{
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(Quanting));
				arginit(map, iterationInt, "quantn", "number of quanting iterations");
				arginit(map, quantileFlt,  "quant",  "the quantile");
				arginit(map, sortBool,     "sort",   "sort the examples by class for threshold?");
			}
		}

	public:
		/** Constructs a copy of a Quanting object.
		 *
		 * @param ref a Quanting object.
		 */
		Quanting(const Quanting& ref) : committee_type(ref), iterationInt(ref.iterationInt), quantileFlt(ref.quantileFlt), sortBool(ref.sortBool)
		{
		}
		/** Assigns a copy of a Quanting object.
		 *
		 * @param ref a Quanting object.
		 * @return a reference to this object.
		 */
		Quanting& operator=(const Quanting& ref)
		{
			committee_type::operator=(ref);
			argument_copy(ref);
			return *this;
		}

	public:
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const;
		/** Builds a quanting wrapper on an importance-weighted classifier.
		 *
		 * @todo add estimate of out-of-bag-error
		 * 
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		 const char* train(dataset_type& learnset);

	public:
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Copies arguments in a quanting wrapper.
		 * 
		 * @param ref a source quanting model.
		 */
		void argument_copy(const Quanting& ref)
		{
			iterationInt = ref.iterationInt;
			quantileFlt = ref.quantileFlt;
			sortBool = ref.sortBool;
		}
		/** Mkes a shallow copy of the source object.
		 *
		 * @param ref the source object.
		 */
		void shallow_copy(Quanting& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("Quanting ")+T::class_name();
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Quanting
		 */
		static std::string name()
		{
			return "Quanting";
		}
		/** Gets the version of the quanting wrapper.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _QUANTING_VERSION;
		}
		/** Get the expected name of the program.
		 * 
		 * @return quant
		 */
		static std::string progname()
		{
			return "quant";
		}
		/** Gets the number of iterations.
		 * 
		 * @return number of iterations.
		 */
		size_type iteration()const
		{
			return iterationInt;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return quant
		 */
		static std::string prefix()
		{
			return "quant";
		}
		/** Reads a quanting committee from the input stream.
		 *
		 * @param in an input stream.
		 * @param ref a quanting committee.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, Quanting& ref)
		{
			in >> ref.quantileFlt;
			if( in.get() != '\n' )
			{
				ref.errormsg(in, "Missing newline character in quanting model");
				return in;
			}
			in >> ref.committee();
			ref.iterationInt = ref.size();
			return in;
		}
		/** Writes an quanting committee to the output stream.
		 *
		 * @param out an output stream.
		 * @param ref a quanting committee.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const Quanting& ref)
		{
			out << ref.quantileFlt << "\n";
			out << ref.committee();
			return out;
		}

	protected:
		/** Predicts the index for the attribute vector.
		 * 
		 * @param pred an attribute vector.
		 * @return predicted index.
		 */
		unsigned int predictn(const_attribute_pointer pred)const;
		
	private:
		typedef std::vector<float_type> float_vector;
		typedef typename float_vector::iterator float_iterator;
		void reweight(example_iterator beg, example_iterator end, subset_iterator it, float_type t);
		void rethreshold(const_example_iterator beg, const_example_iterator end, float_vector& vec);
	private:
		unsigned int iterationInt;
		float quantileFlt;
		int sortBool;
	};
	
	template<class T>
	const char* Quanting<T>::train(dataset_type& learnset)
	{
		const char* msg;
		subset_type subset(learnset, learnset.size());
		subset.classCount(2);
		if(sortBool==1)
		{
			std::vector<float_type> ths(learnset.size());
			float_iterator it = ths.begin();
			rethreshold(learnset.begin(), learnset.end(), ths);
			committee_type::resize(iterationInt);
			for(iterator beg=committee_type::begin(), end=committee_type::end();beg != end;++beg, ++it)
			{
				reweight(learnset.begin(), learnset.end(), subset.begin(), *it);
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				beg->shallow_copy(*this, *it);
			}
		}
		else
		{
			float_type inc = 1.0f / float_type( iterationInt ), idx=inc;
			committee_type::resize(iterationInt);
			for(iterator beg=committee_type::begin(), end=committee_type::end();beg != end;++beg, idx+=inc)
			{
				reweight(learnset.begin(), learnset.end(), subset.begin(), idx);
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				beg->shallow_copy(*this, idx);
			}
		}
		return 0;
	}
	template<class T>
	typename Quanting<T>::float_type Quanting<T>::predict(const_attribute_pointer pred)const
	{
		if( committee_type::empty() ) return 0.5f;
		unsigned int n=predictn(pred);
		if( n == 0 ) return 0.0f;
		return( n < committee_type::size() ) ? committee_type::learnerAt(n-1).weight() : 1.0 ;
	}
	template<class T>
	unsigned int Quanting<T>::predictn(const_attribute_pointer pred)const
	{
		unsigned int n=0;
		for(const_iterator curr=committee_type::begin(), end=committee_type::end();curr != end;++curr)
			if( curr->predict(pred) > committee_type::threshold() ) n++;
		return n;
	}
	template<class T>
	void Quanting<T>::reweight(example_iterator beg, example_iterator end, subset_iterator it, float_type t)
	{
		float qpos = quantileFlt;
		float qneg = 1.0f - quantileFlt;
		for(;beg != end;++beg, ++it) 
		{
			if( beg->y() < t )
			{
				it->y( 0 );
				it->w( qneg );
			}
			else
			{
				it->y( 1 );
				it->w( qpos );
			}
			it->x( beg->x() );
		}
	}

	template<class T>
	void Quanting<T>::rethreshold(const_example_iterator beg, const_example_iterator end, float_vector& vec)
	{
		float_iterator tlst = vec.begin(), tcurr, tend=vec.end();
		for(tcurr=vec.begin();beg != end;++beg, ++tcurr) *tcurr = beg->y();
		std::stable_sort(vec.begin(), vec.end());
		for(tcurr=vec.begin(),tend=vec.end();tcurr != tend;++tcurr)
		{
			if( *tlst != *tcurr )
			{
				++tlst;
				*tlst = *tcurr;
			}
		}
		vec.resize(std::distance(vec.begin(), tlst));
		if( iterationInt < vec.size() )
		{
			tlst = vec.begin(), tend=vec.begin()+iterationInt;
			float_type inc1 = 1.0f/iterationInt, idx1 = inc1;
			float_type idx2 = 0.0f, inc2 = 1.0f/vec.size();
			for(unsigned int i=0;i<vec.size();++i)
			{
				if( idx2 >= idx1 )
				{
					idx1 += inc1;
					*tlst = vec[i];
					tlst++;
					if( tlst == tend ) break;
				}
				idx2 += inc2;
			}
			
		}
		else iterationInt = (unsigned int)vec.size();
	}

};


#endif











