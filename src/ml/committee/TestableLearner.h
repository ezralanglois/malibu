/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * TestableLearner.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_TESTABLELEARNER_H
#define _EXEGETE_TESTABLELEARNER_H
#include "ArgumentMap.h"

/** @file TestableLearner.h
 * @brief Learner with a copy of a testset
 * 
 * This file contains the TestableLearner class template.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	/** @brief Learner with a copy of a testset
	 * 
	 * This class defines a learner with a copy of a testset.
	 */
	template<class T, class U = typename T::testset_type>
	class TestableLearner : public T
	{
	public:
		/** Defines a parent type. **/
		typedef typename T::parent_type parent_type;
		/** Defines a value type. **/
		typedef TestableLearner<T,U> value_type;
		/** Defines a weighted type. **/
		typedef U testset_type;
	public:
		/** Constructs a default weak learner. 
		 */
		TestableLearner()
		{
		}
		/** Destructs a weak learner.
		 */
		~TestableLearner()
		{
		}

	public:
		/** Makes a shallow copy of the weak learner.
		 *
		 * @param ref a reference to a weak learner.
		 * @param w a weight.
		 */
		void shallow_copy(TestableLearner& ref, testset_type& w)
		{
			T::shallow_copy(ref);
			testset(w);
		}
		/** Makes a shallow copy of the weak learner.
		 *
		 * @param ref a reference to a weak learner.
		 * @param w a weight.
		 */
		void shallow_copy(T& ref, testset_type& w)
		{
			T::shallow_copy(ref);
			testset(w);
		}
		/** Makes a shallow copy of the weak learner.
		 *
		 * @param ref a reference to a weak learner.
		 */
		void shallow_copy(TestableLearner& ref)
		{
			T::shallow_copy(ref);
		}
		/** Gets a testset.
		 *
		 * @return a testset.
		 */
		testset_type& testset()
		{
			return testSet;
		}
		/** Sets a shallow copy of a testset.
		 *
		 * @param val a testset.
		 */
		void testset(testset_type& val)
		{
			testSet = val;
			testSet.assign(val);
		}
		/** Gets the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("TestableLearner ")+T::class_name();
		}

	private:
		testset_type testSet;
	};
	/** @brief Selects the parent classifier as a base learner
	 * 
	 * This class selects the parent classifier as a base learner.
	 */
	template<class T, class U>
	struct base_learner< TestableLearner<T, U> >
	{
		/** Defines a base learner result. **/
		typedef typename base_learner<T>::result result;
	};
	
};

#endif


