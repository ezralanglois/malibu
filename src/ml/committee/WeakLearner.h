/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WeakLearner.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_WEAKLEARNER_H
#define _EXEGETE_WEAKLEARNER_H
#include "ArgumentMap.h"

/** @file WeakLearner.h
 * @brief Weighted learner
 * 
 * This file contains the WeakLearner class.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
namespace exegete
{
	/** @brief Weighted learner
	 * 
	 * This class defines a weighted learner.
	 */
	template<class T, class U = typename T::float_type>
	class WeakLearner : public T
	{
	public:
		/** Defines a parent type. **/
		typedef typename T::parent_type parent_type;
		/** Defines a value type. **/
		typedef WeakLearner<T,U> value_type;
		/** Defines a weighted type. **/
		typedef U w_type;
	public:
		/** Constructs a default weak learner. 
		 */
		WeakLearner()
		{
		}
		/** Constructs a copy of a weak learner.
		 *
		 * @param wl a weak learner.
		 * @param n a tune type.
		 */
		WeakLearner(const WeakLearner& wl, int n) : T(wl, n), weightVal(wl.weightVal)
		{
		}
		/** Destructs a weak learner.
		 */
		~WeakLearner()
		{
		}

	public:
		/** Makes a shallow copy of the weak learner.
		 *
		 * @param ref a reference to a weak learner.
		 * @param w a weight.
		 */
		void shallow_copy(WeakLearner& ref, w_type w)
		{
			T::shallow_copy(ref);
			weight(w);
		}
		/** Makes a shallow copy of the weak learner.
		 *
		 * @param ref a reference to a weak learner.
		 */
		void shallow_copy(WeakLearner& ref)
		{
			T::shallow_copy(ref);
		}
		/** Gets a weight. 
		 *
		 * @return a weight.
		 */
		const w_type& weight()const
		{
			return weightVal;
		}
		/** Sets a weight. 
		 *
		 * @param val a weight.
		 */
		void weight(const w_type& val)
		{
			weightVal = val;
		}
		/** Multiply weight by value. 
		 *
		 * @param val a value.
		 */
		void multiply(const w_type& val)
		{
			weightVal *= val;
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("WeakLearner ")+T::class_name();
		}

	private:
		static T& parentType(T& val){return val;}
		static const T& parentType(const T& val){return val;}
		/** Reads a weighted learner from an input stream.
		 *
		 * @param in an input stream.
		 * @param wl a weighted learner.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, WeakLearner& wl)
		{
			in >> wl.weightVal;
			if( in.eof() || in.get() != '\t' )
			{
				wl.errormsg("Error parsing weak learner");
				in.setstate( std::ios::failbit );
				return in;
			}
			in >> parentType(wl);
			return in;
		}
		/** Writes an weighted learner to the output stream.
		 *
		 * @param out an output stream.
		 * @param wl a weighted learner.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const WeakLearner& wl)
		{
			out << wl.weightVal << "\t" << parentType(wl);
			return out;
		}

	private:
		w_type weightVal;
	};


	/** @brief Weighted learner
	 * 
	 * This class defines a weighted learner.
	 */
	template<class T, class U>
	class WeakLearner< T, std::pair< U, U > > : public T
	{
	public:
		/** Defines a parent type. **/
		typedef typename T::parent_type parent_type;
		/** Defines a value type. **/
		typedef WeakLearner<T, std::pair< U, U > > value_type;
		/** Defines a weighted type. **/
		typedef std::pair< U, U > w_type;
		/** Defines an attribute pointer. **/
		typedef typename T::const_attribute_pointer const_attribute_pointer;
		
	public:
		/** Constructs a default weak learner. 
		 */
		WeakLearner()
		{
		}
		/** Constructs a copy of a weak learner.
		 *
		 * @param wl a weak learner.
		 * @param n a tune type.
		 */
		WeakLearner(const WeakLearner& wl, int n) : T(wl, n), weightVal(wl.weightVal)
		{
		}
		/** Destructs a weak learner.
		 */
		~WeakLearner()
		{
		}

	public:
		/** Predict the index in the pair.
		 * 
		 * @param p attribute vector.
		 * @return index.
		 */
		U predict_index(const_attribute_pointer p)const
		{
			return T::predict(p) > T::threshold() ? weightVal.first : weightVal.second;
		}
		/** Makes a shallow copy of the weak learner.
		 *
		 * @param ref a reference to a weak learner.
		 */
		void shallow_copy(WeakLearner& ref)
		{
			T::shallow_copy(ref);
		}
		/** Gets a weight. 
		 *
		 * @return a weight.
		 */
		const w_type& weight()const
		{
			return weightVal;
		}
		/** Sets a weight. 
		 *
		 * @param val a weight.
		 */
		void weight(const w_type& val)
		{
			weightVal = val;
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("WeakLearner ")+T::class_name();
		}

	private:
		static T& parentType(T& val){return val;}
		static const T& parentType(const T& val){return val;}
		/** Reads a weighted learner from an input stream.
		 *
		 * @param in an input stream.
		 * @param wl a weighted learner.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, WeakLearner& wl)
		{
			in >> wl.weightVal.first;
			if( in.eof() || in.get() != '\t' )
			{
				wl.errormsg("Error parsing weak learner");
				in.setstate( std::ios::failbit );
				return in;
			}
			in >> wl.weightVal.second;
			if( in.eof() || in.get() != '\t' )
			{
				wl.errormsg("Error parsing weak learner");
				in.setstate( std::ios::failbit );
				return in;
			}
			in >> parentType(wl);
			return in;
		}
		/** Writes an weighted learner to the output stream.
		 *
		 * @param out an output stream.
		 * @param wl a weighted learner.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const WeakLearner& wl)
		{
			out << wl.weightVal.first << "\t" << wl.weightVal.second << "\t" << parentType(wl);
			return out;
		}

	private:
		w_type weightVal;
	};

	
};

#endif


