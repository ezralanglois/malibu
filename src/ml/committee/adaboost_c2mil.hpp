/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * adaboost_c2mil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ADABOOSTC2M1_H
#define _EXEGETE_ADABOOSTC2M1_H
#include "Committee.h"
#include "WeakLearner.h"
#include "ExampleSetAlgorithms.h"
/** Defines the version of the AdaBoost.C2MIL algorithm. 
 * @todo add to group
 */
#define _ADABOOST_C2MIL_VERSION 101000


/** @file adaboost_c2mil.h
 * @brief Wraps classifier to handle MIL
 * 
 * This file contains the AdaBoost.C2MIL class template.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteCommittee Committee
 * 
 *  This group holds all the committee classifier files.
 */

namespace exegete
{
	/** @brief Wraps classifier to handle MIL using AdaBoost
	 * 
	 * This class template defines an AdaBoost.C2MIL committee classifier.
	 */
	template<class T>
	class adaboost_c2mil : public Committee< WeakLearner<T> >
	{
		typedef T 										learner_type;
		typedef typename T::weight_type 				learner_weight;
		typedef IsVoid<learner_weight> 					is_void;
		typedef typename SelectType<is_void::value,double,learner_weight>::Result w_type;
		typedef Committee< WeakLearner<T> > 			committee_type;
		typedef typename committee_type::learner_type 	wlearner_type;
		typedef typename committee_type::size_type 		size_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator 		iterator;
		typedef typename learner_type::dataset_type 	subset_type;
		typedef typename subset_type::attribute_type 	attribute_type;
		typedef typename subset_type::class_type 		class_type;
	public:
		/** Flags AdaBoost.C2MIL as tunable. **/
		enum{ TUNE=1, ARGUMENT=T::ARGUMENT+1, FAST_TUNE=1 };

	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines an example set as a learnset type. **/
		typedef ExampleSet<attribute_type, class_type, w_type>	dataset_type;
		/** Defines a learnset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a svm attribute value as a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines the second template parameter as a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef void											weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef int prediction_vector;
	private:
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename dataset_type::iterator					example_iterator;
		typedef typename dataset_type::bag_iterator				bag_iterator;

	public:
		/** Constructs a default AdaBoost classifier.
		 */
		adaboost_c2mil() : sumpred(0), iterationInt(0), confratedCh('n'), alphaFlt(0.0f),
						 iterationSel(iterationInt, "Iteration", true, range_type(1024, 1025, 1, '+'))
		{
			iterationSel.add( &learner_type::argument() );
			learner_type::add_test( learner_type::MIL );
		}
		/** Destructs an AdaBoost classifier.
		 */
		~adaboost_c2mil()
		{
			::erase(sumpred);
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(adaboost_c2mil));
				if(t>=0)
				arginit(map, iterationSel,  "boostn",  "number of boosting iterations");
				else
				arginit(map, iterationInt,  "boostn",	 "number of boosting iterations");
				arginit(map, confratedCh, 	"boostc",  "force confidence-rated boosting>Auto:a;Yes:y;No:n");
				if(is_void::value) 
				arginit(map, alphaFlt,	  	"boostr",  "sample without replacement proportion");
			}
		}
		
	public:
		/** Constructs a deep copy of the adaboost_c2mil classifier.
		 *
		 * @param boost a source adaboost_c2mil model.
		 */
		adaboost_c2mil(const adaboost_c2mil& boost) : committee_type(boost), iterationInt(boost.iterationInt), confratedCh(boost.confratedCh), alphaFlt(boost.alphaFlt), 
										  iterationSel(iterationInt, boost.iterationSel)
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Assigns a deep copy of the adaboost_c2mil model.
		 *
		 * @param boost a source adaboost_c2mil model.
		 * @return a reference to this object.
		 */
		adaboost_c2mil& operator=(const adaboost_c2mil& boost)
		{
			committee_type::operator=(boost);
			argument_copy(boost);
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.0f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			ASSERT(!committee_type::empty());
			if( committee_type::empty() ) return 0.0f;
			float_type sum=0.0f;
			const_iterator beg=committee_type::begin(), end=beg+std::min(iterationInt, committee_type::size());

			if( confidence_rated() )//committee_type
			{
				if( committee_type::threshold() > 0.0f )
				{
					for(;beg != end;++beg) sum+=conf(beg->predict(pred));
				}
				else
				{
					for(;beg != end;++beg) sum+=beg->predict(pred);
				}
			}
			else
			{
				for(;beg != end;++beg) sum+=sign(beg->predict(pred), committee_type::threshold())*beg->weight();
			}
			return sum;
		}
		/** Builds a model for the adaboost_c2mil classifier.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			sumpred = reallocate(sumpred, learnset.bag_begin(), learnset.bag_end(), learnset.size());
			if( iterationInt == 0 ) iterationInt = learnset.size()*2;
			committee_type::resize(iterationInt);
			initwgts(learnset.begin(), learnset.end(), float_type(learnset.size()));
			return train(learnset, learnset.bag_begin(), learnset.bag_end(), is_void());
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return AdaBoost.C2MIL
		 */
		static std::string name()
		{
			return "AdaBoost.C2MIL";
		}
		/** Get the version of the adaboost_c2mil.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _ADABOOST_C2MIL_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return boostc2mm
		 */
		static std::string prefix()
		{
			return "boostc2mil";
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationSel;
		}
		/** Copies arguments in a adaboost_c2mil model.
		 * 
		 * @param boost a source adaboost_c2mil model.
		 */
		void argument_copy(const adaboost_c2mil& boost)
		{
			iterationInt = boost.iterationInt;
			confratedCh = boost.confratedCh;
			alphaFlt = boost.alphaFlt;
			iterationSel = boost.iterationSel;
		}
		/** Makes a shallow copy of an adaboost_c2mil classifier.
		 *
		 * @param ref a reference to an adaboost_c2mil classifier.
		 */
		void shallow_copy(adaboost_c2mil& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("adaboost_c2mil ")+T::class_name();
		}
		/** Get the expected name of the program.
		 * 
		 * @return boost
		 */
		static std::string progname()
		{
			return "boost";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Test if AdaBoost is confidence-rated. 
		 * 
		 * @return true if AdaBoost is confidence-rated.
		 */
		bool confidence_rated()const
		{
			return ( confratedCh == 'y' || (confratedCh == 'a' && committee_type::threshold() == 0.0f) );
		}

	protected:
		/** Builds a model for the adaboost_c2mil classifier using sampling.
		 *
		 * @param learnset the training set.
		 * @param bbeg start of a collection of bags.
		 * @param bend end of a collection of bags.
		 * @param v a compile-time method selection.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, bag_iterator fbeg, bag_iterator fend, IsVoid<void> v)
		{
			subset_type subset(learnset);
			float_type alpha=1.0f;
			iterator beg = committee_type::begin();
			const char* msg;
			for(iterator end=committee_type::end();beg != end;++beg)
			{
				if((msg=sample(learnset, subset, beg)) != 0) return msg;
				if( subset.empty() ) break;
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				if( !confidence_rated() )
				{
					alpha = errorwgt(fbeg, fend, committee_type::threshold());
					if( testerror(beg, alpha) ) 
					{
						beg->shallow_copy(*this, 1.0f);
						if( beg == committee_type::begin() ) ++beg;
						break;
					}
					alpha = invconf(alpha);
				}
				updtwgts(fbeg, fend, alpha, sumpred);
				beg->shallow_copy(*this, alpha);
			}
			committee_type::setcount(size_type(std::distance(committee_type::begin(), beg)));
			return 0;
		}
		/** Builds a model for the adaboost_c2mil classifier using weights.
		 *
		 * @param learnset the training set.
		 * @param bbeg start of a collection of bags.
		 * @param bend end of a collection of bags.
		 * @param v a compile-time method selection.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, bag_iterator fbeg, bag_iterator fend, IsVoid<w_type> v)
		{
			float_type alpha=1.0f;
			iterator beg = committee_type::begin();
			committee_type::normalization(learnset.size());
			const char* msg;
			for(iterator end=committee_type::end();beg != end;++beg)
			{
				if( (msg=committee_type::train(learnset)) != 0 ) return msg;
				if( !confidence_rated() )
				{
					alpha = errorwgt(fbeg, fend, committee_type::threshold());
					if( testerror(beg, alpha) ) 
					{
						beg->shallow_copy(*this, 1.0f);
						if( beg == committee_type::begin() ) ++beg;
						break;
					}
					alpha = invconf(alpha);
				}
				updtwgts(fbeg, fend, alpha, sumpred);
				beg->shallow_copy(*this, alpha);
			}
			committee_type::setcount(size_type(std::distance(committee_type::begin(), beg)));
			return 0;
		}

	protected:
		/** Tests if AdaBoost has converged or diverged.
		 *
		 * @param beg an iterator to start of classifiers.
		 * @param alpha the error rate.
		 * @return true if AdaBoost should stop early.
		 */
		bool testerror(iterator beg, float_type alpha)
		{
			if( std::abs(alpha-0.0f) < 1e-5 ) //alpha == 0.0f )
			{
				std::cerr << "Adaboost.C2MIL converged early" << std::endl;
				//committee_type::setcount(std::distance(committee_type::begin(),beg));
				return true;
			}
			if( std::abs(alpha-0.5f) < 1e-5 )
			{
				std::cerr << "Adaboost.C2MIL diverged early" << std::endl;
				//committee_type::setcount(std::distance(committee_type::begin(),beg));
				return true;
			}
			return false;
		}
		/** Gets the sign of a prediction.
		 *
		 * @param y a class value.
		 * @param t a threshold.
		 * @return sign of class value.
		 */
		inline static int sign(float_type y, float_type t)
		{
			return y>t?1:-1;
		}
		/** Gets a confidence in a prediction.
		 *
		 * @param val an error value.
		 * @return a log of a probability.
		 */
		inline static float_type invconf(float_type val)
		{
			static float_type EPS = 0.00001f;
			val = std::min(std::max(val,EPS), float_type(1)-EPS);
			return 0.5f * std::log( (1.0f - val) / val );
		}
		/** Gets a confidence in a prediction.
		 *
		 * @param val a probability value.
		 * @return a log of a probability.
		 */
		inline static float_type conf(float_type val)
		{
			static float_type EPS = 0.00001f;
			val = std::min(std::max(val,EPS), float_type(1)-EPS);
			return 0.5f * std::log( val / (1.0f - val) );
		}
		/** Initializes weights for a collection of examples to a uniform distribution.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param n number of examples.
		 */
		static void initwgts(example_iterator beg, example_iterator end, float_type n)
		{
			ASSERT(n != 0.0f);
			n = 1.0f/n;
			for(;beg != end;++beg) beg->w(n);
		}
		/** Normalizes weights for a collection of examples.
		 *
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param w total weight.
		 */
		static void normwgts(bag_iterator beg, bag_iterator end, float_type w)
		{
			ASSERT(w != 0.0f);
			w = 1.0f/w;
			for(;beg != end;++beg) normwgts(beg->begin(), beg->end(), w);
		}
		/** Normalizes weights for a collection of examples.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param w recepircal of the total weight.
		 */
		static void normwgts(example_iterator beg, example_iterator end, float_type w)
		{
			for(;beg != end;++beg) beg->w_update(w);
		}
		/** Predict label of bag.
		 *
		 * @param learner a classifier to make predictions.
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param t a threshold.
		 * @return a bag-level prediction.
		 */
		float_type predict_bag(learner_type& learner, example_iterator beg, example_iterator end, float_type t)
		{
			ASSERT(beg != end);
			float_type psum=0.0f, nsum=0.0f, pred;
			size_type ptot=0, ntot=0;
			for(;beg != end;++beg)
			{
				if( (pred=learner.predict(*beg)) > t )
				{
					psum += pred;
					ptot++;
				}
				else
				{
					nsum += pred;
					ntot++;
				}
			}
			return (ptot>0) ? psum/float_type(ptot) : nsum/float_type(ntot);
		}
		/** Calculates weighted error over a set of examples.
		 *
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param t a threshold.
		 * @return error sumed over bags.
		 */
		float_type errorwgt(bag_iterator beg, bag_iterator end, float_type t)
		{
			float_type err=0.0f;
			for(;beg != end;++beg)
			{
				if( beg->y() > 0 ) 
				{
					if( predict_bag(*this, beg->begin(), beg->end(), t) <= t )
						err += sumwgt(beg->begin(), beg->end());
				}
				else 
				{
					err += errorwgt(beg->begin(), beg->end(), t);
				}
			}
			return err;
		}
		/** Calculates the sum of the weights over a set of examples.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @return total weight over examples.
		 */
		float_type sumwgt(example_iterator beg, example_iterator end)
		{
			float_type sum=0.0f;
			for(;beg != end;++beg) sum += beg->w();
			return sum;
		}
		/** Calculates weighted error over a set of examples.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param t a threshold.
		 * @return total error over examples.
		 */
		float_type errorwgt(example_iterator beg, example_iterator end, float_type t)
		{
			float_type err=0.0f;
int cl = beg->y();
			for(;beg != end;++beg)
			{
ASSERT(cl == beg->y());
				if( sign(learner_type::predict(*beg), t) != sign(beg->y(), 0) ) 
					err+=beg->w();
			}
			return err;
		}
		/** Updates the weight distribution using predictions.
		 *
		 * @param beg a start iterator to collection of bags.
		 * @param end a end iterator to collection of bags.
		 * @param alpha a prediction weight.
		 * @param th a threshold.
		 */
		void updtwgts(bag_iterator beg, bag_iterator end, float_type alpha, float_type** spred)
		{
			float_type th = committee_type::threshold();
			bag_iterator start=beg;
			float_type wsum = 0.0f;//, pred;
			for(;beg != end;++beg,++spred)
			{
				if( beg->y() > 0 )
					 wsum+=updtwgts_pos(beg->begin(), beg->end(), alpha, *spred);
				else wsum+=updtwgts_neg(beg->begin(), beg->end(), alpha, th);
			}
			normwgts(start, end, wsum);
		}
		/** Scale prediction by confidence.
		 * 
		 * @param pred a prediction.
		 * @param alpha a confidence.
		 * @param th a threshold.
		 * @return scaled prediction.
		 */
		inline float_type confidence(float_type pred, float_type alpha, float_type th)
		{
			if( confidence_rated() ) 
			{
				if( committee_type::threshold() > 0.0f )
					pred = conf(pred);
			}
			else pred = sign(pred, th)*alpha;
			return pred;
		}
		/** Updates the weight distribution using predictions for positive bags.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param alpha a prediction weight.
		 * @param th a threshold.
		 * @return total positive weight over examples.
		 */
		float_type updtwgts_pos(example_iterator beg, example_iterator end, float_type alpha, float_type* spred)
		{
			float_type th = committee_type::threshold(), val;
			float_type wsum = 0.0f, pred=predict_bag(*this,beg,end,th), zsum=0.0f;
			pred = confidence(pred, alpha, th);
			ASSERT(!std::isnan(pred));
			
			float_type* cpred=spred;
			for(example_iterator curr=beg;curr != end;++curr,++cpred)
			{
				*cpred += confidence(committee_type::predict(*curr), alpha, th);
				ASSERTMSG(1 == curr->y(), curr->y());
				zsum = std::exp( -1 * pred * sign(curr->y(), 0) ) * curr->w();
				ASSERT(!std::isnan(zsum));
				curr->w(zsum);
				wsum += zsum;
			}
			if( pred > th )
			{
				cpred=spred;
				zsum = 0.0f;
				for(example_iterator curr=beg;curr != end;++curr,++cpred)
				{
					pred = committee_type::predict(*curr);
					pred = confidence(pred, alpha, th);
					val = 1.0 / ( 1.0 + std::exp(-pred) );
					//val = 1.0 / ( 1.0 + std::exp(-(*cpred)) );
					if( val < 1e-20 ) val = 1e-20 ;
					curr->w( val );
					zsum += val;
				}
				//update bag weights
				zsum = wsum/zsum;
				normwgts(beg, end, zsum);
				wsum=0.0f;
				for(example_iterator curr=beg;curr != end;++curr) wsum+=curr->w();
			}
			return wsum;
		}
		/** Updates the weight distribution using predictions for negative bags.
		 *
		 * @param beg a start iterator to collection of examples.
		 * @param end a end iterator to collection of examples.
		 * @param alpha a prediction weight.
		 * @param th a threshold.
		 * @return total positive weight over examples.
		 */
		float_type updtwgts_neg(example_iterator beg, example_iterator end, float_type alpha, float_type th)
		{
			float_type wsum = 0.0f, pred;
			for(;beg != end;++beg)
			{
				pred = committee_type::predict(*beg);
				pred = confidence(pred, alpha, th);
				ASSERT(!std::isnan(pred));
				pred = std::exp( -1 * pred * sign(beg->y(), 0) ) * beg->w();
				ASSERT(!std::isnan(pred));
				beg->w(pred);
				wsum += pred;
			}
			return wsum;
		}

	private:
		static float_type** reallocate(float_type** ptr, bag_iterator beg, bag_iterator end, size_type n)
		{
			bool init=(ptr==0);
			float_type* old=0;
			size_type r = size_type(std::distance(beg, end));
			if(!init) old = *ptr;
			ptr = ::resize(ptr, r+1);
			*ptr = old;
			*ptr = ::resize(*ptr, n);
			for(unsigned int i=1;beg != end;++beg,++i) ptr[i] = ptr[i-1] + beg->size();
			ptr[r] = ptr[0] + n;
			std::fill(ptr[0], ptr[r], 0.0f);
			return ptr;
		}
		const char* sample(dataset_type& learnset, subset_type& subset, iterator beg)
		{
			size_type tot=0;
			do{
				instance_wsample(learnset, subset, alphaFlt*learnset.size());
				if( tot > 1000 )
				{
					std::cerr << "WARNING: Too few examples for sampling: " << std::distance(committee_type::begin(), beg) << " " << learnset.begin()->w() << " * " << alphaFlt * learnset.size() << " " << learnset.countClass(0) << " " << learnset.countClass(1) << std::endl;
					if(beg != committee_type::begin()) return ERRORMSG("AdaBoost.C2MIL: Too few examples for sampling at iteration " << std::distance(committee_type::begin(), beg));
					subset.clear();
					break;
					//std::exit(1);
				}
				tot++;
			} while( subset.hasZeroClass() );
			return 0;
		}
		/** Reads an AdaBoost.C2MIL model from the input stream.
		 *
		 * @param in an input stream.
		 * @param boost a committee.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, adaboost_c2mil& boost)
		{
			in >> boost.confratedCh;
			if( in.get() != '\t' ) return boost.fail(in, "Missing tab character in AdaBoost model");
			in >> boost.committee();
			boost.iterationInt = boost.size();
			return in;
		}
		/** Writes an AdaBoost.C2MIL model to the output stream.
		 *
		 * @param out an output stream.
		 * @param boost a committee.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const adaboost_c2mil& boost)
		{
			out << boost.confratedCh << "\t";
			out << boost.committee();
			return out;
		}

	private:
		float_type** sumpred;
		size_type iterationInt;
		char confratedCh;
		float alphaFlt;
		argument_type iterationSel;
	};
};


#endif


