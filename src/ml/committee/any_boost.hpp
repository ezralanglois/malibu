/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * any_boost.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ANYBOOST_HPP
#define _EXEGETE_ANYBOOST_HPP
#include "Committee.h"
#include "WeakLearner.h"
#include "ExampleSetAlgorithms.h"
#include "mathutil.h"

/** @file any_boost.hpp
 * @brief AnyBoost committee wrapper
 * 
 * This file contains the AnyBoost class template.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#define _ANYBOOST_VERSION 101000

namespace exegete
{
	/** @brief AnyBoost committee wrapper
	 * 
	 * This class template defines an AnyBoost committee classifier.
	 * 
	 * @todo write confidence to model file
	 */
	template<class T, template<class> class Cost>
	class any_boost : public Committee< WeakLearner<T> >
	{
		typedef Cost< WeakLearner<T> > cost_type;
		typedef T learner_type;
		typedef typename T::weight_type learner_weight;
		typedef IsVoid<learner_weight> is_void;
		typedef typename SelectType<is_void::value,double,learner_weight>::Result w_type;
		typedef Committee< WeakLearner<T> > committee_type;
		typedef typename committee_type::learner_type wlearner_type;
		typedef typename committee_type::size_type size_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator iterator;
		typedef typename learner_type::dataset_type subset_type;
		typedef typename subset_type::attribute_type attribute_type;
		typedef typename subset_type::class_type class_type;
	public:
		/** Flags any_boost as tunable. **/
		enum{ TUNE=cost_type::TUNE, ARGUMENT=T::ARGUMENT+1, FAST_TUNE=cost_type::TUNE, FORCE_MIL=cost_type::MIL, EVAL=cost_type::MIL };

	public:
		/** Defines a parent type. **/
		typedef T 												parent_type;
		/** Defines dataset type. **/
		typedef ExampleSet<attribute_type, class_type, w_type>	dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a float type. **/
		typedef double											float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef void											weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef int 											prediction_vector;
		
	private:
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename dataset_type::bag_iterator				bag_iterator;
		typedef typename dataset_type::iterator					example_iterator;
		typedef typename dataset_type::const_iterator			const_example_iterator;
		typedef typename dataset_type::const_bag_iterator		const_bag_iterator;

	public:
		/** Constructs a default AnyBoost classifier.
		 */		
		any_boost() : iterationInt(0), alphaFlt(0.0f),
					 iterationSel(iterationInt, "Iteration", true, range_type(1024, 1025, 1, '+'))
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Destructs an AnyBoost classifier.
		 */
		~any_boost()
		{
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t=0)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(any_boost));
				if(t>=0)
				arginit(map, iterationSel,  "boostn",  "number of boosting iterations");
				else
				arginit(map, iterationInt,  "boostn",  "number of boosting iterations");
				if(is_void::value) 
				arginit(map, alphaFlt,	  	"boostr",  "sample without replacement proportion");
			}
		}

	public:
		/** Constructs a deep copy of the AnyBoost classifier.
		 *
		 * @param boost a source AnyBoost model.
		 */
		any_boost(const any_boost& boost) : committee_type(boost), iterationInt(boost.iterationInt), alphaFlt(boost.alphaFlt),
										    iterationSel(iterationInt, boost.iterationSel)
		{
			iterationSel.add( &learner_type::argument() );
		}
		/** Assigns a deep copy of the any_boost model.
		 *
		 * @param boost a source any_boost model.
		 * @return a reference to this object.
		 */
		any_boost& operator=(const any_boost& boost)
		{
			committee_type::operator=(boost);
			argument_copy(boost);
			return *this;
		}
		
	public:
		/** Get threshold for a decision.
		 * 
		 * @param bag use bag level threshold.
		 * @return threshold for decision.
		 */
		float threshold(bool bag=false)const
		{
			return 0.5f;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param fbeg start of attribute vectors.
		 * @param fend end of attribute vectors.
		 * @return a real-valued prediction.
		 */
		template<class I>
		float_type predict(I fbeg, I fend)const
		{
			float_type sum=0.0f;
			for(;fbeg != fend;++fbeg) 
			{
				sum += cost_function.instance_sum(predict(*fbeg));
			}
			return cost_function.bag_predict(sum);
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		float_type predict(const_attribute_pointer pred)const
		{
			ASSERT(!committee_type::empty());
			if( committee_type::empty() ) return 0.0f;
			float_type sum=0.0f;
			const_iterator beg=committee_type::begin();
			const_iterator end=beg+std::min(iterationInt, committee_type::size());
			for(;beg != end;++beg) sum += cost_type::predict(*beg, pred);
			return cost_function.instance_predict(sum);
		}
		/** Builds a model for the AnyBoost classifier.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			if( iterationInt == 0 ) iterationInt = learnset.size()*2;
			committee_type::resize(iterationInt);
			cost_function.initialize(learnset);
			return train(learnset, is_void());
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return AnyBoost
		 */
		static std::string name()
		{
			return cost_type::name()+std::string("Boost");
		}
		/** Get the version of the AnyBoost classifier.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _ANYBOOST_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return anyboost
		 */
		static std::string prefix()
		{
			return cost_type::prefix()+std::string("boost");
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationSel;
		}
		/** Copies arguments in an any_boost model.
		 * 
		 * @param boost a source boost model.
		 */
		void argument_copy(const any_boost& boost)
		{
			iterationInt = boost.iterationInt;
			alphaFlt = boost.alphaFlt;
			iterationSel = boost.iterationSel;
		}
		/** Makes a shallow copy of an AnyBoost classifier.
		 *
		 * @param ref a reference to an AnyBoost classifier.
		 */
		void shallow_copy(any_boost& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("any_boost ")+T::class_name();
		}
		/** Gets the expected name of the program.
		 * 
		 * @return boost
		 */
		static std::string progname()
		{
			return cost_type::prefix()+std::string("boost");
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Gets the number of iterations.
		 * 
		 * @return number of iterations.
		 */
		size_type iteration()const
		{
			return iterationInt;
		}
		/** Debug the learning algorithm.
		 * 
		 * @param out an output stream.
		 */
		void debug(std::ostream& out)const
		{
			out << "Debug anyboost: " << iterationInt << " " << committee_type::threshold() << std::endl;
		}

	protected:
		/** Builds a model for the AnyBoost classifier using sampling.
		 *
		 * @param learnset the training set.
		 * @param v a dummy flag.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, IsVoid<void> v)
		{
			subset_type subset(learnset);
			float_type alpha=1.0;
			iterator beg = committee_type::begin();
			const char* msg;
			for(iterator end=committee_type::end();beg != end;++beg)
			{
				if((msg=sample(learnset, subset, beg)) != 0) return msg;
				if( subset.empty() ) break;
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				alpha = cost_function.update_weights(*this, learnset);
				if( alpha == 0.0 )
				{
					std::cerr << "any_boost stopped early, after only " << std::distance(committee_type::begin(), beg) << " iterations" << std::endl;
					break;
				}
				beg->shallow_copy(*this, alpha);
			}
			committee_type::setcount(size_type(std::distance(committee_type::begin(), beg)));
			return 0;
		}
		/** Builds a model for the AnyBoost classifier using weights.
		 *
		 * @param learnset the training set.
		 * @param v a dummy flag.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, IsVoid<w_type> v)
		{
			float_type alpha=1.0;
			iterator beg = committee_type::begin();
			committee_type::normalization(learnset.size());
			const char* msg;
			for(iterator end=committee_type::end();beg != end;++beg)
			{
				if( (msg=committee_type::train(learnset)) != 0 ) return msg;
				alpha = cost_function.update_weights(*this, learnset);
				if( alpha == 0.0 )
				{
					std::cerr << "any_boost stopped early, after only " << std::distance(committee_type::begin(), beg) << " iterations" << std::endl;
					break;
				}
				beg->shallow_copy(*this, alpha);
			}
			committee_type::setcount(size_type(std::distance(committee_type::begin(), beg)));
			return 0;
		}

	protected:
		/** Tests if the AnyBoost has converged or diverged.
		 *
		 * @param beg current example iterator.
		 * @param alpha the error rate.
		 * @return true if AnyBoost should stop early.
		 */
		bool testerror(iterator beg, float_type alpha)
		{
			if( std::abs(alpha-0.0f) < 1e-5 ) //alpha == 0.0f )
			{
				std::cerr << "any_boost converged early: " << alpha << " - " << std::distance(committee_type::begin(),beg) << " < " << committee_type::size() << std::endl;
				return true;
			}
			if( std::abs(alpha-0.5f) < 1e-5 )
			{
				std::cerr << "any_boost diverged early: " << alpha << " - " << std::distance(committee_type::begin(),beg) << " < " << committee_type::size() << std::endl;
				return true;
			}
			return false;
		}
		
	private:
		const char* sample(dataset_type& learnset, subset_type& subset, iterator beg)
		{
			size_type tot=0;
			do{
				instance_wsample(learnset, subset, alphaFlt*learnset.size());
				if( tot > 1000 )
				{
					std::cerr << "WARNING: Too few examples for sampling: " << std::distance(committee_type::begin(), beg) << " " << learnset.begin()->w() << " * " << alphaFlt * learnset.size() << " " << learnset.countClass(0) << " " << learnset.countClass(1) << std::endl;
					if(beg != committee_type::begin()) return ERRORMSG("any_boost: Too few examples for sampling at iteration " << std::distance(committee_type::begin(), beg));
					subset.clear();
					break;
					//std::exit(1);
				}
				tot++;
			} while( subset.hasZeroClass() );
			return 0;
		}
		
	public:
		/** Reads an AnyBoost model from the input stream.
		 *
		 * @param in an input stream.
		 * @param boost a committee.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, any_boost& boost)
		{
			in >> boost.committee();
			boost.iterationInt = boost.size();
			return in;
		}
		/** Writes an AnyBoost model to the output stream.
		 *
		 * @param out an output stream.
		 * @param boost a committee.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const any_boost& boost)
		{
			out << boost.committee();
			return out;
		}

	private:
		size_type iterationInt;
		float alphaFlt;
		argument_type iterationSel;
		cost_type cost_function;
	};
};


#endif


