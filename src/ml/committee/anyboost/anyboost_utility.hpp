/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * anyboost_utility.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_ANYBOOST_UTILITY_HPP
#define _EXEGETE_ANYBOOST_UTILITY_HPP

/** @file anyboost_utility.hpp
 * @brief Holds general functions for boosting
 * 
 * This file contains general functions for boosting.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Basic functions and typedefs for boosting
	 * 
	 * This class defines basic functions and typedefs for boosting.
	 */
	template<class T>
	class anyboost_utility
	{
		typedef T learner_type;
		typedef typename T::weight_type learner_weight;
		typedef typename learner_type::dataset_type subset_type;
	private:
		typedef typename subset_type::attribute_type attribute_type;
		typedef typename subset_type::class_type class_type;
		typedef typename SelectType<is_void::value,double,learner_weight>::Result w_type;
	protected:
		/** Defines a flag testing whether the weak learner does not take weights. **/
		typedef IsVoid<learner_weight> is_void;
		/** Defines the boosting dataset. **/
		typedef ExampleSet<attribute_type, class_type, w_type>	dataset_type;
		/** Defines a float type. **/
		typedef double											float_type;
		
		
	};
};

#endif


