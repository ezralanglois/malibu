/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mil_log_odds_cost_function.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_MIL_LOGODDS_COST_FUNCTION_HPP
#define _EXEGETE_MIL_LOGODDS_COST_FUNCTION_HPP

#include "AnalyticFunction.h"
#include "LocalOptimization1D.h"
#include "NumericalDerivative1D.h"

/** @file mil_log_odds_cost_function.hpp
 * @brief MIL generative log-odds cost function
 * 
 * This file contains the multiple-instance learning generative log-odds cost function
 * for the AnyBoost algorithm.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#define _MIL_LOGODDS_COST_FUNCTION_VERSION 101000

namespace exegete
{
	namespace detail
	{
		/** @brief Gradient of MIL generative log-odd function
		 * 
		 * This class calculates the grade of the MIL generative log-odd function.
		 */
		template<class F, class S> 
		class mil_log_odds_gradient : public LO::Function<F,F>
		{
			typedef F float_type;
			typedef S size_type;
		public:
			/** Setup memory of gradient.
			 * 
			 * @param w weight pointer.
			 * @param e errors pointer.
			 * @param c size of arrays.
			 */
			void setup(float_type* w, float_type* e, size_type c)
			{
				weights = w;
				errors = e;
				bagcnt = c;
			}
			/** Called by line search to evaluate objective function.
			 * 
			 * @param current weight.
			 * @return likelihood given the weight.
			 */
			float_type evaluate(float_type const & w)
			{
				float_type sum = 0.0;
				for(size_type i=0;i<bagcnt;++i)
				{
					sum += weights[i]*errors[i] * std::exp(w*errors[i]);
					ASSERT(!std::isnan(sum));
				}
				return sum;
			}
			
		private:
			float_type* weights;
			float_type* errors;
			size_type bagcnt;
		};
	};
	/** @brief MIL generative log-odd function
	 * 
	 * This class defines the multiple-instance learning generative log-odd cost function.
	 */
	template<class T>
	class mil_log_odds_cost_function : public LO::Function<
										typename T::float_type, 
										typename T::float_type
										>
	{
		typedef T 												learner_type;
		typedef typename learner_type::float_type				float_type;
		typedef typename learner_type::dataset_type 			subset_type;
		typedef typename subset_type::attribute_type 			attribute_type;
		typedef typename subset_type::class_type 				class_type;
		typedef typename learner_type::weight_type 				learner_weight;
		typedef typename subset_type::const_attribute_pointer 	const_attribute_pointer;
		typedef IsVoid<learner_weight> 							is_void;
		typedef typename SelectType<is_void::value,double,learner_weight>::Result w_type;
		typedef ExampleSet<attribute_type, class_type, w_type>	dataset_type;
		typedef typename dataset_type::bag_iterator				bag_iterator;
		typedef typename dataset_type::iterator					iterator;
		typedef typename dataset_type::size_type				size_type;
		typedef detail::mil_log_odds_gradient<float_type, size_type>	gradient_type;
		typedef LO::DLocalOptimization1D<float_type, float_type> optimize_type;
		//typedef LO::LocalOptimization1D<float_type,float_type>  optimize_type;
	public:
		enum{ MIL=1, TUNE=1 };
	
		
	public:
		/** Constructs a cost function.
		 */
		mil_log_odds_cost_function() : optimize(*this, gradient), weights(0), errors(0), bagcnt(0)
		{
		}
		/** Destructs a cost function.
		 */
		~mil_log_odds_cost_function()
		{
			::erase(weights);
			::erase(errors);
		}
		
	public:
		/** Initalizes the cost function and dataset.
		 * 
		 * @param learnset a dataset.
		 */
		void initialize(dataset_type& learnset)
		{
			bagcnt = learnset.bagCount();
			weights = ::resize(weights, learnset.bagCount());
			errors = ::resize(errors, learnset.bagCount());
			setwgts(learnset.bag_begin(), learnset.bag_end(), weights);
			gradient.setup(weights, errors, bagcnt);
		}
		/** Update weights in the dataset. This includes estimating the classifier weight using the line search.
		 * 
		 * @param beg iterator to start of learners.
		 * @param end iterator to end of learners.
		 * @param learner current learner.
		 * @param learnset a dataset.
		 */
		float_type update_weights(const learner_type& learner, dataset_type& learnset)
		{
			if( estimate_error(learner, learnset.bag_begin(), learnset.bag_end(), errors) ) return 0.0;
			float_type wgt = optimize(1).first;
			float_type wsum = update_weights(learnset.bag_begin(), learnset.bag_end(), errors, wgt);
			normwgts(learnset.bag_begin(), learnset.bag_end(), weights, wsum);
			return wgt;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param learner a weak learner.
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		static float_type predict(const learner_type& learner, const_attribute_pointer pred)
		{
			return learner.predict(pred)*learner.weight();
		}
		/** Transforms an instance prediction.
		 * 
		 * @param val an instance prediction.
		 * @return transformed prediction.
		 */
		float_type instance_predict(float_type val)const
		{
			return val;
		}
		/** Transforms an instance prediction.
		 * 
		 * @param val an instance prediction.
		 * @return transformed instance prediction.
		 */
		float_type instance_sum(float_type val)const
		{
			return val;
		}
		/** Transforms a bag prediction.
		 * 
		 * @param val sum of instance predictions.
		 * @return transformed sum of instance predictions.
		 */
		float_type bag_predict(float_type val)const
		{
			return val;
		}
	  
	public:
		/** Calculate the objective function.
		 * 
		 * @param w a classifier weight.
		 * @return objective at w.
		 */
		float_type objective(float_type w)const
		{
			float_type sum = 0.0;
			for(size_type i=0;i<bagcnt;++i)
			{
				sum += weights[i] * std::exp(w*errors[i]);
				ASSERT(!std::isnan(sum));
			}
			return sum;
		}
		
		/** Gets the name of the cost function.
		 * 
		 * @return logodd
		 */
		static std::string str()
		{
			return "logodd";
		}
		/** Gets the prefix of the cost function.
		 * 
		 * @return logodd
		 */
		static std::string prefix()
		{
			return "logodd";
		}
		/** Gets the name of the cost function.
		 * 
		 * @return LogOdds
		 */
		static std::string name()
		{
			return "LogOdds";
		}
		
	protected:
		/** Called by line search to evaluate objective function.
		 * 
		 * @param current weight.
		 * @return likelihood given the weight.
		 */
		float_type evaluate(float_type const & x)
		{
			return objective(x);
		}
		
	private:
		static float_type update_weights(bag_iterator beg, bag_iterator end, float_type* perr, float_type alpha)
		{
			float_type wsum=0.0;
			for(;beg != end;++beg,++perr) 
			{
				beg->w_update( std::exp(*perr)*alpha );
				wsum += beg->w();
			}
			return wsum;
		}
		static bool estimate_error(const learner_type& learner, bag_iterator beg, bag_iterator end, float_type* perr)
		{
			bool noerr=true,allerror=true;
			for(;beg != end;++beg,++perr)
			{
				*perr = bag_error(learner, beg->begin(), beg->end());
		        if(noerr && *perr > 0.5) noerr = false;
		        if(allerror && *perr < 0.5) allerror = false;
		        *perr = 2 * (*perr) - 1;
			}
			return noerr || allerror;
		}
		static float_type bag_error(const learner_type& learner, iterator beg, iterator end)
		{
			float_type w = 1.0 / std::distance(beg, end), sum=0.0;
			for(;beg != end;++beg)
			{
				if( (learner.predict(*beg) > learner.threshold()) != (beg->y()==1) ) sum+=w;
			}
			return sum;
		}
		static void setwgts(bag_iterator beg, bag_iterator end, float_type* pw)
		{
			float_type w = 1.0 / std::distance(beg, end);
			for(;beg != end;++beg,++pw)
			{
				beg->w(w);
				*pw = w;
				setwgts(beg->begin(), beg->end(), w/beg->size());
			}
		}
		static void setwgts(iterator beg, iterator end, float_type w)
		{
			for(;beg != end;++beg) beg->w(w);
		}
		static void normwgts(bag_iterator beg, bag_iterator end, float_type* pw, float_type wbag)
		{
			wbag = 1.0/wbag;
			for(;beg != end;++beg)
			{
				beg->w_update(wbag);
				*pw = beg->w();
				setwgts(beg->begin(), beg->end(), beg->w() / beg->size());
			}
		}
		inline static int sign(float_type y, float_type t)
		{
			return y>t?1:-1;
		}
		
	private:
		optimize_type optimize;
		gradient_type gradient;
		float_type* weights;
		float_type* errors;
		size_type bagcnt;
	};
};


#endif


