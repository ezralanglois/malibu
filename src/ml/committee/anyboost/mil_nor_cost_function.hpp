/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mil_nor_cost_function.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_MIL_NOR_COST_FUNCTION_HPP
#define _EXEGETE_MIL_NOR_COST_FUNCTION_HPP

#include "AnalyticFunction.h"
#include "LocalOptimization1D.h"
#include "NumericalDerivative1D.h"

/** @file mil_nor_cost_function.hpp
 * @brief MIL noisy-Or cost function
 * 
 * This file contains the multiple-instance learning noisy-or cost function
 * for the AnyBoost algorithm.
 *
 * @ingroup ExegeteCommittee
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#define _MIL_NOR_COST_FUNCTION_VERSION 101000

namespace exegete
{
	/** @brief MIL noisy-Or cost function
	 * 
	 * This class defines the multiple-instance learning nosiy-or cost function.
	 */
	template<class T>
	class mil_nor_cost_function : public LO::Function<
									typename T::float_type, 
									typename T::float_type
									>
	{
		typedef T 												learner_type;
		typedef typename learner_type::float_type				float_type;
		typedef typename learner_type::dataset_type 			subset_type;
		typedef typename subset_type::attribute_type 			attribute_type;
		typedef typename subset_type::class_type 				class_type;
		typedef typename learner_type::weight_type 				learner_weight;
		typedef typename subset_type::const_attribute_pointer 	const_attribute_pointer;
		typedef IsVoid<learner_weight> 							is_void;
		typedef typename SelectType<is_void::value,double,learner_weight>::Result w_type;
		typedef ExampleSet<attribute_type, class_type, w_type>	dataset_type;
		typedef typename dataset_type::bag_iterator				bag_iterator;
		typedef typename dataset_type::iterator					iterator;
		typedef typename dataset_type::size_type				size_type;
		typedef LO::LocalOptimization1D<float_type,float_type>  optimize_type;
	public:
		enum{ MIL=1, TUNE=1 };
	
		
	public:
		/** Constructs a cost function.
		 */
		mil_nor_cost_function() : optimize(*this), eps(1e-200), expcnt(0), bagcnt(0), totalcost(0), cost(0), y(0)
		{
		}
		/** Destructs a cost function.
		 */
		~mil_nor_cost_function()
		{
			if( cost ) ::erase(cost[0]);
			::erase(cost);
			if( totalcost ) ::erase(totalcost[0]);
			::erase(totalcost);
			::erase(y);
		}
		
	public:
		/** Initalizes the cost function and dataset.
		 * 
		 * @param learnset a dataset.
		 */
		void initialize(dataset_type& learnset)
		{
			expcnt = learnset.size();
			bagcnt = learnset.bagCount();
			cost = reallocate(cost, learnset.bag_begin(), learnset.bag_end(), learnset.size());
			totalcost = reallocate(totalcost, learnset.bag_begin(), learnset.bag_end(), learnset.size());
			y = reallocate_class(y, learnset.bag_begin(), learnset.bag_end());
			float_type wbag = 1.0 / learnset.bagCount(), w;
			for(bag_iterator beg = learnset.bag_begin(), end=learnset.bag_end();beg != end;++beg)
			{
				w = wbag / beg->size();
				for(iterator ebeg=beg->begin(), eend=beg->end();ebeg != eend;++ebeg)
					ebeg->w(w);
			}
		}
		/** Update weights in the dataset.
		 * 
		 * @param beg iterator to start of learners.
		 * @param end iterator to end of learners.
		 * @param learner current learner.
		 * @param learnset a dataset.
		 */
		float_type update_weights(const learner_type& learner, dataset_type& learnset)
		{
			float_type wgt=0.0f;//, werr=0.0f;
			float_type *pcost = cost[0], *ptotalcost;
			for(iterator ebeg = learnset.begin(), eend=learnset.end();ebeg != eend;++ebeg,++pcost)
			{
				*pcost = sign(learner.predict(*ebeg), learner.threshold());
			}
			
			wgt = optimize(1).first;
			
			if( wgt < 1.0 && (1.0-wgt) <= 1e-4 )
			{
				return 0.0;
			}
	
			pcost = cost[0]; ptotalcost = totalcost[0];
			float_type sum=0.0f;
			for(size_type i=0;i<expcnt;++i) ptotalcost[i]+=pcost[i]*wgt;
			
			float_type** ptc = totalcost;
			sum=0.0f;
			for(bag_iterator beg=learnset.bag_begin(), end=learnset.bag_end();beg != end;++beg, ++ptc)
				sum+=update_bag_weights(beg->begin(), beg->end(), *ptc, beg->y());
			ASSERT(!std::isnan(sum));
			normwgts(learnset.begin(), learnset.end(), sum);
			return wgt;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param learner a weak learner.
		 * @param pred an attribute vector.
		 * @return a real-valued prediction.
		 */
		static float_type predict(const learner_type& learner, const_attribute_pointer pred)
		{
			return learner.predict(pred)*learner.weight();
		}
		/** Transforms an instance prediction. This method transforms the prediction
		 * to a probability using the logistic function.
		 * 
		 * @param val an instance prediction.
		 * @return transformed prediction.
		 */
		float_type instance_predict(float_type val)const
		{
			return logistic_function(val);
		}
		/** Transforms an instance prediction such that it can be summed. This method transforms the 
		 * probablistic prediction with the logarithm for summing.
		 * 
		 * @param val an instance prediction.
		 * @return transformed instance prediction.
		 */
		float_type instance_sum(float_type val)const
		{
			return ln( 1.0-val );
		}
		/** Transforms a bag prediction (sum of instance predictions) to a probability.
		 * 
		 * @param val sum of instance predictions.
		 * @return transformed sum of instance predictions.
		 */
		float_type bag_predict(float_type val)const
		{
			return 1.0 - std::exp(val);
		}
	  
	public:
		/** Calculate the objective function.
		 * 
		 * @param w a classifier weight.
		 * @return objective at w.
		 */
		float_type objective(float_type w)const
		{
			float_type p, sum=0.0;
			for(size_type i=0;i<bagcnt;++i)
			{
				p = noisy_or_objective(cost[i], cost[i+1], totalcost[i], w);
				if( y[i] ) sum-=ln(1.0-std::exp(p));
				else sum-=p;
			}
			return sum;
		}
		/** Gets the prefix of the cost function.
		 * 
		 * @return nor
		 */
		static std::string prefix()
		{
			return "nor";
		}
		/** Gets the name of the cost function.
		 * 
		 * @return Nor
		 */
		static std::string name()
		{
			return "Nor";
		}
		
		
	protected:
		/** Called by line search to evaluate objective function.
		 * 
		 * @param current weight.
		 * @return likelihood given the weight.
		 */
		float_type evaluate(float_type const & x)
		{
			return objective(x);
		}
		
	private:
		static void normwgts(bag_iterator beg, bag_iterator end, float_type wbag)
		{
			wbag = 1.0/wbag;
			for(;beg != end;++beg)
			{
				setwgts(beg->begin(), beg->end(), wbag / beg->size());
			}
		}
		static void setwgts(iterator beg, iterator end, float_type w)
		{
			for(;beg != end;++beg) beg->w_update(w);
		}
		static void normwgts(iterator beg, iterator end, float_type w)
		{
			ASSERT(w != 0.0f);
			w = 1.0/w;
			for(;beg != end;++beg) beg->w_update(w);
		}
		inline static int sign(float_type y, float_type t)
		{
			return y>t?1:-1;
		}
		float_type ln(float_type val)const
		{
			static float_type lneps = std::log(eps);
			ASSERTMSG(val >= 0, val);
			if( val <= eps ) return lneps;
			return std::log(val);
		}
		float_type update_bag_weights(iterator beg, iterator end, float_type* pw, int c)const
		{
			float_type sum=0.0f, val;
			float_type pi = noisy_or(pw, pw+std::distance(beg,end));
			pi = c ? ( (1.0 - pi)/pi ) : 1.0;
			for(;beg != end;++beg,++pw)
			{
				val = pi * logistic_function(*pw);
				ASSERTMSG(beg->y()>0 && c>0 || beg->y()<=0 && c<=0, beg->y() << " == " << c << " at " << std::distance(beg, end));
				beg->w( val );
				sum+=val;
			}
			return sum;
		}
		float_type noisy_or(float_type* beg, float_type* end)const
		{
			float_type sum = 0.0f;
			for(;beg != end;++beg)
			{
				sum += ln( 1.0-logistic_function( *beg ) );
			}
			return 1.0 - std::exp(sum);
		}
		float_type noisy_or_objective(float_type* beg, float_type* end, float_type* tot, float_type w)const
		{
			float_type sum = 0.0f;
			for(;beg != end;++beg,++tot)
			{
				sum += ln( 1.0-logistic_function( (*beg)*w+(*tot) ) );
			}
			return sum;//std::exp(sum);
		}
		static float_type logistic_function(float_type val)
		{
			return 1.0 / ( 1.0 + std::exp(-val) );
		}
		static int* reallocate_class(int* y, bag_iterator beg, bag_iterator end)
		{
			int *py;
			py = y = ::resize(y, std::distance(beg, end));
			for(;beg != end;++beg, ++py) *py = beg->y();
			return y;
		}
		static float_type** reallocate(float_type** ptr, bag_iterator beg, bag_iterator end, size_type n)
		{
			bool init=(ptr==0);
			float_type* old=0;
			if(!init) old = *ptr;
			ptr = ::resize(ptr, r+1);
			*ptr = old;
			*ptr = ::resize(*ptr, n);
			for(unsigned int i=1;beg != end;++beg,++i) ptr[i] = ptr[i-1] + beg->size();
			ptr[r] = ptr[0] + n;
			std::fill(ptr[0], ptr[r], 0.0f);
			return ptr;
		}
		
	private:
		optimize_type optimize;
		float_type eps;
		size_type expcnt;
		size_type bagcnt;
		float_type** totalcost;
		float_type** cost;
		int* y;
	};
};


#endif


