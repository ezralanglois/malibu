/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * output_codes.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_OUTPUT_CODES_HPP
#define _EXEGETE_OUTPUT_CODES_HPP
#include "Committee.h"
#include "WeakLearner.h"
#include "mpi/mpi_def.hpp"

#ifdef _MPI
#include "mpi_learner_node.hpp"
#endif
/** Defines the version of the output codes algorithm. 
 * @todo add to group
 */
#define _OUTPUT_CODES_VERSION 101000

/** @file output_codes.hpp
 * @brief Transforms a binary classifier to a multi-class classifier
 * 
 * This file contains the output codes class template.
 * 
 * @todo ECOC LOSS ay, e^y 1/d = d(x,c) = sum_b=1^l L( M(c,b)f(c,b) )
 * @todo extensive ECOC
 * @todo random ECOC
 * @todo calibration (iterative and non-iterative) controlled by iterations
 *
 * @ingroup ExegeteMultiClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteMultiClassifiers Multi-class Classifiers
 * 
 *  This group holds all the multi-class classifier files.
 */


namespace exegete
{
	/** @brief Transforms a binary classifier to a multi-class classifier
	 * 
	 * This class defines a class to convert binary classifier to a multi-class
	 * classifier.
	 * 
	 * @todo out of bag error for bagging and costing
	 */
	template<class T>
	class output_codes : public Committee< WeakLearner<T> >
	{
		typedef output_codes<T>					this_type;
		typedef T 								learner_type;
		typedef Committee< WeakLearner<T> > 	committee_type;
	public:
		/** Sets argument level. **/
		enum{ ARGUMENT=T::ARGUMENT+1 };

	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines a dataset type. **/
		typedef typename learner_type::dataset_type				dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a float type. **/
		typedef typename learner_type::float_type				float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef typename learner_type::weight_type				weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		//typedef typename learner_type::prediction_vector		prediction_vector;
		typedef int		prediction_vector;
		/** Defines a pointer to a float type as a prediction type. **/
		typedef float_type*										prediction_type;
	private:
		typedef typename dataset_type::attribute_header 		attribute_header;
		typedef TuneParameterTree								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename committee_type::size_type 				size_type;
		typedef typename dataset_type::class_type 				class_type;
		typedef std::vector<class_type> 						class_vector;
		typedef std::vector<float_type> 						float_vector;
		typedef std::vector<class_vector>						class_vector2d;
		typedef class_type*										class_pointer;
		typedef const class_type*								const_class_pointer;
		typedef std::vector<class_pointer> 						code_matrix;
		enum{ OVO, OVA, EECOC, RECOC };
		
	public:
		/** Constructs a default one-versus-all classifier.
		 */
		output_codes() : classcnt(0), algorithmInt(0), predictionInt(0), overridemodelInt(0),
						 algorithmSel(algorithmInt, "OC", range_type(0, 1, 1, '+')),
						 iterationsInt(100), toleranceFlt(1e-4), lossInt(0)
		{
			algorithmSel.add( &learner_type::argument() );
			learner_type::remove_test( learner_type::BINARY );
		}
		/** Destructs an one-versus-all classifier.
		 */
		~output_codes()
		{
			erase_codes(codes.begin(), codes.end());
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(output_codes));
				arginit(map, algorithmSel,		"oc_train", 	"type of multi-class output code>One-v-One:0;One-v-All:1;*:2");
				arginit(map, predictionInt, 	"oc_predict", 	"type of output code prediction>Default:0;Loss:1");
				arginit(map, iterationsInt, 	"oc_iteration", "number of iterations to fit probabilities", ArgumentMap::ADVANCED);
				arginit(map, toleranceFlt, 		"oc_tolerance", "tolerance of probability fitting", ArgumentMap::ADVANCED);
				arginit(map, overridemodelInt, 	"oc_nomodel", 	"use config file to set predict,iteration and tolerance?", ArgumentMap::ADVANCED);
				arginit(map, lossInt,			"oc_loss", 		"output code loss type>Linear:0;Exponential:1", ArgumentMap::ADVANCED);
			}
		}
	public:
		/** Constructs a deep copy of the output codes classifier.
		 *
		 * @param cds a source output code model.
		 */
		output_codes(const output_codes& cds) : committee_type(cds), classcnt(cds.classcnt), algorithmInt(cds.algorithmInt),
												predictionInt(cds.predictionInt), overridemodelInt(cds.overridemodelInt), algorithmSel(algorithmInt, cds.algorithmSel),
												iterationsInt(cds.iterationsInt), toleranceFlt(cds.toleranceFlt), lossInt(cds.lossInt)
		{
			algorithmSel.add( &learner_type::argument() );
		}
		/** Assigns a deep copy of an output code model.
		 *
		 * @param cds a source output codes model.
		 * @return a reference to this object.
		 */
		output_codes& operator=(const output_codes& cds)
		{
			committee_type::operator=(cds);
			argument_copy(cds);
			classcnt = cds.classcnt;
			copy_codes(cds.codes, codes);
			return *this;
		}
		
	public:
		/** Builds a model for the output codes classifier.
		 *
		 * @todo restart backup
		 * @todo test MPI
		 * 
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		//
		//backup restart model??? 
		//how to individualize, same problem with model selection
		//
		//mpi_add_child
		
		const char* train(dataset_type& learnset)
		{
			const char* msg;
			create_code_matrix(codes, learnset.classCount());
			dataset_type subset(learnset, learnset.size(), learnset.bagCount());
			subset.classCount(2);
			committee_type::resize( codes.size() );
			for(size_type i=0,n=codes.size();i<n;++i)
			{
				subset.assign_by_class(learnset, codes[i]);
				ASSERT(subset.size() != 0);
				ASSERT(subset.countClass(0) != 0);
				ASSERT(subset.countClass(1) != 0);
				
				MPI_IF_ROOT
				if( (msg=MPI_POST((*this), new mpi_committee_node<this_type>(*this, subset, i, n ) )) != 0 ) return msg;
				MPI_IF_LEVEL(this_type, (*this))
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				ASSERT(i<committee_type::size());
				committee_type::learnerAt(i).shallow_copy(*this, subset.size());
				MPI_IF_END
				MPI_IF_END
			}
			return 0;
		}
		
		//
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @param parray an array of predictions.
		 */
		void predict(const_attribute_pointer pred, float_type* parray)const
		{
			if( committee_type::threshold() == 0.5f && predictionInt != 1 )
			{
				predict_prob(pred, parray);
			}
			else
			{
				predict_loss(pred, parray);
			}
		}
		MPI_OPTION("OutputCodes")
		
	private:
		void predict_prob(const_attribute_pointer pred, float_type* pe)const
		{
			float_type diff, tmp, tol=toleranceFlt;
			float_vector pt(classcnt);
			float_vector rb(codes.size());
			std::fill(pe, pe+classcnt, 1.0f/classcnt);
			cond_prob(pt, pred);
			unsigned int i, c, b;
			for(i=iterationsInt+1;i>=1;--i)
			{
				diff = 0.0f;
				est_rb(rb, pe);
				for(c=0;c<classcnt;++c)
				{
					tmp=1e-4;
					for(b=0;b<codes.size();++b)
					{
						if( codes[b][c] == 1 )
						{
							tmp += committee_type::learnerAt(b).weight() * rb[b];
						}
						else if(codes[b][c] == -1 )
						{
							tmp += committee_type::learnerAt(b).weight() * (1.0f-rb[b]);
						}
					}
					tmp = pe[c]*pt[c]/tmp;
					diff += (tmp-pe[c])*(tmp-pe[c]);
					pe[c] = tmp;
				}
				tmp=0.0f;
				for(c=0;c<classcnt;++c) tmp += pe[c];
				tmp = 1.0f/tmp;
				for(c=0;c<classcnt;++c) pe[c] *= tmp;
				if( diff < tol ) break;
				if( i == 0 && tol < 1.0f )
				{
					tol *= 10;
					i = iterationsInt+1;
				}
			}
			if( i == 0 ) std::cerr << "WARNING: Multi-class probability failed to converge." << std::endl;
		}
		void est_rb(float_vector& rb, float_type* pe)const
		{
			float_type si, sj;
			for(size_type b=0, c;b<rb.size();++b)
			{
				si = sj = 0.0f;
				for(c=0;c<classcnt;++c)
				{
					if( codes[b][c] == 1) 		si += pe[c];
					else if( codes[b][c] == -1) sj += pe[c];
				}
				if( si == 0.0f ) rb[b] = 0.0f;
				else rb[b] = si / (si+sj);
			}
		}
		void cond_prob(float_vector& rt, const_attribute_pointer pred)const
		{
			float_type rb, val;
			for(unsigned int c=0, b;c<rt.size();++c)
			{
				rb = 0.0f;
				for(b=0;b<codes.size();++b)
				{
					val = committee_type::learnerAt(b).predict(pred);
					if( codes[b][c] == -1 ) val = 1.0f - val;
					else val = 0;
					rb += val*committee_type::learnerAt(b).weight();
				}
				rt[c] = rb;
			}
		}
		
	private:
		void predict_loss(const_attribute_pointer pred, float_type* parray)const
		{
			for(size_type c=0;c<classcnt;++c)
			{
				if(lossInt==0) parray[c] = predict_lin_loss(pred, c);
				else 		   parray[c] = predict_exp_loss(pred, c);
			}
		}
		float_type predict_lin_loss(const_attribute_pointer pred, unsigned int c)const
		{
			float_type l=0.0f, val, off=committee_type::threshold();
			for(unsigned int i=0, n=codes.size();i<n;++i)
			{
				val=committee_type::learnerAt(i).predict(pred)-off;
				l += codes[i][c] * val; //exp
			}
			return l;
		}
		float_type predict_exp_loss(const_attribute_pointer pred, unsigned int c)const
		{
			float_type l=0.0f, val, off=committee_type::threshold();
			for(unsigned int i=0, n=codes.size();i<n;++i)
			{
				val=committee_type::learnerAt(i).predict(pred)-off;
				l += std::exp(codes[i][c] * val); //exp
			}
			return l;
		}

	public:
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("OutputCodes ")+T::class_name();
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Output Codes
		 */
		static std::string name()
		{
			return "Output Codes";
		}
		/** Get the version of the wrapper.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _OUTPUT_CODES_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return oc
		 */
		static std::string prefix()
		{
			return "oc";
		}
		/** Copies arguments in an output code model.
		 * 
		 * @param ref a source output code model.
		 */
		void argument_copy(const output_codes& ref)
		{
			predictionInt = ref.predictionInt;
			algorithmInt = ref.algorithmInt;
			algorithmSel = ref.algorithmSel;
			iterationsInt = ref.iterationsInt;
			toleranceFlt = ref.toleranceFlt;
			overridemodelInt = ref.overridemodelInt;
			lossInt = ref.lossInt;
		}
		/** Makes a shallow copy of an output codes classifier.
		 *
		 * @param ref a reference to an output codes classifier.
		 */
		void shallow_copy(output_codes& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
			classcnt = ref.classcnt;
			erase_codes(codes.begin(), codes.end());
			codes = ref.codes;
			ref.codes.clear();
		}
		/** Get the expected name of the program.
		 * 
		 * @return oc
		 */
		static std::string progname()
		{
			return "oc";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Get the numer of classes.
		 * 
		 * @return number of classes
		 */
		unsigned int classCount()const
		{
			return classcnt;
		}
		
	private:
		void create_code_matrix(code_matrix& mat, size_type c)
		{
			size_type l;
			classcnt = c;
			if( algorithmInt == OVO )
			{
				l = c*(c-1)/2;
				init_code_matrix(mat, c, l, 0);
				for(size_type i=0,k=0,j;i<c;++i)
				{
					for(j=i+1;j<c;++j,++k)
					{
						mat[k][i] =  1;
						mat[k][j] = -1;
					}
				}
			}
			else if( algorithmInt == OVA )
			{
				l = c;
				init_code_matrix(mat, c, l, -1);
				for(size_type i=0;i<c;++i)
				{
					mat[i][i] = 1;
				}
			}
			else ASSERT(false);
		}
		void init_code_matrix(code_matrix& mat, size_type c, size_type l, class_type cl)
		{
			erase_codes(mat.begin(), mat.end());
			size_type n = l*c;
			mat.resize(l);
			mat[0] = ::setsize(mat[0], n);
			std::fill(mat[0], mat[0]+n, cl);
			for(unsigned int i=1;i<l;++i)
			{
				mat[i] = mat[0] + (c*i);
			}
		}
		static void copy_codes(const code_matrix& from, code_matrix& to, unsigned int cl)
		{
			erase_codes(to.begin(), to.end());
			to.resize(from.size());
			for(unsigned int i=0, j;i<from.size();++i)
			{
				to[i] = ::setsize(to[i], cl);
				for(j=0;j<cl;++j) to[i][j] = from[i][j];
			}
		}
		static void erase_codes(typename code_matrix::iterator beg, typename code_matrix::iterator end)
		{
			if(beg != end) ::erase(*beg);
		}
		
	private:
		template<class U>
		static const char* read_param(std::istream& in, U& val, const char* msg, char tst='\t')
		{
			in >> val;
			if( in.get() != tst ) 
			{
				if( tst == '\t' ) return ERRORMSG("Failed reading tab after " << msg);
				else if( tst == '\n' ) return ERRORMSG("Failed reading newline after " << msg);
				else return ERRORMSG("Failed reading character after " << msg);
			}
			return 0;
		}
		/** Reads an output codes committee from the input stream.
		 *
		 * @param in a reference to an input stream.
		 * @param cds a reference to a committee.
		 * @return a reference to an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, output_codes& cds)
		{
			const char* msg;
			size_type l;
			if( cds.overridemodelInt )
			{
				int lossInt;
				int predictionInt;
				unsigned int iterationsInt;
				float_type toleranceFlt;
				if( (msg=read_param(in, lossInt, "loss type")) != 0 ) return cds.fail(in, msg);
				if( (msg=read_param(in, iterationsInt, "iterations")) != 0 ) return cds.fail(in, msg);
				if( (msg=read_param(in, toleranceFlt, "tolerance")) != 0 ) return cds.fail(in, msg);
				if( (msg=read_param(in, predictionInt, "prediction type", '\n')) != 0 ) return cds.fail(in, msg);
			}
			else
			{
				if( (msg=read_param(in, cds.lossInt, "loss type")) != 0 ) return cds.fail(in, msg);
				if( (msg=read_param(in, cds.iterationsInt, "iterations")) != 0 ) return cds.fail(in, msg);
				if( (msg=read_param(in, cds.toleranceFlt, "tolerance")) != 0 ) return cds.fail(in, msg);
				if( (msg=read_param(in, cds.predictionInt, "prediction type", '\n')) != 0 ) return cds.fail(in, msg);
			}
			if( (msg=read_param(in, cds.classcnt, "number of classes")) != 0 ) return cds.fail(in, msg);
			if( (msg=read_param(in, cds.algorithmInt, "algorithm type")) != 0 ) return cds.fail(in, msg);
			if( (msg=read_param(in, l, "number of codes")) != 0 ) return cds.fail(in, msg);
			if( (msg=read_param(in, cds.predictionInt, "prediction type", '\n')) != 0 ) return cds.fail(in, msg);
			if( cds.algorithmInt > OVA )
			{
				cds.init_code_matrix(cds.codes, cds.classcnt, l, 0);
				class_pointer beg, end;
				for(typename code_matrix::iterator mbeg = cds.codes.begin(), mend = cds.codes.end();mbeg != mend;++mbeg)
				{
					in >> **mbeg;
					for(beg=(*mbeg)+1, end=beg+cds.classcnt;beg != end;++beg)
					{
						if( in.get() != ' ' )
						{
							cds.errormsg(in, "Missing space character in output codes model");
							return in;
						}
						in >> *beg;
					}
					if( in.get() != '\n' )
					{
						cds.errormsg(in, "Missing newline character in output codes model");
						return in;
					}
				}
				
			}
			else cds.create_code_matrix(cds.codes, cds.classcnt);
			in >> cds.committee();
			return in;
		}
		/** Writes an output codes committee to the output stream.
		 *
		 * @param out a reference to an output stream.
		 * @param cds a reference to a committee.
		 * @return a reference to an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const output_codes& cds)
		{
			out << cds.lossInt << "\t";
			out << cds.iterationsInt << "\t";
			out << cds.toleranceFlt << "\t";
			out << cds.predictionInt << "\n";
			out << cds.classcnt << "\t";
			out << cds.algorithmInt << "\t";
			out << cds.codes.size() << "\n";
			if( cds.algorithmInt > OVA )
			{
				const_class_pointer beg, end;
				for(typename code_matrix::const_iterator mbeg = cds.codes.begin(), mend = cds.codes.end();mbeg != mend;++mbeg)
				{
					out << **mbeg;
					for(beg=(*mbeg)+1, end=beg+cds.classcnt;beg != end;++beg)
						out << " " << *beg;
					out << "\n";
				}
			}
			out << cds.committee();
			return out;
		}
		
	private:
		code_matrix codes;
		unsigned int classcnt;
		int algorithmInt;
		int predictionInt;
		int overridemodelInt;
		argument_type algorithmSel;
		
	private:
		unsigned int iterationsInt;
		float_type toleranceFlt;
		int lossInt;
	};
};


/** @brief Type utility helper specialized for output codes
 * 
 * This class defines a type utility helper specialized for output codes.
 */
template<class T>
struct TypeTrait < ::exegete::error_correctingoutput_codes<T> >
{
	/** Defines a value type. **/
	typedef ::exegete::output_codes<T> value_type;
};

#endif

