/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * filter_tree.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_FILTER_TREE_HPP
#define _EXEGETE_FILTER_TREE_HPP
#include "Committee.h"
#include "WeakLearner.h"
#include "mpi/mpi_def.hpp"

#ifdef _MPI
#include "mpi_learner_node.hpp"
#endif
/** Defines the version of the filter tree algorithm. 
 * @todo add to group
 */
#define _FILTER_TREE_VERSION 101000

/** @file filter_tree.hpp
 * @brief Transforms a importance-weighted binary classifier to a cost-sensitive multi-class classifier
 * 
 * This file contains the filter tree class template.
 *
 * @ingroup ExegeteMultiClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteMultiClassifiers Multi-class Classifiers
 * 
 *  This group holds all the multi-class classifier files.
 */


namespace exegete
{
	/** @brief Transforms a binary classifier to a multi-class classifier
	 * 
	 * This class defines a class to convert binary classifier to a multi-class
	 * classifier.
	 * 
	 * @todo out of bag error for bagging and costing
	 * @todo add variants
	 */
	template<class T>
	class filter_tree : public Committee< WeakLearner<T, std::pair<int,int> > >
	{
		typedef filter_tree<T>									this_type;
		typedef T 												learner_type;
		typedef Committee< WeakLearner<T, std::pair<int,int> > > committee_type;
		typedef typename learner_type::dataset_type				subset_type;
		typedef typename learner_type::weight_type  			w_type;
		typedef typename subset_type::attribute_type  			attribute_type;
		typedef typename subset_type::class_type  				class_type;
	public:
		/** Sets argument level. **/
		enum{ ARGUMENT=T::ARGUMENT+1 };

	public:
		/** Defines a parent type. **/
		typedef T 												parent_type;
		/** Defines a dataset type. **/
		typedef ExampleSet<attribute_type, class_type, w_type*>	dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a float type. **/
		typedef typename learner_type::float_type				float_type;
		/** Defines a tunable argument iterator. **/
		typedef Learner::argument_iterator						argument_iterator;
		/** Defines a weight type. **/
		typedef w_type*											weight_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		//typedef typename learner_type::prediction_vector		prediction_vector;
		typedef int												prediction_vector;
		/** Defines a pointer to a float type as a prediction type. **/
		typedef float_type*										prediction_type;
		
	private:
		typedef TuneParameterTree 								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef typename committee_type::size_type 				size_type;
		
	public:
		/** Constructs a default filter tree classifier.
		 */
		filter_tree() : classcnt(0), algorithmInt(0),
						 algorithmSel(algorithmInt, "FT", range_type(0, 1, 1, '+'))
		{
			algorithmSel.add( &learner_type::argument() );
			learner_type::remove_test( learner_type::BINARY );
		}
		/** Destructs an filter tree classifier.
		 */
		~filter_tree()
		{
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(filter_tree));
				arginit(map, algorithmSel,		"ft_train", 	"dummy parameter that will control variant>Basic:0");
			}
		}
	public:
		/** Constructs a deep copy of the filter tree classifier.
		 *
		 * @param ft a source output code model.
		 */
		filter_tree(const filter_tree& ft) : committee_type(ft), classcnt(ft.classcnt), algorithmInt(ft.algorithmInt), algorithmSel(algorithmInt, ft.algorithmSel)
		{
			algorithmSel.add( &learner_type::argument() );
		}
		/** Assigns a deep copy of an output code model.
		 *
		 * @param ft a source filter tree model.
		 * @return a reference to this object.
		 */
		filter_tree& operator=(const filter_tree& ft)
		{
			committee_type::operator=(ft);
			argument_copy(ft);
			classcnt = ft.classcnt;
			//
			return *this;
		}
		
	public:
		/** Builds a model for the filter tree classifier.
		 *
		 * @todo restart backup
		 * @todo MPI
		 * 
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */		
		const char* train(dataset_type& learnset)
		{
			const char* msg;
			ASSERT(learnset.size()>0);
			subset_type subset(learnset, learnset.size(), learnset.bagCount());
			subset.classCount(2);
			classcnt = learnset.classCount();
			internalcnt = internal_node_count(classcnt);
			if( classcnt < 3 ) return ERRORMSG("Cannot have less than 3 classes");
			committee_type::resize( node_count(classcnt) );
			setup_classes(classes, classcnt, internalcnt);
			for(size_type j=0,n=committee_type::size(), i;j<n;++j)
			{
				i = n - j - 1;
				setup_iwdataset(learnset, subset, committee_type::learnerAt(i).weight().first, committee_type::learnerAt(i).weight().second);
				ASSERT(subset.size() > 0);
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				committee_type::learnerAt(i).shallow_copy(*this);
				/*MPI_IF_ROOT
				if( (msg=MPI_POST((*this), new mpi_committee_node<this_type>(*this, subset, i, n ) )) != 0 ) return msg;
				MPI_IF_LEVEL(this_type, (*this))
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				ASSERT(i<committee_type::size());
				committee_type::learnerAt(i).shallow_copy(*this, subset.size());
				MPI_IF_END
				MPI_IF_END*/
			}
			return 0;
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * @param pred an attribute vector.
		 * @param parray an array of predictions.
		 */
		void predict(const_attribute_pointer pred, float_type* parray, float_type* pend=0)const
		{
			size_type c = 0;
			if( pend == 0 ) pend=parray+classcnt;
			while( c < node_count(classcnt) )
			{
				c = committee_type::learnerAt(c).predict_index(pred);
			}
			for(size_type n=0;n<classcnt;++n) 
			{
				parray[n] = 0;
			}
			parray[c-node_count(classcnt)] = 1;
		}
		MPI_OPTION("FilterTree")

	public:
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("filter_tree ")+T::class_name();
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return filter tree
		 */
		static std::string name()
		{
			return "filter tree";
		}
		/** Get the version of the wrapper.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _FILTER_TREE_VERSION;
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return oc
		 */
		static std::string prefix()
		{
			return "ft";
		}
		/** Copies arguments in an output code model.
		 * 
		 * @param ref a source output code model.
		 */
		void argument_copy(const filter_tree& ref)
		{
			algorithmInt = ref.algorithmInt;
			algorithmSel = ref.algorithmSel;
		}
		/** Makes a shallow copy of an filter tree classifier.
		 *
		 * @param ref a reference to an filter tree classifier.
		 */
		void shallow_copy(filter_tree& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
			classcnt = ref.classcnt;
		}
		/** Get the expected name of the program.
		 * 
		 * @return oc
		 */
		static std::string progname()
		{
			return "ft";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Get the numer of classes.
		 * 
		 * @return number of classes
		 */
		unsigned int classCount()const
		{
			return classcnt;
		}
		
	protected:
		/** Setup the importance-weighted dataset.
		 * 
		 * @param learnset multi-class dataset.
		 * @param learnset binary importance-weighted dataset.
		 * @param class index 1.
		 * @param class index 2.
		 */
		void setup_iwdataset(dataset_type& learnset, subset_type& subset, int c1, int c2)
		{
			w_type wgt1, wgt2;
			typename dataset_type::const_iterator beg = learnset.begin();
			typename dataset_type::const_iterator end = learnset.end();
			typename subset_type::iterator it;
			size_type ncnt = node_count(classcnt);
			int i = c1 - ncnt, j = c2 - ncnt;

			subset.resize( learnset.size() );
			it = subset.begin();
			for(;beg != end;++beg)
			{
				if( size_type(c1) < ncnt ) 
				{
					ASSERT(size_type(c1) < ncnt );
					i = committee_type::learnerAt(c1).predict_index(*beg)-ncnt;
				}
				if( size_type(c2) < ncnt ) 
				{
					ASSERT(size_type(c2) < ncnt );
					j = committee_type::learnerAt(c2).predict_index(*beg)-ncnt;
				}
				ASSERTMSG(size_type(i) < classcnt, size_type(i) << " < " << classcnt );
				ASSERTMSG(size_type(j) < classcnt, size_type(j) << " < " << classcnt );
				ASSERT(i != j);
				wgt1 = beg->w_pointer()[i];
				wgt2 = beg->w_pointer()[j];
				if( std::abs(wgt1-wgt2) < 1e-6 ) continue;
				*it = *beg;
				it->y( wgt1 < wgt2 );
				it->w( std::abs(wgt1-wgt2) );
				++it;
			}
			subset.resize( std::distance(subset.begin(), it) );
			ASSERT(!subset.hasZeroClass());
		}
		
	private:
		static size_type node_count(size_type classcnt)
		{
			return classcnt - 1;
		}
		static size_type internal_node_count(size_type classcnt)
		{
			return size_type(ceil(classcnt/2.0))-1;
		}
		static bool isodd(size_type classcnt)
		{
			return (classcnt&1) == 1;
		}
		void setup_classes(std::vector<int>& classes, size_type classcnt, size_type internalcnt)
		{
			size_type ncnt = node_count(classcnt);
			if( classes.size() != classcnt )
			{
				classes.resize(classcnt);
				for(size_type i=0;i<classcnt;++i) classes[i] = i;
			}
			shuffle(classes.begin(), classes.end());
			for(size_type i=0, c=0;i<committee_type::size();++i)
			{
				if( i < internalcnt )
				{
					if( isodd(classcnt) && i==(internalcnt-1) )
					{
						ASSERT( (2*i+1) < ncnt);
						committee_type::learnerAt(i).weight( std::make_pair(2*i+1, ncnt+classes[c]) );
						++c;
					}
					else
					{
						ASSERT( (2*i+2) < ncnt);
						committee_type::learnerAt(i).weight( std::make_pair(2*i+1, 2*i+2) );
					}
				}
				else
				{
					ASSERT((c+1) < classes.size());
					ASSERT( (ncnt+classes[c]) != (ncnt+classes[c+1]) );
					committee_type::learnerAt(i).weight( std::make_pair(ncnt+classes[c], ncnt+classes[c+1]) );
					c+=2;
				}
			}
		}
		
	private:
		/** Reads an filter tree committee from the input stream.
		 *
		 * @param in a reference to an input stream.
		 * @param ft a reference to a committee.
		 * @return a reference to an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, filter_tree& ft)
		{
			in >> ft.classcnt;
			if( in.eof() || in.get() != '\t' ) return ft.fail(in, "Missing tab character in Filter Tree model");
			in >> ft.committee();
			ft.internalcnt = internal_node_count(ft.classcnt);
			return in;
		}
		/** Writes an filter tree committee to the output stream.
		 *
		 * @param out a reference to an output stream.
		 * @param ft a reference to a committee.
		 * @return a reference to an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const filter_tree& ft)
		{
			out << ft.classcnt << "\t";
			out << ft.committee();
			return out;
		}
		
	private:
		size_type classcnt;
		size_type internalcnt;
		
	private:
		int algorithmInt;
		argument_type algorithmSel;
		
	private:
		std::vector<int> classes;
	};
};


/** @brief Type utility helper specialized for filter tree
 * 
 * This class defines a type utility helper specialized for filter tree.
 */
template<class T>
struct TypeTrait < ::exegete::filter_tree<T> >
{
	/** Defines a value type. **/
	typedef ::exegete::filter_tree<T> value_type;
};

#endif

