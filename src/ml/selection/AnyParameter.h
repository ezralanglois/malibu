/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AnyParameter.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ANYPARAMETER_H
#define _EXEGETE_ANYPARAMETER_H

/** @file AnyParameter.h
 * @brief Set of classes to handle any parameter type.
 * 
 * This file contains a set of classes to handle any parameter types.
 *
 * @ingroup ExegeteModelSelection
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteModelSelection Model Selection
 * 
 *  This group holds all model selection files.
 */


namespace exegete
{
	/** @brief Abstract interface to any parameter
	 * 
	 * This class provides an interface for setting a parameter value.
	 */
	class AbstractAnyParameter
	{
	public:
		/** Resets the current value to the start of the range.
		 *
		 * @param val source value.
		 * @return a reference to this class.
		 */
		AbstractAnyParameter& operator=(float val)
		{
			set(val);
			return *this;
		}
		/** Destructs abstract any parameter.
		 */
		virtual ~AbstractAnyParameter()
		{
		}
		/** Gets the value for a parameter.
		 * 
		 * @return float value.
		 */
		operator float()const
		{
			return get();
		}
		/** Sets the value for a parameter.
		 *
		 * @param val source value.
		 */
		virtual void set(float val)=0;
		/** Gets the value for a parameter.
		 * 
		 * @return float value.
		 */
		virtual float get()const=0;
		/** Makes a copy of an any parameter.
		 * 
		 * @return pointer to new any parameter.
		 */
		virtual AbstractAnyParameter* clone()const=0;
	};
	/** @brief Concrete implementation of any parameter
	 * 
	 * This class template maintiains a pointer to a parameter of any type.
	 */
	template<class T>
	class AnyParameter : public AbstractAnyParameter
	{
	public:
		/** Constructs an AnyParameter with a reference.
		 *
		 * @param ref a reference to the parameter value.
		 */
		AnyParameter(T& ref) : ptr(&ref)
		{
		}
		/** Constructs a AnyParameter with a pointer.
		 *
		 * @param p a pointer to a value to change.
		 */
		AnyParameter(T* p) : ptr(p)
		{
		}
		
	public:
		/** Sets the value of the pointed to value.
		 *
		 * @param val a source value.
		 */
		void set(float val)
		{
			*ptr = T(val);
		}
		/** Gets the value for a parameter.
		 * 
		 * @return a float value.
		 */
		float get()const
		{
			return float(*ptr);
		}
		/** Makes a copy of an any parameter.
		 * 
		 * @return pointer to new any parameter.
		 */
		AbstractAnyParameter* clone()const
		{
			return new AnyParameter<T>(ptr);
		}
		
	private:
		T* ptr;
	};
};

#endif


