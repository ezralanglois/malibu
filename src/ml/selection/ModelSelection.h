/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ModelSelection.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_MODELSELECTION_H
#define _EXEGETE_MODELSELECTION_H
#include "ModelValidation.h"
#include "SelectCommittee.h"
#include <iostream>

/** @file ModelSelection.h
 * @brief Interface to model selection classes
 * 
 * This file contains the ModelSelection class.
 *
 * @ingroup ExegeteModelSelection
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @page arguments List of Arguments
 *
 *  @section Selection Model Selection
 *	- tune_print: print something while tuning>None:0;Best:1;All:2
 *		-# None: print nothing (default)
 *		-# Best: print best parameters and metric
 *		-# All: print every parameter and metric
 *	- tune_file: print to file, if empty print to console
 */

namespace exegete
{

	/** @brief Interface to model selection
	 * 
	 * This class template performs argument selection on a learning algorithm.
	 */
	template<class L, class M>
	class ModelSelection : public SelectCommittee<L, M>
	{
		typedef ModelSelection<L,M> this_type;
		typedef SelectCommittee<L,M> select_type;
		typedef L learner_type;
		typedef M measure_type;
	public:
		/** Defines a parent type. **/
		typedef select_type 								parent_type;
		/** Defines a learner learnset as a learnset type. **/
		typedef typename learner_type::dataset_type 		dataset_type;
		/** Defines a learner learnset as a learnset type. **/
		typedef typename learner_type::testset_type 		testset_type;
		/** Defines a learner argument iterator as an argument iterator. **/
		typedef typename learner_type::argument_iterator 	argument_iterator;
		/** Defines a prediction vector, algorithm is not self validating. **/
		typedef int 										prediction_vector;
		/** Number of arguments. **/
		enum{ARGUMENT=select_type::ARGUMENT, SELECT=1};

	public:
		/** Constructs a model selection object.
		 */
		ModelSelection()
		{
		}
		/** Constructs a model selection object with an ArgumentMap.
		 *
		 * @param map an ArgumentMap to append arguments.
		 * @param t a parameter flag.
		 */
		ModelSelection(ArgumentMap& map, int t=0)
		{
			init(map, t);
		}
		/** Destructs the model selection object.
		 */
		~ModelSelection()
		{
		}

	public:
		/** Copy arguments in model selection.
		 * 
		 * @note does nothing, for compatibility
		 * 
		 * @param com a model selection object.
		 */
		void argument_copy(const ModelSelection& com)
		{
		}
		/** Initalize parameters for a model selection.
		 * 
		 * @param map an argument map.
		 * @param t a parameter flag.
		 */
		template<class U>
		void init(U& map, int t=0)
		{
			select_type::init(map, t);
		}
		/** Selects the best parameters using some validation scheme and then
		 * trains the learning algorithm with these parameters.
		 *
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
			const char* msg;
			MPI_IF_ROOT
			MPI_ALTER_PARENT( (*this), MPI_LEVEL(this_type), MPI_LEVEL(learner_type) );
			MPI_IF_END
			argument_iterator arg = learner_type::argument();
			if( !arg.empty() )
			{
				arg.reset(select_type::fastTune() || learner_type::FAST_TUNE==0);
			}
			if( select_type::is_tune() )
			{
				if( (msg=select_type::initialize(arg)) != 0 ) return msg;
				//load
				for(;arg.hasNext();++arg)
				{
					MPI_IF_ROOT
#ifdef _MPI
					bool islast = arg.is_last();
					typedef mpi_select_node<select_type, M, detail::SelectionPolicy> select_node;
					if( (msg=MPI_POST((*this), new select_node(*this, learnset, MPI_LEVEL(select_type), arg.is_last() ) )) != 0 ) return msg;
					MPI_IF_LEVEL(select_type, (*this) );
					MPI_ALTER_PARENT( (*this), MPI_LEVEL(select_type), 0 );
					if( (msg=select_type::train(learnset)) != 0 ) return msg;
					MPI_IF_END
					if(islast) break;
#endif
					MPI_IF_ELSE
					if( (msg=select_type::train(learnset)) != 0 ) return msg;
					MPI_IF_END
				}
				MPI_IF_NOT_ROOT
				select_type::finalize();
				MPI_IF_END
			}
			MPI_IF_NOT_ROOT
			if( (!select_type::is_tune() || !select_type::istesting() ) && (msg=learner_type::train(learnset)) != 0 ) return msg;
			MPI_IF_END
			return 0;
		}
		MPI_OPTION("Select")
		/** Get the expected name of the program.
		 * 
		 * @return select
		 */
		static std::string progname()
		{
			return "select";
		}
		/** Get the name.
		 * 
		 * @return ""
		 */
		static std::string name()
		{
			return "";
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			learner_type::accept(visitor);
		}
		/** Get the class name of the learner.
		 * 
		 * @return a class name.
		 */
		static std::string class_name()
		{
			return std::string("ModelSelection ")+L::class_name();
		}
#ifdef _MPI
	public:
		/** Finalize MPI model selection.
		 */
		void mpi_finalize()
		{
			select_type::finalize();
		}
		
	private:
		/** Read a model selection from an input stream.
		 * 
		 * @param in an input stream.
		 * @param ms a model selection.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, ModelSelection& ms)
		{
			in >> ms.parent();
			return in;
		}
		/** Write a model selection to the output stream.
		 * 
		 * @param out an output stream.
		 * @param ms a model selection.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const ModelSelection& ms)
		{
			out << ms.parent();
			return out;
		}
		/** Gets a parent object in learner hierarchy.
		 * 
		 * @return a parent object.
		 */
		learner_type& parent()
		{
			return *this;
		}
		/** Gets a parent object in learner hierarchy.
		 * 
		 * @return a parent object.
		 */
		const learner_type& parent()const
		{
			return *this;
		}
#endif
	};
	/** @brief Selects the parent classifier as a base learner
	 * 
	 * This class selects the parent classifier as a base learner.
	 */
	template<class T, class M>
	struct base_learner< ModelSelection<T, M> >
	{
		/** Defines base learner as a result. **/
		typedef typename base_learner<T>::result result;
	};
};

/** @brief Type utility interface to a model selection algorithm
 * 
 * This class defines a type utility interface to a model selection algorithm.
 */
template<class T, class M>
struct TypeUtil< ::exegete::ModelSelection<T,M> >
{
	/** Flags a model selection as non-primative. */
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::ModelSelection<T,M> value_type;
	/** Tests if a string can be converted to a model selection.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the class type.
	 *
	 * @return ModelSelection
	 */
	static const char* name() 
	{
		return "ModelSelection";
	}
};

#endif


