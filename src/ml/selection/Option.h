/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Option.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_OPTION_H
#define _EXEGETE_OPTION_H
#include "debugutil.h"
#include <string>
#include <list>

/** @file Option.h
 * @brief Contains classes related to a set of options
 * 
 * This file contains a set of classes that provide general program argument 
 * functionality. 
 *
 * @ingroup ExegeteModelSelection
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief String with an associated code
	 * 
	 * This class defines a string representation with an associated code.
	 */
	class Option : public std::string
	{
	public:
		/** Constructs a default option. 
		 *
		 * @param ch a character code.
		 */
		Option(char ch=0) : cde(ch)
		{
		}
		/** Destructs an option. 
		 */
		~Option()
		{
		}

	public:
		/** Assigns a copy of a string.
		 * 
		 * @param str a string.
		 * @return a reference to this object.
		 */
		Option& operator=(const std::string& str)
		{
			std::string::operator=(str);
			return *this;
		}

	public:
		/** Gets a code for the option.
		 *
		 * @return a character code.
		 */
		char code()const
		{
			return cde;
		}
		/** Sets a code for the option.
		 *
		 * @param c a character code.
		 */
		void code(char c)
		{
			cde = c;
		}

	private:
		char cde;
	};

	
	/** @brief Set of string, character options.
	 * 
	 * This class defines a list of string, character options.
	 */
	class OptionSet : public std::list<Option>
	{
		typedef std::list<Option> collection_type;
	public:
		/** Constructs a default option set.
		 */
		OptionSet()
		{
		}
		/** Destructs an option set.
		 */
		~OptionSet()
		{
		}

	public:
		/** Sets the code for this option.
		 * 
		 * @param ch an option code.
		 * @param val a string representation.
		 * @return a string.
		 */
		std::string& operator()(char ch, const std::string& val)
		{
			collection_type::push_back(Option(ch));
			collection_type::back() = val;
			return collection_type::back();
		}
	};
};


#endif


