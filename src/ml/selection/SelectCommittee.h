/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * SelectCommittee.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_SELECTCOMMITTEE_H
#define _EXEGETE_SELECTCOMMITTEE_H
#include "Committee.h"
#include "TestableLearner.h"
/** Defines the version of the model selection algorithm. 
 * @todo add to group
 */
#define _MODELSELECTION_VERSION 101000

/** @file SelectCommittee.h
 * @brief Contains classes for model selection 
 * 
 * This file contains the SelectCommittee class template.
 *
 * @ingroup ExegeteModelSelection
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail 
	{
		/** @brief Policy for training a classifier and saving its model
		 * 
		 * This class defines a policy for training a classifier and saving its model.
		 */
		template<class T>
		struct SelectionPolicy
		{
			/** Defines a learner type. **/
			typedef typename T::parent_type learner_type;
			/** Trains a learner over the given dataset.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a training set.
			 * @param predictions vector to save predictions.
			 * @return an error message or NULL.
			 */
			template<class U>
			static const char* train(learner_type& learner, typename learner_type::dataset_type& dataset, std::vector<U>& predictions)
			{
				return learner.train(dataset, predictions);
			}
			/** Trains a learner on the given dataset.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a training set.
			 * @return an error message or NULL.
			 */
			static const char* train(learner_type& learner, typename learner_type::dataset_type& dataset)
			{
				return learner.train(dataset);
			}
			/** Trains a learner on the given dataset saves the test set.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a training set.
			 * @param testset a testing set.
			 * @return an error message or NULL.
			 */
			static const char* train(learner_type& learner, typename learner_type::dataset_type& dataset, typename T::testset_type& testset)
			{
				return learner.train(dataset);
			}
			/** Save a learning model and testset.
			 * 
			 * @param learner a learning algorithm.
			 * @param testset a testing set.
			 */
			static void save(T& learner, typename T::testset_type& testset)
			{
				learner.save_next(testset);
			}
			/** Resize the number of learners to save.
			 * 
			 * @param learner a committee of learners.
			 * @param n number of learners.
			 */
			static void resize_saved(T& learner, unsigned int n)
			{
				if(n>0) learner.resize_saved(n);
			}
		};
	};

	/** @brief Defines a model selection committee
	 * 
	 * This class template defines a model selection committee.
	 */
	template<class T, class M>
	class SelectCommittee : public T
	{
		typedef SelectCommittee<T,M> select_type;
		typedef M measure_type;
	public:
		/** Defines a parent type. **/
		typedef T parent_type;
		/** Defines a saved type. **/
		typedef TestableLearner<T> saved_type;
		/** Defines a pointer to a learner. **/
		typedef saved_type* saved_pointer;
		/** Defines an iterator to a learner. **/
		typedef saved_type* saved_iterator;
		/** Defines a constant pointer to a learner. **/
		typedef const saved_type* const_saved_pointer;
		/** Defines a constant iterator to a learner. **/
		typedef const saved_type* const_saved_iterator;
		/** Defines a size type. **/
		typedef size_t size_type;
		/** Defines a prediction vector. **/
		typedef typename T::prediction_vector prediction_vector;
		/** Defines a dataset type. **/
		typedef typename parent_type::dataset_type dataset_type;
		/** Defines a testset type. **/
		typedef typename parent_type::testset_type testset_type;
		/** Defines an argument iterator. **/
		typedef typename T::argument_iterator argument_iterator;
		/** Defines an argument level. **/
		enum{ARGUMENT=parent_type::ARGUMENT+1};

	private:
		typedef ModelValidation<select_type, M, dataset_type, detail::SelectionPolicy> validation_type;
		enum{NONE,BEST,ALL};
	
	public:
		/** Constructs a selection committee.
		 */
		SelectCommittee() : maxm(-DBL_MAX), typeInt(NONE), pout(&std::cout), prestartOut(0), learners(0), count(0), current(0), fastTuneInt(T::FAST_TUNE), restartBool(0)
		{
		}
		/** Destructs a selection committee.
		 */
		~SelectCommittee()
		{
			delete[] learners;
			if( pout != &std::cout ) delete pout;
			if( prestartOut != 0 ) delete prestartOut;
		}

	public:
		/** Constructs a committee of selection algorithms.
		 *
		 * @param com a selection committee.
		 */
		SelectCommittee(const SelectCommittee& com) : parent_type(com), validation(com.validation), measure(com.measure), saved(com.saved), maxm(com.maxm), typeInt(com.pout), fileStr(com.fileStr), pout(&std::cout),
				learners(0), count(0), current(0), fastTuneInt(com.fastTuneInt), restartBool(com.restartBool)
		{
		}
		/** Assigns a committee of learning algorithms.
		 *
		 * @param com a committee.
		 * @return a reference to this object.
		 */
		SelectCommittee& operator=(const SelectCommittee& com)
		{
			parent_type::operator=(com);
			argument_copy(com);
			return *this;
		}
		
	public:
		/** Copies arguments in a selection committee.
		 * 
		 * @param com a committee.
		 */
		void argument_copy(const SelectCommittee& com)
		{
			fastTuneInt = com.fastTuneInt;
			validation = com.validation;
			measure = com.measure;
			saved = com.saved;
			maxm = com.maxm;
			typeInt = com.typeInt;
			fileStr = com.fileStr;
			restartBool = com.restartBool;
		}
		/** Get the restart flag.
		 * 
		 * @return a restart flag.
		 */
		int restart()const
		{
			return restartBool;
		}
		/** Set the restart flag.
		 * 
		 * @param r restart flag.
		 * @return an error message or NULL.
		 */
		const char* set_restart(int r)
		{
			const char* msg;
			restartBool = r;
			validation.restart(r);
			if( (msg=validation.check_restart("sel")) != 0 ) return msg;
			// todo: check metric param file
			return T::set_restart(r);
		}
		/** Train a learning algorithm on the given dataset and save
		 * predictions to a prediction vector.
		 * 
		 * @param learnset a training set.
		 * @param vec a prediction vector.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset, prediction_vector& vec)
		{
			return parent_type::train(learnset, vec);
		}
		MPI_OPTION("SelectValidate")
		/** Train a learning algorithm on the given dataset.
		 * 
		 * @param learnset a training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& learnset)
		{
#ifdef _MPI
			MPI_IF_NOT_ROOT
			maxm=-DBL_MAX;
			parent_type::startSelectingParameters();////////need to add to arguments!!!
			measure.initialize();
			parent_type::argument().store(saved);
			MPI_IF_END
#endif
			const char* msg;
			if( (msg=validation.validate(*this, measure, learnset)) != 0 ) return msg;
			MPI_IF_NOT_ROOT
			validate(learnset);
			MPI_IF_END
			return 0;
		}
		/** Resize the number of saved learners, testsets.
		 *
		 * @param n the new size.
		 */
		void resize_saved(size_type n)
		{
			if( n > 0 && fastTuneInt==1 )
			{
				::erase(learners);
				count = n;
				learners = ::setsize<saved_type>(count);
				current = saved_begin();
			}
			else current = 0;
		}
		/** Save the given testset and the current learner.
		 * 
		 * @param t a testset.
		 */
		void save_next(testset_type& t)
		{
			if( current != 0 )
			{
				ASSERTMSG( size_type(std::distance(learners, current)) < count, size_type(std::distance(learners, current)) << " < " << count );
				current->shallow_copy(*this, t);
				current++;
			}
		}
		/** Save the given testset and learner.
		 * 
		 * @param learn a learner.
		 * @param t a testset.
		 */
		void save_next(select_type& learn, testset_type& t)
		{
			if( current != 0 )
			{
				ASSERTMSG( size_type(std::distance(learners, current)) < count, size_type(std::distance(learners, current)) << " < " << count );
				current->shallow_copy(learn, t);
				current++;
			}
		}
		/** Save the given testset and learner.
		 * 
		 * @param learn a learner.
		 * @param t a testset.
		 */
		void save_next(typename select_type::parent_type& learn, testset_type& t)
		{
			if( current != 0 )
			{
				ASSERTMSG( size_type(std::distance(learners, current)) < count, size_type(std::distance(learners, current)) << " < " << count );
				current->shallow_copy(learn, t);
				current++;
			}
		}
		/** Initializes a map with a set of arguments.
		 * 
		 * @param map an argument map.
		 * @param t level of arguments to add.
		 */
		template<class U>
		void init(U& map, int t=0)
		{
			parent_type::init(map, t);
			if( !parent_type::TUNE ) return;
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(SelectCommittee));
				validation.init(map, validint(t, parent_type::TUNE?5:0), "sel");
				measure.init(map, isvalid(t), "sel");
				arginit(map, typeInt, 		"sel", "tune_print", 	"print something while tuning>None:0;Best:1;All:2");
				arginit(map, fileStr, 		"sel", "tune_file",  	"print to file, if empty print to console", ArgumentMap::ADDITIONAL);
				arginit(map, restartStr, 	"sel", "tune_restart",  "restart file for model selection", ArgumentMap::ADDITIONAL);
				if(parent_type::FAST_TUNE)
				arginit(map, fastTuneInt, 	"sel", "tune_fast", 	"trade memory for speed (so far only AdaBoost)?");
			}
		}
		/** Test if validation uses test set.
		 * 
		 * @return true for test set validation.
		 */
		bool istesting()const
		{
			return validation.istesting();
		}
		/** Test if selection uses fast tune.
		 * 
		 * @return true for fast tune.
		 */
		bool fastTune()const
		{
			return fastTuneInt == 1;
		}
		/** Initialize tunable arguments.
		 * 
		 * @param arg an iterator to tunable arguments.
		 * @return an error message or NULL.
		 */
		const char* initialize(argument_iterator arg)
		{
//test=true;
			const char* msg;
			init_stream();
			maxm=-DBL_MAX;
			parent_type::startSelectingParameters();////////need to add to arguments!!!
			measure.initialize();
			parent_type::argument().store(saved);
			if( (msg=load_parameters(arg)) !=0 ) return msg;
			print_header(parent_type::argument(), typeInt != NONE);
			return 0;
		}
		/** Finalize the selection of arguments.
		 */
		void finalize()
		{
//ASSERT(test);
//test=false;
			parent_type::argument().load(saved);
			print(parent_type::argument(), maxm, (typeInt != NONE)?'B':0);
			parent_type::stopSelectingParameters();////////need to add to arguments!!!

			::erase(learners);
			learners = 0;
			count = 0;
			current = 0;
		}
//private:
//bool test;
		
	public:
		/** Gets an iterator to start of collection.
		 *
		 * @return iterator to start of collection.
		 */
		saved_iterator saved_begin()
		{
			return learners;
		}
		/** Gets a constant iterator to start of collection.
		 *
		 * @return constant iterator to start of collection.
		 */
		const_saved_iterator saved_begin()const
		{
			return learners;
		}
		/** Gets an iterator to start of collection.
		 *
		 * @return iterator to start of collection.
		 */
		saved_iterator saved_end()
		{
			return learners+count;
		}
		/** Gets a constant iterator to start of collection.
		 *
		 * @return constant iterator to start of collection.
		 */
		const_saved_iterator saved_end()const
		{
			return learners+count;
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("SelectCommittee ")+T::class_name();
		}
		/** Get the name of the learner.
		 * 
		 * @return Parameter Selection
		 */
		static std::string name()
		{
			return "Parameter Selection";
		}
		/** Get the version of the select committee.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _MODELSELECTION_VERSION;
		}
		/** Select the best arguments for a learned model.
		 * 
		 * @param learnset a dataset. 
		 */
		void validate(dataset_type& learnset)
		{
			argument_iterator arg = parent_type::argument();
			if( istesting() && fastTune() )
			{
				TuneParameterTree sav(arg, false);
				double currm;
				for(sav.reset();sav.hasNext();++sav)
				{
					ASSERT(!parent_type::empty());
					validation.validate_testset(*this, measure, learnset);
					currm = measure.average();
					print(arg, currm, (currm > maxm?'S':'V'), typeInt == ALL);
					if( currm > maxm )
					{
						arg.store(saved);
						maxm = currm;
					}
				}
			}
			else if( !fastTune() )
			{
				double currm = measure.average();
				print(arg, currm, (currm > maxm?'S':'V'), typeInt == ALL);
				if( currm > maxm )
				{
					arg.store(saved);
					maxm = currm;
				}
			}
			else save_validate(learnset);
		}
		
	protected:
		/** Get saved arguments.
		 * 
		 * @return saved argument vector.
		 */
		std::vector<float>& saved_arguments()
		{
			return saved;
		}
		/** Test if should tune arguments.
		 * 
		 * @return true if tuned.
		 */
		bool is_tune()const
		{
			return parent_type::TUNE && validation.type() != validation_type::NO;
		}
		/** Select best parameters over saved learning models.
		 * 
		 * @param learnset a dataset.
		 */
		void save_validate(dataset_type& learnset)
		{
			argument_iterator arg = parent_type::argument();
			TuneParameterTree sav(arg, false);
			double currm;
			for(sav.reset();sav.hasNext();++sav)
			{
				validation.validate_saved(*this, measure, learnset);
				currm = measure.average();
				print(arg, currm, (currm > maxm?'S':'V'), typeInt == ALL);
				if( currm > maxm )
				{
					arg.store(saved);
					maxm = currm;
				}
			}
		}
		
	protected:
		/** Load parameters from a restart file.
		 * 
		 * @param arg a collection of arguments.
		 * @return an error message or NULL.
		 */
		const char* load_parameters(argument_iterator arg)
		{
			if( restartStr.empty() ) return 0;
			if( restartBool && testfile(restartStr) )
			{
				std::ifstream fin(restartStr.c_str());
				if( fin.fail() ) return ERRORMSG("Cannot open restart file: " << restartStr);
				std::vector<float> tmpvec;
				std::streampos pos=0L;
				if( parse_header(fin, arg) )
				{
					while(!fin.eof() && arg.hasNext())
					{
						pos = fin.tellg();
						arg.store(tmpvec);
						if( !parse_param(fin, tmpvec.rbegin(), tmpvec.rend(), tmpvec) ) break;
						++arg;
					}
				}
				fin.close();
				std::ofstream* pfout;
				prestartOut = pfout = new std::ofstream(restartStr.c_str());
				pfout->seekp(pos);
			}
			else
			{
				delete prestartOut;
				if( !restartStr.empty() )
				prestartOut = new std::ofstream(restartStr.c_str());
			}
			if( prestartOut->fail() ) return ERRORMSG("Cannot open " << restartStr << " for writing");
			if( restartBool > 0 ) restartBool = 0;
			return 0;
		}
		/** Parse header of saved parameters.
		 * 
		 * @param in an input stream.
		 * @param arg a tunable argument iterator.
		 * @return true if no error.
		 */
		bool parse_header(std::istream& in, argument_iterator arg)
		{
			if( in.get() != '#' ) return false;
			if( in.get() != 'T' ) return false;
			if( in.get() != 'V' ) return false;
			if( in.get() != '\t' ) return false;
			if( in.fail() ) return false;
			
			std::string line;
			char ch = '\t';
			std::vector< std::string > headers;
			arg.store(headers); headers.push_back( measure.name() );
			for(typename std::vector<std::string>::reverse_iterator beg = headers.rbegin(), end = headers.rend();beg != end;++beg)
			{
				if( (beg+1) == end ) ch = '\n';
				std::getline(in, line, ch);
				if( in.fail() ) return false;
				if( line != *beg ) return false;
			}
			if( in.peek() == '\n' ) in.get();
			if( !in.eof() && in.fail() ) return false;
			return true;
		}
		/** Parse saved parameters.
		 * 
		 * @param in an input stream.
		 * @param beg an iterator to the start of a collection of parameters.
		 * @param end an iterator to the end of a collection of parameters.
		 * @param vec a destination vector.
		 * @return true if no error.
		 */
		template<class I, class U>
		bool parse_param(std::istream& in, I beg, I end, U& vec)
		{
			float tmp, mtmp;
			if( in.get() != '#' ) return false;
			if( in.get() != 'T' ) return false;
			if( in.get() != 'V' ) return false;
			if( in.get() != '\t' ) return false;
			if( (in >> mtmp).fail() ) return false;
			for(;beg != end;++beg)
			{
				if( in.get() != '\t' ) return false;
				if( (in >> tmp).fail() ) return false;
				if( tmp != *beg ) return false;
			}
			if( in.get() != '\n' ) return false;
			if( !in.eof() && in.fail() ) return false;
			if( mtmp > maxm )
			{
				saved = vec;
				maxm = mtmp;
			}
			return true;
		}
		
	public:
		/** Print current tunable arguments and measure to a stream.
		 * 
		 * @param arg a tunable argument iterator.
		 * @param m a measure value.
		 * @param ch a validation prefix.
		 * @param print should print.
		 */
		void print(argument_iterator arg, double m, char ch='V', bool print=false)
		{
			MPI_IF_ROOT
			if( print || prestartOut != 0 )
			{
				std::vector<float> temp;
				arg.store(temp);
				temp.push_back( (float)m );
				if( ch != 0 ) print_array(*pout, temp.rbegin(), temp.rend(), ch);
				if( prestartOut != 0 ) 
				{
					ASSERT(false);
					print_array(*prestartOut, temp.rbegin(), temp.rend(), 'V');
				}
			}
			MPI_IF_END
		}
		/** Print current tunable argument header and measure header to a stream.
		 * 
		 * @param arg a tunable argument iterator.
		 * @param print should print.
		 */
		void print_header(argument_iterator arg, bool print)
		{
			MPI_IF_ROOT//need better determination
			if( print || prestartOut != 0 )
			{
				std::vector<std::string> temp;
				arg.store(temp);
				temp.push_back( measure.name() );
				if( print ) print_array(*pout, temp.rbegin(), temp.rend(), 'H');
				if( prestartOut != 0 ) print_array(*prestartOut, temp.rbegin(), temp.rend(), 'H');
			}
			MPI_IF_END
		}
		/** Debug print.
		 * 
		 * @param out a stream.
		 * @param arg an argument iterator.
		 */
		void debug_print(std::ostream& out, argument_iterator arg)
		{
			std::vector<float> temp;
			arg.store(temp);
			print_array(out, temp.rbegin(), temp.rend(), 't');
		}
		
	private:
		static bool isvalid(int t)
		{
			return t == 0 || t == ARGUMENT;
		}
		static int validint(int t, int s)
		{
			if( s == 0 ) return ( (t == 0 || t == ARGUMENT)?0:-1);
			return ( (t == 0 || t == ARGUMENT)?1:-1) * s;
		}
		template<class I>
		static void print_array(std::ostream& out, I beg, I end, char type)
		{
			out << "#T" << type;
			for(;beg != end;++beg) out << "\t" << *beg;
			out << "\n";
		}
		void init_stream()
		{
			MPI_IF_ROOT
			if( !fileStr.empty() && pout == &std::cout )
			{
				pout = new std::ofstream(fileStr.c_str());
				if( pout == 0 || pout->fail() )
				{
					delete pout;
					pout = &std::cout;
				}
			}
			MPI_IF_ELSE
#ifdef _MPI
			MPI_IF_NOT_ROOT
			typeInt=0;
			MPI_IF_END
#endif
			MPI_IF_END
		}
#ifdef _MPI
	public:
		/** Update an MPI selection.
		 * 
		 * @param learnset a dataset.
		 * @param args a collection of argument values.
		 */
		void mpi_update(dataset_type& learnset, std::vector<float>& args)
		{
			std::vector<float> tmp;
			parent_type::argument().store(tmp);
			parent_type::argument().load(args);
			validate(learnset);
			parent_type::argument().load(tmp);
		}
		
	private:
		/** Read saved models from an input stream.
		 * 
		 * @param in an input stream.
		 * @param com a committee of learners.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, SelectCommittee& com)
		{
			unsigned int n;
			std::vector<float> saved;
			double maxm;
			in.get();
			if( in.get() != ' ' )
			{
				com.errormsg(in, "Missing space character in SelectCommittee model");
				return in;
			}
			in >> n;
			saved.resize(n);
			if( in.get() != '\n' )
			{
				com.errormsg(in, "Missing newline character in SelectCommittee model");
				return in;
			}
			for(unsigned int i=0;i<n;++i)
			{
				in >> saved[i];
				if( in.get() != ',' )
				{
					com.errormsg(in, "Missing newline character in SelectCommittee model");
					return in;
				}
			}
			in >> maxm;
			if( in.get() != '\n' )
			{
				com.errormsg(in, "Missing newline character in SelectCommittee model");
				return in;
			}
			if( com.typeInt == ALL ) com.print(*com.pout, saved, maxm, (maxm > com.maxm?'S':'V'));
			if( maxm > com.maxm )
			{
				com.maxm = maxm;
				com.saved = saved;
			}
			return in;
		}
		/** Write saved models to an output stream.
		 * 
		 * @param out an output stream.
		 * @param com a committee of learners.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const SelectCommittee& com)
		{
			out << "^ " << com.saved.size() << "\n";
			for(unsigned int i=0,n=com.saved.size();i<n;++i)
				out << com.saved[i] << ",";
			out << com.maxm << "\n";
			return out;
		}
		void print(std::ostream& out, const std::vector<float>& ar, double m, char ch='V')
		{
			MPI_IF_ROOT
			std::vector<float> temp(ar);
			temp.push_back( (float)m );
			print_array(out, temp.begin(), temp.end(), ch);
			MPI_IF_END
		}
		parent_type& parent()
		{
			return *this;
		}
		const parent_type& parent()const
		{
			return *this;
		}
#endif

	private:
		validation_type validation;
		measure_type measure;
		std::vector<float> saved;
		double maxm;
		int typeInt;
		std::string fileStr;
		std::string restartStr;
		std::ostream* pout;
		std::ostream* prestartOut;
		
	private:
		saved_pointer learners;
		size_type count;
		saved_iterator current;
		int fastTuneInt;
		int restartBool;
	};
	
	/** @brief Selects the parent classifier as a base learner
	 * 
	 * This class selects the parent classifier as a base learner.
	 */
	template<class T, class M>
	struct base_learner< SelectCommittee<T, M> >
	{
		/** Defines a base learner as a result. **/
		typedef typename base_learner<T>::result result;
	};
	
};
/** @brief Type utility interface to a select committee
 * 
 * This class defines a type utility interface to a select committee.
 */
template<class T, class M>
struct TypeUtil< ::exegete::SelectCommittee<T,M> >
{
	/** Flags class as non-primative.*/
	enum{ ispod=false };
	/** Defines a value type. **/
	typedef ::exegete::SelectCommittee<T,M> value_type;
	/** Tests if a string can be converted to a SelectCommittee.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return SelectCommittee
	 */
	static const char* name() 
	{
		return "SelectCommittee";
	}
};


#endif


