/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * TuneParameter.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_TUNEPARAMETER_H
#define _EXEGETE_TUNEPARAMETER_H
#include "float.h"
#include <iostream>

/** @file TuneParameter.h
 * @brief Holds a parameter range
 * 
 * This file contains the TuneParameter class.
 *
 * @todo setup default param
 * 
 * 
 * @ingroup ExegeteModelSelection
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Holds a parameter range
	 * 
	 * This class maintiains the current value of a machine learning
	 * algorithm parameter and moves it over a specified range by
	 * a specified increment.
	 */
	class TuneParameter
	{
	public:
		/** Constructs a TuneParameter with the begin, end and increment of the 
		 * parameter range.
		 *
		 * @param b the start of the parameter range.
		 * @param e the  of the parameter range.
		 * @param i the increment of the parameter range.
		 * @param o the increment operator.
		 */
		TuneParameter(float b=FLT_MAX, float e=FLT_MAX, float i=FLT_MAX, char o='$') : curr(b),beg(b),end(e),inc(i),opr(o)
		{
			ensureValid();
		}
		/** Destructs a TuneParameter.
		 */
		~TuneParameter()
		{
		}

	public:
		/** Assigns a copy of a tune parameter.
		 * 
		 * @param param a tune parameter.
		 * @return a reference to this object.
		 */
		TuneParameter& operator=(const TuneParameter& param)
		{
			curr = param.curr;
			beg = param.beg;
			end = param.end;
			inc = param.inc;
			opr = param.opr;
			return *this;
		}
		/** Assigns a value to the current parameter value.
		 *
		 * @param d a new value.
		 * @return a reference to this object.
		 */
		TuneParameter& operator=(float d)
		{
			curr = d;
			return *this;
		}
		/** Increments the current parameter value.
		 *
		 * @return a reference to this object.
		 */
		TuneParameter& operator++()
		{
			next();
			return *this;
		}
		/** Gets the current value.
		 *
		 * @return the current value.
		 */
		operator float()const
		{
			return curr;
		}
		/** Save values to vector.
		 * 
		 * @param vec a destination vector.
		 */
		void save(std::vector<float>& vec)const
		{
			vec.push_back(curr);
			vec.push_back(beg);
			vec.push_back(end);
			vec.push_back(inc);
			vec.push_back(float(opr));
		}
		/** Load values from a vector.
		 * 
		 * @param it an source iterator to vector.
		 */
		void load(std::vector<float>::const_iterator& it)
		{
			curr = *it; ++it;
			beg = *it; ++it;
			end = *it; ++it;
			inc = *it; ++it;
			opr = char(*it); ++it;
		}

	public:
		/** Gets the current value.
		 *
		 * @return the current value.
		 */
		float value()const
		{
			return curr;
		}
		/** Resets the current value to the start of the range.
		 */
		void reset(bool last)
		{
			if(last)
			{
				curr = end;
				prev();
			}
			else curr=beg;
		}
		/** Tests if the current value has exceeded the range.
		 *
		 * @param n should increment.
		 * @return true if the current parameter has not exceed the end of the range.
		 */
		bool hasNext(bool n=false)const
		{
			if( n )
			{
				float tmp=curr;
				if(opr == '+') tmp+=inc;
				else if( opr == '-' ) tmp-=inc;
				else if( opr == '/' ) tmp/=inc;
				else tmp*=inc;
				return tmp < end;
			}
			return curr < end;
		}
		/** Increments the current parameter value.
		 */
		void next()
		{
			if(opr == '+') curr+=inc;
			else if( opr == '-' ) curr-=inc;
			else if( opr == '/' ) curr/=inc;
			else curr*=inc;
		}
		/** Decrements the current parameter value.
		 */
		void prev()
		{
			if(opr == '+') curr-=inc;
			else if( opr == '-' ) curr+=inc;
			else if( opr == '/' ) curr*=inc;
			else curr/=inc;
		}
		/** Tests if this parameter range is empty.
		 *
		 * @return true if range is empty.
		 */
		bool empty()const
		{
			return beg == FLT_MAX;
		}

	protected:
		/** Writes a TuneParameter to the output stream.
		 *
		 * @param out a destination output stream.
		 * @param param a source TuneParameter.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const TuneParameter& param)
		{
			out << param.beg;
			if( param.opr == '%' && param.inc != FLT_MAX )
				out << "-" << param.inc << param.opr;
			else if( param.end != -FLT_MAX && param.inc != FLT_MAX && param.opr != '%' )
				out << "-" << param.end << param.opr << param.inc;
			return out;
		}
		/** Reads a TuneParameter from the input stream.
		 *
		 * @todo add error processing here and in stringutil.h
		 * 
		 * @param in an input stream.
		 * @param param a TuneParameter.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, TuneParameter& param)
		{
			param.beg = FLT_MAX;
			param.end = FLT_MAX;
			param.inc = FLT_MAX;
			param.opr = '$';
			in >> param.beg;
			if( !in.eof() ) in.get();
			if( !in.eof() ) in >> param.end;
			if( !in.eof() ) in >> param.opr;
			if( param.opr != '%' && !in.eof() ) in >> param.inc;
			param.ensureValid();
			return in;
		}

	private:
		void ensureValid()
		{
			if( beg != FLT_MAX && end == FLT_MAX )
			{
				curr=beg;
				if( inc == FLT_MAX ) inc = 1;
				if( opr == '$' ) opr = '+';
				next();
				end = curr;
				reset(true);
			}
		}

	private:
		float curr;
		float beg;
		float end;
		float inc;
		char opr;
	};
};

#endif


