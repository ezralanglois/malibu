/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * TuneParameterTree.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_TUNESELECTPARAMETERTREE_H
#define _EXEGETE_TUNESELECTPARAMETERTREE_H
#include "TuneParameter.h"
#include "AnyParameter.h"
#include <vector>

/** @file TuneParameterTree.h
 * @brief A tree of conditional tunable arguments
 * 
 * This file contains the TuneParameterTree class.
 *
 * 
 * @todo fix bug in fast tune printing, 0.9-1.0+0.1 non-fast tune prints both 0.9 and 1.0 for AdaBoost on Willow
 * 
 * @ingroup ExegeteModelSelection
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief A tree of conditional tunable arguments
	 * 
	 * This class maintiains a tree of tunable parameters to
	 * be conditional incremented over a range.
	 */
	class TuneParameterTree
	{
		typedef AbstractAnyParameter*			value_pointer;
		typedef TuneParameterTree*				pointer;
		typedef std::vector<pointer>			child_vector;
		typedef child_vector::iterator			child_iterator;
		typedef child_vector::const_iterator	const_child_iterator;
		typedef child_vector::size_type			size_type;
	public:
		/** Defines a tune parameter. **/
		typedef TuneParameter					parameter_type;
		/** Defines a tune parameter. **/
		typedef TuneParameter					value_type;
	public:
		/** Constructs an empty tune parameter tree.
		 */
		TuneParameterTree() : valueptr(0), saveBool(false)
		{
		}
		/** Constructs a TuneParameterTree.
		 *
		 * @param ref reference to parameter to change.
		 * @param nm name of parameter.
		 * @param s should save parameter.
		 * @param prm a TuneParameter.
		 */
		template<class T>
		TuneParameterTree(T& ref, const std::string& nm, bool s, const parameter_type& prm) : 
					namestr(nm), param(prm), valueptr(new AnyParameter<T>(ref)), 
					rngbeg(-FLT_MAX), rngend(FLT_MAX), saveBool(s)
		{
		}
		/** Constructs a TuneParameterTree.
		 *
		 * @param ref a reference to parameter to change.
		 * @param nm name of parameter.
		 * @param prm a TuneParameter.
		 * @param p a pointer to a parent TuneParameterTree, default NULL.
		 * @param b begin of parameter range, default -FLT_MAX.
		 * @param e end of parameter range, default FLT_MAX.
		 * @param s should save parameter (default false).
		 */
		template<class T>
		TuneParameterTree(T& ref, const std::string& nm, const parameter_type& prm, 
				pointer p=0, float b=-FLT_MAX, float e=FLT_MAX, bool s=false) : 
				namestr(nm), param(prm), valueptr(new AnyParameter<T>(ref)), 
				rngbeg(b), rngend(e), saveBool(s)
		{
			if( p != 0 ) p->add(this);
		}
		/** Constructs a TuneParameterTree.
		 *
		 * @param ref reference to parameter to change.
		 * @param tree a tree to copy.
		 * @param p a pointer to a parent TuneParameterTree, default NULL.
		 */
		template<class T>
		TuneParameterTree(T& ref, const TuneParameterTree& tree, pointer p=0) : 
			namestr(tree.namestr), param(tree.param), valueptr(new AnyParameter<T>(ref)), 
			rngbeg(tree.rngbeg), rngend(tree.rngend), saveBool(tree.saveBool)
		{
			if( p != 0 ) p->add(this);
		}
		/** Constructs a copy of a tune parameter tree.
		 *
		 * @param tree a tree to copy.
		 */
		TuneParameterTree(const TuneParameterTree& tree, bool sav) : 
			namestr(tree.namestr), param(tree.param), 
			valueptr(tree.valueptr->clone()), rngbeg(tree.rngbeg), 
			rngend(tree.rngend), saveBool(false)
		{
		}
		/** Constructs a copy of a tune parameter tree.
		 *2310 W. Arthur
		 * @param tree a tree to copy.
		 */
		TuneParameterTree(const TuneParameterTree& tree) : 
			namestr(tree.namestr), param(tree.param), childern(tree.childern), 
			valueptr(tree.valueptr->clone()), rngbeg(tree.rngbeg), 
			rngend(tree.rngend), saveBool(tree.saveBool)
		{
			current = childern.begin() + std::distance(tree.childern.begin(), const_child_iterator(tree.current));
		}
		/** Destructs a TuneParameterTree.
		 */
		~TuneParameterTree()
		{
			delete valueptr;
		}
		
	public:
		/** Assigns a copy of a tune parameter tree.
		 *
		 * @param tree a tune parameter tree to copy.
		 * @return a reference to this object.
		 */
		TuneParameterTree& operator=(const TuneParameterTree& tree)
		{
			namestr = tree.namestr;
			param = tree.param;
			rngbeg = tree.rngbeg;
			rngend = tree.rngend;
			if( valueptr != 0 && tree.valueptr != 0 ) *valueptr = float(*tree.valueptr);
			current = childern.begin() + std::distance(tree.childern.begin(), const_child_iterator(tree.current));
			return *this;
		}

	public:
		/** Increments the current parameter value.
		 *
		 * @return a reference to this class.
		 */
		TuneParameterTree& operator++()
		{
			next();
			return *this;
		}

	public:
		/** Resets the current value to the start of the range.
		 * 
		 * @param ft should transverse children.
		 */
		void reset(bool ft=true)
		{
			if(!ft)
			{
				saveBool=false;
				child_iterator it = first_child(false);
				while( it != childern.end() )
				{
					(*it)->saveBool=false;
					it = next_child(it,false);
				}
			}
			param.reset(ft&&saveBool);
			ASSERT(valueptr != 0);
			*valueptr = param;
			current = first_child();
			if( current != childern.end() ) (*current)->reset(ft);
		}
		/** Test if tune parameter tree has reached last value.
		 * 
		 * @return true if has reached last value.
		 */
		bool is_last()const
		{
			if( param.hasNext(true) ) return false;
			if( !childern.empty() && current != childern.end() )
			{
				ASSERT(current != childern.end());
				if( (*current)->is_last() )
				{
					const_child_iterator curr = next_child(current);
					if( curr == childern.end() ) return true;
					else return (*curr)->is_last();
				}
				else return false;
			}
			return true;
		}
		/** Increments the current parameter value of some parameter in the tree.
		 */
		void next()
		{
			if( !childern.empty() && current != childern.end() )
			{
				ASSERT(current != childern.end());
				if( !(*current)->hasNext(true) )
				{
					current = next_child(current);
					if( current == childern.end() )
					{
						param.next();
						ASSERT(valueptr != 0);
						*valueptr = param;
						current = first_child();
					}
					else 
					{
						(*current)->next();
					}
				}
				else 
				{
					ASSERT(current != childern.end());
					(*current)->next();
				}
			}
			else
			{
				param.next();
				ASSERT(valueptr != 0);
				*valueptr = param;
			}
		}
		/** Tests if the current value has exceeded the range.
		 *
		 * @return true if the current parameter has not exceed the end of the range.
		 */
		bool hasNext()const
		{
			return param.hasNext();
		}
		/** Adds a child to the child vector.
		 *
		 * @param p a pointer to the child.
		 */
		void add(pointer p)
		{
			if( !p->empty() )
				childern.push_back(p);
		}
		/** Gets the name of the parameter.
		 *
		 * @return the name of the parameter.
		 */
		const std::string& name()const
		{
			return namestr;
		}
		/** Checks if the current parameter value falls in the argument operating range.
		 *
		 * @param val value to check.
		 * @return true if the value falls in the operating range.
		 */
		bool exists(float val)const
		{
			return rngbeg <= val && val < rngend;
		}
		/** Stores the current value of every parameter into a vector.
		 *
		 * @param vec a destination float vector.
		 */
		void store(std::vector<float>& vec)const
		{
			vec.resize(size_impl());
			store_impl(&vec[0]);
		}
		/** Stores the current name of every parameter into a vector.
		 *
		 * @param vec a destination std::string vector.
		 */
		void store(std::vector<std::string>& vec)const
		{
			vec.resize(size_impl());
			store_impl(&vec[0]);
		}
		/** Load the current value from the vector to the tree.
		 *
		 * @param vec a source float vector.
		 */
		void load(const std::vector<float>& vec)
		{
			load_impl(&vec[0]);
		}
		/** Gets size of child vector.
		 *
		 * @return size of vector.
		 */
		size_type size()const
		{
			return childern.size();
		}
		/** Tests if argument is savable.
		 * 
		 * @return true of argument is savable.
		 */
		bool isSaveable()
		{
			return saveBool;
		}
		/** Gets current value of parameter.
		 * 
		 * @return current value.
		 */
		float value()const
		{
			return param;
		}
		/** Gets current value of actual parameter.
		 * 
		 * @return current value.
		 */
		float pvalue()const
		{
			return *valueptr;
		}

	private:
		bool hasNext(bool f)const
		{
			if( !childern.empty() && current != childern.end() )
			{
				if( (*current)->hasNext(f) ) return true;
				else return param.hasNext(f);
			}
			else return param.hasNext(f);
		}
		unsigned int size_impl()const
		{
			const_child_iterator it = first_child();
			unsigned int n=1;
			while( it != childern.end() )
			{
				n += (*it)->size_impl();
				it = next_child(it);
			}
			return n;
		}
		std::string* store_impl(std::string* pFlt)const
		{
			*pFlt = namestr;
			++pFlt;
			const_child_iterator it = first_child();
			while( it != childern.end() )
			{
				pFlt=(*it)->store_impl(pFlt);
				it = next_child(it);
			}
			return pFlt;
		}
		float* store_impl(float* pFlt)const
		{
			*pFlt = *valueptr;
			++pFlt;
			const_child_iterator it = first_child();
			while( it != childern.end() )
			{
				pFlt=(*it)->store_impl(pFlt);
				it = next_child(it);
			}
			return pFlt;
		}
		const float* load_impl(const float* pFlt)
		{
			param = *pFlt;
			ASSERT(valueptr != 0);
			*valueptr = *pFlt;
			++pFlt;
			child_iterator it = first_child(false);
			while( it != childern.end() )
			{
				pFlt=(*it)->load_impl(pFlt);
				it = next_child(it,false);
			}
			return pFlt;
		}
		/** Returns a reference to the internal TuneParameter.
		 *
		 * @return a reference to the TuneParameter.
		 */
		parameter_type& parameter()
		{
			return param;
		}
		
	public:
		/** Tests if the argument is empty, does not tune an parameter.
		 *
		 * @return true if empty.
		 */
		bool empty()const
		{
			return valueptr == 0;
		}

	private:
		const_child_iterator first_child()const
		{
			return next_exist_child(childern.begin());
		}
		child_iterator first_child(bool r=true)
		{
			return next_exist_child(childern.begin(), r);
		}
		child_iterator next_child(child_iterator cit, bool r=true)
		{
			++cit;
			return next_exist_child(cit, r);
		}
		const_child_iterator next_child(const_child_iterator cit)const
		{
			++cit;
			return next_exist_child(cit);
		}
		const_child_iterator next_exist_child(const_child_iterator cit)const
		{
			float curr = param;
			for(const_child_iterator cend=childern.end();cit != cend;++cit) 
				if( (*cit)->exists(curr) ) break;
			return cit;
		}
		child_iterator next_exist_child(child_iterator cit, bool r=true)
		{
			float curr = param;
			for(child_iterator cend=childern.end();cit != cend;++cit) 
			{
				if( (*cit)->exists(curr) )
				{
					if( r )
					{
						(*cit)->reset();
						if( (*cit)->hasNext(true) ) break;
					}
					else break;
				}
			}
			return cit;
		}

	public:
		/** Writes a TuneParameterTree to the output stream.
		 *
		 * @param out a destination output stream.
		 * @param param a source TuneParameterTree.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const TuneParameterTree& param)
		{
			out << param.param;
			return out;
		}
		/** Reads a TuneParameterTree from the input stream.
		 *
		 * @param in an input stream.
		 * @param param a TuneParameterTree.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, TuneParameterTree& param)
		{
			in >> param.param;
			return in;
		}
	public:
		/** Save values to vector.
		 * 
		 * @param vec a destination vector.
		 */
		void save(std::vector<float>& vec)const
		{
			param.save(vec);
		}
		/** Load values from a vector.
		 * 
		 * @param it a source iterator to a vector.
		 */
		void load(std::vector<float>::const_iterator& it)
		{
			param.load(it);
			*valueptr = param;
		}
		
	public:
		/** Write debug information to stream.
		 * 
		 * @param out an output stream.
		 */
		void debug(std::ostream& out)const
		{
			out << namestr << " = " << param << " = " << *valueptr << std::endl;
			for(const_child_iterator beg = childern.begin(), end = childern.end();beg != end;++beg) (*beg)->debug(out);
		}

	private:
		std::string namestr;
		parameter_type param;
		child_vector childern;
		value_pointer valueptr;
		float rngbeg;
		float rngend;
		bool saveBool;
	private:
		child_iterator current;
	};
};

#endif


