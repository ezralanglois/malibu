/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * searn.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_SEARN_H
#define _EXEGETE_SEARN_H
#include "WeakLearner.h"
#include "Committee.h"
#include "memoryutil.h"
#include "mathutil.h"

/** @file searn.hpp
 * @brief Structured-learning from cost-sensitive multi-class classifier
 * 
 * This file contains the SEARN algorithm.
 *
 * @ingroup ExegeteStructuredOutput
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteStructuredOutput Structured Output
 * 
 *  This group holds all model selection files.
 */

/** Defines the version of the output codes algorithm. 
 * @todo add to group
 */
#define _SEARN_VERSION 101000

namespace exegete
{
	/** @brief Structured-learning from cost-sensitive multi-class classifier
	 * 
	 * This class provides a structured-learning from cost-sensitive multi-class 
	 * classifier.
	 * 
	 * The pseudo code for SEARN is found below taken verbatim from 
	 * http://www.cs.utah.edu/~hal/docs/daume06searn.pdf
	 * 
	 * Definitions:
	 *  - $\mathcal{S}^{SP}$: set of structured examples 
	 *  - $\pi$: Initial policy (Search phase that chooses optimal actions)
	 *  - $L$: a multi-class cost-sensitive learner
	 * 
	 * Algorithm SEARN($\mathcal{S}^{SP}$, $\pi$, $L$)
	 * 1. Initialize policy $h \leftarrow \pi$
	 * 2. while $h$ has a significant dependence on $\pi$ do
	 * 3.   Initialize the set of cost-sensitive examples $S \leftarrow \emptyset$
	 * 4.   for $(x,y) \in $\mathcal{S}^{SP}$ do
	 * 5.		Compute predictions under current policy $\hat{y} ~ h(x)$
	 * 6.		for t = 1...$T_x$ do
	 * 7.			Compute features $X^{SP}$ for state $s_t=(x,y_1,\ldots,y_t)$
	 * 8.			Initalize a cost vector $c=(.)$
	 * 9. 			for each possible action $a$ do
	 * 10. 				Let the cost $l_a$ for example (x,c) at state s be $l_h(c,s,a)$
	 * 11. 			end for
	 * 12. 			Add cost-sensitive example ($X^{SP}$,l) to S
	 * 13.		end for
	 * 14.	end for
	 * 15.  Learn classifier over $S: h' \leftarrow L(S)$
	 * 16.  Interpolate: $h \leftarrow \beta h' + (1-\beta)h$
	 * 17. end while
	 * 18. return h without $pi$
	 * 
	 * @todo SECOC, FilterTree, ECOC FilterTree
	 * @todo FilterBoost (is multi-class?)
	 * @todo sparse decision trees (multi-class)
	 */
	template<class T, template<class> class Policy>
	class searn : public Committee< WeakLearner<T> >
	{
		typedef Committee< WeakLearner<T> > committee_type;
		typedef T learner_type;
		typedef typename learner_type::dataset_type parentset_type;
		typedef typename committee_type::const_iterator const_iterator;
		typedef typename committee_type::iterator iterator;
	public:
		/** Sets argument level. **/
		enum{ ARGUMENT=T::ARGUMENT+1, TUNE=1, EVAL=2, STRUCT=1, FAST_TUNE=1 };

	public:
		/** Defines a testset type. **/
		typedef typename learner_type::testset_type 			testset_type;
		/** Defines a parent type. **/
		typedef learner_type 									parent_type;
		/** Defines dataset type. **/
		typedef testset_type									dataset_type;
		/** Defines a dataset constant attribute pointer. **/
		typedef typename dataset_type::const_attribute_pointer	const_attribute_pointer;
		/** Defines a descriptor type. **/
		typedef typename dataset_type::attribute_type			descriptor_type;
		/** Defines a float type. **/
		typedef typename learner_type::float_type				float_type;
		/** Defines a tunable argument iterator. **/
		typedef typename learner_type::argument_iterator		argument_iterator;
		/** Defines a weight type. **/
		typedef void											weight_type;
		/** Defines a pointer to a float type as a prediction type. **/
		typedef float_type*										prediction_type;
		/** Defines a prediction vector, algorithm is self validating. **/
		typedef int 											prediction_vector;
		
	private:
		typedef example_pool<dataset_type, T::USE_TRAINSET>		pool_type;
		typedef TuneParameterTree 								argument_type;
		typedef typename argument_type::parameter_type			range_type;
		typedef Policy<dataset_type> 							policy_type;
		typedef typename dataset_type::size_type				size_type;
		typedef size_type* 										size_pointer;
		typedef float_type* 									float_pointer;
		typedef typename dataset_type::iterator					example_iterator;
		typedef typename dataset_type::const_iterator			const_example_iterator;
		typedef typename dataset_type::bag_iterator				bag_iterator;
		typedef typename dataset_type::const_bag_iterator		const_bag_iterator;
		typedef typename parentset_type::value_type				example_type;
		//typedef typename parentset_type::weight_type			weight_pointer;
		typedef typename example_type::weight_type*				weight_pointer;
		typedef typename example_type::weight_type				w_type;
		typedef typename parentset_type::iterator 				parent_iterator;
		typedef typename dataset_type::attribute_pointer 		attribute_pointer;
		typedef typename dataset_type::type_util 				type_util;
		
	public:
		/** Constructs a SEARN structured-learner.
		 */
		searn() : totalwgt(0.0), attrn(0), verboseTypeInt(0), costTypeInt(0), iterationInt(4), betaFlt(0.9),
				  iterationSel(iterationInt, "Iteration", true, range_type(1, 5, 1, '+')), 
				  ptmpflt(0), ptmpwgt(0), pysize(0)
		{
			iterationSel.add( &learner_type::argument() );
			learner_type::add_test( learner_type::MIL );
		}
		/** Destructs a SEARN structured-learner.
		 */
		~searn()
		{
			::erase(ptmpflt);
			::erase(ptmpwgt);
			::erase(pysize);
		}

	public:		
		/** Constructs a deep copy of the SEARN model.
		 *
		 * @param s a source SEARN model.
		 */
		searn(const searn& s) : committee_type(s), policy(s.policy), totalwgt(s.totalwgt), attrn(s.attrn), verboseTypeInt(s.verboseTypeInt), costTypeInt(s.costTypeInt), iterationInt(s.iterationInt), betaFlt(s.betaFlt),
								iterationSel(iterationInt, s.iterationSel), ptmpflt(0), ptmpwgt(0), pysize(0)
		{
			iterationSel.add( &learner_type::argument() );
			learner_type::add_test( learner_type::MIL );
		}
		/** Assigns a deep copy of the SEARN model.
		 *
		 * @param s a source SEARN model.
		 * @return a reference to this object.
		 */
		searn& operator=(const searn& s)
		{
			committee_type::operator=(s);
			policy = s.policy;
			argument_copy(s);
			totalwgt = s.totalwgt;
			attrn = s.attrn;
			return *this;
		}
		/** Copies arguments in an SEARN model.
		 * 
		 * @param s a source SEARN model.
		 */
		void argument_copy(const searn& s)
		{
			iterationInt = s.iterationInt;
			betaFlt = s.betaFlt;
			iterationSel = s.iterationSel;
			costTypeInt = s.costTypeInt;
			verboseTypeInt = s.verboseTypeInt;
		}
		/** Makes a shallow copy of an SEARN model.
		 *
		 * @param ref a reference to an SEARN model.
		 */
		void shallow_copy(searn& ref)
		{
			committee_type::shallow_copy(ref);
			argument_copy(ref);
			policy = ref.policy;
			totalwgt = ref.totalwgt;
			attrn = ref.attrn;
		}
		
	public:
		/** Initializes arguments in this class.
		 *
		 * @todo make iterations and beta tunable?
		 * 
		 * @param map an argument map.
		 * @param t a tunable parameter.
		 */
		template<class U>
		void init(U& map, int t)
		{
			learner_type::init(map, t);
			if( t == 0 || t == ARGUMENT )
			{
				arginit(map, NAME_VERSION(searn));
				if(t>=0)
				arginit(map, iterationSel, 	 "searn_n", 	"number of iterations to run searn (tunable)");
				else
				arginit(map, iterationInt, 	 "searn_n", 	"number of iterations to run searn (not tunable)");
				arginit(map, betaFlt, 		 "searn_beta", 	"beta interpolation parameter (not tunable)");
				arginit(map, costTypeInt,    "searn_cost", 	"how to estimate the costs>Optimal:0;Simulate:1");
				arginit(map, verboseTypeInt, "searn_verbose","write performance to output>None:0;Training:1");
			}
		}
		/** Accept a vistor class (part of the visitor design pattern).
		 * 
		 * @param visitor a visiting class object.
		 */
		template<class V>
		void accept(V& visitor)const
		{
			visitor.visit(*this);
			committee_type::accept(visitor);
		}
		/** Gets the name of the learning algorithm.
		 *
		 * @return Searn
		 */
		static std::string name()
		{
			return "Searn";
		}
		/** Get the version of the SEARN learner.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _SEARN_VERSION;
		}
		/** Get the class name of the learner.
		 * 
		 * @return class name.
		 */
		static std::string class_name()
		{
			return std::string("SEARN ")+T::class_name();
		}
		/** Gets the expected name of the program.
		 * 
		 * @return searn
		 */
		static std::string progname()
		{
			return "searn";
		}
		/** Gets the prefix of the learning algorithm.
		 *
		 * @return searn
		 */
		static std::string prefix()
		{
			return "searn";
		}
		/* Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator argument()
		{
			return iterationSel;
		}
		/** Get the numer of classes.
		 * 
		 * @return class count
		 */
		unsigned int classCount()const
		{
			return policy.class_count();
		}
		
	public:
		/** Setups a concrete example set.
		 *
		 * @param dataset a concrete dataset.
		 */
		template<class X1, class Y1>
		void setup(ConcreteExampleSet<X1,Y1>& dataset, bool testset=false)
		{
			typedef typename ConcreteExampleSet<X1,Y1>::type_util type_util;
			if( !testset ) policy.initialize(dataset);
			size_type n;
			for(example_iterator curr = dataset.begin(), beg=curr, end = dataset.end();curr != end;++curr)
			{
				n = dataset.attributeCountAt(std::distance(beg, curr));
				ASSERTMSG( !type_util::IS_SPARSE || type_util::indexOf(curr->x()[n-1]) == -1, n << " " << std::distance(beg, curr) );
				dataset.resize_example(*curr, n, policy.attribute_count(n));
				ASSERT( !type_util::IS_SPARSE || type_util::indexOf(curr->x()[n-1]) == -1 );
			}
		}
		/** Predicts a class for the specified attribute vector.
		 *
		 * Sequence + structured decoded in output label -> 
		 * Think Multi-class graph models
		 * 
		 * @param ebeg start of example collection.
		 * @param eend end of example collection.
		 * @param pred a structured prediction vector.
		 */
		template<class I>
		float_type predict(I ebeg, I eend, prediction_type* pred, prediction_type* pred_end=0)const
		{
			ASSERT(!committee_type::empty());
			const_iterator beg = committee_type::begin(), curr;
			const_iterator end = beg + std::min(iterationInt, committee_type::size());
			size_type classcnt = policy.class_count(), y;
			float_type loss=0;
			if( end != committee_type::end() )
			{
				if( betas.size() != size_type(std::distance(beg,end)) )
					bwgt = setup_betas(betas, std::distance(beg,end), betaFlt);
				I ecurr = policy.initial_state(ebeg, eend);
				for(;ecurr != eend;++pred)
				{
					ASSERT(ecurr->labelAt(0) == ebeg->labelAt(0));
					ASSERT(pred_end==0 || pred < pred_end);
					curr = select_policy(beg, end, bwgt, betas.begin());
					//curr->predict(*ecurr, *pred);
					predict_class(*curr, *ecurr, *pred);
					y = class_index(*pred, (*pred)+classcnt);
					loss+=policy.cost(ecurr, y);
					ecurr = policy.next_action(ebeg, ecurr, y);
				}
			}
			else
			{
				I ecurr = policy.initial_state(ebeg, eend);
				for(;ecurr != eend;++pred)
				{
					ASSERT(ecurr->labelAt(0) == ebeg->labelAt(0));
					ASSERT(pred_end == 0 || pred < pred_end);
					curr = select_policy(beg, end, totalwgt);
					//curr->predict(*ecurr, *pred);
					predict_class(*curr, *ecurr, *pred);
					y = class_index(*pred, (*pred)+classcnt);
					loss+=policy.cost(ecurr, y);
					ecurr = policy.next_action(ebeg, ecurr, y);
				}
			}
			return loss;
		}
		/** Builds a model for the SEARN learner.
		 *
		 * From a structure a sequence is created where dependence grows!
		 * 
		 * @param learnset the training set.
		 * @return an error message or NULL.
		 */
		const char* train(dataset_type& trainset)
		{
			const char* msg;
			parentset_type subset(trainset, trainset.size());
			size_type classcnt = policy.class_count(), y, py;
			example_iterator ebeg, ecurr, eend;
			bag_iterator bbeg, bend;
			parent_iterator pbeg;
			size_type idx, optcnt, totcnt, excnt;
			
			ASSERT(classcnt>0);
			examplePool.resize(trainset.size(), policy.attribute_count(trainset.maxSparseAttribute()), iterationInt);
			subset.attributeCount(policy.attribute_count());
			allocate_weights(subset.begin(), subset.end(), classcnt);
			ptmpflt = ::resize(ptmpflt, classcnt);
			ptmpwgt = ::resize(ptmpwgt, classcnt);
			pysize = ::resize(pysize, trainset.maximumBagCount()); // only for optimal?
			optwgt = betaFlt;
			totalwgt = 0;
			
			subset.classCount(policy.class_count());
			committee_type::resize(iterationInt);
			iterator beg = committee_type::begin(), curr = beg, tmp;
			for(iterator end = committee_type::end();curr != end;++curr)
			{
				pbeg = subset.begin();
				excnt = optcnt = totcnt = 0;
				for(bbeg = trainset.bag_begin(), bend = trainset.bag_end();bbeg != bend;++bbeg)
				{
					ebeg = bbeg->begin(); eend = bbeg->end();
					ecurr = policy.initial_state(ebeg, eend);
					py = 0;
					for(;ecurr != eend;++pbeg)
					{
						policy.compute_features(ebeg, ecurr, py);
						// Create cost-sensitive multi-class example
						y = compute_costs(curr, bbeg, ecurr, pbeg->w_pointer());
						pbeg->x( examplePool.append(*ecurr, excnt, std::distance(beg, curr)) );
						pbeg->y( y );
						// Take next action
						tmp = select_policy(beg, curr, totalwgt, optwgt);
						if( tmp == curr ) // Optimal
						{
							idx = find_min_cost(pbeg->w_pointer(), ptmpflt, classcnt, y);
							if( idx > 1 ) py = size_type( ptmpflt[ random_int(idx-1) ] );
							else py = y;
						}
						else
						{
							ASSERT(curr != beg);
							ASSERT(curr != tmp);
							ASSERTMSG(!tmp->empty(), "tmp: " << std::distance(beg, tmp) << " iter: " << std::distance(beg, curr));
							//tmp->predict(*ecurr, ptmpflt);
							predict_class(*tmp, *ecurr, ptmpflt);
							py = class_index(ptmpflt, ptmpflt+classcnt);
							if(py == y) optcnt++;
							totcnt++;
						}
						++excnt;
						ecurr = policy.next_action(ebeg, ecurr, py);
					}
				}
				if( (msg=committee_type::train(subset)) != 0 ) return msg;
				
				optwgt *= (1.0-betaFlt);
				totalwgt=update_weights(beg, curr, betaFlt)+betaFlt;
				ASSERTMSG(!learner_type::empty(), "iter: " << std::distance(beg, curr));
				curr->shallow_copy(*this, betaFlt);
				ASSERTMSG(!curr->empty(), "iter: " << std::distance(beg, curr));
				
				if( verboseTypeInt > 0 && beg != curr )
				{
					float_type percent = ( totcnt==0?0:float_type(optcnt)/totcnt );
					std::cout << "#SERN train total=" << totcnt << " with " << size_type(percent*1000)/10 << "% optimal";
					// if( costTypeInt == 2 )
					std::cout << std::endl;
				}
			}
			deallocate_weights(subset.begin(), subset.end());
			return 0;
		}
		
	private:
		size_type compute_optimal_costs(iterator curr, bag_iterator bbeg, example_iterator ecurr, weight_pointer wbeg)
		{
			size_type classcnt = policy.class_count(), imin = 0;
			for(size_type i=0;i<classcnt;++i)
			{
				wbeg[i] = policy.cost(ecurr, i);
				if( i > 0 && wbeg[i] < wbeg[imin] ) imin = i;
			}
			return imin;
		}
		size_type compute_simulation_costs(iterator curr, bag_iterator bbeg, example_iterator ecurr, weight_pointer wbeg)
		{
			example_iterator ebeg=ecurr, estart=bbeg->begin(), eend=bbeg->end();
			iterator beg = committee_type::begin();
			size_type classcnt = policy.class_count();
			size_type imin = 0, i, y, idx;
			size_pointer py;
			iterator tmp;
			
			for(i=0;i<classcnt;++i)
			{
				ecurr = policy.next_action(estart, ebeg, i);
				py = pysize;
				*py = y = i;
				for(++py;ecurr != eend;++py)
				{
					policy.compute_features(estart, ecurr, y);
					tmp = select_policy(beg, curr, totalwgt, optwgt);
					if( tmp == curr )
					{
						y = compute_optimal_costs(curr, bbeg, ecurr, ptmpwgt);
						idx = find_min_cost(ptmpwgt, ptmpflt, classcnt, y);
						ASSERT(idx > 0);
						if( idx > 1 ) y = size_type( ptmpflt[ random_int(idx-1) ] );
						else y = size_type( ptmpflt[ 0 ] );
					}
					else
					{
						//tmp->predict(*ecurr, ptmpflt);
						predict_class(*tmp, *ecurr, ptmpflt);
						y = class_index(ptmpflt, ptmpflt+classcnt);
					}
					*py = y;
					ecurr = policy.next_action(estart, ecurr, y);
				}
				wbeg[i] = policy.loss(ebeg, eend, pysize);
				if( i > 0 && wbeg[i] < wbeg[imin] ) imin = i;
			}
			for(i=0;i<classcnt;++i) wbeg[i] -= wbeg[imin];
			return imin;
		}
		
	private:
		static iterator select_policy(iterator beg, iterator end, float_type wsum, float_type optwgt)
		{
			if( beg == end ) return end;
			float_type val = random_double()*(wsum+optwgt);
			if( val <= optwgt ) return end;
			val -= optwgt;
			for(;beg != end;++beg)
			{
				val -= beg->weight();
				if( val <= 0.0 ) break;
			}
			ASSERT(beg != end);
			return beg;
		}
		static const_iterator select_policy(const_iterator beg, const_iterator end, float_type wsum)
		{
			ASSERT( beg != end );
			if( (beg+1) == end ) return beg;
			float_type val = random_double()*wsum;
			for(;beg != end;++beg)
			{
				val -= beg->weight();
				if( val <= 0.0 ) break;
			}
			ASSERT(beg != end);
			return beg;
		}
		static const_iterator select_policy(const_iterator beg, const_iterator end, float_type wsum, typename std::vector<double>::const_iterator bit)
		{
			ASSERT( beg != end );
			if( (beg+1) == end ) return beg;
			float_type val = random_double()*wsum;
			for(;beg != end;++beg, ++bit)
			{
				val -= *bit;
				if( val <= 0.0 ) break;
			}
			ASSERTMSG(beg != end, "wsum: " << wsum << " val: " << val);
			return beg;
		}
		static size_type find_min_cost(w_type pwgt, float_pointer predarry, size_type classcnt, size_type y)
		{
			return 1;
		}
		static size_type find_min_cost(weight_pointer pwgt, float_pointer predarry, size_type classcnt, size_type y)
		{
			size_type i=0,j=0;
			for(;i<classcnt;++i)
			{
				if( std::abs( pwgt[i] - pwgt[y] ) < 1e-12 )
				{
					predarry[j] = i;
					++j;
				}
			}
			return j;
		}
		
	private:
		static size_type class_index(float_type* pbeg, float_type* pend)
		{
			size_type max_idx = 0;
			float_type max_val = -TypeUtil<float_type>::max();
			ASSERT( std::distance(pbeg, pend) > 1 );
			for(float_type* pcur=pbeg;pcur != pend;++pcur)
			{
				if( *pcur > max_val )
				{
					max_idx = size_type( std::distance(pbeg,pcur) );
					max_val = *pcur;
				}
			}
			return max_idx;
		}
		inline static float_type update_weights(iterator beg, iterator end, float_type b)
		{
			float_type sum=0.0f;
			for(;beg != end;++beg) 
			{
				beg->multiply( 1.0 - b );
				sum += beg->weight();
			}
			return sum;
		}
		size_type compute_costs(iterator curr, bag_iterator bbeg, example_iterator ecurr, w_type& wval)
		{
			size_type y;
			w_type wbeg[2];
			if( costTypeInt == 0 ) y = compute_optimal_costs(curr, bbeg, ecurr, wbeg);
			else y = compute_simulation_costs(curr, bbeg, ecurr, wbeg);
			wval = 1;
			return y;
		}
		size_type compute_costs(iterator curr, bag_iterator bbeg, example_iterator ecurr, weight_pointer wbeg)
		{
			if( costTypeInt == 0 ) return compute_optimal_costs(curr, bbeg, ecurr, wbeg);
			return compute_simulation_costs(curr, bbeg, ecurr, wbeg);
		}
		
	private:
		void allocate_weights(parent_iterator beg, parent_iterator end, size_type ycnt)
		{
			allocate_weights(beg, end, ycnt, beg->w_pointer());
		}
		void allocate_weights(parent_iterator beg, parent_iterator end, size_type ycnt, w_type){}
		void allocate_weights(parent_iterator beg, parent_iterator end, size_type ycnt, weight_pointer)
		{
			for(;beg != end;++beg)
				beg->w_pointer(::resize((weight_pointer)0, ycnt));
		}
		void deallocate_weights(parent_iterator beg, parent_iterator end)
		{
			deallocate_weights(beg, end, beg->w_pointer());
		}
		void deallocate_weights(parent_iterator beg, parent_iterator end, w_type){}
		void deallocate_weights(parent_iterator beg, parent_iterator end, weight_pointer)
		{
			for(;beg != end;++beg)
				::erase(beg->w_pointer());
		}
		static float_type setup_betas(std::vector<double>& betas, size_type n, float_type b)
		{
			float_type sum=b;
			betas.resize(n);
			std::fill(betas.begin(), betas.end(), b);
			for(size_type i=n-1;i>0;--i)
			{
				betas[i-1] = betas[i]*(1.0-b);
				sum+=betas[i-1];
			}
			return sum;
		}
		
	private:
		static void predict_class(const learner_type& learner, const_attribute_pointer a, float_type* p)
		{
			predict_class(learner, a, p, (typename learner_type::prediction_type)0);
		}
		static void predict_class(const learner_type& learner, const_attribute_pointer a, float_type* p, float_type* d)
		{
			learner.predict(a, p);
		}
		static void predict_class(const learner_type& learner, const_attribute_pointer a, float_type* p, float_type d)
		{
			d = learner.predict(a);
			if( learner.threshold() == 0.5f ) p[0] = 1.0-d;
			else p[0] = -d;
			p[1] = d;
		}
		
	public:
		/* Reads an SEARN model from the input stream.
		 *
		 * @param in an input stream.
		 * @param s a SEARN model.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, searn& s)
		{
			in >> s.attrn;
			if( in.get() != '\n' ) return s.fail(in, "Missing tab character in searn model");
			in >> s.committee();
			s.iterationInt = s.size();
			s.totalwgt = 0.0;
			for(iterator beg=s.begin(), end=s.end();beg != end;++beg) s.totalwgt += beg->weight();
			return in;
		}
		/* Writes an SEARN model to the output stream.
		 *
		 * @param out an output stream.
		 * @param s a SEARN model.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const searn& s)
		{
			out << s.attrn << "\n";
			out << s.committee();
			return out;
		}
		
	private:
		static void debug_out(std::ostream& out, size_type idx, int y, const_attribute_pointer px)
		{
			std::cerr << idx << "(" << y << "):";
			while(type_util::indexOf(*px) != -1 )
			{
				std::cerr << " " << type_util::indexOf(*px);
				++px;
			}
			std::cerr << std::endl;
		}
		template<class F>
		static void deallocate(F** to)
		{
			if( to != 0 ) ::erase(*to);
			::erase(to);
		}
		template<class F>
		static F** reallocate(F** to, unsigned int r, unsigned int c)
		{
			F* val = 0;
			if( to != 0 ) val = *to;
			to = ::resize(to, r);
			*to = val;
			*to = ::resize(*to, r*c);
			for(unsigned int i=1;i<r;++i) to[i] = to[i-1]+c;
			return to;
		}

	private:
		policy_type policy;
		float_type totalwgt;
		size_type attrn;
		
	private:
		int verboseTypeInt;
		int costTypeInt;
		size_type iterationInt;
		float_type betaFlt;
		argument_type iterationSel;
		
	private:// Training dataset
		pool_type examplePool;
		float_pointer ptmpflt;
		weight_pointer ptmpwgt;
		size_pointer pysize;
		float_type optwgt;
		mutable std::vector<double> betas;
		mutable float_type bwgt;
	};
};


#endif

