/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * sequence_policy.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_SEQUENCE_POLICY_HPP
#define _EXEGETE_SEQUENCE_POLICY_HPP

/** @file sequence_policy.hpp
 * 
 * @brief SEARN sequence policy
 * 
 * This file contains the sequence policy: 
 * 
 * - Loss: Hamming
 * - Optimal: Label next correctly
 * - Features: template parameter SF
 * - Actions: template parameter SF
 *
 * @ingroup ExegeteStructuredOutput
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{
	/** @brief SEARN sequence policy
	 * 
	 * This class defines SEARN sequence policy.
	 */
	template<class D, template<class> class SF>
	class sequence_policy : public SF<D>
	{
		typedef SF<D>							 feature_type;
		typedef D 								 dataset_type;
		typedef typename dataset_type::size_type size_type;
		typedef typename dataset_type::iterator  iterator;
		typedef size_type* 						 size_pointer;
		typedef double 							 float_type;
		
	public:
		/** Initializes the policy to the dataset.
		 * 
		 * @param dataset a dataset.
		 */
		void initialize(dataset_type& dataset)
		{
			feature_type::initialize(dataset);
		}
		
	public:
		/** Compute the cost of taking action y.
		 * 
		 * @param curr current state.
		 * @param y current action.
		 * @return cost of taking this action.
		 */
		float_type cost(iterator curr, size_type y)const
		{
/////////////////
			if( curr->y() == 0 )
			{
				if( y != size_type(curr->y()) ) return 0.25;
			}
/////////////////
			if( y != size_type(curr->y()) ) return 1.0;
			return 0.0;
		}
		/** Compute the loss of taking this sequence of actions.
		 * 
		 * @param beg start state.
		 * @param end end state.
		 * @param ybeg current sequence of actions.
		 * @return cost of taking this sequence of actions.
		 */
		float_type loss(iterator beg, iterator end, size_pointer ybeg)const
		{
			float_type sum = 0.0;
			for(;beg != end;++beg, ++ybeg)
			{
/////////////////
			if( beg->y() == 0 )
			{
				if( *ybeg != size_type(beg->y()) ) sum += 0.25;
			}
			else{
/////////////////
				if( *ybeg != size_type(beg->y()) ) sum += 1.0;
			}
			}
			return sum;
		}
		
	public:
		/** Compute features for inital state, initialize missing.
		 * 
		 * @param ebeg start of states.
		 * @param eend end of states.
		 * @param attrn number of attributes.
		 * @return start state.
		 */
		iterator initial_state(iterator ebeg, iterator eend)const
		{
			return ebeg;
		}
		/** Compute features for current state and choose the next state based
		 * on given action.
		 * 
		 * @param ebeg start of states.
		 * @param ecurr current state.
		 * @param y current action.
		 * @param attrn number of attributes.
		 * @return current state.
		 */
		iterator next_action(iterator ebeg, iterator ecurr, size_type y)const
		{
			return ecurr+1;
		}
		/** Compute features for current state and choose the next state based
		 * on given action.
		 * 
		 * @param ebeg start of states.
		 * @param ecurr current state.
		 * @param y current action.
		 * @param attrn number of attributes.
		 * @return current state.
		 */
		void compute_features(iterator ebeg, iterator ecurr, size_type y)const
		{
			feature_type::create_features(ebeg, ecurr, y);
		}
	};
	
};

#endif


