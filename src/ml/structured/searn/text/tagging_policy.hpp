/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * tagging_policy.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_TAGGING_POLICY_HPP
#define _EXEGETE_TAGGING_POLICY_HPP
#include "sequence_policy.hpp"

/** @file tagging_policy.hpp
 * 
 * @brief SEARN tagging features
 * 
 * This file contains tagging features.
 *
 * @ingroup ExegeteStructuredOutput
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


namespace exegete
{
	/** @brief SEARN sequence policy
	 * 
	 * This class defines tagging features.
	 */
	template<class D>
	class tagging_features
	{
		typedef D 								 			dataset_type;
		typedef typename dataset_type::type_util 			util_type;
		typedef typename dataset_type::size_type 			size_type;
		typedef typename dataset_type::class_type 			class_type;
		typedef typename dataset_type::iterator  			iterator;
		typedef typename dataset_type::attribute_pointer 	attribute_pointer;
		
	public:
		/** Constructs a tagging features.
		 */
		tagging_features() : classn(0), attrn(0)
		{
		}
		/** Initializes the policy to the dataset.
		 * 
		 * @param dataset a dataset.
		 */
		void initialize(const dataset_type& dataset)
		{
			classn = dataset.classCount();
			attrn = dataset.attributeCount();
		}
		
	public:
		/** Compute features for current state and choose the next state based
		 * on given action.
		 * 
		 * @param ebeg start of states.
		 * @param ecurr current state.
		 * @param attrn number of attributes.
		 */
		void create_features(iterator ebeg, iterator ecurr, class_type y)const
		{
			ASSERT((long(attrn+1)+long(classn+1)) < long(TypeUtil<class_type>::max()));
			attribute_pointer p=ecurr->x();
			while( util_type::indexOf(*p) != -1 && size_type(util_type::indexOf(*p)) <= attrn ) ++p;
			if( ebeg != ecurr )
			{
				attribute_pointer t=(ecurr-1)->x();
				class_type offset = (y+1)*attrn;
				for(;util_type::indexOf(*p) != -1 && size_type(util_type::indexOf(*p)) <= attrn; ++t, ++p)
				{
					ASSERTMSG( size_type(util_type::indexOf(*t)) < attrn, util_type::indexOf(*t) << " < " << attrn );
					util_type::indexOf(*p)=util_type::indexOf(*t)+offset;
					util_type::valueOf(*p)=util_type::valueOf(*t);
					ASSERTMSG( size_type(util_type::indexOf(*p)) < attribute_count(), util_type::indexOf(*p) << " < " << attribute_count());
				}
			}
			util_type::indexOf(*p) = -1;
		}
		
	public:
		/** Get number of attributes.
		 * 
		 * @param old old number of attributes.
		 * @return number of attributes.
		 */
		size_type attribute_count(size_type old)const
		{
			return old*2;
		}
		/** Get number of attributes.
		 * 
		 * @return number of attributes.
		 */
		size_type attribute_count()const
		{
			return attrn*(classn+1);
		}
		/** Get number of classes.
		 * 
		 * @return number of classes.
		 */
		size_type class_count()const
		{
			return classn;
		}
		
	private:
		size_type classn;
		size_type attrn;
	};
	
	/** @brief SEARN tagging policy.
	 * 
	 * This class defines a tagging policy, which combines tagging features
	 * with the sequence policy.
	 */
	template<class D>
	class tagging_policy : public sequence_policy<D, tagging_features>{};
};


#endif


