/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Learner.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_LEARNER_H
#define _EXEGETE_LEARNER_H
#include "ArgumentMap.h"
#include "ConcreteExampleSet.h"
#include "TuneParameterTree.h"
#include "ArgumentUtil.h"
#include "ErrorCodes.h"
#include <iostream>
#include <string>
#include "mpi/mpi_def.hpp"
#include "version_utility.hpp"

/** Defines a macro to concatenating two strings.
 * 
 * @param x a string.
 * @param y an integer.
 */
#define LEARNER_VERSION1(x, y) x #y
/** Defines a macro to concatenating two strings.
 * 
 * @param x a string.
 * @param y an integer.
 */
#define LEARNER_VERSION(x, y) LEARNER_VERSION1(x, y)
/** Defines a macro to determine the name, version of the learner
 * 
 * @param C a learner class type.
 */
#define NAME_VERSION(C) C::name()+" v"+version_string(C::version())

/** @file Learner.h
 * @brief A base learner
 * 
 * This file contains the Learner class template.
 *
 * @ingroup ExegeteUtilClassifiers
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @defgroup ExegeteUtilClassifiers Classifier Utilities
 * 
 *  This group holds all the multi-class classifier files.
 */

namespace exegete
{
	/** @brief Determines a base learner
	 * 
	 * This class determines a base learner at compile time.
	 */
	template<class T>
	struct base_learner
	{
		/** Defines a base learner as a result. **/
		typedef T result;
	};


	/** @brief A base learning algorithm
	 * 
	 * This class defines a base learning algorithm.
	 */
	class Learner
	{
	public:
		/** Flags special learning type. **/
		enum{ ATTR8=0, LAZY=0, USE_TRAINSET=0, TUNE=0, EVAL=0, FAST_TUNE=0, GRAPHABLE=0, SELECT=0, STRUCT=0 };
		/** Flags dataset type. **/
		enum{ NOMINAL=1, MISSING=2, MIL=4, SEMI=8, BINARY=16, NORMALIZED=32, NOMIL=64 };
		/** Defines a tunable argument iterator. **/
		typedef TuneParameterTree&	argument_iterator;
		/** Defines a dummy prediction vector. **/
		typedef int					prediction_vector;
		/** Defines a tunable argument type. **/
		typedef TuneParameterTree	argument_type;

	public:
		/** Constructs a default learning algorithm.
		 *
		 * @param d a dataset flag.
		 */
		Learner(int d=0) : errmsg(0), datasetType(d), selectParam(false)
#ifdef _MPI
		, pmpitree(0)
#endif
		{
		}
		/** Destructs a default learning algorithm.
		 */
		~Learner()
		{
		}

	public:
		/** Copies arguments in a base learner.
		 * 
		 * @param ref a base learner.
		 */
		void argument_copy(const Learner& ref)
		{
			selectParam=ref.selectParam;
		}
		/** Get the dataset extension expected by the learning algorithm.
		 * 
		 * @return an extension string.
		 */
		const char* ext()const
		{
			if( (datasetType&MISSING) != MISSING && 
				(datasetType&NOMINAL) != NOMINAL &&
				(datasetType&NORMALIZED) != NORMALIZED
				)
			{
				return "std";
			}
			return "dst";
		}
		/** Setups a concrete example set.
		 *
		 * @param dataset a concrete dataset modify.
		 */
		template<class X, class Y>
		void setup(ConcreteExampleSet<X,Y>& dataset, bool testset=false)
		{
		}
		/** Verifies dataset is appropriate for learning algorithm.
		 *
		 * @param dataset a dataset to check.
		 * @param name name of learning algorithm.
		 * @param level level of tests to use.
		 * @return an error message or NULL.
		 */
		template<class X, class Y, class W>
		const char* check(const ExampleSet<X,Y,W>& dataset, const std::string& name, int level)const
		{
			const char* msg;
			if( (datasetType&NOMIL) == NOMIL ) 		if((msg=assert_has_nobags(dataset, name)) != 0) return msg;
			if( (datasetType&MIL) == MIL )			if( (msg=assert_has_bags(dataset, name)) != 0 ) return msg;
			if( (datasetType&BINARY) == BINARY )	if( (msg=assert_class(dataset, name, 2)) != 0 ) return msg;
			if( level > 0 )
			{
				if( (datasetType&MISSING) == MISSING )	if( (msg=assert_not_missing(dataset, name)) != 0 ) return msg;
				if( (datasetType&NOMINAL) == NOMINAL  )	if( (msg=assert_not_nominal(dataset, name)) != 0 ) return msg;
				if( (datasetType&NORMALIZED) == NORMALIZED) 	if( (msg=assert_normalized(dataset, name)) != 0 ) return msg;
			}
			if( level > 1 )
			{
				if( (datasetType&SEMI) )	if( (msg=assert_class_not_missing(dataset, name)) != 0 ) return msg;
			}
			return 0;
		}
		/** Gets an error message.
		 *
		 * @return an error message or NULL.
		 */
		const char* errormsg()const
		{
			return errmsg;
		}
		/** Gets a validation type of a learning algorithm.
		 *
		 * @return name of validation type.
		 */
		static std::string validationType()
		{
			return "";
		}
		/** Clears all dynamically allocated global memory in the parent learner.
		 */
		static void cleanall()
		{
		}
		/** Tests whether the learning algorithm is selecting parameters.
		 *
		 * @return true if learning algorithm is selecting parameters.
		 */
		bool isSelectingParameters()const
		{
			return selectParam;
		}
		/** Sets a flag indicating that parameters are currently being selected.
		 */
		void startSelectingParameters()
		{
			selectParam=true;
		}
		/** Sets a flag indicating that parameters are not currently being selected.
		 */
		void stopSelectingParameters()
		{
			selectParam=false;
		}
		/** Gets an iterator to first tunable argument.
		 *
		 * @return iterator to tunable argument.
		 */
		argument_iterator saved()
		{
			static TuneParameterTree sel;
			ASSERT(false);
			return sel;
		}
		/** Setups a dataset for a learning algorithm.
		 * 
		 * @note does nothing.
		 * 
		 * @param dummy a dummy dataset.
		 */
		template<class T>
		static void setup_dataset(const T& dummy)
		{
		}

	protected:
		/** Add a test for dataset compatibility.
		 * 
		 * @param t a dataset compatibility test.
		 */
		void add_test(int t)
		{
			datasetType |= t;
		}
		/** Remove a test for dataset compatibility.
		 * 
		 * @param t a dataset compatibility test.
		 */
		void remove_test(int t)
		{
			datasetType &= ~t;
		}
		/** Sets an error message.
		 *
		 * @param m an error message or NULL.
		 */
		void errormsg(const char* m)const
		{
			errmsg = m;
		}
		/** Sets an error message and flags the stream as failed.
		 *
		 * @param s a stream .
		 * @param m an error message or NULL.
		 */
		void errormsg(std::ios& s, const char* m)const
		{
			if( m != 0 )
			{
				errmsg = m;
				s.setstate( std::ios::failbit );
			}
		}
		/** Sets an error message and flags the stream as failed.
		 *
		 * @param i a stream.
		 * @param m an error message or NULL.
		 * @return a stream.
		 */
		std::istream& fail(std::istream& i, const char* m)const
		{
			errormsg(i, m);
			return i;
		}
		/** Sets an error message and flags the stream as failed.
		 *
		 * @param o a stream.
		 * @param m an error message or NULL.
		 * @return a stream.
		 */
		std::ostream& fail(std::ostream& o, const char* m)const
		{
			errormsg(o, m);
			return o;
		}
		/** Normalizes weights in a learning algorithm.
		 * 
		 * @note Does nothing; used for compatibility.
		 * 
		 * @param w a new weight factor.
		 */
		void normalization(float w)
		{
		}
		
	public:
		/** Set the restart flag.
		 * 
		 * @param val a restart flag.
		 * @return an error message or NULL.
		 */
		const char* set_restart(int val)const
		{
			return 0;
		}
		/** Set the size of the committee.
		 *
		 * @note does nothing
		 * 
		 * @param n the new size.
		 */
		template<class U>
		void resize_saved(U n)
		{
		}
		/** Save the currnet learner and dataset.
		 * 
		 * @note does nothing
		 * 
		 * @param t a dataset.
		 */
		template<class U>
		void save_next(U& t)
		{
		}
		//Should document?
		MPI_OPTION("")
		/** Get the numer of classes.
		 * 
		 * @return 2
		 */
		unsigned int classCount()const
		{
			return 2;
		}
		
	protected:
		/** Test if dataset is normalized; if not return an error message.
		 * 
		 * @param dataset a dataset to test.
		 * @param name name of learning algorithm.
		 * @return an error message or NULL.
		 */
		template<class X, class Y, class W>
		static const char* assert_normalized(const ExampleSet<X,Y,W>& dataset, const std::string& name)
		{
			if( !dataset.isnormalized() ) 
			{
				return ERRORCODE(EXEGETE_REQUIRES_NORMALIZED, name << " requires normalized dataset");
			}
			return 0;
		}
		/** Test if dataset has missing classes values; if so return an error message.
		 * 
		 * @param dataset a dataset to test.
		 * @param name name of learning algorithm.
		 * @return an error message or NULL.
		 */
		template<class X, class Y, class W>
		static const char* assert_class_not_missing(const ExampleSet<X,Y,W>& dataset, const std::string& name)
		{
			if( dataset.countClassMissing() > 0 ) return ERRORCODE(EXEGETE_MISSING_CLASSES, name << " requires no missing classes; found " << dataset.countClassMissing() << " missing classes");
			return 0;
		}
		/** Test if dataset has missing examples in a class; if so return an error message.
		 * 
		 * @param dataset a dataset to test.
		 * @param name name of learning algorithm.
		 * @param maxclass maximum number of classes for learner.
		 * @return an error message or NULL.
		 */
		template<class X, class Y, class W>
		static const char* assert_class(const ExampleSet<X,Y,W>& dataset, const std::string& name, unsigned maxclass)
		{
			if( dataset.classCount() > maxclass ) return ERRORCODE(EXEGETE_CLASS_COUNT, name << " requires no more than " << maxclass << " classes; found " << dataset.classCount() << " classes");
			for(unsigned int i=0;i<dataset.classCount();++i)
				if( dataset.countClass(i) == 0 ) return ERRORMSG(name << " requires each class to have more than 0 examples; found " << dataset.countClass(i) << " for class " << dataset.classAt(i) << " (" << i << ")");
			return 0;
		}
		/** Test if dataset has bags; if not return an error message.
		 * 
		 * @param dataset a dataset to test.
		 * @param name name of learning algorithm.
		 * @return an error message or NULL.
		 */
		template<class X, class Y, class W>
		static const char* assert_has_bags(const ExampleSet<X,Y,W>& dataset, const std::string& name)
		{
			if( dataset.bagCount() == 0 ) return ERRORCODE(EXEGETE_REQUIRES_BAGS, name << " must have bags.");
			return 0;
		}
		/** Test if dataset has bags; if so return an error message.
		 * 
		 * @param dataset a dataset to test.
		 * @param name name of learning algorithm.
		 * @return an error message or NULL.
		 */
		template<class X, class Y, class W>
		static const char* assert_has_nobags(const ExampleSet<X,Y,W>& dataset, const std::string& name)
		{
			if( dataset.bagCount() > 0 ) return ERRORCODE(EXEGETE_REQUIRES_NOBAGS, name << " must not have bags.");
			return 0;
		}
		/** Test if dataset has nominal attributes; if so return an error message.
		 * 
		 * @param dataset a dataset to test.
		 * @param name name of learning algorithm.
		 * @return an error message or NULL.
		 */
		template<class X, class Y, class W>
		static const char* assert_not_nominal(const ExampleSet<X,Y,W>& dataset, const std::string& name)
		{
			typedef typename ExampleSet<X,Y,W>::const_header_iterator const_header_iterator;
			for(const_header_iterator hcurr=dataset.header_begin(),hend=dataset.header_end();hcurr != hend;++hcurr)
				if( hcurr->isnominalnotbinary() ) return ERRORCODE(EXEGETE_REQUIRES_NONOMINAL, name << " cannot have nominal attributes; found in column: " << (int)std::distance(dataset.header_begin(), hcurr));
			return 0;
		}
		/** Test if dataset has missing attributes; if so return an error message.
		 * 
		 * @param dataset a dataset to test.
		 * @param name name of learning algorithm.
		 * @return an error message or NULL.
		 */
		template<class X, class Y, class W>
		static const char* assert_not_missing(const ExampleSet<X,Y,W>& dataset, const std::string& name)
		{
			typedef typename ExampleSet<X,Y,W>::const_header_iterator const_header_iterator;
			for(const_header_iterator hcurr=dataset.header_begin(),hend=dataset.header_end();hcurr != hend;++hcurr)
				if( hcurr->missing() ) return ERRORCODE(EXEGETE_REQUIRES_NOMISSING, name << " cannot have missing attributes; found in column: " << (int)std::distance(dataset.header_begin(), hcurr));
			return 0;
		}

	private:
		mutable const char* errmsg;
		int datasetType;
		bool selectParam;
		
#ifdef _MPI
	public:
		/** Set an MPI tree for learner.
		 * 
		 * @param t an MPI tree.
		 */
		void set_mpi_tree(mpi_tree& t)
		{
			pmpitree = &t;
		}
		/** Get an MPI tree from learner.
		 * 
		 * @return an MPI tree.
		 */
		mpi_tree& get_mpi_tree()
		{
			return *pmpitree;
		}
		/** Finalize MPI learner.
		 * 
		 */
		void mpi_finalize()
		{
		}
		/** Update MPI learner.
		 * 
		 * @param dummy a dummy variable.
		 * @param args a vector of floats.
		 */
		template<class T>
		void mpi_update(T& dummy, std::vector<float>& args)
		{
		}
	private:
		mpi_tree* pmpitree;
#endif
	};

	/** @brief Visits static methods in a learner hierarchy
	 * 
	 * This class defines a visitor for static methods in a learner hierarchy:
	 * 	- a argument copy method
	 */
	template<class T, class P=typename T::parent_type>
	struct static_learner_vistor
	{
		/** Copies arguments from one learner to another.
		 * 
		 * @param to a destination learner.
		 * @param from a source learner.
		 */
		static void argument_copy(T& to, const T& from)
		{
			to.argument_copy(from);
			static_learner_vistor<P>::argument_copy(to, from);
		}
		/** Builds a name of a learning algorithm.
		 * 
		 * @return full name.
		 */
		static std::string name()
		{
			std::string nm = T::name();
			if( nm != "" ) return nm + " on " + static_learner_vistor<P>::name();
			return static_learner_vistor<P>::name();
		}
	};

	/** @brief Visits static methods in a learner hierarchy
	 * 
	 * This class defines a visitor for static methods in a learner hierarchy:
	 * 	- a argument copy method
	 */
	template<class T>
	struct static_learner_vistor<T,void>
	{
		/** Copies arguments from one learner to another.
		 * 
		 * @param to a destination learner.
		 * @param from a source learner.
		 */
		static void argument_copy(T& to, const T& from)
		{
			to.argument_copy(from);
		}
		/** Builds a name of a learning algorithm.
		 * 
		 * @return full name.
		 */
		static std::string name()
		{
			return T::name();
		}
	};

	/** @brief Utility assigns pod arguments
	 * 
	 * This class defines methods to copy pod type arguments.
	 */
	template<class T, int I=TypeUtil<T>::ispod >
	struct ArgumentAssignUtil
	{
		/** Copy argument to argument vector.
		 * 
		 * @param vec a vector of arguments floats.
		 * @param val an argument value.
		 * @param nm an argument name.
		 */
		static void set(std::vector<float>& vec, T val, std::string nm)
		{
			vec.push_back(float(val));
		}
		/** Copy argument to argument vector.
		 * 
		 * @param it an iterator to a vector of floats.
		 * @param val a destination argument value.
		 * @param nm an argument name.
		 */
		static void set(std::vector<float>::const_iterator& it, T& val, std::string nm)
		{
			val = T(*it);
			++it;
		}
	};
	/** @brief Utility assigns non-pod tune parameter tree arguments.
	 * 
	 * This class defines methods to copy non-pod tune parameter tree arguments.
	 */
	template<>
	struct ArgumentAssignUtil<TuneParameterTree, 0>
	{
		/** Copy argument to argument vector.
		 * 
		 * @param vec a vector of arguments floats.
		 * @param val an argument value.
		 * @param nm an argument name.
		 */
		static void set(std::vector<float>& vec, TuneParameterTree& val, std::string nm)
		{
			val.save(vec);
		}
		/** Copy argument to argument vector.
		 * 
		 * @param it an iterator to a vector of floats.
		 * @param val a destination argument value.
		 * @param nm an argument name.
		 */
		static void set(std::vector<float>::const_iterator& it, TuneParameterTree& val, std::string nm)
		{
			val.load(it);
		}
	};
	/** @brief Utility assigns non-pod arguments.
	 * 
	 * This class defines methods to copy non-pod arguments.
	 */
	template<class T>
	struct ArgumentAssignUtil<T, 0>
	{
		/** Copy argument to argument vector.
		 * 
		 * @param vec a vector of arguments floats.
		 * @param val an argument value.
		 * @param nm an argument name.
		 */
		static void set(std::vector<float>& vec, T& val, std::string nm){}
		/** Copy argument to argument vector.
		 * 
		 * @param it an iterator to a vector of floats.
		 * @param val a destination argument value.
		 * @param nm an argument name.
		 */
		static void set(std::vector<float>::const_iterator it, T& val, std::string nm){}
	};

	/** @brief Specializes an argument utility to a vector of floats.
	 * 
	 * This class defines a specialization of an argument utility to a vector of floats.
	 */
	template<>
	class ArgumentUtil< std::vector<float> >
	{
	public:
		/** Add parameter to vector of floats.
		 * 
		 * @param vec a vector of floats.
		 * @param val a float value.
		 * @param nm name unused.
		 * @param ds description unused.
		 * @param pf an unused prefix.
		 * @param l an unused level.
		 */
		template<class U>
		static void set(std::vector<float>& vec, U& val, std::string nm, std::string ds, std::string pf, int l)
		{
			ArgumentAssignUtil<U>::set(vec, val, pf.empty()?nm:ds);
		}
		/** Does nothing.
		 * 
		 * @param vec a vector of floats.
		 * @param nm unused.
		 * @param l an unused level.
		 */
		static void set(std::vector<float>& vec, std::string nm, int l){}
	};
	
	/** @brief Specializes an argument utility to an iterator to a vector of floats.
	 * 
	 * This class defines a specialization of an argument utility to an iterator to a vector of floats.
	 */
	template<>
	class ArgumentUtil< std::vector<float>::const_iterator >
	{
	public:
		/** Loads the parameters to the learning algorithm.
		 * 
		 * @param it an iterator to a vector of floats.
		 * @param val a reference to any value.
		 * @param nm unused.
		 * @param ds unused.
		 * @param pf unused.
		 * @param l an unused level.
		 */
		template<class U>
		static void set(std::vector<float>::const_iterator& it, U& val, std::string nm, std::string ds, std::string pf, int l)
		{
			ArgumentAssignUtil<U>::set(it, val, pf.empty()?nm:ds);
		}
		/** Does nothing.
		 * 
		 * @param it an iterator.
		 * @param nm unused.
		 * @param l an unused level.
		 */
		static void set(std::vector<float>::const_iterator it, std::string nm, int l){}
	};
	
	namespace detail
	{
		/**@brief Extracts weight type 
		 * 
		 * This class extracts weight type.
		 */
		template<class T>
		struct weight_adapter
		{
			/** Defines weight type. **/
			typedef T weight_type;
		};
		/** @brief Extracts weight type
		 *  
		 * This class extracts weight type.
		 */
		template<>
		struct weight_adapter<void*>
		{
			/** Defines weight type. **/
			typedef void weight_type;
		};
		/**@brief Selects a prediction type 
		 * 
		 * This class selects a prediction type.
		 */
		template<class F, class T>
		struct prediction_adapter
		{
			/** Defines as binary **/
			enum{ binary = Learner::BINARY };
			/** Defines weight type. **/
			typedef F result;
		};
		/** @brief Selects a prediction type
		 *  
		 * This class selects a prediction type.
		 */
		template<class F, class T>
		struct prediction_adapter<F, T*>
		{
			/** Defines as multi-class **/
			enum{ binary = 0 };
			/** Defines weight type. **/
			typedef F* result;
		};
	};
};


#endif


