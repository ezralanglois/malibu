/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * ModelValidation.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MODELVALIDATION_H
#define _EXEGETE_MODELVALIDATION_H
#include "PartitionValidation.h"
#include "fileutil.h"
//Graphs
#include "GraphFormatFactory.h"
#include "BuildGraphVisitor.h"
#include "WillowConservedVisitor.h"
#include "GraphPredVisitor.h"
#include "PredictionVisitor.h"
#include "rule_visitor.hpp"
#include "complexity_visitor.hpp"

/** Defines the version of the model validation algorithm. 
 * @todo add to group
 */
#define _MODELVALIDATION_VERSION 101000

/** @file ModelValidation.h
 * @brief Model validation algorithms
 * 
 * This file contains the ModelValidation class.
 *
 * @ingroup ExegeteValidation
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page arguments List of Arguments
 *
 *  @section Validation Model Validation
 *	- trnpfx: prefix for the output training set filename
 *	- tstpfx: prefix for the output testing set filename
 *	- pinc: progressive validation increment
 *	- forcepos: force instances in positive bag as positive
 *	- stratify: stratify training and test set distribution?
 *	- nfold: number of paritions/size of training set
 *	- alpha: sample without replacement proportion, 0.0 for sample with replacement
 *	- runs: the number of times to repeat the validation
 *	- type: type of partition
 *		-# None: doe not parition dataset or perform validation.
 *		-# Cross-validation: Parition the dataset into n sets, each is used once for testing and the n-1 rest for training
 *		-# Holdout: Parition the dataset into n sets, one is used for testing, the rest for training
 *		-# Invert-Holdout: Parition the dataset into n sets, one is used for training, the rest for testing
 *		-# Sample: Drawn a random sample of some proportion size
 *		-# Progressive: Start with n-fold training set and progressiving predict and add examples.
 *		-# Invert-progressive: Start with n-fold inverted training set and progressiving predict and add examples.
 */

/** @defgroup ExegeteValidation Learner Validation
 * 
 *  This group holds all learner validation files.
 */

namespace exegete
{
	/** @brief Model validation algorithms
	 * 
	 * This class template performs validation on machine learning model and/or 
	 * partitions a dataset.
	 *
	 * @todo add fixed size cv
	 * @todo add fixed size sampling
	 */
	template<
		class L, 
		class M, 
		class D=typename DatasetHandler<L>::dataset_type, 
		template<class>class TP = DefaultTrainPolicy 
	>
	class ModelValidation : public PartitionValidation<M, TP>
	{
		typedef PartitionValidation<M, TP> handler_type;
		typedef typename base_learner<L>::result base_type;
	public:
		/** Defines template parameter L as learner type. **/
		typedef L learner_type;
		/** Defines template parameter D as dataset type. **/
		typedef D dataset_type;
		/** Defines template parameter M as measure type. **/
		typedef M measure_type;
		/** Defines a dataset handler. **/
		typedef DatasetHandler< base_type > dataset_handler;
		/** Defines an attribute header. **/
		typedef typename dataset_type::attribute_header attribute_header;

	public:
		/** Enumerates validation/partition types. **/
		enum{SF='o',TS='t',GR='g'};
		
	private:
		typedef typename learner_type::dataset_type learnset_type;
		typedef typename learner_type::testset_type testset_type;
		typedef typename learner_type::float_type float_type;
		typedef typename learnset_type::class_type class_type;
		typedef typename learnset_type::attribute_type attribute_type;
		typedef typename learner_type::prediction_vector prediction_vector;
		enum{IS_SELF_VALIDATING=(IsSameType<prediction_vector,int>::value==0)};
		typedef std::vector< std::string > string_vector;
		typedef GraphFormatFactory graph_factory;
		typedef typename dataset_handler::dataset_type readable_dataset;
		typedef prediction< typename M::prediction_type, class_type> pred_value;
		typedef std::vector< pred_value > prediction_array;

	public:
		/** Constructs a model validation.
		 */
		ModelValidation(int t=0) : rulePercFlt(0.8f), saveBool(false), complexityInt(0)
		{
		}
		/** Constructs a model validation.
		 *
		 * @param map an argument map.
		 * @param t default validation type.
		 */
		ModelValidation(ArgumentMap& map, int t=0) : rulePercFlt(0.8f), saveBool(false), complexityInt(0)
		{
			map(NAME_VERSION(ModelValidation));
			init(map, t);
		}
		/** Destructs a model validation.
		 */
		~ModelValidation()
		{
		}
		
	public:
		/** Assigns a copy of a model validation.
		 * 
		 * @param mv a model validation.
		 * @return a reference to this object.
		 */
		ModelValidation& operator=(const ModelValidation& mv)
		{
			handler_type::operator=(mv);
			rulePercFlt = mv.rulePercFlt;
			rulefile = mv.rulefile;
			predfile = mv.predfile;
			testfilename = mv.testfilename;
			gmodels = mv.gmodels;
			//gfactory = mv.gfactory;
			//testset = mv.testset;
			saveBool = mv.saveBool;
			complexityInt = mv.complexityInt;
			return *this;
		}

	public:
		/** Initalize parameters for validation handler.
		 * 
		 * @param map an initializer.
		 * @param t a parameter flag.
		 * @param prefix an argument prefix.
		 */
		template<class U>
		void init(U& map, int t=0, std::string prefix="")
		{
			if( t >= 0 )
			{
				std::string addit;
				if( IS_SELF_VALIDATING ) addit = std::string(";")+std::string(learner_type::validationType())+std::string(":o");
				if( learner_type::GRAPHABLE ) addit += ";Graph:g";
				addit += (prefix=="")?";Testset:t":";Testset:t";//;None:n";
				if( prefix == "" ) handler_type::type(TS);
				if( prefix != "" && learner_type::TUNE==0 ) handler_type::type(handler_type::NO);
				
				handler_type::init(map, t>=0?L::STRUCT:t, prefix, addit);
				arginit(map, handler_type::modelfile,     prefix, "model", 		  std::string("a file that holds or will hold a learning model"));
				arginit(map, handler_type::pfxalgoInt,    prefix, "prefix_model", std::string("prefix model will learning algorithm name?"), ArgumentMap::ADDITIONAL);
				arginit(map, testfilename,  			  prefix, "test",  		  std::string("a file that holds the test set"));
				if( prefix == "" )
				arginit(map, handler_type::restartBool,	  prefix, "restart",	  "use check points to restart an algorithm", ArgumentMap::ADDITIONAL);
				if( learner_type::GRAPHABLE && prefix == "" )
				{
					arginit(map, NAME_VERSION(GraphFormatFactory), ArgumentMap::ADDITIONAL);
					arginit(map, complexityInt, prefix, "complexity", "write complexity not graph?", ArgumentMap::ADDITIONAL);
					arginit(map, rulePercFlt, prefix, "support", "support for rule", ArgumentMap::ADDITIONAL);
					arginit(map, rulefile, prefix, "rulefile", "contains all rules of a specific support", ArgumentMap::ADDITIONAL);
					arginit(map, predfile, prefix, "predmapfile", "file to hold mapped examples to node predictions", ArgumentMap::ADDITIONAL);
					arginit(map, gmodels,  prefix, "gmodels", "list of model files", ArgumentMap::ADDITIONAL);
					gfactory.init(map);
				}
			}
		}
		/** Get the name of the algorithm.
		 * 
		 * @return Validation
		 */
		static std::string name()
		{
			return "Validation";
		}
		/** Get the version of the algorithm.
		 * 
		 * @return version
		 */
		static int version()
		{
			return _MODELVALIDATION_VERSION;
		}

	public:
		/** Initializes the learner and measure.
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a dataset.
		 * @param measure a measure.
		 * @param runs number of runs (repeats).
		 * @param folds number of folds.
		 * @return an error message or NULL.
		 */
		//template<class L, class D>
		static const char* init(L& learner, D& dataset, M& measure, unsigned int runs, unsigned int folds=1)
		{
			return handler_type::init(learner, dataset, measure, runs, folds);
		}
		/** Validates a learning algorithm on the given dataset with the given measure.
		 *	-# Cross-validation: Parition the dataset into n sets, each is used once for testing and the n-1 rest for training
		 *
		 * @param learner the learning algorithm.
		 * @param measure a class to store or measures a set of predictions.
		 * @param dataset a dataset to partition for training and testing.
		 * @return an error message or NULL.
		 */
		const char* validate(learner_type& learner, measure_type& measure, dataset_type& dataset)
		{
			const char* msg;
			if( is_graphing(handler_type::type())) return validate_graph(learner, dataset, measure);
			if( is_testing(handler_type::type()) ) return validate_test(learner, dataset, measure);
			if( dataset.empty() ) return ERRORMSG("Validation methods other than Testset require training set; currently using " << toString() );
			if( is_selfvalidating(handler_type::type()) )
			{
				prediction_vector predictions(0);
				learnset_type trainset(dataset); trainset.assign(dataset);
				return validate_self(learner, trainset, measure, predictions);
			}
			if( (msg=handler_type::validate(learner, measure, dataset)) != 0 ) return msg;
			return 0;
		}
		/** Enable the algorithm to write restart files.
		 * 
		 * @param learner a learning algorithm.
		 * @return an error message or NULL.
		 */
		const char* set_restart(learner_type& learner)const
		{
			const char* msg;
			if( (msg=learner.set_restart(handler_type::restart())) != 0 ) return msg;
			return handler_type::check_restart();
		}
		/** Validate a learner over a testset for a specific parameter.
		 * 
		 * @param learner a learning algorithm.
		 * @param measure destination measure.
		 * @param dataset a source dataset.
		 * @return an error message or NULL.
		 */
		const char* validate_testset(learner_type& learner, measure_type& measure, dataset_type& dataset)
		{
			handler_type::init_measure(learner, testset, measure);
			handler_type::evaluate(learner, testset, measure, learner);
			handler_type::final_measure(learner, measure);
			return 0;
		}
		/** Validate a saved learning algorithm for specific parameter.
		 * 
		 * @param learner a learning algorithm.
		 * @param measure destination measure.
		 * @param dataset a source dataset.
		 * @return an error message or NULL.
		 */
		const char* validate_saved(learner_type& learner, measure_type& measure, dataset_type& dataset)
		{
			ASSERT(dataset.size() > 0);
			handler_type::init_measure(learner, dataset, measure);
			ASSERTMSG(learner.saved_begin() != learner.saved_end(), "No saved learners");
			for(typename learner_type::saved_iterator beg = learner.saved_begin(), end = learner.saved_end();beg != end;++beg)
			{
				beg->argument() = learner.argument();
				ASSERTMSG(beg->testset().size() > 0, "Testset size 0");
				handler_type::evaluate(*beg, beg->testset(), measure, *beg);
			}
			handler_type::final_measure(learner, measure);
			return 0;
		}
		/** Gets a string representation of the validation type.
		 *
		 * @return validation string.
		 */
		std::string toString()const
		{
			if( is_extended(handler_type::type()) ) return typeName(handler_type::type())+" validation";
			return handler_type::toString();
		}
		/** Set the validation save flag.
		 * 
		 * @param s whether to save learner and testset.
		 */
		void save(bool s)
		{
			saveBool = s;
		}
		
	protected:
		/** Build a graph of the learning algorithm.
		 *
		 * @param learner a learning algorithm.
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		const char* validate_graph(learner_type& learner, dataset_type& dataset, measure_type& measure)
		{
			typedef WillowConservedVisitor<attribute_type,class_type> conserved_visitor;
			typedef GraphPredVisitor< readable_dataset > pred_visitor;
			typedef PredictionVisitor< readable_dataset > prediction_visitor;
			typedef BuildGraphVisitor<attribute_type, class_type> graph_visitor;
			typedef rule_visitor<attribute_header> rule_visitor_t;
			const char* msg;
			if( gfactory.file().empty() ) return ERRORMSG("Graph output file empty");
			learnset_type trainset(dataset); trainset.assign(dataset);
			if( (msg = train(learner, trainset)) != 0 ) return msg;
			if( learner.empty() ) return ERRORMSG("Empty learning model");
			if( complexityInt == 1 )
			{
				complexity_visitor cmplex;
				learner.accept(cmplex);
				if( (msg=writefile(gfactory.file(), cmplex)) != 0 ) return msg;
				return 0;
			}
			Graph graph;
			const attribute_header* header = (dataset.empty()?(const attribute_header*)&testset:(const attribute_header*)&dataset);
			graph_visitor visit(graph, *header);
			conserved_visitor conserveVisit;
			rule_visitor_t ruleVisit(*header, rulePercFlt);
			learner.accept(visit);
			if( !gmodels.empty() ) learner.accept(conserveVisit);
			learner_type tmplearn;
			for(unsigned int i=0;i<gmodels.size();++i)
			{
				if( (msg=readmodel(gmodels[i], tmplearn, testset)) != 0 ) return msg;
				tmplearn.accept(conserveVisit);
				tmplearn.accept(ruleVisit);
			}
			if( !rulefile.empty() )
			{
				if( (msg=writefile(rulefile, ruleVisit)) != 0 ) return msg;
			}
			if( !gmodels.empty() ) conserveVisit.setup(graph);
			if( testset.empty() && !testfilename.empty() )
			{
				if( (msg=dataset_handler::handler()->read(learner, testset, dataset, testfilename)) != 0 ) return msg;	
				pred_visitor predvisit(graph, testset);
				learner.accept(predvisit);
				prediction_visitor predictionvisit(testset);
				learner.accept(predictionvisit);
				if( !predfile.empty() && (msg=writefile(predfile, predictionvisit)) != 0 ) return msg;
			}
			return gfactory.write(graph);
		}
		/** Validates the learning algorithm over a separate test set.
		 *
		 * @param learner a learning algorithm.
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		const char* validate_test(learner_type& learner, dataset_type& dataset, measure_type& measure)
		{
			const char* msg;
			if( dataset.empty() && testfilename.empty() ) return ERRORMSG("Requires training or test file.");
			else if( handler_type::modelfile.empty() && testfilename.empty() ) return ERRORMSG("Requires model file or test file.");
			learnset_type trainset(dataset); trainset.assign(dataset);
			if( (msg = train(learner, trainset)) != 0 ) return msg;
			ASSERT(!learner.empty());
			if( testset.empty() && !testfilename.empty() )
			{
				if( (msg=dataset_handler::handler()->read(learner, testset, dataset, testfilename)) != 0 ) return msg;
			}
			if( !testset.empty() )
			{
				if( (msg=handler_type::init_measure(learner, testset, measure, 1)) != 0 ) return msg;
				if( (msg=handler_type::evaluate(learner, testset, measure, learner)) != 0 ) return msg;
				if( (msg=handler_type::final_measure(learner, measure)) != 0 ) return msg;
			}
			else measure.clear();
			return 0;
		}
		/** Allows the learning algorithm to validate itself e.g. out-of-bag error for Bagging.
		 *
		 * @param learner a learning algorithm.
		 * @param trainset a source dataset.
		 * @param measure a destination measure.
		 * @param predictions a prediction vector.
		 * @return an error message or NULL.
		 */
		template<class U>
		const char* validate_self(learner_type& learner, learnset_type& trainset, measure_type& measure, std::vector<U>& predictions)
		{
			const char* msg;
			predictions.assign(trainset.size(), U(0,0));
			//if( (msg=TP<L>::train(learner, trainset, predictions)) != 0 ) return msg;
			if( (msg=learner.train(trainset, predictions)) != 0 ) return msg;
#ifdef _MPI
			std::cerr << "Not currently support by MPI" << std::endl;
			std::exit(1);
#endif
			//if( (msg=MPI_POST(*this, new mpi_learner_node<L>(learner, learnset, MPI_LEVEL(L)) )) != 0 ) return msg;
			//if( (msg=MPI_WAIT(learner.get_mpi_tree())) != 0 ) return msg;
			if( (msg=handler_type::writemodel(learner, trainset)) != 0 ) return msg;
			if( (msg=handler_type::init_measure(learner, trainset, measure, 1)) != 0 ) return msg;
			if( (msg=handler_type::evaluate(predictions, trainset, measure, learner)) != 0 ) return msg;
			if( (msg=handler_type::final_measure(learner, measure)) != 0 ) return msg;
			return 0;
		}
		/** Checks for logic errors.
		 *
		 * @param learner a learning algorithm.
		 * @param trainset a source dataset.
		 * @param measure a destination measure.
		 * @param dummy a dummy variable
		 * @return an error message or NULL.
		 */
		static const char* validate_self(learner_type& learner, learnset_type& trainset, measure_type& measure, int dummy)
		{
			ASSERT(false);
			return ERRORMSG("Validation not supported for algorithm " << learner_type::name());
		}
		/** Trains the learning algoritm.
		 *
		 * @todo compare model to testset. and to prev model
		 * 
		 * @param learner a learning algorithm.
		 * @param trainset a source dataset.
		 * @param r number of runs.
		 * @param f number of folds.
		 * @return an error message or NULL.
		 */
		const char* train(learner_type& learner, learnset_type& trainset, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
#ifdef _MPI
			typedef typename TP<L>::learner_type learn_type;
#endif
			const char* msg;
			if( !trainset.empty() )
			{
				MPI_IF_ROOT
				if( (msg=MPI_POST(learner, new mpi_learner_node<L>(learner, trainset, MPI_LEVEL(learn_type)) )) != 0 ) return msg;
				MPI_IF_LEVEL(learn_type, learner)
				if( (msg=TP<L>::train(learner, trainset)) != 0 ) return msg;
				MPI_IF_END
				if( (msg=MPI_WAIT(learner.get_mpi_tree())) != 0 ) return msg;
				MPI_IF_ELSE
#ifdef _MPI
				if( (msg=TP<L>::train(learner, trainset)) != 0 ) return msg;
#endif
				MPI_IF_END
				if( (msg=handler_type::writemodel(learner, trainset, r, f)) != 0 ) return msg;
			}
			else
			{
				if( (msg=handler_type::readmodel(learner, testset, r, f)) != 0 ) return msg;
				if( learner_type::LAZY ) return ERRORMSG("Lazy learners require training set!");
			}
			return 0;
		}
		
	public:
		/** Tests if a partitioning algorithm is used.
		 * 
		 * @return true if a partitioning algorithm was used.
		 */
		bool ispartition()const
		{
			return !is_extended(handler_type::type());
		}
		/** Tests if a testing algorithm is used.
		 * 
		 * @return true if a testing algorithm is used.
		 */
		bool istesting()const
		{
			return is_testing(handler_type::type());
		}

	private:
		static bool is_extended(int t)
		{
			return is_graphing(t) || is_testing(t) || is_selfvalidating(t);
		}
		static bool is_graphing(int t)
		{
			return t == GR;
		}
		static bool is_testing(int t)
		{
			return t == TS;
		}
		static bool is_selfvalidating(int t)
		{
			return t == SF;
		}
		static std::string typeName(int t)
		{
			switch(t)
			{
			case GR:
				return "Graph";
			case TS:
				return "Testset";
			case SF:
				return learner_type::validationType();
			default:
				return "None";
			};
		}

	private:
		float rulePercFlt;
		std::string rulefile;
		std::string predfile;
		std::string testfilename;
		string_vector gmodels;
		graph_factory gfactory;
		readable_dataset testset;
		bool saveBool;
		int complexityInt;
	};

};

#endif


