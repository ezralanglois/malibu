/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * model_evaluation.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_MODEL_EVALUATION_HPP
#define _EXEGETE_MODEL_EVALUATION_HPP
#include "prediction.hpp"
#include <vector>

/** @file model_evaluation.hpp
 * @brief Contains model evaluation classes.
 * 
 * This file contains files related to model evaluation.
 *
 * @ingroup ExegeteValidation
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail
	{
		/** @brief Adapts evaluation based on class (binary)
		 * 
		 * This class defines an adapter for evaluation of binary classes.
		 */
		template<class T, class F>
		struct class_adapter
		{
			/** Defines a float type. **/
			typedef F float_type;
			/** Defines a prediction type. **/
			typedef F prediction_type;
			/** Defines a prediction reference. **/
			typedef F prediction_ref;
			/** Defines a vector of pairs (float and unsigned int). **/
			typedef std::vector< std::pair<float_type, unsigned int> > inst_vector;
			/** Resize a prediction (does nothing).
			 * 
			 * @param pred a prediction.
			 * @param cl number of classes.
			 */
			static void resize(prediction_type pred, unsigned int cl)
			{
			}
			/** Make a prediction with a learner.
			 * 
			 * @param learner a learning algorithm.
			 * @param pattr an array of predictions.
			 * @param pred a prediction.
			 * @return a prediction value.
			 */
			template<class X>
			static F predict(const T& learner, X pattr, prediction_type pred)
			{
				return learner.predict(*pattr);
			}
			/** Make a multiple-instance prediction with a learner.
			 * 
			 * @param learner a learning algorithm.
			 * @param beg start of prediction collection.
			 * @param end end of prediction collection.
			 * @param pred a prediction.
			 * @return a prediction value.
			 */
			template<class X>
			static F predict(const T& learner, X beg, X end, prediction_type pred)
			{
				return learner.predict(beg, end);
			}
			/** Initialize vector to zero.
			 * 
			 * @param inst an instance vector
			 */
			static void init(inst_vector& inst)
			{
				inst[1].first = 0;
				inst[1].second = 0;
				inst[0].first = 0;
				inst[0].second = 0;
			}
			/** Update a bag prediction.
			 * 
			 * @param inst an instance vector.
			 * @param pred a prediction.
			 * @param th a threshold.
			 */
			static void update_bag(inst_vector& inst, F pred, float_type th)
			{
				if( pred > th )
				{
					ASSERT(inst.size()>1);
					inst[1].first += pred;
					inst[1].second++;
				}
				else
				{
					ASSERT(inst.size()>0);
					inst[0].first += pred;
					inst[0].second++;
				}
			}
			/** Make a bag-level prediction.
			 * 
			 * @param inst an instance vector.
			 * @param pred a prediction.
			 * @return a real-valued prediction.
			 */
			static F predict_bag(inst_vector& inst, prediction_type pred)
			{
				ASSERT(inst.size()>1);
				F p = (inst[1].second > 0) ? (inst[1].first / inst[1].second) : (inst[0].first / inst[0].second);
				return p;
			}
		};
		/** @brief Adapts evaluation based on class (multi-class)
		 * 
		 * This class defines an adapter for evaluation of multi-class classes.
		 */
		template<class T, class F>
		struct class_adapter< T, F* >
		{
			/** Defines a float type. **/
			typedef F float_type;
			/** Defines a vector of floats as a prediction type. **/
			typedef std::vector<F> prediction_type;
			/** Defines a reference of a vector of floats as a prediction reference. **/
			typedef std::vector<F>& prediction_ref;
			/** Defines a vector of pairs (float and unsigned int). **/
			typedef std::vector< std::pair<float_type, unsigned int> > inst_vector;
			/** Resize a prediction array.
			 * 
			 * @param pred a prediction.
			 * @param cl number of classes.
			 */
			static void resize(prediction_ref pred, unsigned int cl)
			{
				pred.resize(cl);
			}
			/** Make a prediction with a learner.
			 * 
			 * @param learner a learning algorithm.
			 * @param pattr an array of predictions.
			 * @param pred a prediction.
			 * @return a prediction value.
			 */
			template<class X>
			static F* predict(const T& learner, X pattr, prediction_ref pred)
			{
				std::fill(pred.begin(), pred.end(), 0.0f);
				learner.predict(*pattr, &(*pred.begin()));
				return &(*pred.begin());
			}
			/** Make a multiple-instance prediction with a learner.
			 * 
			 * @param learner a learning algorithm.
			 * @param beg start of prediction collection.
			 * @param end end of prediction collection.
			 * @param pred a prediction.
			 * @return a prediction pointer.
			 */
			template<class X>
			static F* predict(const T& learner, X beg, X end, prediction_ref pred)
			{
				std::fill(pred.begin(), pred.end(), 0.0f);
				learner.predict(beg, end, &(*pred.begin()));
				return &(*pred.begin());
			}
			/** Initialize vector to zero.
			 * 
			 * @param inst an instance vector
			 */
			static void init(inst_vector& inst)
			{
			}
			/** Update a bag prediction.
			 * 
			 * @param inst an instance vector.
			 * @param pred a prediction.
			 * @param th a threshold.
			 */
			static void update_bag(inst_vector& inst, F* pred, float_type th)
			{
			}
			/** Make a bag-level prediction.
			 * 
			 * @param inst an instance vector.
			 * @param pred a prediction.
			 * @return a real-valued prediction pointer.
			 */
			static F* predict_bag(inst_vector& inst, prediction_type pred)
			{
				return &(*pred.begin());
			}
		};
		/** @brief Adapts evaluation to learner type
		 * 
		 * This class defines an adapter for evaluation of a learning algorithm.
		 */
		template<class T, class F>
		struct model_adapter : public class_adapter<T, F>
		{
			/** Defines a prediction reference. */
			typedef typename class_adapter<T, F>::prediction_ref prediction_ref;
			/** Make a prediction with given learner.
			 * 
			 * @param learner a learning algorithm.
			 * @param pattr an attribute vector.
			 * @param pred a prediction.
			 * @param idx prediction index.
			 * @return a prediction.
			 */
			template<class X>
			static F predict(const T& learner, X pattr, prediction_ref pred, unsigned int idx)
			{
				return class_adapter<T, F>::predict(learner, pattr, pred);
			}
		};
		/** @brief Adapts evaluation to a vector of predictions
		 * 
		 * This class defines an adapter for evaluation of a vector of predictions.
		 */
		template<class T, class F>
		struct model_adapter< std::vector<T>, F > : public class_adapter<T, F>
		{
			/** Defines a float type. **/
			typedef typename class_adapter<T, F>::float_type float_type;
			/** Defines a prediction type. **/
			typedef float_type prediction_type;
			/** Defines a prediction reference. **/
			typedef float_type prediction_ref;
			/** Resize a prediction array.
			 * 
			 * @param pred a prediction.
			 * @param cl number of classes.
			 */
			static void resize(prediction_ref pred, unsigned int cl)
			{
			}
			/** Make a prediction with a given learner.
			 * 
			 * @param arr a prediction vector.
			 * @param pattr an attribute vector.
			 * @param pred a prediction.
			 * @param idx prediction index.
			 * @return a prediction.
			 */
			template<class X>
			static F predict(const std::vector<T>& arr, X pattr, prediction_ref pred, unsigned int& idx)
			{
				F val = arr[idx].p();
				++idx;
				return val;
			}
		};
	};

	/** @brief Adapts evaluation for multiple-instance learning
	 * 
	 * This class defines an adapter to evaluate multiple-instance learning.
	 */
	template<class L, int I=L::EVAL>
	struct model_evaluation
	{
		/** Evaluate a prediction with given model, testset and measure.
		 * 
		 * @param learner a learning algorithm.
		 * @param testset a testset.
		 * @param measure a measure.
		 * @param arg learning arguments.
		 * @return an error message or NULL.
		 */
		template<class U, class D, class M>
		static const char* evaluate(U& learner, D& testset, M& measure, L& arg)
		{
			typedef typename L::float_type float_type;
			typedef typename D::bag_iterator bag_iterator;
			typedef typename D::iterator iterator;
			typedef typename M::prediction_type prediction_type;
			typedef detail::model_adapter<U, prediction_type> model_adapter;
			typedef typename model_adapter::prediction_type prediction_val;
			typedef typename model_adapter::inst_vector inst_vector;
			if( learner.empty() ) return ERRORMSG("Empty learning model");
			unsigned int index=0;
			float_type th = arg.threshold();
			prediction_val pred(0);
			model_adapter::resize(pred, testset.classCount());
			if(testset.bagCount() > 0)
			{
				if( testset.classCount() > 2 ) return ERRORMSG("Must use OVA MIL learner for multi-class MIL problems");
				float_type bth = arg.threshold(true);
				inst_vector ins( testset.classCount() );
				prediction_type pval;
				for(bag_iterator bbeg=testset.bag_begin(), bend=testset.bag_end();bbeg != bend;++bbeg)
				{
					model_adapter::init(ins);
					for(iterator beg=bbeg->begin(), end=bbeg->end();beg != end;++beg)
					{
						measure.next((pval=model_adapter::predict(learner, beg, pred, index)), *beg, th, beg->weight(1.0f));
						model_adapter::update_bag(ins, pval, th);
					}
					measure.next_bag(model_adapter::predict_bag(ins, pred), *bbeg, bth, bbeg->weight(1.0f));
				}
			}
			else
			{
				for(iterator beg=testset.begin(), end=testset.end();beg != end;++beg)
				{
					measure.next(model_adapter::predict(learner, beg, pred, index), *beg, th, beg->weight(1.0f));
				}
			}			
			return 0;
		}
	};
	/** @brief Adapts evaluation for classification
	 * 
	 * This class defines an adapter to evaluate classification.
	 */
	template<class L>
	struct model_evaluation<L, 1>
	{
		/** Evaluate a prediction with given model, testset and measure.
		 * 
		 * @param learner a learning algorithm.
		 * @param testset a testset.
		 * @param measure a measure.
		 * @param arg learning arguments.
		 * @return an error message or NULL.
		 */
		template<class U, class D, class M>
		static const char* evaluate(U& learner, D& testset, M& measure, L& arg)
		{
			typedef typename L::float_type float_type;
			typedef typename D::bag_iterator bag_iterator;
			typedef typename D::iterator iterator;
			typedef typename M::prediction_type prediction_type;
			typedef detail::class_adapter<U, prediction_type> class_adapter;
			typedef typename class_adapter::prediction_type prediction_val;
			
			if( learner.empty() ) return ERRORMSG("Empty learning model");
			if( testset.bagCount() == 0 ) return ERRORMSG("Must use MIL dataset with this learner");
			//unsigned int index=0;
			float_type th = arg.threshold();
			float_type bth = arg.threshold(true);
			prediction_val pred(0);
			class_adapter::resize(pred, testset.classCount());
			for(bag_iterator bbeg=testset.bag_begin(), bend=testset.bag_end();bbeg != bend;++bbeg)
			{
				for(iterator beg=bbeg->begin(), end=bbeg->end();beg != end;++beg)//, index
					measure.next(class_adapter::predict(learner, beg, pred), *beg, th, beg->weight(1.0f));
				measure.next_bag(class_adapter::predict(learner, bbeg->begin(), bbeg->end(), pred), *bbeg, bth, bbeg->weight(1.0f));
			}
			
			return 0;
		}
	};
	/** @brief Adapts evaluation for classification
	 * 
	 * This class defines an adapter to evaluate classification.
	 */
	template<class L>
	struct model_evaluation<L, 2>
	{
		/** Evaluate a prediction with given model, testset and measure.
		 * 
		 * @param learner a learning algorithm.
		 * @param testset a testset.
		 * @param measure a measure.
		 * @param arg learning arguments.
		 * @return an error message or NULL.
		 */
		template<class U, class D, class M>
		static const char* evaluate(U& learner, D& testset, M& measure, L& arg)
		{
			typedef typename L::float_type float_type;
			typedef typename D::bag_iterator bag_iterator;
			typedef typename D::iterator iterator;
			typedef typename D::size_type size_type;
			typedef typename M::prediction_type prediction_type;

			if( learner.empty() ) return ERRORMSG("Empty learning model");
			if( testset.bagCount() == 0 ) return ERRORMSG("Must use Structured dataset with this learner");
			float_type th = arg.threshold();
			float_type bth = arg.threshold(true);
			size_type classcnt = learner.classCount();
			size_type maxbag = testset.maximumBagCount();

			ASSERT(classcnt>1);
			prediction_type* pred = reallocate((prediction_type*)0, maxbag, classcnt), *curr_pred, *end_pred=pred+maxbag;
			float_type c;
			for(bag_iterator bbeg=testset.bag_begin(), bend=testset.bag_end();bbeg != bend;++bbeg)
			{
				c = learner.predict(bbeg->begin(), bbeg->end(), pred, end_pred);
				curr_pred = pred;
				for(iterator beg=bbeg->begin(), end=bbeg->end();beg != end;++beg, ++curr_pred)//, index
					measure.next(*curr_pred, *beg, th, beg->weight(1.0f));
				(*pred)[0] = c;
				(*pred)[1] = bbeg->size();
				if(classcnt > 2) std::fill((*pred)+2, (*pred)+classcnt, 0);
				measure.next_bag(*pred, *bbeg, bth, bbeg->weight(1.0f));
			}
			deallocate(pred);
			return 0;
		}
		
	private:
		template<class F>
		static void deallocate(F** to)
		{
			if( to != 0 ) ::erase(*to);
			::erase(to);
		}
		template<class F>
		static F** reallocate(F** to, unsigned int r, unsigned int c)
		{
			F* val = 0;
			if( to != 0 ) val = *to;
			to = ::resize(to, r);
			*to = val;
			*to = ::resize(*to, r*c);
			for(unsigned int i=1;i<r;++i) to[i] = to[i-1]+c;
			return to;
		}
	};
};

#endif


