/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * PartitionHandler.h
 * Copyright (C) 2007 Robert Ezra Langlois
 */

#ifndef _EXEGETE_PARTIIONHANDLER_H
#define _EXEGETE_PARTIIONHANDLER_H
#include "errorutil.h"
#include "debugutil.h"
#include "ExampleSet.h"
#include "DatasetHandler.h"

#if (_MALIBU_VERSION <= 100000)
#include "ModelEvaluation.h"
#else
#include "model_evaluation.hpp"
#endif

#ifdef _MPI
#include "mpi_learner_node.hpp"
#endif


/** @file PartitionHandler.h
 * This file contains the PartitionHandler class.
 * 
 * 
 * @author Robert Ezra Langlois (rlangl1@uic.edu)
 * @version 1.0
 */

namespace exegete
{
template <bool x> struct STATIC_ASSERTION_FAILURE;
template <> struct STATIC_ASSERTION_FAILURE<true> { enum { value = 1 }; };

	/** This class template modifies the ParitionValidation class.
	 *
	 * @todo check if learning failed, not performed and empty model file.
	 * @todo set class map in testset based on model
	 */
	template<class M, template<class> class TP>
	class PartitionHandler
	{
	public:
		/** Constructs a partition handler.
		 */
		PartitionHandler() : pfxalgoInt(0)
		{
		}
		/** Destructs a partition handler.
		 */
		~PartitionHandler()
		{
		}
		
	public:
		/** Initalize parameters for validation handler.
		 * 
		 * @param map an initializer.
		 * @param prefix an argument prefix.
		 */
		template<class U>
		void init(U& map, std::string prefix="")
		{
		}
		/**
		 */
		void algoname(const std::string& nm)
		{
			algonameStr = nm;
		}
		/**
		 */
		void add_search_path(const std::string& pth)
		{
			implicitpth = pth;
		}
		/**
		 */
		const std::string& search_path()const
		{
			return implicitpth;
		}
		/**
		 */
		const std::string& algoname()const
		{
			return algonameStr;
		}
		
	public:
		/** This method initalizes the measure type.
		 *
		 * @todo fix save bug progressive validation, need to resize after finalize (only in this case)
		 * 
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @param runs number of runs.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		static const char* init(L& learner, D& dataset, M& measure, unsigned int runs, unsigned int folds=1)
		{
			ASSERT(dataset.size()>0);
			TP<L>::resize_saved(learner, runs*folds);
#if (_MALIBU_VERSION <= 100000)
			measure.resize(dataset.size(), dataset.bagCount(), runs);
#else
			measure.resize(dataset.size(), dataset.bagCount(), runs, dataset.classCount());
#endif
			measure.initialize();
			return 0;
		}
		/** This method initalizes the measure type.
		 *
		 * @todo fix save bug progressive validation, need to resize after finalize (only in this case)
		 * 
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @param runs number of runs.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		static const char* init_measure(L& learner, D& dataset, M& measure, unsigned int runs, unsigned int folds=1)
		{
			ASSERT(dataset.size()>0);
#if (_MALIBU_VERSION <= 100000)
			measure.resize(dataset.size(), dataset.bagCount(), runs);
#else
			measure.resize(dataset.size(), dataset.bagCount(), runs, dataset.classCount());
#endif
			return 0;
		}
		/** This method finalizes the measure type.
		 *
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		static const char* next_run(M& measure)
		{
			measure.next_run();
			return 0;
		}
		/** This method finalizes the measure type.
		 *
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		static const char* finalize(M& measure)
		{
			measure.finalize();
			return 0;
		}
		/** This method trains a learning algorithm on the source training set.
		 *
		 * @param learner a learning algorithm.
		 * @param trainset a source training set.
		 * @param testset a source testing set.
		 * @param measure a destination measure.
		 * @param r run number.
		 * @param f fold number.
		 * @return an error message or NULL.
		 */
		template<class L, class T>
		inline const char* eval(L& learner, typename L::dataset_type& trainset, T& testset, M& measure, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			typedef typename L::dataset_type learnset_type;
			typedef typename learnset_type::attribute_type attribute_type;
			typedef typename learnset_type::class_type class_type;
			typedef ExampleSet<attribute_type, class_type, std::string*> testset_type;
			const char* msg;
#ifndef _MPI
			TP<L>::train(learner, trainset, testset);
			if( !modelfile.empty() ) if( (msg=writemodel(learner, trainset, r, f)) != 0 ) return msg;
			if( (msg=evaluate(learner, testset, measure, learner)) != 0 ) return msg;
<<<<<<< .mine
<<<<<<< .mine
<<<<<<< .mine
<<<<<<< .mine
			TP<L>::save(learner, testset);
#else
			if( (msg=MPI_POST(learner, new mpi_validate_node<L>(learner, trainset, testset, MPI_LEVEL(L)))) != 0 ) return msg;
=======
>>>>>>> .r89
=======
			TP<L>::save(learner, testset);
#else
			if( (msg=MPI_POST(learner, new mpi_validate_node<L>(learner, trainset, testset, 1))) != 0 ) return msg;
=======
>>>>>>> .r93
>>>>>>> .r91
=======
			TP<L>::save(learner, testset);
#else
			if( (msg=MPI_POST(learner, new mpi_validate_node<L>(learner, trainset, testset, 1))) != 0 ) return msg;
>>>>>>> .r93
#endif
			return 0;
		}
		/**
		 */
		template<class U, class T, class L>
		static const char* evaluate(U& learner, T& testset, M& measure, L& arg)
		{
#if (_MALIBU_VERSION <= 100000)
			return ModelEvaluation<L>::evaluate(learner, testset, measure, arg);
#else
			return model_evaluation<L>::evaluate(learner, testset, measure, arg);
#endif
		}
		
#ifdef _EXEGETE_LEARNER_H
	public:
		/**
		 */
		template<class L>
		const char* writemodel(const L& learner, const typename L::dataset_type& header, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			typedef typename base_learner<L>::result base_type;
			typedef DatasetHandler< base_type > dataset_handler;
			if( modelfile.empty() ) return 0;
			const char* msg;
			std::string pfx = (pfxalgoInt != 0) ? algonameStr : std::string("") ;
			std::string file = modelfile;
			std::ofstream fout;
			if( r != UINT_MAX || !pfx.empty() ) file = genfile(modelfile, pfx, r, f);
			ASSERT(!modelfile.empty());
			if( implicitpth != "" ) file = join_file(implicitpth.c_str(), file.c_str());
			if( (msg=openfile(fout, file)) != 0 ) return msg;
			ASSERTMSG( dataset_handler::handler() != 0, base_type::class_name() );
			if( (msg=dataset_handler::handler()->write_model(fout, header)) != 0 )  return msg;
			fout << learner;
			fout.close();
			return learner.errormsg();
		}
		/**
		 */
		template<class L, class D>
		const char* readmodel(L& learner, D& dataset, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			std::string pfx = pfxalgoInt ? algonameStr : std::string("") ;
			std::string file = modelfile;
			if( implicitpth != "" ) file = join_file(implicitpth.c_str(), file.c_str());
			return readmodel(file, learner, dataset, r, f, pfx);
		}
		/**
		 */
		template<class L, class D>
		static const char* readmodel(std::string mfile, L& learner, D& dataset, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX, std::string pfx="")
		{
			typedef typename base_learner<L>::result base_type;
			typedef DatasetHandler< base_type > dataset_handler;
			const char* msg;
			std::string file = mfile;
			std::ifstream fin;
			if( r != UINT_MAX || !pfx.empty() ) file = genfile(mfile, pfx, r, f);
			if( (msg=openfile(fin, file)) != 0 ) return msg;
			if( (msg=dataset_handler::handler()->read_model(fin, dataset)) != 0 ) return msg;
			L::setup_dataset(dataset);
			fin >> learner;
			return learner.errormsg();
		}
#endif
		
	protected:
		std::string implicitpth;
		std::string modelfile;
		std::string algonameStr;
		int pfxalgoInt;
	};

	/** This class template modifies the ParitionValidation class.
	 *
	 * @todo check if learning failed, not performed and empty model file.
	 * @todo set class map in testset based on model
	 */
	template< template<class>class TP >
	class PartitionHandler<std::string, TP>
	{
	public:
		/** Constructs a partition handler.
		 */
		PartitionHandler()
		{
		}
		/** Destructs a partition handler.
		 */
		~PartitionHandler()
		{
		}
		
	public:
		/** Initalize parameters for validation handler.
		 * 
		 * @param map an initializer.
		 * @param prefix an argument prefix.
		 */
		template<class U>
		void init(U& map, std::string prefix="")
		{
			arginit(map, trnpfx, prefix, "trnpfx", "prefix for training set");
			arginit(map, tstpfx, prefix, "tstpfx", "prefix for testing set");
		}
		
	public:
		/** This method initalizes the measure type.
		 *
		 * @todo fix save bug progressive validation, need to resize after finalize (only in this case)
		 * 
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @param runs number of runs.
		 * @return an error message or NULL.
		 */
		template<class F, class D>
		const char* init(F& format, D& dataset, std::string& measure, unsigned int runs, unsigned int folds=1)
		{
			return 0;
		}
		/** This method finalizes the measure type.
		 *
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		const char* next_run(const std::string& measure)
		{
			return 0;
		}
		/** This method finalizes the measure type.
		 *
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		const char* finalize(const std::string& measure)
		{
			return 0;
		}
		/** This method trains a learning algorithm on the source training set.
		 *
		 * @param learner a learning algorithm.
		 * @param trainset a source training set.
		 * @param testset a source testing set.
		 * @param measure a destination measure.
		 * @param r run number.
		 * @param f fold number.
		 * @return an error message or NULL.
		 */
		template<class F, class Tr, class T>
		inline const char* eval(F& format, Tr& trainset, T& testset, const std::string& file, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			const char* msg;
			std::string outputfile;
			if( !trnpfx.empty() )
			{
				outputfile=genfile(file, trnpfx, r, f);
				if( !outputfile.empty() && (msg = writefile(outputfile, format(trainset))) != 0 ) return msg;
			}
			outputfile=genfile(file, tstpfx, r, f);
			if( !tstpfx.empty() )
			{
				if( !outputfile.empty() && (msg = writefile(outputfile, format(testset))) != 0 ) return msg;
			}
			if( trnpfx.empty() && tstpfx.empty() )
			{
				if( !outputfile.empty() && (msg = writefile(outputfile, format(testset))) != 0 ) return msg;
			}
			return 0;
		}
		
	private:
		std::string trnpfx;
		std::string tstpfx;
	};
	/**
	 */
	template<class T>
	struct DefaultTrainPolicy
	{
		static void train(T& learner, typename T::dataset_type& dataset, typename T::testset_type& testset)
		{
			learner.train(dataset);
		}
		static void save(T& learner, typename T::testset_type& testset)
		{
#ifdef _MPI
			learner.save_next(testset);
#endif
		}
		static void resize_saved(T& learner, unsigned int n)
		{
#ifdef _MPI
			if(n>0) learner.resize_saved(n);
#endif
		}
	};
};

#endif

