/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * PartitionHandler.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_PARTIIONHANDLER_H
#define _EXEGETE_PARTIIONHANDLER_H
#include "errorutil.h"
#include "debugutil.h"
#include "ExampleSet.h"
#include "DatasetHandler.h"
#include "model_evaluation.hpp"

#ifdef _MPI
#include "mpi_learner_node.hpp"
#endif


/** @file PartitionHandler.h
 * @brief Contains classes to handle learning over partitions
 * 
 * This file contains related to the PartitionHandler class.
 * 
 * @ingroup ExegeteValidation
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
//template <bool x> struct STATIC_ASSERTION_FAILURE;
//template <> struct STATIC_ASSERTION_FAILURE<true> { enum { value = 1 }; };

	/** @brief Handles partitioning and learning
	 * 
	 * This class template modifies the ParitionValidation class.
	 *
	 * @todo check if learning failed, not performed and empty model file.
	 * @todo set class map in testset based on model
	 */
	template<class M, template<class> class TP>
	class PartitionHandler
	{
	public:
		/** Constructs a partition handler.
		 */
		PartitionHandler() : pfxalgoInt(0), restartBool(0)
		{
		}
		/** Destructs a partition handler.
		 */
		~PartitionHandler()
		{
		}
		
	public:
		/** Initalize parameters for validation handler.
		 * 
		 * @param map an initializer.
		 * @param prefix an argument prefix.
		 */
		template<class U>
		void init(U& map, std::string prefix="")
		{
		}
		/** Sets an algorithm name.
		 * 
		 * @param nm an algorithm name.
		 */
		void algoname(const std::string& nm)
		{
			algonameStr = nm;
		}
		/** Sets the search path for files.
		 * 
		 * @param pth a search path.
		 */
		void add_search_path(const std::string& pth)
		{
			implicitpth = pth;
		}
		/** Gets the search path for files.
		 * 
		 * @return a search path.
		 */
		const std::string& search_path()const
		{
			return implicitpth;
		}
		/** Get an algorithm name.
		 * 
		 * @return algorithm name.
		 */
		const std::string& algoname()const
		{
			return algonameStr;
		}
		/** Get the model file name.
		 * 
		 * @return model file name.
		 */
		const std::string& model()const
		{
			return modelfile;
		}
		/** Test if should write restart files.
		 * 
		 * @return true if should write restart files.
		 */
		int restart()const
		{
			return restartBool;
		}
		/** Flag if should write restart files.
		 * 
		 * @param t restart flag.
		 */
		void restart(int t)
		{
			restartBool = t;
		}
		
	public:
		/** Initalizes the measure and learner.
		 *
		 * @todo fix save bug progressive validation, need to resize after finalize (only in this case)
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @param runs number of runs.
		 * @param folds number of folds.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		static const char* init(L& learner, D& dataset, M& measure, unsigned int runs, unsigned int folds=1)
		{
			ASSERT(dataset.size()>0);
			TP<L>::resize_saved(learner, runs*folds);
			measure.resize(dataset.size(), dataset.bagCount(), runs, dataset.classCount());
			measure.initialize();
			return 0;
		}
		/** Initalizes the measure.
		 *
		 * @todo fix save bug progressive validation, need to resize after finalize (only in this case)
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @param runs number of runs.
		 * @param folds number of folds.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		static const char* init_measure(L& learner, D& dataset, M& measure, unsigned int runs, unsigned int folds=1)
		{
			ASSERT(dataset.size()>0);
			measure.resize(dataset.size(), dataset.bagCount(), runs, dataset.classCount());
			return 0;
		}
		/** Finalizes the measure.
		 *
		 * @param learner a learning algorithm.
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		template<class L>
		const char* final_measure(L& learner, M& measure)
		{
			measure.next_run();
			measure.finalize();
			return 0;
		}
		/** Finalizes the current run in the measure.
		 *
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		static const char* next_run(M& measure)
		{
			MPI_IF_NOT_ROOT
			measure.next_run();
			MPI_IF_END
			return 0;
		}
		/** Finalizes the learner and measure.
		 *
		 * @param learner a learning algorithm.
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		template<class L>
		const char* finalize(L& learner, M& measure)
		{
			typedef typename TP<L>::learner_type learner_type;
			MPI_IF_NOT_ROOT
			measure.finalize();
			MPI_IF_END
			if( restartBool > 0 ) restartBool = 0;
			return 0;
		}
		/** Trains a learning algorithm on the source training set and evaluates
		 * the learned model on a test set saving the predictions  in a measure.
		 *
		 * @param learner a learning algorithm.
		 * @param trainset a source training set.
		 * @param testset a source testing set.
		 * @param measure a destination measure.
		 * @param r run number.
		 * @param f fold number.
		 * @return an error message or NULL.
		 */
		template<class L, class T>
		inline const char* eval(L& learner, typename L::dataset_type& trainset, T& testset, M& measure,
#ifdef _MPI
				bool is_last,
#endif
				unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			typedef typename TP<L>::learner_type learner_type;
			typedef typename L::dataset_type learnset_type;
			typedef typename learnset_type::class_type class_type;
			typedef typename learnset_type::attribute_type attribute_type;
			typedef ExampleSet<attribute_type, class_type, std::string*> testset_type;
			const char* msg;
			//
			bool loaded=false;
			if( restartBool && !modelfile.empty() )
			{
				loaded = ( (msg=readmodel(learner, testset, r, f)) == 0 );
			}
			if( !loaded )
			{
			MPI_IF_ROOT
#ifdef _MPI
			mpi_validate_node<learner_type, M, TP>* pnode;
			if( (msg=MPI_POST(learner, (pnode=new mpi_validate_node<learner_type,M,TP>(learner, trainset, testset, *this, measure, r, f, is_last, MPI_LEVEL(learner_type))) )) != 0 ) return msg;
			MPI_IF_LEVEL(learner_type, learner)
			if( (msg=TP<L>::train(pnode->learner(), trainset, testset)) != 0 ) return msg;
			MPI_IF_END
#endif
			MPI_IF_ELSE
			if( (msg=TP<L>::train(learner, trainset, testset)) != 0 ) return msg;
			if( !modelfile.empty() ) if( (msg=writemodel(learner, trainset, r, f)) != 0 ) return msg;
			if( (msg=evaluate(learner, testset, measure, learner)) != 0 ) return msg;
			TP<L>::save(learner, testset);
			MPI_IF_END
			}
			else
			{
			if( (msg=evaluate(learner, testset, measure, learner)) != 0 ) return msg;
			TP<L>::save(learner, testset);
			}
			return 0;
		}
#ifdef _MPI
		template<class T1, class T2, class L>
		const char* mpi_evaluate(L& learner, T1& trainset, T2& testset, M& measure, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			const char* msg;
			if( !modelfile.empty() ) if( (msg=writemodel(learner, trainset, r, f)) != 0 ) return msg;
			return model_evaluation<L>::evaluate(learner, testset, measure, learner);
		}
#endif
		/** Evaluates a learned model over a test set and save the result in
		 * a measure.
		 * 
		 * @param learner a learning algorithm.
		 * @param testset a source testing set.
		 * @param measure a destination measure.
		 * @param arg a learning algorithm.
		 */
		template<class U, class T, class L>
		static const char* evaluate(U& learner, T& testset, M& measure, L& arg)
		{
			return model_evaluation<L>::evaluate(learner, testset, measure, arg);
		}
		
#ifdef _EXEGETE_LEARNER_H
	public:
		/** Writes a dataset header and a learned model to a file.
		 * 
		 * @param learner a learned model.
		 * @param header a dataset header.
		 * @param r current run.
		 * @param f current fold.
		 * @return an error message or NULL.
		 */
		template<class L>
		const char* writemodel(const L& learner, const typename L::dataset_type& header, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			typedef typename base_learner<L>::result base_type;
			typedef DatasetHandler< base_type > dataset_handler;
			if( modelfile.empty() ) return 0;
			const char* msg;
			std::string pfx = (pfxalgoInt != 0) ? algonameStr : std::string("") ;
			std::string file = modelfile;
			std::ofstream fout;
			if( r != UINT_MAX || !pfx.empty() ) file = genfile(modelfile, pfx, r, f);
			ASSERT(!modelfile.empty());
			if( implicitpth != "" ) file = join_file(implicitpth.c_str(), file.c_str());
			if( (msg=openfile(fout, file)) != 0 ) return msg;
			ASSERTMSG( dataset_handler::handler() != 0, base_type::class_name() );
			if( std::string(ext_name(file.c_str()) ) == "bz2" )
			{
				bzip2_stream::bzip2_ostream zout( fout );
				if( (msg=dataset_handler::handler()->write_model(zout, header)) != 0 )  return msg;
				zout << learner;
			}
			else
			{
				if( (msg=dataset_handler::handler()->write_model(fout, header)) != 0 )  return msg;
				fout << learner;
			}
			fout.close();
			return learner.errormsg();
		}
		/** Reads a dataset header and a learned model from a file.
		 * 
		 * @param learner a learned model.
		 * @param dataset a dataset.
		 * @param r current run.
		 * @param f current fold.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		const char* readmodel(L& learner, D& dataset, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			std::string pfx = pfxalgoInt ? algonameStr : std::string("") ;
			std::string file = modelfile;
			if( implicitpth != "" ) file = join_file(implicitpth.c_str(), file.c_str());
			return readmodel(file, learner, dataset, r, f, pfx);
		}
		/** Reads a dataset header and a learned model from a file.
		 * 
		 * @param mfile the model file.
		 * @param learner a learned model.
		 * @param dataset a dataset.
		 * @param r current run.
		 * @param f current fold.
		 * @param pfx file prefix.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		static const char* readmodel(std::string mfile, L& learner, D& dataset, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX, std::string pfx="")
		{
			typedef typename base_learner<L>::result base_type;
			typedef DatasetHandler< base_type > dataset_handler;
			const char* msg;
			std::string file = mfile;
			std::ifstream fin;
			if( r != UINT_MAX || !pfx.empty() ) file = genfile(mfile, pfx, r, f);
			if( (msg=openfile(fin, file)) != 0 ) return msg;
			
			if( std::string(ext_name(file.c_str()) ) == "bz2" )
			{
				bzip2_stream::bzip2_istream zin( fin );
				if( (msg=dataset_handler::handler()->read_model(zin, dataset)) != 0 ) return msg;
				L::setup_dataset(dataset);
				zin >> learner;
			}
			else
			{
				if( (msg=dataset_handler::handler()->read_model(fin, dataset)) != 0 ) return msg;
				L::setup_dataset(dataset);
				fin >> learner;
			}
			fin.close();
			return learner.errormsg();
		}
#endif
		
	protected:
		/** An implicit path to search for files. **/
		std::string implicitpth;
		/** A model file. **/
		std::string modelfile;
		/** An learning algorithm name. **/
		std::string algonameStr;
		/** A flag indicating whether to prefix the model file. **/
		int pfxalgoInt;
		/** A flag to indicate should write restart files. **/
		int restartBool;
	};

	/** @brief Handles for partitioning datasets
	 * 
	 * This class template modifies the ParitionValidation class.
	 *
	 * @todo check if learning failed, not performed and empty model file.
	 * @todo set class map in testset based on model
	 */
	template< template<class>class TP >
	class PartitionHandler<std::string, TP>
	{
	public:
		/** Constructs a partition handler.
		 */
		PartitionHandler()
		{
		}
		/** Destructs a partition handler.
		 */
		~PartitionHandler()
		{
		}
		
	public:
		/** Initalizes parameters for validation handler.
		 * 
		 * @param map an initializer.
		 * @param prefix an argument prefix.
		 */
		template<class U>
		void init(U& map, std::string prefix="")
		{
			arginit(map, trnpfx, prefix, "trnpfx", "prefix for training set");
			arginit(map, tstpfx, prefix, "tstpfx", "prefix for testing set");
		}
		
	public:
		/** Test if should write restart files.
		 * 
		 * @return 0
		 */
		int restart()const
		{
			return 0;
		}
		/** Initalizes the measure type.
		 *
		 * @todo fix save bug progressive validation, need to resize after finalize (only in this case)
		 * 
		 * @param format a format.
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @param runs number of runs.
		 * @param folds number of folds.
		 * @return an error message or NULL.
		 */
		template<class F, class D>
		const char* init(F& format, D& dataset, std::string& measure, unsigned int runs, unsigned int folds=1)
		{
			return 0;
		}
		/** Finalizes the current run in the measure.
		 *
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		const char* next_run(const std::string& measure)
		{
			return 0;
		}
		/** Finalizes the measure type.
		 *
		 * @param format a format.
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		template<class F>
		const char* finalize(F& format, const std::string& measure)
		{
			return 0;
		}
		/** Trains a learning algorithm on the source training set.
		 *
		 * @param format a format.
		 * @param trainset a source training set.
		 * @param testset a source testing set.
		 * @param file a destination file.
		 * @param r run number.
		 * @param f fold number.
		 * @return an error message or NULL.
		 */
		template<class F, class Tr, class T>
		inline const char* eval(F& format, Tr& trainset, T& testset, const std::string& file, unsigned int r=UINT_MAX, unsigned int f=UINT_MAX)
		{
			const char* msg;
			std::string outputfile;
			if( !trnpfx.empty() )
			{
				outputfile=genfile(file, trnpfx, r, f);
				if( !outputfile.empty() && (msg = writefile(outputfile, format(trainset))) != 0 ) return msg;
			}
			outputfile=genfile(file, tstpfx, r, f);
			if( !tstpfx.empty() )
			{
				if( !outputfile.empty() && (msg = writefile(outputfile, format(testset))) != 0 ) return msg;
			}
			if( trnpfx.empty() && tstpfx.empty() )
			{
				if( !outputfile.empty() && (msg = writefile(outputfile, format(testset))) != 0 ) return msg;
			}
			return 0;
		}
		
	private:
		std::string trnpfx;
		std::string tstpfx;
	};
	/** @brief Defines a default training policy.
	 * 
	 * This class defines a default training policy.
	 */
	template<class T>
	struct DefaultTrainPolicy
	{
		/** Defines a learner type. **/
		typedef T learner_type;
		
		/** Trains a learner over the given dataset.
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a training set.
		 * @param predictions vector to save predictions.
		 * @return an error message or NULL.
		 */
		template<class U>
		static const char* train(T& learner, typename T::dataset_type& dataset, std::vector<U>& predictions)
		{
			return learner.train(dataset, predictions);
		}
		/** Trains a learner over the given dataset.
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a training set.
		 * @return an error message or NULL.
		 */
		static const char* train(T& learner, typename T::dataset_type& dataset)
		{
			return learner.train(dataset);
		}
		/** Trains a learner over the given dataset.
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a training set.
		 * @param testset a testing set.
		 * @return an error message or NULL.
		 */
		static const char* train(T& learner, typename T::dataset_type& dataset, typename T::testset_type& testset)
		{
			return learner.train(dataset);
		}
		/** Save a learned model and testset.
		 * 
		 * @param learner a learning algorithm.
		 * @param testset a testing set.
		 */
		static void save(T& learner, typename T::testset_type& testset)
		{
//#ifdef _MPI
			//learner.save_next(testset);
//#endif
		}
		/** Resizes the number of saved learners.
		 * 
		 * @param learner a learning algorithm.
		 * @param n number of learners.
		 */
		static void resize_saved(T& learner, unsigned int n)
		{
//#ifdef _MPI
			//if(n>0) learner.resize_saved(n);
//#endif
		}
	};
};

#endif

