/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * PartitionValidation.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_PARTITIONVALIDATION_H
#define _EXEGETE_PARTITIONVALIDATION_H
#include "PartitionHandler.h"
#include "ExampleSetAlgorithms.h"
#include "ArgumentMap.h"
#include "fileutil.h"

/** @file PartitionValidation.h
 * @brief Handles training and evaluating a learner over partitions
 * 
 * This file contains the PartitionValidation class.
 *
 * @ingroup ExegeteValidation
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page arguments List of Arguments
 *
 *  @section Validation Model Validation
 *	- trnpfx: prefix for the output training set filename
 *	- tstpfx: prefix for the output testing set filename
 *	- pinc: progressive validation increment
 *	- forcepos: force instances in positive bag as positive
 *	- stratify: stratify training and test set distribution?
 *	- nfold: number of paritions/size of training set
 *	- alpha: sample without replacement proportion, 0.0 for sample with replacement
 *	- runs: the number of times to repeat the validation
 *	- type: type of partition
 *		-# None: doe not parition dataset or perform validation.
 *		-# Cross-validation: Parition the dataset into n sets, each is used once for testing and the n-1 rest for training
 *		-# Holdout: Parition the dataset into n sets, one is used for testing, the rest for training
 *		-# Invert-Holdout: Parition the dataset into n sets, one is used for training, the rest for testing
 *		-# Sample: Drawn a random sample of some proportion size
 *		-# Progressive: Start with n-fold training set and progressiving predict and add examples.
 *		-# Invert-progressive: Start with n-fold inverted training set and progressiving predict and add examples.
 */

namespace exegete
{
	/** @brief Handles training and evaluating a learner over partitions
	 * 
	 * This class template performs validation on machine learning model 
	 * and/or partitions a dataset.
	 *
	 * @todo add fixed size cv
	 * @todo add fixed size sampling
	 */
	template<class M, template<class>class TP=DefaultTrainPolicy>
	class PartitionValidation : public PartitionHandler<M, TP>
	{
		typedef PartitionHandler<M, TP> handler_type;
	public:
		/** Defines template parameter M as measure type. **/
		typedef M measure_type;

	public:
		/** Enumerates validation/partition types. **/
		enum{NO='n',CV='c',HO='h',IH='i',SA='s',PV='p',IP='k'};

	public:
		/** Constructs a model validation.
		 * 
		 * @param t a dummy flag.
		 */
		PartitionValidation(int t=0) : typeInt(CV), stratifiedBool(1), positiveBool(0), nfoldInt(5), nrunsInt(1), prognInt(1)
		{
		}
		/** Constructs a model validation.
		 * 
		 * @param map an argument map.
		 * @param t a dummy flag.
		 */
		PartitionValidation(ArgumentMap& map, int t=0) : typeInt(NO), stratifiedBool(1), positiveBool(0), nfoldInt(5), nrunsInt(1), prognInt(1)
		{
			map("Partition");
			init(map, t);
		}
		/** Destructs a model validation.
		 */
		~PartitionValidation()
		{
		}

	public:
		/** Initalizes parameters for validation handler.
		 * 
		 * @param map an initializer.
		 * @param t a parameter flag.
		 * @param prefix an argument prefix.
		 * @param addit additional vtype options.
		 */
		template<class U>
		void init(U& map, int t, std::string prefix="", std::string addit="")
		{
			if( t >= 0 )
			{
				arginit(map, typeInt, 		 prefix, 	"vtype", 		"type of partition>None:n;Cross-validation:c;Holdout:h;Invert-Holdout:i;Sample:s;Progressive:p;Invert-progressive:k"+addit);
				arginit(map, nfoldInt,		 prefix,	"nfold",		"number of folds in n-fold partitioning");
				if(t<1)
				arginit(map, positiveBool,	 prefix,	"forcepos",		"force instances in positive bag as positive?", ArgumentMap::ADDITIONAL);
				arginit(map, stratifiedBool, prefix,	"stratify",		"stratify training and test set distribution?", ArgumentMap::ADDITIONAL);
				arginit(map, rand.alpha(),	 prefix,	"alpha",		"sample without replacement proportion, 0.0 for sample with replacement");
				arginit(map, nrunsInt,		 prefix,	"runs",			"the number of times to repeat the validation");
				arginit(map, prognInt,		 prefix,	"pinc",			"progressive validation increment", ArgumentMap::ADDITIONAL);
				arginit(map, shufflefile,    prefix, 	"shuffle",  	"allows user to use same indices for a dataset", ArgumentMap::ADDITIONAL);
				handler_type::init(map, prefix);
			}
		}

	public:
		/** Initializes the learner and measure.
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a dataset.
		 * @param measure a measure.
		 * @param runs number of runs.
		 * @param folds number of folds.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		static const char* init(L& learner, D& dataset, M& measure, unsigned int runs, unsigned int folds=1)
		{
			return handler_type::init(learner, dataset, measure, runs, folds);
		}
		/** Validates a learning algorithm on the given dataset with the given measure.
		 *	-# Cross-validation: Parition the dataset into n sets, each is used once for testing and the n-1 rest for training
		 *	-# Holdout: Parition the dataset into n sets, one is used for testing, the rest for training
		 *	-# Invert-Holdout: Parition the dataset into n sets, one is used for training, the rest for testing
		 *	-# Sample: Drawn a random sample of some proportion size
		 *	-# Progressive: Start with n-fold training set and progressiving predict and add examples.
		 *	-# Invert-progressive: Start with n-fold inverted training set and progressiving predict and add examples.
		 *
		 * @param learner the learning algorithm.
		 * @param measure destination measure.
		 * @param dataset a dataset to partition for training and testing.
		 * @return an error message or NULL.
		 */
		template<class L, class X, class Y, class W>
		const char* validate(L& learner, measure_type& measure, ExampleSet<X,Y,W>& dataset)
		{
			const char* msg;
			if( prognInt < 1 )  	prognInt = 1;
			if( nrunsInt == 0 ) 	nrunsInt = 1;
			if( nfoldInt < 2 )  	nfoldInt = 2;
			if( rand.alpha() < 0 )  rand.alpha(0.0f);
			if( nfoldInt > dataset.size() ) nfoldInt = (unsigned int)dataset.size();
			if( is_progressive(typeInt)   )	nrunsInt=1;

			if( is_parition(typeInt) )			{if( (msg=validate_fold(learner, dataset, measure)) != 0 ) return msg;}
			else if( is_sample(typeInt) )		{if( (msg=validate_samp(learner, dataset, measure)) != 0 ) return msg;}
			else if( is_progressive(typeInt) )	{if( (msg=validate_prog(learner, dataset, measure)) != 0 ) return msg;}
			handler_type::finalize(learner, measure);
			return 0;
		}
		/** Checks if restart is possible.
		 * 
		 * @param pfx a prefix.
		 * @return an error message or NULL.
		 */
		const char* check_restart(std::string pfx="")const
		{
			//const char* msg;
			// todo disable checking for partial restart
			if( handler_type::restart() == 1)
			{
				if( pfx != "" ) pfx="_"+pfx;
				if( typeInt != NO && handler_type::modelfile == "" ) 	return ERRORMSG("Need to specify " << pfx << "model or restart is not possible.");
				if( is_validation(typeInt) && shufflefile == "" ) 		return ERRORMSG("Need to specify " << pfx << "shuffle or restart is not possible.");
			}
			return 0;
		}

	protected:
		/** Initalizes the learner and measure.
		 *
		 * @todo fix save bug progressive validation, need to resize after finalize (only in this case)
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @param r number of runs.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		const char* init(L& learner, D& dataset, M& measure, unsigned int r=0)
		{
			return handler_type::init(learner, dataset, measure, r==0?nrunsInt:r);
		}
		/** Initalizes the measure.
		 *
		 * @todo fix save bug progressive validation, need to resize after finalize (only in this case)
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a source dataset.
		 * @param measure a destination measure.
		 * @param r number of runs.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		const char* init_measure(L& learner, D& dataset, M& measure, unsigned int r=0)
		{
			return handler_type::init_measure(learner, dataset, measure, r==0?nrunsInt:r);
		}
		/** Finalizes the measure.
		 *
		 * @param learner a learning algorithm.
		 * @param measure a destination measure.
		 * @return an error message or NULL.
		 */
		template<class L>
		const char* final_measure(L& learner, M& measure)
		{
			return handler_type::final_measure(learner, measure);
		}
		/** Validates a learning algorithm on the given dataset with the given measure.
		 *	-# Cross-validation: Parition the dataset into n sets, each is used once for testing and the n-1 rest for training
		 *	-# Holdout: Parition the dataset into n sets, one is used for testing, the rest for training
		 *	-# Invert-Holdout: Parition the dataset into n sets, one is used for training, the rest for testing
		 *
		 * @param learner the learning algorithm.
		 * @param dataset a dataset to partition for training and testing.
		 * @param measure a class to store or measures a set of predictions.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		const char* validate_fold(L& learner, D& dataset, measure_type& measure)
		{
			typedef typename L::dataset_type learnset_type;
			typedef typename learnset_type::attribute_type attribute_type;
			typedef typename learnset_type::class_type class_type;
			typedef ExampleSet<attribute_type, class_type, std::string*> testset_type;
			typedef std::vector< typename learnset_type::size_type > index_vector;
			const char* msg;
			learnset_type trainset(dataset);
			testset_type testset(dataset);
			unsigned int nfold=nfoldInt;
			index_vector folds(nfold+1);
			if( typeInt != CV ) nfold=1;
			if( (msg=handler_type::init(learner, dataset, measure, nrunsInt, nfold)) != 0 ) return msg;
			if( !shufflefile.empty() ) { if( (msg=shuffle_restart()) != 0 ) return msg; }
			for(unsigned int i=0;i<nrunsInt;++i)
			{
				::exegete::shuffle(dataset, folds, rand, stratifiedBool==1);
				if( nfold > 1 ) nfold = (unsigned int)folds.size()-1;
				for(unsigned int j=0;j<nfold;++j)
				{
					if( typeInt == IH )
					{
						trainset.assign_include(dataset, folds[j], folds[j+1]);
						testset.assign_exclude(dataset,  folds[j], folds[j+1]);
					}
					else
					{
						trainset.assign_exclude(dataset, folds[j], folds[j+1]);
						testset.assign_include(dataset,  folds[j], folds[j+1]);
					}
					if(positiveBool) trainset.setPositive();
					ASSERT( (dataset.bagCount() > 0) == (testset.bagCount()  > 0) );
					ASSERT( (dataset.bagCount() > 0) == (trainset.bagCount() > 0) );
					//read model
					if( (msg=handler_type::eval(learner, trainset, testset, measure, 
#ifdef _MPI
						((i+1) == nrunsInt) && ((j+1) == nfold),
#endif
						i, j)) != 0 ) return msg;
				}
				if( (msg=handler_type::next_run(measure)) != 0 ) return msg;
			}
			return 0;
		}
		/** Validates a learning algorithm on the given dataset with the given measure.
		 *	-# Sample: Drawn a random sample of some proportion size
		 *
		 * @param learner the learning algorithm.
		 * @param dataset a dataset to partition for training and testing.
		 * @param measure a class to store or measures a set of predictions.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		const char* validate_samp(L& learner, D& dataset, measure_type& measure)
		{
			typedef typename L::dataset_type learnset_type;
			typedef typename learnset_type::attribute_type attribute_type;
			typedef typename learnset_type::class_type class_type;
			typedef ExampleSet<attribute_type, class_type, std::string*> testset_type;
			const char* msg;
			learnset_type trainset(dataset);
			testset_type testset(dataset);
			if( (msg=handler_type::init(learner, dataset, measure, nrunsInt, nrunsInt)) != 0 ) return msg;
			for(unsigned int i=0;i<nrunsInt;++i)
			{
				::exegete::standard_sample(dataset, trainset, testset, rand);
				if( positiveBool ) trainset.setPositive();
				if( (msg=handler_type::eval(learner, trainset, testset, measure, 
#ifdef _MPI
						(i+1)==nrunsInt,
#endif
						i)) != 0 ) return msg;
				if( (msg=handler_type::next_run(measure)) != 0 ) return msg;
			}
			return 0;
		}
		/** Validates a learning algorithm on the given dataset with the given measure.
		 *	-# Progressive: Start with n-fold training set and progressiving predict and add examples.
		 *	-# Invert-progressive: Start with n-fold inverted training set and progressiving predict and add examples.
		 *
		 * @param learner the learning algorithm.
		 * @param dataset a dataset to partition for training and testing.
		 * @param measure a class to store or measures a set of predictions.
		 * @return an error message or NULL.
		 */
		template<class L, class D>
		const char* validate_prog(L& learner, D& dataset, measure_type& measure)
		{
			if( dataset.bagCount() > 0 ) return validate_prog(learner, dataset, measure, dataset.bag_begin());
			else return validate_prog(learner, dataset, measure, dataset.begin());
		}

	private:
		template<class L, class D, class I>
		const char* validate_prog(L& learner, D& dataset, measure_type& measure, I beg)
		{
			typedef typename L::dataset_type learnset_type;
			typedef typename learnset_type::attribute_type attribute_type;
			typedef typename learnset_type::class_type class_type;
			typedef ExampleSet<attribute_type, class_type, std::string*> testset_type;
			typedef std::vector< typename learnset_type::size_type > index_vector;
			const char* msg;
			index_vector folds(nfoldInt+1);
			::exegete::shuffle(dataset, folds, rand, stratifiedBool==1);
			learnset_type trainset(dataset);
			testset_type testset(trainset, prognInt);
			I end = beg+folds[1];
			if( typeInt == IP )
			{
				trainset.assign_include(dataset, folds[0], folds[1]);
				beg = end; end = dataset.any_end(beg);
			}
			else trainset.assign_exclude(dataset, folds[0], folds[1]);
			if( (msg=handler_type::init(learner, dataset, measure, nrunsInt, std::distance(beg,end))) != 0 ) return msg;
			for(unsigned int j=0, i;beg != end;++j)
			{
				if( positiveBool ) trainset.setPositive();
				testset.assign(beg, std::min(beg+prognInt, end));
				if( (msg=handler_type::eval(learner, trainset, testset, measure, 
#ifdef _MPI
						(beg+prognInt) >= end,
#endif
						
						j)) != 0 ) return msg;
				for(i=0;i<prognInt && beg != end;++i,++beg) trainset.append(beg);
			}
			return handler_type::next_run(measure);
		}
		const char* shuffle_restart()
		{
			const char* msg;
			if( handler_type::restart() )
			{
				if( readfile(shufflefile, rand) != 0 )
				{
					if( (msg=writefile(shufflefile, rand)) != 0 ) return msg;
				}
			}
			else if( testfile(shufflefile) == 0 )
			{
				if( (msg=readfile(shufflefile, rand)) != 0 ) return msg;
			}
			else
			{
				if( (msg=writefile(shufflefile, rand)) != 0 ) return msg;
			}
			return 0;
		}

	public:
		/** Set a validation type.
		 * 
		 * @param t a validation type.
		 */
		void type(int t)
		{
			typeInt=t;
		}
		/** Gets the validation type code.
		 *
		 * @return a validation type code.
		 */
		int type()const
		{
			return typeInt;
		}
		/** Tests if type is partition.
		 * 
		 * @return true if type is partition.
		 */
		bool is_partition_type()const
		{
			return is_parition(typeInt) || is_sample(typeInt) || is_progressive(typeInt);
		}
		/** Gets the sample alpha parameter.
		 *
		 * @return sample alpha parameter.
		 */
		float alpha()const
		{
			return rand.alpha();
		}
		/** Tests if dataset partitioning is stratified.
		 *
		 * @return true if dataset partitioning stratified.
		 */
		bool stratified()const
		{
			return stratifiedBool==1;
		}
		/** Tests whether to force instances as positive.
		 *
		 * @return true if force positive.
		 */
		bool positive()const
		{
			return positiveBool==1;
		}
		/** Gets the number of parition folds.
		 *
		 * @return number of parition folds.
		 */
		unsigned int nfold()const
		{
			return nfoldInt;
		}
		/** Gets the number of runs.
		 *
		 * @return number of parition runs.
		 */
		unsigned int nruns()const
		{
			return nrunsInt;
		}
		/** Gets the progressive increment.
		 *
		 * @return progressive increment.
		 */
		unsigned int progn()const
		{
			return prognInt;
		}
		/** Gets a string representation of the validation type.
		 *
		 * @return validation string.
		 */
		std::string toString()const
		{
			if( typeInt == SA )
			{
				if( rand.alpha() == 0.0f ) return typeName(typeInt)+" validation with replacement";
				return typeName(typeInt)+" validation without replacement";
			}
			else return paritionType(prognInt, nfoldInt, nrunsInt, stratifiedBool==1)+" "+typeName(typeInt)+" validation";
		}

	private:
		static bool is_validation(int t)
		{
			return 	is_parition(t) || 
					is_sample(t) ||
					is_progressive(t);
		}
		static bool is_parition(int t)
		{
			return t == CV || t == HO || t == IH;
		}
		static bool is_sample(int t)
		{
			return t == SA;
		}
		static bool is_progressive(int t)
		{
			return t == PV || t == IP;
		}
		static std::string typeName(int t)
		{
			switch(t)
			{
			case CV:
				return "Cross";
			case HO:
				return "Holdout";
			case IH:
				return "Holdout(inv)";
			case SA:
				return "Bootstrap";
			case PV:
				return "Progressive";
			case IP:
				return "Progressive(inv)";
			default:
				return "None";
			};
		}
		static std::string paritionType(int i, unsigned int n, unsigned int r, bool s)
		{
			std::string nfold; valueToString(n, nfold);
			std::string inc; 
			if( i > 1 ) 
			{
				valueToString(i, inc); 
				inc = "by "+inc;
			}
			std::string nruns = (r>1) ? "Iterated " : "";
			std::string strat = (s) ? "Stratified " : "";
			return nruns+strat+nfold+"-fold"+inc;
		}

	private:
		char typeInt;
		int stratifiedBool;
		int positiveBool;
		unsigned int nfoldInt;
		unsigned int nrunsInt;
		unsigned int prognInt;
		std::string shufflefile;
		SampleRand rand;
	};
};

#endif


