/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * NameLevelVisitor.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_NAMELEVELVISITOR_H
#define _EXEGETE_NAMELEVELVISITOR_H
#include <vector>

/** @file NameLevelVisitor.h
 * @brief Contains the name level visitor
 * 
 * This file contains the NameLevelVisitor class.
 *
 * @ingroup ExegeteLearnerVisitor
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Determines the level of a program name
	 * 
	 * This class provides a Visitor interface to determine program name level.
	 */
	class NameLevelVisitor
	{
	public:
		/** Constructs a name level visitor.
		 * 
		 * @param s a program name string.
		 */
		NameLevelVisitor(const std::string& s) : name(s), level(-1)
		{
		}
		
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a WillowNode; does nothing.
		 * 
		 * @param obj a source WillowNode object.
		 * @return false
		 */
		template<class T>
		bool visit(const WillowNode<T>& obj)
		{
			return false;
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj)
		{
			if( level == -1 && name == T::progname() ) level = T::ARGUMENT;
		}
		/** Implicity converts an object to an int.
		 * 
		 * @return an integer level.
		 */
		operator int()
		{
			return level;
		}
		
	private:
		std::string name;
		int level;
	};
};

#endif


