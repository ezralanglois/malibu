/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * NameVectorVisitor.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_NAMEVECTORVISITOR_H
#define _EXEGETE_NAMEVECTORVISITOR_H
#include <vector>
#include <string>

/** @file NameVectorVisitor.h
 * @brief Contains name visitor classes
 * 
 * This file contains name visitor classes.
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Build full name of learning algorithm.
	 * 
	 * This class provides a Visitor interface to build full name of learning 
	 * algorithm.
	 */
	class FullNameVisitor : public std::string
	{
		typedef std::string parent_type;
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a WillowNode; does nothing.
		 * 
		 * @param obj a source WillowNode object.
		 * @return false
		 */
		template<class T>
		bool visit(const WillowNode<T>& obj)
		{
			return false;
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj)
		{
			std::string& str = *this;
			std::string nm = T::name();
			if( nm != "" )
			{
				if( str == "" )
					str += nm;
				else str += " on " + nm;
			}
		}
	};
	/** @brief Accumulate a vector of program names
	 * 
	 * This class provides a Visitor interface to accumulate a vector of program names.
	 */
	class NameVectorVisitor : public std::vector< std::string >
	{
		typedef std::vector< std::string > parent_type;
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a WillowNode; does nothing.
		 * 
		 * @param obj a source WillowNode object.
		 * @return false
		 */
		template<class T>
		bool visit(const WillowNode<T>& obj)
		{
			return false;
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj)
		{
#ifdef _WEIGHTED
			if( T::ARGUMENT == 1 )
			{
				std::string tmp = T::progname();
				parent_type::push_back(tmp.substr(1));
			}
#endif
			parent_type::push_back(T::progname());
		}
	};
	/** @brief Build a program name of a learner
	 * 
	 * This class provides a Visitor interface to build a program name of a learner.
	 */
	class NameStringVisitor : public std::string
	{
		typedef std::string parent_type;
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a WillowNode; does nothing.
		 * 
		 * @param obj a source WillowNode object.
		 * @return false
		 */
		template<class T>
		bool visit(const WillowNode<T>& obj)
		{
			return false;
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj)
		{
			std::string& str = *this;
			if( T::progname() != "select")
				str += T::progname();
		}
	};
};

#endif


