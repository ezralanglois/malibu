/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowConservedVisitor.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_WILLOWCONSERVEDVISITOR_H
#define _EXEGETE_WILLOWCONSERVEDVISITOR_H
#include <map>
#include <vector>
#include <string>
#include "Graph.h"
#include "AttributeHeader.h"

/** @file WillowConservedVisitor.h
 * @brief Determines conserved rules in a graph
 * 
 * This file contains the WillowConservedVisitor class.
 *
 * @ingroup ExegeteLearnerVisitor
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Determines conserved rules in a graph
	 * 
	 * This class provides a Visitor to estimate conservation.
	 */
	template<class X, class Y>
	class WillowConservedVisitor
	{
		typedef std::map<std::string, unsigned int> conserve_map;
		typedef std::map<long, std::string> node_map;
	public:
		/** Constructs a willow conserved visitor.
		 */
		WillowConservedVisitor() : total(0), checkConserved(false)
		{
		}
		
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a willow node.
		 * 
		 * @param obj a source willow node.
		 * @return whether to visit children.
		 */
		template<class T>
		bool visit(const WillowNode<T>& node)
		{
			if( int(node.type()) == WillowNode<T>::Leaf )
			{
				if( !checkConserved ) build(node);
				else conserved(node);
			}
			return false;
		}
#endif
#ifdef _EXEGETE_WILLOWKMRSTUMP_H
		/** Visit a willow KMR stump.
		 * 
		 * @param obj a willow KMR stump.
		 */
		template<class A, class C, class W>
		void visit(const WillowKMRStump<A,C,W>& obj)
		{
			checkConserved = !conserveMap.empty();
			++total;
		}
#endif
#ifdef _EXEGETE_WILLOWTREE_H
		/** Visit a willow tree.
		 * 
		 * @param obj a willow tree.
		 */
		template<class A, class C, class W>
		void visit(const WillowTree<A,C,W>& obj)
		{
			checkConserved = !conserveMap.empty();
			++total;
		}
#endif
#ifdef _EXEGETE_WILLOWADTREE_H
		/** Visit a willow ADTree.
		 * 
		 * @param obj a willow ADTree.
		 */
		template<class A, class C>
		void visit(const WillowADTree<A,C>& obj)
		{
			checkConserved = !conserveMap.empty();
			++total;
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj){}
		/** Setup the graph.
		 * 
		 * @param graph a graph.
		 */
		void setup(Graph& graph)
		{
			float val;
			for(typename Graph::node_iterator beg = graph.node_begin(), end=graph.node_end();beg != end;++beg)
			{
				val = float(conserveMap[nodeMap[ beg->second.id() ]])/float(total);
				beg->second.conserved(val);
			}
		}
		
	private:
#ifdef _EXEGETE_WILLOWNODE_H
		template<class T>
		void build(const WillowNode<T>& node, std::string str="")
		{
			if( int(node.type()) != WillowNode<T>::Leaf )
			{
				std::string tmp; valueToString(node.index(), tmp);
				str+=tmp;
				conserveMap[str] = 0;
				nodeMap[long(&node)] = str;
			}
			for(unsigned int i=0,n=node.size();i<n;++i) build(node[i], str);
		}
		template<class T>
		void conserved(const WillowNode<T>& node, std::string str="")
		{
			if( int(node.type()) != WillowNode<T>::Leaf )
			{
				std::string tmp; valueToString(node.index(), tmp);
				str+=tmp;
				typename conserve_map::iterator it = conserveMap.find(str);
				if( it != conserveMap.end() ) it->second++;
			}
			for(unsigned int i=0,n=node.size();i<n;++i) conserved(node[i], str);
		}
#endif
		
	private:
		unsigned int total;
		bool checkConserved;
		conserve_map conserveMap;
		node_map nodeMap;
	};
};

#endif



