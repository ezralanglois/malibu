/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowGraphVisitor.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_WILLOWGRAPHVISITOR_H
#define _EXEGETE_WILLOWGRAPHVISITOR_H
#include <map>
#include <vector>
#include <string>
#include "Graph.h"
#include "AttributeHeader.h"

/** @file WillowGraphVisitor.h
 * @brief Contains willow graph visitor
 * 
 * This file contains the WillowGraphVisitor class.
 *
 * @ingroup ExegeteLearnerVisitor
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Interface to build a graph of a willow tree
	 * 
	 * This class provides a Visitor interface to build a graph.
	 */
	template<class X, class Y>
	class WillowGraphVisitor
	{
		typedef std::map<long, unsigned int> index_map;
		typedef typename index_map::iterator iterator;
	public:
		/** Constructs a willow graph visitor.
		 * 
		 * @param g a source graph.
		 * @param h a dataset header.
		 */
		WillowGraphVisitor(Graph& g, const AttributeHeader<X,Y>& h) : pgraph(&g), pheader(&h)
		{
		}
		
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a willow node.
		 * 
		 * @param obj a source willow node.
		 * @return true if should visit children.
		 */
		template<class T>
		bool visit(const WillowNode<T>& node)
		{
			typedef typename WillowNode<T>::attribute_type attribute_type;
			unsigned int rank = incrementDepth(node);
			int type = node.type();
			std::string str1, str2;
			if( type != WillowNode<T>::Leaf )
			{
				valueToString(node.order(), str2);
				//valueToString(orderMap[long(&node)], str2);
				ASSERTMSG(node.index()<pheader->attributeCount(), node.index() << " < " << pheader->attributeCount());
				str1 = pheader->attributeAt(node.index()).name();
				str1 += "(" + str2 + ")";
				pgraph->addnode(long(&node), str1, rank, type);
			}
			switch(type)
			{
			case WillowNode<T>::Leaf:
				valueToString(node.threshold(), str1);
				pgraph->addnode(long(&node), str1, rank, type);
				for(unsigned int i=0,n=node.size();i<n;++i)
				{
					incrementDepth(node[i], rank);
					pgraph->addedge(long(&node), long(&node[i]), "", type+1);
				}
				break;
			case WillowNode<T>::Nominal:
				for(unsigned int i=0,n=node.size();i<n;++i)
				{
					incrementDepth(node[i], rank);
					str1 = pheader->attributeAt(node.index())[i];
					pgraph->addedge(long(&node), long(&node[i]), str1, type+1);
				}
				break;
			case WillowNode<T>::Real:
				str1="";
				valueToString(node.threshold(), str1);
				pgraph->addedge(long(&node), long(&node[0]), str1, type+1);
				pgraph->addedge(long(&node), long(&node[1]), "", -(type+1));
				incrementDepth(node[0], rank);
				incrementDepth(node[1], rank);
				break;
			default:
				str1=""; str2="";
				for(unsigned int i=0,n=pheader->attributeAt(node.index()).size();i<n;++i)
				{
					if( node.belongsTo(attribute_type(i)) ) 
					{
						str1+= pheader->attributeAt(node.index())[i];
						if( i != (n-1) ) str1+="|";
					}
					else
					{
						str2+= pheader->attributeAt(node.index())[i];
						if( i != (n-1) ) str2+="|";
					}
				}
				pgraph->addedge(long(&node), long(&node[0]), str1, type+1);
				pgraph->addedge(long(&node), long(&node[1]), str2, -(type+1));
				incrementDepth(node[0], rank);
				incrementDepth(node[1], rank);
			};
			return true;
		}
#endif
#ifdef _EXEGETE_WILLOWKMRSTUMP_H
		/** Visit a willow KMR stump (does nothing).
		 * 
		 * @param obj a willow KMR stump.
		 */
		template<class A, class C, class W>
		void visit(const WillowKMRStump<A,C,W>& obj)
		{
		}
#endif
#ifdef _EXEGETE_WILLOWTREE_H
		/** Visit a willow tree (does nothing).
		 * 
		 * @param obj a willow tree.
		 */
		template<class A, class C, class W>
		void visit(const WillowTree<A,C,W>& obj)
		{
		}
#endif
#ifdef _EXEGETE_WILLOWADTREE_H
		/** Visit a willow ADTree.
		 * 
		 * @param obj a willow ADTree.
		 */
		template<class A, class C>
		void visit(const WillowADTree<A,C>& obj)
		{
			//orderMap.clear();
			//for(unsigned int i=0;i<obj.iterations();++i)
			//	orderMap[obj.order(i)] = i+1;
		}
#endif
#ifdef _C45
		/** Visit a C4.5 tree.
		 * 
		 * @param tree a C4.5 tree.
		 */
		void visit(const C45& tree)
		{
			tree.graph(*pgraph);
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj){}
		
	private:
#ifdef _EXEGETE_WILLOWNODE_H
		template<class T>
		unsigned int incrementDepth(const WillowNode<T>& node, int rank=0)
		{
			rank++;
			iterator it = depthMap.insert(depthMap.begin(), std::make_pair(long(&node), rank));
			return it->second;
		}
#endif
		
	private:
		Graph* pgraph;
		//index_map orderMap;
		index_map depthMap;
		const AttributeHeader<X,Y>* pheader;
	};
};

#endif



