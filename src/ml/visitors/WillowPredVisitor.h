/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowPredVisitor.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_WILLOWPREDVISITOR_H
#define _EXEGETE_WILLOWPREDVISITOR_H
#include <map>
#include <set>
#include <vector>
#include <string>
#include "Graph.h"

/** @file WillowPredVisitor.h
 * 
 * @brief Add predictions to a willow graph
 * 
 * This file contains the WillowPredVisitor class.
 *
 * @ingroup ExegeteLearnerVisitor
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Add predictions to a willow graph
	 * 
	 * This class provides a Visitor to add predictions.
	 */
	template<class D>
	class WillowPredVisitor
	{
		typedef std::vector<long> long_vector;
		typedef typename D::value_type example_type;
	public:
		/** Constructs a willow prediction visitor.
		 * 
		 * @param g a graph.
		 * @param set a testset.
		 */
		WillowPredVisitor(Graph& g, const D& set) : pgraph(&g), pset(&set)
		{
			unsigned int rank = g.highestRank()+1;
			long id;
			long_vector saved(g.nodeCount());
			unsigned int i=0;
			for(typename Graph::const_node_iterator beg = g.node_begin(), end=g.node_end();beg != end;++beg, ++i)
			{
				if( g.isleaf(beg->second) ) saved[i] = beg->first;
			}
			for(typename Graph::const_node_iterator beg=g.node_begin(), end=g.node_end();beg != end;++beg)
			{
				if( std::find(saved.begin(), saved.end(), beg->first) != saved.end() )
				{
					id = long(&beg->second.label());
					g.addnode(id, "", rank, Graph::HTML);
					g.addedge(beg->first, id);
				}
			}
			g.resize_groups(set.labelCount()+1);
			typename Graph::count_iterator it;
			for(typename D::const_iterator beg=set.begin(), end=set.end();beg != end;++beg)
			{
				for(i=0;i<set.labelCount();++i)
				{
					it = g.group(i).insert(g.group(i).begin(), std::make_pair(beg->labelAt(i), 0));
					it->second++;
				}
				it = g.group(i).insert(g.group(i).begin(), std::make_pair(set.classAt(beg->y()), 0));
				it->second++;
			}
			g.exampleCount(set.size());
		}
		
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a willow node.
		 * 
		 * @param obj a source willow node.
		 * @return whether to visit children (false)
		 */
		template<class T>
		bool visit(const WillowNode<T>& node)
		{
			long id;
			unsigned int len;
			unsigned int ln=pset->labelCount();
			std::string cl, str;
			long_vector vec(pgraph->nodeCount());
			for(typename D::const_iterator beg = pset->begin(), end = pset->end();beg != end;++beg)
			{
				len = node.predictarray(*beg, vec, example_type::missing());
				cl = pset->classAt(beg->y());
				for(unsigned int i=0;i<len;++i)
				{
					id = long(&pgraph->get(vec[i]).label());
					str = "";
					for(unsigned int j=0;j<ln;++j)
					{
						str += beg->labelAt(j);
						str += ":";
					}
					str += cl;
					str += "\n";
					pgraph->get(id) += str;
				}
			}
			return false;
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj)
		{
		}
		
	private:
		Graph* pgraph;
		const D* pset;
	};
	
};

#endif



