/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * WillowPredictionVisitor.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_WILLOWPREDICTIONVISITOR_H
#define _EXEGETE_WILLOWPREDICTIONVISITOR_H
#include <vector>
#include <string>
#include "AbstractFormat.h"

/** @file WillowPredictionVisitor.h
 * 
 * @brief Determines predictions for rules of a willow tree
 * 
 * This file contains the WillowPredictionVisitor class.
 *
 * @ingroup ExegeteLearnerVisitor
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Determines predictions for rules of a willow tree
	 * 
	 * This class provides a Visitor to determine predictions for rules of a willow tree.
	 */
	template<class D>
	class WillowPredictionVisitor : public AbstractFormat
	{
		typedef std::map<long, unsigned int> index_map;
		typedef std::vector<std::string> string_vector;
		typedef std::vector<string_vector> string_vector2d;
		typedef typename D::value_type example_type;
	public:
		/** Constructs a willow prediction visitor.
		 * 
		 * @param set a testset.
		 */
		WillowPredictionVisitor(const D& set) : pset(&set)
		{
		}
		
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a willow node.
		 * 
		 * @param obj a source willow node.
		 * @return whether to visit children.
		 */
		template<class T>
		bool visit(const WillowNode<T>& node)
		{
			//long id;
			unsigned int len, i,j;
			unsigned int ln=pset->labelCount();
			std::string cl, str;
			string_vector vec;
			std::vector< std::pair<const WillowNode<T>*, unsigned int>  > pvec;
			for(typename D::const_iterator beg = pset->begin(), end = pset->end();beg != end;++beg)
			{
				vec.resize(pset->labelCount()+1);
				for(i=0;i<ln;++i) vec[i] = beg->labelAt(i);
				vec[i] = pset->classAt(beg->y());
				len = node.predictarray2(*beg, pvec, example_type::missing());
				vec.resize(vec.size()+len);
				for(i=0,j=ln+1;i<len;++i,++j)
				{
					vec[j] = pset->attributeAt(pvec[i].first->index()).name();
					//ASSERT(orderMap.find(long(pvec[i].first)) != orderMap.end());
					//valueToString(orderMap[long(pvec[i].first)],str);
					valueToString(pvec[i].first->order(),str);
					vec[j] += "(" + str + ")-";
					valueToString(pvec[i].second,str);
					vec[j] += str;
				}
				predictions.push_back(vec);
			}
			return false;
		}
#endif
#ifdef _EXEGETE_WILLOWADTREE_H
		/** Visit a willow ADTree.
		 * 
		 * @param obj a willow ADTree.
		 */
		template<class A, class C>
		void visit(const WillowADTree<A,C>& obj)
		{
			//orderMap.clear();
			//for(unsigned int i=0;i<obj.iterations();++i)
			//	orderMap[obj.order(i)] = i+1;
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj){}
		
	public:
		/** Writes a format to an output stream.
		 *
		 * @param out a reference to an output stream.
		 * @return an error message or NULL
		 */
		const char* write(std::ostream& out)const
		{
			for(unsigned int i=0,j;i<predictions.size();++i)
			{
				if( predictions[i].empty() ) continue;
				out << predictions[i][0];
				for(j=1;j<predictions[i].size();++j)
					out << "," << predictions[i][j];
				out << "\n";
			}
			return 0;
		}
		
	private:
		const D* pset;
		string_vector2d predictions;
		//index_map orderMap;
	};
	
};

#endif



