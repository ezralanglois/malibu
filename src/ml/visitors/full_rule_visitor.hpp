/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * full_rule_visitor.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_GRAPH_FULL_RULE_VISITOR_HPP
#define _EXEGETE_GRAPH_FULL_RULE_VISITOR_HPP
#include "AbstractFormat.h"

/** @file full_rule_visitor.hpp
 * 
 * @brief Collects binary rules
 * 
 * This file contains the full rule visitor class, which collects rules from 
 * learning algorithms.
 *
 * @ingroup ExegeteLearnerVisitor
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

	/** @brief Collects binary rules
	 * 
	 * This class defines a visitor that collects rules from learning 
	 * algorithms and rule counts.
	 */
	template<class D>
	class full_rule_visitor : public AbstractFormat
	{
		typedef std::vector< std::vector<int> > vector_2d;
	public:
		/** Constructs a rule visitor.
		 * 
		 * @param d a dataset.
		 */
		full_rule_visitor(const D& d) : pdataset(&d)
		{
		}
		
	public:
#ifdef _C45
		/** Visit a C4.5 tree.
		 * 
		 * @param tree a C4.5 tree.
		 */
		void visit(const C45& tree)
		{
			tree.graph(vec);
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj){}
		
	public:
		/** Writes a format to the output stream.
		 *
		 * @param out a reference to an output stream.
		 * @return an error message or NULL.
		 */
		const char* write(std::ostream& out)const
		{
			return 0;
		}
		
	private:
		const D* pdataset;
		vector_2d vec;
	};
};

#endif





