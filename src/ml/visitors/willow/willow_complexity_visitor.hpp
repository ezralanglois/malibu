/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * willow_complexity_visitor.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_WILLOW_COMPLEXITY_VISITOR_HPP
#define _EXEGETE_WILLOW_COMPLEXITY_VISITOR_HPP

/** @file willow_complexity_visitor.hpp
 * 
 * @brief Collects complexity statistics from willow tree nodes
 * 
 * This file contains the willow complexity visitor class.
 *
 * @ingroup ExegeteLearnerVisitor
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

	/** @brief Collects complexity statistics from willow tree nodes
	 * 
	 * This class collects complexity statistics from willow tree nodes.
	 */
	class willow_complexity_visitor
	{
	public:
		/** Constructs a willow complexity visitor.
		 */
		willow_complexity_visitor()
		{
		}
		
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a willow node.
		 * 
		 * @param node a source willow node.
		 * @return whether to visit children.
		 */
		template<class T>
		bool visit(const WillowNode<T>& node)
		{
			unsigned int d = max_depth(node);
			unsigned int c = node_count(node);
			maxVec.back().push_back(d);
			maxVec.back().push_back(c);
			return false;
		}
#endif
#ifdef _EXEGETE_WILLOWKMRSTUMP_H
		/** Visit a willow KMR stump.
		 * 
		 * @param obj a willow KMR stump.
		 */
		template<class A, class C, class W>
		void visit(const WillowKMRStump<A,C,W>& obj)
		{
		}
#endif
#ifdef _EXEGETE_WILLOWTREE_H
		/** Visit a willow tree.
		 * 
		 * @param obj a willow tree.
		 */
		template<class A, class C, class W>
		void visit(const WillowTree<A,C,W>& obj)
		{
			maxVec.resize(maxVec.size()+1);
		}
#endif
#ifdef _EXEGETE_WILLOWADTREE_H
		/** Visit a willow ADTree.
		 * 
		 * @param obj a willow ADTree.
		 */
		template<class A, class C>
		void visit(const WillowADTree<A,C>& obj)
		{
			maxVec.resize(maxVec.size()+1);
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj)
		{
			maxVec.clear();
		}
		
	public:
		/** Write rules to an output stream.
		 * 
		 * @param out an output stream.
		 */
		void write(std::ostream& out)const
		{
			for(unsigned int i=0;i<maxVec.size();++i)
			{
				for(unsigned int j=0;j<maxVec[i].size();++j)
				{
					if( j > 0 ) out << "\t";
					out << maxVec[i][j];
				}
				out << "\n";
			}
		}
		
	private:
#ifdef _EXEGETE_WILLOWNODE_H
		template<class T>
		unsigned int max_depth(const WillowNode<T>& node, unsigned int d=0)
		{
			unsigned int md=d;
			for(unsigned int i=0,n=node.size();i<n;++i) md=std::max(max_depth(node[i], d+1), md);
			return md;
		}
		template<class T>
		unsigned int node_count(const WillowNode<T>& node)
		{
			if( int(node.type()) != WillowNode<T>::Leaf )
			{
				unsigned int cnt=0;
				for(unsigned int i=0,n=node.size();i<n;++i) cnt+=node_count(node[i]);
				return cnt;
			}
			return 1;
		}
#endif
		
	private:
		std::vector< std::vector<unsigned int> > maxVec;
	};
};

#endif





