/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * willow_rule_visitor.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_WILLOW_RULE_VISITOR_HPP
#define _EXEGETE_WILLOW_RULE_VISITOR_HPP

/** @file willow_rule_visitor.hpp
 * 
 * @brief Collects rules from willow tree nodes and their counts.
 * 
 * This file contains the willow node visitor class, which collects rules from 
 * willow nodes and rule counts.
 *
 * @ingroup ExegeteLearnerVisitor
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{

	/** @brief Collects rules and their counts.
	 * 
	 * This class collects rules from willow nodes and rule counts.
	 */
	template<class D>
	class willow_rule_visitor
	{
		typedef std::map<std::string, unsigned int> conserve_map;
	public:
		/** Constructs a willow node rule visitor.
		 * 
		 * @param d a dataset.
		 * @param p a percent of support.
		 */
		willow_rule_visitor(const D& d, float p) : pdataset(&d), total(0), percent(p)
		{
		}
		
	public:
#ifdef _EXEGETE_WILLOWNODE_H
		/** Visit a willow node.
		 * 
		 * @param node a source willow node.
		 * @return whether to visit children.
		 */
		template<class T>
		bool visit(const WillowNode<T>& node)
		{
			if( int(node.type()) == WillowNode<T>::Leaf )
			{
				build(node);
			}
			return false;
		}
#endif
#ifdef _EXEGETE_WILLOWKMRSTUMP_H
		/** Visit a willow KMR stump.
		 * 
		 * @param obj a willow KMR stump.
		 */
		template<class A, class C, class W>
		void visit(const WillowKMRStump<A,C,W>& obj)
		{
			++total;
		}
#endif
#ifdef _EXEGETE_WILLOWTREE_H
		/** Visit a willow tree.
		 * 
		 * @param obj a willow tree.
		 */
		template<class A, class C, class W>
		void visit(const WillowTree<A,C,W>& obj)
		{
			++total;
		}
#endif
#ifdef _EXEGETE_WILLOWADTREE_H
		/** Visit a willow ADTree.
		 * 
		 * @param obj a willow ADTree.
		 */
		template<class A, class C>
		void visit(const WillowADTree<A,C>& obj)
		{
			++total;
		}
#endif
		/** Visit a general learning object.
		 * 
		 * @param obj a source learning object.
		 */
		template<class T>
		void visit(const T& obj){}
		
	public:
		/** Write rules to an output stream.
		 * 
		 * @param out an output stream.
		 */
		void write(std::ostream& out)const
		{
			unsigned int t = (unsigned int)(percent*total);
			for(conserve_map::const_iterator beg = conserveMap.begin(), end=conserveMap.end();beg != end;++beg)
			{
				if( beg->second > t )
					out << beg->first << "\t" << beg->second << "\n";
			}
		}
		
	private:
#ifdef _EXEGETE_WILLOWNODE_H
		template<class T>
		void build(const WillowNode<T>& node, std::string str="")
		{
			if( int(node.type()) != WillowNode<T>::Leaf )
			{
				std::string tmp = pdataset->attributeAt(node.index()).name();
				//std::string tmp; valueToString(node.index(), tmp);
				if( str != "" ) str+="\t";
				str+=tmp;
				typename conserve_map::iterator it = conserveMap.insert(conserveMap.begin(), std::make_pair(str,0u));
				it->second++;
			}
			for(unsigned int i=0,n=node.size();i<n;++i) build(node[i], str);
		}
#endif
		
	private:
		const D* pdataset;
		unsigned int total;
		float percent;
		conserve_map conserveMap;
	};
};

#endif





