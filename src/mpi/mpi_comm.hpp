/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mpi_comm.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


/** @defgroup ExegeteMPI MPI System
 * 
 *  This group contains all files related to MPI communications.
 */

#ifndef _EXEGETE_MPI_COMM_HPP
#define _EXEGETE_MPI_COMM_HPP
#include <mpi.h>
#include "typeutil.h"

#if defined(_MPI) && defined(_MPI_DEBUG)
#include <iostream>
#ifdef _MPI_DEBUG_ONLY
/** Defines only use debug methods with level equal to _MPI_DEBUG. **/
#define MPI_DEBUG_USE(X,Y) if( Y == _MPI_DEBUG ) X
#else
/** Defines only use debug methods with level greater than _MPI_DEBUG. **/
#define MPI_DEBUG_USE(X,Y) if( Y > _MPI_DEBUG ) X
#endif
#else
/** Defines no debug methods. **/
#define MPI_DEBUG_USE(X,Y) ((void)0)
#endif
/** Defines debug printing. **/
#define MPI_DEBUG_PRINT(X,Y) MPI_DEBUG_USE(std::cerr << X << std::endl,Y)

/** @file mpi_comm.hpp
 * 
 * @brief Handles MPI communications
 * 
 * This file contains the MPI comm class.
 *
 * @ingroup ExegeteMPI
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail
	{
		template<class T> struct mpi_ptr_util;
	};
	
	/** @brief Handles MPI communications
	 * 
	 * This class handles MPI communications. 
	 */
	class mpi_comm
	{
	public:
		/** Initalizes the MPI framework with the given arguments and
		 * tests if the program is given at least two computers/processors.
		 * 
		 * @param argc number of arguments.
		 * @param argv list of string arguments.
		 * @return an error message or NULL.
		 */
		static const char* init(int& argc, char **& argv)
		{
			if( (is_error()=MPI_Init(&argc, &argv)) != MPI_SUCCESS ) 
				return ERRORMSG("Error initalizing MPI");
			if( rank() == 0 && size() < 2 ) 
				return ERRORMSG("MPI requires at least two processors");
			return 0;
		} 
		/** Causes MPI to exit all jobs:
		 * 	- Program is finished, finalize MPI
		 * 	- Error occurred, abort all jobs
		 */
		static void mpi_exit()
		{
			if( !is_init() ) return;
			if( is_error() == MPI_SUCCESS ) MPI_Finalize();
			else MPI_Abort(MPI_COMM_WORLD, is_error());
		}
		/** Write an MPI error message to an output stream.
		 * 
		 * @param out an output stream.
		 */
		static void mpi_write_error(std::ostream& out)
		{
			if( !is_init() ) return;
			if( is_error() != MPI_SUCCESS )
			{
				int errlen;
				char errmsg[MPI_MAX_ERROR_STRING];
				MPI_Error_string( is_error(), errmsg, &errlen);
				out << "MPI error: " << errmsg << std::endl;
			}
			else is_error() = 1;
		}
		/** Write an error and exit all MPI jobs.
		 * 
		 * @param out an output stream.
		 */
		static void mpi_write_error_exit(std::ostream& out)
		{
			mpi_write_error(out);
			mpi_exit();
		}
		
	public:
		/** Get the rank of the job.
		 * 
		 * @return rank of job.
		 */
		static int rank()
		{
			ASSERT(is_init());
			int r;
			MPI_Comm_rank(MPI_COMM_WORLD, &r);
			return r;
		}
		/** Get the number of MPI jobs.
		 * 
		 * @return number of MPI jobs.
		 */
		static int size()
		{
			ASSERT(is_init());
			int s;
			MPI_Comm_size(MPI_COMM_WORLD, &s);
			return s;
		}
		/** Test or set error condition.
		 * 
		 * @return reference to error code.
		 */
		static int& is_error()
		{
			static int e=MPI_SUCCESS;
			return e;
		}
		/** Test if MPI framework has already been initalized.
		 * 
		 * @return true if already initalized.
		 */
		static int is_init()
		{
			int flag;
			if( MPI_Initialized(&flag) != MPI_SUCCESS ) return 0;
			return flag;
		}
		
	public:
		/** Broadcast data in raw form (as a string of characters). This method interfaces
		 * the MPI_Bcast function.
		 * 
		 * @note From root to all workers
		 * 
		 * @param p some object(s).
		 * @param n number of objects.
		 * @return true, if no error.
		 */
		template<class T>
		static bool broadcast_raw(T& p, unsigned int n=1)
		{
			typedef detail::mpi_ptr_util<T> util;
			ASSERT(is_init());
			is_error() = MPI_Bcast(util::ref(p), char_index(p,n), TypeUtil<char>::MPI_TYPE(), 0, MPI_COMM_WORLD);
			return is_error() == MPI_SUCCESS;
		}
		/** Broadcast data in some MPI type form. This method interfaces the MPI_Bcast function.
		 * 
		 * @note From root to all workers
		 * 
		 * @param p some object(s).
		 * @param n number of objects.
		 * @return true, if no error.
		 */
		template<class T>
		static bool broadcast(T& p, unsigned int n=1)
		{
			typedef detail::mpi_ptr_util<T> util;
			ASSERT(is_init());
			is_error() = MPI_Bcast(util::ref(p), util::index(p,n), util::MPI_TYPE(), 0, MPI_COMM_WORLD);
			return is_error() == MPI_SUCCESS;
		}
		/** Send data to some process t.
		 * 
		 * @param t process index.
		 * @param p some object(s).
		 * @param n number of objects.
		 * @param tag a tag index.
		 * @return true, if no error.
		 */
		template<class T>
		static bool send(unsigned int t, T& p, unsigned int n=1, int tag=1)
		{
			typedef detail::mpi_ptr_util<T> util;
			ASSERT(is_init());
			ASSERTMSG(t<(unsigned int)size(), t << " < " << size());
			is_error() = MPI_Send(util::ref(p), util::index(p,n), util::MPI_TYPE(), t, tag, MPI_COMM_WORLD);
			return is_error() == MPI_SUCCESS;
		}
		/** Receive data from some process t.
		 * 
		 * @param t process index.
		 * @param p some object(s).
		 * @param stat status of receive.
		 * @param n number of objects.
		 * @param tag a tag index.
		 * @return true, if no error.
		 */
		template<class T>
		static bool recv(unsigned int t, T& p, MPI_Status& stat, unsigned int n=1, int tag=1)
		{
			typedef detail::mpi_ptr_util<T> util;
			ASSERT(is_init());
			ASSERTMSG(t<(unsigned int)size(), t << " < " << size());
			is_error() = MPI_Recv(util::ref(p), util::index(p,n), util::MPI_TYPE(), t, tag, MPI_COMM_WORLD, &stat);
			return is_error() == MPI_SUCCESS;
		}
		/** Receive (non-blocking) data from some process t.
		 * 
		 * @param t process index.
		 * @param p some object(s).
		 * @param reqs a request array.
		 * @param n number of objects.
		 * @param d a MPI data type.
		 * @param tag a tag index.
		 * @return true, if no error.
		 */
		template<class T>//MPI_STATUS_IGNORE
		static bool irecv(unsigned int t, T& p, MPI_Request* reqs, unsigned int n=1, MPI_Datatype d = detail::mpi_ptr_util<T>::MPI_TYPE(), int tag=1)
		{
			typedef detail::mpi_ptr_util<T> util;
			ASSERT(reqs!=0);
			ASSERT(is_init());
			ASSERTMSG(t<(unsigned int)size(), t << " < " << size());
			is_error()=MPI_Irecv(util::ref(p), util::index(p,n), d, t, tag, MPI_COMM_WORLD, &reqs[t]);
			return is_error() == MPI_SUCCESS;
		}
		/** Wait for a non-blocking send or receive.
		 * 
		 * @param reqcnt number of requests.
		 * @param reqs an array of requests.
		 * @param pos current position in request array.
		 * @return true, if no error.
		 */
		static bool wait(int reqcnt, MPI_Request* reqs, int& pos)
		{
			ASSERT(reqs!=0);
			is_error()=MPI_Waitany(reqcnt, reqs, &pos, MPI_STATUS_IGNORE);
			ASSERTMSG(0 < pos && pos < reqcnt, pos << " " << reqcnt);
			return is_error() == MPI_SUCCESS;
		}
		
	private:
		template<class T>
		static unsigned int char_index(T& p, unsigned int i)
		{
			typedef typename detail::mpi_ptr_util<T>::value_type value_type;
			return detail::mpi_ptr_util<T>::index(p, i)*sizeof(value_type);
		}
	};
	
	namespace detail
	{
		/** @brief Utility handling object array, size and type.
		 * 
		 * This class defines utility to handle object arrays, their size
		 * and type.
		 */
		template<class T> 
		struct mpi_ptr_util
		{
			/** Defines a pointer. **/
			typedef T* pointer;
			/** Defines a value type. **/
			typedef T value_type;
			/** Gets a pointer to an object.
			 * 
			 * @param p some object.
			 * @return a pointer to the object.
			 */
			static T* ref(T& p)
			{
				return &p;
			}
			/** Get the size of the object array.
			 * 
			 * @param p some object.
			 * @param i number of objects.
			 * @return number of objects.
			 */
			static unsigned int index(T& p, unsigned int i)
			{
				return i;
			}
			/** Get MPI type of object.
			 * 
			 * @return MPI type.
			 */
			static MPI_Datatype MPI_TYPE()
			{
				return TypeUtil<T>::MPI_TYPE();
			}
		};
		/** @brief Utility handling a pointer to an object, size and type.
		 * 
		 * This class defines utility to handle pointer to an object array, their size
		 * and type.
		 */
		template<class T> 
		struct mpi_ptr_util<T*>
		{
			/** Defines a pointer. **/
			typedef T* pointer;
			/** Defines a value type. **/
			typedef T value_type;
			/** Gets a pointer to an object.
			 * 
			 * @param p a pointer object.
			 * @return a pointer to an object.
			 */
			static T* ref(T* p)
			{
				return p;
			}
			/** Get the size of the object array.
			 * 
			 * @param p some object.
			 * @param i number of objects.
			 * @return number of objects.
			 */
			static unsigned int index(T* p, unsigned int i)
			{
				return i;
			}
			/** Get MPI type of object.
			 * 
			 * @return MPI type.
			 */
			static MPI_Datatype MPI_TYPE()
			{
				return TypeUtil<T>::MPI_TYPE();
			}
		};
		/** @brief Utility handling a pointer to a static object array, size and type.
		 * 
		 * This class defines utility to handle pointer to a static object array, 
		 * their size and type.
		 */
		template<class T, int I> 
		struct mpi_ptr_util<T[I]>
		{
			/** Defines a pointer. **/
			typedef T* pointer;
			/** Defines a value type. **/
			typedef T value_type;

			/** Gets a pointer to an object.
			 * 
			 * @param p a pointer object.
			 * @return a pointer to an object.
			 */
			static T* ref(T* p)
			{
				return p;
			}
			/** Get the size of the object array.
			 * 
			 * @param p some object.
			 * @param i number of objects.
			 * @return number of objects.
			 */
			static unsigned int index(T* p, unsigned int i)
			{
				return I;
			}
			/** Get MPI type of object.
			 * 
			 * @return MPI type.
			 */
			static MPI_Datatype MPI_TYPE()
			{
				return TypeUtil<T>::MPI_TYPE();
			}
		};
		/** @brief Utility handling string holding a character array, size and type.
		 * 
		 * This class defines utility to handlestring holding a character array, 
		 * size and type.
		 */
		template<> 
		struct mpi_ptr_util<std::string>
		{
			/** Defines a pointer. **/
			typedef char* pointer;
			/** Defines a value type. **/
			typedef char value_type;
			/** Gets a pointer to a string.
			 * 
			 * @param p a string.
			 * @return a pointer to a string.
			 */
			static pointer ref(std::string& p)
			{
				return const_cast<pointer>(p.c_str());
			}
			/** Get the size of the object array.
			 * 
			 * @param p some object.
			 * @param i number of characters.
			 * @return number of characters.
			 */
			static unsigned int index(std::string& p, unsigned int i)
			{
				return p.length();
			}
			/** Get MPI char type of object.
			 * 
			 * @return MPI char type.
			 */
			static MPI_Datatype MPI_TYPE()
			{
				return TypeUtil<char>::MPI_TYPE();
			}
		};
		/** @brief Utility handling a constant pointer to an object, size and type.
		 * 
		 * This class defines utility to handle a constant pointer to an object array, 
		 * their size and type.
		 */
		template<class T> 
		struct mpi_ptr_util<const T*>
		{
			/** Defines a pointer. **/
			typedef T* pointer;
			/** Defines a value type. **/
			typedef T value_type;
			/** Gets a pointer.
			 * 
			 * @param p a constant pointer to an object.
			 * @return a pointer to an object.
			 */
			static T* ref(const T* p)
			{
				return const_cast<T*>(p);
			}
			/** Get the size of the object array.
			 * 
			 * @param p some object.
			 * @param i number of objects.
			 * @return number of objects.
			 */
			static unsigned int index(const T* p, unsigned int i)
			{
				return i;
			}
			/** Get MPI type of object.
			 * 
			 * @return MPI type.
			 */
			static MPI_Datatype MPI_TYPE()
			{
				return TypeUtil<T>::MPI_TYPE();
			}
		};
		/** @brief Utility handling a vector of objects, size and type.
		 * 
		 * This class defines utility to handle a vector of objects, size and type.
		 */
		template<class T, class U> 
		struct mpi_ptr_util< std::vector<T, U> >
		{
			/** Defines a pointer. **/
			typedef T* pointer;
			/** Defines a value type. **/
			typedef T value_type;
			/** Gets a pointer to a collection of objects.
			 * 
			 * @param p a vector of objects.
			 * @return a pointer to a collection of objects.
			 */
			static T* ref(std::vector<T, U>& p)
			{
				return &(*p.begin());
			}
			/** Get the size of the object array.
			 * 
			 * @param p some object.
			 * @param i number of objects.
			 * @return number of objects.
			 */
			static unsigned int index(std::vector<T, U>& p, unsigned int i)
			{
				return (unsigned int)p.size();
			}
			/** Get MPI type of object.
			 * 
			 * @return MPI type.
			 */
			static MPI_Datatype MPI_TYPE()
			{
				return TypeUtil<T>::MPI_TYPE();
			}
		};
	};
};
#endif




