/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mpi_def.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MPI_DEF_HPP
#define _EXEGETE_MPI_DEF_HPP

/** @file mpi_def.hpp
 * 
 * @brief Contains macros for conditionally adding MPI functionaility
 * 
 * This file contains macros for conditionally adding MPI functionaility.
 *
 * @ingroup ExegeteMPI
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#ifdef _MPI
#include "mpi_comm.hpp"
#include "mpi_tree.hpp"
#include "mpi_learner.hpp"
/** Defines MPI initalization. **/
#define MPI_INIT(X,Y) ::exegete::mpi_comm::init(X,Y)
/** Defines initalization for an MPI tree. **/
#define MPI_INIT_TREE(L) L.init()
/** Defines an exit of an MPI application.*/
#define MPI_EXIT ::exegete::mpi_comm::mpi_exit()
/** Defines an error exit and message for an MPI application. **/
#define MPI_ERROR(X) ::exegete::mpi_comm::mpi_write_error(X)
/** Defines a test if process is root. **/
#define MPI_IS_ROOT(X) ::exegete::mpi_comm::rank() == 0 
/** Defines a if statement testing if process is root. **/
#define MPI_IF_ROOT if( ::exegete::mpi_comm::rank() == 0 ) {
/** Defines a if statement testing if process is not root. **/
#define MPI_IF_NOT_ROOT if( ::exegete::mpi_comm::rank() != 0 ) {
/** Defines an else statement for an MPI test. **/
#define MPI_IF_ELSE } else {
/** Defines the end for an MPI test. **/
#define MPI_IF_END }
/** Defines an MPI learner tree. **/
#define MPI_LEARNER(X, M, Y, A, P) ::exegete::mpi_learner<X,M> Y(A,P)
/** Defines setting an MPI tree in a learner. **/
#define MPI_SET_LEARNER(X, Y) X.set_mpi_tree(Y)
/** Defines setting up an MPI learner tree. **/
#define MPI_SETUP(T, L, D, V, S) T.mpi_setup(L, D, V, S)
/** Defines finalizing an MPI learner tree. **/
#define MPI_FINALIZE(T,V) T.finalize(V.ispartition())
/** Defines finalizing an MPI learner tree. **/
#define MPI_WAIT(T) T.finalize()
/** Defines child training. **/
#define MPI_CHILD(L, D) L.train_child(D)
/** Defines posting a task to an MPI tree. **/
#define MPI_POST(L, N) L.get_mpi_tree().mpi_post(N)
/** Defines a test for the learner level. **/
#define MPI_LEVEL(L) detail::select_learner<L>::level
/** Defines a child process. **/
#define MPI_CHILD_TRAIN(T, L, D) T.mpi_train_child(L, D) 
/** Defines a if statement testing if the learner exceeds a threshold. **/
#define MPI_IF_LEVEL(L, T) if( detail::select_learner<L>::level > T.get_mpi_tree().level() ) {
/** Selects first parameter if MPI. **/
#define MPI_SELECT(M,N) M
/** Defines an exit and error message. **/
#define MPI_EXIT_ERROR(out, msg) out << msg << std::endl; mpi_write_error_exit(out)
/** Defines an alteration of the parent level. **/
#define MPI_ALTER_PARENT(L, N, M) L.get_mpi_tree().parent(N)->alter_level(M);
/** Defines a method for getting an MPI level name. **/
#define MPI_OPTION(N) static const char* get_mpi_name(){return N;}
#else
/** Defines an empty placeholder for MPI initalization. **/
#define MPI_INIT(X,Y) 0
/** Defines an empty placeholder for initalization of an MPI tree. **/
#define MPI_INIT_TREE(L) ((void)0)
/** Defines an empty placeholder for an exit of an MPI application.*/
#define MPI_EXIT ((void)0)
/** Defines an empty placeholder for an error exit and message for an MPI application. **/
#define MPI_ERROR(X) ((void)0)
/** Defines an empty placeholder for a test if the process is root. **/
#define MPI_IS_ROOT(X) X
/** Defines an empty placeholder for a if statement testing if process is root. **/
#define MPI_IF_ROOT ((void)0);
/** Defines an empty placeholder for a if statement testing if process is not root. **/
#define MPI_IF_NOT_ROOT ((void)0);
/** Defines an empty placeholder for an else statement for an MPI test. **/
#define MPI_IF_ELSE ((void)0);
/** Defines an empty placeholder for the end for an MPI test. **/
#define MPI_IF_END ((void)0);
/** Defines an empty placeholder for an MPI learner tree. **/
#define MPI_LEARNER(X, M, Y, A, P) ((void)0)
/** Defines an empty placeholder for setting an MPI tree in a learner. **/
#define MPI_SET_LEARNER(X, Y) ((void)0)
/** Defines an empty placeholder for setting up an MPI learner tree. **/
#define MPI_SETUP(T, L, D, V, S) 0
/** Defines an empty placeholder for finalizing an MPI learner tree. **/
#define MPI_FINALIZE(T,V) 0
/** Defines an empty placeholder for finalizing an MPI learner tree. **/
#define MPI_WAIT(T) 0
/** Defines an empty placeholder for child processing. **/
#define MPI_CHILD(L, D) 0
/** Defines an empty placeholder for posting a task to an MPI tree. **/
#define MPI_POST(L, N) 0
/** Defines an empty placeholder for a test for the learner level. **/
#define MPI_LEVEL(L) ((void)0)
/** Defines an empty placeholder for a child process. **/
#define MPI_CHILD_TRAIN(T, L, D) 0
/** Defines an empty placeholder for a if statement testing if the learner exceeds a threshold. **/
#define MPI_IF_LEVEL(L, T) ((void)0);
/** Selects second parameter if not MPI. **/
#define MPI_SELECT(M,N) N
/** Defines an empty placeholder for an exit and error message. **/
#define MPI_EXIT_ERROR(out, msg) ((void)0)
/** Defines an empty placeholder for an alteration of the parent level. **/
#define MPI_ALTER_PARENT(L, N, M) ((void)0)
/** Defines an empty placeholder for a method for getting an MPI level name. **/
#define MPI_OPTION(N) 
#endif

#endif


