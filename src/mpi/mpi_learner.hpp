/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mpi_learner.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MPI_LEARNER_HPP
#define _EXEGETE_MPI_LEARNER_HPP
#include "mpi_tree.hpp"
#include "model_evaluation.hpp"
#include "ConcreteExampleSet.h"

/** @file mpi_learner.hpp
 * 
 * @brief Contains the interface to the MPI tree.
 * 
 * This file contains the mpi learner class, an interface to the
 * MPI learner tree.
 *
 * @ingroup ExegeteMPI
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail
	{
		template<class T, class P=typename T::parent_type >
		class select_learner;
	};
	/** @brief Defines the interface to the MPI tree.
	 */
	template<class T, class M>
	class mpi_learner : public mpi_tree
	{
		typedef T 												learner_type;
		typedef typename T::dataset_type						dataset_type;
		typedef typename dataset_type::size_type 				size_type;
		typedef typename dataset_type::attribute_type			descriptor_type;
		typedef typename dataset_type::class_type 				class_type;
		typedef ConcreteExampleSet<descriptor_type,class_type>	concrete_set;
		typedef typename mpi_tree::list_ptr						list_ptr;
		typedef typename list_ptr::iterator 					list_iterator;
		typedef typename list_ptr::const_iterator 				const_list_iterator;
		typedef typename mpi_tree::value_type 					node_pointer;
		//typedef mpi_validate_node<T>							learner_node;
		
	public:
		/** Constructs a MPI learning tree.
		 * 
		 * @param map an argument map.
		 * @param m a measure.
		 */
		mpi_learner(ArgumentMap& map, M& m) : mpi_tree(2), pmeasure(&m)
		{
			map("MPI");
			map(mpi_tree::levelInt, "mpi_level", build_options("MPI distribution level>Validate:1"));
		}
		
	public:
		/** Initalizes the level of the MPI tree.
		 */
		void init()
		{
			mpi_tree::init(detail::select_learner<T>::level);
		}
		/** Called by a worker to repeatedly process learning tasks.
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a dataset for training.
		 * @return an error message or NULL.
		 */
		const char* mpi_train_child(learner_type& learner, concrete_set& dataset)
		{
			typedef std::vector<size_type> index_vector;
			dataset_type trainset(dataset);
			index_vector indices;
			MPI_Status stat;
			size_type head[2], len;
			std::vector<float> args;
			std::string buf;
			const char* msg;

			while(1)
			{
				if( !mpi_tree::recv(head, stat, 2) ) return ERRORMSG("Cannot receive subset header");
				if( head[0] == 0 ) break;
				indices.resize(head[0]);
				if( !mpi_tree::recv(indices, stat) ) return ERRORMSG("Cannot receive subset indices");
				trainset.assignidx(dataset, indices.begin(), indices.end());
				detail::select_learner<T>::select(learner, args, head[1]);
				if( !args.empty() )
				{
					if( !mpi_tree::recv(args, stat) ) return ERRORMSG("Cannot receive args");
					typename std::vector<float>::const_iterator it = args.begin();
					MPI_DEBUG_PRINT("read: " << learner.class_name(), 5);
					detail::select_learner<T>::select_init(learner, args, head[1]);
					//learner.init(it);
				}
				if( (msg=detail::select_learner<T>::train(learner, trainset, buf, head[1])) != 0 ) return msg;
				len = buf.size();
				if( !mpi_tree::send(len) ) return ERRORMSG("Cannot send model size");
				if( !mpi_tree::send(buf) ) return ERRORMSG("Cannot send model");
				if( !args.empty() ) args.clear();
			}
			return 0;
		}
		/** Setup the MPI learner tree by
		 * 	- Sending the full dataset to each worker
		 * 	- Sending learning arguments to each worker
		 * 
		 * Setup the worker by
		 * 	- Receiving the full data from the root
		 * 	- Receiving learning arguments from the root
		 * 
		 * @param learner a learning algorithm.
		 * @param dataset a training set.
		 * @param validation a validation algorithm.
		 * @param seed a reference to a seed.
		 * @return an error message or NULL.
		 */
		template<class V>
		const char* mpi_setup(learner_type& learner, concrete_set& dataset, V& validation, unsigned long& seed)
		{
			typedef typename std::vector<float>::const_iterator const_iterator;
			const char* msg;
			std::vector<float> args;
			learner.init(args);
			args.push_back(float(validation.positive()));
			args.push_back(float(seed));
			if( (msg = mpi_setup(dataset, args)) != 0 ) return msg;
			if( mpi_comm::rank() != 0 )
			{
				const_iterator it = args.begin();
				seed = (unsigned long)args.back();
				if( ((int)args[args.size()-2]) == 1 ) dataset.setPositive();
				learner.init(it);
			}
			return 0;
		}
		/** Setup the MPI learner tree by
		 * 	- Sending the full dataset to each worker
		 * 	- Sending learning arguments to each worker
		 * 
		 * Setup the worker by
		 * 	- Receiving the full data from the root
		 * 	- Receiving learning arguments from the root
		 * 
		 * @param dataset a training set.
		 * @param fltvec a vector of arguments.
		 * @return an error message or NULL.
		 */
		const char* mpi_setup(concrete_set& dataset, std::vector<float>& fltvec)
		{
			const char* msg;
			bool recv=false;
			if( mpi_comm::rank() == 0 && dataset.empty() ) return ERRORMSG("MPI cannot send empty dataset");
			if( (msg=broadcast_arguments(dataset, fltvec)) != 0 ) return msg;
			if( dataset.empty() ) recv=true;
			if( (msg=broadcast_header(dataset, recv)) != 0 ) return msg;
			if( (msg=broadcast_examples(dataset,recv)) != 0 ) return msg;
			if( (msg=broadcast_bags(dataset,recv)) != 0 ) return msg;
			if( recv ) 
			{
				dataset.recalculate();
				MPI_DEBUG_USE( dataset.toString("#DS\t"), 1 );
			}
			return 0;
		}
		
	public:
		/** Send a termination signal to a worker.
		 * 
		 * @param tree an MPI tree.
		 * @param task the worker index to terminate.
		 * @return an error message or NULL.
		 */
		const char* send_node(mpi_tree& tree, int task)
		{
			size_type head[2] = {0, 0};
			if( !mpi_comm::send(task, head, 2) ) return ERRORMSG("Cannot send shutdown signal");
			return 0;
		}
		/** Get the name of the class.
		 * 
		 * @return mpi_learner.
		 */
		virtual const char* name()const
		{
			return "mpi_learner";
		}

	private:
		static std::string build_options(std::string str)
		{
			return str + detail::select_learner<T>::option(2);
		}
		bool is_null()const
		{
			for(const_list_iterator beg = mpi_tree::begin(), end = mpi_tree::end();beg != end;++beg)
			{
				if( *beg == 0 ) return true;
			}
			return false;
		}
		const char* broadcast_arguments(concrete_set& dataset, std::vector<float>& fltvec)
		{
			if( !mpi_comm::broadcast(fltvec) ) return ERRORMSG("Cannot broadcast arguments");
			return 0;
		}
		const char* broadcast_header(concrete_set& dataset, bool recv)
		{
			const unsigned int len = 4;
			unsigned long dim[len], *pdim=dim;
			if( !recv )
			{
				dim[0] = dataset.size();
				dim[1] = dataset.attributeCount();
				dim[2] = dataset.classCount();
				dim[3] = dataset.bagCount();
			}
			if( !mpi_comm::broadcast(pdim, len) ) return ERRORMSG("Cannot broadcast dataset header");
			if( recv )
			{
				dataset.resizeall(dim[0], dim[1], dim[2], dim[3]);
			}
			return 0;
		}
		const char* broadcast_examples(concrete_set& dataset, bool recv)
		{
			typedef typename concrete_set::attribute_pointer attribute_pointer;
			typedef typename concrete_set::iterator iterator;
			typedef typename concrete_set::size_type size_type;
			typedef typename concrete_set::feature_type feature_type;
			typedef typename concrete_set::type_util data_util;
			size_type len = dataset.attributeCount()+1+data_util::IS_SPARSE;
			attribute_pointer attr;
			for(iterator beg = dataset.begin(), end=dataset.end();beg != end;++beg)
			{
				attr = beg->x()-1;
				if( !recv ) 
				{
					data_util::valueOf(*attr) = feature_type(beg->y());
				}
				if( !mpi_comm::broadcast_raw(attr, len) ) return ERRORMSG("Cannot broadcast example");
				if( recv ) 
				{
					beg->y(class_type(data_util::valueOf(*attr)));
				}
				data_util::valueOf(*attr) = feature_type(std::distance(dataset.begin(), beg));
			}
			return 0;
		}
		const char* broadcast_bags(concrete_set& dataset, bool recv)
		{
			typedef typename concrete_set::bag_iterator bag_iterator;
			const unsigned int len = 3;
			unsigned long dim[len], *pdim=dim;
			for(bag_iterator beg = dataset.bag_begin(), end=dataset.bag_end();beg != end;++beg)
			{
				if( !recv )
				{
					dim[0] = beg->y();
					dim[1] = std::distance(dataset.begin(), beg->begin());
					dim[2] = std::distance(dataset.begin(), beg->end());
				}
				if( !mpi_comm::broadcast(pdim, len) ) return ERRORMSG("Cannot broadcast bags");
				if( recv )
				{
					beg->y(dim[0]);
					beg->begin(dataset.begin()+dim[1]);
					beg->end(dataset.begin()+dim[2]);
				}
			}
			return 0;
		}
		
	private:
		M* pmeasure;
	};
	
	namespace detail
	{	
		/** @brief Adapts dataset to learner
		 * 
		 * This class template creates a copy of a dataset in the correct format.
		 */
		template<class T, class D1, class D2=typename T::dataset_type>
		struct learner_adapter
		{
			/** Trains a learner on a copied dataset.
			 * 
			 * @param learner a learner.
			 * @param dataset a training set.
			 */
			static const char* train(T& learner, D1& dataset)
			{
				D2 trainset(dataset); 
				assign(trainset, dataset);
				return learner.train(trainset);
			}
			/** Does nothing.
			 * 
			 * @param to a destination dataset.
			 * @param from a source dataset.
			 */
			template<class X, class Y1, class Y2, class W1, class W2>
			static void assign(ExampleSet<X,Y1,W1>& to, ExampleSet<X,Y2,W2>& from)
			{
			}
			/** Assigns one dataset to another.
			 * 
			 * @param to a destination dataset.
			 * @param from a source dataset.
			 */
			template<class X, class Y, class W1, class W2>
			static void assign(ExampleSet<X,Y,W1>& to, ExampleSet<X,Y,W2>& from)
			{
				to.assign(from);
			}
		};
		/** @brief Dataset matches learner
		 * 
		 * This class template trains the learner over a dataset.
		 */
		template<class T, class D>
		struct learner_adapter<T,D,D>
		{
			/** Trains a learner the given dataset.
			 * 
			 * @param learner a learner.
			 * @param dataset a training set.
			 */
			static const char* train(T& learner, D& dataset)
			{
				return learner.train(dataset);
			}
		};
		/** @brief Select a learner in a hierarchy of learners
		 * 
		 * This class selects a learner in a hierarchy:
		 * 	- trains a learner
		 * 	- loads arguments from a learner
		 * 	- saves arguments to a learner
		 * 	- creates a list of MPI compatible learners
		 */
		template<class T, class P>
		class select_learner
		{
			typedef select_learner< P > 		select_parent;
		public:
			/** Holds the level of the learner in the hierarchy. **/
			enum { level = select_parent::level+1 };
			/** Selects the learner at the specified level, trains it over the 
			 * the given dataset and saves the model to a buffer.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a training set.
			 * @param buf a buffer to save a learned model.
			 * @param l level of the learner in the hierarchy.
			 * @return an error message or NULL.
			 */
			template<class D>
			static const char* train(T& learner, D& dataset, std::string& buf, int l)
			{
				if( l == level ) 
				{
					const char* msg = learner_adapter<T,D>::train(learner, dataset);
					valueToString(learner, buf);
					return msg;
				}
				else return select_parent::train(learner, dataset, buf, l);
			}
			/** Saves arguments into a learner at a specific level in the hierarchy.
			 * 
			 * @param learner a learner.
			 * @param args source arguments.
			 * @param l level of learner to save.
			 */
			static void select_init(T& learner, const std::vector<float>& args, int l)
			{
				if( l == level ) 
				{
					if( T::SELECT == 0 ) 
					{
						typename std::vector<float>::const_iterator it = args.begin();
						learner.init(it, 0);
					}
				}
				else return select_parent::select_init(learner, args, l);
			}
			/** Loads arguments from a learner at a specific level in the hierarchy.
			 * 
			 * @param learner a learner.
			 * @param args destination vector of arguments.
			 * @param l level of learner to save.
			 */
			static void select(T& learner, std::vector<float>& args, int l)
			{
				if( l == level ) 
				{
					if( T::SELECT == 0 ) 
					{
						learner.init(args, 0);
					}
				}
				else return select_parent::select(learner, args, l);
			}
			/** Gets a list of options of MPI compatible learners.
			 * 
			 * @param i start index.
			 * @return list of options.
			 */
			static std::string option(unsigned int i)
			{
				std::string tmp = T::get_mpi_name();
				if( tmp == "" ) return tmp;
				std::string idx, nxt;
				tmp = ";";
				nxt = select_parent::option(i+1);
				valueToString(i,idx);
				tmp += T::get_mpi_name();
				tmp += ":";
				tmp += idx;
				return tmp + nxt;
			}
		};
		/** @brief Select a learner in a hierarchy of learners
		 * 
		 * This class selects a learner in a hierarchy:
		 * 	- trains a learner
		 * 	- loads arguments from a learner
		 * 	- saves arguments to a learner
		 * 	- creates a list of MPI compatible learners
		 */
		template<class T>
		class select_learner<T, void>
		{
		public:
			/** Holds the level of the learner at the top of the hierarchy. **/
			enum { level = 1 };
			/** Selects the learner at the top level, trains it over the 
			 * the given dataset and saves the model to a buffer.
			 * 
			 * @param learner a learning algorithm.
			 * @param dataset a training set.
			 * @param buf a buffer to save a learned model.
			 * @param l level of the learner in the hierarchy.
			 * @return an error message or NULL.
			 */
			template<class D>
			static const char* train(T& learner, D& dataset, std::string& buf, int l)
			{
				const char* msg = learner_adapter<T,D>::train(learner, dataset);
				valueToString(learner, buf);
				return msg;
			}
			/** Loads arguments from a learner at a specific level in the hierarchy.
			 * 
			 * @param learner a learner.
			 * @param args destination vector of arguments.
			 * @param l level of learner to save.
			 */
			static void select(T& learner, std::vector<float>& args, int l)
			{
				if( T::SELECT == 0 ) 
				{
					learner.init(args, 0);
				}
			}
			/** Saves arguments into a learner at a specific level in the hierarchy.
			 * 
			 * @param learner a learner.
			 * @param args source arguments.
			 * @param l level of learner to save.
			 */
			static void select_init(T& learner, const std::vector<float>& args, int l)
			{
				if( T::SELECT == 0 ) 
				{
					typename std::vector<float>::const_iterator it = args.begin();
					learner.init(it, 0);
				}
			}
			/** Gets a list of options of MPI compatible learners.
			 * 
			 * @param i start index.
			 * @return empty string.
			 */
			static std::string option(unsigned int i)
			{
				return "";
			}
		};
	};
};

#endif

