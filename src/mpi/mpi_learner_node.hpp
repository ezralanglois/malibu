/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mpi_learner_node.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MPI_LEARNER_NODE_H
#define _EXEGETE_MPI_LEARNER_NODE_H
#include "mpi_tree.hpp"

/** @file mpi_learner_node.hpp
 * 
 * @brief Contains all concrete tree nodes
 * 
 * This file contains concrete tree nodes including:
 * 	- mpi learner node
 * 	- mpi validate node
 * 	- mpi select node
 * 	- mpi committee node
 * 
 * @todo move concrete nodes to own class
 *
 * @ingroup ExegeteMPI
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	namespace detail
	{
		template<class T, class P=typename T::parent_type >
		class select_learner_node;
	};
	/** @brief Concrete tree node for training a learner
	 * 
	 * This class defines a concrete MPI tree node for training a learner.
	 */
	template<class T>
	class mpi_learner_node : public mpi_tree_node
	{
	protected:
		/** Defines a learner type. **/
		typedef T 									learner_type;
		/** Defines a dataset type. **/
		typedef typename learner_type::dataset_type	dataset_type;
		/** Defines a testset type. **/
		typedef typename learner_type::testset_type	testset_type;
		/** Defines a size type. **/
		typedef typename dataset_type::size_type 	size_type;
		/** Defines an index vector. **/
		typedef std::vector<size_type>				index_vector;
		
	public:
		/** Constructs a MPI learner node to add to an MPI tree.
		 * 
		 * @param d a dataset.
		 * @param l a node level.
		 * @param flag test if node is last.
		 */
		mpi_learner_node(dataset_type& d, int l, bool flag=false) : mpi_tree_node(l,flag), plearn(0), trnset(d), modelLen(0)
		{
			trnset.assign(d);
			ASSERT(trnset.size() > 0);
		}
		/** Constructs a MPI learner node to add to an MPI tree.
		 * 
		 * @param lrn a learner.
		 * @param d a dataset.
		 * @param l a node level.
		 * @param flag test if node is last.
		 */
		mpi_learner_node(learner_type& lrn, dataset_type& d, int l, bool flag=false) : mpi_tree_node(l, flag), plearn(&lrn), trnset(d), modelLen(0)
		{
			lrn.argument().store(carg);
			lrn.init(args,0);
			trnset.assign(d);
			ASSERT(trnset.size() > 0);
		}
		/** Destructs a learner node.
		 */
		virtual ~mpi_learner_node()
		{
		}
		
	public:
		/** Gets the type of the node.
		 * 
		 * @return a learner type.
		 */
		virtual char type()const
		{
			return 'L';
		}
		/** Sends training data to a worker task.
		 * 
		 * @param tree an MPI tree.
		 * @param task a worker task.
		 * @return an error message or NULL.
		 */
		const char* send_node(mpi_tree& tree, int task)
		{
			ASSERT(mpi_tree_node::level() > 0);
			if( mpi_tree_node::level() > 0 ) 
			{
				size_type head[2];
				head[0] = trnset.bagCount()>0?trnset.bagCount():trnset.size();
				ASSERTMSG(head[0] > 0, trnset.size() << " " << T::class_name() );
				head[1] = mpi_tree_node::level();	//detail::select_learner<T>::level;
				if( !mpi_comm::send(task, head, 2) ) return ERRORMSG("Cannot send subset header");
				index_vector indices(head[0]);
				trnset.indices(&(*indices.begin()));
				if( !mpi_comm::send(task, indices) ) return ERRORMSG("Cannot send index vector");
				std::vector<float> tmp;
				detail::select_learner_node<T>::select(*plearn, tmp, mpi_tree_node::level());
				if( !tmp.empty() )
				{
					ASSERTMSG(args.size()>=tmp.size(), args.size() << " >= " << tmp.size());
					if( plearn->isSelectingParameters() ) 
					//need to use saved argument, could have changed before sending
					{
						float* addr=(&(*args.begin()));
						//sends vector will use vector size, uses reference
						if( !mpi_comm::send(task, addr, tmp.size()) ) return ERRORMSG("Cannot send args vector");
					}
					else 
					//cant use saved, too old, need best found durning selection
					{
						if( !mpi_comm::send(task, tmp) ) return ERRORMSG("Cannot send args vector");
					}
				}
				if( !tree.irecv(task, modelLen) ) return ERRORMSG("Cannot post receive model size");
			}
			//trnset.clear(); //- clears the dataset
			return 0;
		}
		/** Receive the data from a worker task.
		 * 
		 * @param task a worker task.
		 * @return an error message or NULL.
		 */
		const char* recv_node(int task)
		{
			ASSERT(mpi_tree_node::level() > 0);
			if( mpi_tree_node::level() > 0 )  
			{
				MPI_Status stat;
				std::string buf;
				buf.resize(modelLen);
				if( !mpi_comm::recv(task, buf, stat, modelLen) ) return ERRORMSG("Cannot receive model");
				if( !detail::select_learner_node<T>::save(*plearn, buf, mpi_tree_node::level()) ) return plearn->errormsg();
			}
			ASSERT(mpi_tree_node::parent()!=0);
			mpi_tree_node::parent()->child_finished(mpi_tree_node::is_last());
			return 0;
		}
		/** Intializes children in a parent before training the parent.
		 * 
		 * @return an error message or NULL.
		 */
		virtual const char* initialize()
		{
			plearn->mpi_finalize();
			return 0;
		}
		/** Process the children before moving to a parent.
		 */
		void process()
		{
			mpi_tree_node::parent()->child_finished(mpi_tree_node::is_last());
			this->finalize();
			plearn->mpi_update(trnset, carg);  //update arguments????
			trnset.clear();
		}
		/** Get the training set for the learning process.
		 * 
		 * @return the training set.
		 */
		dataset_type& trainset()
		{
			return trnset;
		}
		/** Get the name of the class.
		 * 
		 * @return mpi_learner_node.
		 */
		virtual const char* name()const
		{
			return "mpi_learner_node";
		}
		/** Get the learner to be trained in a worker.
		 * 
		 * @return learner.
		 */
		learner_type& learner()
		{
			ASSERT(plearn != 0);
			return *plearn;
		}
		
	protected:
		/** Set the learner for the learning node:
		 * 	- save a pointer
		 * 	- save tunable arguments (in case of parameter tuning)
		 * 	- save all arguments
		 */
		void set_learner(learner_type& l)
		{
			ASSERT(args.empty());
			plearn = &l;
			l.argument().store(carg);
			l.init(args,0);
		}

	protected:
		/** Current tunable arguments. **/
		std::vector<float> carg;
		/** All arguments. **/
		std::vector<float> args;
		/** Pointer to learning algorithm. **/
		learner_type* plearn;
		/** Training set **/
		dataset_type trnset;
		/** Holder of model select. **/
		size_type modelLen;
	};

	template<class T, class M, template<class> class TP>
	class mpi_validate_node;
	/** @brief Concrete tree node for selecting parameters for a learner
	 * 
	 * This class defines a concrete MPI tree node for selecting parameters for a learner
	 */
	template<class T, class M, template<class> class TP>
	class mpi_select_node : public mpi_learner_node< T >
	{
		typedef typename TP<T>::learner_type				parent_learner;
		typedef mpi_validate_node<parent_learner, M, TP>	validate_type;
		typedef mpi_learner_node<T> 						parent_type;
		typedef typename parent_type::learner_type			learner_type;
		typedef typename parent_type::dataset_type			dataset_type;
		typedef typename parent_type::testset_type			testset_type;
		typedef typename parent_type::size_type 			size_type;
		
	public:
		/** Constructs a selection node.
		 * 
		 * @param lrn a learner.
		 * @param d a dataset.
		 * @param l a node level.
		 * @param flag test if node is last.
		 */
		mpi_select_node(learner_type& lrn, dataset_type& d, int l, bool flag=false) : parent_type(lrn, d, l, flag )
		{
		}
		
	public:
		/** Get the type of the node.
		 * 
		 * @return S
		 */
		char type()const
		{
			return 'S';
		}
		/** Saves children for fast tune learners.
		 * 
		 * @return an error message or NULL.
		 */
		const char* finalize()
		{
			const char* msg;
			if( (msg=mpi_tree_node::finalize()) != 0 ) return msg;
			parent_type::learner().resize_saved(mpi_tree_node::size());
			validate_type* parent;
			for(typename mpi_tree_node::iterator beg = mpi_tree_node::begin(), end = mpi_tree_node::end();beg != end;++beg)
			{
				ASSERT((*beg)->type() == 'V');
				ASSERTMSG( dynamic_cast<validate_type*>(*beg) != 0, (*beg)->type() );
				parent = static_cast<validate_type*>(*beg);
				ASSERT(parent->testset().size()>0);
				parent_type::learner().save_next(parent->learner(), parent->testset());
			}
			return 0;
		}
	};

	/** @brief Concrete tree node for training a committee learner
	 * 
	 * This class defines a concrete MPI tree node for training a committee learner.
	 */
	template<class T>
	class mpi_committee_node : public mpi_learner_node< typename T::value_type >
	{
		typedef T committee_type;
		typedef typename T::value_type learner_type;
		typedef detail::select_learner<learner_type> select_learner_type;
		typedef mpi_learner_node< learner_type > node_type;
		typedef typename node_type::dataset_type dataset_type;
		typedef typename dataset_type::size_type 	size_type;
		
	public:
		/** Constructs a committee learning node.
		 * 
		 * @param com a committee learner.
		 * @param trnset a dataset.
		 * @param i index in committee.
		 * @param n number of learners in committee.
		 */
		mpi_committee_node(committee_type& com, dataset_type& trnset, size_type i, size_type n ) : node_type(com, trnset, select_learner_type::level, (i+1)==n ), subsetInt(trnset.size()), indexInt(i), pcomm(&com)
		{
		}
		
	public:
		/** Get the type of the node.
		 * 
		 * @return C
		 */
		char type()const
		{
			return 'C';
		}
		/** Adds this learner to the committee.
		 * 
		 * @return an error message or NULL.
		 */
		const char* finalize_child()
		{
			pcomm->mpi_add_child(indexInt, subsetInt);
			return 0;
		}
		
	private:
		size_type subsetInt;
		size_type indexInt;
		committee_type* pcomm;
	};

	template<class M, template<class> class TP>
	class PartitionHandler;
	/** @brief Concrete tree node for training and validating a learner
	 * 
	 * This class defines a concrete MPI tree node for training and validating a learner.
	 */
	template<class T, class M, template<class> class TP>
	class mpi_validate_node : public mpi_learner_node<T>
	{
	protected:
		/** Defines a learner type. **/
		typedef T 									learner_type;
		/** Defines a dataset type. **/
		typedef typename learner_type::dataset_type	dataset_type;
		/** Defines a testset type. **/
		typedef typename learner_type::testset_type	testset_type;
		/** Defines a size type. **/
		typedef typename dataset_type::size_type 	size_type;
		
	public:
		/** Constructs a validation node.
		 * 
		 * @param lrn a learner.
		 * @param d a dataset.
		 * @param t a testset.
		 * @param ph a partition handler.
		 * @param m a measure.
		 * @param r current run.
		 * @param f current fold.
		 * @param islst flag as last.
		 * @param l current level.
		 */
		mpi_validate_node(learner_type& lrn, dataset_type& d, testset_type& t, PartitionHandler<M,TP>& ph, M& m, unsigned int r, unsigned int f, bool islst, int l) : mpi_learner_node<T>(d, l, islst), tstset(t), ppartition(&ph), pmeasure(&m), runs(r), fold(f)
		{
			tstset.assign(t);
			learn.set_mpi_tree(lrn.get_mpi_tree());
			static_learner_vistor<T>::argument_copy(learn, lrn);
			mpi_learner_node<T>::set_learner(learn);
			//
			ins_cnt = m.instance_count();
			bag_cnt = m.bag_count();
			cls_cnt = m.class_count();
			run_cnt = m.run_count();
		}
		
	public:
		/** Gets the testset.
		 * 
		 * @return a testset.
		 */
		testset_type& testset()
		{
			return tstset;
		}
		/** Gets the name of the class.
		 * 
		 * @return mpi_validate_node.
		 */
		virtual const char* name()const
		{
			return "mpi_validate_node";
		}
		/** Gets the learner.
		 * 
		 * @return learner.
		 */
		learner_type& learner()
		{
			return learn;
		}
		/** Validate learner on testset to some measure or set of predictions.
		 * 
		 * @return an error message or NULL.
		 */
		const char* finalize_child()
		{
			const char* msg;
			bool isnext=true;
			if( mpi_tree_node::test_first() )
			{
				pmeasure->resize(ins_cnt, bag_cnt, run_cnt, cls_cnt);
			}
			if( (msg=ppartition->mpi_evaluate(learn, mpi_learner_node<T>::trainset(), tstset, *pmeasure, runs, fold)) != 0 ) return msg;
			if( fold == UINT_MAX || (fold == 0 && !mpi_tree_node::test_first()) ) 
			{
				isnext=false;
				pmeasure->next_run();
			}
			if( mpi_tree_node::test_last() )
			{
				if(isnext)pmeasure->next_run();
				pmeasure->finalize();
			}
			return 0;
		}
		/** Get type of node.
		 * 
		 * @return V
		 */
		char type()const
		{
			return 'V';
		}
		
	private:
		learner_type learn;
		testset_type tstset;
	private:
		PartitionHandler<M,TP>* ppartition;
		M* pmeasure;
		unsigned int runs;
		unsigned int fold;
	private:
		size_type ins_cnt;
		size_type bag_cnt;
		size_type cls_cnt;
		size_type run_cnt;
	};
	
	namespace detail
	{
		/** @brief Processes a learner in hierarchy of learners
		 * 
		 * This class processes a learner in hierarchy of learners by:
		 * 	- saving it parameters in a vector
		 * 	- saving it model in a string
		 */
		template<class T, class P>
		class select_learner_node
		{
			typedef select_learner_node< P > select_parent;
		public:
			/** Holds the level in the hierarchy. **/
			enum { level = select_parent::level+1 };
			/** Store arguments of select learner in a vector.
			 * 
			 * @param learner current learner in hierarchy.
			 * @param args destination for arguments.
			 * @param l specified level.
			 */
			static void select(T& learner, std::vector<float>& args, int l)
			{
				if( l == level ) 
				{
					if( T::SELECT == 0 ) 
					{
						learner.init(args, 0);
					}
				}
				else return select_parent::select(learner, args, l);
			}
			/** Store model of select learner in a string.
			 * 
			 * @param learner current learner in hierarchy.
			 * @param buf string buffer for model.
			 * @param l specified level.
			 * @return true if successful
			 */
			static bool save(T& learner, std::string& buf, int l)
			{
				if( l == level ) 
				{
					return stringToValue(buf, learner);
				}
				else return select_parent::save(learner, buf, l);
			}
		};
		/** @brief Processes a learner at root of a hierarchy of learners
		 * 
		 * This class processes a learner at root of a hierarchy of learners by:
		 * 	- saving it parameters in a vector
		 * 	- saving it model in a string
		 */
		template<class T>
		class select_learner_node<T, void>
		{
		public:
			/** Holds the level in the hierarchy. **/
			enum { level = 1 };
			/** Store arguments of select learner in a vector.
			 * 
			 * @param learner current learner in hierarchy.
			 * @param args destination for arguments.
			 * @param l specified level.
			 */
			static void select(T& learner, std::vector<float>& args, int l)
			{
				if( T::SELECT == 0 ) 
				{
					learner.init(args, 0);
				}
			}
			/** Store model of select learner in a string.
			 * 
			 * @param learner current learner in hierarchy.
			 * @param buf string buffer for model.
			 * @param l specified level.
			 * @return true if successful
			 */
			static bool save(T& learner, std::string& buf, int l)
			{
				return stringToValue(buf, learner);
			}
		};
	};
};

#endif


