/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mpi_tree.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MPI_TREE_H
#define _EXEGETE_MPI_TREE_H
#include "mpi_tree_node.hpp"
#include "mpi_comm.hpp"
/** @file mpi_tree.hpp
 * 
 * @brief An MPI tree interface
 * 
 * This file contains the mpi tree class.
 * 
 * @todo restart file
 *     	1. config file
 * 		2. dataset shuffle indices
 * 		3. model / param + metric / ?
 * 
 * @todo Advanced restart
 * 		1. Child check point send back
 * 
 * @todo
 *     	1. delete unused nodes
 *     	2. several files? finished file deleted each level
 *     	3. indices of datasets, models, arguments
 * 
 *
 * @ingroup ExegeteMPI
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an MPI tree
	 * 
	 * This class defines an interface to an MPI tree.
	 */
	class mpi_tree : public mpi_tree_node
	{
	public:
		/** Defines a value type. **/
		typedef mpi_tree_node 			value_type;
		/** Defines a pointer. **/
		typedef mpi_tree_node* 			pointer;
		/** Defines a constant pointer. **/
		typedef const mpi_tree_node* 	const_pointer;
		/** Defines a reference. **/
		typedef mpi_tree_node& 			reference;
		/** Defines a constant reference. **/
		typedef const mpi_tree_node& 	const_reference;
		/** Defines a size type. **/
		typedef size_t 					size_type;
	private:
		typedef std::vector<MPI_Request> vector_req;
		typedef std::vector<int>		vector_int;
		typedef std::vector<pointer> 	vector_ptr;
		
	public:
		/** Constructs an MPI tree.
		 * 
		 * @param d the level of the tree distrubtion.
		 */
		mpi_tree(int d) : mpi_tree_node(d), taskIdx(1), taskCnt(1), taskTot(0), rankIdx(0), errmsg(0)
		{
		}
		/** Constructs an MPI tree.
		 * 
		 * @param l number of children.
		 * @param d the level of the tree distrubtion.
		 */
		mpi_tree(int l, int d) : mpi_tree_node(d), taskIdx(1), taskCnt(1), taskTot(0), rankIdx(0), errmsg(0)
		{
			taskTot = mpi_comm::size();
			rankIdx = mpi_comm::rank();
			if( rankIdx > 0 ) taskIdx = 0;
			waitVec.resize(taskTot, MPI_REQUEST_NULL);
			taskVec.resize(taskTot, 0);
			parentVec.resize(l+1, 0);
			parentVec[1] = this;
			taskTot--;
		}
		/** Destructs an MPI tree.
		 */
		virtual ~mpi_tree()
		{
		}
		
	public:
		/** Initalizes an MPI tree.
		 * 
		 * @param l number of children.
		 */
		void init(int l)
		{
			taskTot = mpi_comm::size();
			rankIdx = mpi_comm::rank();
			if( rankIdx > 0 ) taskIdx = 0;
			waitVec.resize(taskTot, MPI_REQUEST_NULL);
			taskVec.resize(taskTot, 0);
			parentVec.resize(l+1, 0);
			parentVec[1] = this;
			taskTot--;
		}
		/** Flag level entry node as last.
		 * 
		 * @param lev the level to flag.
		 */
		void flag_last(int lev)
		{
			ASSERT( lev < int(parentVec.size()) );
			lev = parentVec.size() - lev;
			ASSERT( (lev+1) < int(parentVec.size()) );
			parentVec[ lev+1 ]->is_last(true);
		}
		/** Post a process to the next available worker or saves to the
		 * tree for later posting.
		 * 
		 * @param node a node containing a process.
		 * @return an error message or NULL.
		 */
		const char* mpi_post(pointer node)
		{
			const char* msg;
			ASSERTMSG( node->level() < int(parentVec.size()), " " << node->level() << " < " << int(parentVec.size()) );
			int lev = parentVec.size() - node->level();
			ASSERTMSG( lev <= mpi_tree_node::level(), lev << " <= " << mpi_tree_node::level() );
			parentVec[ lev ]->add(node);
			if( (lev+1) < int(parentVec.size()) )
			{
				ASSERTMSG( (lev+1) < int(parentVec.size()), (lev+1) << " " << int(parentVec.size()) );
				parentVec[ lev+1 ] = node;
			}
			if( mpi_tree_node::level() == lev )
			{
				taskVec[taskIdx]=node;
				if( (msg=node->send_node(*this, taskIdx)) != 0 ) return msg;
				int curr=1;
				while(true)
				{
					curr = mpi_wait();
					if( curr == 0 ) break;
					if( curr < 0 ) return ERRORMSG("Failed to post");
					if( mpi_post_parent() ) break;
					if( mpi_tree_node::errmsg != 0 ) return mpi_tree_node::errmsg;
				}
			}
			return 0;
		}
		/** Finalize the MPI tree processing by finishing up each child.
		 * 
		 * @return an error message or NULL.
		 */
		const char* finalize(bool f=true)
		{
			if(!f) return 0;
			const char* msg;
			//if( is_done() ) return 0;// -----------------------------------------------Correct?
			if( (msg=mpi_shutdown()) != 0 ) return msg;
			while( !is_done() ) 
			{
				if( !wait() ) return ERRORMSG("Failed to finalize MPI");
				if( mpi_post_parent() )
				{
					if( (msg=mpi_shutdown()) != 0 ) return msg;
				}
				if( mpi_tree_node::errmsg != 0 ) return mpi_tree_node::errmsg;
			}
			return mpi_tree_node::finalize();
		}
		/** Check if MPI tree is done processing jobs.
		 * 
		 * @return true if zero tasks left.
		 */
		bool is_done()const
		{
			return taskCnt == 0;
		}
		/** Test if current worker is root.
		 * 
		 * @return true rank is zero.
		 */
		bool is_root()const
		{
			return rankIdx == 0;
		}
		/** Get the current task index.
		 * 
		 * @return current task index.
		 */
		int task()const
		{
			return taskIdx;
		}
		/** Get the name of the tree.
		 * 
		 * @return mpi_tree.
		 */
		virtual const char* name()const
		{
			return "mpi_tree";
		}
		/** Alters the level of the tree.
		 * 
		 * @note does nothing.
		 * 
		 * @param l current level.
		 */
		void alter_level(int l)
		{
		}
		/** Get the processing level.
		 * 
		 * @return processing level.
		 */
		int level()const
		{
			return parentVec.size() - mpi_tree_node::level();
		}
		
	protected:
		/** Tests if the parent of the current finished node has any more
		 * unfinished children.
		 * 
		 * @return true if parent has unfinished nodes.
		 */
		bool mpi_post_parent()
		{
			pointer curr_node;
			ASSERTMSG(taskIdx<taskVec.size(), taskIdx << " < " << taskVec.size());
			curr_node = taskVec[taskIdx];
			if( (mpi_tree_node::errmsg=curr_node->recv_node(taskIdx)) != 0 ) return false;
			curr_node->finalize();
			curr_node = curr_node->parent();
			while( curr_node->is_ready() )
			{
				if( curr_node->level() > 0 )
				{
					curr_node->initialize();
					taskVec[taskIdx]=curr_node;
					mpi_tree_node::errmsg=curr_node->send_node(*this, taskIdx);
					return false;
				}
				else 
				{
					curr_node->process();
					curr_node = curr_node->parent();
					if( curr_node == 0 ) break;
				}
			}
			return true;
		}
		/** Shutdown a child process.
		 * 
		 * @return an error message or NULL.
		 */
		const char* mpi_shutdown()
		{
			const char* msg;
			ASSERTMSG(taskIdx<=taskTot, taskIdx << " < " << taskTot);
			if( (msg = send_node(*this, taskIdx)) != 0 ) return msg;
			taskCnt--;
			return 0;
		}
		/** Tests whether the tree should wait or distribute a process to a worker.
		 * 
		 * @return current task index or zero.
		 */
		int mpi_wait()
		{
			if( taskCnt == taskTot )
			{
				if( !wait() ) return -1;
				return taskIdx;
			}
			else
			{
				taskIdx++;
				taskCnt++;
				return 0;
			}
		}
		
	public:
		/** Send data to current worker.
		 * 
		 * @param val data object to send.
		 * @param n number of data elements.
		 * @return true if success.
		 */
		template<class T>
		bool send(T& val, unsigned int n=1)
		{
			return mpi_comm::send(taskIdx, val, n);
		}
		/** Receive data from current task.
		 * 
		 * @param val destination object for data.
		 * @param stat receive status.
		 * @param n number of data elements.
		 * @return true if success.
		 */
		template<class T>
		bool recv(T& val, MPI_Status& stat, unsigned int n=1)
		{
			return mpi_comm::recv(taskIdx, val, stat, n);
		}
		/** Receive data to some buffer without blocking the distribution tree.
		 * 
		 * @param t task worker from which to receive.
		 * @param p destination object.
		 * @param n number of data elements.
		 * @param d type of data to receive.
		 * @return true if success.
		 */
		template<class T>
		bool irecv(unsigned int t, T& p, unsigned int n=1, MPI_Datatype d=detail::mpi_ptr_util<T>::MPI_TYPE())
		{
			return mpi_comm::irecv(t, p, &(*waitVec.begin()), n, d);
		}
		/** Wait for non-blocking receive to complete.
		 * 
		 * @return true if success.
		 */
		bool wait()
		{
			if(!mpi_comm::wait((int)waitVec.size(), &(*waitVec.begin()), taskIdx)) return false;
			ASSERTMSG(0 < taskIdx && taskIdx <= taskTot, taskIdx);
			return true;
		}
		
	public:
		/** Receive data from some task.
		 * 
		 * @param task a worker index.
		 * return an error message or NULL.
		 */
		const char* recv_node(int task)
		{
			return 0;
		}
		/** Tests if tree is ready.
		 * 
		 * @return false.
		 */
		bool is_ready()const
		{
			return false;
		}
		/** Get the current parent level node.
		 * 
		 * @param n parent index.
		 * @return parent node.
		 */
		pointer parent(unsigned int n)
		{
			n = parentVec.size()-n + 1;
			ASSERT(n < parentVec.size());
			ASSERT( parentVec[n] != 0 );
			return parentVec[n];
		}
		
	private:
		int taskIdx;
		int taskCnt;
		int taskTot;
		int rankIdx;
		vector_req waitVec;
		vector_ptr taskVec;
		vector_ptr parentVec;
		const char* errmsg;
	};
};

#endif


