/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mpi_tree_node.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MPI_TREE_NODE_H
#define _EXEGETE_MPI_TREE_NODE_H
#include <list>

/** @file mpi_tree_node.hpp
 * 
 * @brief Contains node in the MPI tree
 * 
 * This file contains the mpi tree node class.
 *
 * @ingroup ExegeteMPI
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	class mpi_tree;
	/** @brief A node in the MPI tree
	 * 
	 * This class defines a node in the MPI tree.
	 */
	class mpi_tree_node : public std::list< mpi_tree_node* >
	{
	public:
		/** Defines a list of child nodes. **/
		typedef std::list< mpi_tree_node* > list_ptr;
		/** Defines an iterator to a list of child nodes. **/
		typedef list_ptr::iterator iterator;
		/** Defines a value type. **/
		typedef mpi_tree_node* value_type;
		
	public:
		/** Constructs an MPI tree node.
		 * 
		 * @param l the level of the node.
		 * @param f flags nodes if it is last.
		 */
		mpi_tree_node(int l, bool f=false) : islast(f), isdone(false), donecnt(0), parentPtr(0), levelInt(l)
		{
		}
		/** Destructs an MPI tree node deallocating all children.
		 */
		virtual ~mpi_tree_node()
		{
			for(iterator beg = list_ptr::begin(), end = list_ptr::end();beg != end;++beg) delete *beg;
		}
		
	public:
		/** Adds a child node to the tree.
		 * 
		 * @note The parent of the child is also set.
		 * 
		 * @param val a child node.
		 */
		void add(value_type val)
		{
			list_ptr::push_back(val);
			val->parent(this);
		}
		/** Sets the parent of the child node.
		 * 
		 * @param p a parent node.
		 */
		void parent(value_type p)
		{
			parentPtr = p;
		}
		/** Gets the parent node.
		 * 
		 * @return parent node.
		 */
		value_type parent()
		{
			return parentPtr;
		}
		/** Gets the level of the node.
		 * 
		 * @return level of the node.
		 */
		int level()const
		{
			return levelInt;
		}
		/** Sets the level of the node.
		 * 
		 * @param l a level.
		 */
		void level(int l)
		{
			levelInt = l;
		}
		/** Change the level of node.
		 * 
		 * @param l a level.
		 */
		void alter_level(int l)
		{
			levelInt = l;
		}
		/** Flags the node as finished and increments the number of finished.
		 * 
		 * @param i a flag indicating it is finished.
		 */
		void child_finished(bool i)
		{
			if(i) isdone = true;
			donecnt++;
		}
		/** Test whether the node is last.
		 * 
		 * @return true if node is last.
		 */
		bool is_last()const
		{
			return islast;
		}
		/** Flags the node as last.
		 * 
		 * @param l a boolean flag.
		 */
		void is_last(bool l)
		{
			islast=l;
		}
		
	protected:
		/** Test if node is the first child.
		 * 
		 * @return true if first child.
		 */
		bool test_first()const
		{
			ASSERT(parentPtr != 0);
			return parentPtr->test_first(this);
		}
		/** Test if node is the last child.
		 * 
		 * @return true if last child.
		 */
		bool test_last()const
		{
			ASSERT(parentPtr != 0);
			return parentPtr->test_last(this);
		}
		/** Test if child node is the first child.
		 * 
		 * @param pn child node.
		 * @return true if first child.
		 */
		bool test_first(const mpi_tree_node* pn)const
		{
			return pn == list_ptr::front();
		}
		/** Test if child node is the last child.
		 * 
		 * @param pn child node.
		 * @return true if last child.
		 */
		bool test_last(const mpi_tree_node* pn)const
		{
			return pn == list_ptr::back();
		}
		
	public:
		/** Test if node is ready for postprocessing: all children have finished.
		 * 
		 * @return true if all children are finished.
		 */
		virtual bool is_ready()const
		{
			return isdone && donecnt == list_ptr::size();
		}
		/** Get type of node.
		 * 
		 * @return type of node.
		 */
		virtual char type()const
		{
			return 0;
		}
		/** Process finished nodes.
		 * 
		 * @note does nothing, must be implemented in subclass
		 */
		virtual void process()
		{
		}
		/** Finalize this child node.
		 * 
		 * @return an error message or NULL.
		 */
		virtual const char* finalize_child()
		{
			return 0;
		}
		/** Finalize all child nodes.
		 * 
		 * @return an error message or NULL.
		 */
		virtual const char* finalize()
		{
			const char* msg;
			for(iterator beg = list_ptr::begin(), end = list_ptr::end();beg != end;++beg)
			{
				if( (msg=(*beg)->finalize_child()) != 0 ) return msg;
			}
			return 0;
		}
		/** Finalize this node.
		 * 
		 * @return an error message or NULL.
		 */
		virtual const char* initialize()
		{
			return 0;
		}
		/** Get name of this node type.
		 * 
		 * @return mpi_tree_node
		 */
		virtual const char* name()const
		{
			return "mpi_tree_node";
		}
		/** Send the data in this node to a child process.
		 * 
		 * @param tree the tree containing this node.
		 * @param task an available child process.
		 * @return an error message or NULL.
		 */
		virtual const char* send_node(mpi_tree& tree, int task)=0;
		/** Receive the data to this node from a child process.
		 * 
		 * @param task the child process sending the data.
		 * @return an error message or NULL.
		 */
		virtual const char* recv_node(int task)=0;
		
	private:
		bool islast;
		bool isdone;
		unsigned int donecnt;
		value_type parentPtr;
		
	protected:
		/** Holds the level of the tree node. **/
		int levelInt;
		/** Holds an error message for the tree node. **/
		const char* errmsg;
	};
};

#endif


