/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * http_post.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_HTTP_POST_HPP
#define _EXEGETE_HTTP_POST_HPP
#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include "stringutil.h"

/** @file http_post.hpp
 * 
 * @brief Posts data to a CGI web server
 * 
 * This file contains the http_post class.
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Posts data to a CGI web server
	 * 
	 * This class defines a method to post data to a CGI webserver.
	 */
	class http_post
	{
	public:
		/** Posts a string buffer data to a CGI webserver at the specified URL.
		 * 
		 * @param url an address of a webserver.
		 * @param buf a string buffer data.
		 * @param out an output stream to write a reply.
		 * @return an error message or NULL.
		 */
		static const char* post(std::string url, const std::string& buf, std::ostream& out)
		{
			using boost::asio::ip::tcp;
			std::string script;
			std::string user;
			std::string host=url;
			
			split_first(host, user, host, '@');
			split_first(host, host, script, ':');
			
			if(host.empty()) return ERRORMSG("Host empty");
			if(script.empty()) return ERRORMSG("Script empty");
			
			boost::asio::io_service io_service;
	
		    // Get a list of endpoints corresponding to the server name.
		    tcp::resolver resolver(io_service);
		    tcp::resolver::query query(host, "http");
		    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		    tcp::resolver::iterator end;
	
		    // Try each endpoint until we successfully establish a connection.
		    tcp::socket socket(io_service);
		    boost::system::error_code error = boost::asio::error::host_not_found;
		    while (error && endpoint_iterator != end)
		    {
		    	socket.close();
		    	socket.connect(*endpoint_iterator++, error);
		    }
		    if (error) return ERRORMSG("Host not found");
		    
		    boost::asio::streambuf request;
		    std::ostream request_stream(&request);
		    request_stream << "POST " << script << " HTTP/1.0\r\n";
		    request_stream << "Accept: */*\r\n";
		    request_stream << "Host: " << host << "\r\n";
		    if(!user.empty()) request_stream << "From: " << user << "\r\n";
		    request_stream << "Content-Type: text/plain\r\n";
		    request_stream << "Content-Length: " << buf.length() << "\r\n";
		    request_stream << "Connection: close\r\n\r\n";
		    request_stream << buf << "\r\n";
		    
		    //post data
		    boost::asio::write(socket, request);
		    
		    // Read the response status line.
	        boost::asio::streambuf response;
	        boost::asio::read_until(socket, response, "\r\n");

	        // Check that response is OK.
	        std::istream response_stream(&response);
	        std::string http_version;
	        response_stream >> http_version;
	        unsigned int status_code;
	        response_stream >> status_code;
	        std::string status_message;
	        std::getline(response_stream, status_message);
	        if (!response_stream || http_version.substr(0, 5) != "HTTP/") return ERRORMSG("Invalid response");
	        if (status_code != 200) return ERRORMSG("Response returned with status code " << status_code );
		    
	        // Read the response headers, which are terminated by a blank line.
	        boost::asio::read_until(socket, response, "\r\n\r\n");

	        // Process the response headers.
	        std::string header;
	        while (std::getline(response_stream, header) && header != "\r")
	          out << header << "\n";
	        out << "\n";

	        // Write whatever content we already have to output.
	        if (response.size() > 0)
	          std::cout << &response;

	        // Read until EOF, writing data to output as we go.
	        while (boost::asio::read(socket, response,
	              boost::asio::transfer_at_least(1), error))
	        	out << &response;
	        if (error != boost::asio::error::eof) return ERRORMSG("Unexpected end of file");
	        
		    return 0;
		}
	};
};

#endif /*HTTP_POST_HPP_*/
