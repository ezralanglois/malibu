/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * standard_learner.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois 
 */
#include "exegete.h"
//#ifdef _STANDARD_LEARNER
#define _STANDARD_VALIDATION
#include "exegete_ml.h"
#include "ExampleFormat.h"
#include "ModelValidation.h"
#include "ModelSelection.h"
#include "prediction_metric.hpp"
#include "prediction_record.hpp"

#include "fileutil.h"
#include "fileioutil.h"
#include "NameVectorVisitor.h"
#include "NameLevelVisitor.h"

#ifdef _MALIBU_POST
#include "http_post.hpp"
#endif

/** @file standard_learner.cpp
 * @brief Main driver for every learning algorithm.
 * 
 * This source file contains the main function as well as several
 * support functions for writing argument groups.
 *
 * @ingroup ExegeteProgram
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @defgroup ExegeteProgram Executable and Library drivers
 *  This group holds all the drivers for the malibu programs.
 */

typedef ::exegete::ArgumentMap										argument_map;
typedef Learner::dataset_type										learnset_type;
typedef Learner::float_type											float_type;
typedef learnset_type::attribute_type								attribute_type;
typedef learnset_type::class_type									class_type;
typedef ::exegete::ExampleLabel<class_type, std::string>			label_type;
typedef Learner::prediction_type									prediction_value;
typedef ::exegete::prediction_record<prediction_value, label_type>	prediction_type;
typedef ::exegete::prediction_metric<prediction_value, class_type>	metric_type;
typedef ::exegete::ModelSelection<Learner, metric_type>				learner_type;
typedef ::exegete::ModelValidation<learner_type, prediction_type>	validation_type;
typedef validation_type::dataset_handler							reader_type;
typedef reader_type::dataset_type									dataset_type;
typedef ::exegete::NameVectorVisitor								name_vector;
typedef ::exegete::NameLevelVisitor									name_level;
typedef ::exegete::NameStringVisitor								name_string;
typedef ::exegete::FullNameVisitor									full_name_str;
typedef ::exegete::static_learner_vistor<Learner>					learner_vistor;

/** @page args Program Arguments
 *
 * @section arglist Standard Learner
 * 	- train
 * 		- Description: training file name 
 * 		- Type: string filename
 * 		- Viewability: Standard
 * 	- seed
 * 		- Description: seed general random number generator
 * 		- Type: long integer
 * 		- Viewability: Advanced
 * 	- config
 * 		- Description: list of configuration files
 * 		- Type: list of strings
 * 		- Viewability: Advanced
 * 		- Master argument
 */

/** This function writes out a partial configuration file.
 * 
 * @todo tests: dataset format, sparse, validation
 * 
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @return an error code pass to the operating system.
 */
int config_type(int argc, char **argv);
/** The main driver function for a standard learner.
 * 
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @return an error code pass to the operating system.
 */
int main(int argc, char **argv)
{
	const char* msg;
	argument_map map; 
	std::string trainfile;
	std::string mapfile=newfilename("", argv[0], ".cfg");
	std::vector<std::string> mapfiles;
	dataset_type trainset;
	prediction_type predictions;
	unsigned long seedInt = 0L;
	full_name_str full_name;

	if( config_type(argc, argv) != 0 ) return 1;
	//if( (msg = MPI_INIT(argc, argv)) != 0 ) return error(map, msg, std::cout);

	header(map, learner_vistor::name());//learner_type::name());
	//Learning Arguments
	learner_type learner(map);//build
	
	MPI_LEARNER(learner_type, prediction_type, mpitree, map, predictions);
	MPI_SET_LEARNER(learner, mpitree);

	//Experiment Arguments
	validation_type validation(map);
	name_string learn_names;
	learner.accept(learn_names);
	validation.algoname(learn_names);
	map.init(map);
	//Dataset Arguments
	map("Dataset");
	map(trainfile, "train", "training file name");
	map(seedInt,   "seed", 	"seed single random number generator", argument_map::ADVANCED);
	//Format Arguments
	reader_type reader(map, "#LS\t", argument_map::ADDITIONAL);
	//ArgumentMap Arguments
	map(mapfiles, "config", "list of configuration files", argument_map::ADVANCED, true);

	if( (msg = MPI_INIT(argc, argv)) != 0 ) return error(map, msg, std::cout);
	
MPI_IF_ROOT
MPI_IF_ELSE
#ifdef _MPI
	map.level(0);
#endif
MPI_IF_END

	MPI_INIT_TREE(mpitree);
	
MPI_IF_ROOT
	learner.accept(full_name);
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg, std::cout);
	name_vector names;
	learner.accept(names); names.push_back(base_name(argv[0]));
	if( (msg=map.parse_implicit(names, "cfg", "cfg", ".exegete.cfg")) != 0 ) return error(map, msg, std::cout);
	if( (msg=map.parse(mapfiles, "cfg", "cfg")) != 0 ) return error(map, msg, std::cout);
#ifndef _MPI
	if( (msg=map.parse()) != 0 ) return error(map, msg, std::cout);
#endif
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg, std::cout);
MPI_IF_END

	reader.add_search_path(map.implicit_path());
	validation.add_search_path(map.implicit_path());
	if( ( !trainfile.empty() && MPI_IS_ROOT(true) ) && (msg=reader.read(learner, trainset, trainfile)) != 0 ) return error(map, msg, std::cout);
	if( (msg = MPI_SETUP(mpitree, learner, trainset, validation, seedInt)) != 0 ) return error(map, msg, std::cout);
	if( seedInt != 0L ) seed_random(seedInt); //@todo remove?
MPI_IF_ROOT
	std::string modelfile = trainfile.empty() ? validation.model() : trainfile;
	if( validation.restart() && validation.set_restart(learner) ) return error(map, msg, std::cout);
	predictions.start_timer();
	if( (msg=validation.validate(learner, predictions, trainset)) != 0 ) return error(map, msg, std::cout);
	if( (msg=MPI_FINALIZE(mpitree, validation)) != 0 ) return error(map, msg, std::cout);
	predictions.stop_timer();
	if( !predictions.empty() ) std::cout << predictions(full_name, validation.toString(), base_name(modelfile.c_str()), "VA") << std::endl;
MPI_IF_ELSE
	if( (msg=MPI_CHILD_TRAIN(mpitree, learner, trainset)) != 0 ) return error(map, msg, std::cout);
MPI_IF_END
	learner_type::cleanall();
	
MPI_IF_ROOT
	map.level(5);
MPI_IF_END
	
#ifdef _MALIBU_POST
#endif
	//Dataset Statistics
	//Predictions
	//ArgumentMap
	return writemap(map, std::cout);
}

/** Gets the level of a learner given a name.
 * 
 * @param name a learning algorithm name.
 * @return corresponding level.
 */
int levelof(const std::string& name)
{
	learner_type learner;
	
	int t = -1;
	if( name == "learning" ) t = 0;
	else if(!name.empty()) 
	{
		name_level level(name);
		learner.accept(level);
		t = level;
	}
	return t;
}
/** Write a partial configuration file for a learning algorithm to an output stream.
 * 
 * @param map an argument map.
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @param out an output stream.
 * @param t selected algorithm level.
 * @return an error code.
 */
int learner_config(argument_map& map, int argc, char **argv, std::ostream& out, int t)
{
	const char* msg;
	learner_type learner(map, t);
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg);
	if( (msg=map.parse()) != 0 ) return error(map, msg);
	return error(map, "Writing learning config", out);
}

/** Write a partial configuration file for validation to an output stream.
 * 
 * @param map an argument map.
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @param out an output stream.
 * @return an error code.
 */
int validation_config(argument_map& map, int argc, char **argv, std::ostream& out)
{
	const char* msg;
	//int selfInt=1;
	validation_type validation(map);
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg);
	if( (msg=map.parse()) != 0 ) return error(map, msg);
	return error(map, "Writing validation config", out);
}

/** Write a partial configuration file for an experiment (validation and dataset) to an output stream.
 * 
 * @param map an argument map.
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @param out an output stream.
 * @return an error code.
 */
int experiment_config(argument_map& map, int argc, char **argv, std::ostream& out)
{
	const char* msg;
	//int selfInt=1;
	std::string trainfile;
	//Dataset Arguments
	map("Input");
	map(trainfile,		"train", "training file name");

	//Format Arguments
	reader_type reader(map, "#LS\t");
	validation_type validation(map);
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg);
	if( (msg=map.parse()) != 0 ) return error(map, msg);
	return error(map, "Writing experiment config", out);
}

/** Write a partial configuration file for a dataset to an output stream.
 * 
 * @param map an argument map.
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @param out an output stream.
 * @return an error code.
 */
int dataset_config(argument_map& map, int argc, char **argv, std::ostream& out)
{
	const char* msg;
	std::string trainfile;
	//Dataset Arguments
	map("Input");
	map(trainfile,		"train", "training file name");

	//Format Arguments
	reader_type reader(map, "#LS\t");
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg);
	if( (msg=map.parse()) != 0 ) return error(map, msg);
	return error(map, "Writing dataset config", out);
}

/** Tests if partial configuration file name is valid.
 * 
 * @param file name of partial configuration file.
 * @return true if valid.
 */
bool isvalid(const std::string& file)
{
	return file == "validation" || 
	   	   file == "experiment" || 
	   	   file == "dataset" ||
	   	   levelof(file) != -1;
}

int config_type(int argc, char **argv)
{
	const char* msg;
	argument_map map;
	std::string configtype;
	map(configtype, "iconfig", "type of configuration file", argument_map::ADVANCED, true);
	if( (msg=map.parse(argc, argv)) != 0 ) 
	{
		return 0;
	}
	if( configtype.empty() ) return 0;
	if( configtype[0] == '@' )
	{
		configtype=configtype.substr(1);
		if( configtype == "validation" )	return validation_config(map, argc, argv, std::cout);
		if( configtype == "experiment" )	return experiment_config(map, argc, argv, std::cout);
		if( configtype == "dataset" )		return dataset_config(map, argc, argv, std::cout);
		return learner_config(map, argc, argv, std::cout, levelof(configtype));
	}
	else if( configtype[0] == '+' && isvalid(configtype.substr(1)) )
	{
		int t;
		const char* msg;
		configtype=configtype.substr(1);
		std::ofstream fout;
		std::string filename;
		if( configtype == "learning" ) filename = newfilename("cfg", argv[0], "cfg");
		else filename = newfilename("cfg", configtype.c_str(), "cfg");
		if( (msg=openfile(fout, filename)) != 0 ) return error(map, msg);
		if( configtype == "validation" ) t = validation_config(map, argc, argv, fout);
		else if( configtype == "experiment" ) t = experiment_config(map, argc, argv, fout);
		else if( configtype == "dataset" ) t = dataset_config(map, argc, argv, fout);
		else t = learner_config(map, argc, argv, fout, levelof(configtype));
		fout.close();
		return t;
	}
	return 0;
}


//#endif


