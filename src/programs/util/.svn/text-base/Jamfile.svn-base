# malibu program utility jamfile
# 
# Copyright Robert Langlois 2006-2008
#
# Distributed under the Malibu Software License, Version 1.0.
#

project util
    : requirements
      <include>$(MALIBU_ROOT)/.
      <include>$(MALIBU_ROOT)/arguments
	  <include>$(MALIBU_ROOT)/evaluate/prediction
	  <include>$(MALIBU_ROOT)/evaluate
	  <include>$(MALIBU_ROOT)/evaluate/measure
	  <include>$(MALIBU_ROOT)/dataset
	  <include>$(MALIBU_ROOT)/util
	  <include>$(MALIBU_ROOT)/util/template
	  <include>$(MALIBU_ROOT)/util/format
	  <include>$(MALIBU_ROOT)/3rdparty/util/bzip2
	  <toolset>msvc:<define>_SCL_SECURE_NO_WARNINGS
	  <toolset>msvc:<define>_CRT_SECURE_NO_WARNINGS
    : usage-requirements
    ;
    
import os ;

if [ os.name ] = SOLARIS
{
  lib socket ;
  lib nsl ;
}
else if [ os.name ] = NT
{
  lib ws2_32 ;
  lib mswsock ;
}
else if [ os.name ] = HPUX
{
  lib ipv6 ;
}
    
util_def = <define>_MALIBU_VERSION=$(MALIBU_VERSION) ;
utillib = $(MALIBU_ROOT)/3rdparty/util//bz2
	  	  $(MALIBU_ROOT)/util//util ;
	  	   
boostinc = ;
boostdef = ;
stdflag = <libtype>static ;
thrflag = <libtype>threading ;
if [ boost-configured ]
{
	boostinc = [ boost-include ] ;
	boostdef = <define>_USING_BOOST ;
	stdflag = $(stdflag)/$(boostdef) ;
	thrflag = $(thrflag)/$(boostdef) ;
	if $(boostinc)
	{
		stdflag = $(stdflag)/$(boostinc) ;
		thrflag = $(thrflag)/$(boostinc) ;
	}
	thrflag = $(thrflag)/<threading>multi ;
	util_lib = $(utillib)/$(stdflag) ;
}
else
{
	thrflag = $(thrflag)/<threading>multi ;
	util_lib = $(utillib) ;
}

asio = <build>no ;

if [ boost_asio-configured ]
{
	asio = [ boost_system-library ] 
	 	   <threading>multi:<find-shared-library>boost_system-mt 
	 	   <threading>single:<find-shared-library>boost_system 
	;
}

exe postfile : 	postfile.cpp $(utillib)/$(thrflag)
			 : 	<include>$(MALIBU_ROOT)/network
			    $(asio) $(util_def) $(boostinc) $(boostdef)
    			<threading>multi
			    <os>SOLARIS:<define>_XOPEN_SOURCE=500
			    <os>SOLARIS:<define>__EXTENSIONS__
			    <os>SOLARIS:<library>socket
			    <os>SOLARIS:<library>nsl
			    <os>NT:<define>_WIN32_WINNT=0x0501
			    <os>NT,<toolset>cw:<library>ws2_32
			    <os>NT,<toolset>cw:<library>mswsock
			    <os>NT,<toolset>gcc:<library>ws2_32
			    <os>NT,<toolset>gcc:<library>mswsock
			    <os>NT,<toolset>gcc-cygwin:<define>__USE_W32_SOCKETS
			    <os>HPUX,<toolset>gcc:<define>_XOPEN_SOURCE_EXTENDED
			    <os>HPUX:<library>ipv6
			   <define>BOOST_ALL_NO_LIB=1
    ;

exe bench : bench.cpp $(util_lib) : $(util_def)
            <include>$(MALIBU_ROOT)/evaluate/measure
            <include>$(MALIBU_ROOT)/evaluate/prediction
            <include>$(MALIBU_ROOT)/evaluate/prediction/template
          ;

#exe exam      : exam.cpp 	  $(util_lib) : $(util_def) ;
#exe evalf     : evalf.cpp 	  $(util_lib) : <define>_EVALF=0 $(util_def) ;
#exe transform : transform.cpp $(util_lib) : <define>_TRANSFORM $(util_def) ;
#exe transmute : transmute.cpp $(util_lib) : <define>_TRANSMUTE $(util_def) ;
#exe distform  : distform.cpp  $(util_lib) : <define>_DISTANCE_FORMAT $(util_def) ;
exe deform     : deform.cpp  $(util_lib)   : <define>_DEFORM $(util_def) ;
exe deform2     : deform2.cpp  $(util_lib)   : <define>_DEFORM $(util_def) ;
exe adeform2     : deform2.cpp  $(util_lib)   : <define>_ASPEN $(util_def) ;
exe sdeform2     : deform2.cpp  $(util_lib)   : <define>_SPARSE $(util_def) ;
exe partition
	: expartition.cpp $(util_lib)
	: <include>$(MALIBU_ROOT)/ml/validation <define>_PARTITION $(util_def)
	;

#run evalf.cpp  : $(test).cfg : : <define>_EVALF=0 : evalf_test ;
#exe spartition : expartition.cpp : : <include>$(MALIBU_ROOT)/ml/validation <define>_PARTITION <define>_SPARSE ;
#exe stransform : transform.cpp : : <define>_TRANSFORM <define>_SPARSE ;


	