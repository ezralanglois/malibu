/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * bench.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#include "exegete.h"
#include "ExampleLabel.h"
#include "prediction_template.hpp"
#include "prediction_template_def.hpp"
#include <map>

typedef double float_type;
typedef void threshold_type;
typedef float_type* conf_type;

typedef ::exegete::ExampleLabel<float, std::string>								class_type;
typedef ::exegete::prediction_template<conf_type,class_type,threshold_type>		template_type;
typedef ::exegete::prediction_record_set<conf_type,class_type,threshold_type>	prediction_type;
typedef ::exegete::prediction_template<float_type,class_type,float_type>		bin_template_type;
typedef ::exegete::prediction_record_set<float_type,class_type,float_type>		bin_prediction_type;
typedef ::exegete::ArgumentMap													argument_map;
typedef ::exegete::prediction_template_def										template_def;
typedef std::vector< std::pair< std::string, float > >							label_map;

/** @file bench.cpp
 * @brief Learning output benchmark utility
 * 
 * This source file contains the main function as well as several
 * support functions for writing argument groups.
 *
 * @ingroup ExegeteProgram
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section bench Bench Utility
 * 	- i
 * 		- Description: a comma separated list of files, or space separated list
 * 		- Type: list of string filenames
 * 		- Viewability: Standard
 * 		- Master argument
 * 	- t
 * 		- Description: a file that holds the template
 * 		- Type: a string template name
 * 		- Viewability: Standard
 * 	- v
 * 		- Description: the type of output
 * 		- Type: options(None:0;Predictions:1;Builtin:2)
 * 		- Viewability: Standard
 * 	- c
 * 		- Description: an array of costs for each class
 * 		- Type: a list of floats
 * 		- Viewability: Standard
 * 	- w
 * 		- Description: set automated costs
 * 		- Type: options(None:0;Balanced:1;Unweighted:2)
 * 		- Viewability: Standard
 */

/** Reads a label map from a file.
 * 
 * @param file a source string filename.
 * @param map a destination label map.
 * @param an error message or NULL.
 */
const char* read_label_map(const std::string& file, label_map& map, int type);

/** The main driver function for the bench utility.
 * 
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @return an error code pass to the operating system.
 */
int main(int argc, char **argv)
{
	const char* msg;
	argument_map map;
	std::string templatestr;
	prediction_type predictions;
	template_type templateformat(predictions);
	bin_prediction_type bin_predictions;
	bin_template_type bin_templateformat(bin_predictions);

	std::vector<const char*> files;
	std::vector<float> costs;
	label_map labelmap;
	std::string templatefile;
	std::string labelfile;
	int labelTypeInt=0;
	int mc=0;
	int verbose=0;
	int weight_type=0;
	std::vector<int> only_pos;
	float thresh = TypeUtil<float>::max();

	header(map, argv[0]);
	map(files,			"i", "a comma separated list of files, or space separated command line list", argument_map::NONE, true);
	map(templatefile,	"t", "a file that holds the template");
	map(verbose,		"v", "the type of output>None:0;Predictions:1;Builtin:2");
	map(costs,			"c", "an array of costs for each class");
	map(weight_type,	"w", "set automated costs>None:0;Balanced:1;Unweighted:2");
	map(thresh,  		"b", "change binary threshold - used only for multi-class output");
	map(mc,  			"m", "force multi-class?");
	map(labelfile, 		"l", "a file containing a mapping from unique id to class label");
	map(only_pos,       "o", "only positive bags will be considered");
	map(labelTypeInt,   "lt", "label type>UniqueID:0;Index:1");
	templateformat.init(map);

	std::vector<std::string> names; names.push_back(argv[0]);
	if( (msg=map.parse_implicit(names, "cfg", "cfg", ".exegete.cfg")) != 0 ) return error(map, msg, std::cout);
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg);
	if( verbose == 1 )
	{
		template_def def_bn(true);
		template_def def_mc(false);
		std::cout << "Binary templates\n" << std::endl;
		std::cout << def_bn << std::endl;
		std::cout << "\n\nMulti-class templates\n" << std::endl;
		std::cout << def_mc << std::endl;
		return 0;
	}
	else if( verbose == 2 )
	{
		template_def def_bn(true);
		template_def def_mc(false);
		std::string templatestr_bn;
		if( templatefile.empty() ) 
		{
			templatefile = def_bn.default_file();
			templatestr_bn = def_bn.build(templatefile.c_str());
			templatefile = def_mc.default_file();
			templatestr = def_mc.build(templatefile.c_str());
		}
		else
		{
			templatestr_bn = def_bn.build(templatefile.c_str());
				templatestr = def_mc.build(templatefile.c_str());
		}
		
		if( templatestr_bn == "" && templatestr == "" ) return error(map, ERRORMSG("Error finding template: " << templatefile));
		
		if(!templatestr_bn.empty()) 
		{
			std::cout << "Binary template\n" << std::endl;
			std::cout << templatestr_bn << std::endl;
		}
		if(!templatestr.empty()) 
		{
			if( !templatestr_bn.empty() ) std::cout << "\n" << std::endl;
			std::cout << "Multi-class templates\n" << std::endl;
			std::cout << templatestr << std::endl;
		}
		std::cout << templatestr << std::endl;
		return 0;
	}
	
	if ( !labelfile.empty() ) 
	{
		if( (msg=read_label_map(labelfile, labelmap, labelTypeInt)) != 0 ) return error(map, msg);
	}
	
	if( isstdin() && (msg=readfile(std::cin, predictions)) != 0 ) return error(map, msg);
	for(unsigned int i=0;i<files.size();++i)
		if( (msg=readfile(files[i], predictions)) != 0 ) return error(map, msg);
	if( predictions.empty() ) return error(map, "No predictions read");
	if( (msg=predictions.set_cost(costs, weight_type)) != 0 ) return error(map, msg);
	if( !mc ) predictions.split(bin_predictions);
	//split
	if( !bin_predictions.empty() )
	{
		if( !labelmap.empty() ) 
		{
			if( (msg=bin_predictions.set_labels(labelmap, labelTypeInt)) != 0 ) return error(map, msg);
		}
		if( thresh != TypeUtil<float>::max() ) bin_predictions.threshold(thresh);
		if( !only_pos.empty() ) 
		{
			if( only_pos.size() == 1 ) bin_predictions.only_positive_bags(only_pos[0], only_pos[0]);
			else bin_predictions.only_positive_bags(only_pos[0], only_pos[1]);
		}
		template_def def(true);
		if( templatefile.empty() ) templatefile = def.default_file();
		templatestr = def.build(templatefile.c_str());
		if( templatestr == "" ) return error(map, ERRORMSG("Error finding template for binary: " << templatefile));
		if( (msg=bin_templateformat.build(&(*templatestr.begin()))) != 0 ) return error(map, msg);
		if( (msg=writefile(std::cout, bin_templateformat, "stdout")) != 0 ) return error(map,msg);
	}
	if( !predictions.empty() )
	{
		if( !labelmap.empty() ) 
		{
			if( (msg=predictions.set_labels(labelmap, labelTypeInt)) != 0 ) return error(map, msg);
		}
		template_def def(false);
		if( templatefile.empty() ) templatefile = def.default_file();
		templatestr = def.build(templatefile.c_str());
		if( templatestr == "" ) return error(map, ERRORMSG("Error finding template for multi-class: " << templatefile));
		if( verbose == 2 )
		{
			std::cout << templatestr << std::endl;
			return 0;
		}
		if( (msg=templateformat.build(&(*templatestr.begin()))) != 0 ) return error(map, msg);
		if( (msg=writefile(std::cout, templateformat, "stdout")) != 0 ) return error(map,msg);
	}
	return 0;
}

const char* read_label_map(const std::string& file, label_map& map, int type)
{
	const char* msg;
	std::ifstream fin;
	std::string line;
	unsigned int linenum=0;
	if( (msg=openfile(fin, file.c_str())) != 0 ) return msg;
	while(!fin.eof())
	{
		std::getline(fin, line);
		linenum++;
		if( line.empty() ) continue;
		map.resize(map.size()+1);
		if(!stringToValue(line, map.back(), ","))
			return ERRORMSG("Failed to parse line: (" << linenum << ") " << line);
	}
	fin.close();
	return 0;
}



