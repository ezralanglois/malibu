/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * deform.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#include "exegete.h"
//#ifdef _DEFORM
#include "ExampleFormat.h"
#include "transform/attribute_transform.hpp"
//#include "HeaderFormat.h"

#ifdef _SPARSE
typedef std::pair<float,int>									feature_type;
#else
typedef float													feature_type;
#endif
typedef int														class_type;
typedef ::exegete::ArgumentMap									argument_map;
typedef ::exegete::ExampleFormat<feature_type, class_type>		reader_type;
typedef ::exegete::attribute_transform<feature_type, class_type> transform_type;
//typedef ::exegete::HeaderFormat<feature_type, class_type> 		header_format;
typedef reader_type::concrete_type								dataset_type;
typedef std::vector<dataset_type::size_type>					index_vector;

/** @file deform.cpp
 * @brief DEscriptor TransFORM utility
 * 
 * This is the main driver for deform, an attribute transformation program.
 *	# Normalizes real attributes, e.g. 20.3 ... -20.3 -> 1 ... -1
 *	# Converts nominal attributes to binary (adds number of columns equal to number of nominal values minus 1)
 *	# Fills in missing values using the average from that column
 * 
 * @todo remove missing classes
 * @todo add/remove labels
 *
 * @ingroup ExegeteProgram
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section deform Deform Utility
 * 	- input
 * 		- Description: input dataset file (master argument 1)
 * 		- Type: list of string filenames
 * 		- Viewability: Standard
 * 		- Master argument
 * 	- mode
 * 		- Description: mode of usage
 * 		- Type: option(Print:p;Distance:d;Remove:r)
 * 		- Viewability: Standard
 * 	- columns
 * 		- Description: columns to process (remove) either indices or labels
 * 		- Type: list of strings
 * 		- Viewability: Standard
 */

/** The main driver function for the bench utility.
 * 
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @return an error code pass to the operating system.
 */
int main(int argc, char **argv)
{
	const char* msg;
	argument_map map; 

	header(map, argv[0]);
	reader_type reader(map, "#DS\t", false);
	transform_type transform;
	std::vector< std::string > inputfiles;

	map("I/O");
	map(inputfiles,   "input",   "input dataset file (master argument 1)", argument_map::NONE, true);
	transform.init(map, 0);
	map.init(map);
	
	std::vector<std::string> names;names.push_back(argv[0]);
	if( (msg=map.parse_implicit(names, "cfg", "cfg", ".exegete.cfg")) != 0 ) return error(map, msg, std::cout);
	if( (msg=map.parse()) != 0 ) return error(map, msg);
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg);
	
	if( inputfiles.empty() ) return error(map, ERRORMSG("No input files"));
	
	std::string extension=transform.extension(), tmp;
	dataset_type dataset, headerset;
	std::string outputfile;
	for(unsigned int i=0;i<inputfiles.size();++i)
	{
		if( (msg=reader.read(inputfiles[i], dataset)) != 0 ) return error(map, msg);
		headerset=dataset;
		if( (msg=transform.transform(dataset)) != 0 ) return error(map, msg);
		if( extension != "" )
		{
			tmp = ext_name(inputfiles[i].c_str());
			outputfile=strip_ext(inputfiles[i].c_str());
			outputfile+="_"+tmp+"."+extension;
			if( (msg=reader.write(outputfile, dataset)) != 0 ) return error(map, msg);
		}
		dataset = headerset;
	}
	return 0;
}

//#endif


