/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * partition.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#include "exegete.h"
//#ifdef _PARTITION
#include "ExampleFormat.h"
#include "PartitionValidation.h"

#ifdef _SEQ
#include "SequenceExample.h"
typedef const ::exegete::Symbol<float>*										feature_type;
#else
#ifdef _SPARSE
typedef std::pair<float,int>												feature_type;
#else
typedef float																feature_type;
#endif
#endif
typedef int																	class_type;
typedef ::exegete::ArgumentMap												argument_map;
typedef ::exegete::ExampleFormat<feature_type, class_type>					reader_type;
typedef reader_type::concrete_type											dataset_type;
typedef reader_type::format_type											format_type;
typedef ::exegete::PartitionValidation<std::string>							validation_type;
/** @file expartition.cpp
 * @brief Dataset partition utility
 * 
 * This is the main driver for partition; it uses an automated partition algorithm
 * to split a dataset into training and testing sets:
 * 	- Cross-validation
 * 	- Holdout
 * 	- Inverted Holdout
 * 	- Progressive Validation
 * 	- Inverted Progressive Validation
 * 	- Sampling with replacement
 * 	- Sampling without replacement
 *
 * @todo rename to depart
 * @ingroup ExegeteProgram
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section partition Partition Utility
 * 	- input
 * 		- Description: input dataset file (master argument 1)
 * 		- Type: a string filename
 * 		- Viewability: Standard
 * 		- Master argument
 * 	- output
 * 		- Description: output dataset file directory and name
 * 		- Type: a string filename
 * 		- Viewability: Standard
 * 	- seed
 * 		- Description: seed single random number generator
 * 		- Type: long integer
 * 		- Viewability: Standard
 */

/** The main driver function for the partition utility.
 * 
 * 
 * @todo remove seed argument
 * 
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @return an error code pass to the operating system.
 */
int main(int argc, char **argv)
{
	const char* msg;
	argument_map map; 
	std::string inputfile;
	std::string outputfile;
	dataset_type dataset;
	unsigned long seedInt=0L;

	header(map, argv[0]);
	map("Partition");
	validation_type validation(map);
	reader_type reader(map, "#DS\t");

	map("I/O");
	map(inputfile,		"input",		"input dataset file (master argument 1)", argument_map::NONE,  true);
	map(outputfile,		"output",		"output dataset file directory and name");
	map(seedInt,		"seed",			"seed single random number generator");
	
	std::vector<std::string> names;names.push_back(argv[0]);
	if( (msg=map.parse_implicit(names, "cfg", "cfg", ".exegete.cfg")) != 0 ) return error(map, msg, std::cout);
	if( (msg=map.parse()) != 0 )			return error(map, msg);
	if( (msg=map.parse(argc, argv)) != 0 )	return error(map, msg);

	if( seedInt != 0L ) seed_random(seedInt);
	if( inputfile.empty() ) return error(map, ERRORMSG("No input file"));
	if( outputfile.empty() ) outputfile = base_name(inputfile.c_str());
	if( (msg=reader.read(inputfile, dataset)) != 0 ) return error(map, msg);
	if( validation.type() != validation_type::NO ) std::cout << validation.toString() << std::endl;//move to validation init?
	
	if( (msg=validation.validate(reader.currentFormat(), outputfile, dataset)) != 0 ) return error(map, msg);
	return 0;
}


//#endif




