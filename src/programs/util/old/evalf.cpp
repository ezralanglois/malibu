/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * evaluate.cpp
 * Copyright (C) 2007 Robert Ezra Langlois
 */
#include "exegete.h"
#ifdef _EVALF
#include "MetricTemplateDef.h"
//#include "MetricTemplate.h"
#include "ExampleLabel.h"
#include "PredictionTemplate.h"

typedef float														float_type;
typedef ::exegete::ExampleLabel<int, std::string>					class_type;
typedef ::exegete::MetricTemplateDef								metricdef_type;
typedef ::exegete::ExperimentPredictionSet<class_type, float_type>	prediction_type;
//typedef ::exegete::MetricTemplate<class_type, float_type>		prediction_type;
typedef ::exegete::PredictionTemplate<class_type, float_type>		template_type;
typedef ::exegete::ArgumentMap										argument_map;
typedef template_type::metric_factory 								metric_factory;
typedef template_type::plot_factory 								plot_factory;

int main(int argc, char **argv)
{
	const char* msg;
	argument_map map;
	metricdef_type def;
	std::string templatestr;
	prediction_type predictions;
	template_type templateformat(predictions);
	const metric_factory& metrics = metric_factory::instance();

	std::vector<const char*> files;
	std::string templatefile;
	int type = _EVALF;
	int post = 0;
	int zip = 0;
	int metricInt = 0;
	int verbose=0;//last argument type, single flag
	std::vector<float> threshAr;

	if( _EVALF == 0 ) header(map, "evalf");
	else			  header(map, "evalp");

	map(files,			"i", "a comma separated list of files, or space separated command line list", argument_map::NONE, true);
	map(templatefile,	"t", "a file that holds the template");
	map(type,			"p", "prediction read type treat files as separate experiments, runs of same experiment, or single experiment>Experiment:0;Run:1;Single:2");
	map(post,			"m", "post processing preditions>None:0;Sum:1");
	map(threshAr,		"th", "threshold, two thresholds for MIL: (bag_thresh),(inst_thresh)");
	map(verbose,		"v", "the type of output>Predictions:0;Builtin:1;Template:2");
	map(zip,			"z", "write compressed output?");
	map(metricInt,		"o", metrics.options("threshold metric for optimal bag>None:0;",1));

	std::vector<std::string> names;names.push_back("evalf");
	if( (msg=map.parse_implicit(names, "cfg", "cfg", ".exegete.cfg")) != 0 ) return error(map, msg, std::cout);
	if( (msg=map.parse(argc, argv)) != 0 )	return error(map, msg);

	if( verbose == 1 )
	{
		def.print(std::cout, "\n");
		return 0;
	}
	if( templatefile == "" ) templatestr = def.build("std");
	else templatestr = def.build(templatefile.c_str());
	if( templatestr=="" ) return error(map, ERRORMSG("Error finding template: " << templatefile));
	if( verbose == 2 )
	{
		std::cout << templatestr << std::endl;
		return 0;
	}

	predictions.type(type);
	predictions.post(post);
	if( (msg=templateformat.build(&(*templatestr.begin()))) != 0 ) return error(map, msg);
	if( isstdin() && (msg=readfile(std::cin, predictions)) != 0 ) return error(map, msg);
	for(unsigned int i=0;i<files.size();++i)
		if( (msg=readfile(files[i], predictions)) != 0 ) return error(map, msg);
	if( predictions.empty() ) return error(map, "No predictions read");
	if( !threshAr.empty() )
	{
		if( threshAr.size() == 1 ) threshAr.resize(2, FLT_MAX);
		if( metricInt > 0 )
			 predictions.set_optimal_threshold(threshAr[0], metrics[metricInt-1]);
		else predictions.set_threshold(threshAr[0], threshAr[1]);
	}
	if( (msg=writefile(std::cout, templateformat, zip?".bz2":"stdout")) != 0 ) return error(map,msg);
	return 0;
}



#endif


