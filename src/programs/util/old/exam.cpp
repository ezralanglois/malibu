/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * evaluate.cpp
 * Copyright (C) 2007 Robert Ezra Langlois
 */
#include "exegete.h"
#include "MetricTemplateDef.h"
//#include "MetricTemplate.h"
#include "ExampleLabel.h"
#include "PredictionTemplate.h"

typedef float														float_type;
typedef ::exegete::ExampleLabel<int, std::string>					class_type;
typedef ::exegete::MetricTemplateDef								metricdef_type;
typedef ::exegete::ExperimentPredictionSet<class_type, float_type>	prediction_type;
//typedef ::exegete::MetricTemplate<class_type, float_type>		prediction_type;
typedef ::exegete::PredictionTemplate<class_type, float_type>		template_type;
typedef ::exegete::ArgumentMap										argument_map;
typedef template_type::metric_factory 								metric_factory;
typedef template_type::plot_factory 								plot_factory;

int main(int argc, char **argv)
{
	const char* msg;
	argument_map map;
	metricdef_type def;
	std::string templatestr;
	prediction_type predictions;
	template_type templateformat(predictions);
	const metric_factory& metrics = metric_factory::instance();

	std::vector<const char*> files;
	std::vector<std::string> templatefiles;
	std::string base="exam";
	int type = 0;
	int post = 0;
	int zip = 0;
	std::vector<int> metricInt;
	int verbose=0;//last argument type, single flag
	std::vector<float> threshAr;

	header(map, argv[0]);

	map(files,			"i", "a comma separated list of files, or space separated command line list", argument_map::NONE, true);
	map(templatefiles,	"t", "a file that holds the template");
	map(base,			"b", "the base name for the output files");
	map(type,			"p", "prediction read type treat files as separate experiments, runs of same experiment, or single experiment>Experiment:0;Run:1;Single:2");
	map(post,			"m", "post processing preditions>None:0;Sum:1");
	map(threshAr,		"th", "threshold, two thresholds for MIL: (bag_thresh),(inst_thresh)");
	map(verbose,		"v", "the type of output>Predictions:0;Builtin:1");
	map(zip,			"z", "write compressed output?");
	map(metricInt,		"o", "metrics");//metrics.options("threshold metric for optimal bag--"));

	std::vector<std::string> names; names.push_back(argv[0]);
	if( (msg=map.parse_implicit(names, "cfg", "cfg", ".exegete.cfg")) != 0 ) return error(map, msg, std::cout);
	if( (msg=map.parse(argc, argv)) != 0 )	return error(map, msg);

	if( verbose == 1 )
	{
		def.print(std::cout, "\n");
		return 0;
	}
	if( templatefiles.empty() ) templatefiles.push_back("std");

	predictions.type(type);
	predictions.post(post);
	if( isstdin() && (msg=readfile(std::cin, predictions)) != 0 ) return error(map, msg);
	for(unsigned int i=0;i<files.size();++i)
		if( (msg=readfile(files[i], predictions)) != 0 ) return error(map, msg);
	if( predictions.empty() ) return error(map, "No predictions read");

	std::string output;
	if(!metricInt.empty())
	{
		if( threshAr.size() == 0 ) threshAr.resize(1, FLT_MAX);
		predictions.set_optimal_threshold(threshAr[0], metrics, metricInt);
	}
	if( !threshAr.empty() )
	{
		if( threshAr.size() == 1 ) threshAr.resize(2, FLT_MAX);
		predictions.set_threshold(threshAr[0], threshAr[1]);
	}

	for(unsigned int i=0;i<templatefiles.size();++i)
	{
		output=base+"_"+templatefiles[i];
		if( zip ) output+=".bz2";
		templatestr = def.build(templatefiles[i].c_str());
		if( templatestr=="" ) return error(map, ERRORMSG("Error finding template: " << templatefiles[i]));
		if( (msg=templateformat.build(&(*templatestr.begin()))) != 0 ) return error(map, msg);
		if( (msg=writefile(output, templateformat)) != 0 ) return error(map,msg);
	}
	return 0;
}



