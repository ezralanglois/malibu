/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * transform.cpp
 * Copyright (C) 2007 Robert Ezra Langlois
 */
#include "exegete.h"
#ifdef _TRANSFORM
#include "ExampleFormat.h"
#include "ConcreteExampleSetAlgorithms.h"
#include "HeaderFormat.h"

/** @todo
 *    - remove missing classes
 *	  - add/remove labels
 */

#ifdef _SPARSE
typedef std::pair<float,int>									feature_type;
#else
typedef float													feature_type;
#endif
typedef int														class_type;
typedef ::exegete::ArgumentMap									argument_map;
typedef ::exegete::ExampleFormat<feature_type, class_type>		reader_type;
typedef ::exegete::HeaderFormat<feature_type, class_type> 		header_format;
typedef reader_type::concrete_type								dataset_type;
typedef std::vector<dataset_type::size_type>					index_vector;


int main(int argc, char **argv)
{
	std::vector<int> flags;
	int alter=0;
	const char* msg;
	argument_map map; 
	std::string inputfile;
	std::string outputfile;
	std::string headfile;
	dataset_type dataset;
	header_format hformat(map);

	header(map, argv[0]);
	reader_type reader(map, "#DS\t", false);

#ifndef _SPARSE
	map("Transform");
	map(alter, "transform", "convert dataset type>None:0;Nominal2Binary:1;MinMaxNorm:2;ZNorm:3;ZANorm:4");
#endif

	map("I/O");
	map(inputfile,  "input",  "input dataset file (master argument 1)", argument_map::NONE,  true);
	map(outputfile, "output", "output dataset file (master argument 2)", argument_map::NONE, true);
	map(headfile,   "header", "a header file mapping attribute types", argument_map::NONE, true);
	
	std::vector<std::string> names;names.push_back("evalf");
	if( (msg=map.parse_implicit(names, "cfg", "cfg", ".exegete.cfg")) != 0 ) return error(map, msg, std::cout);
	if( (msg=map.parse()) != 0 ) return error(map, msg);
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg);
	if( (msg=reader.read(inputfile, dataset)) != 0 ) return error(map, msg);
	if( !headfile.empty() && (msg=hformat.read(headfile, dataset)) != 0 ) return error(map, msg);
	
#ifndef _SPARSE
	if( alter )
	{
		if( outputfile.empty() ) return error(map, "Output file empty");
			 if( alter == 1 ) ::exegete::nominalToBinary(dataset);
		else if( alter == 2 ) ::exegete::normalize_minmax(dataset, flags);
		else if( alter == 3 ) ::exegete::normalize_zscore(dataset, flags);
		else if( alter == 4 ) ::exegete::normalize_zscorea(dataset, flags);
		std::cout << "Outputfile: " << outputfile << std::endl;
		std::cout << dataset.toString() << std::endl;
	}
#endif
	if( (msg=reader.write(outputfile, dataset)) != 0 ) return error(map, msg);
	return 0;
}

#endif


