/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * postfile.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#include "exegete.h"
#include "http_post.hpp"
#include "fileutil.h"
#include "fileioutil.h"

/** @file postfile.cpp
 * @brief Post prediction file utility
 * 
 * This is the main driver for post file; it will post a prediction file
 * to a database using CGI POST. This program depends on Boost.Asio.
 *
 * @ingroup ExegeteProgram
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @page args Program Arguments
 *
 * @section postfile Post File Utility
 * 	- input
 * 		- Description: input dataset file (master argument 1)
 * 		- Type: a list of string filenames
 * 		- Viewability: Standard
 * 		- Master argument
 * 	- url
 * 		- Description: destination url
 * 		- Type: a string url
 * 		- Viewability: Standard
 */

/** The main driver function for the postfile utility.
 * 
 * 
 * @param argc number of arguments.
 * @param argv list of string arguments.
 * @return an error code pass to the operating system.
 */
int main(int argc, char **argv)
{
	const char* msg;
	::exegete::ArgumentMap map; 
	std::vector< std::string > inputfiles;
	std::string url="jacob@cgi.www.emccsoft.com:/cgi-bin/www.emccsoft.com/post.pl";
	std::string buf;

	header(map, argv[0]);
	map("General");
	map(inputfiles,  "input",  "input dataset file (master argument 1)", ::exegete::ArgumentMap::NONE, true);
	map(url, 		 "url",    "destination url");
	
	std::vector<std::string> names;names.push_back(argv[0]);
	if( (msg=map.parse_implicit(names, "cfg", "cfg", ".exegete.cfg")) != 0 ) return error(map, msg, std::cout);
	if( (msg=map.parse()) != 0 ) return error(map, msg);
	if( (msg=map.parse(argc, argv)) != 0 ) return error(map, msg);
	
	if( inputfiles.empty() ) return error(map, ERRORMSG("No input files"));
	for(unsigned int i=0;i<inputfiles.size();++i)
	{
		if( (msg=readfile2buffer(inputfiles[i], buf)) != 0 ) return error(map, msg, std::cout);
		if( (msg=::exegete::http_post::post(url, buf, std::cout)) != 0 ) return error(map, msg, std::cout);
	}
	return 0;
}


