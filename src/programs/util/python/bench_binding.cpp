/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2007-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * bench_binding.cpp
 * Copyright (C) 2007-2008 Robert Ezra Langlois
 */
#include "ExampleLabel.h"
#include "prediction_record_set.hpp"
#include "measure.hpp"

#include "prediction_binding.hpp"
#include "prediction_set_binding.hpp"
#include "prediction_record_binding.hpp"
#include "prediction_record_set_binding.hpp"
#include "measure_binding.hpp"

/** Defines a Boost.Python module with name X **/
#define GETMODULENAME(X) BOOST_PYTHON_MODULE(X)

typedef float class_value;
typedef double float_type;
typedef void threshold_type;
typedef float_type* conf_type;

typedef ::exegete::ExampleLabel<class_value, std::string>						class_type;
typedef ::exegete::prediction_record_set<conf_type,class_type,threshold_type>	prediction_record_set_mc;
typedef ::exegete::prediction_record_set<float_type,class_type,float_type>		prediction_record_set_bn;

typedef prediction_record_set_bn::value_type	prediction_record_bn;
typedef prediction_record_bn::value_type		prediction_set_bn;
typedef prediction_set_bn::value_type			prediction_bn;

typedef prediction_record_set_mc::value_type	prediction_record_mc;
typedef prediction_record_mc::value_type		prediction_set_mc;
typedef prediction_set_mc::value_type			prediction_mc;

typedef prediction_set_bn::const_iterator 								const_bn_pred_iterator;
typedef ::exegete::measure<const_bn_pred_iterator,double,float_type> 	measure_bn;
typedef prediction_set_mc::const_iterator 								const_mc_pred_iterator;
typedef ::exegete::measure<const_mc_pred_iterator,double,conf_type> 	mc_measure;

// Bindings

typedef ::exegete::prediction_binding< prediction_mc > 							prediction_binding_mc;
typedef ::exegete::prediction_set_binding< prediction_set_mc > 					prediction_set_binding_mc;
typedef ::exegete::prediction_record_binding< prediction_record_mc > 			prediction_record_binding_mc;
typedef ::exegete::prediction_record_set_binding< prediction_record_set_mc > 	prediction_record_set_binding_mc;

typedef ::exegete::prediction_binding< prediction_bn > 							prediction_binding_bn;
typedef ::exegete::prediction_set_binding< prediction_set_bn > 					prediction_set_binding_bn;
typedef ::exegete::prediction_record_binding< prediction_record_bn > 			prediction_record_binding_bn;
typedef ::exegete::prediction_record_set_binding< prediction_record_set_bn > 	prediction_record_set_binding_bn;
typedef ::exegete::measure_binding< measure_bn, prediction_record_set_bn > 		measure_binding_bn;

/** @file bench_binding.cpp
 * @brief Python binding to learning output benchmark utility
 * 
 * This source file contains a python interface module to the
 * benchmark program.
 *
 * @ingroup ExegeteProgram
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

using namespace boost::python;
/** Defines a python module with a name specified during compile-time. This 
 * module contains the following classes:
 * 	- measure_bn: a binary measure
 * 	- prediction_bn: a binary prediction
 * 	- prediction_set_bn: a set of binary predictions
 * 	- prediction_record_bn: a record of binary predictions
 * 	- prediction_record_set_bn: a set of binary prediction records
 * 	- prediction_mc: a multi-class prediction
 * 	- prediction_set_mc: a set of multi-class predictions
 * 	- prediction_record_mc: a record of multi-class predictions
 * 	- prediction_record_set_mc: a set of multi-class prediction records
 * 
 * @todo add multi-class measure
 * @todo make predictions independent of measure??
 */
GETMODULENAME(_SHAREDEXEGETEMODULE)
{
	class_< measure_bn >("measure_bn")
			.def( measure_binding_bn() )
		;
	class_< prediction_bn >("prediction_bn")
			.def( prediction_binding_bn() )
		;
	class_< prediction_set_bn >("prediction_set_bn")
			.def( prediction_set_binding_bn() )
		;
	class_< prediction_record_bn >("prediction_record_bn")
			.def( prediction_record_binding_bn() )
		;
	class_< prediction_record_set_bn >("prediction_record_set_bn")
			.def( prediction_record_set_binding_bn() )
		;
	class_< prediction_mc >("prediction_mc")
			.def( prediction_binding_mc() )
		;
	class_< prediction_set_mc >("prediction_set_mc")
			.def( prediction_set_binding_mc() )
		;
	class_< prediction_record_mc >("prediction_record_mc")
			.def( prediction_record_binding_mc() )
		;
	class_< prediction_record_set_mc >("prediction_record_set_mc")
			.def( prediction_record_set_binding_mc() )
		;
}


