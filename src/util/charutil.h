/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * charutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_CHARUTIL_H
#define _EXEGETE_CHARUTIL_H
#include <cctype>

/** @file charutil.h
 * @brief Defines character utility macros.
 * 
 * This header file contains a set of string substitution macros to 
 * deal with bugs in the GNU complier.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#ifdef __GNUC__ // Hack for GNU GCC <= 2.96 (maybe later)
#if ! defined __GNUC_PREREQ
/** Provides a standard interface for a cctype hack. **/
#define IS_DIGIT(n) std::isdigit(n)
/** Provides a standard interface for a cctype hack. **/
#define IS_SPACE(n) std::isspace(n)
/** Provides a standard interface for a cctype hack. **/
#define IS_ALPHA(n) std::isalpha(n)
# elif defined __cplusplus && __GNUC_PREREQ (3,0)
/** Provides a standard interface for a cctype hack. **/
#define IS_DIGIT(n) std::isdigit(n)
/** Provides a standard interface for a cctype hack. **/
#define IS_SPACE(n) std::isspace(n)
/** Provides a standard interface for a cctype hack. **/
#define IS_ALPHA(n) std::isalpha(n)
# else
/** Provides a standard interface for a cctype hack. **/
#define IS_DIGIT(n) isdigit(n)
/** Provides a standard interface for a cctype hack. **/
#define IS_SPACE(n) isspace(n)
/** Provides a standard interface for a cctype hack. **/
#define IS_ALPHA(n) isalpha(n)
#endif
#else
/** Provides a standard interface for a cctype hack. **/
#define IS_DIGIT(n) std::isdigit(n)
/** Provides a standard interface for a cctype hack. **/
#define IS_SPACE(n) std::isspace(n)
/** Provides a standard interface for a cctype hack. **/
#define IS_ALPHA(n) std::isalpha(n)
#endif





#endif










