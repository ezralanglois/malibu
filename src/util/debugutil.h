/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * debugutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_DEBUGUTIL_H
#define _EXEGETE_DEBUGUTIL_H

/** @file debugutil.h
 * @brief Defines a debug utility classes and macros.
 * 
 * This header file contains a set of string substitution macros utilities 
 * for debugging. When NDEBUG is defined (-DNDEBUG) then all the debug code 
 * is removed before compiling the exectuable.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#include "typeutil.h"
#ifndef _USING_BOOST
/** @brief Compile time assertion class (failed).
 * 
 * This class template defines a compile time assert. If the 
 *  compile time value is false, then this class is used and 
 *  a compile error occurs.
 */
template <bool x> struct STATIC_ASSERTION_FAILURE;
/** @brief Compile time assertion class (passed).
 * 
 * This class template defines a compile time assert.
 */
template <> struct STATIC_ASSERTION_FAILURE<true> 
{ 
	/** Flags the assertion as true.*/
	enum { value = 1 }; 
};
/** Defines a compile time assert. **/
#define BOOST_STATIC_ASSERT(x) typedef STATIC_ASSERTION_FAILURE<x> IS_STATIC_FAIL;
#else 
#include <boost/static_assert.hpp>
#endif
#ifndef NDEBUG
#include "fileutil.h"
#include <assert.h>
#include <iostream>
#include <string>

#ifdef __GNUC__
#include <cstdlib>
/** Tells gnuc to release pooled resources. **/
extern "C" void __libc_freeres(void);
/** A hack for valgrind that tells GNUC to release any pooled resources. **/
#define ONEXIT atexit(__libc_freeres)
#else
/** A hack for valgrind that does nothing for non-gnulinix os. **/
#define ONEXIT ((void)0)
#endif

/** The standard assert statement for malibu that includes filename, function and line in message. **/
#define ASSERT(TST)					\
			( (TST) ? (void)0			\
				: (std::cerr << strip_ext(base_name(__FILE__)) \
			<< "::" << __FUNCTION__ \
			<< " (" << __LINE__ << ")" \
			<< "  " #TST << std::endl,abort()))

/** A more informative assert statement for malibu that includes an additional message along with the filename, function and line in message. **/
#define ASSERTMSG(TST,MSG)					\
			( (TST) ? (void)0			\
				: (std::cerr << strip_ext(base_name(__FILE__)) \
			<< "::" << __FUNCTION__ \
			<< " (" << __LINE__ << ")" \
			<< "  " #TST	\
			<< "  " << MSG << std::endl,abort()))

/** Warns on a particular condition. **/
#define WARN(TST,MSG)					\
			if(!TST) std::cerr << strip_ext(base_name(__FILE__)) \
			<< "::" << __FUNCTION__ \
			<< " (" << __LINE__ << ")" \
			<< "  " #TST	\
			<< "  " << MSG << std::endl

/** Prints on a particular condition. **/
#define DBG_PRINT(TST,MSG)					\
			if(TST) std::cerr << MSG << std::endl

#else
/** This standard assert statment does nothing in "Release Mode". **/
#define ASSERT(MSG)			((void)0)
/** This standard assert message statment does nothing in "Release Mode". **/
#define ASSERTMSG(TST, MSG)	((void)0)
/** A hack for valgrind that does nothing in "Release Mode". **/
#define ONEXIT				((void)0)
/** This warning does nothing in "Release Mode". **/
#define WARN(TST,MSG) 		((void)0)
/** Prints on a particular condition. **/
#define DBG_PRINT(TST,MSG)  ((void)0)
#endif

#endif


