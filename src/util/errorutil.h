/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * errorutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ERRORUTIL_H
#define _EXEGETE_ERRORUTIL_H
#include "stringutil.h"
#include "fileutil.h"

/** @file errorutil.h
 * @brief Defines an error utility class and macros.
 * 
 * This header file contains a string substitution macro for real time errors 
 * and a corresponding helper class. The helper class converts a message 
 * in the iostream format to a simple c string (const char*).
 * 
 * The ErrorUtil class is not used directly; it is used in the macros:
 *	# ERRORMSG
 *	# ERRORCODE
 *	# ERRORADD
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** Takes a message in iostream format and converts it into a 
 * string. It also includes the file, function and line of the error.
 */
#define ERRORMSG(M) (ErrorUtil::new_util() << stripall(__FILE__) << "::" << __FUNCTION__ << "(" << __LINE__ << ")  " << M).str()
/** Takes a code and a message in iostream format and converts 
 * it into a string. The code will be the return value in main function.
 */
#define ERRORCODE(I, M) (ErrorUtil::new_util(I) << stripall(__FILE__) << "::" << __FUNCTION__ << "(" << __LINE__ << ")  " << M).str()
/** This macro adds a string to the current error message. **/
#define ERRORADD(M) (ErrorUtil::same_util() << M).str()

/** @brief Defines an interface to a static error buffer.
 * 
 * This class takes a message in iostream format,  
 * e.g. <code> "hello" << i << " " << j </code>, and stores
 * the message in a std::string held in static memory.
 */
struct ErrorUtil
{
	/** Gets a reference to a std::string held in static memory.
	 * 
	 *  @return reference to static std::string.
	 */
	static std::string& errorMsg()
	{
		static std::string msg="";
		return msg;
	}
    /** Gets a reference to a singleton ErrorUtil held in static memory
     *  after clearning the error message and setting the return error 
     *  code.
     * 
     * @param i an error code.
	 * @return reference to static ErrorUtil.
	 */
	static ErrorUtil& new_util(int i=1)
	{
		static ErrorUtil util;
		errorMsg()="";
		errcode()=i;
		return util;
	}
    /** Gets a reference to a singleton ErrorUtil held in static memory.
     * 
	 *  @return reference to static ErrorUtil.
	 */
	static ErrorUtil& same_util()
	{
		static ErrorUtil util;
		return util;
	}  
	/** Gets a pointer referring to the data held in the static std::string.
	 * 
	 *  @return a constant character pointer.
	**/
	const char* str()const
	{
		return errorMsg().c_str();
	}
	/** Returns a reference to an error code.
	 * 
	 * @return a reference to an error code.
	 */
	static int& errcode()
	{
		static int c=1;
		return c;
	}
};

/** Allows ErrorUtil to use the input stream operator.
 * 
 * @param util a reference to ErrorUtil.
 * @param val some value (determined a compile time).
 * @return a reference to ErrorUtil.
 */
template<class T>
inline ErrorUtil& operator<<(ErrorUtil& util, const T& val)
{
	std::string temp;
	::valueToString(val, temp);
	ErrorUtil::errorMsg()+=temp;
	return util;
}

/** Sets an error message in an object and sets the fail bit in some stream.
 * 
 * @param s a stream.
 * @param obj an object.
 * @param m an error message.
 * @return a stream.
 */
template<class S, class U>
inline S& fireError(S& s, const U& obj, const char* m)
{
	obj.errmsg = m;
	s.setstate( std::ios::failbit );
	return s;
}

#endif




