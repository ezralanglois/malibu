/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * fileioutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_FILEIOUTIL_H
#define _EXEGETE_FILEIOUTIL_H
#include "errorutil.h"
#include "fileutil.h"
#include "bzip2stream.hpp"
#include <fstream>
#include <string>

/** @file fileioutil.h
 * @brief Defines a set of file input/output utility functions.
 * 
 * 
 * This header file contains a set of convenience functions to standardize 
 * opening, reading and writing files. It also has routines for dealing 
 * with file names.
 * 
 * @attention Use Bzip2 compression when filename has extension .bz2
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @brief Concat path and filename.
 * 
 * Concats a file path with a file name testing for a platform dependent path
 *  separator.
 * 
 *  @param file an output concatenated file name.
 *  @param path a file path.
 *  @param name a file path/name.
 */
inline void concat_file(std::string& file, const std::string& path, std::string& name)
{
	file = path;
	if( file[file.size()-1] == PATH_SEP ) file+=PATH_SEP;
	file += name;
}
/** @brief Opens a file testing for errors.
 * 
 * Opens a file and tests for several types of errors including:
 *  # Empty file name
 *  # Error opening file
 * 
 *  It also clears the stream of any previous errors.
 *
 *  @param f a file stream.
 *  @param filename the file name.
 *  @return an error message or NULL.
 */
template<class fstream>//@todo correct stream?
inline const char* openfile(fstream& f, const std::string& filename, bool binary=false)
{
	if( filename.empty() ) return ERRORMSG("Empty filename");
	if( f.fail() ) f.clear();
	if( !binary ) binary = ( std::string(ext_name(filename.c_str()) ) == "bz2" );
	if(binary) f.open(filename.c_str(), std::ios::out | std::ios::binary);
	else f.open(filename.c_str());
	if( f.fail() ) return ERRORMSG("Cannot open " << filename);
	return 0;
}
/** @brief Test if file is open.
 * 
 * Tests the existence of a file.
 *
 *  @param filename the file name.
 *  @return an error message or NULL.
 */
inline const char* testfile(const std::string& filename)
{
	std::ifstream fin;
	return openfile(fin, filename);
}
/** @brief Searches for and opens a file.
 * 
 * Attempts to open a file searchs the current directory and the list of specified 
 *  directories. It also tests for several types of errors including:
 *  # Empty file name
 *  # Error opening file or finding the file
 *
 *  @param f a file stream.
 *  @param filename the file name.
 *  @param beg an iterator to the start of a collection of file paths.
 *  @param end an iterator to the end of a collection of file paths.
 *  @return an error message or NULL.
 */
template<class fstream, class I>
const char* openfile(fstream& f, const std::string& filename, I beg, I end)
{
	if( filename.empty() ) return ERRORMSG("Empty filename");
	std::string file;
	if( f.fail() ) f.clear();
	f.open(filename.c_str());
	if( !f.fail() ) return 0;
	for(;beg != end;++beg)
	{
		concat_file(file, *beg, filename);
		f.clear();
		f.open(file.c_str());
		if( !f.fail() ) return 0;
	}
	return ERRORMSG("Cannot open " << filename);
}
/** @brief Reads data from an input stream into an object.
 *
 * 
 * @attention Use Bzip2 decompression when filename has extension .bz2
 * 
 * @todo detect if bzip2 from stream
 * @todo checksum if bzip2 from stream
 * @todo add other compression types e.g. gzip
 *
 * @param in an input stream.
 * @param obj a reference to an object.
 * @param filename a filename.
 * @return an error message or NULL.
 */
template<class T>
const char* readfile(std::istream& in, T& obj, std::string filename="stdin")
{
	if( std::string(ext_name(filename.c_str()) ) == "bz2" )
	{
		bzip2_stream::bzip2_istream unzip( in );
		unzip >> obj;
		if( !unzip.eof() && unzip.fail() )
		{
			if( obj.errormsg() != 0 ) return obj.errormsg();
			return ERRORMSG("Failed to read bzip2 file: " << filename);
		}
	}
	else in >> obj;
	if( !in.eof() && in.fail() )
	{
		if( obj.errormsg() != 0 ) return obj.errormsg();
		return ERRORMSG("Failed to read file: " << filename);
	}
	return 0;
}
/** @brief Reads an object from a file.
 * 
 * Opens, reads and closes a file testing for several types of errors. The first line 
 *  of the file is read into a string and the rest into an object.
 * 
 * @attention Use Bzip2 decompression when filename has extension .bz2
 * 
 * @param filename a file name.
 * @param obj an object that supports the iostream input operator.
 * @param line a string to hold the first line of the file.
 * @return an error message or NULL.
 */
template<class T>
const char* readfile_line(const std::string& filename, T& obj, std::string& line)
{
	const char* msg;
	std::ifstream fin;
	if( (msg=openfile(fin, filename)) != 0 ) return msg;
	std::getline(fin, line);
	if( fin.eof() || fin.fail() )
	{
		fin.close();
		return ERRORMSG("Failed to read file: " << filename);
	}
	if( (msg=readfile(fin, obj, filename)) != 0 )
	{
		fin.close();
		return msg;
	}
	else fin.close();
	return 0;
}
/** @brief Reads an object from a file.
 * 
 * Opens, reads and closes a file testing for several types of errors.
 * 
 * @attention Use Bzip2 decompression when filename has extension .bz2
 * 
 * @param filename a file name.
 * @param obj an object that supports the iostream input operator.
 * @return an error message or NULL.
 */
template<class T>
const char* readfile(const std::string& filename, T& obj)
{
	const char* msg;
	std::ifstream fin;
	if( (msg=openfile(fin, filename)) != 0 ) return msg;
	if( (msg=readfile(fin, obj, filename)) != 0 )
	{
		fin.close();
		return msg;
	}
	else fin.close();
	return 0;
}
/** @brief Reads an object from a file.
 * 
 * Opens, reads and closes a file testing for several types of errors. If
 *  the file is not found in the current directoy, this function will search
 *  the list of search paths.
 * 
 * @attention Use Bzip2 decompression when filename has extension .bz2
 * 
 * @param filename a file name.
 * @param obj an object that supports the iostream input operator.
 * @param beg an iterator to the start of a collection of file paths.
 * @param end an iterator to the end of a collection of file paths.
 * @return an error message or NULL.
 */
template<class T, class I>
const char* readfile(const std::string& filename, T& obj, I beg, I end)
{
	const char* msg;
	std::ifstream fin;
	if( (msg=openfile(fin, filename, beg, end)) != 0 ) return msg;
	if( (msg=readfile(fin, obj, filename)) != 0 )
	{
		fin.close();
		return msg;
	}
	else fin.close();
	return 0;
}

/** @brief Reads a file to a string buffer.
 * 
 * Opens, reads and closes a file testing for several types of errors. The file is
 *  read into a string buffer.
 *
 * @param filename a file name.
 * @param buffer a string buffer.
 * @return an error message or NULL.
 */
inline const char* readfile2buffer(const std::string& filename, std::string& buffer)
{
	const char* msg;
	std::ifstream fin;
	if( (msg=openfile(fin, filename)) != 0 ) return msg;
	std::string buf;
	buffer="";
	while( !fin.eof() ) 
	{
		std::getline(fin, buf);
		buffer+=buf;
		buffer+="\n";
	}
	if( fin.fail() )
	{
		fin.close();
		//return ERRORMSG("Failed to read file: " << filename);
	}
	else fin.close();
	return 0;
}
/** @brief Writes an object to an output stream.
 * 
 * Writes an object to the specified output stream. If the file has a bz2 extension,
 * then the file is written with bzip2 compression.
 * 
 * @attention Can use Bzip2 compression.
 *
 * @param out an output stream.
 * @param obj an object.
 * @param filename a filename (to test the extension).
 * @return an error message or NULL.
 */
template<class T>
const char* writefile(std::ostream& out, const T& obj, std::string filename="stdout")
{
	if( std::string(ext_name(filename.c_str()) ) == "bz2" )
	{
		bzip2_stream::bzip2_ostream zip( out );
		zip << obj;
		if( zip.fail() )
		{
			if( obj.errormsg() != 0 ) return obj.errormsg();
			return ERRORMSG("Failed to write bzip2 file: " << filename);
		}
	}
	else out << obj;
	if( out.fail() )
	{
		if( obj.errormsg() != 0 ) return obj.errormsg();
		return ERRORMSG("Failed to write file: " << filename);
	}
	return 0;
}
/** @brief Writes an object to a file.
 * 
 * Opens, writes and closes a file writing an object that supports the output 
 * stream operator.
 *
 * @attention Use Bzip2 compression when filename has extension .bz2
 * 
 * @param filename a file name.
 * @param obj an object that supports the output operator.
 * @return an error message or NULL.
 */
template<class T>
const char* writefile(const std::string& filename, const T& obj)
{
	const char* msg;
	std::ofstream fout;
	if( (msg=openfile(fout, filename)) != 0 ) return msg;
	if( (msg=writefile(fout, obj, filename)) != 0 )
	{
		fout.close();
		return msg;
	}
	else fout.close();
	return 0;
}
/** @brief Writes an object to a file.
 * 
 * Opens (see openfile) and writes to a file while testing for several types of errors. The object
 *  passed can have any type but must support the iostream output operator (<<). It also writes a single string to
 *  the first line of the file.
 * 
 * @attention Use Bzip2 compression when filename has extension .bz2
 * 
 * @todo test if line has \n characters
 *
 * @param filename a file name.
 * @param obj an object that supports the output stream operator.
 * @param line a string to be written to the first line of the file.
 * @return an error message or NULL.
 */
template<class T>
const char* writefile_line(const std::string& filename, const T& obj, const std::string& line)
{
	const char* msg;
	std::ofstream fout;
	if( (msg=openfile(fout, filename)) != 0 ) return msg;
	fout << line << "\n";
	if( (msg=writefile(fout, obj, filename)) != 0 )
	{
		fout.close();
		return msg;
	}
	else fout.close();
	return 0;
}



#endif


