/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * fileutil.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

/** @file fileutil.cpp
 * @brief Contains the implementation of the file utility.
 * 
 * This source file contains implementation source for file
 * utility functions.
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#include "fileutil.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>

#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#endif


const char* home_path()
{
	return getenv(HOME_PTH);
}

static char buf[FILENAME_MAX];
const char* join_file(const char* path, const char* file)
{
	if( path == 0 ) return file;
	if( file == 0 ) return path;
	size_t len = strlen(path);
	if( len == 0 ) return file;
	std::strcpy(buf, path);
	if( path[len-1] != PATH_SEP && file[0] != PATH_SEP ) 
	{
		buf[len] = PATH_SEP;
		len++;
	}
	std::strcpy(buf+len, file);
	return buf;
}
const char* remove_before_first(const char* file, char ch, bool flag)
{
	const char* progname;
	if ((progname = std::strrchr(file, ch)) == 0 || *++progname == '\0') 
	{
		if( !flag )
		{
			buf[0] = '\0';
			progname = buf;
		}
		else progname=file;
	}
	if( *progname == PATH_SEP )
	{
		buf[0] = '\0';
		progname = buf;
	}
	return progname;
}

//#include <iostream>
const char* remove_after_first(const char* file, char ch, int o=0)
{
	const char* tmp = std::strrchr(file, ch);
	if( tmp == 0 ) 
	{
		size_t n = strlen(file);
		std::strncpy(buf+o, file,n);
		buf[n+o] = '\0';
		return buf;
	}
	size_t n = size_t(tmp-file);
	std::strncpy(buf+o, file, n);
	buf[n+o] = '\0';
	return buf;
}
const char* base_name(const char* file)
{
	return remove_before_first(file, PATH_SEP, true);
}

const char* ext_name(const char* file)
{
	return remove_before_first(file, '.', false);
}

const char* strip_ext(const char* file)
{
	return remove_after_first(file, '.');
}
const char* replace_ext(const char* file, const char* ext)
{
	remove_after_first(file, '.');
	if(*buf == '\0' || *buf == '.' ) std::strcpy(buf, file);
	int n = strlen(buf);
	if( buf[n-1] != '.' && ext[0] != '.' ) 
	{
		buf[n] = '.';
		n++;
	}
	std::strncpy(buf+n, ext, strlen(ext));
	buf[n+strlen(ext)] = '\0';
	return buf;
}

const char* dir_name(const char* file)
{
	return remove_after_first(file, PATH_SEP);
}

const char* program_name(const char* pfx, const char* name)
{
	size_t len = std::strlen(pfx);
	std::strcpy(buf, pfx);
	std::strcpy(buf+len, base_name(name));
	return buf;
}

const char* stripall(const char* file)
{
	return strip_ext(base_name(file));
}

bool isstdin()
{
#ifdef _WIN32
	return !_isatty(_fileno(stdin));
#else
	return !isatty(fileno(stdin));
#endif
}

const char* newfilename(const char* path, const char* file, const char* ext)
{
	file = base_name(file);
	int n = strlen(path);
	std::strncpy(buf, path, n);
	if( buf[n-1] != PATH_SEP )
	{
		buf[n] = PATH_SEP; 
		n++;
	}
	buf[n] = '\0';
	file = remove_after_first(file, '.', n);
	n=strlen(buf);
	if( buf[n-1] != '.' && ext[0] != '.' ) 
	{
		buf[n] = '.';
		n++;
	}
	std::strncpy(buf+n, ext, strlen(ext));
	buf[n+strlen(ext)] = '\0';
	return buf;
}
