/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * fileutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_FILEUTIL_H
#define _EXEGETE_FILEUTIL_H


/** @file fileutil.h
 * @brief Defines a set of file utility functions.
 * 
 * This header file contains a set of convenience functions to handle file names.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#ifdef _WIN32
/** A pathform dependent path pointing to the user's home directory (windows).
 */
#define HOME_PTH "%HOMEPATH%"
/** A platform dependent path separator (windows)
 */
#define PATH_SEP '\\'
/** A platform dependent file separator (windows)
 */
#define FILE_SEP ';'
#else
/** A pathform dependent path pointing to the user's home directory (unix).
 */
#define HOME_PTH "$HOME"
/** A platform dependent path separator (unix)
 */
#define PATH_SEP '/'
/** A platform dependent file separator (unix)
 */
#define FILE_SEP ':'
#endif

/** @brief Replaces an extension on a filename.
 * 
 * @param file the filename.
 * @param ext the new extension.
 * @return filename with new extension.
 */
const char* replace_ext(const char* file, const char* ext);
/** @brief Gets the homepath in a platform independent manner.
 * 
 * @warning Only tested on windows and linux.
 * 
 * @return user's homepath.
 */
const char* home_path();
/** @brief Join a path and a filename/path in a platform independent manner.
 * 
 * @param path a path.
 * @param file a file/path.
 * @return joined path and file using platform dependent separator.
 */
const char* join_file(const char* path, const char* file);
/** @brief Replace extension and path.
 * 
 * Strips directory and extension then appends a new directory and extension
 * to the base file name.
 *
 *  @param path a path.
 *  @param file a filename.
 *  @param ext an extension.
 *  @return a filename with a new path and extension.
 */
const char* newfilename(const char* path, const char* file, const char* ext);
/** @brief Strips directory and extension.
 *  
 *  e.g. <example>/home/temp.cfg -> temp</example>.
 *
 *  @param file a filename.
 *  @return basename without extension or path.
 */
const char* stripall(const char* file);
/** @brief Determines the basename of a filename.
 *  
 *  e.g. <example>/home/temp.cfg -> temp.cfg</example>.
 *
 * @param file filename.
 * @return the basename.
 */
const char* base_name(const char* file);
/** @brief Determines the directory path of a filename.
 *
 *  e.g. <example>/home/temp.cfg -> /home</example>.
 * 
 * @param file a filename.
 * @return the directory path.
 */
const char* dir_name(const char* file);
/** @brief Strips an extension from a filename.
 * 
 *  e.g. <example>temp.cfg -> temp</example>.
 *
 * @param file a filename.
 * @return the filename minus the extension.
 */
const char* strip_ext(const char* file);
/** @brief Determines the extension of a file string.
 * 
 *  e.g. <example>temp.cfg -> cfg</example>.
 *
 * @param file a filename.
 * @return the extension.
 */
const char* ext_name(const char* file);
/** @brief basename of program.
 * 
 * Determines the basename of a filename and prefixes the output filename.
 * 
 * @param pfx the prefix to the output.
 * @param name a filename.
 * @return the basename with the new prefix.
 */
const char* program_name(const char* pfx, const char* name);
/** @brief Tests if the file descriptor is active.
 *
 * @return true if a file was redirected to the stdin.
 */
bool isstdin();

#endif

