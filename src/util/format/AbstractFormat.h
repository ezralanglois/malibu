/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AbstractFormat.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ABSTRACTFORMAT_H
#define _EXEGETE_ABSTRACTFORMAT_H
#include <iostream>
#include <sstream>


/** @file AbstractFormat.h
 * @brief Defines an abstract format class.
 * 
 * This file contains the AbstractFormat class. An abstract format provides
 * a common interface for a stream format parser.
 * 
 * Every format provides the following:
 * 
 *	- An input stream operator (maybe a dummy).
 *	- An output stream operator (maybe a dummy).
 *	- Test the format of the stream.
 *	- An error message. 
 *	- Name of a format.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines an abstract interface to a format.
	 * 
 	 * This class template defines an abstract format.
	 */
	class AbstractFormat
	{
	public:
		/** Constructs an abstract format.
		 */
		AbstractFormat() : errmsg(0)
		{
		}
		/** Destructs an abstract format.
		 */
		virtual ~AbstractFormat()
		{
		}

	public:
		/** Reads an abstract format from the input stream.
		 *
		 * @param in an input stream.
		 * @param format an abstract format.
		 * @return an input stream.
		 */
		friend std::istream& operator>>(std::istream& in, AbstractFormat& format)
		{
			format.errmsg = format.read(in);
			if( format.errmsg != 0 ) in.setstate( std::ios::failbit );
			return in;
		}
		/** Writes an abstract format to the output stream.
		 *
		 * @param out an output stream.
		 * @param format an abstract format.
		 * @return an output stream.
		 */
		friend std::ostream& operator<<(std::ostream& out, const AbstractFormat& format)
		{
			format.errmsg = format.write(out);
			if( format.errmsg != 0 ) out.setstate( std::ios::failbit );
			return out;
		}
		/** Gets an error message otherwise NULL if no error has occurred.
		 *
		 * @return an error message.
		 */
		const char* errormsg()const
		{
			return errmsg;
		}

	protected:
		/** Parsers a line from the input stream. 
		 *
		 * @param line a line from the stream.
		 * @return an error message.
		 */
		virtual const char* read(const std::string& line)
		{
			std::istringstream iss(line);
			return read(iss);
		}
		/** Tests if stream holds this format.
		 *
		 * @param line a line from the input stream.
		 * @return true if the stream holds the format.
		 */
		virtual bool detect(const std::string& line)const
		{
			return true;
		}
		/** Reads an object from the stream with this format.
		 * 
		 * @warning Only returns an error, need to make concrete in a subclass.
		 *
		 * @param in an input stream.
		 * @return an error message or NULL.
		 */
		virtual const char* read(std::istream& in)
		{
			return ERRORMSG("Read not support for " << name());
		}
		/** Writes a format to the output stream.
		 *
		 * @warning Only returns an error, need to make concrete in a subclass.
		 * 
		 * @param out an output stream.
		 * @return an error message or NULL.
		 */
		virtual const char* write(std::ostream& out)const
		{
			return ERRORMSG("Write not support for " << name());
		}

	public:
		/** Gets the name of the format.
		 *
		 * @return the format name.
		 */
		virtual std::string name()const
		{
			return "";
		}

	private:
		mutable const char* errmsg;
	};

};


#endif


