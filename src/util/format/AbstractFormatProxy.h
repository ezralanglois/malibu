/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AbstractFormatProxy.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ABSTRACTFORMATPROXY_H
#define _EXEGETE_ABSTRACTFORMATPROXY_H
#include "AbstractFormat.h"

/** @file AbstractFormatProxy.h
 * @brief Defines an abstract format proxy class.
 * 
 * This file contains the AbstractFormatProxy class template.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a proxy to a format with different read/write class.
	 * 
 	 * This class template defines an abstract format proxy in the case
	 * where the read object differs from the write object.
	 */
	template<class T, class U=T>
	class AbstractFormatProxy : public AbstractFormat
	{
	public:
		/** Defines a value type. **/
		typedef T value_type;
		/** Defines a pointer. **/
		typedef T* pointer;
		/** Defines a pointer. **/
		typedef const U* const_pointer;
		/** Defines a reference. **/
		typedef T& reference;
		/** Defines a constant reference. **/
		typedef const U& const_reference;
	public:
		/** Constructs a default abstract format proxy.
		 */
		AbstractFormatProxy() : pRead(0), pWrite(0)
		{
		}
		/** Constructs an abstract format proxy, sets the object for
		 *  both reading and writing.
		 *
		 * @param ref an object.
		 */
		AbstractFormatProxy(reference ref) : pRead(&ref), pWrite(&ref)
		{
		}
		/** Constructs an abstract format proxy, sets the object only for
		 *  writing.
		 *
		 * @param ref an object.
		 */
		AbstractFormatProxy(const_reference ref) : pRead(0), pWrite(&ref)
		{
		}
		/** Destructs an abstract format proxy.
		 */
		virtual ~AbstractFormatProxy()
		{
		}

	public:
		/** Sets the object for both read/write.
		 *
		 * @param ref reference to an object.
		 * @return reference to this object.
		 */
		AbstractFormatProxy& operator()(reference ref)
		{
			pRead = &ref;
			pWrite = &ref;
			return *this;
		}
		/** Sets the object for writing.
		 *
		 * @param ref reference to an object.
		 * @return reference to this object.
		 */
		const AbstractFormatProxy& operator()(const_reference ref)const
		{
			pWrite = &ref;
			return *this;
		}

	protected:
		/** Reads a format from the input stream.
		 *
		 * @param in an input stream.
		 * @return an error message or NULL.
		 */
		const char* read(std::istream& in)
		{
			if( pRead == 0 ) return ERRORMSG("No object to read");
			return read(in, *pRead);
		}
		/** Writes a format to the output stream.
		 *
		 * @param out an output stream.
		 * @return an error message or NULL.
		 */
		const char* write(std::ostream& out)const
		{
			if( pWrite == 0 ) return ERRORMSG("No object to write");
			return write(out, *pWrite);
		}

	protected:
		/** Reads a format from the input stream.
		 *
		 * @warning Only returns an error, need to make concrete in a subclass.
		 * 
		 * @param ref an object.
		 * @param in an input stream.
		 * @return an error message or NULL.
		 */
		virtual const char* read(std::istream& in, reference ref)
		{
			return ERRORMSG("Read not support for " << AbstractFormat::name());
		}
		/** Writes a format to the output stream.
		 *
		 * @warning Only returns an error, need to make concrete in a subclass.
		 * 
		 * @param ref an object.
		 * @param out an output stream.
		 * @return an error message or NULL.
		 */
		virtual const char* write(std::ostream& out, const_reference ref)const
		{
			return ERRORMSG("Writing not support for " << AbstractFormat::name());
		}

	private:
		pointer pRead;
		mutable const_pointer pWrite;
	};
	/** @brief Defines a proxy to a format with same read/write class.
	 * 
 	 * This class template defines an abstract format proxy in the case
	 *  where both the read object and write object are the same.
	 */
	template<class T>
	class AbstractFormatProxy<T,T> : public AbstractFormat
	{
	public:
		/** Defines a value type. **/
		typedef T value_type;
		/** Defines a pointer. **/
		typedef T* pointer;
		/** Defines a pointer. **/
		typedef const T* const_pointer;
		/** Defines a reference. **/
		typedef T& reference;
		/** Defines a constant reference. **/
		typedef const T& const_reference;
	public:
		/** Constructs a default abstract format proxy.
		 */
		AbstractFormatProxy() : ptr(0)
		{
		}
		/** Constructs an abstract format proxy.
		 *
		 * @param ref an object.
		 */
		AbstractFormatProxy(reference ref) : ptr(&ref)
		{
		}
		/** Destructs an abstract format proxy.
		 */
		virtual ~AbstractFormatProxy()
		{
		}

	public:
		/** Assigns an object to the format proxy.
		 *
		 * @param ref an object.
		 * @return reference to this object.
		 */
		AbstractFormatProxy& operator()(reference ref)
		{
			ptr = &ref;
			return *this;
		}
		/** Assigns an object to the format proxy.
		 *
		 * @param ref reference to an object.
		 * @return reference to this object.
		 */
		const AbstractFormatProxy& operator()(const_reference ref)const
		{
			ptr = &ref;
			return *this;
		}

	protected:
		/** Reads a format from the input stream.
		 *
		 * @param in an input stream.
		 * @return an error message or NULL.
		 */
		const char* read(std::istream& in)
		{
			if( ptr == 0 ) return ERRORMSG("No object to read");
			return read(in, const_cast<reference>(*ptr));
		}
		/** Writes a format to the output stream.
		 *
		 * @param out an output stream.
		 * @return an error message or NULL.
		 */
		const char* write(std::ostream& out)const
		{
			if( ptr == 0 ) return ERRORMSG("No object to write");
			return write(out, *ptr);
		}

	protected:
		/** Reads a format from the input stream.
		 *
		 * @param ref an object.
		 * @param in an input stream.
		 * @return an error message or NULL.
		 */
		virtual const char* read(std::istream& in, reference ref)
		{
			return ERRORMSG("Read not support for " << AbstractFormat::name());
		}
		/** Writes a format to the output stream.
		 *
		 * @param ref an object.
		 * @param out an output stream.
		 * @return an error message or NULL.
		 */
		virtual const char* write(std::ostream& out, const_reference ref)const
		{
			return ERRORMSG("Writing not support for " << AbstractFormat::name());
		}

	private:
		mutable const_pointer ptr;
	};

};


#endif


