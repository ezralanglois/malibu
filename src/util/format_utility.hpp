/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * format_utility.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */
#ifndef _EXEGETE_FORMAT_UTILITY_HPP
#define _EXEGETE_FORMAT_UTILITY_HPP


/** @file format_utility.hpp
 * @brief Contains format utilities.
 * 
 * This file contains format utilities.
 * 
 * @ingroup ExegeteDataset
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

namespace exegete
{
	/** @brief Defines a base format utility
	 * 
	 * This class template defines an base format utility.
	 */
	class base_format_utility
	{
	public:
		base_format_utility() : errmsg(0)
		{
		}
		
	public:
		/** Gets an error message.
		 *
		 * @return an error message or NULL.
		 */
		const char* errormsg()const
		{
			return errmsg;
		}
		/** Sets an error message and flags the stream as failed.
		 *
		 * @param s a stream .
		 * @param m an error message or NULL.
		 */
		void errormsg(std::ios& s, const char* m)const
		{
			if( m != 0 )
			{
				errmsg = m;
				s.setstate( std::ios::failbit );
			}
		}
		/** Sets an error message and flags the stream as failed.
		 *
		 * @param i a stream.
		 * @param m an error message or NULL.
		 * @return a stream.
		 */
		std::istream& fail(std::istream& i, const char* m)const
		{
			errormsg(i, m);
			return i;
		}
		/** Sets an error message and flags the stream as failed.
		 *
		 * @param o a stream.
		 * @param m an error message or NULL.
		 * @return a stream.
		 */
		std::ostream& fail(std::ostream& o, const char* m)const
		{
			errormsg(o, m);
			return o;
		}
		
	private:
		mutable const char* errmsg;
	};
};

#endif






