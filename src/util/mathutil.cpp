/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mathutil.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


/** @file mathutil.cpp
 * @brief Contains the implementation of the math utility.
 * 
 * This source file contains implementation source for random
 * functions of math utility.
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */
#include "debugutil.h"
#include "mathutil.h"
#include "MersenneTwister.h"


static MTRand mtrand;

double random_double()
{
	return mtrand.rand();
}

void seed_random(unsigned long n)
{
	mtrand.seed(n);
}

unsigned long random_int(unsigned long n)
{
	return mtrand.randInt(n);
}

long random()
{
	return mtrand.randInt();
}
//lnGamma
double log_to_binomial(double u, double l)
{
	ASSERT(u >= l);
	return ( log_factorial(u) - log_factorial(l) - log_factorial(u-l) ) / math::log2;
}

double log_to_multinomial(double u, const double* ls, unsigned int n)
{
	double sum = 0.0;
	for(unsigned int i=0;i<n;++i)
	{
		ASSERTMSG(!((ls[i]-u) > math::eps), u << " > " << ls[i]);
		sum = sum + log_factorial(ls[i]);
	}
	return (log_factorial(u) - sum) / math::log2;
}

double p1evl( double x, const double* coef, unsigned int n )
{
  double y = x + coef[0];
  for(unsigned int i=1;i<n;++i) y = y*x+coef[i];
  return y;
}

double polevl( double x, const double* coef, unsigned int n )
{
  double y = coef[0];
  for(unsigned int i=1; i<=n; i++) y = y*x+coef[i];
  return y;
}

double log_gamma(double x)
{
    double A[] = {
      8.11614167470508450300E-4,
      -5.95061904284301438324E-4,
      7.93650340457716943945E-4,
      -2.77777777730099687205E-3,
      8.33333333333331927722E-2
    };
    double B[] = {
      -1.37825152569120859100E3,
      -3.88016315134637840924E4,
      -3.31612992738871184744E5,
      -1.16237097492762307383E6,
      -1.72173700820839662146E6,
      -8.53555664245765465627E5
    };
    double C[] = {
      //1.00000000000000000000E0,
      -3.51815701436523470549E2,
      -1.70642106651881159223E4,
      -2.20528590553854454839E5,
      -1.13933444367982507207E6,
      -2.53252307177582951285E6,
      -2.01889141433532773231E6
    };
    double p, q, w, z;
    if( x < -34.0 )
    {
    	q = -x;
    	w = log_gamma(q);
        p = std::floor(q);
        ASSERT(p != q);
        z = q - p;
        if( z > 0.5 ) 
        {
        	p += 1.0;
        	z = p - q;
        }
        z = q * std::sin( PI * z );
        ASSERT(z != 0.0);
        z = math::logpi - std::log( z ) - w;
        return z;
    }
    if( x < 13.0 )
    {
    	 z = 1.0;
    	 while( x >= 3.0 )
    	 {
    		 x -= 1.0;
    		 z *= x;
    	 }
    	 while( x < 2.0 )
    	 {
    		 ASSERT(x != 0.0);
    		 z /= x;
    		 x += 1.0;
    	 }
    	 if( z < 0.0 ) z = -z;
    	 if( x == 2.0 ) return std::log(z);
    	 x -= 2.0;
    	 p = x * polevl( x, B, 5 ) / p1evl( x, C, 6);
    	 return ( std::log(z) + p );
    }
    ASSERT(x < DBL_MAX);
    q = ( x - 0.5 ) * std::log(x) - x + 0.91893853320467274178;
    if( x > 1.0e8 ) return q;
    p = 1.0/(x*x);
    if( x >= 1000.0 ) q += ((7.9365079365079365079365e-4 * p - 2.7777777777777777777778e-3) * p + 0.0833333333333333333333) / x;
    else q += polevl( p, A, 4 ) / x;
    return q;
}

