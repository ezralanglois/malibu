/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * mathutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */



#ifndef _EXEGETE_MATHUTIL_H
#define _EXEGETE_MATHUTIL_H

/** @file mathutil.h
 * @brief Defines a set of math utility functions.
 * 
 * This header file contains a set of math functions.
 *
 * This header file contains a set functions for:
 *	- testing nan
 *	- generating a random number
 *	- seeding the random number generator
 *	- safely dividing a number
 *	- squaring a number
 *	- calculating trapezodial area
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


#include <float.h>
#include <cmath>

#if  defined(_WIN32) //|| defined(__MINGW32__) //_MSC_VER
#ifndef _EXEGETE_ISNAN
#define _EXEGETE_ISNAN
namespace std
{
	/** @brief Redefines the visual studio _isnan to the standard linux isnan.
	 * 
	 * @param val a value to test.
	 * @return true if the value is NAN.
	 */
	inline int isnan(double val)
	{
		return _isnan(val);
	}
};
#endif
#endif

#ifndef EXP
/** This string macro defines the exponential constant. **/
#define EXP 2.7182818284590452354
#endif

#ifndef PI
#define PI 3.14159265358979323846
#endif


namespace math
{
	/** Defines a log base 2 constant. **/
	const double log2 = std::log(2.0);
	/** Defines a double deviation. **/
	const double eps = 1e-6;
	/** Defines a log of PI. **/
	const double logpi = 1.14472988584940017414;
};

/** @brief Gets a random integer within the range [0,n]. 
 * This method uses the Mersenne Twister (MTRand).
 *
 * @param n a range for the random number.
 * @return a random number between [0,n].
 */
unsigned long random_int(unsigned long n);
/** @brief Gets a random floating-point number. 
 * This method uses the Mersenne Twister (MTRand).
 *
 * @return a random floating-point number.
 */
double random_double();
/** @brief Seeds the random number generator. 
 * This method uses the Mersenne Twister (MTRand).
 * 
 * @param n an integer seed.
 */
void seed_random(unsigned long n);


/** @brief Safely divides two numbers checking if the denominator is zero.
 *
 * \f$\frac{num}{den}\f$
 *
 * @param num the numerator.
 * @param den the denominator.
 * @return the resulting value.
 */
inline double divide(double num, double den)
{
	return den != 0.0 ? num/den : 0.0;
}

/** @brief Calculates the square of a double.
 *
 * @param v1 a double value.
 * @return a squared double value.
 */
inline double square(double v1)
{
	return v1*v1;
}
/** @brief Calculates the area under a trapezoid.
 *
 * \f$x_2-x_1 * \frac{y_2+y_1}{2}\f$
 * 
 * @param x1 first x-coordinate.
 * @param x2 second x-coordinate.
 * @param y1 first y-coordinate.
 * @param y2 second y-coordinate.
 * @return area under trapezoid.
 */
inline double trap_area(double x1, double x2, double y1, double y2)
{
	return std::abs( x1 - x2 ) * ( 0.5 * (y1 + y2) );
}
/** Test if two double values are equal within some margin of error.
 * 
 * @param val1 first double value.
 * @param val2 second double value.
 * @return true is approximately equal.
 */
inline bool double_eq(double val1, double val2, double _eps=math::eps)
{
	return (val1 - val2 < _eps) && (val2 - val1 < _eps); 
}

/** Calculates log base 2 of a number.
 *
 * @param val a number.
 * @return log2 of number. 
 */
inline double log2(double val)
{
	return std::log(val) / math::log2;
}
/** Calculates log base 2 of binomial coefficient using the gamma function.
 * 
 * @param u upper part of binomial coefficient.
 * @param l lower part of binomial coefficient.
 * @return base 2 log of binomial coefficient u over l.
 */
double log_to_binomial(double u, double l);
/** Calculates log base 2 of multinomial coefficient using the gamma function.
 * 
 * @param u upper part of binomial coefficient.
 * @param ls lower parts of binomial coefficient.
 * @param n number of lower parts.
 * @return base 2 log of multinomial coefficient u over ls.
 */
double log_to_multinomial(double u, const double* ls, unsigned int n);
/** Calculates the log of gamma function.
 * 
 * @param x a value.
 * @return log of gamma function.
 */
double log_gamma(double x);
/**
 * Evaluates the following polynomial of degree n at z.
 * 
 * \f$ y = C_0 + C_1x + C_2x^2 + ... + C_nx^n \f$
 *
 * @note Coefficients are stored in reverse order
 * 
 * @param x argument to the polynomial.
 * @param coef the coefficients of the polynomial.
 * @param n the degree of the polynomial.
 */
double p1evl(double x, const double* coef, unsigned int n);
/**
 * Evaluates the following polynomial of degree n at x.
 * 
 * \f$ y = C_0 + C_1x + C_2x^2 + ... + C_nx^n \f$
 *
 * @note Coefficients are stored in reverse order.
 *
 * @param x argument to the polynomial.
 * @param coef the coefficients of the polynomial.
 * @param n the degree of the polynomial.
 * @return evaluated polynomical.
 */
double polevl( double x, const double* coef, unsigned int n );

/** Calculates the log of the factorial using the gamma function.
 * 
 * @param x a value.
 * @return log of factorial.
 */
inline double log_factorial(double x)
{
	return log_gamma(x+1);
}

#endif

