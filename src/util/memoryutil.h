/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * memoryutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_MEMORYUTIL_H
#define _EXEGETE_MEMORYUTIL_H
#include "typeutil.h"
#include <cstdlib>
#ifndef _WIN32
#include <malloc.h>
#endif

/** @file memoryutil.h
 * @brief Defines a set of string memory utility classes and functions.
 * 
 * This header file contains a class template and a set of function templates to deal with allocating
 * memory. The functions provide an interface to the MemoryUtil template class. The MemoryUtil class
 * has a specialization to determine at compile time whether the type is plain Ol' data (POD) or a
 * C++ class.
 * 
 * If the data is POD, then the following routines are mapped:
 *  - setsize --> malloc
 *  - resize --> realloc
 *  - erase --> free
 * 
 * If the data is a C++ class (non-POD), then the following routines are mapped:
 *  - setsize --> new[]
 *  - resize --> delete[]
 *  - erase --> new[] and delete[]
 * 
 * A data type is flagged POD at compile-time by the TypeUtil class template.
 *
 * @ingroup ExegeteUtil
 * @see TypeUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @brief Defines memory allocation for a primative type.
 * 
 * This class template defines a set of functions to allocate, reallocate and 
 * deallocate memory. It also provides a routine to push an element onto a list. This template
 * specialization is invoked when the type is primative or POD.
 */
template<class T, bool pod=TypeUtil<T>::ispod>
struct MemoryUtil
{
	/** Allocates memory of a specific size (number of elements in an array) 
	 *  and returns a pointer to the memory location.
	 *
	 * @param len number of elements in an array.
	 * @return a pointer to the new memory location or NULL if the allocate function failed.
	 */
	inline static T* setsize(size_t len)
	{
		return (T*)std::malloc(len*sizeof(T));
	}
	/** Allocates memory of a specific size (number of elements in an array) 
	 *  and returns a pointer to the memory location.
	 *
	 * @param len number of elements in an array.
	 * @return a pointer to the new memory location or NULL if the allocate function failed.
	 */
	inline static T* calloc(size_t len)
	{
		return (T*)std::calloc(len, sizeof(T));
	}
	/** Reallocates memory of a specific size to a new size. It will take an existing memory location
	 *  and increase its size. 
	 *
	 * @param ptr pointer to exisiting memory location.
	 * @param len new number of elements in array.
	 * @param old dummy variable (unnecessary for primative types).
	 * @return a pointer to the new memory location.
	 */
	inline static T* resize(T* ptr, size_t len, size_t old=0)
	{
		return (T*)std::realloc(ptr, len*sizeof(T));
	}
	/** Deallocates memory of a specific size. 
	 * 
	 * @warning It assumes the pointer is to an array (was allocated by setsize).
	 *
	 * @param ptr pointer to exisiting memory location.
	 */
	inline static void erase(T*& ptr)
	{
		if( ptr != 0 ) std::free(ptr);
		ptr = 0;
	}
	/** Reallocates memory of in blocks and only reallocates new memory when the 
	 *  block size has been exceeded.
	 *
	 * @param ptr pointer to exisiting memory location.
	 * @param len current amount of memory allocated.
	 * @param oldlen current memory requirement.
	 * @param inc size of block.
	 * @return a pointer to the new memory location.
	 */
	inline static T* resize(T* ptr, size_t& len, size_t oldlen, size_t inc)
	{
		if( oldlen >= len )
		{
			len = oldlen + inc;
			return resize(ptr, len);
		}
		return ptr;
	}
	/** Allocates memory for an array one element at a time.
	 *  
	 *
	 * @param ptr pointer to exisiting memory location.
	 * @param len current amount of memory allocated (incremented at end of function).
	 * @param val the value of the last block (newly allocated).
	 * @return a pointer to the new memory location.
	 */
	inline static T* push_back(T* ptr, size_t& len, const T& val)
	{
		ptr = resize(ptr, len+1);
		ptr[len] = val;
		++len;
		return ptr;
	}
};

/** @brief Defines memory allocation for a non-primative type.
 * 
 * This class template defines a set of functions to allocate, reallocate and 
 *  deallocate memory. It also provides a routine to push an element onto a list.
 *  In the case of non-pod type, the normal resize function is not defined. This template
 *  specialization is invoked when the type is not primative.
 */
template<class T>
struct MemoryUtil<T, false>
{
	/** Allocates memory of a specific size (number of elements in an array) 
	 *  and returns a pointer to the memory location.
	 *
	 * @param len number of elements in array.
	 * @return a pointer to the new memory location or NULL if the allocate function failed.
	 */
	inline static T* setsize(size_t len)
	{
		return new T[len];
	}
	/** Allocates memory of a specific size (number of elements in an array) 
	 *  and returns a pointer to the memory location.
	 *
	 * @param len number of elements in an array.
	 * @return a pointer to the new memory location or NULL if the allocate function failed.
	 */
	inline static T* calloc(size_t len)
	{
		return new T[len];
	}
	/** Reallocates memory of a specific size to a new size. It will take an existing memory location
	 *  and increase its size.
	 * 
	 * @warning The old parameter is only required for non-primative types.
	 *
	 * @param ptr pointer to exisiting memory location.
	 * @param len new number of elements in array.
	 * @param old old length (necessary for non-primative types).
	 * @return a pointer to the new memory location.
	 */
	inline static T* resize(T* ptr, size_t len, size_t old)
	{
		if( len > old )
		{
			T* tmp = setsize(len);
			if( ptr != 0 )
			{
				std::copy(ptr, ptr+old, tmp);
				erase(ptr);
			}
			return tmp;
		}
		return ptr;
	}
	/** Deallocates memory of a specific size. It assumes the pointer is to an array.
	 *
	 * @param ptr pointer to exisiting memory location.
	 */
	inline static void erase(T*& ptr)
	{
		delete[] ptr;
		ptr = 0;
	}
	/** Reallocates memory of in blocks and only reallocates new memory when the 
	 *  block size has been exceeded.
	 *
	 * @param ptr pointer to exisiting memory location.
	 * @param len current amount of memory allocated.
	 * @param oldlen current memory requirement.
	 * @param inc size of block.
	 * @return a pointer to the new memory location.
	 */
	inline static T* resize(T* ptr, size_t& len, size_t oldlen, size_t inc)
	{
		if( oldlen > len )
		{
			T* old = (T*)std::realloc(ptr, (oldlen + inc)*sizeof(T));
			new(old+len) T;
			len = oldlen + inc;
		}
		return ptr;
	}
	/** Allocates memory for an array one element at a time.
	 * 
	 * @param ptr pointer to exisiting memory location.
	 * @param len current amount of memory allocated (incremented at end of function).
	 * @param val the value of the last block (newly allocated).
	 * @return a pointer to the new memory location.
	 */
	inline static T* push_back(T* ptr, size_t& len, const T& val)
	{
		ptr = resize(ptr, len, len+1, 1);
		ptr[len] = val;
		len++;
		return ptr;
	}
};

/** @brief Allocates memory for an array one element at a time.
 *  
 *
 * @see MemoryUtil::push_back
 * @param ptr pointer to exisiting memory location.
 * @param len current amount of memory allocated (incremented at end of function).
 * @param val the value of the last block (newly allocated).
 * @return a pointer to the new memory location.
 */
template<class T>
void push_back(T*& ptr, size_t& len, const T& val)
{
	ptr=MemoryUtil<T>::push_back(ptr, len, val);
}
/** @brief Reallocates memory of a specific size.
 * 
 * Reallocates memory of a specific size to a new size. It will take an existing memory location
 * and increase its size. 
 * 
 * @warning It is not defined for non-primative types.
 *
 * @see MemoryUtil::resize
 * @param ptr pointer to exisiting memory location.
 * @param len new number of elements in array.
 * @return a pointer to the new memory location.
 */
template<class T>
inline T* resize(T* ptr, size_t len)
{
	return MemoryUtil<T>::resize(ptr, len);
}
/** @brief Allocates memory of a specific size.
 * 
 * Allocates memory of a specific size to a new size and initalizes values to zero. 
 *
 * @see MemoryUtil::resize
 * @param ptr pointer to exisiting memory location.
 * @param len new number of elements in array.
 * @return a pointer to the new memory location.
 */
template<class T>
inline T* calloc(T* ptr, size_t len)
{
	return MemoryUtil<T>::calloc(len);
}
/** @brief Reallocates memory of a specific size.
 * 
 * Reallocates memory of a specific size to a new size. It will take an existing memory location
 * and increase its size.
 *
 * @see MemoryUtil::resize
 * @param ptr pointer to exisiting memory location.
 * @param len new number of elements in array.
 * @param old old length
 * @return a pointer to the new memory location.
 */
template<class T>
inline T* resize(T* ptr, size_t len, size_t old)
{
	return MemoryUtil<T>::resize(ptr, len, old);
}
/** @brief Allocates memory of a specific size.
 * 
 * Allocates memory of a specific size (number of elements in an array) 
 * and returns a pointer to the memory location.
 *
 * @param dummy a dummy pointer to determine type.
 * @param len number of elements in array.
 * @return a pointer to the new memory location or NULL if the allocate function failed.
 */
template<class T>
inline T* setsize(T* dummy, size_t len)
{
	return MemoryUtil<T>::setsize(len);
}
/** @brief Allocates memory of a specific size.
 * 
 * Allocates memory of a specific size (number of elements in an array) 
 * and returns a pointer to the memory location.
 *
 * @param len number of elements in array.
 * @return a pointer to the new memory location or NULL if the allocate function failed.
 */
template<class T>
inline T* setsize(size_t len)
{
	return MemoryUtil<T>::setsize(len);
}
/** @brief Reallocates memory of a specific size.
 * 
 * Reallocates memory of in blocks and only reallocates new memory when the 
 * block size has been exceeded.
 *
 * @param val pointer to exisiting memory location.
 * @param len a reference to the current amount of memory allocated.
 * @param n current memory requirement.
 * @param inc size of block.
 * @return a pointer to the new memory location.
 */
template<class T> 
inline T* resize(T* val, size_t& len, size_t n, size_t inc)
{
	return MemoryUtil<T>::resize(val, len, n, inc);
}
/** @brief Erases memory.
 * 
 * Deallocates memory of a specific size. It assumes the pointer is to an array.
 *
 * @param ptr pointer to exisiting memory location.
 */
template<class T>
inline void erase(T*& ptr)
{
	MemoryUtil<T>::erase(ptr);
}
/** @brief Erases a collection of memory.
 * 
 * Deallocates a range of memory pointed to by some iterator
 *
 * @param beg start iterator.
 * @param end end iterator.
 */
template<class I>
inline void erase(I beg, I end)
{
	for(;beg != end;++beg) ::erase(*beg);
}


#endif

