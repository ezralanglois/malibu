/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * strconvertutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_STRCONVERTUTIL_H
#define _EXEGETE_STRCONVERTUTIL_H
#include "typeutil.h"
#include <sstream>
#include <string>
#include <vector>
#include <cstring>

/** @file strconvertutil.h
 * @brief Defines a set of string conversion utility classes.
 * 
 * This header file contains two classe templates (and corresponding specializations) for string conversion 
 *  - From string to an object of some type.
 *  - To string from an object of some type.
 *
 * @ingroup ExegeteUtil
 * @see stringutil.h
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @brief Trims space on both sides of a std::string.
 * 
 * Trims space on both sides of a std::string.
 *
 * @param source a std::string to trim.
 * @return a trimed std::string.
 */
std::string trim(const std::string& source);

/** @brief Converts a string to a value of any type.
 * 
 * This class template defines a conversion from a c-string to an object of any type.
 */
template<class T>
struct String2Value
{
	/** Defines a TypeUtil as a type traits. **/
	typedef TypeUtil<T> type_traits;
	/** Converts from a c-string to an object of any type. The function uses TypeUtil 
	 *  to check if the conversion is valid. If the conversion is valid, then converts 
	 *  from c-string to value using std::istringstream.
	 *
	 * @param str a source c-string.
	 * @param val a destination object of any type.
	 * @param m a unused list of character separators.
	 * @param c clear the object.
	 * @return true if conversion is successful.
	 */
	static bool convert(const char* str, T& val, const char* m, bool c)
	{
		if( !type_traits::valid(str) ) return false;
		std::istringstream convert(str);
		convert >> val;
		if(!convert.eof() && convert.fail()) return false;
		return true;
	}
};
/** @brief Converts a string to a string.
 * 
 * This specialized class template defines a conversion from a const c-string to a std::string.
 */
template<>
struct String2Value< std::string >
{
	/** This typedef defines a TypeUtil of a std::string as type traits. **/
	typedef TypeUtil< std::string > type_traits;
	/** This function converts from a c-string to a std::string. It assigns 
     *  the c-string to the std::string.
	 *
	 * @param str a source c-string.
	 * @param val a destination std::string.
	 * @param m a unused list of character separators.
	 * @param c clear the object.
	 * @return true if conversion is successful.
	 */
	static bool convert(const char* str, std::string& val, const char* m, bool c)
	{
		if( c ) val=str;
		else val+=str;
		return true;
	}
};
/** @brief Converts a string to a const char*.
 * 
 * This specialized specialized class template defines a conversion from a const c-string to c-string.
 */
template<>
struct String2Value< const char* >
{
	/** Defines a TypeUtil of a std::string as type traits. **/
	typedef TypeUtil< const char* > type_traits;
	/** Converts from a c-string to a std::string. It assigns the c-string to 
	 * the std::string.
	 *
	 * @param str a source c-string.
	 * @param val a destination c-string.
	 * @param m a unused list of character separators.
	 * @param c clear the object.
	 * @return true if conversion is successful.
	 */
	static bool convert(const char* str, const char*& val, const char* m, bool c)
	{
		val=str;
		return true;
	}
};
/** @brief Converts a string to a char.
 * 
 * This specialized class template defines a conversion from a const c-string to char.
 */
template<>
struct String2Value< char >
{
	/** Defines a TypeUtil of a char as type traits. **/
	typedef TypeUtil< char > type_traits;
	/** Converts from a c-string to a char. It assigns the first character 
	 * of the c-string to the character. It also uses the TypeUtil class to check 
	 * if the string has a single character.
	 *
	 * @param str the source c-string.
	 * @param val the destination char.
	 * @param m a unused list of character separators.
	 * @param c clear the object.
	 * @return true if conversion is successful.
	 */
	static bool convert(const char* str, char& val, const char* m, bool c)
	{
		if( !type_traits::valid(str) ) return false;
		val=str[0];
		return true;
	}
};
/** @brief Converts a string to a pair (string, value of any type).
 * 
 * This specialized class template defines a conversion from a const c-string to a 
 * std::pair of objects, a std::string and an object of any type.
 */
template<class T>
struct String2Value< std::pair<std::string,T> >
{
	/** Defines a TypeUtil of any type T as type traits. **/
	typedef TypeUtil< T > type_traits;
	/** Converts from a const c-string to a std::pair of objects, a std::string and an object of any type. 
	 *  It splits the c-string using the first character in the list of token characters
	 *  and returns an error if the string cannot be split into two parts or the second part 
	 *  of the c-string cannot be converted to the correct type.
	 *
	 * @param str the source c-string.
	 * @param val the destination object pair.
	 * @param m a list of character separators.
	 * @param c clear the object.
	 * @return true if conversion is successful.
	 */
	static bool convert(const char* str, std::pair<std::string,T>& val, const char* m, bool c)
	{
		if( m == 0 || *m == '\0' ) return false;
		const char* pos = std::strrchr(str, m[0]);
		if( pos == 0 || *pos == '\0' ) return false;
		val.first = pos+1;
		if(!String2Value<T>::convert(val.first.c_str(), val.second, m, c)) return false;
		if( c ) val.first.assign(str, pos);
		else val.first.append(str, pos);
		return true;
	}
};
/** @brief Converts a string to a pair (value of any type, value of any type).
 * 
 * This specialized class template defines a conversion from a const c-string to an std::pair of objs, both of any type.
 */
template<class T, class U>
struct String2Value< std::pair<T, U> >
{
	/** Defines a TypeUtil of the first pair type as type traits T. **/
	typedef TypeUtil< T > type_traitsT;
	/** Defines a TypeUtil of the second pair type as type traits U. **/
	typedef TypeUtil< U > type_traitsU;
	/** Converts a const c-string to a std::pair of objects where the first and second
	 *  object can have any type. It splits the c-string using the first character in the list of token characters
	 *  and returns an error if the string cannot be split into two parts or the first or second part of the c-string 
	 *  cannot be converted to the correct type.
	 *
	 * @param str the source c-string.
	 * @param val the destination object pair.
	 * @param m a list of character separators.
	 * @param c clear the object.
	 * @return true if conversion is successful.
	 */
	static bool convert(const char* str, std::pair<T, U>& val, const char* m, bool c)
	{
		std::string temp;
		if( m == 0 || *m == '\0' ) return false;
		const char* pos = std::strrchr(str, m[0]);
		if( pos == 0 || *pos == '\0' ) return false;
		temp = pos+1;
		if(!String2Value<U>::convert(temp.c_str(), val.second, m, c)) return false;
		temp.assign(str, pos);
		if(!String2Value<T>::convert(temp.c_str(), val.first, m, c)) return false;
		return true;
	}
};
/** @brief Converts a string to a vector of values of any type.
 * 
 * This specialized class template defines a conversion from a const c-string to a vector of any type.
 */
template<class T>
struct String2Value< std::vector<T> >
{
	/** Defines a TypeUtil of specified type as type traits. **/
	typedef TypeUtil< T > type_traits;
	/** Converts a const c-string to a std::vector (or list) of objects of any type. It splits the 
	 *  c-string using the first character in the list of token characters and returns an 
	 *  error if a tokenized string cannot be converted to the proper type.
	 *
	 * @param str the source c-string.
	 * @param cont the destination vector of types.
	 * @param m a list of character separators.
	 * @param c clear the object.
	 * @return true if conversion is successful.
	 */
	static bool convert(const char* str, std::vector<T>& cont, const char* m, bool c)
	{
		if( m == 0 || *m == '\0' ) return false;
		char ch = m[0];
		T value;
		std::string buf;
		unsigned int i=0, j=0;
		if( c ) cont.clear();
		while(str[i] != '\0')
		{
			if( str[i] == ch) while(str[i+1] != '\0' && str[i+1] == ch) ++i;
			if( str[i] == ch || str[i+1] == '\0' )
			{
				if( str[i+1] == '\0' && str[i] != ch ) buf.assign(str+j, str+i+1);
				else buf.assign(str+j, str+i);
				buf = trim(buf);
				if( !buf.empty() )
				{
					if(j == 0 && str[i+1] == '\0' )
					{
						if(!String2Value<T>::convert(str, value, m+1, c)) return false;
						cont.push_back(value);
						break;
					}
					else
					{
						if(!String2Value<T>::convert(buf.c_str(), value, m+1, c)) return false;
						cont.push_back(value);
					}
				}
				j = i+1;
			}
			++i;
		}
		return true;
	}
};
/** @brief Converts a value of any type to a string.
 * 
 * This class template defines a conversion from an object of any type to a string.
 */
template<class T>
struct Value2String
{
	/** Converts an object of any type to a c-string.
	 *
	 * @param val a source object of any type.
	 * @param str the destination c-string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const T& val, char* str, const char* m)
	{
		std::ostringstream convert;
		convert << val;
		std::strcpy(str, convert.str().c_str());
	}
	/** Converts an object of any type to a std::string.
	 *
	 * @param val a source object of any type.
	 * @param str the destination std::string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const T& val, std::string& str, const char* m)
	{
		std::ostringstream convert;
		convert << val;
		str = convert.str();
	}
};

/** @brief Converts a pair values of any type to a string.
 * 
 * This specialized class template defines a conversion from a pair of types to a string.
 */
template<class T, class U>
struct Value2String< std::pair<T,U> >
{
	/** Defines a std::pair of objects as a value type. **/
	typedef std::pair<T,U> value_type;
	/** Converts a pair of objects of any type to a c-string. It uses the
	 * first character in the list of tokens to separate each object in the
	 * destination string. 
	 *
	 * @param val a source object of any type.
	 * @param str the destination c-string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const value_type& val, char* str, const char* m)
	{
		std::string tmp;
		convert(val, tmp, m);
		std::strcpy(str, tmp.c_str());

	}
	/** Converts an object of any type to a std::string. It uses the
	 * first character in the list of tokens to separate each object in the
	 * destination string. 
	 *
	 * @param val a source object of any type.
	 * @param str the destination std::string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const value_type& val, std::string& str, const char* m)
	{
		Value2String<T>::convert(val.first, str, m+1);
		if( m != 0 && *m != '\0' ) str+=m[0];
		else str+=" ";
		std::string tmp;
		Value2String<U>::convert(val.second, tmp, m+1);
		str+=tmp;
	}
};
/** @brief Converts a string to a string.
 * 
 * This specialized class template defines a conversion from a std::string to a string.
 */
template<>
struct Value2String< std::string >
{	
	/** Converts an std::string to a c-string using strcpy.
	 *
	 * @param val a source std::string.
	 * @param str the destination c-string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const std::string& val, char* str, const char* m)
	{
		std::strcpy(str, val.c_str());
	}
	/** Converts an std::string to a std::string using a simple assignment.
	 *
	 * @param val a source std::string.
	 * @param str the destination std::string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const std::string& val, std::string& str, const char* m)
	{
		str=val;
	}
};
/** @brief Converts a c-string to a string.
 * 
 * This specialized class template defines a conversion from a c-string to a string.
 */
template<>
struct Value2String< char* >
{
	/** Converts a c-string to a c-string using strcpy.
	 *
	 * @param val a source std::string.
	 * @param str the destination c-string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const char* val, char* str, const char* m)
	{
		std::strcpy(str, val);
	}
	/** Converts an c-string to a std::string using a simple assignment.
	 *
	 * @param val a source std::string.
	 * @param str the destination std::string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const char* val, std::string& str, const char* m)
	{
		str=val;
	}
};
/** @brief Converts a constant c-string to a string.
 * 
 * This specialized class template defines a conversion from a const c-string to a string.
 */
template<>
struct Value2String< const char* >
{
	/** Converts a c-string to a c-string using strcpy.
	 *
	 * @param val a source std::string.
	 * @param str the destination c-string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const char* val, char* str, const char* m)
	{
		std::strcpy(str, val);
	}
	/** Converts an c-string to a std::string using a simple assignment.
	 *
	 * @param val a source std::string.
	 * @param str the destination std::string.
	 * @param m an unused list of character separators.
	 */
	static void convert(const char* val, std::string& str, const char* m)
	{
		str=val;
	}
};
/** @brief Converts a vector of objects of any type to a string.
 * 
 * This class template defines a conversion from a vector of objects of any type to a string.
 */
template<class T>
struct Value2String< std::vector<T> >
{
	/** Converts a std::vector of objects of any type to a c-string. It uses the
	 * first character in the list of tokens to separate each object in the
	 * destination string. 
	 *
	 * @param array a source vector of objects.
	 * @param str the destination c-string.
	 * @param m a list of character separators.
	 */
	static void convert(const std::vector<T>& array, char* str, const char* m)
	{
		if( m == 0 || *m == '\0') return;
		std::string temp;
		convert(array, temp, m);
		std::strcpy(str, temp.c_str());
	}
	/** Converts a std::vector of objects of any type to a c-string. It uses the
	 * first character in the list of tokens to separate each object in the
	 * destination string. 
	 *
	 * @param cont array a source vector of objects.
	 * @param str the destination value of type std::string.
	 * @param m a list of character separators.
	 */
	static void convert(const std::vector<T>& cont, std::string& str, const char* m)
	{
		if( m == 0 || *m == '\0') return;
		char ch=m[0];
		std::string buf;
		for(typename std::vector<T>::const_iterator it=cont.begin();it != cont.end();)
		{
			Value2String<T>::convert(*it, buf, m+1);
			str+=buf;
			++it;
			if(it != cont.end()) str+=ch;
		}
	}
};




#endif












