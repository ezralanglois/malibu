/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * stringutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_STRINGUTIL_H
#define _EXEGETE_STRINGUTIL_H
#include "debugutil.h"
#include "memoryutil.h"
#include "strconvertutil.h"
#include "fileutil.h"
#include <string>
#include <vector>
#include <algorithm>

/** @file stringutil.h
 * @brief Defines a set of string utility functions.
 * 
 * This header file contains a set of functions to support string operations
 * including conversion between types, trimming white space, creating new strings,
 * comparing strings and simple string parsing.
 *
 * @ingroup ExegeteUtil
 * @see TypeUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @brief Tests if a character is not a space.
 *
 * @param ch a character.
 * @return true if character is not a space character.
 */
inline static bool isnotspace(char ch)
{
	return IS_SPACE(ch) == 0;
}
/** @brief Trims space on the right side of a std::string.
 *
 * @param source a std::string to trim.
 * @return a trimed std::string.
 */
//cite where got this from
inline std::string rtrim(const std::string& source)
{
	std::string str = source;
	return str.erase( str.size()-(std::find_if(str.rbegin(), str.rend(), isnotspace)-str.rbegin()) );
}
/** @brief Trims space on the left side of a std::string.
 *
 * @param source a std::string to trim.
 * @return a trimed std::string.
 */
inline std::string ltrim(const std::string& source)
{
	std::string str = source;
	return str.erase( 0, std::find_if(str.begin(), str.end(), isnotspace)-str.begin() );
}
/** @brief Trims space on both sides of a std::string.
 *
 * @param source a std::string to trim.
 * @return a trimed std::string.
 */
inline std::string trim(const std::string& source)
{
	return ltrim( rtrim( source ) );
}
/** @brief Converts a c-string to a value of any type.
 *
 * @see String2Value
 * 
 * @param str source string.
 * @param val destination value of any type.
 * @param m a list of tokens.
 * @param c should clear object.
 * @return true if the conversion was successful.
 */
template<class T>
bool stringToValue(const char* str, T& val, const char* m=0, bool c = true)
{
	return String2Value<T>::convert(str, val, m, c);
}
/** @brief Converts a std::string to a value of any type.
 *
 * @see String2Value
 * 
 * @param str source string.
 * @param val destination value of any type.
 * @param m a list of tokens.
 * @param c should clear object.
 * @return true if the conversion was successful.
 */
template<class T>
bool stringToValue(const std::string& str, T& val, const char* m=0, bool c = true)
{
	return String2Value<T>::convert(str.c_str(), val, m, c);
}
/** @brief Converts a value of any type to a c-string.
 *
 * @see Value2String
 * 
 * @param val source value of any type.
 * @param str destination string.
 * @param m a list of tokens.
 */
template<class T>
void valueToString(const T& val, char* str, const char* m=0)
{
	Value2String<T>::convert(val, str, m);
}
/** @brief Converts a value of any type to a std::string.
 *
 * @see Value2String
 * 
 * @param val source value of any type.
 * @param str destination string.
 * @param m a list of tokens.
 */
template<class T>
void valueToString(const T& val, std::string& str, const char* m=0)
{
	Value2String<T>::convert(val, str, m);
}
/** @brief Unescapes a string of hexidecimal characters
 * 
 * Unescapes a string of hexidecimal characters and returns a string with their actual equivalents.
 *
 * @todo link unescape to specification
 *
 * @param escape a string with escaped hexidecimal digits.
 * @return a string with ascii equivalents.
 */
template<class U>
std::basic_string<U> unescape(const std::basic_string<U>& escape)
{
	char* end;
	std::basic_string<U> str;
	std::basic_string<U> hex;
	int ch;
	for(typename std::basic_string<U>::const_iterator it = escape.begin();it != escape.end();++it)
	{
		if(*it == '%')
		{
			hex="";
			++it;hex += (*it);
			++it;hex += (*it);
			ch = int( strtol(hex.c_str(), &end, 16));
			str+=char(ch);
		}
		else str+=(*it);
	}
	return str;
}
/** @brief Compare two strings, ignoring case, for a specified number of characters.
 *
 * @param str1 first string to compare.
 * @param str2 second string to compare.
 * @param n number of characters from beginning to compare.
 * @param end the position in the first string at which to stop.
 * @return 1 if the strings are equivalent.
 */
inline int stricmpn(const char* str1, const char* str2, unsigned int n, const char* end=0)
{
	if( end == 0 ) end=str1+n;
	for(;str1 != end && *str1 != '\0' && *str2 != '\0';++str1,++str2) 
		if( std::toupper(*str1) != std::toupper(*str2) ) return 0;
	return str1 == end || (*str1 == '\0' && *str2 == '\0');
}
/** @brief Compares two strings, for a specified number of characters.
 *
 * @param str1 first string to compare.
 * @param str2 second string to compare.
 * @param n number of characters from beginning to compare.
 * @param end the position in the first string at which to stop.
 * @return 1 if the strings are equivalent.
 */
inline int strcmpn(const char* str1, const char* str2, unsigned int n, const char* end=0)
{
	if( end == 0 ) end=str1+n;
	for(;str1 != end && *str1 != '\0' && *str2 != '\0';++str1,++str2) 
		if( *str1 != *str2 ) return 0;
	return str1 == end || (*str1 == '\0' && *str2 == '\0');
}
/** @brief Compares to strings with limited regexp.
 * 
 * Compares to strings, ignoring case and regular expression ?, for a specified number of characters.
 *
 * @param str1 first string to compare.
 * @param str2 second string to compare.
 * @param n number of characters from beginning to compare.
 * @param end the position in the first string at which to stop.
 * @return 1 if the strings are equivalent.
 */
inline int stricmpn_regex(const char* str1, const char* str2, unsigned int n, const char* end=0)
{
	if( end == 0 ) end=str1+n;
	for(;str1 != end && *str1 != '\0' && *str2 != '\0';++str1,++str2) 
		if( std::toupper(*str1) != std::toupper(*str2) && *str2 != '?' ) return 0;
	return str1 == end || (*str1 == '\0' && *str2 == '\0');
}

/** @brief Splits a string by some token into a list.
 * 
 * Splits a string at each space and stores the values in some container that allows push_back.
 *
 * @param str the string to split.
 * @param list a reference to a destination container.
 */
template<class T>
void split2words(const std::string& str, T& list)
{
	std::istringstream iss(str);
	std::istream_iterator<std::string> beg(iss);
	std::istream_iterator<std::string> end;
	std::back_insert_iterator<T> it(list);
	std::copy(beg, end, it);
}

/** @brief Splits a string by some token into two strings.
 * 
 * Split the source string by some character token at first occurrence.
 * 
 * @param sr a source string.
 * @param beg the string before the split.
 * @param end the string after the split
 * @param ch a character token.
 */
inline void split_first(const std::string& sr, std::string& beg, std::string& end, char ch)
{
	std::string::size_type n;
	std::string source = sr;
	if( (n=source.find_first_of(ch)) != std::string::npos)
	{
		beg = source.substr(0, n);
		end = source.substr(n+1);
	}
}

/** @brief Splits a string by some token into a list.
 * 
 * Splits a string into no more that n parts at each space and stores 
 * the values in some container that allows push_back.
 *
 * @param str the string to split.
 * @param list a reference to a destination container.
 * @param n only save the first n parts.
 */
template<class T>
void split2words(const std::string& str, T& list, unsigned int n)
{
	std::istringstream iss(str);
	std::istream_iterator<std::string> beg(iss);
	std::istream_iterator<std::string> end;
	std::back_insert_iterator<T> it(list);
	for(unsigned int i=0;i<n && beg != end;++i, ++it, ++beg) (*it) = (*beg);
	for(;beg != end;++beg) list.back() += " "+(*beg);
}

/** @brief Copies a string.
 * 
 * Copies a std::string to a c-string allocating the appropriate amount of memory.
 *
 * @param str a source string
 * @return newly allocated array of characters.
 */
inline char* strdupl(std::string str)
{
	char* new_str = setsize<char>(str.length()+1);
	strcpy(new_str, str.c_str());
	new_str[str.length()] = '\0';
	return new_str;
}
/** @brief Copies a string.
 * 
 * Copies an array of characters to a c-string allocating the appropriate amount of memory.
 *
 * @param str a source string.
 * @return newly allocated array of characters.
 */
inline char* strdupl(const char* str)
{
	int len = strlen(str);
	char* new_str = setsize<char>(len+1);
	strcpy(new_str, str);
	new_str[len] = '\0';
	return new_str;
}
/** @brief Modify a file name by adding prefix and numeric suffix.
 * 
 * @param outputfile the original file name.
 * @param pfx prefix for file name.
 * @param r run suffix.
 * @param f fold suffix.
 * @return new file name.
 */
inline std::string genfile(std::string outputfile, std::string pfx, unsigned int r, unsigned int f)
{
	std::string run, fold;
	if( f != UINT_MAX) valueToString(f, fold);
	if( r != UINT_MAX) valueToString(r, run);
	if( !pfx.empty() && !outputfile.empty() ) pfx += "_";
	std::string ext;
	if( !outputfile.empty() )
	{
		pfx += base_name(outputfile.c_str());
		ext = ext_name(pfx.c_str());
		pfx = strip_ext(pfx.c_str());
		std::string dir=dir_name(outputfile.c_str());
		if( dir != "" && dir != base_name(outputfile.c_str()) ) 
			outputfile = join_file(dir.c_str(), pfx.c_str());
		else outputfile = pfx;
	}
	else outputfile = pfx;
	if( !run.empty() ) outputfile+="_"+run;
	if( !fold.empty() )
	{
		outputfile+='_';
		outputfile+=fold;
	}
	if( ext != "" ) outputfile+="."+ext;
	return outputfile;
}


#endif


