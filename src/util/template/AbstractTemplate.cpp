/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AbstractTemplate.cpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

/** @file AbstractTemplate.cpp
 * @brief Contains the implementation of the abstract template.
 * 
 * This source file contains implementation source for AbstractTemplate.
 *
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

#include "AbstractTemplate.h"
#include "errorutil.h"

char* find_token_end(char* layout, char* layoutEnd, const char* token)
{
	const char* tok;
	while(layout != layoutEnd)
	{
		for(tok=token;*tok != '\0' && layout != layoutEnd && *tok == *layout;++tok) ++layout;
		if( *tok == '\0' ) return layout;
		if( layout != layoutEnd ) ++layout;
	}
	return 0;
}

char* rfind_token(char* layout, char* layoutEnd, const char* token)
{
	const char* tok;
	while(layout != layoutEnd)
	{
		for(tok=token;*tok != '\0' && layout != layoutEnd && *tok == *layout;--tok) --layout;
		if( *tok == '\0' ) return layout+1;
		if( *(tok-1) == '\0' ) return layout;
		if( layout != layoutEnd ) --layout;
	}
	return 0;
}

char* find_token(char* layout, char* layoutEnd, const char* token)
{
	char* layoutBeg;
	const char* tok;
	while(layout != layoutEnd)
	{
		layoutBeg=layout;
		for(tok=token;*tok != '\0' && layoutBeg != layoutEnd && *tok == *layoutBeg;++tok) ++layoutBeg;
		if( *tok == '\0' ) return layout;
		if( layoutBeg != layoutEnd ) layout=layoutBeg+1;
		else layout=layoutBeg;
	}
	return 0;
}

const char* build_child(char* layout, char* layoutEnd, TemplateNode::pointer node, const char toks[][6])//, const char const*const* toks)
{
	const char* msg;
	const char* begtok = toks[1]+1;
	const char* endtok = toks[0]+1;
	char *beg, *end, *nxt, tmp;
	int beglen = (int)strlen(begtok);
	int endlen = (int)strlen(endtok);
	ASSERT(layout != 0);
	while(layout != layoutEnd)
	{
		ASSERT(layout < layoutEnd);
		if( (end=find_token(layout, layoutEnd, endtok)) == 0 ) break;
		if( (beg=rfind_token(end, layout, begtok+beglen-1)) == 0 ) return ERRORMSG("Error parsing token " << layout);
		if( beg != layout ) node->data(layout);//add empty
		if( ( *(end-2) == '/' && *(end-1) == '/' ) || end+endlen == layoutEnd )
		{
			node->code(beg+beglen, end-3);
			layout = end+endlen;
		}
		else
		{
			tmp=*(end+endlen); *(end+endlen)=0;
			if( (nxt = find_token_end(end+endlen+1, layoutEnd, beg)) == 0 ) return ERRORMSG("Error parsing token span: " << beg << " \n\n " << std::string(beg+beglen, end));// << " \n\n " << layoutEnd );
			*(end+endlen)=tmp;
			node->code(beg+beglen, end);
			if( (msg=build_child(end+endlen, nxt, node->addchild(), toks)) != 0 ) return msg;
			layout = nxt;
		}
		*beg = 0;
		node = node->addsibling();
	}
	return 0;
}

void AbstractTemplate::prune_child(TemplateNode::pointer node, const char* last, const char* empty)
{
	for(;node != 0;node = node->nextsibling())
	{
		if( node->code() != 0 ) node->id(lex(node->code()));
		else node->code(last);
		if( node->nextsibling() != 0 && node->nextsibling()->code() == 0 && node->nextsibling()->data() == 0 ) 
		{
			delete node->nextsibling();
			node->nextsibling(0);
		}
		else if( node->nextsibling() != 0 && node->nextsibling()->code() == 0 ) node->nextsibling()->code(last);
		if( node->nextchild() ) prune_child(node->nextchild(), last, empty);
	}
}


const char* AbstractTemplate::build_template(char* layout)
{
	char toks[][6] = { {0, '!', '-', '-', '>', 0}, {0, '<', '!', '-', '-', 0} };
	char* layoutEnd=layout+strlen(layout);
	const char* msg = build_child(layout, layoutEnd, &root, toks);
	if( msg != 0 ) return msg;
	prune_child(&root, last, empty);
	return 0;
}


const char* AbstractTemplate::write(std::ostream& out, const_pointer node)const
{
	bool flag=false;
	for(;node != 0;node=node->nextsibling())
	{
		if( symbolcheck(node) )
		{
			if( node->data() != 0 ) out << node->data();
			do{
				symbolwrite(out, node);
				if( node->nextchild() )
				{
					write(out, node->nextchild());
					flag=symbolwritelast(out, node);
				}
			}while(flag);
		}
	}
	return 0;
}


