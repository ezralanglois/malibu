/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * AbstractTemplate.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_ABSTRACTTEMPLATE_H
#define _EXEGETE_ABSTRACTTEMPLATE_H
#include "TemplateNode.h"
#include <string>
#include <iostream>

/** @file AbstractTemplate.h
 * @brief Defines an abstract template class.
 * 
 * This file contains the AbstractTemplate class template.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @brief Defines an abstract template interface.
 * 
 * This class defines a AbstractTemplate class. An abstract template performs
 *  two main functions:
 *	# Reads in a template format
 *	# Provides an interface for writing an object in this format. 
 */
class AbstractTemplate
{
	typedef TemplateNode::pointer pointer;
	typedef TemplateNode::const_pointer const_pointer;

public:
	/** Constructs an abstract template.
	 */
	AbstractTemplate()
	{
	}
	/** Destructs an abstract template.
	 */
	virtual ~AbstractTemplate()
	{
	}

public:
	/** Builds a template from a string.
	 *
	 * @param layout a string containing the template layout.
	 * @return an error message.
	 */
	const char* build(const std::string& layout)
	{
		templatestr=layout;
		return build_template( & (*templatestr.begin()) );
	}
	/** Gets an error message.
	 *
	 * @return an error message.
	 */
	const char* errormsg()const
	{
		return errmsg;
	}

protected:
	/** Prune the template tree.
	 *
	 * @param node a template node.
	 * @param last a speical string code for last symbol.
	 * @param empty an empty string code.
	 */
	void prune_child(pointer node, const char* last, const char* empty);
	/** Builds a template node tree from a string template.
	 * 
	 * @param layout a template layout string.
	 * @return an error message or NULL.
	 */
	const char* build_template(char* layout);
	/** This method writes an object in the specified template to the output stream.
	 *
	 * @param out an output stream.
	 * @param node a constant pointer to a template node.
	 * @return an error message or NULL.
	 */
	const char* write(std::ostream& out, const_pointer node)const;
	/** Writes the template to the output stream.
	 *
	 * @param out an output stream.
	 * @param format an abstract template.
	 * @return an output stream.
	 */
	friend std::ostream& operator<<(std::ostream& out, const AbstractTemplate& format)
	{
		format.errmsg = format.write(out, &format.root);
		if( format.errormsg() != 0 ) out.setstate( std::ios::failbit );
		return out;
	}
	/** Reads a template from the input stream.
	 *
	 * @param in an input stream.
	 * @param format an abstract template.
	 * @return an input stream.
	 */
	friend std::istream& operator>>(std::istream& in, AbstractTemplate& format)
	{
		std::string line;
		format.templatestr = "";
		while( !in.eof() )
		{
			std::getline(in, line);
			format.templatestr += line + "\n";
		}
		format.errmsg = format.build_template( & (*format.templatestr.begin()) );
		if( format.errormsg() != 0 ) in.setstate( std::ios::failbit );
		return in;
	}

protected:
	/** Checks if a symbol should be written.
	 *
	 * @param node a constant pointer to a template node.
	 * @return true if the symbol should be written out.
	 */
	virtual bool symbolcheck(const_pointer node)const=0;
	/** Writes the data corresponding to the symbol.
	 *
	 * @param out an output stream.
	 * @param node a constant pointer to a template node.
	 */
	virtual void symbolwrite(std::ostream& out, const_pointer node)const=0;
	/** Writes out the last symbol.
	 *
	 * @param out an output stream.
	 * @param node a constant pointer to a template node.
	 * @return true if the wrapper should be repeated.
	 */
	virtual bool symbolwritelast(std::ostream& out, const_pointer node)const=0;
	/** Gets an integer identifier of a string symbol.
	 *
	 * @param str a string symbol.
	 * @return integer identifier.
	 */
	virtual int lex(const char* str)const=0;

private:
	std::string templatestr;
	TemplateNode root;
	const char* last;
	const char* empty;
	mutable const char* errmsg;
};


#endif


