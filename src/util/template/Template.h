/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * Template.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_TEMPLATE_H
#define _EXEGETE_TEMPLATE_H
#include "AbstractTemplate.h"

/** @file Template.h
 * @brief Defines the template class and related classes.
 * 
 * This file contains the Template, AbstractTemplateSymbol and 
 * TemplateSymbolFunctor class templates.
 * 
 * @attention A template reads and holds a format and writes an object in that format.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @brief Defines a template format symbol functor.
 * 
 * Defines an template symbol functor. A functor is an object that holds
 *  a pointer to some function as well as a name/description for that function.
 */
template<class T>
class TemplateSymbolFunctor
{
public:
	/** Defines template argument as argument type. **/
	typedef T argument_type;
private:
	typedef void (T::*fsymbolwrite)(std::ostream& out, const std::string& nameStr, const char* val);
	typedef bool (T::*fsymbolnext)(const std::string& nameStr, const char* val);
	typedef bool (T::*fsymbolcheck)(const std::string& nameStr, const char* val);

public:
	/** Constructs a template symbol functor with a given name, description and
	 *  pointers to a set of functions that write, go to next, and test symbols.
	 *
	 * @param nm name of symbol.
	 * @param ds description of symbol.
	 * @param w a pointer to a function that writes a symbol value.
	 * @param n a pointer to a function that gets the next symbol.
	 * @param c a pointer to a function that tests if the next symbol should be written.
	 */
	TemplateSymbolFunctor(const std::string& nm, const std::string& ds, fsymbolwrite w, fsymbolnext n, fsymbolnext c) : nameStr(nm), descStr(ds), psymbolwrite(w), psymbolnext(n), psymbolcheck(c)
	{
	}
	/** Destructs a template symbol functor.
	 */
	~TemplateSymbolFunctor()
	{
	}

public:
	/** Tests if the name of this symbol equals the given string.
	 *
	 * @param str string to test.
	 * @return true if string equals name.
	 */
	bool operator==(const std::string& str)const
	{
		return nameStr == str;
	}
	/** Gets the name of the symbol.
	 *
	 * @return name of the symbol.
	 */
	const std::string& name()const
	{
		return nameStr;
	}
	/** Gets the description of the symbol.
	 *
	 * @return description of the symbol.
	 */
	const std::string& description()const
	{
		return descStr;
	}
	/** Tests if the name of this symbol equals the given string.
	 *
	 * @param str string to test.
	 * @return true if string equals name.
	 */
	bool test(const std::string& str)const
	{
		return nameStr == str;
	}

public:
	/** Writes a record of data in place of the symbol.
	 *
	 * @param out an output stream.
	 * @param pos the current position in the data.
	 * @param val some value of the symbol.
	 */
	void symbolwrite(std::ostream& out, argument_type& pos, const char* val)
	{
		if( psymbolwrite != 0 ) ((pos).*(psymbolwrite))(out, nameStr, val);
	}
	/** Moves current position to next record of data.
	 *
	 * @param pos the current position in the data.
	 * @param val some value of the symbol.
	 * @return true if should call next again.
	 */
	bool symbolnext(argument_type& pos, const char* val)
	{
		if( psymbolnext != 0 ) return ((pos).*(psymbolnext))(nameStr, val);
		return false;
	}
	/** Checks if the symbol should be written out with the current record.
	 *
	 * @param pos the current position in the data.
	 * @param val some value of the symbol.
	 * @return true if the symbol should be written.
	 */
	bool symbolcheck(argument_type& pos, const char* val)
	{
		if( psymbolcheck != 0 ) return ((pos).*(psymbolcheck))(nameStr, val);
		return true;
	}

private:
	std::string nameStr;
	std::string descStr;
	fsymbolwrite psymbolwrite;
	fsymbolnext psymbolnext;
	fsymbolnext psymbolcheck;
};

/** @brief Defines an interface to a template symbol.
 * 
 * Defines an abstract template symbol. This class defines an interface to
 *  symbols in the template. A symbol has the following properties:
 *  	- a code or name
 * 		- a description
 * 		- a virtual method to write the data for the symbol.
 * 		- a virtual method to increment to the next record.
 * 		- a virtual method test if the symbol should be written.
 */
template<class T>
class AbstractTemplateSymbol
{
public:
	/** Defines template argument as argument type. **/
	typedef T argument_type;

public:
	/** Constructs a abstract template symbol.
	 *
	 * @param nm name of symbol.
	 * @param ds description of symbol.
	 */
	AbstractTemplateSymbol(const std::string& nm, const std::string& ds) : nameStr(nm), descStr(ds)
	{
	}
	/** Destructs an abstract template symbol.
	 */
	virtual ~AbstractTemplateSymbol()
	{
	}

public:
	/** Tests if the name of this symbol equals the given string.
	 *
	 * @param str string to test.
	 * @return true if string equals name.
	 */
	bool operator==(const std::string& str)const
	{
		return nameStr == str;
	}
	/** Gets the name of the symbol.
	 *
	 * @return name of the symbol.
	 */
	const std::string& name()const
	{
		return nameStr;
	}
	/** Gets the description of the symbol.
	 *
	 * @return description of the symbol.
	 */
	const std::string& description()const
	{
		return descStr;
	}
	/** Tests if the name of this symbol equals the given string.
	 *
	 * @param str string to test.
	 * @return true if string equals name.
	 */
	bool test(const std::string& str)const
	{
		return nameStr == str;
	}

public:
	/** Writes a data record in place of symbol.
	 *
	 * @warning This is a virtual function that does nothing.
	 * 
	 * @param out an output stream.
	 * @param pos the current position in the data records.
	 * @param val some value of the symbol.
	 */
	virtual void symbolwrite(std::ostream& out, argument_type& pos, const char* val)
	{
	}
	/** Moves current position to next data record.
	 *
	 * @warning This is a virtual function that does nothing.
	 * 
	 * @param pos the current position in the data records.
	 * @param val some value of the symbol.
	 */
	virtual bool symbolnext(argument_type& pos, const char* val)
	{
		return false;
	}
	/** Check if symbol should be written out at the current position.
	 * 
	 * @warning This is a virtual function that does nothing.
	 *
	 * @param pos the current position in the data records.
	 * @param val some value of the symbol.
	 */
	virtual bool symbolcheck(argument_type& pos, const char* val)
	{
		return true;
	}

private:
	std::string nameStr;
	std::string descStr;
};

/** @brief Defines an interface to a template tree.
 * 
 * This class defines a Template class. A template performs two functions:
 * 
 *  # Reads a format into a template structure.
 *  # Writes data in this format.
 */
template<class T>
class Template : public AbstractTemplate
{
	typedef TemplateNode::pointer pointer;
	typedef TemplateNode::const_pointer const_pointer;
public:
	/** Defines the template parameter as a symbol type. **/
	typedef T										symbol_type;
	/** Defines the symbol argument as symbol args. **/
	typedef typename symbol_type::argument_type		symbol_args;
	/** Defines a vector of symbol pointers. **/
	typedef std::vector< symbol_type* >				symbol_vector;

public:
	/** Constructs a default template.
	 */
	Template()
	{
	}
	/** Constructs an template with the symbol arguments.
	 *
	 * @param a symbol arguments.
	 */
	Template(const symbol_args& a) : args(a)
	{
	}
	/** Destructs an template. Deallocates all symbols.
	 */
	~Template()
	{
		for(typename symbol_vector::iterator it = syms.begin(), end = syms.end();it != end;++it) delete *it;
	}

public:
	/** Assigns symbol arguments to a template.
	 *
	 * @param a symbol arguments.
	 * @return reference to this object.
	 */
	Template& operator=(const symbol_args& a)
	{
		args = a;
		return *this;
	}
	/** Adds a symbol to the template.
	 *
	 * @param ptr a pointer to a symbol.
	 */
	void add(symbol_type* ptr)
	{
		syms.push_back(ptr);
	}

protected:
	/** Checks if a symbol should be written.
	 * 
	 * @note Overrides the virtual method in AbstractTemplate.
	 *
	 * @param node a constant pointer to a template node.
	 * @return true if the symbol should be written out.
	 */
	bool symbolcheck(const_pointer node)const
	{
		if( node->id() < (int)syms.size() )
			return syms[ node->id() ]->symbolcheck(args, node->value());
		return true;
	}
	/** Writes data for a corresponding symbol.
	 *
	 * @note Overrides the virtual method in AbstractTemplate.
	 * 
	 * @param out an output stream.
	 * @param node a constant pointer to a template node.
	 */
	void symbolwrite(std::ostream& out, const_pointer node)const
	{
		if( node->id() < (int)syms.size() )
			return syms[ node->id() ]->symbolwrite(out, args, node->value());
	}
	/** Increments to next symbol, called when last symbol is reached.
	 *
	 * @note Overrides the virtual method in AbstractTemplate.
	 * 
	 * @param out an output stream.
	 * @param node a constant pointer to a template node.
	 * @return true if the wrapper should be repeated.
	 */
	bool symbolwritelast(std::ostream& out, const_pointer node)const
	{
		if( node->id() < (int)syms.size() )
			return syms[ node->id() ]->symbolnext(args, node->value());
		return false;
	}
	/** Gets integer map to string symbol.
	 * 
	 * @note Overrides the virtual method in AbstractTemplate.
	 *
	 * @param str a string symbol.
	 * @return integer identifier.
	 */
	int lex(const char* str)const
	{
		for(unsigned int i=0;i<syms.size();++i)
			if( (*syms[i]) == str ) return i;
		return (int)syms.size();
	}

private:
	mutable symbol_vector syms;
	mutable symbol_args args;
};

#endif

