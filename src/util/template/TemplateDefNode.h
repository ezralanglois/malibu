/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * TemplateDefNode.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_TEMPLATEDEFNODE_H
#define _EXEGETE_TEMPLATEDEFNODE_H
#include "stringutil.h"

/** @file TemplateDefNode.h
 * @brief Defines a template definition node.
 * 
 * This file contains the TemplateDefNode class template.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @brief Defines a node holding a template definition.
 * 
 * This class defines a template definition node class. A template definition is
 *  a tree representation of a template allowing the developer to hardcode a set 
 *  of templates quickly and easily.
 * 
 *  A definition node defines the prefix and suffix surrounding the given code.
 */
class TemplateDefNode
{
public:
	/** Defines a std::vector of TemplateDefNode as node vector. **/
	typedef std::vector<TemplateDefNode>	node_vector;
	/** Defines a contant iterator to a node vector as a constant iterator. **/
	typedef node_vector::const_iterator		const_iterator;
	/** Defines an iterator to a node vector as an iterator. **/
	typedef node_vector::iterator			iterator;
	/** Defines a pointer to a node vector as a pointer. **/
	typedef node_vector::pointer			pointer;
public:
	/** Constructs a default template definition node.
	 */
	TemplateDefNode()
	{
	}
	/** Constructs a template definition node with a code, prefix, suffix and code.
	 *
	 * @param t a key code.
	 * @param p a prefix.
	 * @param s a suffix.
	 * @param ch a character flag.
	 */
	TemplateDefNode(const std::string& t, const std::string& p, const std::string& s, char ch='/') : tag(toTag(t,ch)), pfx(p), sfx(s)
	{
	}
	/** Destructs a default template definition node.
	 */
	~TemplateDefNode()
	{
	}

public:
	/** Sets key, prefix, suffix and code for the definition.
	 *
	 * @param t a code.
	 * @param p a prefix.
	 * @param s a suffix.
	 * @param ch a character flag.
	 * @return a pointer to this object.
	 */
	pointer set(const std::string& t, const std::string& p, const std::string& s, char ch='/')
	{
		tag = toTag(t,ch);
		pfx = p;
		sfx = s;
		nodes.clear();
		return this;
	}
	/** Adds a new template definition node with the specific key, prefix, suffix and code for the definition. The
	 * key is list of comma separated values for single template tags.
	 *
	 * @param t a code.
	 * @param p a prefix.
	 * @param s a suffix.
	 * @param ch a character flag.
	 */
	void sadd(const std::string& t, const std::string& p, const std::string& s, char ch='/')
	{
		std::vector<std::string> sarr;
		stringToValue(t, sarr, ",");
		for(unsigned int i=0;i<sarr.size();++i)
			nodes.push_back(TemplateDefNode(sarr[i],p,s,ch));
	}
	/** Adds a new template definition node with the specific key, prefix, suffix and code for the definition.
	 *
	 * @param t a code.
	 * @param p a prefix.
	 * @param s a suffix.
	 * @param ch a character flag.
	 * @return pointer to the newly added child.
	 */
	pointer add(const std::string& t, const std::string& p, const std::string& s, char ch='/')
	{
		nodes.push_back(TemplateDefNode(t,p,s,ch));
		return &nodes.back();
	}
	/** Gets a string representation of the template.
	 *
	 * @return a string representation.
	 */
	std::string str()const
	{
		std::string s;
		if( !is_single(tag) )
		{
			s=tag+pfx;
			for(const_iterator beg=nodes.begin(),end=nodes.end();beg != end;++beg) s+=beg->str();
			s+=sfx+tag;
		}
		else s = pfx + tag + sfx;
		return s;
	}

protected:
	/** Tests if a tag is a single tag.
	 *
	 * @param str a string tag to test.
	 * @return true if the tag is single.
	 */
	static bool is_single(const std::string& str)
	{
		return str[str.length()-5] == '/' && str[str.length()-6] == '/';
	}
	/** Converts a tag name and code to a tag.
	 *
	 * @param tag a tag name.
	 * @param ch a tag code.
	 * @return a complete tag.
	 */
	static std::string toTag(std::string tag, char ch)
	{
		if( ch != '/' )
		{
			tag  = "<!-- "+tag;
			if( ch != 0 ) 
			{
				tag += ' ';
				tag += ch;
			}
			tag += " //!-->";
			return tag;
		}
		else return "<!-- "+tag+" !-->";
	}

private:
	node_vector nodes;
	std::string tag;
	std::string pfx;
	std::string sfx;
};

#endif


