/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * TemplateNode.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */


#ifndef _EXEGETE_TEMPLATENODE_H
#define _EXEGETE_TEMPLATENODE_H
#include "charutil.h"


/** @file TemplateNode.h
 * @brief Defines a concrete node in the template tree.
 * 
 * This file contains the TemplateNode class template.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */

/** @brief Defines a node in the template tree.
 * 
 * Defines a TemplateNode class with the following properties:
 *  # Internal ID
 *  # A code string
 *  # Static data in the vicinity of the code
 *  # Tree structure elements: child and sibling
 */
class TemplateNode
{
public:
	/** Defines a pointer to a TemplateNode as a pointer. **/
	typedef TemplateNode*			pointer;
	/** Defines a constant pointer to a TemplateNode as a constant pointer. **/
	typedef const TemplateNode*		const_pointer;
	/** Defines a constant pointer to a char as a string type. **/
	typedef const char*				string_type;

public:
	/** Constructs an empty template node.
	 */
	TemplateNode() : idInt(0), childPtr(0), siblingPtr(0), dataStr(0), codeStr(0), valueStr(0)
	{
	}
	/** Destructs a template node releasing both child and sibling pointers.
	 */
	~TemplateNode()
	{
		delete childPtr;
		delete siblingPtr;
	}

public:
	/** Sets the string data.
	 *
	 * @param d string data.
	 */
	void data(string_type d)
	{
		dataStr = d;
	}
	/** Sets the string code.
	 *
	 * @param d string code.
	 */
	void code(string_type d)
	{
		codeStr = d;
	}
	/** Converts a tag to a string code and a char value.
	 *
	 * @param b a begin pointer to a range of characters.
	 * @param e an end pointer to a range of characters.
	 */
	void code(char* b, char* e)
	{
		while(b != e && IS_SPACE(*b)) ++b;
		while(e != b && IS_SPACE(*e)) { *e = 0; --e; }
		codeStr = b;
		while( b != e && !IS_SPACE(*b) ) ++b;
		if( b != e ) 
		{
			*b = 0;++b;
			while(b != e && IS_SPACE(*b)) ++b;
			valueStr=b;
		}
	}
	/** Gets the string code.
	 *
	 * @return a string code.
	 */
	string_type code()const
	{
		return codeStr;
	}
	/** Gets the string value.
	 *
	 * @return a string value.
	 */
	string_type value()const
	{
		return valueStr;
	}
	/** Gets the string data.
	 *
	 * @return string data.
	 */
	string_type data()const
	{
		return dataStr;
	}
	/** Sets the internal integer code.
	 *
	 * @param i internal integer code.
	 */
	void id(int i)
	{
		idInt = i;
	}
	/** Gets the internal integer code.
	 *
	 * @return an internal integer code.
	 */
	int id()const
	{
		return idInt;
	}

public:
	/** This method creates and returns a pointer to a new sibling.
	 *
	 * @return a pointer to the new sibling.
	 */
	pointer addsibling()
	{
		return (siblingPtr=new TemplateNode);
	}
	/** Creates a new child template node.
	 *
	 * @return a pointer to the new child.
	 */
	pointer addchild()
	{
		return (childPtr=new TemplateNode);
	}
	/** Gets a constant pointer to the next sibling.
	 *
	 * @return a constant pointer to the next sibling.
	 */
	const_pointer nextsibling()const
	{
		return siblingPtr;
	}
	/** Gets a pointer to the next sibling.
	 *
	 * @return a pointer to the next sibling.
	 */
	pointer nextsibling()
	{
		return siblingPtr;
	}
	/** Gets a pointer to the next child.
	 *
	 * @return a pointer to the next child.
	 */
	pointer nextchild()
	{
		return childPtr;
	}
	/** Gets a constant pointer to the next child.
	 *
	 * @return a constant pointer to the next child.
	 */
	const_pointer nextchild()const
	{
		return childPtr;
	}
	/** Sets the next sibling.
	 *
	 * @param p a pointer to the next sibling.
	 */
	void nextsibling(pointer p)
	{
		siblingPtr = p;
	}
	/** Tests if the node has a child.
	 *
	 * @return true if child not NULL.
	 */
	bool isparent()const
	{
		return childPtr != 0;
	}
	/** Tests if the code is empty.
	 *
	 * @return true if the code is empty.
	 */
	bool islast()const
	{
		return *codeStr == '\0';
	}

private:
	int idInt;
	pointer childPtr;
	pointer siblingPtr;
	string_type dataStr;
	string_type codeStr;
	string_type valueStr;
};



#endif

