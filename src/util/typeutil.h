/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/*
 * typeutil.h
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_TYPEUTIL_H
#define _EXEGETE_TYPEUTIL_H

#include "charutil.h"
#include <float.h>
#include <limits.h>
#include <iterator>
#ifdef _MPI
#undef SEEK_SET
#undef SEEK_CUR
#undef SEEK_END
#include <mpi.h>
#endif

/** @file typeutil.h
 * @brief Defines a set of type utility classes.
 * 
 * This header file contains a set of classes and functions to deal with a variety of typed data constructs
 * in C++. The following is a summary of classes and their main function:
 * 
 *	- TypeUtil: name of type, test if valid conversion from string and test if type is POD, e.g. int, float, ...
 *	- SelectType: compile time selection of one of two types based on an integer known at compile time.
 *	- Type2Type: an empty class template that holds a type.
 *	- Int2Type: an empty class template that holds a type.
 *	- IsSameType: compile time test if two types are the same.
 *	- IsVoid: compile time test if type is void.
 *	- TypeTrait: a helper function for TypeUtil (so far a hack for value_type).
 *	- type_traits: provides a set of compile time tests for a type, e.g. is the type a discreet number
 * 
 * The following is a summary of the available functions:
 * 
 *	- type_valid: test if conversion from strong to Type is valid.
 *	- type_name: get name of type.
 *	- max: get maximum value of type, e.g. FLT_MAX for a single precision float.
 *	- min: get minimum value of type.
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** @brief Defines an empty type selector.
 * 
 * This class template selects a class based on the predicate.
 */
template<int I, class T, class U> struct SelectType;
/** @brief Defines a specialization to select the second type.
 * 
 * This class template is called with the predicate is false and selects
 * the second class (U).
 */
template<class T, class U> 
struct SelectType<0, T, U>
{
	/** This method defines the second argument U as Result. **/
	typedef U Result;
};
/** @brief Defines a specialization to select the first type.
 * 
 * This class template is called with the predicate is true and selects
 * the first class (T).
 */
template<class T, class U> 
struct SelectType<1, T, U>
{
	/** This method defines the first argument T as Result. **/
	typedef T Result;
};
/** @brief Defines a placeholder to a type.
 * 
 * This class template acts as a place holder for other types.
 */
template<class T> struct Type2Type{};

/** @brief Defines a placeholder for an integer.
 * 
 * This class template acts as a place holder for an integer.
 */
template<int I> struct Int2Type{};


/** @brief Compile time test if two types are the same (false).
 * 
 * This class template tests if two classes types are the same.
 */
template<class T, class U> 
struct IsSameType
{
	/** This enumerated type indicates the two types T and U as different. **/
	enum{value=0};
};

/** @brief Compile time test if two types are the same (true).
 * 
 * This class template specialization is used when both classes are the same.
 */
template<class T> 
struct IsSameType<T,T>
{
	/** This enumerated type indicates the two types T and T are the same. **/
	enum{value=1};
};

/** @brief Compile time test if type is void (false).
 * 
 * This class template serves to test is a particular type is void.
 */
template<class T>
struct IsVoid
{
	/** In this case, the type is not void so value is set to zero. */
	enum{value=0};
};
/** @brief Compile time test if type is void (true).
 * 
 * This class template specialization is used when type is void.
 */
template<>
struct IsVoid<void>
{
	/** In this case, the type is void so value is set to one. */
	enum{value=1};
};
/** @brief Type utility helper.
 * 
 * This class template provides basic type operations for some type (most likely a class).
 */
template<class T>
struct TypeTrait
{
	/** Defines a value type in T as a value type. **/
	typedef typename T::value_type value_type;
};
/** @brief Type utility interface for any (other) type.
 * 
 * This class template is used for general (unspecialized) types.
 */
template<class T>
struct TypeUtil
{
	/** An unspecified type is most likely a class and not primative.
	 */
	enum{ ispod=false };
	/** Defines a T value_type as a value type. **/
	typedef typename TypeTrait<T>::value_type value_type;
	/** Tests if a string can be converted to a specific type.
	 *  
	 * @note This unspecialized template assumes the class has the appropriate iostream operator and returns true.
	 *
	 * @param str a string to test.
	 * @return true always.
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return name of the type.
	 */
	static const char* name() 
	{
		return "any";
	}
};
/** @brief Type utility interface for a pair of types.
 * 
 * This class template provides basic type operations for a pair of types.
 */
template<class T, class U>
struct TypeUtil< std::pair<T, U> >
{
	/** Define a pair of types as a value type.
	 */
	typedef std::pair<T, U> value_type;
	enum{ ispod=false };
	/** Tests if a string can be converted to a specific type.
	 * 
	 * @note This specialized template assumes both types have the appropriate iostream operator and returns true.
	 *
	 * @param str a string to test
	 * @return true if conversion is valid
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return pair
	 */
	static const char* name() 
	{
		return "pair";
	}
};
/** @brief Type utility interface for a bool.
 * 
 * This class template provides basic type operations for the bool type.
 */
template<>
struct TypeUtil<bool>
{
	/** A bool type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	
	/** Defines bool as value type.
	 */
	typedef bool value_type;
	/** Tests if a string can be converted to a bool type.
	 *
	 * @param str a string.
	 * @return true if conversion is valid.
	*/
	static bool valid(const char* str)
	{
		return str != 0 && *str != '\0';
	}
	/** Gets the name of the type.
	 *
	 * @return bool
	 */
	static const char* name() 
	{
		return "bool";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return 1.
	 */
	static value_type max()
	{
		return 1;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return 0
	 */
	static value_type min()
	{
		return 0;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_CHAR
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_CHAR;
	}
#endif
};
/** @brief Type utility interface for a char.
 * 
 * This class template provides basic type operations for the char type.
 */
template<>
struct TypeUtil<char>
{
	/** A char type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines char as a value type.
	 */
	typedef char value_type;
	/** This function tests if a string can be converted to a char type.
	 *
	 * @param str a string to test
	 * @return true if conversion is valid
	 */
	static bool valid(const char* str)
	{
		return str != 0 && str[0] != '\0' && str[1] == '\0';
	}
	/** Gets the name of the type.
	 *
	 * @return char
	 */
	static const char* name() 
	{
		return "char";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return CHAR_MAX
	 */
	static value_type max()
	{
		return CHAR_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return CHAR_MIN
	 */
	static value_type min()
	{
		return CHAR_MIN;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_CHAR
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_CHAR;
	}
#endif
};
/** @brief Type utility interface for an unsigned char.
 * 
 * This class template provides basic type operations for the unsigned char type.
 */
template<>
struct TypeUtil<unsigned char>
{
	/** An unsigned char type is primative (no constructor/destructor).
	 */
	enum{ ispod=true };
	/** Defines the unsigned char as a value type.
	 */
	typedef unsigned char value_type;
	/** Tests if a string can be converted to a unsigned char type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return str != 0 && *str != '\0' && str[1] == '\0';
	}
	/** Gets the name of the type.
	 *
	 * @return unsigned char
	 */
	static const char* name() 
	{
		return "unsigned char";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return UCHAR_MAX
	 */
	static value_type max()
	{
		return UCHAR_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return 0
	 */
	static value_type min()
	{
		return 0;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_UNSIGNED_CHAR
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_UNSIGNED_CHAR;
	}
#endif
};
/** @brief Type utility interface for a float.
 * 
 * This class template provides basic type operations for the float type.
 */
template<>
struct TypeUtil<float>
{
	/** A float type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines a float as a value type.
	 */
	typedef float value_type;
	/** Tests if a string can be converted to a float type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		unsigned int i=0;
		if( str[i] == '\0' ) return false;
		if( str[i] == '-' || str[i] == '+' ) ++i;
		if( str[i] == '\0' ) return false;
		while( str[i] != '\0' && IS_DIGIT(str[i]) ) ++i;
		if( str[i] != '\0' && str[i] == '.' ) ++i;
		while( str[i] != '\0' && IS_DIGIT(str[i]) ) ++i;
		if( i > 0 && !IS_DIGIT(str[i-1]) ) return false; 
		if( i > 0 && (str[i] == 'e' || str[i] == 'E') ) 
		{
			++i;
			if( str[i] == '+' || str[i] == '-' ) ++i;
		}
		if( str[i] != '\0' && !IS_DIGIT(str[i]) ) return false;
		while( str[i] != '\0' && IS_DIGIT(str[i]) ) ++i;
		return str[i] == '\0' ? true : false;
	}
	/** Gets the name of the type.
	 *
	 * @return float
	 */
	static const char* name() 
	{
		return "float";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return FLT_MAX
	 */
	static value_type max()
	{
		return FLT_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return -FLT_MAX
	 */
	static value_type min()
	{
		return -FLT_MAX;
	}
	/** Gets the positive minimum value of the type.
	 *
	 * @return FLT_MIN
	 */
	static value_type fltmin()
	{
		return FLT_MIN;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_FLOAT
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_FLOAT;
	}
#endif
};
/** @brief Type utility interface for a double.
 * 
 * This class template provides basic type operations for the double type.
 */
template<>
struct TypeUtil<double>
{
	/** A double type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines double as a value type.
	 */
	typedef double value_type;
	/** Tests if a string can be converted to a double type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return TypeUtil<float>::valid(str);
	}
	/** Gets the name of the type.
	 *
	 * @return double
	 */
	static const char* name() 
	{
		return "double";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return DBL_MAX
	 */
	static value_type max()
	{
		return DBL_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return -DBL_MAX
	 */
	static value_type min()
	{
		return -DBL_MAX;
	}
	/** Gets the non-negative minimum value of the type.
	 *
	 * @return DBL_MIN
	 */
	static value_type fltmin()
	{
		return DBL_MIN;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_DOUBLE
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_DOUBLE;
	}
#endif
};
/** @brief Type utility interface for a long double.
 * 
 * This class template provides basic type operations for the long double type.
 */
template<>
struct TypeUtil<long double>
{
	/** A long double type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines long double as a value type.
	 */
	typedef long double value_type;
	/** Tests if a string can be converted to a long double type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return TypeUtil<float>::valid(str);
	}
	/** Gets the name of the type.
	 *
	 * @return long double
	 */
	static const char* name() 
	{
		return "long double";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return DBL_MAX
	 */
	static value_type max()
	{
		return DBL_MAX;
	}
	/** This function returns the minimum value of the type.
	 *
	 * @return -DBL_MAX
	 */
	static value_type min()
	{
		return -DBL_MAX;
	}
	/** Gets the minimum non-negative value of the type.
	 *
	 * @return DBL_MIN
	 */
	static value_type fltmin()
	{
		return DBL_MIN;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_LONG_DOUBLE
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_LONG_DOUBLE;
	}
#endif
};
/** @brief Type utility interface for a int.
 * 
 * This class template provides basic type operations for the int type.
 */
template<>
struct TypeUtil<int>
{
	/** A int type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines an int as a value type.
	 */
	typedef int value_type;
	/** Tests if a string can be converted to a int type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		unsigned int i=0;
		if( str[i] == '\0' ) return false;
		if( str[i] == '-' || str[i] == '+' ) ++i;
		if( str[i] == '\0' ) return false;
		while( str[i] != '\0' && IS_DIGIT(str[i]) ) ++i;
		return str[i] == '\0' || IS_DIGIT(str[i]) ? true : false;
	}
	/** Gets the name of the type.
	 *
	 * @return int
	 */
	static const char* name() 
	{
		return "int";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return INT_MAX
	 */
	static value_type max()
	{
		return INT_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return INT_MIN
	 */
	static value_type min()
	{
		return INT_MIN;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_INT
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_INT;
	}
#endif
};
/** @brief Type utility interface for a long.
 * 
 * This class template provides basic type operations for the long type.
 */
template<>
struct TypeUtil<long>
{
	/** A long type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines a long as a value type.
	 */
	typedef long value_type;
	/** Tests if a string can be converted to a long type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return TypeUtil<int>::valid(str);
	}
	/** Gets the name of the type.
	 *
	 * @return long
	 */
	static const char* name() 
	{
		return "long";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return LONG_MAX
	 */
	static value_type max()
	{
		return LONG_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return LONG_MIN
	 */
	static value_type min()
	{
		return LONG_MIN;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_LONG
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_LONG;
	}
#endif
};
/** @brief Type utility interface for a short.
 * 
 * This class template provides basic type operations for the short type.
 */
template<>
struct TypeUtil<short>
{
	/** A short type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines a short as a value type.
	 */
	typedef short value_type;
	/** Tests if a string can be converted to a short type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return TypeUtil<int>::valid(str);
	}
	/** Gets the name of the type.
	 *
	 * @return short
	 */
	static const char* name() 
	{
		return "short";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return SHRT_MAX
	 */
	static value_type max()
	{
		return SHRT_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return SHRT_MIN
	 */
	static value_type min()
	{
		return SHRT_MIN;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_SHORT
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_SHORT;
	}
#endif
};
/** @brief Type utility interface for an unsigned long.
 * 
 * This class template provides basic type operations for the unsigned long type.
 */
template<>
struct TypeUtil<unsigned long>
{
	/** A unsigned long type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines an unsigned long as a value type.
	 */
	typedef unsigned long value_type;
	/** Tests if a string can be converted to a unsigned long type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return TypeUtil<int>::valid(str) && str[0] != '-';
	}
	/** Gets the name of the type.
	 *
	 * @return unsigned long
	 */
	static const char* name() 
	{
		return "unsigned long";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return ULONG_MAX
	 */
	static value_type max()
	{
		return ULONG_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return 0
	 */
	static value_type min()
	{
		return 0;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_UNSIGNED_LONG
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_UNSIGNED_LONG;
	}
#endif
};
/** @brief Type utility interface for an unsigned int.
 * 
 * This class template provides basic type operations for the unsigned int type.
 */
template<>
struct TypeUtil<unsigned int>
{
	/** A unsigned int type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines an unsigned int as a value type.
	 */
	typedef unsigned int value_type;
	/** Tests if a string can be converted to a unsigned int type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return TypeUtil<int>::valid(str) && str[0] != '-';
	}
	/** Gets the name of the type.
	 *
	 * @return unsigned int
	 */
	static const char* name() 
	{
		return "unsigned int";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return UINT_MAX
	 */
	static value_type max()
	{
		return UINT_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return 0
	 */
	static value_type min()
	{
		return 0;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_UNSIGNED
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_UNSIGNED;
	}
#endif
};
/** @brief Type utility interface for a unsigned short.
 * 
 * This class template provides basic type operations for the unsigned short type.
 */
template<>
struct TypeUtil<unsigned short>
{
	/** A unsigned short type is primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines an unsigned short as a value type.
	 */
	typedef unsigned short value_type;
	/** Tests if a string can be converted to a unsigned short type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return TypeUtil<int>::valid(str) && str[0] != '-';
	}
	/** Gets the name of the type.
	 *
	 * @return unsigned short
	 */
	static const char* name() 
	{
		return "unsigned short";
	}
	/** Gets the maximum value of the type.
	 *
	 * @return USHRT_MAX
	 */
	static value_type max()
	{
		return USHRT_MAX;
	}
	/** Gets the minimum value of the type.
	 *
	 * @return 0
	 */
	static value_type min()
	{
		return 0;
	}
#ifdef _MPI
	/** Gets the MPI equivalent type.
	 * 
	 * @return MPI_UNSIGNED_SHORT
	 */
	static MPI_Datatype MPI_TYPE()
	{
		return MPI_UNSIGNED_SHORT;
	}
#endif
};
#include <string>
/** @brief Type utility interface for a string.
 * 
 * This class template provides basic type operations for a std::string type.
 *
 * @todo add MPI_TYPE for std::string
 */
template<>
struct TypeUtil< std::string >
{
	/** An a class type, non-POD.
	 */
	enum{ ispod=false };
	/** Defines std::string as a value type **/
	typedef std::string value_type;
	/** Tests if a string can be converted to a specific type.
	 *
	 * @param str a string to test.
	 * @return true
	 */
	static bool valid(const char* str)
	{
		return true;
	}
	/** Gets the name of the type.
	 *
	 * @return std::string
	 */
	static const char* name() 
	{
		return "std::string";
	}
};

/** @brief Type utility interface for a pointer.
 * 
 * This class template provides basic type operations for a pointer to some type.
 */
template<class T>
struct TypeUtil<T*>
{
	/** A pointer to a type is a primative (no constructor/destructor)
	 */
	enum{ ispod=true };
	/** Defines the value of the pointer as a value type.
	 */
	typedef typename TypeUtil<T>::value_type value_type;
	/** Tests if a string can be converted to a value of the pointer type.
	 *
	 * @param str a string to test.
	 * @return true if conversion is valid.
	 */
	static bool valid(const char* str)
	{
		return TypeUtil<T>::valid(str);
	}
	/** Gets the name of the type.
	 *
	 * @return pointer to typename
	 */
	static const char* name() 
	{
		static std::string tmp=std::string("pointer to ")+std::string(TypeUtil<T>::name());
		return tmp.c_str();
	}
	/** Gets the maximum value of the type of the pointer.
	 *
	 * @return maximum value of type.
	 */
	static value_type max()
	{
		return TypeUtil<T>::max();
	}
	/** Gets the minimum value of the type of the pointer.
	 *
	 * @return minimum value of type.
	 */
	static value_type min()
	{
		return TypeUtil<T>::min();
	}
};

/** @brief Defines traits for a type.
 * 
 * This class template provides traits for some type.
 */
template<class T>
struct type_traits
{
	/** Flag type as enumerated
	 *  - discreet
	 *  - floating
	 */
	enum{ 
		is_discreet = IsSameType<T,int>::value   || 
						IsSameType<T,short>::value || 
						IsSameType<T,long>::value || 
						IsSameType<T,unsigned int>::value || 
						IsSameType<T,unsigned long>::value || 
						IsSameType<T,unsigned char>::value ||
						IsSameType<T,char>::value ||
						IsSameType<T,bool>::value, 
		is_floating = 	IsSameType<T,float>::value  || 
						IsSameType<T,double>::value || 
						IsSameType<T,long double>::value
	};
};

/** @brief Tests if string can be converted to some type.
 *
 * @param str string to test conversion.
 * @return true if string can be converted to type.
 */
template<class T>
bool type_valid(const char* str)
{
	return TypeUtil<T>::valid(str);
}
/** @brief Gets name for type.
 *
 * @return name of type as a string.
 */
template<class T>
const char* type_name()
{
	return TypeUtil<T>::name();
}
/** @brief Gets maximum value of type.
 *
 * @return maximum value of type.
 */
template<class T>
T max()
{
	return TypeUtil<T>::max();
}
/** @brief Gets minimum value of type.
 *
 * @return minimum value of type.
 */
template<class T>
T min()
{
	return TypeUtil<T>::min();
}


namespace exegete
{
	/** @brief Defines a comparison between a pair and the first element in the pair.
	 * 
	 * This class template defines a comparison between a pair of objects and an object
	 * similar to the first objec in the pair.
	 */
	template<class T, class U>
	struct pair_first_less
	{
		/** Tests if the first object in the pair is less than the
		 * given value.
		 * 
		 * @param p a pair of object.
		 * @param val an object.
		 * @return true if the first object is less than the value.
		 */
		bool operator()(const std::pair<T,U>& p, const T& val)const
		{
			return p.first < val;
		}
		/** Tests if the given value is less than the first object.
		 * 
		 * @param val an object.
		 * @param p a pair of object.
		 * @return true if the given value is less than the first object the pair.
		 */
		bool operator()(const T& val, const std::pair<T,U>& p)const
		{
			return val < p.first;
		}
		/** Tests if the first object is less than the second.
		 * 
		 * @param val the first object.
		 * @param p the second object.
		 * @return true if the first object is less than the second.
		 */
		bool operator()(const T& val, const T& p)const
		{
			return val < p;
		}
		/** Tests if the first object in one pair is less than the first object in another.
		 * 
		 * @param lhs left hand side pair.
		 * @param rhs right hand side pair.
		 * @return true if the first object is less than the second.
		 */
		bool operator()(const std::pair<T,U>& lhs, const std::pair<T,U>& rhs)const
		{
			return lhs.first < rhs.first;
		}
	};
};

#endif


