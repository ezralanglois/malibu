/* Illinois Open Source License
 *
 * University of Illinois/NCSA
 * Open Source License
 * Copyright (C) 2006-2008, Laboratory of Computational Proteomics.�All rights reserved.
 *
 * Developed by:
 * Laboratory of Computational Proteomics
 * University of Illinois at Chicago 
 * http://proteomics.bioengr.uic.edu/malibu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the �Software�), to deal with the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions 
 *    and the following disclaimers.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *    and the following disclaimers in the documentation and/or other materials provided with the 
 *    distribution.
 * 3. Neither the names of Laboratory of Computational Proteomics, University of Illinois at Chicago, 
 *    nor the names of its contributors may be used to endorse or promote products derived from this 
 *    Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.�
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 */

/** @defgroup ExegeteUtil Utility Classes and Functions
 *  This group holds all the utility classes and functions.
 */


/*
 * version_utility.hpp
 * Copyright (C) 2006-2008 Robert Ezra Langlois
 */

#ifndef _EXEGETE_VERSION_UTILITY_HPP
#define _EXEGETE_VERSION_UTILITY_HPP
#include "stringutil.h"

/** @file version_utility.hpp
 * @brief Defines version utility functions.
 * 
 * This header contains several inline functions that convert an internal version
 * number to a formatted string and extract it's component parts:
 *  - major version
 *  - minor version
 *  - subminor version
 * 
 * Idea taken from: http://www.boost.org/doc/libs/1_36_0/boost/version.hpp
 *
 * @ingroup ExegeteUtil
 * @author Robert Ezra Langlois (ezra@uic.edu)
 * @version 1.0
 */


/** Get the major version of the version code.
 * 
 * @param v version code.
 * @return major version.
 */
inline int major_version(int v)
{
	return ( v / 100000 );
}

/** Get the minor version of the version code.
 * 
 * @param v version code.
 * @return minor version.
 */
inline int minor_version(int v)
{
	return ((v / 100) % 1000);
}

/** Get the subminor version of the version code.
 * 
 * @param v version code.
 * @return subminor version.
 */
inline int subminor_version(int v)
{
	return (v % 100);
}

/** Get the string representation of the version code.
 * 
 * @param v version code.
 * @return formatted string version.
 */
inline std::string version_string(int v)
{
	std::string tmp, str;
	valueToString(major_version(v), tmp);
	str += tmp;
	str += ".";
	valueToString(minor_version(v), tmp);
	if ( tmp.length() == 1 ) tmp = "0"+tmp;
	str += tmp;
	if( subminor_version(v) != 0 )
	{
		str += ".";
		valueToString(subminor_version(v), tmp);
		if ( tmp.length() == 1 ) tmp = "0"+tmp;
		str += tmp;
	}
	return str;
}

#endif
